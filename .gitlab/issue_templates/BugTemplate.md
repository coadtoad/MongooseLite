>***Title:***- Title of issue should be an explanation of expected behavior based on "should"
ex: "Ares should not blink red led on button press" (this should replace the title of the issue, not in description) 
 
## **SW Version - Commit #:**   
20.00.10 - 031015ae61ad3e8cded1b8d0bc215225dd74934a 
 
## **Issue:**   
**Expected behavior:** ex: "Button press should result in a chirp when held for 100ms"  
  
**Observed behavior:** ex: "Button would chirp immediately, need to insert 100ms debounce"   
 
## **State of device:**   
ex: Durring Smoke Alarm 
 
>***Attached File Naming Convention:***  (this should be removed, but name attached files in this format) 
#46_Platform_Title   
#46_NoBra_Title 
where the #number is the number of the issue.