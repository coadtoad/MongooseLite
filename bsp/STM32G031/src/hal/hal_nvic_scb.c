//******************************************************************
/*! \file hal_nvic_scb.c
 *
 * \brief Kidde: Mongoose-fw - Implement NVIC and SCB functions
 *
 * \author Daniel Dilts
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "hal_nvic.h"
#include "hal_scb.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

//*********************************************************************
/**
 * \brief  CPUID register fields
 */
//*********************************************************************
struct cpuid
{
    unsigned revision     : 4;  ///< Minor revision number \a m in the npm revision status
    unsigned partno       : 12; ///< Part number of the processor
    unsigned architecture : 4;  ///< Constant that defines the architecture of the processor
    unsigned variant      : 4;  ///< Major revision number \a n in the npm revision status
    unsigned implementer  : 8;  ///< Implementer code
};

//*********************************************************************
/**
 * \brief  Interrupt Control and State Register fields
 */
//*********************************************************************
struct icsr
{
    unsigned             : 12;
    /**
     * Indicates the exception number of the highest priority pending enabled exception
     *
     * 0 = no pending exceptions
     *
     * \note Subtract 16 from this value to obtain the CMSIS IRQ number that identifies
     * the corresponding bit in the Interrupt Clear-Enable, Clear-Pending, Set-Pending,
     * and Priority Register.
     */
    unsigned vectpending : 6;
    unsigned             : 7;
    unsigned pendstclr   : 1; ///< SysTick exception clear-pending bit
    unsigned pendstset   : 1; ///< SysTick exception set-pending bit
    unsigned pendsvclr   : 1; ///< PendSV clear-pending bit
    unsigned pendsvset   : 1; ///< PendSV set-pending bit
    unsigned             : 2;
    unsigned nmipendset  : 1; ///< NMI set-pending bit
};

//*********************************************************************
/**
 * \brief  Application Interrupt and Reset Control Register fields
 */
//*********************************************************************
struct aircr
{
    unsigned               : 1;
    /**
     * Reserved for debug use.  This bit reads as 0.
     * \warn When writing to the register you must write 0 to this bit,
     *  otherwise behavior is Unpredictable.
     */
    unsigned vectclractive : 1;
    unsigned sysresetreq   : 1; ///< System reset request
    unsigned               : 12;
    unsigned endianness    : 1;  ///< Data endianness implemented
    unsigned vectkey       : 16; ///< On writes, write 0x05FA, otherwise the write is ignored
};

//*********************************************************************
/**
 * \brief  System Control Register fields
 */
//*********************************************************************
struct scr
{
    unsigned             : 1;
    unsigned sleeponexit : 1; ///< Indicates sleep-on-exit when returning from Handler to Thread mode
    unsigned sleepdeep   : 1; ///< Controls if the processor uses sleep or deep sleep for low power
    unsigned             : 1;
    unsigned sevonpend   : 1; ///< Send event on pending bit
    unsigned             : 27;
};

//*********************************************************************
/**
 * \brief  Configuration and Control Register fields
 */
//*********************************************************************
struct ccr
{
    unsigned             : 3;
    unsigned unalign_trp : 1; ///< Always reads as one
    unsigned             : 5;
    unsigned stkalign    : 1; ///< Always reads as one
    unsigned             : 22;
};

//*********************************************************************
/**
 * \brief  Memory mapped struct for SCB registers
 */
//*********************************************************************
struct
{
    unsigned reserved1[832];
    struct cpuid cpuid;
    struct icsr icsr;
    unsigned vtor;
    struct aircr aircr;
    struct scr scr;
    struct ccr ccr;
    unsigned shpr[3]; // First element is not actually valid on M0+.
} volatile scb __attribute__((section(".bss.scb")));

//*********************************************************************
/**
 * \brief  Memory mapped struct for NVIC registers
 */
//*********************************************************************
struct
{
    unsigned reserved1[64];
    unsigned iser;
    unsigned reserved2[31];
    unsigned icer;
    unsigned reserved3[31];
    unsigned ispr;
    unsigned reserved4[31];
    unsigned icpr;
    unsigned reserved5[95];
    unsigned ipr[8];
} volatile nvic __attribute__((section(".bss.nvic")));

//*********************************************************************
/**
 * \brief Constant required for AIRCR writes to succeed
 */
//*********************************************************************
#define VECTKEY 0x05fa

//*********************************************************************
/**
 * \brief Determine the bit mask for the IRQ
 * \param x IRQ to determine bit mask of
 * \returns The bit mask for the IRQ (single bit set)
 */
//*********************************************************************
#define BIT(x)  (1 << ((unsigned)(x) % 32))

//*********************************************************************
/**
 * \brief Enable a device specific interrupts
 *
 * This function enables the specified device specific interrupt \p irq.
 * \warn \p irq cannot be negative.
 *
 * \param irq Interrupt number
 */
//*********************************************************************
void nvic_enable_irq(enum irq const irq)
{
    // assert((int)irq >= 0);

    nvic.iser = BIT(irq);
}

//*********************************************************************
/**
 * \brief Get a specific interrupt enable status
 *
 * This function returns the interrupt enable status for the specified
 * interrupt \p irq.
 * \warn \p irq cannot be negative.
 *
 * \param irq Interrupt number
 * \returns If the interrupts is enabled
 */
//*********************************************************************
bool nvic_get_enable_irq(enum irq const irq)
{
    // assert((int)irq >= 0);

    return nvic.iser & BIT(irq);
}

//*********************************************************************
/**
 * \brief Disable a device specific interrupt
 *
 * This function disables the specified device specific interrupt \p irq.
 * \warn \p irq cannot be negative.
 *
 * \param irq Interrupt number
 */
//*********************************************************************
void nvic_disable_irq(enum irq const irq)
{
    // assert((int)irq >= 0);

    nvic.icer = BIT(irq);
}

//*********************************************************************
/**
 * \brief Get the pending device specific interrupt
 *
 * This function returns the pending status of the specified device
 * specific interrupt \p irq.
 * \warn \p irq cannot be negative.
 *
 * \param irq Interrupt number
 * \returns If the IRQ is pending
 */
//*********************************************************************
bool nvic_get_pending_irq(enum irq const irq)
{
    // assert((int)irq >= 0);

    return nvic.ispr & BIT(irq);
}

//*********************************************************************
/**
 * \brief Set a device specific interrupt to pending
 *
 * This function sets the pending bit for the specified device specific
 * interrupt \p irq.
 * \warn \p irq cannot be negative.
 *
 * \param irq Interrupt number
 */
//*********************************************************************
void nvic_set_pending_irq(enum irq const irq)
{
    // assert((int)irq >= 0);

    nvic.ispr = BIT(irq);
}

//*********************************************************************
/**
 * \brief Clear a device specific interrupt from pending.
 *
 * This function removes the pending state of the specified device
 * specific interrupt \p irq.
 * \warn \p irq cannot be negative.
 *
 * \param irq Interrupt number
 */
//*********************************************************************
void nvic_clear_pending_irq(enum irq const irq)
{
    // assert((int)irq >= 0);

    nvic.icpr = BIT(irq);
}

//*********************************************************************
/**
 * \brief Set the priority for an interrupt.
 *
 * Sets the priority for the interrupt specified by \p irq.  \p irq can
 * specify any device specific interrupt or processor interrupt.  The
 * \p priority specifies the interrupt priority value, whereby lower
 * values indicate a higher priority.  The default priority is 0 for
 * every interrupt.  This is the highest possible priority.
 *
 * The priority cannot be set for every core interrupt.  HardFault and
 * NMI have a fixed (negative) priority that is higher than any configurable
 * exception or interrupt.
 *
 * \note STM32G031K8T6 only supports 4 levels (0-3) in hardware.
 *
 * \param irq Interrupt number
 * \param priority The priority level (0-3) for the interrupt
 */
//*********************************************************************
void nvic_set_priority(enum irq const irq, uint8_t const priority)
{
    unsigned index;
    unsigned volatile *register_address;
    if ((int)irq < 0)
    {
        index            = (unsigned)irq + 12;
        register_address = scb.shpr;
    }
    else
    {
        index            = (unsigned)irq;
        register_address = nvic.ipr;
    }
    // STM32G031K8T6 only supports 4 priority levels, which must be in 2 upper bits
    uint8_t const priority_field = (uint8_t)((priority > 3 ? 3 : priority) << 6);
    unsigned reg                 = register_address[index / 4];
    reg &= ~(0xffu << (8 * (index % 4)));
    reg |= (unsigned)priority_field << (8 * (index % 4));
    register_address[index / 4] = reg;
}

//*********************************************************************
/**
 * \brief Get the priority of an interrupt.
 *
 * This function reads the priority for the specified interrupt \p irq.
 * \p irq can specify any device specific interrupt or processor
 * exception.
 *
 * \note STM32G031K8T6 only supports 4 levels (0-3) in hardware.
 *
 * \param irq Interrupt number
 * \returns The priority level (0-3) for the interrupt
 */
//*********************************************************************
uint8_t nvic_get_priority(enum irq const irq)
{
    unsigned index;
    unsigned volatile *register_address;
    if ((int)irq < 0)
    {
        index            = (unsigned)irq + 12;
        register_address = scb.shpr;
    }
    else
    {
        index            = (unsigned)irq;
        register_address = nvic.ipr;
    }
    uint8_t const priority_field = (register_address[index / 4] >> (8 * (index % 4))) & 0xff;
    // STM32G031K8T6 only supports 4 priority levels, which are in 2 upper bits
    return priority_field >> 6;
}

//*********************************************************************
/**
 * \brief Read interrupt vector
 *
 * This function reads the address of an interrupt handler function.
 *
 * \param irq Interrupt number
 * \returns A pointer to the interrupt handler
 */
//*********************************************************************
void (*nvic_get_vector(enum irq const irq))(void)
{
    void (**base)(void) = (void (**)(void))scb.vtor;
    return base[irq + 16];
}

//*********************************************************************
/**
 * \brief Set interrupt vector
 *
 * This function sets the address of an interrupt handler function.
 *
 * \param irq Interrupt number
 * \param vector A pointer to the interrupt handler
 */
//*********************************************************************
void nvic_set_vector(enum irq const irq, void (*const vector)(void))
{
    void (**base)(void) = (void (**)(void))scb.vtor;
    base[irq + 16]      = vector;
}

//*********************************************************************
/**
 * \brief Reset the system
 *
 * This function requests a system reset by setting the SYSRESETREQ flag
 * in the AIRCR register.
 */
//*********************************************************************
void nvic_system_reset(void)
{
    struct aircr aircr = scb.aircr;
    aircr.vectkey      = VECTKEY;
    aircr.sysresetreq  = 1;
    scb.aircr          = aircr;
}

//*********************************************************************
/**
 * \brief Sets the deep sleep bit.
 *
 * The deep sleep bit is set in for stop0/1, standby and shutdown modes.
 */
//*********************************************************************
void nvic_set_deepsleep(void)
{
    scb.scr.sleepdeep = 1;
}

//*********************************************************************
/**
 * \brief Clears the deep sleep bit.
 *
 * The deep sleep bit is clear for sleep mode.
 */
//*********************************************************************
void nvic_clear_deepsleep(void)
{
    scb.scr.sleepdeep = 0;
}
