/**
 * @file hal_flash.c
 * @brief Implement the Flash support functions.
 */
#include "hal_flash.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

/**
 * @brief Number of loops to wait for flash write to complete
 */
#define WRITE_WAIT_COUNT 20

/**
 * @brief FLASH access control register
 */
struct acr
{
    unsigned latency  : 3; /**< Flash memory access latency */
    unsigned          : 5;
    unsigned prften   : 1; /**< CPU Prefetch enable */
    unsigned icen     : 1; /**< CPU Instruction cache enable */
    unsigned          : 1;
    unsigned icrst    : 1; /**< CPU Instruction cache reset */
    unsigned          : 4;
    unsigned empty    : 1; /**< Main Flash memory area empty */
    unsigned          : 1;
    unsigned dbg_swen : 1; /**< Debug access software enable */
    unsigned          : 13;
};

/**
 * @brief FLASH status register
 */
struct sr
{
    unsigned eop     : 1; /**< End of operation */
    unsigned operr   : 1; /**< Operation error */
    unsigned         : 1;
    unsigned progerr : 1; /**< Programming error */
    unsigned wrperr  : 1; /**< Write protection error */
    unsigned pgaerr  : 1; /**< Programming alignment error */
    unsigned sizerr  : 1; /**< Size error */
    unsigned pgserr  : 1; /**< Programming sequence error */
    unsigned misserr : 1; /**< Fast programming data miss error */
    unsigned fasterr : 1; /**< Fast programming error */
    unsigned         : 4;
    unsigned rderr   : 1; /**< PCROP read error */
    unsigned optverr : 1; /**< Option and Engineering bits loading validity error */
    unsigned bsy1    : 1; /**< Busy bank 1 */
    unsigned bsy2    : 1; /**< Busy bank 2 (not available on 64Kb devices) */
    unsigned cfgbsy  : 1; /**< Programming or erase configuration busy. */
    unsigned         : 13;
};

/**
 * @brief FLASH control register
 */
struct cr
{
    unsigned pg         : 1;  /**< Flash memory programming enable*/
    unsigned per        : 1;  /**< Page erase enable */
    unsigned mer1       : 1;  /**< Mass erase (Bank 1) */
    unsigned pnb        : 10; /**< Page number selection */
    unsigned bker       : 1;  /**< Bank selection for erase operation */
    unsigned            : 1;
    unsigned mer2       : 1; /**< Mass erase, Bank 2 */
    unsigned strt       : 1; /**< Start erase operation */
    unsigned optstrt    : 1; /**< Start of modification of option bytes */
    unsigned fstpg      : 1; /**< Fast programming enable */
    unsigned            : 5;
    unsigned eopie      : 1; /**< End-of-operation interrupt enable */
    unsigned errie      : 1; /**< Error interrupt enable */
    unsigned rderrie    : 1; /**< PCROP read error interrupt enable */
    unsigned obl_launch : 1; /**< Option byte load launch */
    unsigned sec_prot   : 1; /**< Securable memory area protection enable (Bank 1) */
    unsigned sec_prot2  : 1; /**< Securable memory area protection enable (Bank 2) */
    unsigned optlock    : 1; /**< Options Lock */
    unsigned lock       : 1; /**< FLASH_CR Lock */
};

/**
 * @brief FLASH ECC register
 */
struct eccr
{
    unsigned addr_ecc : 14; /**< ECC fail double-word address offset */
    unsigned          : 6;
    unsigned sysf_ecc : 1; /**< System Flash memory ECC fail */
    unsigned          : 3;
    unsigned eccie    : 1; /**< ECC correction interrupt enable */
    unsigned          : 5;
    unsigned eccc     : 1; /**< ECC correction */
    unsigned eccd     : 1; /**< ECC detection */
};

/**
 * @brief FLASH ECC2 register
 */
struct eccr2
{
    unsigned addr_ecc : 14; /**< ECC fail double-word address offset */
    unsigned          : 6;
    unsigned sysf_ecc : 1; /**< System Flash memory ECC fail */
    unsigned          : 3;
    unsigned eccie    : 1; /**< ECC correction interrupt enable */
    unsigned          : 5;
    unsigned eccc     : 1; /**< ECC correction */
    unsigned eccd     : 1; /**< ECC detection */
};

/**
 * @brief FLASH option register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
union optr
{
    struct
    {
        unsigned rdp              : 8; /**< Read protection level */
        unsigned bor_en           : 1; /**< Brown out reset enable */
        unsigned borr_lev         : 2; /**< BOR threshold at rising VDD supply */
        unsigned borf_lev         : 2; /**< BOR threshold at falling VDD supply */
        unsigned nrst_stop        : 1; /**< Reset behavior for stop mode */
        unsigned nrst_stdby       : 1; /**< Reset behavior for standby mode */
        unsigned nrst_shdw        : 1; /**< Reset behavior for shutdown mode */
        unsigned iwdg_sw          : 1; /**< Independent watchdog selection */
        unsigned iwdg_stop        : 1; /**< Independent watchdog counter freeze in Stop mode */
        unsigned iwdg_stdby       : 1; /**< Independent watchdog counter freeze in Standby mode */
        unsigned wwdg_sw          : 1; /**< Window watchdog selection */
        unsigned swap_bank        : 1; /**< Empty check boot configuration */
        unsigned dual_bank        : 1; /**< Dual-bank on 512Kbytes or 256Kbytes Flash memory devices */
        unsigned ram_parity_check : 1; /**< SRAM parity check control */
        unsigned                  : 1; /**< Option byte load launch */
        unsigned nboot_sel        : 1; /**< BOOT0 signal selection */
        unsigned nboot1           : 1; /**< Boot configuration */
        unsigned nboot0           : 1; /**< nBOOT0 option bit */
        unsigned nrst_mode        : 2; /**< Defines behavior of NRST pin */
        unsigned irhen            : 1; /**< Internal reset holder enable bit */
        unsigned                  : 2;
    };

    unsigned word;
};

/**
 * @brief FLASH PCROP area A start address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop1asr
{
    unsigned pcrop1a_strt : 9; /**< PCROP1A area start offset (Bank 1) */
    unsigned              : 23;
};

/**
 * @brief FLASH PCROP area A end address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop1aer
{
    unsigned pcrop1a_end : 9; /**< PCROP1A area end offset (Bank 1) */
    unsigned             : 22;
    unsigned pcrop_rdp   : 1; /**< PCROP area erase upon RDP level regression */
};

/**
 * @brief FLASH WRP area A address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct wrp1ar
{
    unsigned wrp1a_strt : 7; /**< WRP area A start offset (Bank 1) */
    unsigned            : 9;
    unsigned wrp1a_end  : 7; /**< WRP area A end offset (Bank 1) */
    unsigned            : 9;
};

/**
 * @brief FLASH WRP area B address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct wrp1br
{
    unsigned wrp1b_strt : 7; /**< WRP area B start offset (Bank 1) */
    unsigned            : 9;
    unsigned wrp1b_end  : 7; /**< WRP area B end offset (Bank 1) */
    unsigned            : 9;
};

/**
 * @brief FLASH PCROP area B start address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop1bsr
{
    unsigned pcrop1b_strt : 9; /**< PCROP1B area start offset (Bank 1) */
    unsigned              : 23;
};

/**
 * @brief FLASH PCROP area B end address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop1ber
{
    unsigned pcrop1b_end : 9; /**< PCROP1B area end offset (Bank 1) */
    unsigned             : 23;
};

/**
 * @brief FLASH PCROP2 area A start address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop2asr
{
    unsigned pcrop2a_strt : 9; /**< PCROP2A area start offset (Bank 2) */
    unsigned              : 23;
};

/**
 * @brief FLASH PCROP2 area A end address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop2aer
{
    unsigned pcrop2a_end : 9; /**< PCROP2A area end offset (Bank 2) */
    unsigned             : 23;
};

/**
 * @brief FLASH WRP2 area A address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct wrp2ar
{
    unsigned wrp2a_strt : 7; /**< WRP area A start offset (Bank 2) */
    unsigned            : 9;
    unsigned wrp2a_end  : 7; /**< WRP area A end offset (Bank 2) */
    unsigned            : 9;
};

/**
 * @brief FLASH WRP2 area B address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct wrp2br
{
    unsigned wrp2b_strt : 7; /**< WRP2 area B start offset (Bank 2) */
    unsigned            : 9;
    unsigned wrp2b_end  : 7; /**< WRP2 area B end offset (Bank 2) */
    unsigned            : 9;
};

/**
 * @brief FLASH PCROP2 area B start address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop2bsr
{
    unsigned pcrop2b_strt : 9; /**< PCROP2B area start offset (Bank 2) */
    unsigned              : 23;
};

/**
 * @brief FLASH PCROP2 area B end address register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct pcrop2ber
{
    unsigned pcrop2b_end : 9; /**< PCROP2B area end offset (Bank 2) */
    unsigned             : 23;
};

/**
 * @brief FLASH security register
 * @note The option bits are loaded with values from Flash memory at power-on reset release.
 */
struct secr
{
    unsigned sec_size  : 8; /**< Securable memory area size (Bank 1) */
    unsigned           : 8;
    unsigned boot_lock : 1; /**< used to force boot from user area */
    unsigned           : 3;
    unsigned sec_size2 : 8; /**< Securable memory area size, Bank 2 */
    unsigned           : 4;
};

/**
 * @brief The address of the FLASH port is defined in the linker file.
 * @todo Make sure all gaps have been accounted for.
 */
struct
{
    struct acr acr; /**< FLASH access control register */
    unsigned reserved1;
    unsigned keyr;              /**< FLASH key register */
    unsigned optkeyr;           /**< FLASH option key register */
    struct sr sr;               /**< FLASH status register */
    struct cr cr;               /**< FLASH control register */
    struct eccr eccr;           /**< FLASH ECC register */
    struct eccr2 eccr2;         /**< FLASH ECC register 2 */
    union optr optr;            /**< FLASH option register */
    struct pcrop1asr pcrop1asr; /**< FLASH PCROP area A start address register */
    struct pcrop1aer pcrop1aer; /**< FLASH PCROP area A end address register */
    struct wrp1ar wrp1ar;       /**< FLASH WRP area A address register */
    struct wrp1br wrp1br;       /**< FLASH WRP area A address register */
    struct pcrop1bsr pcrop1bsr; /**< FLASH PCROP area B start address register */
    struct pcrop1ber pcrop1ber; /**< FLASH PCROP area B end address register */
    unsigned reserved2[2];
    struct pcrop2asr pcrop2asr; /**< FLASH PCROP2 area A start address register */
    struct pcrop2aer pcrop2aer; /**< FLASH PCROP2 area A end address register */
    struct wrp2ar wrp2ar;       /**< FLASH WRP2 area A address register */
    struct wrp2br wrp2br;       /**< FLASH WRP2 area A address register */
    struct pcrop2bsr pcrop2bsr; /**< FLASH PCROP2 area B start address register */
    struct pcrop2ber pcrop2ber; /**< FLASH PCROP2area B end address register */
    unsigned reserved3[9];
    struct secr secr; /**< FLASH PCROP2area B end address register */
} volatile flash __attribute__((section(".bss.flash")));

/**
 * @brief Locks the flash
 */
static void flash_lock(void)
{
    flash.cr.lock = 1;
}

/**
 * @brief Unlocks the flash
 */
void flash_unlock(void)
{
    flash.keyr = 0x45670123;
    flash.keyr = 0xCDEF89AB;
}

/**
 * @brief Unlocks the flash option register
 */
static void flash_optreg_unlock(void)
{
    flash.optkeyr = 0x08192A3B;
    flash.optkeyr = 0x4C5D6E7F;
}

/**
 * @brief Waits for busy to clear.
 * @return true if busy is clear, false if timeout.
 */
static bool flash_wait_busy_clear(void)
{
    uint8_t i = 0;
    while (flash.sr.bsy1)
    {
        if (++i > WRITE_WAIT_COUNT)
        {
            return false;
        }
    }

    return true;
}

bool flash_write(unsigned *const buffer, unsigned *const flash_address, unsigned const size)
{
    // First check to see that the flash is not busy.
    if (flash.sr.bsy1)
    {
        return false;
    }

    flash_unlock();

    // Clear any previous errors by writing 1 to the bit
    struct sr sr   = flash.sr;
    sr.operr       = 1;
    sr.progerr     = 1;
    sr.wrperr      = 1;
    sr.pgaerr      = 1;
    sr.sizerr      = 1;
    sr.pgserr      = 1;
    sr.misserr     = 1;
    sr.fasterr     = 1;
    sr.rderr       = 1;
    flash.sr       = sr;

    unsigned *dest = flash_address;
    unsigned *src  = (unsigned *)buffer;

    // Set standard programming mode.
    flash.cr.pg    = 1;

    // Divide the size by 8 because two words are written at a time.
    for (unsigned i = 0; i < size / 8; i++)
    {
        *dest++   = *src++;
        *dest++   = *src++;

        // Wait for busy to clear after each write.  If too much time elapses, return
        // error status.
        uint8_t i = 0;
        while (flash.sr.bsy1)
        {
            if (++i > WRITE_WAIT_COUNT)
            {
                flash.cr.pg = 0;
                flash_lock();
                return false;
            }
        }
    }

    // Check for errors.
    bool return_val = true;
    sr              = flash.sr;
    if (sr.operr == 1 || sr.progerr == 1 || sr.wrperr == 1 || sr.pgaerr == 1 || sr.sizerr == 1 || sr.pgserr == 1 ||
        sr.misserr == 1 || sr.fasterr == 1 || sr.rderr == 1)
    {
        return_val = false;
    }

    flash.cr.pg = 0;
    flash_lock();

    return return_val;
}

bool flash_erase_page(pages const page)
{
    // First check to see that the flash is not busy.
    if (flash.sr.bsy1)
    {
        return false;
    }

    flash_unlock();

    // Clear any previous errors by writing 1 to the bit
    struct sr sr    = flash.sr;
    sr.operr        = 1;
    sr.progerr      = 1;
    sr.wrperr       = 1;
    sr.pgaerr       = 1;
    sr.sizerr       = 1;
    sr.pgserr       = 1;
    sr.misserr      = 1;
    sr.fasterr      = 1;
    sr.rderr        = 1;
    flash.sr        = sr;

    struct cr cr    = flash.cr;
    cr.per          = 1;
    cr.pnb          = page;
    flash.cr        = cr;

    // Start the erase.
    flash.cr.strt   = 1;

    uint8_t i       = 0;
    bool return_val = true;
    // Wait for busy to clear.
    while (flash.sr.bsy1)
    {
        if (++i > WRITE_WAIT_COUNT)
        {
            return_val = false;
            break;
        }
    }

    // Clear page erase bit.
    flash.cr.per = 0;

    // Check for errors.
    sr           = flash.sr;
    if (sr.operr == 1 || sr.progerr == 1 || sr.wrperr == 1 || sr.pgaerr == 1 || sr.sizerr == 1 || sr.pgserr == 1 ||
        sr.misserr == 1 || sr.fasterr == 1 || sr.rderr == 1)
    {
        return_val = false;
    }

    flash_lock();

    return return_val;
}

/**
 * @brief Sets the read protection level.
 * @param level the read protection level to set.
 * @note level = 0, read protection level 0
 *          level = 1, read protection level 1
 *          all other values, read protection level 2
 */
bool flash_set_rdp_level(const uint8_t level)
{
    flash_unlock();
    flash_optreg_unlock();

    union optr opt = flash.optr;

    if (0 == level)
    {
        opt.rdp = 0xAA;
    }
    else if (1 == level)
    {
        // For level 1, RDB byte can be anything except 0xAA or 0xCC
        opt.rdp = 0xBB;
    }
    else
    {
        opt.rdp = 0xCC;
    }

    flash.optr = opt;

    // Make sure flash is not busy.
    if (!flash_wait_busy_clear())
    {
        // Timeout waiting for busy to clear.
        return false;
    }

    // Set the Options Start bit.
    flash.cr.optstrt = 1;

    // Wait for busy bit to clear.
    if (!flash_wait_busy_clear())
    {
        // Timeout waiting for busy to clear.
        return false;
    }

    return true;
}

unsigned flash_get_optionbyte(void)
{
    return flash.optr.word;
}
