//******************************************************************
/*! \file hal_options.c
 *
 * \brief Kidde: Mongoose-fw - Configure flash options bytes
 *
 * \author Stan Burnette
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "hal_flash.h"
#include <stdbool.h>
#include <stdint.h>

#define OPTION_NO_READ_PROTECTION 0xAAu       ///< Read protection level 0
#define OPTION_READ_PROTECTION    0xBBu       ///< Read protection level 1
#define OPTION_BOR_ENABLE         0x100u      ///< BOR Enabled
#define OPTION_BOR_RISING_2_9     0x600u      ///< Rising level 2.9 volts
#define OPTION_BOR_RISING_2_6     0x400u      ///< Rising level 2.6 volts
#define OPTION_BOR_RISING_2_3     0x100u      ///< Rising level 2.3 volts
#define OPTION_BOR_RISING_2_1     0x00u       ///< Rising level 2.1 volts
#define OPTION_BOR_FALLING_2_8    0x1800u     ///< Falling level 2.8 volts
#define OPTION_BOR_FALLING_2_5    0x1000u     ///< Falling level 2.5 volts
#define OPTION_BOR_FALLING_2_2    0x400u      ///< Falling level 2.2 volts
#define OPTION_BOR_FALLING_2_0    0x00u       ///< Falling level 2.0 volts
#define OPTION_NO_RESET_STOP      0x2000u     ///< No reset on entering Stop mode
#define OPTION_NO_RESET_STDBY     0x4000u     ///< No reset on entering Standby mode
#define OPTION_NO_RESET_SHTDWN    0x8000u     ///< No reset on entering Shutdown mode
#define OPTION_SOFT_IWDG          0x10000u    ///< Software independent WDT
#define OPTION_IWDG_STOP          0x20000u    ///< IWDG runs in stop mode
#define OPTION_IWDG_STDBY         0x40000u    ///< IWDG runs in standby mode
#define OPTION_SOFT_IWWDG         0x80000u    ///< Software window WDT enabled
#define OPTION_RAM_PARITY_DSBL    0x400000u   ///< RAM Parity check disabled
#define OPTION_NBOOT_SEL          0x01000000u ///< nBOOT0 defined by BOOT0 and BOOT1 bits
#define OPTION_BOOT1              0x02000000u ///< BOOT1 bit
#define OPTION_BOOT0              0x04000000u ///< BOOT0 bit
#define OPTION_NRST_BIDER         0x18000000u ///< NRST pin configured in reset input/output mode (legacy mode)
#define IRHEN_INIT                0x20000000u ///< Internal reset drives NRST pin low
#define RESET_INIT                0xc0b00000u ///< Bits which should not change from reset state

#define PCROP1A_END_INIT          0x80000000u ///< Readout protection A ends at 0x0800 0000, RDP bit set.
#define PCROP1A_STRT_INIT         0x0000007Fu ///< Readout protection A starts at 0x0800 ffff
#define WRP1A_STRT_INIT           0x0000001Fu ///< Write protection area A starts at 0x0800 0000 and ends at 0x0800 E600
#define WRP1B_STRT_INIT           0x0000001Fu ///< Write protection area B disabled
#define PCROP1B_STRT_INIT         0x0000007Fu ///< Readout protection B starts at 0x0800 ffff
#define PCROP1B_END_INIT          0x00000000u ///< Readout protection B ends at 0x0800 0000
#define SEC_INIT                  0x00000000u ///< No security section,

#ifdef DEBUG
#define OPTION_INIT                                                                                                    \
    (OPTION_NO_READ_PROTECTION | OPTION_BOR_ENABLE | OPTION_BOR_RISING_2_1 | OPTION_BOR_FALLING_2_0 |                  \
     OPTION_NO_RESET_STOP | OPTION_NO_RESET_STDBY | OPTION_NO_RESET_SHTDWN | OPTION_SOFT_IWDG | OPTION_IWDG_STOP |     \
     OPTION_IWDG_STDBY | OPTION_SOFT_IWWDG | OPTION_RAM_PARITY_DSBL | OPTION_NBOOT_SEL | OPTION_BOOT1 | OPTION_BOOT0 | \
     OPTION_NRST_BIDER | IRHEN_INIT | RESET_INIT)
#else
#define OPTION_INIT                                                                                                    \
    (OPTION_NO_READ_PROTECTION | OPTION_BOR_ENABLE | OPTION_BOR_RISING_2_1 | OPTION_BOR_FALLING_2_0 |                  \
     OPTION_NO_RESET_STOP | OPTION_NO_RESET_STDBY | OPTION_NO_RESET_SHTDWN | OPTION_SOFT_IWDG | OPTION_IWDG_STOP |     \
     OPTION_IWDG_STDBY | OPTION_SOFT_IWWDG | OPTION_RAM_PARITY_DSBL | OPTION_NBOOT_SEL | OPTION_BOOT1 | OPTION_BOOT0 | \
     OPTION_NRST_BIDER | IRHEN_INIT | RESET_INIT)
#endif

/**
 * @brief The option bytes mapped into flash space
 */
struct
{
    unsigned option;                ///< Option and read protection option bytes
    unsigned option_compl;          ///< Complement of User and read protection option bytes
    unsigned flash_pcrop1asr;       ///< PCROP1A start address
    unsigned flash_pcrop1asr_compl; ///< PCROP1A start address complement
    unsigned flash_pcrop1aer;       ///< PCROP1A end address
    unsigned flash_pcrop1aer_compl; ///< PCROP1A end address complement
    unsigned flash_wrp1ar;          ///< WRP1AR area A
    unsigned flash_wrp1ar_compl;    ///< WRP1AR area A complement
    unsigned flash_wrp1br;          ///< WRP1BR area B
    unsigned flash_wrp1br_compl;    ///< WRP1BR area B complement
    unsigned flash_pcrop1bs;        ///< PCROP1B start address
    unsigned flash_pcrop1bs_compl;  ///< PCROP1B start address complement
    unsigned flash_pcrop1ber;       ///< PCROP1B end address
    unsigned flash_pcrop1ber_compl; ///< PCROP1B end address complement
} const option_bytes __attribute__((section(".option_bytes"))) = {
    .option                = OPTION_INIT,
    .option_compl          = ~OPTION_INIT,
    .flash_pcrop1asr       = PCROP1A_STRT_INIT,
    .flash_pcrop1asr_compl = ~PCROP1A_STRT_INIT,
    .flash_pcrop1aer       = PCROP1A_END_INIT,
    .flash_pcrop1aer_compl = ~PCROP1A_END_INIT,
    .flash_wrp1ar          = WRP1A_STRT_INIT,
    .flash_wrp1ar_compl    = ~WRP1A_STRT_INIT,
    .flash_wrp1br          = WRP1B_STRT_INIT,
    .flash_wrp1br_compl    = ~WRP1B_STRT_INIT,
    .flash_pcrop1bs        = PCROP1B_STRT_INIT,
    .flash_pcrop1bs_compl  = ~PCROP1B_STRT_INIT,
    .flash_pcrop1ber       = PCROP1B_END_INIT,
    .flash_pcrop1ber_compl = ~PCROP1B_END_INIT,
};

/**
 * @brief The security bytes mapped into flash space
 */
struct
{
    unsigned flash_secr;       ///< Security option bytes
    unsigned flash_secr_compl; ///< Security option bytes complement
} const security_bytes __attribute__((section(".security_bytes"))) = {
    .flash_secr       = SEC_INIT,
    .flash_secr_compl = ~SEC_INIT,
};
