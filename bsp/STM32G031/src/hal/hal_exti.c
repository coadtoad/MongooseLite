/**
 * @file hal_exti.c
 * @brief Implement the EXTI HAL functions
 */
#include "hal_exti.h"

/**
 * @brief Mask to make sure reserved wake sources are not written to. A
 *         set bit is reserved.
 */
#define RESERVED_WAKE_SOURCES ((uint32_t)0x09560000)

/**
 * @brief The address of the exti port is defined in the linker file.
 */
struct
{
    unsigned rtsr1;         /**< Rising Trigger Selection Register */
    unsigned ftsr1;         /**< Falling Trigger Selection Register */
    unsigned swier1;        /**< Software Interrupt Event Register 1 */
    unsigned rpr1;          /**< Rising Edge Pending Register 1 */
    unsigned fpr1;          /**< Falling Edge Pending Register 1 */
    unsigned reserved1[19]; /**< No registers in this range */
    unsigned exticrx[4];    /**< Falling Edge Pending Register 1 */
    unsigned reserved2[4];  /**< No registers in this range */
    unsigned imr1;          /**< CPU Wakeup With Interrupt Mask Register */
    unsigned emr1;          /**< CPU Wakeup With Event Mask Register */
} volatile exti __attribute__((section(".bss.exti")));

void exti_enable_rising_edge_interrupt(exti_edge_trigger const line_num)
{
    exti.rtsr1 |= 1 << line_num;
}

void exti_disable_rising_edge_interrupt(exti_edge_trigger const line_num)
{
    exti.rtsr1 &= ~(1 << line_num);
}

void exti_enable_falling_edge_interrupt(exti_edge_trigger const line_num)
{
    exti.ftsr1 |= 1 << line_num;
}

void exti_disable_falling_edge_interrupt(exti_edge_trigger const line_num)
{
    exti.ftsr1 &= ~(1 << line_num);
}

void exti_force_rising_edge_interrupt(exti_edge_trigger const line_num)
{
    exti.swier1 |= 1 << line_num;
}

bool exti_rising_edge_occured(exti_edge_trigger const line_num)
{
    return exti.rpr1 & (1 << line_num);
}

bool exti_falling_edge_occured(exti_edge_trigger const line_num)
{
    return exti.fpr1 & (1 << line_num);
}

void exti_clear_rising_edge(exti_edge_trigger const line_num)
{
    exti.rpr1 |= 1 << line_num;
}

void exti_clear_falling_edge(exti_edge_trigger const line_num)
{
    exti.fpr1 |= 1 << line_num;
}

void exti_config_port(exti_mux_num const exti_mux, exti_port_cfg const port)
{
    // Mask off the appropriate bits and or-in the port.
    exti.exticrx[exti_mux / 4] &= ~((uint32_t)0xff << ((exti_mux % 4) * 8));
    exti.exticrx[exti_mux / 4] |= (uint32_t)port << ((exti_mux % 4) * 8);
}

void exti_config_wakeonint_mask(exti_wakeup const wake_src, exti_bit_cfg const mode)
{
    // Check the wake source passed with allowable sources.
    if (~RESERVED_WAKE_SOURCES & (uint32_t)0x01 << wake_src)
    {
        exti.imr1 = mode ? exti.imr1 | ((uint32_t)0x01 << wake_src) : exti.imr1 & ~((uint32_t)0x01 << wake_src);
    }
}

void exti_config_wakeonevt_mask(exti_wakeup const wake_src, exti_bit_cfg const mode)
{
    // Check the wake source passed with allowable sources.
    if (~RESERVED_WAKE_SOURCES & (uint32_t)0x01 << wake_src)
    {
        exti.emr1 = mode ? exti.emr1 | ((uint32_t)0x01 << wake_src) : exti.emr1 & ~((uint32_t)0x01 << wake_src);
    }
}
