/**
 * @file hal_adc.c
 * @brief Implement ADC HAL functions.
 */

#include "hal_adc.h"
#include "hal_rcc.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

/**
 * @brief A memory mapped type for ADC interrupt and status register
 */
struct isr
{
    unsigned adrdy : 1; /**< Used to identify when ADC is ready for conversion */
    unsigned eosmp : 1; /**< Used to identify end of sampling phase */
    unsigned eoc   : 1; /**< Used to identify end of each conversion of a channel */
    unsigned eos   : 1; /**< Used to identify end of conversion of sequence of channels set by CHSEL */
    unsigned ovr   : 1; /**< Used to identify when the overrun happens */
    unsigned       : 2; /**< Unused bits*/
    unsigned
        awd1 : 1; /**< Used to identify when the converted voltage crosses the threshold set by TR1 and HR1 registers */
    unsigned
        awd2 : 1; /**< Used to identify when the converted voltage crosses the threshold set by TR1 and HR1 registers */
    unsigned awd3  : 1;  /**< Used to identify when the converted voltage crosses the threshold set by AWD2TR and AWD2TR
                            registers */
    unsigned       : 1;  /**< Unused bits */
    unsigned eocal : 1;  /**< Used to identify end of the ADC calibration */
    unsigned       : 1;  /**< Unused bits */
    unsigned ccrdy : 1;  /**< Used to identify the completion of channel configuration */
    unsigned       : 18; /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC interrupt enable register
 */
struct ier
{
    unsigned adrdyie : 1;  /**< Used to enable/disable ADC ready interrupt */
    unsigned eosmpie : 1;  /**< Used to enable/disable end of sampling flag interrupt */
    unsigned eocie   : 1;  /**< Used to enable/disable end of conversion interrupt */
    unsigned eosie   : 1;  /**< Used to enable/disable end of conversion sequence interrupt */
    unsigned ovrie   : 1;  /**< Used to enable/disable overrun interrupt */
    unsigned         : 2;  /**< Unused bits */
    unsigned awd1ie  : 1;  /**< Used to enable/disable analog watchdog 1 interrupt */
    unsigned awd2ie  : 1;  /**< Used to enable/disable analog watchdog 2 interrupt */
    unsigned awd3ie  : 1;  /**< Used to enable/disable analog watchdog 3 interrupt */
    unsigned         : 1;  /**< Unused bits */
    unsigned eocalie : 1;  /**< Used to enable/disable end of calibration interrupt */
    unsigned         : 1;  /**< Unused bits */
    unsigned ccrdyie : 1;  /**< Used to enable/disable channel configuration ready interrupt */
    unsigned         : 18; /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC control register
 */
struct cr
{
    unsigned aden     : 1;  /**< Used to enable the ADC */
    unsigned addis    : 1;  /**< Used to disable the ADC */
    unsigned adstart  : 1;  /**< Used to start the ADC conversion */
    unsigned          : 1;  /**< Unused bits */
    unsigned adstp    : 1;  /**< Used to stop the ongoing ADC conversion */
    unsigned          : 23; /**< Unused bits */
    unsigned advregen : 1;  /**< Used to enable/disable internal voltage regulator */
    unsigned          : 2;  /**< Unused bits */
    unsigned adcal    : 1;  /**< Used to start the ADC calibration */
};

/**
 * @brief A memory mapped type for ADC configuration register 1
 */
struct cfgr1
{
    unsigned dmaen     : 1; /**< Used to enable/disable the generation of DMA requests */
    unsigned dmacfg    : 1; /**< Used to switch between DMA modes of operation */
    unsigned scandir   : 1; /**< Used to select the direction in which the channels are scanned */
    unsigned res       : 2; /**< Used to select the resolution of the conversion */
    unsigned align     : 1; /**< Used to select the alignment in which the ADC result is stored in the register */
    unsigned extsel    : 3; /**< Used to select the external trigger for the start of ADC conversion */
    unsigned           : 1; /**< Unused bits */
    unsigned exten     : 2; /**< Used to enable/disable external trigger and the trigger polarity */
    unsigned ovrmod    : 1; /**< Used to select overrun management mode */
    unsigned cont      : 1; /**< Used to select between single/continuous mode */
    unsigned wait      : 1; /**< Used to enable/disable wait conversion mode */
    unsigned autoff    : 1; /**< Used to enable/disable auto-off mode */
    unsigned discen    : 1; /**< Used to enable/disable discontinuous mode */
    unsigned           : 4; /**< Unused bits */
    unsigned chselrmod : 1; /**< Used to select the mode of CHSELR register */
    unsigned awd1sgl   : 1; /**< Used to enable watchdog on single channel or all channels */
    unsigned awd1en    : 1; /**< Used to enable/disable analog watchdog 1 */
    unsigned           : 2; /**< Unused bits */
    unsigned awd1ch    : 5; /**< Used to select the channel to be guarded by analog watchdog 1 */
    unsigned           : 1; /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC configuration register 2
 */
struct cfgr2
{
    unsigned ovse   : 1;  /**< Used to enable/disable over sampler */
    unsigned        : 1;  /**< Unused bits */
    unsigned ovsr   : 3;  /**< Used to set the oversampling ratio */
    unsigned ovss   : 4;  /**< Used to set the oversampling shift */
    unsigned tovs   : 1;  /**< Used to set the trigger selection for oversampling */
    unsigned        : 19; /**< Unused bits */
    unsigned lftrig : 1;  /**< Used to enable/disable low frequency trigger mode */
    unsigned ckmode : 2;  /**< Used to select the clock mode for ADC */
};

/**
 * @brief A memory mapped type for ADC sampling register
 */
struct smpr
{
    unsigned smp1   : 3;  /**< Used to set the sampling time1 */
    unsigned        : 1;  /**< Unused bits */
    unsigned smp2   : 3;  /**< Used to set the sampling time2 */
    unsigned        : 1;  /**< Unused bits */
    unsigned smpsel : 19; /**< Used to select between sampling time1/sampling time2 for all channels */
    unsigned        : 5;  /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC watchdog 1 threshold register
 */
struct awd1tr
{
    unsigned lt1 : 12; /**< Used to set analog watchdog 1 lower threshold */
    unsigned     : 4;  /**< Unused bits */
    unsigned ht1 : 12; /**< Used to set analog watchdog 1 higher threshold */
    unsigned     : 4;  /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC watchdog 2 threshold register
 */
struct awd2tr
{
    unsigned lt2 : 12; /**< Used to set analog watchdog 2 lower threshold */
    unsigned     : 4;  /**< Unused bits */
    unsigned ht2 : 12; /**< Used to set analog watchdog 2 higher threshold */
    unsigned     : 4;  /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC watchdog 3 threshold register
 */
struct awd3tr
{
    unsigned lt3 : 12; /**< Used to set analog watchdog 3 lower threshold */
    unsigned     : 4;  /**< Unused bits */
    unsigned ht3 : 12; /**< Used to set analog watchdog 3 higher threshold */
    unsigned     : 4;  /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC data register
 */
struct dr
{
    unsigned data : 16; /**< Used to read the converted analog voltage count */
    unsigned      : 16; /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC watchdog 2 configuration register
 */
struct awd2cr
{
    unsigned awd2ch : 19; /**< Used to enable and select the channels to be guarded by analog watchdog 2 */
    unsigned        : 13; /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC watchdog 3 configuration register
 */
struct awd3cr
{
    unsigned awd3ch : 19; /**< Used to enable and select the channels to be guarded by analog watchdog 3 */
    unsigned        : 13; /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC calibration factor register
 */
struct calfact
{
    unsigned calfact : 7;  /**< Used to store the calibration factor after the calibration is done */
    unsigned         : 25; /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC common configuration register
 */
struct ccr
{
    unsigned        : 18; /**< Unused bits */
    unsigned presc  : 4;  /**< Used to set the prescalar for ADC CLOCK */
    unsigned vrefen : 1;  /**< Used to enable/disable vrefint */
    unsigned tsen   : 1;  /**< Used to enable/disable temperature sensor */
    unsigned vbaten : 1;  /**< Used to enable/disable VBAT channel */
    unsigned        : 7;  /**< Unused bits */
};

/**
 * @brief A memory mapped type for ADC registers
 */
struct
{
    struct isr isr;          /**< Used to read/configure ADC interrupt and status register */
    struct ier ier;          /**< Used to configure ADC interrupt enable register */
    struct cr cr;            /**< Used to configure ADC control register */
    struct cfgr1 cfgr1;      /**< Used to configure ADC configuration register 1 */
    struct cfgr2 cfgr2;      /**< Used to configure ADC configuration register 2 */
    struct smpr smpr;        /**< Used to configure ADC sampling time register */
    unsigned reserved1[2];   /**< Unused */
    struct awd1tr awd1tr;    /**< Used to configure ADC watchdog 1 threshold register */
    struct awd2tr awd2tr;    /**< Used to configure ADC watchdog 2 threshold  register */
    unsigned chselr;         /**< Used to configure ADC channel selection register */
    struct awd3tr awd3tr;    /**< Used to configure ADC watchdog 3 threshold  register */
    unsigned reserved2[4];   /**< Unused */
    struct dr dr;            /**< Used to read ADC data register */
    unsigned reserved3[23];  /**< Unused */
    struct awd2cr awd2cr;    /**< Used to configure ADC watchdog 2 configuration register */
    struct awd3cr awd3cr;    /**< Used to configure ADC watchdog 3 configuration register */
    unsigned reserved4[3];   /**< Unused */
    struct calfact calfact;  /**< Used to store/set calibration factor */
    unsigned reserved5[148]; /**< Unused */
    struct ccr ccr;          /**< Used to configure ADC common configuration register */
} volatile adc __attribute__((section(".bss.adc")));

/**
 * @def ADC_VREG_DELAY
 * @brief Used to keep a delay after VREG bit set by software.
 */
#define ADC_VREG_DELAY 160

/**
 * @brief Enable the internal voltage regulator
 */
static void adc_vreg_enable(void)
{
    // Enable the internal voltage regulator
    adc.cr.advregen = 1;

    // Wait for stabilizing ADC after VREG bit is set by software
    // As per the data sheet,  it takes 20microsec to stabilize.
    /** @todo The delay value will be verified in the future */
    for (uint8_t volatile i = ADC_VREG_DELAY; i; --i)
        ;
}

/**
 * @brief Disable the internal voltage regulator
 */
static void adc_vreg_disable(void)
{
    // Disable the internal voltage regulator
    adc.cr.advregen = 0;

    // Wait for stabilizing ADC after VREG bit is set by software
    // As per the data sheet,  it takes 20microsec to stabilize.
    /** @todo The delay value will be verified in the future */
    for (uint8_t volatile i = ADC_VREG_DELAY; i; --i)
        ;
}

void adc_enable(resolution_type resolution)
{
    // Enable voltage regulator
    adc_vreg_enable();

    // Set the resolution of the data conversion
    adc.cfgr1.res = resolution;

    // Enable the ADC
    adc.cr.aden   = 1;

    // Wait for ADRDY to be high
    while (!adc.isr.adrdy)
        ;
}

void adc_disable(void)
{
    adc.cr.addis = 1;

    // Ensure if the ADC is effectively disabled
    while (adc.cr.aden)
        ;

    // Disable the internal voltage regulator
    adc.cr.advregen = 0;
}

void adc_calibrate(void)
{
    // Start the ADC calibration
    adc.cr.adcal = 1;

    // Wait for the end of calibration
    // After calibration is done, calibration factor will be stored in CALFACT[6:0] and DATA[6:0]
    // of DR register
    while (adc.cr.adcal)
        ;
}

void adc_init(void)
{
    // Enable the ADC clock
    rcc_enable_adc_clock(adc_hsi16, true);

    // Enable the internal voltage regulator
    adc_vreg_enable();

    // Calibrate ADC
    adc_calibrate();

    // Configure CFGR2 register - default
    // Set the clock mode for ADC - Asynchronous clock

    // Configure the CCR register - default
    // Set ADC prescalar value to be ADCCLK/1
    // Disable VBAT,TEMPSENSOR,VREF

    // Configure the SMPR register - default
    // Set the sampling 1&2 to be 1.5 ADC cycles by default
    // Set the sampling time 1 to all the ADC channels

    // Disable the VREG
    adc_vreg_disable();
}

/**
 * @brief Configure the channel for ADC conversion
 *
 * @param[in] channel The channel to convert the voltage
 */
void adc_set_channel(uint8_t const channel)
{
    // Set the channel
    adc.chselr = 1 << channel;

    // Wait for CCRDY to be high
    while (!adc.isr.ccrdy)
        ;
}

uint16_t adc_sample(void)
{
    // Clear the flags by software
    struct isr isr = {0};
    isr.eoc        = 1; // Writing 1 to this bitfield by software will clear the register bit.
    isr.eos        = 1; // Writing 1 to this bitfield by software will clear the register bit.
    isr.ovr        = 1; // Writing 1 to this bitfield by software will clear the register bit.
    adc.isr        = isr;

    // Start the ADC
    adc.cr.adstart = 1;

    // Wait for the end of conversion flag to be high
    while (!adc.isr.eoc)
        ;

    return adc.dr.data;
}
