//*********************************************************************
/*! \file hal_tim14.c
 *
 * \brief Kidde: Mongoose-fw - Implement the TIM14 HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************
#include "hal_tim14.h"
#include "hal_rcc.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

//*********************************************************************
/**
 * \brief A memory mapped type for the CR1, TIM14 control register
 */
//*********************************************************************
struct cr1
{
    unsigned cen      : 1; ///< Counter enable
    unsigned udis     : 1; ///< Update disable
    unsigned urs      : 1; ///< Update request source
    unsigned opm      : 1; ///< One-pulse mode
    unsigned          : 3;
    unsigned arpe     : 1; ///< Auto-reload preload enable
    unsigned ckd      : 2; ///< Clock division
    unsigned          : 1;
    unsigned uifremap : 1; ///< UIF status bit remapping
    unsigned          : 20;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the DIER, TIM14 interrupt enable register
 */
//*********************************************************************
struct dier
{
    unsigned uie   : 1; ///< Update interrupt enable
    unsigned cc1ie : 1; ///< Capture/Compare 1 interrupt enable
    unsigned       : 30;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the SR, TIM14 status register
 */
//*********************************************************************
struct sr
{
    unsigned uif   : 1; ///< Update interrupt flag
    unsigned cc1if : 1; ///< Capture/compare 1 interrupt flag
    unsigned       : 7;
    unsigned cc1of : 1; ///< Capture/Compare 1 overcapture flag
    unsigned       : 22;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the EGR, TIM14 event generation register
 */
//*********************************************************************
struct egr
{
    unsigned ug   : 1; ///< Update generation
    unsigned cc1g : 1; ///< Capture/compare 1 generation
    unsigned      : 30;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCMR1, TIM14 capture/compare mode register
 */
//*********************************************************************
union ccmr1
{
    struct
    {
        unsigned cc1s   : 2; ///< Capture/Compare 1 selection
        unsigned ic1psc : 2; ///< Input capture 1 prescaler
        unsigned ic1f   : 4; ///< Input compare 1 filter
        unsigned        : 24;
    } input;

    struct
    {
        unsigned cc1s     : 2; ///< Capture/Compare 1 selection
        unsigned oc1fe    : 1; ///< Output capture 1 fast enable
        unsigned oc1pe    : 1; ///< Output compare 1 preload enable
        unsigned oc1m     : 3; ///< Output compare 1 mode OC1M[2:0]
        unsigned          : 9;
        unsigned oc1m_msb : 1; ///< Output compare 1 mode OC1M[3]
        unsigned          : 15;
    } output;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCER, TIM14 capture/compare enable register
 */
//*********************************************************************
struct ccer
{
    unsigned cc1e  : 1; ///< Capture/Compare 1 output enable
    unsigned cc1p  : 1; ///< Capture/Compare 1 output polarity
    unsigned       : 1;
    unsigned cc1np : 1; ///< Capture/Compare 1 complementary output polarity
    unsigned       : 28;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CNT, TIM14 counter register
 */
//*********************************************************************
struct cnt
{
    unsigned cnt    : 16; ///< Counter value
    unsigned        : 15;
    unsigned uifcpy : 1; ///< UIF copy
};

//*********************************************************************
/**
 * \brief A memory mapped type for the PSC, TIM14 prescaler register
 */
//*********************************************************************
struct psc
{
    unsigned psc : 16; ///< Prescaler value
    unsigned     : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the ARR, TIM14 auto-reload register
 */
//*********************************************************************
struct arr
{
    unsigned arr : 16; ///< Auto-reload value
    unsigned     : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCR1, TIM14 capture/compare register
 */
//*********************************************************************
struct ccr1
{
    unsigned ccr1 : 16; ///< Capture/Compare 1 value
    unsigned      : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the TISEL, TIM14 timer input selection register
 */
//*********************************************************************
struct tisel
{
    unsigned ti1sel : 4; ///< Timer input clock select
    unsigned        : 28;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the TIM14 control registers
 */
//*********************************************************************
struct
{
    struct cr1 cr1;
    unsigned unused1[2];
    struct dier dier;
    struct sr sr;
    struct egr egr;
    union ccmr1 ccmr1;
    unsigned unused2[1];
    struct ccer ccer;
    struct cnt cnt;
    struct psc psc;
    struct arr arr;
    unsigned unused3[1];
    struct ccr1 ccr1;
    unsigned unused4[12];
    struct tisel tisel;
} volatile tim14 __attribute__((section(".bss.tim14")));

void tim14_init(void)
{
    // Initialize TIM14
    rcc_enable_tim14_clock(true);
    // Set the prescaler. New prescale will take affect after the update event is generated
    tim14.psc.psc = 15u;
    // Stop counting after the count is reached. CEN will be cleared when complete
    tim14.cr1.opm = 1;
}

void tim14_start_count(uint16_t const count)
{
    // Set the value to count up to
    tim14.arr.arr = count;
    // Enable update generation / generate update event
    tim14.egr.ug  = 1;
    // Clear update event flag
    tim14.sr.uif  = 0;
    // Start counting
    tim14.cr1.cen = 1;
}

bool tim14_check_timer_expiration(void)
{
    return tim14.sr.uif;
}
