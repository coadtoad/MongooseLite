/**
 * @file hal_iwdg.c
 * @brief Implement the Watch Dog Timer HAL functions
 */
#include "hal_iwdg.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

/**
 * @brief IWDG prescale register.
 */
struct pr
{
    unsigned pr : 3; /**< Prescaler divider */
    unsigned    : 29;
};

/**
 * @brief IWDG status register.
 */
struct sr
{
    unsigned pvu : 1; /**< Watchdog prescaler value update */
    unsigned rvu : 1; /**< Watchdog counter reload value update */
    unsigned wvu : 1; /**< Watchdog counter window value update */
    unsigned     : 29;
};

/**
 * @brief IWDG reload register.
 */
struct rlr
{
    unsigned rl : 12; /**< Watchdog counter reload value */
    unsigned    : 20;
};

/**
 * @brief IWDG window register.
 */
struct winr
{
    unsigned win : 12; /**< Watchdog counter window value */
    unsigned     : 20;
};

/**
 * @brief The address of the IWDG port is defined in the linker file.
 */
struct
{
    unsigned kr;      /**< IWDG key register */
    struct pr pr;     /**< IWDG prescale register */
    struct rlr rlr;   /**< IWDG reload register */
    struct sr sr;     /**< IWDG status register */
    struct winr winr; /**< IWDG window register */
} volatile iwdg __attribute__((section(".bss.iwdg")));

void iwdg_reset_timer(void)
{
    iwdg.kr = 0xAAAAu;
}

void iwdg_start_timer(void)
{
    iwdg.kr = 0xCCCCu;
}

void iwdg_set_reload(uint16_t const val)
{
    // Make sure update bit is clear before writing
    while (iwdg.sr.rvu)
        ;

    // Unlock reload register by writing to key register.
    iwdg.kr        = 0x5555u;

    // Set reload register
    struct rlr rlr = {0};
    rlr.rl         = val;
    iwdg.rlr       = rlr;
}

void iwdg_set_prescaler(iwdg_prescale const val)
{
    // Make sure update bit is clear before writing
    while (iwdg.sr.pvu)
        ;

    // Unlock prescale register by writing to key register.
    iwdg.kr      = 0x5555u;

    // Set reload register
    struct pr pr = {0};
    pr.pr        = val;
    iwdg.pr      = pr;
}

void iwdg_set_window(uint16_t const val)
{
    // Make sure update bit is clear before writing
    while (iwdg.sr.wvu)
        ;

    // Unlock window register by writing to key register.
    iwdg.kr          = 0x5555u;

    // Set window register
    struct winr winr = {0};
    winr.win         = val;
    iwdg.winr        = winr;
}
