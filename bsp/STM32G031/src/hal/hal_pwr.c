//*********************************************************************
/*! \file hal_pwr.c
 *
 * \brief Kidde: Mongoose-fw - Implement the PWR HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************
#include "hal_pwr.h"
#include "hal_rcc.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

//*********************************************************************
/**
 * \brief A memory mapped type for the CR1, PWR control register
 */
//*********************************************************************
struct cr1
{
    unsigned lpms      : 3; ///< Low-power mode selection
    unsigned fpd_stop  : 1; ///< Flash memory powered down during Low-power stop mode
    unsigned fdp_lprun : 1; ///< Flash memory powered down during Low-power run mode
    unsigned fdp_lpslp : 1; ///< Flash memory powered down during Low-power sleep mode
    unsigned           : 2;
    unsigned dbp       : 1; ///< Disable RTC domain write protection
    unsigned vos       : 2; ///< Voltage scaling range selection
    unsigned           : 3;
    unsigned lpr       : 1; ///< Low-power run
    unsigned           : 17;
};

struct cr2
{
    unsigned pvde     : 1; ///< Power voltage detector enable
    unsigned pvdft    : 3; ///< Power voltage detector falling threshold selection
    unsigned pvdrt    : 3; ///< Power voltage detector rising threshold selection
    unsigned pvmendac : 1; ///< DAC supply voltage monitoring enable
    unsigned pvmenusb : 1; ///< USB supply voltage monitoring enable
    unsigned iosv     : 1; ///< VVDDIO2 monitoring enable
    unsigned usv      : 1; ///< USB supply enable
    unsigned          : 21;
};

struct cr3
{
    unsigned ewup    : 6; ///< Enable Wakeup pins
    unsigned         : 2;
    unsigned rrs     : 1; ///< SRAM retention in Standby mode
    unsigned enb_ulp : 1; ///<  Ultra-low-power enable
    unsigned apc     : 1; ///< Apply pull-up and pull-down configuration
    unsigned         : 4;
    unsigned eiwul   : 1; ///< Enable internal wakeup line
    unsigned         : 16;
};

struct cr4
{
    unsigned wp   : 6; ///< Wakeup pins polarity
    unsigned      : 2;
    unsigned vbe  : 1; ///< VBAT battery charging enable
    unsigned vbrs : 1; ///< VBAT battery charging resistor selection
    unsigned      : 22;
};

struct sr1
{
    unsigned wuf  : 6; ///< Wakeup flags 1-6
    unsigned      : 2;
    unsigned sbf  : 1; ///< Standby flag
    unsigned      : 6;
    unsigned wufi : 1; ///< Wakeup flag internal
    unsigned      : 16;
};

struct sr2
{
    unsigned           : 7;
    unsigned flash_rdy : 1; ///<  Flash ready flag
    unsigned reglps    : 1; ///< Low-power regulator started
    unsigned reglpf    : 1; ///<  Low-power regulator flag
    unsigned vosf      : 1; ///< Voltage scaling flag
    unsigned pvdo      : 1; ///< Power voltage detector output
    unsigned pvmousb   : 1; ///< USB supply voltage monitoring output flag
    unsigned           : 2;
    unsigned pvmodac   : 1; ///< VDDA monitoring output flag
    unsigned           : 16;
};

struct scr
{
    unsigned cwuf : 6; ///< Clear wakeup flags 1-6
    unsigned      : 2;
    unsigned csbf : 1; ///< Clear standby flag
    unsigned      : 23;
};

struct pucra
{
    unsigned pu0  : 1; ///< Port A pull-up bit 1
    unsigned pu2  : 1; ///< Port A pull-up bit 2
    unsigned pu3  : 1; ///< Port A pull-up bit 3
    unsigned pu4  : 1; ///< Port A pull-up bit 4
    unsigned pu5  : 1; ///< Port A pull-up bit 5
    unsigned pu6  : 1; ///< Port A pull-up bit 6
    unsigned pu7  : 1; ///< Port A pull-up bit 7
    unsigned pu8  : 1; ///< Port A pull-up bit 8
    unsigned pu9  : 1; ///< Port A pull-up bit 9
    unsigned pu10 : 1; ///< Port A pull-up bit 10
    unsigned pu11 : 1; ///< Port A pull-up bit 11
    unsigned pu12 : 1; ///< Port A pull-up bit 12
    unsigned pu13 : 1; ///< Port A pull-up bit 13
    unsigned pu14 : 1; ///< Port A pull-up bit 14
    unsigned pu15 : 1; ///< Port A pull-up bit 15
    unsigned      : 16;
};

struct pdcra
{
    unsigned pu0  : 1; ///< Port A pull-down bit 1
    unsigned pu2  : 1; ///< Port A pull-down bit 2
    unsigned pu3  : 1; ///< Port A pull-down bit 3
    unsigned pu4  : 1; ///< Port A pull-down bit 4
    unsigned pu5  : 1; ///< Port A pull-down bit 5
    unsigned pu6  : 1; ///< Port A pull-down bit 6
    unsigned pu7  : 1; ///< Port A pull-down bit 7
    unsigned pu8  : 1; ///< Port A pull-down bit 8
    unsigned pu9  : 1; ///< Port A pull-down bit 9
    unsigned pu10 : 1; ///< Port A pull-down bit 10
    unsigned pu11 : 1; ///< Port A pull-down bit 11
    unsigned pu12 : 1; ///< Port A pull-down bit 12
    unsigned pu13 : 1; ///< Port A pull-down bit 13
    unsigned pu14 : 1; ///< Port A pull-down bit 14
    unsigned pu15 : 1; ///< Port A pull-down bit 15
    unsigned      : 16;
};

struct pucrb
{
    unsigned pu0  : 1; ///< Port b pull-up bit 1
    unsigned pu2  : 1; ///< Port b pull-up bit 2
    unsigned pu3  : 1; ///< Port b pull-up bit 3
    unsigned pu4  : 1; ///< Port b pull-up bit 4
    unsigned pu5  : 1; ///< Port b pull-up bit 5
    unsigned pu6  : 1; ///< Port b pull-up bit 6
    unsigned pu7  : 1; ///< Port b pull-up bit 7
    unsigned pu8  : 1; ///< Port b pull-up bit 8
    unsigned pu9  : 1; ///< Port b pull-up bit 9
    unsigned pu10 : 1; ///< Port b pull-up bit 10
    unsigned pu11 : 1; ///< Port b pull-up bit 11
    unsigned pu12 : 1; ///< Port b pull-up bit 12
    unsigned pu13 : 1; ///< Port b pull-up bit 13
    unsigned pu14 : 1; ///< Port b pull-up bit 14
    unsigned pu15 : 1; ///< Port b pull-up bit 15
    unsigned      : 16;
};

struct pdcrb
{
    unsigned pu0  : 1; ///< Port b pull-down bit 1
    unsigned pu2  : 1; ///< Port b pull-down bit 2
    unsigned pu3  : 1; ///< Port b pull-down bit 3
    unsigned pu4  : 1; ///< Port b pull-down bit 4
    unsigned pu5  : 1; ///< Port b pull-down bit 5
    unsigned pu6  : 1; ///< Port b pull-down bit 6
    unsigned pu7  : 1; ///< Port b pull-down bit 7
    unsigned pu8  : 1; ///< Port b pull-down bit 8
    unsigned pu9  : 1; ///< Port b pull-down bit 9
    unsigned pu10 : 1; ///< Port b pull-down bit 10
    unsigned pu11 : 1; ///< Port b pull-down bit 11
    unsigned pu12 : 1; ///< Port b pull-down bit 12
    unsigned pu13 : 1; ///< Port b pull-down bit 13
    unsigned pu14 : 1; ///< Port b pull-down bit 14
    unsigned pu15 : 1; ///< Port b pull-down bit 15
    unsigned      : 16;
};

struct pucrc
{
    unsigned pu0  : 1; ///< Port c pull-up bit 1
    unsigned pu2  : 1; ///< Port c pull-up bit 2
    unsigned pu3  : 1; ///< Port c pull-up bit 3
    unsigned pu4  : 1; ///< Port c pull-up bit 4
    unsigned pu5  : 1; ///< Port c pull-up bit 5
    unsigned pu6  : 1; ///< Port c pull-up bit 6
    unsigned pu7  : 1; ///< Port c pull-up bit 7
    unsigned pu8  : 1; ///< Port c pull-up bit 8
    unsigned pu9  : 1; ///< Port c pull-up bit 9
    unsigned pu10 : 1; ///< Port c pull-up bit 10
    unsigned pu11 : 1; ///< Port c pull-up bit 11
    unsigned pu12 : 1; ///< Port c pull-up bit 12
    unsigned pu13 : 1; ///< Port c pull-up bit 13
    unsigned pu14 : 1; ///< Port c pull-up bit 14
    unsigned pu15 : 1; ///< Port c pull-up bit 15
    unsigned      : 16;
};

struct pdcrc
{
    unsigned pu0  : 1; ///< Port c pull-down bit 1
    unsigned pu2  : 1; ///< Port c pull-down bit 2
    unsigned pu3  : 1; ///< Port c pull-down bit 3
    unsigned pu4  : 1; ///< Port c pull-down bit 4
    unsigned pu5  : 1; ///< Port c pull-down bit 5
    unsigned pu6  : 1; ///< Port c pull-down bit 6
    unsigned pu7  : 1; ///< Port c pull-down bit 7
    unsigned pu8  : 1; ///< Port c pull-down bit 8
    unsigned pu9  : 1; ///< Port c pull-down bit 9
    unsigned pu10 : 1; ///< Port c pull-down bit 10
    unsigned pu11 : 1; ///< Port c pull-down bit 11
    unsigned pu12 : 1; ///< Port c pull-down bit 12
    unsigned pu13 : 1; ///< Port c pull-down bit 13
    unsigned pu14 : 1; ///< Port c pull-down bit 14
    unsigned pu15 : 1; ///< Port c pull-down bit 15
    unsigned      : 16;
};

struct pucrd
{
    unsigned pu0  : 1; ///< Port d pull-up bit 1
    unsigned pu2  : 1; ///< Port d pull-up bit 2
    unsigned pu3  : 1; ///< Port d pull-up bit 3
    unsigned pu4  : 1; ///< Port d pull-up bit 4
    unsigned pu5  : 1; ///< Port d pull-up bit 5
    unsigned pu6  : 1; ///< Port d pull-up bit 6
    unsigned pu7  : 1; ///< Port d pull-up bit 7
    unsigned pu8  : 1; ///< Port d pull-up bit 8
    unsigned pu9  : 1; ///< Port d pull-up bit 9
    unsigned pu10 : 1; ///< Port d pull-up bit 10
    unsigned pu11 : 1; ///< Port d pull-up bit 11
    unsigned pu12 : 1; ///< Port d pull-up bit 12
    unsigned pu13 : 1; ///< Port d pull-up bit 13
    unsigned pu14 : 1; ///< Port d pull-up bit 14
    unsigned pu15 : 1; ///< Port d pull-up bit 15
    unsigned      : 16;
};

struct pdcrd
{
    unsigned pu0  : 1; ///< Port d pull-down bit 1
    unsigned pu2  : 1; ///< Port d pull-down bit 2
    unsigned pu3  : 1; ///< Port d pull-down bit 3
    unsigned pu4  : 1; ///< Port d pull-down bit 4
    unsigned pu5  : 1; ///< Port d pull-down bit 5
    unsigned pu6  : 1; ///< Port d pull-down bit 6
    unsigned pu7  : 1; ///< Port d pull-down bit 7
    unsigned pu8  : 1; ///< Port d pull-down bit 8
    unsigned pu9  : 1; ///< Port d pull-down bit 9
    unsigned pu10 : 1; ///< Port d pull-down bit 10
    unsigned pu11 : 1; ///< Port d pull-down bit 11
    unsigned pu12 : 1; ///< Port d pull-down bit 12
    unsigned pu13 : 1; ///< Port d pull-down bit 13
    unsigned pu14 : 1; ///< Port d pull-down bit 14
    unsigned pu15 : 1; ///< Port d pull-down bit 15
    unsigned      : 16;
};

struct pucre
{
    unsigned pu0  : 1; ///< Port e pull-up bit 1
    unsigned pu2  : 1; ///< Port e pull-up bit 2
    unsigned pu3  : 1; ///< Port e pull-up bit 3
    unsigned pu4  : 1; ///< Port e pull-up bit 4
    unsigned pu5  : 1; ///< Port e pull-up bit 5
    unsigned pu6  : 1; ///< Port e pull-up bit 6
    unsigned pu7  : 1; ///< Port e pull-up bit 7
    unsigned pu8  : 1; ///< Port e pull-up bit 8
    unsigned pu9  : 1; ///< Port e pull-up bit 9
    unsigned pu10 : 1; ///< Port e pull-up bit 10
    unsigned pu11 : 1; ///< Port e pull-up bit 11
    unsigned pu12 : 1; ///< Port e pull-up bit 12
    unsigned pu13 : 1; ///< Port e pull-up bit 13
    unsigned pu14 : 1; ///< Port e pull-up bit 14
    unsigned pu15 : 1; ///< Port e pull-up bit 15
    unsigned      : 16;
};

struct pdcre
{
    unsigned pu0  : 1; ///< Port e pull-down bit 1
    unsigned pu2  : 1; ///< Port e pull-down bit 2
    unsigned pu3  : 1; ///< Port e pull-down bit 3
    unsigned pu4  : 1; ///< Port e pull-down bit 4
    unsigned pu5  : 1; ///< Port e pull-down bit 5
    unsigned pu6  : 1; ///< Port e pull-down bit 6
    unsigned pu7  : 1; ///< Port e pull-down bit 7
    unsigned pu8  : 1; ///< Port e pull-down bit 8
    unsigned pu9  : 1; ///< Port e pull-down bit 9
    unsigned pu10 : 1; ///< Port e pull-down bit 10
    unsigned pu11 : 1; ///< Port e pull-down bit 11
    unsigned pu12 : 1; ///< Port e pull-down bit 12
    unsigned pu13 : 1; ///< Port e pull-down bit 13
    unsigned pu14 : 1; ///< Port e pull-down bit 14
    unsigned pu15 : 1; ///< Port e pull-down bit 15
    unsigned      : 16;
};

struct pucrf
{
    unsigned pu0  : 1; ///< Port f pull-up bit 1
    unsigned pu2  : 1; ///< Port f pull-up bit 2
    unsigned pu3  : 1; ///< Port f pull-up bit 3
    unsigned pu4  : 1; ///< Port f pull-up bit 4
    unsigned pu5  : 1; ///< Port f pull-up bit 5
    unsigned pu6  : 1; ///< Port f pull-up bit 6
    unsigned pu7  : 1; ///< Port f pull-up bit 7
    unsigned pu8  : 1; ///< Port f pull-up bit 8
    unsigned pu9  : 1; ///< Port f pull-up bit 9
    unsigned pu10 : 1; ///< Port f pull-up bit 10
    unsigned pu11 : 1; ///< Port f pull-up bit 11
    unsigned pu12 : 1; ///< Port f pull-up bit 12
    unsigned pu13 : 1; ///< Port f pull-up bit 13
    unsigned pu14 : 1; ///< Port f pull-up bit 14
    unsigned pu15 : 1; ///< Port f pull-up bit 15
    unsigned      : 16;
};

struct pdcrf
{
    unsigned pu0  : 1; ///< Port f pull-down bit 1
    unsigned pu2  : 1; ///< Port f pull-down bit 2
    unsigned pu3  : 1; ///< Port f pull-down bit 3
    unsigned pu4  : 1; ///< Port f pull-down bit 4
    unsigned pu5  : 1; ///< Port f pull-down bit 5
    unsigned pu6  : 1; ///< Port f pull-down bit 6
    unsigned pu7  : 1; ///< Port f pull-down bit 7
    unsigned pu8  : 1; ///< Port f pull-down bit 8
    unsigned pu9  : 1; ///< Port f pull-down bit 9
    unsigned pu10 : 1; ///< Port f pull-down bit 10
    unsigned pu11 : 1; ///< Port f pull-down bit 11
    unsigned pu12 : 1; ///< Port f pull-down bit 12
    unsigned pu13 : 1; ///< Port f pull-down bit 13
    unsigned pu14 : 1; ///< Port f pull-down bit 14
    unsigned pu15 : 1; ///< Port f pull-down bit 15
    unsigned      : 16;
};

//*********************************************************************
/**
 * @brief A memory mapped type for PWR registers
 */
struct pwr
{
    struct cr1 cr1;     ///< Control register 1
    struct cr2 cr2;     ///< Control register 2
    struct cr3 cr3;     ///< Control register 3
    struct cr4 cr4;     ///< Control register 4
    struct sr1 sr1;     ///< Status register 1
    struct sr2 sr2;     ///< Status register 2
    struct scr scr;     ///< Status clear register
    unsigned reserved;  ///< No register here.
    struct pucra pucra; ///< Port A pull-up control register
    struct pdcra pdcra; ///< Port A pull-down control register
    struct pucrb pucrb; ///< Port B pull-up control register
    struct pdcrb pdcrb; ///< Port B pull-down control register
    struct pucrc pucrc; ///< Port C pull-up control register
    struct pdcrc pdcrc; ///< Port C pull-down control register
    struct pucrd pucrd; ///< Port D pull-up control register
    struct pdcrd pdcrd; ///< Port D pull-down control register
    struct pucre pucre; ///< Port E pull-up control register
    struct pdcre pdcre; ///< Port E pull-down control register
    struct pucrf pucrf; ///< Port F pull-up control register
    struct pdcrf pdcrf; ///< Port F pull-down control register
} volatile pwr __attribute__((section(".bss.pwr")));

//*********************************************************************
/**
 * \brief Enable access to the RTC domain and backup registers
 * \note In reset state, the RTC and backup registers are write protected. This must be called to enable write access.
 */
//*********************************************************************
void pwr_enable_rtc_access(void)
{
    rcc_enable_pwr_clock(true);
    pwr.cr1.dbp = 1;
}

//*********************************************************************
/**
 * \brief Enable a specific wakeup pin and set their polarity to a falling edge
 * \param pin_number The value that corresponds to the a given wakeup pin. The wake up pin can be 6 through 1 inclusive.
 * \param polarity The polarity to set.
 */
//*********************************************************************
void enable_wkup_pin(uint8_t const pin_number, wkup_polarity const polarity)
{
    // Set polarity of the wakeup pin via WP6 through WP1 based on passed pin number
    struct cr4 cr4 = pwr.cr4;
    cr4.wp &= ~(1 << (pin_number - 1));     // set the wakeup pin polarity to modify to an inital value of 0.
    cr4.wp |= polarity << (pin_number - 1); // set the wakeup pin polarity to our desired value.
    pwr.cr4 = cr4;

    // Enable the wakeup pin via EWUP6 through EWUP1 based on passed pin number
    pwr.cr3.ewup |= 1 << (pin_number - 1);
}

//*********************************************************************
/**
 * \brief Disable a specific wakeup pin
 * \param pin_number The value that corresponds to the a given wakeup pin. The wake up pin can be 6 through 1 inclusive.
 */
//*********************************************************************
void disable_wkup_pin(uint8_t const pin_number)
{
    // Disable the wakeup pin via EWUP6 through EWUP1 based on passed pin number
    pwr.cr3.ewup &= ~(1 << (pin_number - 1));
}

//*********************************************************************
/**
 * \brief Determine if a specific wakeup pin caused a wakeup event to occur
 * \param pin_number The value that corresponds to the a given wakeup pin. The wake up pin can be 6 through 1 inclusive.
 * \returns returns true if a wakeup event occurred on the specific pin.
 */
//*********************************************************************
bool wkup_occurred(uint8_t const pin_number)
{
    // Check wakeup flag via WUF6through WUF1 based on passed pin number.
    bool const occurred = pwr.sr1.wuf & (1 << (pin_number - 1)); // check temp value at bit pin_number to see if it is 1
                                                                 // meaning wuf bit pin_number flag up

    // Set CWUF 6through CWUF1 to clear the selected wakeup pin flag WP6 through WP1
    pwr.scr.cwuf |= 1 << (pin_number - 1);

    // returns true if a wakeup event occured
    return occurred;
}

//*********************************************************************
/**
 * \brief clears a specific wakeup pin
 * \param pin_number The value that corresponds to the a given wakeup pin. The wake up pin can be 6 through 1 inclusive.
 */
//*********************************************************************
void clear_wkup_pin(uint8_t const pin_number)
{
    // Disable the wakeup pin flag via CWUF6 through CWUF1 based on passed pin number
    pwr.scr.cwuf |= 1 << (pin_number - 1);
}

//*********************************************************************
/**
 * \brief Clears all wakeup pins.
 */
//*********************************************************************
void clear_wkup_pin_all(void)
{
    // Clear all by setting all pins.
    pwr.scr.cwuf = 0x3f;
}

//*********************************************************************
/**
 * \brief Determine if a wake from standby occurred.
 * \returns returns true if the wakeup event was standby.
 */
//*********************************************************************
bool wkup_occurred_standby(void)
{
    return pwr.sr1.sbf;
}

//*********************************************************************
/**
 * \brief Clears the standby wakeup pin
 */
//*********************************************************************
void clear_wkup_pin_standby(void)
{
    // Disable the wakeup pin flag via CWUF6 through CWUF1 based on passed pin number
    pwr.scr.csbf = 1;
}

//*********************************************************************
/**
 * \brief Sets the lpms bits.
 * \param mode the low power mode to set.
 */
//*********************************************************************
void pwr_set_low_power(lpms_mode const mode)
{
    pwr.cr1.lpms = mode;
}

//*********************************************************************
/**
 * \brief Sets pullup/pulldown regs for standby mode.
 * \param reg The register to set.
 * \param setting The setting of the register.
 */
//*********************************************************************
void set_pullupdown(pullupdown_control const reg, unsigned setting)
{
    unsigned *ptr_regs = (unsigned *)&(pwr.pucra);
    *(ptr_regs + reg)  = setting;
}

//*********************************************************************
/**
 * \brief Enables RAM in standby mode.
 */
//*********************************************************************
void pwr_ram_on_in_standby(void)
{
    pwr.cr3.rrs = 1;
}

//*********************************************************************
/**
 * \brief Enables the pullup/pulldown configuration.
 */
//*********************************************************************
void pwr_enable_pullupdown(void)
{
    pwr.cr3.apc = 1;
}
