/**
 * @file hal_gpio.c
 * @brief Implement the GPIO HAL functions
 */
#include "hal_gpio.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

/** Lock Bit mask for Lock bit function     */
#define LOCK_BIT ((uint32_t)0x0100)

/**
 * @brief General Purpose I/O
 */
struct gpio_port
{
    unsigned moder;   /**< GPIO port mode register,               Address offset: 0x00      */
    unsigned otyper;  /**< GPIO port output type register,        Address offset: 0x04      */
    unsigned ospeedr; /**< GPIO port output speed register,       Address offset: 0x08      */
    unsigned pupdr;   /**< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
    unsigned idr;     /**< GPIO port input data register,         Address offset: 0x10      */
    unsigned odr;     /**< GPIO port output data register,        Address offset: 0x14      */
    unsigned bsrr;    /**< GPIO port bit set/reset  register,     Address offset: 0x18      */
    unsigned lckr;    /**< GPIO port configuration lock register, Address offset: 0x1C      */
    unsigned afr[2];  /**< GPIO alternate function registers,     Address offset: 0x20-0x24 */
    unsigned brr;     /**< GPIO Bit Reset register,               Address offset: 0x28      */
};

/**
 * @brief the addresses of the gpio ports are defined in the linker file.
 */
struct gpio_port volatile porta __attribute__((section(".bss.gpioa")));
struct gpio_port volatile portb __attribute__((section(".bss.gpiob")));
struct gpio_port volatile portc __attribute__((section(".bss.gpioc")));

void gpio_set_pin_mode(port_type port, pin_modes const mode, uint8_t const pin)
{
    uint32_t const temp = port->moder & ~((uint32_t)0x03 << (pin * 2));
    port->moder         = temp | ((uint32_t)mode << (pin * 2));
}

void gpio_set_output_type(port_type port, pin_out_type const type, uint8_t const pin)
{
    uint32_t const temp = port->otyper & ~(0x01u << pin);
    port->otyper        = temp | ((uint32_t)type << (pin));
}

void gpio_set_output(port_type port, uint8_t const pin)
{
    gpio_set_pin_mode(port, gpio_mode_output, pin);
}

void gpio_set_input(port_type port, uint8_t const pin)
{
    gpio_set_pin_mode(port, gpio_mode_input, pin);
}

void gpio_drive_high(port_type port, uint8_t const pin)
{
    port->bsrr = (uint32_t)0x01 << pin;
}

void gpio_drive_low(port_type port, uint8_t const pin)
{
    port->brr = (uint32_t)0x01 << pin;
}

void gpio_set_pin_speed(port_type port, pin_speed const mode, uint8_t const pin)
{
    uint32_t const temp = port->ospeedr & ~((uint32_t)0x03 << (pin * 2));
    port->ospeedr       = temp | ((uint32_t)mode << (pin * 2));
}

void gpio_pull_updown_mode(port_type port, pin_pupd const mode, uint8_t const pin)
{
    uint32_t const temp = port->pupdr & ~((uint32_t)0x03 << (pin * 2));
    port->pupdr         = temp | ((uint32_t)mode << (pin * 2));
}

void gpio_set_afr_pin(port_type port, uint32_t const mode, uint8_t const pin)
{
    uint32_t const temp = port->afr[pin / 8] & ~(0xfu << ((pin % 8) * 4));
    port->afr[pin / 8]  = temp | (mode << ((pin % 8) * 4));
}

bool gpio_read(port_type port, uint8_t const pin)
{
    return port->idr & (1 << pin);
}

bool gpio_lock(port_type port, uint16_t const lock_mask)
{
    // Perform the lock key write sequence
    port->lckr    = (uint32_t)lock_mask | LOCK_BIT;
    port->lckr    = (uint32_t)lock_mask;
    port->lckr    = (uint32_t)lock_mask | LOCK_BIT;
    uint32_t temp = port->lckr;

    // Cast to void to eliminate compiler "unused variable" warning.
    (void)temp;
    return port->lckr & LOCK_BIT;
}

void process_pulls(port_type port, unsigned *pullup_config, unsigned *pulldown_config)
{
    unsigned gpiox_moder = port->moder;
    gpiox_moder &= ~gpiox_moder >> 1;
    gpiox_moder              = (gpiox_moder | (gpiox_moder >> 0)) & 0x55555555;
    gpiox_moder              = (gpiox_moder | (gpiox_moder >> 1)) & 0x33333333;
    gpiox_moder              = (gpiox_moder | (gpiox_moder >> 2)) & 0x0f0f0f0f;
    gpiox_moder              = (gpiox_moder | (gpiox_moder >> 4)) & 0x00ff00ff;
    gpiox_moder              = (gpiox_moder | (gpiox_moder >> 8)) & 0x0000ffff;

    unsigned const gpiox_odr = port->odr;

    *pullup_config           = gpiox_moder & gpiox_odr;
    *pulldown_config         = gpiox_moder & ~gpiox_odr;
}
