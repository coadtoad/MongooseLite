//*********************************************************************
/*! \file hal_rtc.c
 *
 * \brief Kidde: Mongoose-fw - Implement the RTC HAL functions
 *
 * \author
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************

#include "hal_rtc.h"
#include "drv_led.h"
#include "drv_timer.h"
#include "hal_exti.h"
#include "hal_interrupt.h"
#include "hal_nvic.h"
#include "hal_rcc.h"
#include <arm_acle.h> // For __nop()
#include <stdlib.h>

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

//*********************************************************************
/**
 * \brief A memory mapped type for calendar time shadow register.
 */
//*********************************************************************
struct tr
{
    unsigned su  : 4; /**< Used to read/write second units in BCD format */
    unsigned st  : 3; /**< Used to read/write second tens in BCD format */
    unsigned     : 1; /**< Unused */
    unsigned mnu : 4; /**< Used to read/write minute units in BCD format */
    unsigned mnt : 3; /**< Used to read/write minute tens in BCD format */
    unsigned     : 1; /**< Unused */
    unsigned hu  : 4; /**< Used to read/write hour units in BCD format */
    unsigned ht  : 2; /**< Used to read/write hour tens in BCD format */
    unsigned pm  : 1; /**< Used to denote the time is AM or PM */
    unsigned     : 9; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for calendar date shadow register.
 */
//*********************************************************************
struct dr
{
    unsigned du  : 4; /**< Used to read/write date units in BCD format */
    unsigned dt  : 2; /**< Used to read/write date tens in BCD format */
    unsigned     : 2; /**< Unused */
    unsigned mu  : 4; /**< Used to read/write month units in BCD format */
    unsigned mt  : 1; /**< Used to read/write month tens in BCD format */
    unsigned wdu : 3; /**< Used to read/write week day unit */
    unsigned yu  : 4; /**< Used to read/write year units in BCD format */
    unsigned yt  : 4; /**< Used to read/write year tens in BCD format */
    unsigned     : 8; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for RTC sub second register.
 */
//*********************************************************************
struct ssr
{
    unsigned ss : 16; /**< Used to read sub second value */
    unsigned    : 16; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for RTC initialization control and status register.
 */
//*********************************************************************
struct icsr
{
    unsigned alrawf  : 1;  /**< Used to read alarm A write flag */
    unsigned alrbwf  : 1;  /**< Used to read alarm B write flag */
    unsigned wutwf   : 1;  /**< Used to read wake-up timer write flag */
    unsigned shpf    : 1;  /**< Used to indicate if shift operation is pending or not */
    unsigned inits   : 1;  /**< Used to indicate if the calendar has been initialized or not */
    unsigned rsf     : 1;  /**< Used to indicate if calendar shadow registers are synchronized or not */
    unsigned initf   : 1;  /**< Used to indicate if calendar registers update is allowed or not */
    unsigned init    : 1;  /**< Used to configure init mode to program time and date registers */
    unsigned         : 8;  /**< Unused */
    unsigned recalpf : 1;  /**< Used to indicate if the recalibration is pending or not */
    unsigned         : 15; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for prescalar register.
 */
//*********************************************************************
struct prer
{
    unsigned prediv_s : 15; /**< Used to write synchronous prescalar factor */
    unsigned          : 1;  /**< Unused */
    unsigned prediv_a : 7;  /**< Used to write asynchronous prescalar factor */
    unsigned          : 9;  /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for wake up timer register.
 */
//*********************************************************************
struct wutr
{
    unsigned wut : 16; /**< Used to read/write wake-up auto reload value */
    unsigned     : 16; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for control register.
 */
//*********************************************************************
struct cr
{
    unsigned wucksel       : 3; /**< Used to configure wake-up clock */
    unsigned tsedge        : 1; /**< Used to configure time stamp active edge*/
    unsigned refckon       : 1; /**< Used to enable/disable reference clock detection */
    unsigned bypshad       : 1; /**< Used to bypass calendar shadow registers */
    unsigned fmt           : 1; /**< Used to configure hour format - 24hr/12hr */
    unsigned               : 1; /**< Unused */
    unsigned alrae         : 1; /**< Used to enable/disable ALARM A */
    unsigned alrbe         : 1; /**< Used to enable/disable ALARM B */
    unsigned wute          : 1; /**< Used to enable/disable wake up timer */
    unsigned tse           : 1; /**< Used to enable/disable time stamp */
    unsigned alraie        : 1; /**< Used to enable/disable ALARM A interrupt */
    unsigned alrbie        : 1; /**< Used to enable/disable ALARM B interrupt */
    unsigned wutie         : 1; /**< Used to enable/disable wake up timer interrupt */
    unsigned tsie          : 1; /**< Used to enable/disable time stamp interrupt */
    unsigned add1h         : 1; /**< Used to configure summer time change - Adding 1 hr */
    unsigned sub1h         : 1; /**< Used to configure winter time change - Subtracting 1 hr */
    unsigned bkp           : 1; /**< Used to memorize if the day light saving time change has been performed or not */
    unsigned cosel         : 1; /**< Used to configure the calibration output - 512Hz/1Hz */
    unsigned pol           : 1; /**< Used to configure the polarity of TAMPALRM output */
    unsigned osel          : 2; /**< Used to select the flag to be routed to TAMPALRM output */
    unsigned coe           : 1; /**< Used to enable/disable calibration output */
    unsigned itse          : 1; /**< Used to enable/disable time stamp on internal event */
    unsigned tampts        : 1; /**< Used to activate time stamp on tamper detection event */
    unsigned tampoe        : 1; /**< Used to enable/disable tamper detection output on TAMPALRM */
    unsigned               : 2; /**< Unused */
    unsigned tampalrm_pu   : 1; /**< Used to enable/disable pull up for TAMPALRM */
    unsigned tampalrm_type : 1; /**< Used to select between push-pull and open drain output for TAMPALRM */
    unsigned out2en        : 1; /**< Used to enable/disable RTC output 2 */
};

//*********************************************************************
/**
 * \brief A memory mapped type for RTC calibration register.
 */
//*********************************************************************
struct calr
{
    unsigned calm   : 9;  /**< Used to write/read calibration minus */
    unsigned        : 4;  /**< Unused */
    unsigned calw16 : 1;  /**< Used to select 16 second calibration cycle period */
    unsigned calw8  : 1;  /**< Used to select 8 second calibration cycle period */
    unsigned calp   : 1;  /**< Used to increase the frequency of RTC by 488.5 ppm */
    unsigned        : 16; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for RTC shift control register.
 */
//*********************************************************************
struct shiftr
{
    unsigned subfs : 15; /**< Used to write a value to subtract from synchronous presaclar counter */
    unsigned       : 16; /**< Unused */
    unsigned add1s : 1;  /**< Used to add 1 sec to clock/calendar */
};

//*********************************************************************
/**
 * \brief A memory mapped type for time stamp time register.
 */
//*********************************************************************
struct tstr
{
    unsigned su  : 4; /**< Used to read second units */
    unsigned st  : 3; /**< Used to read second tens */
    unsigned     : 1; /**< Unused */
    unsigned mnu : 4; /**< Used to read minute units */
    unsigned mnt : 3; /**< Used to read minute tens */
    unsigned     : 1; /**< Unused */
    unsigned hu  : 4; /**< Used to read hour units */
    unsigned ht  : 2; /**< Used to read hour tens */
    unsigned pm  : 1; /**< Used to read AM/PM notation */
    unsigned     : 9; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for time stamp date register.
 */
//*********************************************************************
struct tsdr
{
    unsigned du  : 4;  /**< Used to read date units */
    unsigned dt  : 2;  /**< Used to read date tens */
    unsigned     : 2;  /**< Unused */
    unsigned mu  : 4;  /**< Used to read month units */
    unsigned mt  : 1;  /**< Used to read month tens */
    unsigned wdu : 3;  /**< Used to read week day units */
    unsigned     : 16; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for time stamp sub second register.
 */
//*********************************************************************
struct tsssr
{
    unsigned ss : 16; /**< Used to read sub second value */
    unsigned    : 16; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for Alarm A register.
 */
//*********************************************************************
struct alrmar
{
    unsigned su    : 4; /**< Used to read/write second units in BCD format */
    unsigned st    : 3; /**< Used to read/write date tens in BCD format */
    unsigned msk1  : 1; /**< Alarm A seconds mask */
    unsigned mnu   : 4; /**< Used to read/write minute units in BCD format */
    unsigned mnt   : 3; /**< Used to read/write minute tens in BCD format */
    unsigned msk2  : 1; /**< Alarm A minutes mask */
    unsigned hu    : 4; /**< Used to read/write hour units in BCD format */
    unsigned ht    : 2; /**< Used to read/write hour tens in BCD format */
    unsigned pm    : 1; /**< Used to read/write AM/PM notation */
    unsigned msk3  : 1; /**< Alarm A hours mask */
    unsigned du    : 4; /**< Used to read/write date units in BCD format */
    unsigned dt    : 2; /**< Used to read/write date tens in BCD format */
    unsigned wdsel : 1; /**< Used to indicate the week day selection */
    unsigned msk4  : 1; /**< Alarm A date mask */
};

//*********************************************************************
/**
 * \brief A memory mapped type for Alarm A sub second register.
 */
//*********************************************************************
struct alrmassr
{
    unsigned ss    : 15; /**< Used to read/write sub second value */
    unsigned       : 9;  /**< Unused */
    unsigned mskss : 4;  /**< Mask the most-significant bits starting at this bit */
    unsigned       : 4;  /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for Alarm B register.
 */
//*********************************************************************
struct alrmbr
{
    unsigned su    : 4; /**< Used to read/write second units in BCD format */
    unsigned st    : 3; /**< Used to read/write date tens in BCD format */
    unsigned msk1  : 1; /**< Alarm B seconds mask */
    unsigned mnu   : 4; /**< Used to read/write minute units in BCD format */
    unsigned mnt   : 3; /**< Used to read/write minute tens in BCD format */
    unsigned msk2  : 1; /**< Alarm B minutes mask */
    unsigned hu    : 4; /**< Used to read/write hour units in BCD format */
    unsigned ht    : 2; /**< Used to read/write hour tens in BCD format */
    unsigned pm    : 1; /**< Used to read/write AM/PM notation */
    unsigned msk3  : 1; /**< Alarm B hours mask */
    unsigned du    : 4; /**< Used to read/write date units in BCD format */
    unsigned dt    : 2; /**< Used to read/write date tens in BCD format */
    unsigned wdsel : 1; /**< Used to indicate the week day selection */
    unsigned msk4  : 1; /**< Alarm B date mask */
};

//*********************************************************************
/**
 * \brief A memory mapped type for Alarm B sub second register.
 */
//*********************************************************************
struct alrmbssr
{
    unsigned ss    : 15; /**< Used to read/write sub second value */
    unsigned       : 9;  /**< Unused */
    unsigned mskss : 4;  /**< Mask the most-significant bits starting at this bit */
    unsigned       : 4;  /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for status register.
 */
//*********************************************************************
struct sr
{
    unsigned alraf : 1;  /**< Used to read Alarm A flag */
    unsigned alrbf : 1;  /**< Used to read Alarm B flag */
    unsigned wutf  : 1;  /**< Used to read wake up timer flag */
    unsigned tsf   : 1;  /**< Used to read time stamp flag */
    unsigned tsovf : 1;  /**< Used to read time stamp overflow flag */
    unsigned itsf  : 1;  /**< Used to read internal time stamp flag */
    unsigned       : 26; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for masked interrupt status register.
 */
//*********************************************************************
struct misr
{
    unsigned alramf : 1;  /**< Used to read Alarm A masked flag */
    unsigned alrbmf : 1;  /**< Used to read Alarm B masked flag */
    unsigned wutmf  : 1;  /**< Used to read wake up timer masked flag */
    unsigned tsmf   : 1;  /**< Used to read time stamp masked flag */
    unsigned tsovmf : 1;  /**< Used to read time stamp overflow masked flag */
    unsigned itsmf  : 1;  /**< Used to read internal time stamp masked flag */
    unsigned        : 26; /**< Unused */
};

//*********************************************************************
/**
 * \brief A memory mapped type for status clear register.
 */
//*********************************************************************
struct scr
{
    unsigned calraf : 1;  /**< Used to clear Alarm A flag */
    unsigned calrbf : 1;  /**< Used to clear Alarm B flag */
    unsigned cwutf  : 1;  /**< Used to clear wake up timer flag */
    unsigned ctsf   : 1;  /**< Used to clear time stamp flag */
    unsigned ctsovf : 1;  /**< Used to clear time stamp overflow flag */
    unsigned citsf  : 1;  /**< Used to clear internal time stamp flag */
    unsigned        : 26; /**< Unused */
};

//*********************************************************************
/**
 * \brief The address of the RTC is defined in the linker file.
 */
//*********************************************************************
struct rtc
{
    struct tr tr;             /**< Used to read/configure RTC time register */
    struct dr dr;             /**< Used to read/configure RTC date register */
    struct ssr ssr;           /**< Used to read/configure RTC sub second register */
    struct icsr icsr;         /**< Used to read/configure RTC initialization and stauts register */
    struct prer prer;         /**< Used to read/configure RTC prescalar register */
    struct wutr wutr;         /**< Used to read/configure RTC wake up timer register */
    struct cr cr;             /**< Used to read/configure RTC control register */
    unsigned reserved1[2];    /**< Unused */
    unsigned wpr;             /**< Usd to read/configure RTC write protection register */
    struct calr calr;         /**< Usd to read/configure RTC calibration register */
    struct shiftr shiftr;     /**< Usd to read/configure RTC shift control register */
    struct tstr tstr;         /**< Usd to read/configure RTC time stamp time register */
    struct tsdr tsdr;         /**< Usd to read/configure RTC time stamp date register */
    struct tsssr tsssr;       /**< Usd to read/configure RTC time stamp sub second register */
    unsigned reserved2;       /**< Unused */
    struct alrmar alrmar;     /**< Usd to read/configure RTC Alarm A register */
    struct alrmassr alrmassr; /**< Usd to read/configure RTC Alarm A subsecond register */
    struct alrmbr alrmbr;     /**< Usd to read/configure RTC Alarm B register */
    struct alrmbssr alrmbssr; /**< Usd to read/configure RTC Alarm B subsecond register */
    struct sr sr;             /**< Usd to read/configure RTC status register */
    struct misr misr;         /**< Usd to read/configure RTC masked interrupt status register */
    unsigned reserved3;       /**< Unused */
    struct scr scr;           /**< Usd to read/configure RTC status clear register */
} volatile rtc __attribute__((section(".bss.rtc")));

//*********************************************************************
/**
 * \brief Disable write protection of RTC registers.
 */
//*********************************************************************
static void rtc_write_protection_disable(void)
{
    rtc.wpr = 0xCAu;
    __nop(); // 2 wait states required
    __nop();
    rtc.wpr = 0x53u;
}

//*********************************************************************
/**
 * \brief Enable write protection of RTC registers.
 */
//*********************************************************************
static void rtc_write_protection_enable(void)
{
    rtc.wpr = 0xFFu;
}

//*********************************************************************
/**
 * \brief Enter the RTC init mode
 */
//*********************************************************************
static void rtc_enter_init_mode(void)
{
    struct icsr icsr = rtc.icsr;
    if (!icsr.initf)
    {
        icsr.init = 1;
        rtc.icsr  = icsr;
        __nop();
        __nop();
        while (!rtc.icsr.initf)
            ;
    }
}

void rtc_disable_alarma(void)
{
    rtc_write_protection_disable();
    struct cr cr = rtc.cr;
    // Disable the ALARM A
    cr.alrae     = 0;
    // Disable the Alarm A interrupt
    cr.alraie    = 0;
    rtc.cr       = cr;
    rtc_write_protection_enable();
}

void rtc_disable_alarmb(void)
{
    rtc_write_protection_disable();
    struct cr cr = rtc.cr;
    // Disable the ALARM A
    cr.alrbe     = 0;
    // Disable the Alarm A interrupt
    cr.alrbie    = 0;
    rtc.cr       = cr;
    rtc_write_protection_enable();
}

void rtc_disable_wake_up_timer(void)
{
    rtc_write_protection_disable();
    struct cr cr;
    // Disable the wake up timer
    cr.wute  = 0;
    // Disable the wake up timer interrupt
    cr.wutie = 0;
    rtc.cr   = cr;

    rtc_write_protection_enable();
}
//*********************************************************************
/**
 * \brief Exit the RTC init mode
 */
//*********************************************************************
static void rtc_exit_init_mode(void)
{
    struct icsr icsr = rtc.icsr;
    __nop();
    icsr.init = 0;
    rtc.icsr  = icsr;
}

bool rtc_init(void)
{
    if (!rcc_lse_clock_source_control(true, false, lse_high_drive))
    {
        return false;
    }
    rcc_enable_rtc_clock(rtc_lse);
    rcc_enable_rtcapb_clock(true);

    rtc_write_protection_disable();

    rtc.icsr.rsf = 0;

    rtc_enter_init_mode();
    struct prer prer = {0};
    // Assign the default asynchronous prescalar
    prer.prediv_a    = 32;
    // Assign the default synchronous prescalar
    // To have a counter which counts every ms
    prer.prediv_s    = 999;
    rtc.prer         = prer;

    nvic_enable_irq(irq_rtc_tamp);

    rtc_exit_init_mode();

    rtc_write_protection_enable();
    return true;
}

uint16_t rtc_get_subminute(void)
{
    struct ssr const ssr = rtc.ssr;
    struct tr const tr   = rtc.tr;
    // Read RTC_DR to unlock the shadow registers
    struct dr const dr   = rtc.dr;
    (void)dr;
    uint16_t ms = 10 * tr.st + tr.su;
    ms *= 1000;
    ms += 999 - ssr.ss;
    return ms;
}

uint16_t rtc_get_subminute_with_bypshad(void)
{
    rtc_write_protection_disable();
    rtc.cr.bypshad = 1;
    struct ssr ss1 = rtc.ssr;
    struct tr tr   = rtc.tr;
    struct ssr ss2 = rtc.ssr;
    if (ss1.ss != ss2.ss)
    {
        ss1 = rtc.ssr;
        tr  = rtc.tr;
    }
    rtc.cr.bypshad = 0;
    rtc_write_protection_enable();
    uint16_t ms = 10 * tr.st + tr.su;
    ms *= 1000;
    ms += 999 - ss1.ss;
    return ms;
}

void rtc_set_alarma_subminute(uint16_t const tick)
{
    div_t const s_ms = div(tick, 1000);
    div_t const s_s  = div(s_ms.quot, 10);
    rtc_disable_alarma();

    rtc_write_protection_disable();

    // Clear the alarm A flag
    rtc.scr.calraf = 1;

    while (!rtc.icsr.alrawf)
        ;

    // Configure ALARM A subsecond register
    struct alrmassr const alrmassr = {.ss = 999 - s_ms.rem, .mskss = 15};
    rtc.alrmassr                   = alrmassr;

    // Mask all other attributes of ALARM A(date,hour,minute,second)
    struct alrmar const alrmar     = {.msk4 = 1, .msk3 = 1, .msk2 = 1, .msk1 = 0, .st = s_s.quot, .su = s_s.rem};
    rtc.alrmar                     = alrmar;

    struct cr cr                   = rtc.cr;
    __nop();
    // Enable the ALARM A
    cr.alrae  = 1;
    // Enable the ALARM A interrupt
    cr.alraie = 1;
    rtc.cr    = cr;

    exti_config_wakeonint_mask(rtc_wakeup, exti_enable);

    rtc_write_protection_enable();
}

void rtc_set_alarmb_subsecond(uint16_t const tick)
{
    rtc_disable_alarmb();
    rtc_write_protection_disable();

    // Clear the alarm A flag
    rtc.scr.calrbf = 1;

    while (!rtc.icsr.alrbwf)
        ;

    // Configure ALARM A subsecond register
    struct alrmbssr alrmbssr = {0};
    alrmbssr.ss              = tick;
    alrmbssr.mskss           = 15;
    rtc.alrmbssr             = alrmbssr;

    // Mask all other attributes of ALARM A(date,hour,minute,second)
    struct alrmbr alrmbr     = {0};
    alrmbr.msk1              = 1;
    alrmbr.msk2              = 1;
    alrmbr.msk3              = 1;
    alrmbr.msk4              = 1;
    rtc.alrmbr               = alrmbr;

    struct cr cr             = cr;
    // Enable the ALARM B
    cr.alrbe                 = 1;
    // Enable the ALARM B interrupt
    cr.alrbie                = 1;
    rtc.cr                   = cr;

    exti_config_wakeonint_mask(rtc_wakeup, exti_enable);

    rtc_write_protection_enable();
}

void rtc_set_wake_up_timer(unsigned const wake_up_counter, wake_up_clock const clock)
{
    rtc_disable_wake_up_timer();
    rtc_write_protection_disable();

    // Clear the wake-up timer flag
    rtc.scr.cwutf = 1;

    while (!rtc.icsr.wutwf)
        ;

    // Assign wake-up counter value.
    struct wutr wutr = {0};
    wutr.wut         = wake_up_counter;
    rtc.wutr         = wutr;

    struct cr cr     = rtc.cr;
    // Assign the wake-up clock.
    cr.wucksel       = clock;
    // Enable the wake up timer
    cr.wute          = 1;
    // Enable the wake up timer interrupt
    cr.wutie         = 1;
    rtc.cr           = cr;

    exti_config_wakeonint_mask(rtc_wakeup, exti_enable);

    rtc_write_protection_enable();
}

unsigned rtc_get_wake_up_timer(void)
{
    return rtc.wutr.wut;
}

void rtc_reset_clock(void)
{
    rtc_write_protection_disable();
    rtc_enter_init_mode();

    // Init the time to zero
    struct ssr const ssr = {0};
    struct tr const tr   = {0};
    struct dr const dr   = {0};
    rtc.ssr              = ssr;
    rtc.tr               = tr;
    rtc.dr               = dr;

    rtc_exit_init_mode();
    rtc_write_protection_enable();
}

//*********************************************************************
/**
 * \brief ISR routine for ALARM A interrupt.
 */
//*********************************************************************
__attribute__((weak)) void rtc_alarma_irq(void)
{
    /** \todo ISR body */
}

//*********************************************************************
/**
 * \brief ISR routine for ALARM B interrupt.
 */
//*********************************************************************
__attribute__((weak)) void rtc_alarmb_irq(void)
{
    /** \todo ISR body */
}

//*********************************************************************
/**
 * \brief ISR routine for wake up timer interrupt.
 */
//*********************************************************************
__attribute__((interrupt)) void rtc_wake_up_timer_irq(void)
{
    // TODO for enabling wakeup timer: set prescaler
    // set WU register, enable the wakeup interrupt
    // Also need to disable the alarmA interrupt (in set subseconda)
    // WUCKSEL = RTC/2
    // WUTIE = 1
    // 
}

//*********************************************************************
/**
 * @brief This function handles RTC interrupts.
 */
//*********************************************************************
__attribute__((interrupt)) void rtc_tamp_handler(void)
{
    struct sr sr = rtc.sr;
    if (sr.alraf)
    {
        // Clear the ALARM A flag
        rtc.scr.calraf = 1;

        // Call the main timer interrupt handler.
        timer_irq_task();
    }

    if (sr.alrbf)
    {
        // Clear the ALARM B flag
        rtc.scr.calrbf = 1;
        rtc_alarmb_irq();
    }

    if (sr.wutf)
    {
        // Clear the flag
        rtc.scr.cwutf = 1;
        rtc_wake_up_timer_irq();
    }
}

void rtc_enable_wakeup_timer(uint16_t const setting)
{
    struct cr cr     = rtc.cr;
    cr.wucksel = 3;  // RTC/2
    cr.wute = 1;
    cr.wutie = 1;
    
    rtc_write_protection_disable();
    rtc.cr = cr;
    rtc.wutr.wut = setting;
    rtc_write_protection_enable();    
}

