/**
 * @file hal_usart.c
 * @brief Implement the USART HAL functions
 */
#include "hal_usart.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

/**
 * @brief USART control register 1
 */
struct cr1
{
    unsigned ue     : 1; /**< USART enable */
    unsigned uesm   : 1; /**< USART enable in low-power mode*/
    unsigned re     : 1; /**< Receiver enable */
    unsigned te     : 1; /**< Transmitter enable */
    unsigned idleie : 1; /**< IDLE interrupt enable */
    unsigned rxneie : 1; /**< Receive data register not empty */
    unsigned tcie   : 1; /**< USART transmission complete interrupt enable/disable */
    unsigned txeie  : 1; /**< Transmit data register empty */
    unsigned peie   : 1; /**< PE interrupt enable*/
    unsigned ps     : 1; /**< Parity selection */
    unsigned pce    : 1; /**< Parity control enable */
    unsigned wake   : 1; /**< Receiver wakeup method */
    unsigned m0     : 1; /**< USART word length (with m1) */
    unsigned mme    : 1; /**< Mute mode enable */
    unsigned cmie   : 1; /**< Character match interrupt enable */
    unsigned over8  : 1; /**< Oversampling mode */
    unsigned dedt   : 5; /**< Driver Enable deassertion time */
    unsigned deat   : 5; /**< Driver Enable assertion time */
    unsigned rtoie  : 1; /**< Receiver timeout interrupt enable */
    unsigned eobie  : 1; /**< End of Block interrupt enable */
    unsigned m1     : 1; /**< USART USART word length (with m) */
    unsigned fifoen : 1; /**< FIFO mode enable */
    unsigned        : 2;
};

/**
 * @brief USART control register 2
 */
struct cr2
{
    unsigned slven    : 1; /**< Synchronous Slave mode enable */
    unsigned          : 2;
    unsigned dis_nss  : 1; /**< Disable NSS pin */
    unsigned addm7    : 1; /**< 7-bit Address Detection/4-bit Address Detection */
    unsigned lbdl     : 1; /**< LIN break detection length*/
    unsigned lbdie    : 1; /**< LIN break detection interrupt enable */
    unsigned          : 1;
    unsigned lbcl     : 1; /**< Last bit clock pulse*/
    unsigned cpha     : 1; /**< Clock phase */
    unsigned cpol     : 1; /**< Clock polarity*/
    unsigned clken    : 1; /**< Clock enable*/
    unsigned stop     : 2; /**< stop bits */
    unsigned linen    : 1; /**< LIN mode enable */
    unsigned swap     : 1; /**< Swap TX/RX pins */
    unsigned rxinv    : 1; /**< RX pin active level inversion */
    unsigned txinv    : 1; /**< TX pin active level inversion */
    unsigned datainv  : 1; /**< Binary data inversion*/
    unsigned msbfirst : 1; /**< Most significant bit first */
    unsigned abren    : 1; /**< Auto baud rate enable */
    unsigned abrmod   : 2; /**< Auto baud rate mode */
    unsigned rtoen    : 1; /**< Receiver timeout enable */
    unsigned add      : 8; /**< Address of the USART node */
};

/**
 * @brief USART control register 3
 */
struct cr3
{
    unsigned eie     : 1; /**< Error interrupt enable */
    unsigned iren    : 1; /**< IrDA mode enable */
    unsigned irlp    : 1; /**< IrDA low-power */
    unsigned hdsel   : 1; /**< Half-duplex selection*/
    unsigned nack    : 1; /**< Smartcard NACK enable */
    unsigned scen    : 1; /**< Smartcard mode enable*/
    unsigned dmar    : 1; /**< DMA enable receiver */
    unsigned dmat    : 1; /**< DMA enable transmitter */
    unsigned rtse    : 1; /**< RTS enable*/
    unsigned ctse    : 1; /**< CTS enable */
    unsigned ctsie   : 1; /**< CTS interrupt enable */
    unsigned onebit  : 1; /**< One sample bit method enable */
    unsigned ovrdis  : 1; /**< Overrun Disable */
    unsigned ddre    : 1; /**< DMA Disable on Reception Error */
    unsigned dem     : 1; /**< Driver enable mode */
    unsigned dep     : 1; /**< Driver enable polarity selection */
    unsigned         : 1;
    unsigned scarcnt : 3; /**<Smartcard auto-retry count */
    unsigned wus     : 2; /**< Wakeup from low-power mode interrupt flag selection */
    unsigned wufie   : 1; /**< Wakeup from low-power mode interrupt enable */
    unsigned txftie  : 1; /**< TXFIFO threshold interrupt enable */
    unsigned tcbgtie : 1; /**< Transmission Complete before guard time, interrupt enable */
    unsigned rxftcfg : 3; /**< Receive FIFO threshold configuration */
    unsigned rxftie  : 1; /**< RXFIFO threshold interrupt enable*/
    unsigned txftcfg : 3; /**< TXFIFO threshold configuration */
};

/**
 * @brief USART baudrate register
 */
struct brr
{
    unsigned brr : 16; /**< USART baud rate */
    unsigned     : 16;
};

/**
 * @brief USART guard time and prescaler register
 */
struct gtpr
{
    unsigned psc : 8; /**< USART prescaler (used only in IrDA and Smartcard mode) */
    unsigned gt  : 8; /**< Guard time value (used only in Smartcard mode) */
    unsigned     : 16;
};

/**
 * @brief USART guard time and prescaler register
 */
struct rtor
{
    unsigned rto  : 24; /**< Receiver timeout value */
    unsigned blen : 8;  /**< Block length (Smartcard mode) */
};

/**
 * @brief USART request register
 */
struct rqr
{
    unsigned abrrq : 1; /**< Auto baud rate request*/
    unsigned sbkrq : 1; /**< Send break request*/
    unsigned mmrq  : 1; /**< Mute mode request*/
    unsigned rxfrq : 1; /**< Receive data flush request */
    unsigned txfrq : 1; /**< Transmit data flush request*/
    unsigned       : 27;
};

/**
 * @brief USART interrupt and status register
 */
struct isr
{
    unsigned pe    : 1; /**< Parity error */
    unsigned fe    : 1; /**< Framing error */
    unsigned ne    : 1; /**< Noise detection flag */
    unsigned ore   : 1; /**< Overrun error */
    unsigned idle  : 1; /**< Idle line detected */
    unsigned rxne  : 1; /**< RXFIFO not empty */
    unsigned tc    : 1; /**< Transmission complete */
    unsigned txe   : 1; /**< TXFIFO not full */
    unsigned lbdf  : 1; /**< LIN break detection flag */
    unsigned ctsif : 1; /**< CTS interrupt flag */
    unsigned cts   : 1; /**< CTS flag */
    unsigned rtof  : 1; /**< Receiver timeout */
    unsigned eobf  : 1; /**< End of block flag */
    unsigned udr   : 1; /**< SPI slave underrun error flag */
    unsigned abre  : 1; /**< Auto baud rate error*/
    unsigned abrf  : 1; /**< Auto baud rate flag */
    unsigned busy  : 1; /**< Busy flag */
    unsigned cmf   : 1; /**< Character match flag */
    unsigned sbkf  : 1; /**< Send break flag */
    unsigned rwu   : 1; /**< Receiver wakeup from Mute mode */
    unsigned wuf   : 1; /**< Wakeup from low-power mode flag */
    unsigned teack : 1; /**< Transmit enable acknowledge flag */
    unsigned reack : 1; /**< Receive enable acknowledge flag */
    unsigned       : 2;
    unsigned tcbgt : 1; /**< Transmission complete before guard time flag */
    unsigned       : 6;
};

/**
 * @brief USART interrupt flag clear register
 */
struct icr
{
    unsigned pecf    : 1; /**< Parity error */
    unsigned fecf    : 1; /**< Framing error */
    unsigned necf    : 1; /**< Noise detection flag */
    unsigned orecf   : 1; /**< Overrun error */
    unsigned idlecf  : 1; /**< Idle line detected */
    unsigned txfecf  : 1; /**< RXFIFO not empty */
    unsigned tccf    : 1; /**< Transmission complete */
    unsigned tcbgtcf : 1; /**< TXFIFO not full */
    unsigned lbdcf   : 1; /**< LIN break detection flag */
    unsigned ctscf   : 1; /**< CTS interrupt flag */
    unsigned         : 1; /**< CTS flag */
    unsigned rtocf   : 1; /**< Receiver timeout */
    unsigned eobcf   : 1; /**< End of block flag */
    unsigned udrcf   : 1; /**< SPI slave underrun error flag */
    unsigned         : 3;
    unsigned cmcf    : 1; /**< Character match flag */
    unsigned         : 2;
    unsigned wufcf   : 1; /**< Wakeup from low-power mode flag */
    unsigned         : 11;
};

/**
 * @brief USART receive data register
 */
struct rdr
{
    unsigned rdr : 8; /**< Receive data value*/
    unsigned     : 24;
};

/**
 * @brief USART transmit data register
 */
struct tdr
{
    unsigned tdr : 8; /**< Transmit data value*/
    unsigned     : 24;
};

/**
 * @brief USART prescaler register
 */
struct presc
{
    unsigned prescaler : 4; /**< Clock prescaler */
    unsigned           : 28;
};

/**
 * @brief USART registers
 */
struct
{
    struct cr1 cr1;     /**< USART control register 1 */
    struct cr2 cr2;     /**< USART control register 2 */
    struct cr3 cr3;     /**< USART control register 3 */
    struct brr brr;     /**< USART baud rate register */
    struct gtpr gtpr;   /**< USART guard time and prescaler register */
    struct rtor rtor;   /**< USART receiver timeout register */
    struct rqr rqr;     /**< USART request register */
    struct isr isr;     /**< USART interrupt and status register */
    struct icr icr;     /**< USART interrupt flag clear register */
    struct rdr rdr;     /**< USART receive data register */
    struct tdr tdr;     /**< USART transmit data register */
    struct presc presc; /**< USART prescaler register */
} volatile usart1 __attribute__((section(".bss.usart1")));

/**
 * @brief This needs to be set based on the clock speed set in the RCC
 */
#define USART_PERIPHERAL_CLOCK 16000000

void usart_init(void)
{
}

void usart_enable(void)
{
    usart1.cr1.ue = 1;
}

void usart_set_word_length(usart_word_len const len)
{
    struct cr1 cr1 = usart1.cr1;
    cr1.m0         = (unsigned)len;
    cr1.m1         = (unsigned)len >> 1;
    usart1.cr1     = cr1;
}

void usart_set_parity(usart_bit_cfg const parity)
{
    usart1.cr1.pce = parity;
}

void usart_tx_enable(void)
{
    usart1.cr1.te = 1;
}

void usart_tx_disable(void)
{
    usart1.cr1.te = 0;
}

void usart_rx_enable(void)
{
    usart1.cr1.re = 1;
}

void usart_rx_disable(void)
{
    usart1.cr1.re = 0;
}

void usart_set_baudrate(uint32_t const baud)
{
    // Calculate the divider in 16x oversampling mode.
    usart1.brr.brr = ((uint32_t)USART_PERIPHERAL_CLOCK) / baud;
}

void usart_tx_empty_int_enable(void)
{
    usart1.cr1.txeie = 1;
}

void usart_tx_empty_int_disable(void)
{
    usart1.cr1.txeie = 0;
}

void usart_rx_buffer_full_int_enable(void)
{
    usart1.cr1.rxneie = 1;
}

void usart_rx_buffer_full_int_disable(void)
{
    usart1.cr1.rxneie = 0;
}

void usart_set_stop_bits(usart_stop_bits const bits)
{
    usart1.cr2.stop = bits;
}

void usart_set_clock_prescale(usart_prescale_factor const factor)
{
    usart1.presc.prescaler = factor;
}

void usart_write_tx(uint8_t const data)
{
    usart1.tdr.tdr = data;
}

uint8_t usart_read_byte(void)
{
    return usart1.rdr.rdr;
}

bool usart_is_rx_buffer_full(void)
{
    return usart1.isr.rxne;
}

bool usart_is_rcv_interrupt_enabled(void)
{
    return usart1.cr1.rxneie;
}

bool usart_is_tx_buffer_empty(void)
{
    return usart1.isr.txe;
}

bool usart_is_tx_interrupt_enabled(void)
{
    return usart1.cr1.txeie;
}
