//*********************************************************************
/*! \file hal_rcc.c
 *
 * \brief Kidde: Mongoose-fw - Implement the RCC HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************
#include "hal_rcc.h"
#include "hal_pwr.h"
#include <arm_acle.h> // For __nop()

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

//*********************************************************************
/**
 * \def TICKS_TIMEOUT
 * \brief Used to timeout of wait loops
 */
//*********************************************************************
#define TICKS_TIMEOUT      255

//*********************************************************************
/**
 * \def XTAL_TICKS_TIMEOUT
 * \brief Maximum amount of time allowed to stabilize the crystal before declaring initialization failure
 * \note Estimates to about 700ms while the average time to stabilize is about 200ms with the high drive capability
 */
//*********************************************************************
#define XTAL_TICKS_TIMEOUT 800000

//*********************************************************************
/**
 * \brief A memory mapped type for the CR, RCC control register
 */
//*********************************************************************
struct cr
{
    unsigned          : 8;
    unsigned hsion    : 1; ///< HSI16 clock calibration
    unsigned hsikeron : 1; ///< HSI16 always enabled for peripheral kernels even in Stop modes
    unsigned hsirdy   : 1; ///< HSI16 clock ready flag
    unsigned hsidiv   : 3; ///< HSI16 clock division factor
    unsigned          : 2;
    unsigned hseon    : 1; ///< HSE clock enable
    unsigned hserdy   : 1; ///< HSE clock ready flag
    unsigned hsebyp   : 1; ///< HSE crystal oscillator bypass
    unsigned csson    : 1; ///< Clock security system enable
    unsigned          : 4;
    unsigned pllon    : 1; ///< PLL enable
    unsigned pllrdy   : 1; ///< PLL clock ready flag
    unsigned          : 6;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the ICSCR, RCC internal clock calibration register
 */
//*********************************************************************
struct icscr
{
    unsigned hsical  : 8; ///< HSI16 clock calibration
    unsigned hsitrim : 7; ///< HSI16 clock trimming
    unsigned         : 17;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CFGR, RCC clock configuration register
 */
//*********************************************************************
struct cfgr
{
    unsigned sw     : 3; ///< System clock switch
    unsigned sws    : 3; ///< System clock switch status
    unsigned        : 2;
    unsigned hpre   : 4; ///< AHB prescaler
    unsigned ppre   : 3; ///< APB prescaler
    unsigned        : 9;
    unsigned mcosel : 4; ///< Microcontroller clock output clock selector
    unsigned mcopre : 4; ///< Microcontroller clock output prescaler
};

//*********************************************************************
/**
 * \brief A memory mapped type for the PLLCFGR, RCC PLL configuration register
 */
//*********************************************************************
struct pllcfgr
{
    unsigned pllscr : 2; ///< PLL input clock source
    unsigned        : 2;
    unsigned pllm   : 3; ///< Division factor M of the PLL input clock divider
    unsigned        : 1;
    unsigned plln   : 7; ///< PLL frequency multiplication factor N
    unsigned        : 1;
    unsigned pllpen : 1; ///< PLLPCLK clock output enable
    unsigned pllp   : 5; ///< PLL VCO division factor P for PLLPCLK clock output
    unsigned        : 2;
    unsigned pllqen : 1; ///< PLLQCLK clock output enable
    unsigned pllq   : 3; ///< PLL VCO division factor Q for PLLQCLK clock output
    unsigned pllren : 1; ///< PLLRCLK clock output enable
    unsigned pllr   : 3; ///< PLL VCO division factor R for PLLRCLK clock output
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CIER, RCC clock interrupt enable register
 */
//*********************************************************************
struct cier
{
    unsigned lsirdyie : 1; ///< LSI ready interrupt enable
    unsigned lserdyie : 1; ///< LSE ready interrupt enable
    unsigned          : 1;
    unsigned hsirdyie : 1; ///< HSI16 ready interrupt enable
    unsigned hserdyie : 1; ///< HSE ready interrupt enable
    unsigned pllrdyie : 1; ///< PLL ready interrupt enable
    unsigned          : 26;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CIFR, RCC clock interrupt flag register
 */
//*********************************************************************
struct cifr
{
    unsigned lsirdyf : 1; ///< LSI ready interrupt flag
    unsigned lserdyf : 1; ///< LSE ready interrupt flag
    unsigned         : 1;
    unsigned hsirdyf : 1; ///< HSI16 ready interrupt flag
    unsigned hserdyf : 1; ///< HSE ready interrupt flag
    unsigned pllrdyf : 1; ///< PLL ready interrupt flag
    unsigned         : 2;
    unsigned cssf    : 1; ///< HSE clock security system interrupt flag
    unsigned lsecssf : 1; ///< LSE clock security system interrupt flag
    unsigned         : 22;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CICR, RCC clock interrupt clear register
 */
//*********************************************************************
struct cicr
{
    unsigned lsirdyc : 1; ///< LSI ready interrupt clear
    unsigned lserdyc : 1; ///< LSE ready interrupt clear
    unsigned         : 1;
    unsigned hsirdyc : 1; ///< HSI16 ready interrupt clear
    unsigned hserdyc : 1; ///< HSE ready interrupt clear
    unsigned pllrdyc : 1; ///< PLL ready interrupt clear
    unsigned         : 2;
    unsigned cssc    : 1; ///< Clock security system interrupt clear
    unsigned lsecssc : 1; ///< LSE Clock security system interrupt clear
    unsigned         : 22;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the IOPRSTR, RCC I/O port reset register
 */
//*********************************************************************
struct ioprstr
{
    unsigned gpioarst : 1; ///< I/O port A reset
    unsigned gpiobrst : 1; ///< I/O port B reset
    unsigned gpiocrst : 1; ///< I/O port C reset
    unsigned gpiodrst : 1; ///< I/O port D reset
    unsigned          : 1;
    unsigned gpiofrst : 1; ///< I/O port F reset
    unsigned          : 26;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the AHBRSTR, RCC AHB peripheral reset register
 */
//*********************************************************************
struct ahbrstr
{
    unsigned dma1rst  : 1; ///< DMA1 and DMAMUX reset
    unsigned          : 7;
    unsigned flashrst : 1; ///< Flash memory interface reset
    unsigned          : 3;
    unsigned crcrst   : 1; ///< CRC reset
    unsigned          : 19;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the APBRSTR1, RCC APB peripheral reset register 1
 */
//*********************************************************************
struct apbrstr1
{
    unsigned tim2rst    : 1; ///< TIM2 timer reset
    unsigned tim3rst    : 1; ///< TIM3 timer reset
    unsigned            : 12;
    unsigned spi2rst    : 1; ///< SPI2 reset
    unsigned            : 2;
    unsigned usart2rst  : 1; ///< USART2 reset
    unsigned            : 2;
    unsigned lpuart1rst : 1; ///< LPUART1 reset
    unsigned i2c1rst    : 1; ///< I2C1 reset
    unsigned i2c2rst    : 1; ///< I2C2 reset
    unsigned            : 4;
    unsigned dbgrst     : 1; ///< Debug support reset
    unsigned pwrrst     : 1; ///< Power interface reset
    unsigned            : 1;
    unsigned lptim2rst  : 1; ///< Low Power Timer 1 reset
    unsigned lptim1rst  : 1; ///< Low Power Timer 2 reset
};

//*********************************************************************
/**
 * \brief A memory mapped type for the APBRSTR2, RCC APB peripheral reset register 2
 */
//*********************************************************************
struct apbrstr2
{
    unsigned syscfgrst : 1; ///< SYSCFG reset
    unsigned           : 10;
    unsigned tim1rst   : 1; ///< TIM1 timer reset
    unsigned spi1rst   : 1; ///< SPI1 reset
    unsigned           : 1;
    unsigned usart1rst : 1; ///< USART1 reset
    unsigned tim14rst  : 1; ///< TIM14 timer reset
    unsigned           : 1;
    unsigned tim16rst  : 1; ///< TIM16 timer reset
    unsigned tim17rst  : 1; ///< TIM17 timer reset
    unsigned           : 1;
    unsigned adcrst    : 1; ///< ADC reset
    unsigned           : 11;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the IOPENR, RCC I/O port clock enable register
 */
//*********************************************************************
struct iopenr
{
    unsigned gpioaen : 1; ///< I/O port A clock enable
    unsigned gpioben : 1; ///< I/O port B clock enable
    unsigned gpiocen : 1; ///< I/O port C clock enable
    unsigned gpioden : 1; ///< I/O port D clock enable
    unsigned         : 1;
    unsigned gpiofen : 1; ///< I/O port F clock enable
    unsigned         : 26;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the AHBENR, RCC AHB peripheral clock enable register
 */
//*********************************************************************
struct ahbenr
{
    unsigned dma1en  : 1; ///< DMA1 and DMAMUX clock enable
    unsigned         : 7;
    unsigned flashen : 1; ///< Flash memory interface clock enable
    unsigned         : 3;
    unsigned crcen   : 1; ///< CRC clock enable
    unsigned         : 19;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the APBENR1, RCC APB peripheral clock enable register 1
 */
//*********************************************************************
struct apbenr1
{
    unsigned tim2en    : 1; ///< TIM2 timer clock enable
    unsigned tim3en    : 1; ///< TIM3 timer clock enable
    unsigned           : 8;
    unsigned rtcapben  : 1; ///< RTC APB clock enable
    unsigned wwdgen    : 1; ///< WWDG clock enable
    unsigned           : 2;
    unsigned spi2en    : 1; ///< SPI2 clock enable
    unsigned           : 2;
    unsigned usart2en  : 1; ///< USART2 clock enable
    unsigned           : 2;
    unsigned lpuart1en : 1; ///< LPUART1 clock enable
    unsigned i2c1en    : 1; ///< I2C1 clock enable
    unsigned i2c2en    : 1; ///< I2C2 clock enable
    unsigned           : 4;
    unsigned dbgen     : 1; ///< Debug support clock enable
    unsigned pwren     : 1; ///< Power interface clock enable
    unsigned           : 1;
    unsigned lptim2en  : 1; ///< LPTIM2 clock enable
    unsigned lptim1en  : 1; ///< LPTIM1 clock enable
};

//*********************************************************************
/**
 * \brief A memory mapped type for the APBENR2, RCC APB peripheral clock enable register 2
 */
//*********************************************************************
struct apbenr2
{
    unsigned syscfgen : 1; ///< SYSCFG clock enable
    unsigned          : 10;
    unsigned tim1en   : 1; ///< TIM1 timer clock enable
    unsigned spi1en   : 1; ///< SPI1 clock enable
    unsigned          : 1;
    unsigned usart1en : 1; ///< USART1 clock enable
    unsigned tim14en  : 1; ///< TIM14 timer clock enable
    unsigned          : 1;
    unsigned tim16en  : 1; ///< TIM16 timer clock enable
    unsigned tim17en  : 1; ///< TIM17 timer clock enable
    unsigned          : 1;
    unsigned adcen    : 1; ///< ADC clock enable
    unsigned          : 11;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the IOPSMENR, RCC I/O port in Sleep mode clock enable register
 */
//*********************************************************************
struct iopsmenr
{
    unsigned gpioasmen : 1; ///< I/O port A clock enable during Sleep mode
    unsigned gpiobsmen : 1; ///< I/O port B clock enable during Sleep mode
    unsigned gpiocsmen : 1; ///< I/O port C clock enable during Sleep mode
    unsigned gpiodsmen : 1; ///< I/O port D clock enable during Sleep mode
    unsigned           : 1;
    unsigned gpiofsmen : 1; ///< I/O port F clock enable during Sleep mode
    unsigned           : 26;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the AHBSMENR, RCC AHB peripheral clock enable in Sleep/Stop mode register
 */
//*********************************************************************
struct ahbsmenr
{
    unsigned dma1smen  : 1; ///< DMA1 and DMAMUX clock enable during Sleep mode
    unsigned           : 7;
    unsigned flashsmen : 1; ///< Flash memory interface clock enable during Sleep mode
    unsigned sramsmen  : 1; ///< SRAM clock enable during Sleep mode
    unsigned           : 2;
    unsigned crcsmen   : 1; ///< CRC clock enable during Sleep mode
    unsigned           : 19;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the APBSMENR1, RCC APB peripheral clock enable in Sleep/Stop mode register 1
 */
//*********************************************************************
struct apbsmenr1
{
    unsigned tim2smen    : 1; ///< TIM2 timer clock enable during Sleep mode
    unsigned tim3smen    : 1; ///< TIM3 timer clock enable during Sleep mode
    unsigned             : 8;
    unsigned rtcapbsmen  : 1; ///< RTC APB clock enable during Sleep mode
    unsigned wwdgsmen    : 1; ///< WWDG clock enable during Sleep and Stop modes
    unsigned             : 2;
    unsigned spi2smen    : 1; ///< SPI2 clock enable during Sleep mode
    unsigned             : 2;
    unsigned usart2smen  : 1; ///< USART2 clock enable during Sleep and Stop modes
    unsigned             : 2;
    unsigned lpuart1smen : 1; ///< LPUART1 clock enable during Sleep and Stop modes
    unsigned i2c1smen    : 1; ///< I2C1 clock enable during Sleep and Stop modes
    unsigned i2c2smen    : 1; ///< I2C2 clock enable during Sleep mode
    unsigned             : 4;
    unsigned dbgsmen     : 1; ///< Debug support clock enable during Sleep mode
    unsigned pwrsmen     : 1; ///< Power interface clock enable during Sleep mode
    unsigned             : 1;
    unsigned lptim2smen  : 1; ///< Low Power Timer 2 clock enable during Sleep and Stop modes
    unsigned lptim1smen  : 1; ///< Low Power Timer 1 clock enable during Sleep and Stop modes
};

//*********************************************************************
/**
 * \brief A memory mapped type for the APBSMENR2, RCC APB peripheral clock enable in Sleep/Stop mode register 2
 */
//*********************************************************************
struct apbsmenr2
{
    unsigned syscfgsmen : 1; ///< SYSCFG clock enable during Sleep and Stop modes
    unsigned            : 10;
    unsigned tim1smen   : 1; ///< TIM1 timer clock enable during Sleep mode
    unsigned spi1smen   : 1; ///< SPI1 clock enable during Sleep mode
    unsigned            : 1;
    unsigned usart1smen : 1; ///< USART1 clock enable during Sleep and Stop modes
    unsigned tim14smen  : 1; ///< TIM14 timer clock enable during Sleep mode
    unsigned            : 1;
    unsigned tim16smen  : 1; ///< TIM16 timer clock enable during Sleep mode
    unsigned tim17smen  : 1; ///< TIM17 timer clock enable during Sleep mode
    unsigned            : 1;
    unsigned adcsmen    : 1; ///< ADC clock enable during Sleep mode
    unsigned            : 11;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCIPR, RCC peripherals independent clock configuration register
 */
//*********************************************************************
struct ccipr
{
    unsigned usart1sel   : 2; ///< USART1 clock source selection
    unsigned             : 8;
    unsigned lpuart1sel  : 2; ///< LPUART1 clock source selection
    unsigned i2c1sel     : 2; ///< I2C1 clock source selection
    unsigned i2c2i2s1sel : 2; ///< I2C2/I2S1 clock source selection
    unsigned             : 2;
    unsigned lptim1sel   : 2; ///< LPTIM1 clock source selection
    unsigned lptim2sel   : 2; ///< LPTIM2 clock source selection
    unsigned tim1sel     : 1; ///< TIM1 clock source selection
    unsigned             : 7;
    unsigned adcsel      : 2; ///< ADCs clock source selection
};

//*********************************************************************
/**
 * \brief A memory mapped type for the BDCR, RCC domain control register
 */
//*********************************************************************
struct bdcr
{
    unsigned lseon    : 1; ///< LSE oscillator enable
    unsigned lserdy   : 1; ///< LSE oscillator ready
    unsigned lsebyp   : 1; ///< LSE oscillator bypass
    unsigned lsedrv   : 2; ///< LSE oscillator drive capability
    unsigned lsecsson : 1; ///< CSS on LSE enable
    unsigned lsecssd  : 1; ///< CSS on LSE failure Detection
    unsigned          : 1;
    unsigned rtcsel   : 2; ///< RTC clock source selection
    unsigned          : 5;
    unsigned rtcen    : 1; ///< RTC clock enable
    unsigned bdrst    : 1; ///< RTC domain software reset
    unsigned          : 7;
    unsigned lscoen   : 1; ///< Low-speed clock output (LSCO) enable
    unsigned lscosel  : 1; ///< Low-speed clock output selection
    unsigned          : 6;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CSR, RCC control/status register
 */
//*********************************************************************
union csr
{
    struct
    {
        unsigned lsion    : 1; ///< LSI oscillator enable
        unsigned lsirdy   : 1; ///< LSI oscillator ready
        unsigned          : 21;
        unsigned rmvf     : 1; ///< Remove reset flags
        unsigned          : 1;
        unsigned oblrstf  : 1; ///< Option byte loader reset flag
        unsigned pinrstf  : 1; ///< Pin reset flag
        unsigned pwrrstf  : 1; ///< POR/PDR flag
        unsigned sftrstf  : 1; ///< Software reset flag
        unsigned iwdgrstf : 1; ///< Independent window watchdog reset flag
        unsigned wwdgrstf : 1; ///< Window watchdog reset flag
        unsigned lpwrrstf : 1; ///< Low-power reset flag
    };

    unsigned word;
};
//*********************************************************************
/**
 * \brief A memory mapped type for the RCC control registers
 */
//*********************************************************************
struct
{
    struct cr cr;
    struct icscr icscr;
    struct cfgr cfgr;
    struct pllcfgr pllcfgr;
    unsigned unused1[2];
    struct cier cier;
    struct cifr cifr;
    struct cicr cicr;
    struct ioprstr ioprstr;
    struct ahbrstr ahbrstr;
    struct apbrstr1 apbrstr1;
    struct apbrstr2 apbrstr2;
    struct iopenr iopenr;
    struct ahbenr ahbenr;
    struct apbenr1 apbenr1;
    struct apbenr2 apbenr2;
    struct iopsmenr iopsmenr;
    struct ahbsmenr ahbsmenr;
    struct apbsmenr1 apbsmenr1;
    struct apbsmenr2 apbsmenr2;
    struct ccipr ccipr;
    unsigned unused2[1];
    struct bdcr bdcr;
    union csr csr;
} volatile rcc __attribute__((section(".bss.rcc")));

bool rcc_hsi16_clock_source_control(bool const enable, bool const on_in_stop)
{
    struct cr cr = rcc.cr;
    cr.hsikeron  = on_in_stop;
    cr.hsion     = enable;
    rcc.cr       = cr;

    // Wait for clock ready flag
    for (uint8_t ticks = TICKS_TIMEOUT; rcc.cr.hsirdy != enable && ticks; --ticks)
        ;

    return rcc.cr.hsirdy == enable;
}

bool rcc_hse_clock_source_control(bool const enable, bool const bypass)
{
    struct cr cr = rcc.cr;
    cr.hsebyp    = bypass;
    cr.hseon     = enable;
    rcc.cr       = cr;

    // Wait for clock ready flag
    for (uint8_t ticks = TICKS_TIMEOUT; rcc.cr.hserdy != enable && ticks; --ticks)
        ;

    return rcc.cr.hserdy == enable;
}

void rcc_set_hse_clock_security(void)
{
    rcc.cr.csson = 1;
}

void rcc_clear_hse_clock_security_interrupt(void)
{
    rcc.cicr.cssc = 1;
}

bool rcc_lsi_clock_source_control(bool const enable)
{
    union csr csr = rcc.csr;
    __nop(); // Ensure at least 3 wait states before writing to RCC_CSR
    __nop();
    csr.lsion = enable;
    rcc.csr   = csr;

    // Wait for clock ready flag
    for (uint8_t ticks = TICKS_TIMEOUT; rcc.csr.lsirdy != enable && ticks; --ticks)
        ;

    return rcc.csr.lsirdy == enable;
}

bool rcc_lse_clock_source_control(bool const enable, bool const bypass, lse_drive_capability const drive)
{
    // Enable access to RTC domain
    pwr_enable_rtc_access();

    // Clear the reset flag before writing to BDCR
    struct bdcr bdcr = rcc.bdcr;
    if (bdcr.bdrst)
    {
        bdcr.bdrst = 0;
        rcc.bdcr   = bdcr;
    }

    // LSE bypass can be written only when the external 32 kHz oscillator is disabled
    if (bdcr.lsebyp != bypass && (bdcr.lseon || bdcr.lserdy))
    {
        bdcr.lseon = 0;
        rcc.bdcr   = bdcr;

        for (uint8_t ticks = TICKS_TIMEOUT; rcc.bdcr.lserdy && ticks; --ticks)
            ;
        if (rcc.bdcr.lserdy)
        {
            return false;
        }
    }

    bdcr = rcc.bdcr;
    __nop(); // Ensure at least 3 wait states before writing to RCC_BDCR
    __nop();
    bdcr.lsebyp = bypass;
    bdcr.lsedrv = drive;
    bdcr.lseon  = enable;
    rcc.bdcr    = bdcr;
    __nop(); // Ensure at least 3 wait states before writing to RCC_BDCR
    __nop();

    // Wait for clock ready flag
    for (uint32_t ticks = XTAL_TICKS_TIMEOUT; rcc.bdcr.lserdy != enable && ticks; --ticks)
        ;

    return rcc.bdcr.lserdy == enable;
}

void rcc_set_lse_clock_security(void)
{
    struct bdcr bdcr = rcc.bdcr;
    union csr csr    = rcc.csr;
    if (bdcr.lseon && bdcr.lserdy && csr.lsion && csr.lsirdy && bdcr.rtcsel)
    {
        bdcr.lsecsson = 1;
        rcc.bdcr      = bdcr;
    }
}

void rcc_clear_lse_clock_security_interrupt(void)
{
    rcc.cicr.lsecssc = 1;
}

void rcc_configure_hsi_division_factor(hsi_divide const factor)
{
    rcc.cr.hsidiv = factor;
}

void rcc_select_system_clock(system_clock const clock)
{
    rcc.cfgr.sw = clock;
}

void rcc_configure_sysclk_division_factor(sysclk_divide const factor)
{
    rcc.cfgr.hpre = factor;
}

void rcc_configure_hclk_division_factor(hclk_divide const factor)
{
    rcc.cfgr.ppre = factor;
}

void rcc_enable_rtc_clock(rtc_clock const clock)
{
    // Enable access to RTC domain
    pwr_enable_rtc_access();

    // Reset the RTC domain if a different clock source is already selected
    struct bdcr bdcr = rcc.bdcr;
    if (bdcr.rtcsel != clock)
    {
        bdcr.bdrst = 1;
        rcc.bdcr   = bdcr;
        __nop(); // Ensure at least 3 wait states before next write
        __nop();
        bdcr.bdrst = 0;
        rcc.bdcr   = bdcr;
        __nop(); // Ensure at least 3 wait states before next write
        bdcr.rtcsel = clock;
    }
    bdcr.rtcen = 1;
    rcc.bdcr   = bdcr;
}

void rcc_enable_tim1_clock(bool const on_in_sleep)
{
    rcc.apbenr2.tim1en     = 1;
    rcc.apbsmenr2.tim1smen = on_in_sleep;
}

void rcc_enable_tim2_clock(bool const on_in_sleep)
{
    rcc.apbenr1.tim2en     = 1;
    rcc.apbsmenr1.tim2smen = on_in_sleep;
}

void rcc_enable_tim3_clock(bool const on_in_sleep)
{
    rcc.apbenr1.tim3en     = 1;
    rcc.apbsmenr1.tim3smen = on_in_sleep;
}

void rcc_enable_tim14_clock(bool const on_in_sleep)
{
    rcc.apbenr2.tim14en     = 1;
    rcc.apbsmenr2.tim14smen = on_in_sleep;
}

void rcc_enable_tim16_clock(bool const on_in_sleep)
{
    rcc.apbenr2.tim16en     = 1;
    rcc.apbsmenr2.tim16smen = on_in_sleep;
}

void rcc_enable_tim17_clock(bool const on_in_sleep)
{
    rcc.apbenr2.tim17en     = 1;
    rcc.apbsmenr2.tim17smen = on_in_sleep;
}

void rcc_enable_lptim1_clock(lptim_clock const clock, bool const on_in_stop)
{
    rcc.ccipr.lptim1sel      = clock;
    rcc.apbenr1.lptim1en     = 1;
    rcc.apbsmenr1.lptim1smen = on_in_stop;
}

void rcc_enable_lptim2_clock(lptim_clock const clock, bool const on_in_stop)
{
    rcc.ccipr.lptim2sel      = clock;
    rcc.apbenr1.lptim2en     = 1;
    rcc.apbsmenr1.lptim2smen = on_in_stop;
}

void rcc_enable_usart1_clock(uart_clock const clock, bool const on_in_stop)
{
    rcc.ccipr.usart1sel      = clock;
    rcc.apbenr2.usart1en     = 1;
    rcc.apbsmenr2.usart1smen = on_in_stop;
}

void rcc_enable_usart2_clock(bool const on_in_stop)
{
    rcc.apbenr1.usart2en     = 1;
    rcc.apbsmenr1.usart2smen = on_in_stop;
}

void rcc_enable_lpuart1_clock(lpuart_clock const clock, bool const on_in_stop)
{
    rcc.ccipr.lpuart1sel      = clock;
    rcc.apbenr1.lpuart1en     = 1;
    rcc.apbsmenr1.lpuart1smen = on_in_stop;
}

void rcc_enable_i2c1_clock(i2c_clock const clock, bool const on_in_stop)
{
    rcc.ccipr.i2c1sel      = clock;
    rcc.apbenr1.i2c1en     = 1;
    rcc.apbsmenr1.i2c1smen = on_in_stop;
}

void rcc_enable_i2c2_clock(bool const on_in_sleep)
{
    rcc.apbenr1.i2c2en     = 1;
    rcc.apbsmenr1.i2c2smen = on_in_sleep;
}

void rcc_enable_spi1_clock(i2s_clock const clock, bool const on_in_sleep)
{
    rcc.ccipr.i2c2i2s1sel  = clock;
    rcc.apbenr2.spi1en     = 1;
    rcc.apbsmenr2.spi1smen = on_in_sleep;
}

void rcc_enable_spi2_clock(bool const on_in_sleep)
{
    rcc.apbenr1.spi2en     = 1;
    rcc.apbsmenr1.spi2smen = on_in_sleep;
}

void rcc_enable_rtcapb_clock(bool const on_in_sleep)
{
    rcc.apbenr1.rtcapben     = 1;
    rcc.apbsmenr1.rtcapbsmen = on_in_sleep;
}

void rcc_enable_wwdg_clock(bool const on_in_stop)
{
    rcc.apbenr1.wwdgen     = 1;
    rcc.apbsmenr1.wwdgsmen = on_in_stop;
}

void rcc_enable_dbg_clock(bool const on_in_sleep)
{
    rcc.apbenr1.dbgen     = 1;
    rcc.apbsmenr1.dbgsmen = on_in_sleep;
}

void rcc_enable_pwr_clock(bool const on_in_sleep)
{
    rcc.apbenr1.pwren     = 1;
    rcc.apbsmenr1.pwrsmen = on_in_sleep;
}

void rcc_enable_syscfg_clock(bool const on_in_stop)
{
    rcc.apbenr2.syscfgen     = 1;
    rcc.apbsmenr2.syscfgsmen = on_in_stop;
}

void rcc_enable_adc_clock(adc_clock const clock, bool const on_in_sleep)
{
    rcc.ccipr.adcsel      = clock;
    rcc.apbenr2.adcen     = 1;
    rcc.apbsmenr2.adcsmen = on_in_sleep;
}

void rcc_enable_crc_clock(bool const on_in_sleep)
{
    rcc.ahbenr.crcen     = 1;
    rcc.ahbsmenr.crcsmen = on_in_sleep;
}

void rcc_disable_crc_clock(void)
{
    rcc.ahbenr.crcen = 0;
}

void rcc_enable_flash_clock(bool const on_in_sleep)
{
    rcc.ahbenr.flashen     = 1;
    rcc.ahbsmenr.flashsmen = on_in_sleep;
}

void rcc_enable_dma1_clock(bool const on_in_sleep)
{
    rcc.ahbenr.dma1en     = 1;
    rcc.ahbsmenr.dma1smen = on_in_sleep;
}

void rcc_enable_gpioa_clock(bool const on_in_sleep)
{
    rcc.iopenr.gpioaen     = 1;
    rcc.iopsmenr.gpioasmen = on_in_sleep;
}

void rcc_enable_gpiob_clock(bool const on_in_sleep)
{
    rcc.iopenr.gpioben     = 1;
    rcc.iopsmenr.gpiobsmen = on_in_sleep;
}

void rcc_enable_gpioc_clock(bool const on_in_sleep)
{
    rcc.iopenr.gpiocen     = 1;
    rcc.iopsmenr.gpiocsmen = on_in_sleep;
}

void rcc_enable_enable_gpiod_clock(bool const on_in_sleep)
{
    rcc.iopenr.gpioden     = 1;
    rcc.iopsmenr.gpiodsmen = on_in_sleep;
}

void rcc_enable_enable_gpiof_clock(bool const on_in_sleep)
{
    rcc.iopenr.gpiofen     = 1;
    rcc.iopsmenr.gpiofsmen = on_in_sleep;
}

uint8_t rcc_get_reset_flags(void)
{
    return rcc.csr.word >> 25;
}

void rcc_clear_reset_flags(void)
{
    // Set the bit to clear the reset flags.
    rcc.csr.rmvf = 1;
}
