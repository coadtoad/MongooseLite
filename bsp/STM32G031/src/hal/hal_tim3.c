//*********************************************************************
/*! \file hal_tim3.c
 *
 * \brief Kidde: Mongoose-fw - Implement the TIM3 HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************
#include "hal_tim3.h"
#include "hal_rcc.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

//*********************************************************************
/**
 * \brief A memory mapped type for the CR1, TIM3 control register 1
 */
//*********************************************************************
struct cr1
{
    unsigned cen      : 1; ///< Counter enable
    unsigned udis     : 1; ///< Update disable
    unsigned urs      : 1; ///< Update request source
    unsigned opm      : 1; ///< One-pulse mode
    unsigned dir      : 1; ///< Direction
    unsigned cms      : 2; ///< Center-aligned mode selection
    unsigned arpe     : 1; ///< Auto-reload preload enable
    unsigned ckd      : 2; ///< Clock division
    unsigned          : 1;
    unsigned uifremap : 1; ///< UIF status bit remapping
    unsigned          : 20;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CR2, TIM3 control register 2
 */
//*********************************************************************
struct cr2
{
    unsigned      : 3;
    unsigned ccds : 1; ///< Capture/compare DMA selection
    unsigned mms  : 3; ///< Master mode selection
    unsigned ti1s : 1; ///< TI1 selection
    unsigned      : 24;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the SMCR, TIM3 slave mode control register
 */
//*********************************************************************
struct smcr
{
    unsigned sms     : 3; ///< Slave mode selection, low bits [2:0]
    unsigned occs    : 1; ///< OCREF clear selection
    unsigned ts      : 3; ///< Trigger selection, low bits [2:0]
    unsigned msm     : 1; ///< Master/Slave mode
    unsigned etf     : 4; ///< External trigger filter
    unsigned etps    : 2; ///< External trigger prescaler
    unsigned ece     : 1; ///< External clock enable
    unsigned etp     : 1; ///< External trigger polarity
    unsigned sms_msb : 1; ///< Slave mode selection, high bit [3]
    unsigned         : 3;
    unsigned ts_msb  : 2; ///< Trigger selection, high bits [4:3]
    unsigned         : 10;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the DIER, TIM3 DMA/Interrupt enable register
 */
//*********************************************************************
struct dier
{
    unsigned uie   : 1; ///< Update interrupt enable
    unsigned cc1ie : 1; ///< Capture/Compare 1 interrupt enable
    unsigned cc2ie : 1; ///< Capture/Compare 2 interrupt enable
    unsigned cc3ie : 1; ///< Capture/Compare 3 interrupt enable
    unsigned cc4ie : 1; ///< Capture/Compare 4 interrupt enable
    unsigned       : 1;
    unsigned tie   : 1; ///< Trigger interrupt enable
    unsigned       : 1;
    unsigned ude   : 1; ///< Update DMA request enable
    unsigned cc1de : 1; ///< Capture/Compare 1 DMA request enable
    unsigned cc2de : 1; ///< Capture/Compare 2 DMA request enable
    unsigned cc3de : 1; ///< Capture/Compare 3 DMA request enable
    unsigned cc4de : 1; ///< Capture/Compare 4 DMA request enable
    unsigned       : 1;
    unsigned tde   : 1; ///< Trigger DMA request enable
    unsigned       : 17;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the SR, TIM3 status register
 */
//*********************************************************************
struct sr
{
    unsigned uif   : 1; ///< Update interrupt flag
    unsigned cc1if : 1; ///< Capture/Compare 1 interrupt flag
    unsigned cc2if : 1; ///< Capture/Compare 2 interrupt flag
    unsigned cc3if : 1; ///< Capture/Compare 3 interrupt flag
    unsigned cc4if : 1; ///< Capture/Compare 4 interrupt flag
    unsigned       : 1;
    unsigned tif   : 1; ///< Trigger interrupt flag
    unsigned       : 2;
    unsigned cc1of : 1; ///< Capture/Compare 1 overcapture flag
    unsigned cc2of : 1; ///< Capture/Compare 2 overcapture flag
    unsigned cc3of : 1; ///< Capture/Compare 3 overcapture flag
    unsigned cc4of : 1; ///< Capture/Compare 4 overcapture flag
    unsigned       : 19;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the EGR, TIM3 event generation register
 */
//*********************************************************************
struct egr
{
    unsigned ug   : 1; ///< Update generation
    unsigned cc1g : 1; ///< Capture/compare 1 generation
    unsigned cc2g : 1; ///< Capture/compare 2 generation
    unsigned cc3g : 1; ///< Capture/compare 3 generation
    unsigned cc4g : 1; ///< Capture/compare 4 generation
    unsigned      : 1;
    unsigned tg   : 1; ///< Trigger generation
    unsigned      : 25;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCMR1, TIM3 capture/compare mode register 1
 */
//*********************************************************************
union ccmr1
{
    struct
    {
        unsigned cc1s   : 2; ///< Capture/Compare 1 selection
        unsigned ic1psc : 2; ///< Input capture 1 prescaler
        unsigned ic1f   : 4; ///< Input capture 1 filter
        unsigned cc2s   : 2; ///< Capture/Compare 2 selection
        unsigned ic2psc : 2; ///< Input capture 2 prescaler
        unsigned ic2f   : 4; ///< Input capture 2 filter
        unsigned        : 16;
    } input;

    struct
    {
        unsigned cc1s     : 2; ///< Capture/Compare 1 selection
        unsigned oc1fe    : 1; ///< Output compare 1 fast enable
        unsigned oc1pe    : 1; ///< Output compare 1 preload enable
        unsigned oc1m     : 3; ///< Output compare 1 mode, low bits [2:0]
        unsigned oc1ce    : 1; ///< Output compare 1 clear enable
        unsigned cc2s     : 2; ///< Capture/Compare 2 selection
        unsigned oc2fe    : 1; ///< Output compare 2 fast enable
        unsigned oc2pe    : 1; ///< Output compare 2 preload enable
        unsigned oc2m     : 3; ///< Output compare 2 mode, low bits [2:0]
        unsigned oc2ce    : 1; ///< Output compare 2 clear enable
        unsigned oc1m_msb : 1; ///< Output compare 1 mode, high bit [3]
        unsigned          : 7;
        unsigned oc2m_msb : 1; ///< Output compare 2 mode, high bit [3]
        unsigned          : 7;
    } output;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCMR2, TIM3 capture/compare mode register 2
 */
//*********************************************************************
union ccmr2
{
    struct
    {
        unsigned cc3s   : 2; ///< Capture/Compare 3 selection
        unsigned ic3psc : 2; ///< Input capture 3 prescaler
        unsigned ic3f   : 4; ///< Input capture 3 filter
        unsigned cc4s   : 2; ///< Capture/Compare 4 selection
        unsigned ic4psc : 2; ///< Input capture 4 prescaler
        unsigned ic4f   : 4; ///< Input capture 4 filter
        unsigned        : 16;
    } input;

    struct
    {
        unsigned cc3s     : 2; ///< Capture/Compare 3 selection
        unsigned oc3fe    : 1; ///< Output compare 3 fast enable
        unsigned oc3pe    : 1; ///< Output compare 3 preload enable
        unsigned oc3m     : 3; ///< Output compare 3 mode, low bits [2:0]
        unsigned oc3ce    : 1; ///< Output compare 3 clear enable
        unsigned cc4s     : 2; ///< Capture/Compare 4 selection
        unsigned oc4fe    : 1; ///< Output compare 4 fast enable
        unsigned oc4pe    : 1; ///< Output compare 4 preload enable
        unsigned oc4m     : 3; ///< Output compare 4 mode, low bits [2:0]
        unsigned oc4ce    : 1; ///< Output compare 4 clear enable
        unsigned oc3m_msb : 1; ///< Output compare 3 mode, high bit [3]
        unsigned          : 7;
        unsigned oc4m_msb : 1; ///< Output compare 4 mode, high bit [3]
        unsigned          : 7;
    } output;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCER, TIM3 capture/compare enable register
 */
//*********************************************************************
struct ccer
{
    unsigned cc1e  : 1; ///< Capture/Compare 1 output enable
    unsigned cc1p  : 1; ///< Capture/Compare 1 output polarity
    unsigned       : 1;
    unsigned cc1np : 1; ///< Capture/Compare 1 complementary output polarity
    unsigned cc2e  : 1; ///< Capture/Compare 2 output enable
    unsigned cc2p  : 1; ///< Capture/Compare 2 output polarity
    unsigned       : 1;
    unsigned cc2np : 1; ///< Capture/Compare 2 complementary output polarity
    unsigned cc3e  : 1; ///< Capture/Compare 3 output enable
    unsigned cc3p  : 1; ///< Capture/Compare 3 output polarity
    unsigned       : 1;
    unsigned cc3np : 1; ///< Capture/Compare 3 complementary output polarity
    unsigned cc4e  : 1; ///< Capture/Compare 4 output enable
    unsigned cc4p  : 1; ///< Capture/Compare 4 output polarity
    unsigned       : 1;
    unsigned cc4np : 1; ///< Capture/Compare 4 complementary output polarity
    unsigned       : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CNT, TIM3 counter register
 */
//*********************************************************************
union cnt
{
    struct
    {
        unsigned cnt : 16; ///< Counter valye
        unsigned     : 16;
    } uifremap_lo;

    struct
    {
        unsigned cnt    : 16; ///< Counter value
        unsigned        : 15;
        unsigned uifcpy : 1; ///< UIF copy
    } uifremap_hi;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the PSC, TIM3 prescaler register
 */
//*********************************************************************
struct psc
{
    unsigned psc : 16; ///< Prescaler value
    unsigned     : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the ARR, TIM3 auto-reload register
 */
//*********************************************************************
struct arr
{
    unsigned arr : 16; ///< Auto-reload value
    unsigned     : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCR1, TIM3 capture/compare register 1
 */
//*********************************************************************
struct ccr1
{
    unsigned ccr1 : 16; ///< Capture/Compare 1 value
    unsigned      : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCR2, TIM3 capture/compare register 2
 */
//*********************************************************************
struct ccr2
{
    unsigned ccr2 : 16; ///< Capture/Compare 2 value
    unsigned      : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCR3, TIM3 capture/compare register 3
 */
//*********************************************************************
struct ccr3
{
    unsigned ccr3 : 16; ///< Capture/Compare 3 value
    unsigned      : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CCR4, TIM3 capture/compare register 4
 */
//*********************************************************************
struct ccr4
{
    unsigned ccr4 : 16; ///< Capture/Compare 4 value
    unsigned      : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the DCR, TIM3 DMA control register
 */
//*********************************************************************
struct dcr
{
    unsigned dba : 5; ///< DMA base address
    unsigned     : 3;
    unsigned dbl : 5; ///< DMA burst length
    unsigned     : 19;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the DMAR, TIM3 DMA address for full transfer
 */
//*********************************************************************
struct dmar
{
    unsigned dmab : 16; ///< DMA register for burst accesses
    unsigned      : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the OR1, TIM3 option register 1
 */
//*********************************************************************
struct or1
{
    unsigned ocref_clr : 2; ///< OCREF_CLR source selection
    unsigned           : 30;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the AF1, TIM3 alternate function option register 1
 */
//*********************************************************************
struct af1
{
    unsigned        : 14;
    unsigned etrsel : 4;
    unsigned        : 14;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the TISEL, TIM3 timer input selection register
 */
//*********************************************************************
struct tisel
{
    unsigned ti1sel : 4; ///< TI1[0] to TI1[15] input selection
    unsigned        : 4;
    unsigned ti2sel : 4; ///< TI2[0] to TI2[15] input selection
    unsigned        : 20;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the TIM3 control registers
 */
//*********************************************************************
struct
{
    struct cr1 cr1;
    struct cr2 cr2;
    struct smcr smcr;
    struct dier dier;
    struct sr sr;
    struct egr egr;
    union ccmr1 ccmr1;
    union ccmr2 ccmr2;
    struct ccer ccer;
    union cnt cnt;
    struct psc psc;
    struct arr arr;
    unsigned reserved1[1];
    struct ccr1 ccr1;
    struct ccr2 ccr2;
    struct ccr3 ccr3;
    struct ccr4 ccr4;
    unsigned reserved2[1];
    struct dcr dcr;
    struct dmar dmar;
    struct or1 or1;
    unsigned reserved3[3];
    struct af1 af1;
    unsigned reserved4[1];
    struct tisel tisel;
} volatile tim3 __attribute__((section(".bss.tim3")));

void tim3_init(void)
{
    // Initialize TIM3 peripheral clock
    rcc_enable_tim3_clock(true);
    // Set PWM frequency
    tim3.arr.arr   = 2000u;
    // Set the duty cycle
    tim3.ccr4.ccr4 = 1000u;
}

void start_8khz_pwm_output(void)
{
    // Select the PWM mode
    tim3.ccmr2.output.oc4m  = 0x6;
    // Set the preload register
    tim3.ccmr2.output.oc4pe = 1;
    // Set the auto-reload preload register
    tim3.cr1.arpe           = 1;
    // Initialize the shadow registers before an update event occurs
    tim3.egr.ug             = 1;
    // Enable OC4 output
    tim3.ccer.cc4e          = 1;
    // Start the timer output
    tim3.cr1.cen            = 1;
}

void stop_8khz_pwm_output(void)
{
    // Stop the timer output
    tim3.cr1.cen   = 0;
    // Set signal low
    tim3.ccer.cc4p = 1;
    // Disable OC4 output
    tim3.ccer.cc4e = 0;
}
