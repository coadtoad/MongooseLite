//******************************************************************
/*! \file hal_crc.c
 *
 * \brief Kidde: STM32G031 - Implement hal CRC functions
 *
 * \author Benjamin Merrick
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#include <arm_acle.h>
#include <hal_crc.h>
#include <stdbool.h>
#include <stdint.h>

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

/**
 * \brief CRC control register
 */
struct cr
{
    unsigned reset    : 1;  ///<   Temporary storage location for 4 bytes
    unsigned          : 2;  ///<   reserved bytes during reset
    unsigned polysize : 2;  ///<   Set size pf polynomial
    unsigned rev_in   : 2;  ///<   reserve input data
    unsigned rev_out  : 1;  ///<   reserve output data
    unsigned          : 24; ///<   unused
};

/**
 * \brief CRC data register
 */
union dr
{
    unsigned w;
    unsigned short h;
    unsigned char b;
};

/**
 * \brief CRC struct of registers
 */

struct
{
    union dr dr;  ///< Data register
    unsigned idr; ///< Independent data register
    struct cr cr; ///< Control register
    unsigned unused;
    unsigned init; ///< CRC initialization
    unsigned pol;  ///< CRC polynomial
} volatile crc __attribute__((section(".bss.crc")));

/**
 * \brief Initialize CRC register values
 *
 * \param seed The initial value for the CRC
 */
void crc_init(uint32_t const seed)
{
    crc.init = seed;
    crc_reset();
}

/**
 * \brief Reset the running CRC
 */
void crc_reset(void)
{
    crc.cr.reset = 1;
}

/**
 * \brief CRC the value in \p bytes
 * \param bytes The value to be CRC-ed
 */
void crc_32(uint32_t const bytes)
{
    crc.dr.w = __rev(bytes);
}

/**
 * \brief CRC the value in \p bytes
 * \param bytes The value to be CRC-ed
 */
void crc_16(uint16_t const bytes)
{
    crc.dr.h = __revsh(bytes);
}

/**
 * \brief CRC the value in \p bytes
 * \param bytes The value to be CRC-ed
 */
void crc_8(uint8_t const bytes)
{
    crc.dr.b = bytes;
}

/**
 * \brief Get the running CRC value
 * \returns The running CRC value
 */
uint32_t get_crc(void)
{
    return crc.dr.w;
}
