#include "hal_electronic_signature.h"
#include "utility.h"

struct
{
    unsigned uid[3];
} volatile electronic_signature __attribute__((section(".bss.electronic_signature")));

void uid_to_hex(char *const buffer)
{
    uint32_to_hex(buffer, electronic_signature.uid[2]);
    uint32_to_hex(buffer + 8, electronic_signature.uid[1]);
    uint32_to_hex(buffer + 16, electronic_signature.uid[0]);
}

