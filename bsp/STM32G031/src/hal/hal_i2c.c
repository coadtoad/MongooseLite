//*********************************************************************
/*! \file hal_i2c.c
 *
 * \brief Kidde: Mongoose-fw - Implement the I2C HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************
#include "hal_i2c.h"
#include "hal_nvic.h"

/**
 * @todo Remove when the optimization bug is fixed
 */
#pragma clang optimize off

//*********************************************************************
/**
 * \def TICKS_TIMEOUT
 * \brief Used to timeout of wait loops
 */
//*********************************************************************
#define TICKS_TIMEOUT 255

//*********************************************************************
/**
 * \brief A memory mapped type for the CR1, I2C control register 1
 */
//*********************************************************************
struct cr1
{
    unsigned pe        : 1; ///< Peripheral enable
    unsigned txie      : 1; ///< TX interrupt enable
    unsigned rxie      : 1; ///< RX interrupt enable
    unsigned addrie    : 1; ///< Address match interrupt enable (slave only)
    unsigned nackie    : 1; ///< Not acknowledge received interrupt enable
    unsigned stopie    : 1; ///< Stop detection interrupt enable
    unsigned tcie      : 1; ///< Transfer Complete interrupt enable
    unsigned errie     : 1; ///< Error interrupts enable
    unsigned dnf       : 4; ///< Digital noise filter
    unsigned anfoff    : 1; ///< Analog noise filler OFF
    unsigned           : 1;
    unsigned txdmaen   : 1; ///< DMA transmission requests enable
    unsigned rxdmaen   : 1; ///< DMA reception requests enable
    unsigned sbc       : 1; ///< Slave byte control
    unsigned nostretch : 1; ///< Clock stretching disable
    unsigned wupen     : 1; ///< Wakeup from Stop mode enable
    unsigned gcen      : 1; ///< General call enable
    unsigned smbhen    : 1; ///< SMBus Host Address enable
    unsigned smbden    : 1; ///< SMBus Device Default Address enable
    unsigned alerten   : 1; ///< SMBus alert enable
    unsigned pecen     : 1; ///< PEC enable
    unsigned           : 8;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the CR2, I2C control register 2
 */
//*********************************************************************
struct cr2
{
    unsigned sadd    : 10; ///< Slave address (master mode)
    unsigned rd_wrn  : 1;  ///< Transfer direction (master mode)
    unsigned add10   : 1;  ///< 10-bit addressing mode (master mode)
    unsigned head10r : 1;  ///< 10-bit address header only read direction (master receiver mode)
    unsigned start   : 1;  ///< Start generation
    unsigned stop    : 1;  ///< Stop generation (master mode)
    unsigned nack    : 1;  ///< NACK generation (slave mode)
    unsigned nbytes  : 8;  ///< Number of bytes
    unsigned reload  : 1;  ///< NBYTES reload mode
    unsigned autoend : 1;  ///< Automatic end mode (master mode)
    unsigned pecbyte : 1;  ///< Packet error checking byte
    unsigned         : 5;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the OAR1, I2C own address 1 register
 */
//*********************************************************************
struct oar1
{
    unsigned oa1     : 10; ///< Interface own slave address
    unsigned oa1mode : 1;  ///< Own Address 1 10-bit mode
    unsigned         : 4;
    unsigned oa1en   : 1; ///< Own Address 1 enable
    unsigned         : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the OAR2, I2C own address 2 register
 */
//*********************************************************************
struct oar2
{
    unsigned oa2    : 8; ///< Interface address
    unsigned oa2msk : 3; ///< Own Address 2 masks
    unsigned        : 4;
    unsigned oa2en  : 1; ///< Own Address 2 enable
    unsigned        : 16;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the TIMINGR, I2C timing register
 */
struct timingr
{
    unsigned scll   : 8; ///< SCL low period (master mode)
    unsigned sclh   : 8; ///< SCL high period (master mode)
    unsigned sdadel : 4; ///< Data hold time
    unsigned scldel : 4; ///< Data setup time
    unsigned        : 4;
    unsigned presc  : 4; ///< Timing prescaler
};

//*********************************************************************
/**
 * \brief A memory mapped type for the TIMEOUTR, I2C timeout register
 */
//*********************************************************************
struct timeoutr
{
    unsigned timeouta  : 12; ///< Bus timeout A
    unsigned tidle     : 1;  ///< Idle clock timeout detection
    unsigned           : 2;
    unsigned timeouten : 1;  ///< Clock tiemout enable
    unsigned timeoutb  : 12; ///< Bus timeout B
    unsigned           : 3;
    unsigned texten    : 1; ///< Extended clock timeout enable
};

//*********************************************************************
/**
 * \brief A memory mapped type for the ISR, I2C interrupt and status register
 */
//*********************************************************************
struct isr
{
    unsigned txe     : 1; ///< Transmit data register empty (transmitters)
    unsigned txis    : 1; ///< Transmit interrupt status (transmitters)
    unsigned rxne    : 1; ///< Received data register not empty (receivers)
    unsigned addr    : 1; ///< Address matched (slave mode)
    unsigned nackf   : 1; ///< Not acknowledge received flag
    unsigned stopf   : 1; ///< Stop detection flag
    unsigned tc      : 1; ///< Transfer complete (master mode)
    unsigned tcr     : 1; ///< Transfer complete reload
    unsigned berr    : 1; ///< Bus error
    unsigned arlo    : 1; ///< Arbitration lost
    unsigned ovr     : 1; ///< Overrun/underrun (slave mode)
    unsigned pecerr  : 1; ///< PEC Error in reception
    unsigned timeout : 1; ///< Timeout or t.LOW detection flag
    unsigned alert   : 1; ///< SMBus alert
    unsigned         : 1;
    unsigned busy    : 1; ///< Bus busy
    unsigned dir     : 1; ///< Transfer direction (slave mode)
    unsigned addcode : 7; ///< Address match mode (slave mode)
    unsigned         : 8;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the ICR, I2C interrupt clear register
 */
//*********************************************************************
struct icr
{
    unsigned          : 3;
    unsigned addrcf   : 1; ///< Address matched flag clear
    unsigned nackcf   : 1; ///< Not Acknowledge flag clear
    unsigned stopcf   : 1; ///< STOP detection flag clear
    unsigned          : 2;
    unsigned berrcf   : 1; ///< Bus error flag clear
    unsigned arlocf   : 1; ///< Arbitration lost flag clear
    unsigned ovrcf    : 1; ///< Overrun/Underrun flag clear
    unsigned peccf    : 1; ///< PEC Error flag clear
    unsigned timoutcf : 1; ///< Timeout detection flag clear
    unsigned alertcf  : 1; ///< Alert flag clear
    unsigned          : 18;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the PECR, I2C packet error checking register
 */
//*********************************************************************
struct pecr
{
    unsigned pec : 8; ///< Packet error checking register
    unsigned     : 24;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the RXDR, I2C receive data register
 */
//*********************************************************************
struct rxdr
{
    unsigned rxdata : 8; ///< 8-bit receive data
    unsigned        : 24;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the TXDR, I2C transmit data register
 */
//*********************************************************************
struct txdr
{
    unsigned txdata : 8; ///< 8-bit transmit data
    unsigned        : 24;
};

//*********************************************************************
/**
 * \brief A memory mapped type for the I2C control registers
 */
//*********************************************************************
struct
{
    struct cr1 cr1;
    struct cr2 cr2;
    struct oar1 oar1;
    struct oar2 oar2;
    struct timingr timingr;
    struct timeoutr timeoutr;
    struct isr isr;
    struct icr icr;
    struct pecr pecr;
    struct rxdr rxdr;
    struct txdr txdr;
} volatile i2c1 __attribute__((section(".bss.i2c1")));

void i2c_set_fast_mode_baud(bool const fast_mode)
{
    struct timingr timingr = i2c1.timingr;
    if (fast_mode)
    {
        // Configure 12C clock to 400kHz for 16MHz clock source
        timingr.scll   = 0x1A;
        timingr.sclh   = 0x06;
        timingr.sdadel = 0x0;
        timingr.scldel = 0x1;
    }
    else
    {
        // Configure 12C clock to 100kHz for 16MHz clock source
        timingr.scll   = 0x5B;
        timingr.sclh   = 0x3D;
        timingr.sdadel = 0x0;
        timingr.scldel = 0x3;
    }
    i2c1.timingr = timingr;
}

void i2c_enable(void)
{
    // Enable address match and receive interrupts
    i2c1.cr1.addrie = 1;
    i2c1.cr1.rxie   = 1;
    nvic_enable_irq(irq_i2c1);

    // Enable I2C
    i2c1.cr1.pe = 1;
}

//*********************************************************************
/**
 * \brief Waits for the STOP condition to be set
 * \returns True if the condition was set or false if the wait period timed out
 */
//*********************************************************************
static bool i2c_wait_for_stop(void)
{
    // Wait for STOP condition after transfer completion
    for (uint8_t ticks = TICKS_TIMEOUT; !i2c1.isr.stopf && ticks; --ticks)
        ;
    bool const stop = i2c1.isr.stopf;
    // Clear the stop flag
    i2c1.icr.stopcf = 1;
    return stop;
}

//*********************************************************************
/**
 * \brief Waits for the Transfer Complete bit to be set
 * \returns True if the condition was set or false if the wait period timed out
 */
//*********************************************************************
static bool i2c_wait_for_transmit_complete(void)
{
    // Wait for transfer to complete
    for (uint8_t ticks = TICKS_TIMEOUT; i2c1.isr.tc && ticks; --ticks)
        ;
    return !i2c1.isr.tc;
}

//*********************************************************************
/**
 * \brief Sends the START condition and slave address byte
 * \param module_address The slave module to initiate an I2C communication with
 * \param read Initiates a read request if true. A write request is done if false
 * \param count Number of bytes to write/read following the slave address
 * \param stop Sends STOP after write/read transmit completes if true. Holds line if false
 */
//*********************************************************************
static void i2c_master_send_slave_address(uint8_t const module_address, bool const read, uint8_t const count,
                                          bool const stop)
{
    struct cr2 cr2 = i2c1.cr2;
    // Set the number of bytes to transmit after the slave address is sent
    cr2.nbytes     = count;
    // Set slave address
    cr2.sadd       = (uint8_t)(module_address << 1);
    // Configure Read/Write
    cr2.rd_wrn     = read;
    // Configure if stop is sent after transmit is complete
    cr2.autoend    = stop;
    // Start communication
    cr2.start      = 1;
    i2c1.cr2       = cr2;
}

bool i2c_master_write_byte(uint8_t const data, bool const stop)
{
    // Wait until the transmit data register is cleared for writing
    // TXIS is set after each byte transmission when an ACK is received
    struct isr isr = i2c1.isr;
    for (uint8_t ticks = TICKS_TIMEOUT; !isr.txis && !isr.nackf && !isr.arlo && ticks; --ticks)
    {
        isr = i2c1.isr;
    }

    // Write if the transmit register is empty and awaiting data
    if (isr.txis)
    {
        // Configure if stop is sent after transmit is complete
        i2c1.cr2.autoend = stop;

        // Load the transmit data register
        struct txdr txdr = {0};
        txdr.txdata      = data;
        i2c1.txdr        = txdr;
    }

    return isr.txis;
}

//*********************************************************************
/**
 * \brief Waits for the received register to be filled then reads the data
 * \param data Pointer to a byte to store the read data
 * \param stop Sends STOP after write/read transmit completes if true. Holds line if false
 * \returns True if the operation was a success or false if an error occurred during operation
 */
//*********************************************************************
static bool i2c_master_read_byte(uint8_t *const data, bool const stop)
{
    // Disable the receive interrupt handler while in master mode
    i2c1.cr1.rxie  = 0;

    // Wait until the receive data register is loaded with the received data
    // RXNE is set after data is received
    struct isr isr = i2c1.isr;
    for (uint8_t ticks = TICKS_TIMEOUT; !isr.rxne && !isr.arlo && ticks; --ticks)
    {
        isr = i2c1.isr;
    }

    // Read if the receive register is not empty
    if (isr.rxne)
    {
        // Read receive data register
        *data            = i2c1.rxdr.rxdata;

        // Configure if stop is sent after transmit is complete
        i2c1.cr2.autoend = stop;
    }

    // Re-enable the receive interrupt handler
    i2c1.cr1.rxie = 1;

    return isr.rxne;
}

void i2c_enable_slave(uint16_t const address, bool const ten_bit_mode)
{
    i2c1.oar1.oa1mode = ten_bit_mode;
    if (ten_bit_mode)
    {
        i2c1.oar1.oa1 = address;
    }
    else
    {
        i2c1.oar1.oa1 = address << 1;
    }
    i2c1.oar1.oa1en = 1;
}

bool i2c_get_slave_address_matched(void)
{
    return i2c1.isr.addr;
}

bool i2c_get_transfer_direction(void)
{
    return i2c1.isr.dir;
}

void i2c_clear_address_matched_flag(void)
{
    i2c1.icr.addrcf = 1;
}

bool i2c_get_received_status(void)
{
    return i2c1.isr.rxne;
}

uint8_t i2c_read_receive_register(void)
{
    return i2c1.rxdr.rxdata;
}

bool i2c_get_stop_status(void)
{
    return i2c1.isr.stopf;
}

bool i2c_write_read(uint8_t const module_address, uint8_t const *const output, size_t const output_size,
                    uint8_t *const input, size_t const input_size)
{
    if (output_size)
    {
        i2c_master_send_slave_address(module_address, false, output_size, false);
        for (size_t count = 0; count < output_size; ++count)
        {
            bool const last = count == output_size - 1;
            bool const stop = last && !input_size;
            if (!i2c_master_write_byte(output[count], stop) ||
                (last && input_size && !i2c_wait_for_transmit_complete()) || (stop && !i2c_wait_for_stop()))
            {
                return false;
            }
        }
    }

    if (input_size)
    {
        i2c_master_send_slave_address(module_address, true, input_size, false);
        for (size_t count = 0; count < input_size; ++count)
        {
            bool const stop = count >= input_size - 1;
            if (!i2c_master_read_byte(input + count, stop) || (stop && !i2c_wait_for_stop()))
            {
                return false;
            }
        }
    }
    return true;
}
