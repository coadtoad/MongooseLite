//******************************************************************
/*! \file drv_button.c
 *
 * \brief Kidde: Mongoose-fw - "Brief sentence about file"
 *
 * \author Benjamin Merrick
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "drv_button.h"
#include "drv_sleep.h"
#include "hal_exti.h"
#include "hal_gpio.h"
#include "hal_interrupt.h"
#include "hal_nvic.h"
#include "hal_pwr.h"
#include "hal_rcc.h"

#define BTN_PORT      &porta
#define BTN_PIN       4
#define EXTI_PORT     cfg_porta
#define BTN_EXTI      exti4
#define BTN_EXTI_WKUP exti4_wkp
#define WKUP_POLARITY wkup_falling_edge
#define BTN_EXTI_LINE exti_line4

//*********************************************************************
/**
 * \brief If the interrupt should be enabled
 *
 * This is used to ensure that the button interrupt is only enabled when
 * exiting Standby when it should be.
 */
//*********************************************************************
static bool interrupt_enabled = true;

//*********************************************************************
/**
 * \brief Configure button pin, and set the interrupt for the button
 *
 * \note The following pins should be configured:
 *   - TEST_HUSH
 *     - Digital input
 *     - Interrupt on product specific edge
 */
//*********************************************************************
void button_init(void)
{
    // enable rtc access for wakeup pins to write, and gpioa clock for button input
    pwr_enable_rtc_access();

    gpio_set_pin_mode(BTN_PORT, gpio_mode_input, BTN_PIN);
    gpio_pull_updown_mode(BTN_PORT, pupd_none, BTN_PIN);

    exti_config_port(BTN_EXTI, EXTI_PORT);
    exti_config_wakeonint_mask(BTN_EXTI_WKUP, exti_enable);
    nvic_enable_irq(irq_exti4_15); // no macro because multiple values can be in this irq exti?

    if (interrupt_enabled)
    {
        enable_button_intr();
    }
}

//*********************************************************************
/**
 * \brief Read the value of TEST_HUSH
 *
 * \returns If TEST_HUSH indicates that the button is down
 */
//*********************************************************************
bool button_is_down(void)
{
    // if buttonn is pressed down (current of 1 -> 0), then return that input occurs (!0 aka input 1)
    return !(gpio_read(BTN_PORT, BTN_PIN));
}

//*********************************************************************
/**
 * \brief Clear the TEST_HUSH edge interrupt
 */
//*********************************************************************
void button_clear(void)
{
    clear_wkup_pin(WKUP_FLAG_TEST_BUTTON);
    exti_clear_falling_edge(BTN_EXTI_LINE);
}

//*********************************************************************
/**
 * \brief Enable the TEST_HUSH edge interrupt
 */
//*********************************************************************
void enable_button_intr(void)
{
    enable_wkup_pin(WKUP_FLAG_TEST_BUTTON, WKUP_POLARITY);
    exti_enable_falling_edge_interrupt(BTN_EXTI_LINE);

    interrupt_enabled = true;
}

//*********************************************************************
/**
 * \brief Disable the TEST_HUSH edge interrupt
 */
//*********************************************************************
void disable_button_intr(void)
{
    disable_wkup_pin(WKUP_FLAG_TEST_BUTTON);
    exti_disable_falling_edge_interrupt(BTN_EXTI_LINE);

    interrupt_enabled = false;
}

void exti4_handler(void)
{
    button_clear();
    disable_button_intr();
    ////schedule_message(0, task_button, msg_ready, 0);
}
