#include "drv_database.h"
#include "app_database.h"
#include "database.h"
#include "drv_memory.h"
#include "hal_crc.h"
#include "hal_flash.h"
#include "hal_rcc.h"
#include <arm_acle.h>
#include <string.h>

/**
 * @brief The page that is used to store the primary copies.
 */
#define DATABASE_PAGE_PRIMARY         page31

/**
 * @brief The page that is used to store the backup copies.
 */
#define DATABASE_PAGE_BACKUP          page30

/**
 * @brief The size of the database block
 */
#define BLOCK_SIZE                    0x80u

/**
 * @brief The size of the flash page
 */
#define PAGE_SIZE                     0x800u

/**
 * @brief The number of the last block that can be written to a page
 */
#define LAST_BLOCK                    (PAGE_SIZE / BLOCK_SIZE - 1)

/**
 * @brief The address of the primary database page.
 */
#define DATABASE_PAGE_ADDRESS_PRIMARY ((unsigned *)(0x08000000u + PAGE_SIZE * DATABASE_PAGE_PRIMARY))

/**
 * @brief The page that is used for NV memory.
 */
#define DATABASE_PAGE_ADDRESS_BACKUP  ((unsigned *)(0x08000000u + PAGE_SIZE * DATABASE_PAGE_BACKUP))

/**
 * @brief This is the current primary database block within the page.
 */
static uint8_t current_block_primary = 0;

/**
 * @brief This is the current backup database block within the page.
 */
static uint8_t current_block_backup  = 0;

/**
 * @brief Find the current copy of the database.
 * @param target primary or backup database.
 * @return The current block in the page.
 */
static uint8_t find_current_block(db_target const target)
{
    unsigned const *const address = primary_db == target ? DATABASE_PAGE_ADDRESS_PRIMARY : DATABASE_PAGE_ADDRESS_BACKUP;

    for (uint8_t i = 1; i < PAGE_SIZE / BLOCK_SIZE; i++)
    {
        if (0xffffffff == address[i * BLOCK_SIZE / 4])
        {
            return i - 1;
        }
    }

    return LAST_BLOCK;
}

bool database_init(uint8_t *const buffer, size_t const size)
{
    // If the db is empty, write these values to the primary db.
    if (0xffffffff == *DATABASE_PAGE_ADDRESS_PRIMARY)
    {
        // Primary database was empty.  See if there is data in the backup database.
        if (0xffffffff == *DATABASE_PAGE_ADDRESS_BACKUP)
        {
            return false;
        }
        else
        {
            // Primary db is empty but backup db has data in it.  Find the most recent copy of the backup db.
            current_block_backup = find_current_block(backup_db);
            uint8_t const *const backup_base =
                (uint8_t *)DATABASE_PAGE_ADDRESS_BACKUP + current_block_backup * BLOCK_SIZE / 4;

            memcpy(buffer, backup_base, size);

            if (buffer[db_canary] != CONFIG_CANARY ||
                !memory_crc_valid(memory_calculate_crc((uint32_t const *)buffer, (uint32_t const *)(buffer + size - 4)),
                                  *(uint32_t const *)(buffer + size - 4)))
            {
                return false;
            }

            // Write it to the primary
            current_block_primary = 0;
            return flash_write((unsigned *)buffer, DATABASE_PAGE_ADDRESS_PRIMARY, size);
        }
    }

    // Find the current block in both databases.
    current_block_primary = find_current_block(primary_db);
    current_block_backup  = find_current_block(backup_db);

    // Database is initalized.  Read database from flash.
    memcpy(buffer, DATABASE_PAGE_ADDRESS_PRIMARY + current_block_primary * BLOCK_SIZE / 4, size);

    // Verify the checksum.  Note: Both all zeros and all 0xff's produce a checksum error.
    if (buffer[db_canary] == CONFIG_CANARY &&
        memory_crc_valid(memory_calculate_crc((uint32_t const *)buffer, (uint32_t const *)(buffer + size - 4)),
                         *(uint32_t const *)(buffer + size - 4)))
    {
        // Primary DB has been read successfully, update the backup.
        update_page_offset(backup_db);
        if (current_block_backup == 0 && !flash_erase_page(DATABASE_PAGE_BACKUP))
        {
            return false;
        }

        // Write the next block of the backup.  The backup will be updated on every reset.
        return flash_write((unsigned *)buffer, DATABASE_PAGE_ADDRESS_BACKUP + current_block_backup * BLOCK_SIZE / 4,
                           size);
    }
    else
    {
        // Checksum failed.  Read backup database.
        memcpy(buffer, DATABASE_PAGE_ADDRESS_BACKUP + current_block_backup * BLOCK_SIZE / 4, size);

        if (buffer[db_canary] == CONFIG_CANARY &&
            memory_crc_valid(memory_calculate_crc((uint32_t const *)buffer, (uint32_t const *)(buffer + size - 4)),
                             *(uint32_t const *)(buffer + size - 4)))
        {
            // Update the page offset before writing.
            update_page_offset(primary_db);

            // Backup copy is good, write buffer to primary db.
            return flash_write((unsigned *)buffer,
                               DATABASE_PAGE_ADDRESS_PRIMARY + current_block_primary * BLOCK_SIZE / 4, size);
        }
        else
        {
            // Backup copy is corrupt
            return false;
        }
    }
}

uint8_t read_primary_db(uint8_t const index)
{
    return ((uint8_t *)(DATABASE_PAGE_ADDRESS_PRIMARY + current_block_primary * BLOCK_SIZE / 4))[index];
}

uint8_t read_backup_db(uint8_t const index)
{
    return ((uint8_t *)(DATABASE_PAGE_ADDRESS_BACKUP + current_block_backup * BLOCK_SIZE / 4))[index];
}

bool is_flash_full(db_target const target)
{
    return target == primary_db ? current_block_primary == LAST_BLOCK : current_block_backup == LAST_BLOCK;
}

bool begin_flash_erase(db_target const block)
{
    pages target = (primary_db == block) ? DATABASE_PAGE_PRIMARY : DATABASE_PAGE_BACKUP;

    return flash_erase_page(target);
}

void update_page_offset(db_target const target)
{
    if (target == primary_db)
    {
        current_block_primary++;
        current_block_primary &= LAST_BLOCK;
    }
    else
    {
        current_block_backup++;
        current_block_backup &= LAST_BLOCK;
    }
}

bool begin_flash_write(db_target const block, uint8_t *const buffer, size_t const size)
{
    unsigned *const target_addr = (primary_db == block) ? DATABASE_PAGE_ADDRESS_PRIMARY : DATABASE_PAGE_ADDRESS_BACKUP;
    uint8_t const current_block = (primary_db == block) ? current_block_primary : current_block_backup;

    return flash_write((unsigned *)buffer, target_addr + (current_block * BLOCK_SIZE / 4), size);
}

bool begin_flash_verify(db_target const block, uint8_t *const buffer, size_t const size)
{
    unsigned *const target_addr = (primary_db == block) ? DATABASE_PAGE_ADDRESS_PRIMARY : DATABASE_PAGE_ADDRESS_BACKUP;
    uint8_t const current_block = (primary_db == block) ? current_block_primary : current_block_backup;

    unsigned *buf32             = (unsigned *)buffer;
    unsigned *flash             = target_addr + (current_block * BLOCK_SIZE / 4);

    // Compare all bytes written, including CRC.
    for (uint16_t i = 0; i < size / 4; i++)
    {
        if (*(buf32++) != *(flash++))
        {
            return false;
        }
    }
    return true;
}
