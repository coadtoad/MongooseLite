#include "drv_battery.h"
#include "app_serial.h"
#include "drv_adc.h"
#include "drv_sleep.h"
#include "hal_adc.h"
#include "hal_gpio.h"
#include "hal_rcc.h"

#define BAT_TEST_PORT             &portb          ///< The port for the battery test output
#define BAT_TEST_PIN              9               ///< The pin for the battery test output
#define BAT_CHK_PORT              &porta          ///< The A/D channel for the battery test voltage
#define BAT_CHK_PIN               3               ///< The A/D channel for the battery test voltage
#define AC_DETECT_PORT            &portc          ///< The port for the AC detect input
#define AC_DETECT_PIN             6               ///< The pin for the AC detect input
#define BAT_CHK_CHANNEL           ADC_CHANNEL_IN3 ///< The A/D channel for VBAT_CHK

//*********************************************************************
/**
 * \def BATTERY_SAMPLE_COUNT
 * \brief Number of ADC samples to take when taking a battery measurement
 */
//*********************************************************************
#define BATTERY_SAMPLE_COUNT      12

//*********************************************************************
/**
 * \def BATT_SAMPLE_DISCARD_COUNT
 * \brief Number of battery samples to discard before calculating the average
 */
//*********************************************************************
#define BATT_SAMPLE_DISCARD_COUNT 3

/**
 * \def ADC_CHANNEL_IN3
 * \brief Used to configure the ADC channel for battery voltage input.
 */
#define ADC_CHANNEL_IN3           3

/**
 * \def NO_BATTERY_LEVEL
 * \brief Value that defines at what level no battery is detected.
 */
#define NO_BATTERY_LEVEL          20
/**
 * \def LB_HUSH_LEVEL
 * \brief This value is subtrated from the LB threshold to determine the
 *          level at which low battery cannot be hushed. The delta is set to 50mV from Vbatt.
 * \note LOW BATTERY HUSH THRESHOLD CALCULATION
 *          -# R1 = 13.3 ohm
 *          -# R2 = 6.65 ohm
 *          -# LB hush threshold volt = 2500mv (LB - 50mV)
 *          -# I = 2500mV/(13.3 ohms + 6.65 ohms) = 125.3 mamps
 *          -# VLB = 6.65 ohms x 125.3 mA = 833.2 mV
 *          -# LB A/D value = 833mV/2500mV(A/D Vref) * 4096(max ADC counts) = 1364.8 = 0x555
 */
#define LB_HUSH_LEVEL_DELTA       27

/**
 * \def low_battery_threshold
 * \brief Holds the low battery threshold.
 * \note This value is initialized so that if it is never set, the device
 *          will enter low battery.
 */
static uint16_t low_battery_threshold = 0xfff;

void battery_init(void)
{
    // Configure the battery test pin.
    gpio_set_pin_mode(BAT_TEST_PORT, gpio_mode_output, BAT_TEST_PIN);
    gpio_pull_updown_mode(BAT_TEST_PORT, pupd_none, BAT_TEST_PIN);
    gpio_drive_low(BAT_TEST_PORT, BAT_TEST_PIN);

    // Configure AC detect port.
    /// @todo Figure out what to do with the AC_DETECT for ES+
    //    gpio_set_pin_mode(AC_DETECT_PORT, gpio_mode_input, AC_DETECT_PIN);
    //    gpio_pull_updown_mode(AC_DETECT_PORT, pupd_none, AC_DETECT_PIN);

    // Configure the battery test voltage pin.
    gpio_set_pin_mode(BAT_CHK_PORT, gpio_mode_input_an, BAT_CHK_PIN);
}

void battery_test_on(void)
{
    // Go into sleep mode during battery check.
    sleep_set_sleep_flag(SLEEP_MODE_BATCHECK);
    gpio_drive_high(BAT_TEST_PORT, BAT_TEST_PIN);
}

uint16_t battery_sample(void)
{
    // Enable adc
    adc_enable(resolution_12bits); // ADC

    // Sample VBAT_CHK
    adc_set_channel(BAT_CHK_CHANNEL);
    uint32_t battery_samples[BATTERY_SAMPLE_COUNT];
    adc_take_multiple_samples(battery_samples, BATTERY_SAMPLE_COUNT);

    // disable adc
    adc_disable(); // ADC

    // Sample has been taken, turn off battery test.
    gpio_drive_low(BAT_TEST_PORT, BAT_TEST_PIN);
    sleep_clear_sleep_flag(SLEEP_MODE_BATCHECK);

    uint16_t const battery_voltage = sort_and_average_samples(battery_samples, BATTERY_SAMPLE_COUNT,
                                                              BATT_SAMPLE_DISCARD_COUNT, serial_enable_battery);

    return battery_voltage;
}

battery_status get_battery_status(uint16_t *const batt_value)
{
    *batt_value = battery_sample();

    // First test for no battery.
    if (*batt_value < NO_BATTERY_LEVEL)
    {
        return no_battery;
    }
    // If below the allowable hush threshold, that's low battery.
    else if (*batt_value < low_battery_threshold - LB_HUSH_LEVEL_DELTA)
    {
        return low_battery;
    }
    // If below the low battery theshold, that's hushable LB.
    else if (*batt_value < low_battery_threshold)
    {
        return hushable_low_battery;
    }
    // Battery must be good.
    else
    {
        return good_battery;
    }
}

bool battery_is_ac_detected(void)
{
    return gpio_read(AC_DETECT_PORT, AC_DETECT_PIN);
}

void battery_set_threshold(uint16_t const cal_value)
{
    low_battery_threshold = cal_value;
}
