//*********************************************************************
/*! \file drv_sounder.c
 *
 * \brief Kidde: Mongoose-fw - Implement the sounder driver functions
 *
 * \author
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************

#include "drv_sounder.h"
#include "drv_sleep.h"
#include "drv_vboost.h"
#include "hal_gpio.h"
#include "hal_rcc.h"
#include "key_value_store.h"
#include "message.h"

/**
 * \def HBEN_PIN
 * \brief Used to configure HBEN gpio pin.
 */
#define HBEN_PIN      8

/**
 * \def HBEN_PORT
 * \brief Used to configure HBEN gpio.
 */
#define HBEN_PORT     &porta

/**
 * \def HORN_SUP_PORT
 * \brief Used to configure HORN_SUP gpio port.
 */
#define HORN_SUP_PORT &portb

/**
 * \def HORN_SUP_PIN
 * \brief Used to configure HORN_SUP gpio pin.
 */
#define HORN_SUP_PIN  4

void sounder_on(void)
{
    // Put system in sleep mode for the duration of the beep.
    sleep_set_sleep_flag(SLEEP_MODE_HORN);
    // Enable the vboost to the horn
    vboost_horn_on();
    // Set to turn on
    gpio_drive_high(HBEN_PORT, HBEN_PIN);
    // Update the KVS for tasks that need to avoid the sounder
    set_key_value_store_flag(key_avoidance_flags, kvs_avoid_sounder, true);
}

void sounder_off(void)
{
    // Clear the sleep mode flag.
    sleep_clear_sleep_flag(SLEEP_MODE_HORN);

    // Disable the vboost to the horn
    vboost_horn_off();
    // Clear to turn off
    gpio_drive_low(HBEN_PORT, HBEN_PIN);
    // Update the KVS for tasks that need to avoid the sounder, allow time for sounder to wind down
}

void sounder_init(void)
{
    // Init horn pin as output low.
    gpio_set_output(HBEN_PORT, HBEN_PIN);
    gpio_set_output_type(HBEN_PORT, gpio_out_type_pp, HBEN_PIN);
    gpio_set_pin_speed(HBEN_PORT, pinspeed_very_low, HBEN_PIN);
    gpio_pull_updown_mode(HBEN_PORT, pupd_none, HBEN_PIN);
    gpio_drive_low(HBEN_PORT, HBEN_PIN);

    // Configure the HORN_SUP pin.  This pin must remain high at all times.
    // Setting it low will turn on the horn.
    gpio_drive_high(HORN_SUP_PORT, HORN_SUP_PIN);
    gpio_set_pin_mode(HORN_SUP_PORT, gpio_mode_output, HORN_SUP_PIN);
    gpio_pull_updown_mode(HORN_SUP_PORT, pupd_none, HORN_SUP_PIN);
}
