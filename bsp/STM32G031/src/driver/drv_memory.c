#include "drv_memory.h"
#include "hal_crc.h"
#include "hal_rcc.h"
#include <arm_acle.h>

/**
 * @brief The location to store codespace CRC at
 */
uint32_t const volatile code_crc __attribute__((section(".code_crc"))) = 0xEFBEADDE; // 0xDEADBEEF;

uint32_t memory_calculate_crc(uint32_t const *const begin, uint32_t const *const end)
{
    return memory_calculate_crc_running(begin, end, 0xffffffff);
}

uint32_t memory_calculate_crc_unaligned(uint8_t const *const begin, uint8_t const *const end)
{
    return memory_calculate_crc_unaligned_running(begin, end, 0xffffffff);
}

uint32_t memory_calculate_crc_running(uint32_t const *const begin, uint32_t const *const end, uint32_t const seed)
{
    // Enable the clock to the CRC, it's off in sleep.
    rcc_enable_crc_clock(false);

    // Init the peripheral.
    crc_init(seed);

    for (uint32_t const *i = begin; i < end; ++i)
    {
        crc_32(*i);
    }

    uint32_t const crc = get_crc();

    // Disable clock to save power.
    rcc_disable_crc_clock();

    return crc;
}

uint32_t memory_calculate_crc_unaligned_running(uint8_t const *const begin, uint8_t const *const end,
                                                uint32_t const seed)
{
    // Enable the clock to the CRC, it's off in sleep.
    rcc_enable_crc_clock(false);

    // Init the peripheral.
    crc_init(seed);

    for (uint8_t const *i = begin; i < end; ++i)
    {
        crc_8(*i);
    }

    uint32_t const crc = get_crc();

    // Disable clock to save power.
    rcc_disable_crc_clock();

    return crc;
}

bool memory_crc_valid(uint32_t const calculated, uint32_t const expected)
{
    return calculated == __rev(expected);
}

void memory_set_crc(uint32_t *const destination, uint32_t const crc)
{
    *destination = __rev(crc);
}
