#include "drv_vboost.h"
#include "drv_afe.h"

/**
 * \brief Keeps track of vboost state.
 */
static uint8_t state = 0;

/**
 * \brief Bit masks for each function.
 */
typedef enum
{
    vboost_horn_mask    = 0x01,
    vboost_int_mask     = 0x02,
    vboost_photo_mask   = 0x04,
    vboost_battery_mask = 0x08,
} state_mask;

static void vboost_control(bool on, state_mask mask)
{
    uint8_t const new_state = on ? state | mask : state & ~mask;

    // afe must be awake while horn is on.
    // no need to worry about afe_wake/sleep to interfere with photo reads, due to sounder avoid.
    if (mask == vboost_horn_mask)
    {
        if (on)
        {
            afe_wake();
        }
        else
        {
            afe_sleep();
        }
    }

    if (!on && mask == vboost_photo_mask)
    {
        // Photo Vboost disable needs to override other Vboost enables when taking a sample
        // After the photo samples are taken, the Vboost must be re-enabled if it was enabled earlier
        afe_boost_disable();
    }
    else if ((bool)new_state != (bool)state)
    {
        if (new_state)
        {
            afe_boost_enable();
        }
        else
        {
            afe_boost_disable();
        }
    }
    state = new_state;
}

void vboost_init(void)
{
    // In case Vboost was left on, initialize it to off.
    afe_boost_disable();
}

void vboost_horn_on(void)
{
    vboost_control(true, vboost_horn_mask);
}

void vboost_horn_off(void)
{
    vboost_control(false, vboost_horn_mask);
}

void vboost_interconnect_on(void)
{
    vboost_control(true, vboost_int_mask);
}

void vboost_interconnect_off(void)
{
    vboost_control(false, vboost_int_mask);
}

void vboost_photo_on(void)
{
    vboost_control(true, vboost_photo_mask);
}

void vboost_photo_off(void)
{
    vboost_control(false, vboost_photo_mask);
}

void vboost_battery_on(void)
{
    vboost_control(true, vboost_battery_mask);
}

void vboost_battery_off(void)
{
    vboost_control(false, vboost_battery_mask);
}

// Used in cases where the VBST is enabled from the APP, but cannot assume the AFE is being used
void vboost_enable(void)
{
    afe_boost_enable();
}

uint8_t get_vboost_state(void)
{
    return state;
}
