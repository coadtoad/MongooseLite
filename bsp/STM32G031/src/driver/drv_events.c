#include "drv_events.h"
#include "hal_flash.h"
#include <string.h>

/**
 * @brief The page that is used to store the event queue.
 */
#define EVENT_PAGE         page29

/**
 * @brief The size of the event
 */
#define BLOCK_SIZE         sizeof(event_entry)

/**
 * @brief The size of the flash page
 */
#define PAGE_SIZE          0x800u

/**
 * @brief The number of the last event that can be written to a page
 */
#define LAST_BLOCK         (PAGE_SIZE / BLOCK_SIZE - 1)

/**
 * @brief The address of the primary database page.
 */
#define EVENT_PAGE_ADDRESS ((unsigned *)(0x08000000u + PAGE_SIZE * EVENT_PAGE))

uint16_t max_events(void)
{
    return PAGE_SIZE / BLOCK_SIZE;
}

int16_t write_events(event_entry const *const buffer, size_t const offset, size_t const count)
{
    if (offset >= max_events())
    {
        return 0;
    }

    // Make sure that the number of events won't run past the end of the page.
    int16_t const writes = (offset + count > max_events()) ? (max_events() - offset) : count;
    if (writes && !flash_write((unsigned *)buffer, EVENT_PAGE_ADDRESS + offset * BLOCK_SIZE / 4, writes * BLOCK_SIZE))
    {
        return -1;
    }

    return writes;
}

int16_t read_events(event_entry *const buffer, size_t const offset, size_t const count)
{
    if (offset >= max_events())
    {
        return 0;
    }

    // Make sure that the number of events won't run past the end of the page.
    int16_t const reads = (count + offset > max_events()) ? (max_events() - offset) : count;
    if (reads)
    {
        memcpy(buffer, EVENT_PAGE_ADDRESS + offset * BLOCK_SIZE / 4, reads * BLOCK_SIZE);
    }

    return reads;
}

bool begin_event_erase(void)
{
    return flash_erase_page(EVENT_PAGE);
}

bool begin_event_verify(event_entry const *const buffer, size_t const offset, size_t const count)
{
    unsigned const *buf32 = (unsigned *)buffer;
    unsigned const *flash = (EVENT_PAGE_ADDRESS + (offset * BLOCK_SIZE / 4));

    // Compare all bytes written.  Count is multiplied by 2 because the size of the
    // event structure is 2 words.  If the event structure changes size, this code must
    // be modified.
    for (uint16_t i = 0; i < count * (sizeof(event_entry) / sizeof(unsigned)); i++)
    {
        if (*(buf32++) != *(flash++))
        {
            return false;
        }
    }
    return true;
}
