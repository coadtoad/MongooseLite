//*********************************************************************
/*! \file drv_photo.c
 *
 * \brief Kidde: Mongoose-fw - Implement the photo driver functions
 *
 * \author Sreekanth Madiyan
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************

#include "drv_photo.h"
#include "app_database.h"
#include "app_serial.h"
#include "drv_adc.h"
#include "drv_afe.h"
#include "drv_led.h"
#include "drv_timer.h"
#include "hal_adc.h"
#include "hal_gpio.h"
#include "hal_rcc.h"
#include "photo_common_single.h"

/**
 * \def LEDEN_PIN
 * \brief Used to configure LEDEN gpio pin.
 */
#define LEDEN_PIN       2

/**
 * \def LEDEN_PORT
 * \brief Used to configure LEDEN gpio.
 */
#define LEDEN_PORT      &portb

/**
 * \def GPIO_AFE_PIN
 * \brief Used to configure GPIO_AFE pin.
 */
#define GPIO_AFE_PIN    3

/**
 * \def GPIO_AFE_PORT
 * \brief Used to configure GPIO_AFE.
 */
#define GPIO_AFE_PORT   &portb

/**
 * \def ADC_CHANNEL_IN1
 * \brief Used to configure the ADC channel for photo input.
 */
#define ADC_CHANNEL_IN1 1

/**
 * \def LED_DAC_IRB
 * \brief Used to set the LED current for IR-B led.
 */
#define LED_DAC_IRB     0xAA

/**
 * \def LED_DAC_IRF
 * \brief Used to set the LED current for IR-F led.
 */
#define LED_DAC_IRF     0xAA

/**
 * \brief Initialize GPIO_AFE pin
 */
static void ir_f_init(void)
{
    // Initialize GPIO pin for AFE
    gpio_set_output(GPIO_AFE_PORT, GPIO_AFE_PIN);
    gpio_set_output_type(GPIO_AFE_PORT, gpio_out_type_pp, GPIO_AFE_PIN);
    gpio_set_pin_speed(GPIO_AFE_PORT, pinspeed_very_low, GPIO_AFE_PIN);
    gpio_pull_updown_mode(GPIO_AFE_PORT, pupd_none, GPIO_AFE_PIN);
    gpio_drive_low(GPIO_AFE_PORT, GPIO_AFE_PIN);
}

/**
 * \brief Initialize LEDEN gpio pin.
 */
static void ir_b_init(void)
{
    // Initialize LEDEN pin for AFE
    gpio_set_output(LEDEN_PORT, LEDEN_PIN);
    gpio_set_output_type(LEDEN_PORT, gpio_out_type_pp, LEDEN_PIN);
    gpio_set_pin_speed(LEDEN_PORT, pinspeed_very_low, LEDEN_PIN);
    gpio_pull_updown_mode(LEDEN_PORT, pupd_none, LEDEN_PIN);
    gpio_drive_low(LEDEN_PORT, LEDEN_PIN);
}

bool photo_init(void)
{
    // Initialize the gpio for LEDEN pin
    ir_b_init();

    // Initialize the gpio for GPIO_AFE pin
    ir_f_init();

    return true;
}

bool begin_photo_read(void)
{
    // Enable adc
    adc_enable(resolution_8bits); // ADC

    // Select photo output from AMUX of AFE chip.
    if (!afe_amux_select(aout_ph))
    {
        return false;
    }

    // Wake afe
    if (!afe_wake())
    {
        return false;
    }

    // The enable1, and gpio_amux register must be refreshed after sleeping.
    if (!afe_write_shadow(enable1))
    {
        return false;
    }

    if (!afe_write_shadow(gpio_amux))
    {
        return false;
    }

    // wait microseconds for the circuit to stabilize
    timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

    return true;
}

bool end_photo_read(void)
{
    led_off();

    // disable adc
    adc_disable(); // ADC

    // Make afe enter the sleep mode.
    if (!afe_sleep())
    {
        return false;
    }

    return true;
}

/**
 * \def led_direction
 * \brief global used to determine which LED to turn on
 */
static light_signal led_direction = IR_B;

void select_led_direction(const light_signal light_signal)
{
    led_direction = light_signal;
}

void select_high_gain(void)
{
    /** \todo: Afe functions to be created to configure this */
}

void select_low_gain(void)
{
    /** \todo: Afe functions to be created to configure this */
}

uint8_t sample(bool led_set_on)
{
    uint8_t const sample_count  = db_get_value(db_photo_read_sample_count);
    uint8_t const discard_count = db_get_value(db_photo_read_sample_discard);

    uint8_t const wait_delay    = db_get_value(db_photo_sw_wait);
    adc_set_channel(ADC_CHANNEL_IN1);

    if (led_set_on)
    {
        led_on();
    }
    else
    {
        led_off(); // shouldnt be needed, but doesnt hurt
    }

    timer_delay_us(wait_delay);

    // Take multiple ADC measurements
    uint32_t samples_array[SAMPLE_COUNT_MAX];
    adc_take_multiple_samples(samples_array, sample_count);

    led_off();

    // Filter the ADC measurements and average the data
    uint8_t const average = sort_and_average_samples(samples_array, sample_count, discard_count, serial_enable_photo);

    return average;
}

bool set_drive_current_ir_b_led(uint8_t const value)
{
    return afe_set_ledb_current(value);
}

bool set_drive_current_ir_f_led(uint8_t const value)
{
    return afe_set_leda_current(value);
}

bool set_drive_current(const light_signal photo_reading, uint8_t const value)
{
    switch (photo_reading)
    {
    case IR_F:
        return set_drive_current_ir_f_led(value);
        break;
    case IR_B:
        return set_drive_current_ir_b_led(value);
        break;
    default:
        break;
    }

    return false;
}

void led_on(void)
{
    switch (led_direction)
    {
    case IR_F:
        gpio_drive_high(GPIO_AFE_PORT, GPIO_AFE_PIN);
        break;
    case IR_B:
        gpio_drive_high(LEDEN_PORT, LEDEN_PIN);
        break;
    default:
        break;
    }
}

void led_off(void)
{
    gpio_drive_low(GPIO_AFE_PORT, GPIO_AFE_PIN);
    gpio_drive_low(LEDEN_PORT, LEDEN_PIN);
}
