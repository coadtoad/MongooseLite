#include "drv_intercon.h"
#include "hal_exti.h"
#include "hal_gpio.h"
#include "hal_nvic.h"
#include "hal_pwr.h"
#include "hal_rcc.h"
#include "message_id.h"


//*********************************************************************
/**
 * \def INT_IN_PORT
 * \brief Port of the INT_IN pin
 */
//*********************************************************************
#define INT_IN_PORT        &portb

//*********************************************************************
/**
 * \def INT_IN_PIN
 * \brief Index of the INT_IN pin on its port
 */
//*********************************************************************
#define INT_IN_PIN         5

//*********************************************************************
/**
 * \def INT_MCU_PORT
 * \brief Port of the INT_MCU pin
 */
//*********************************************************************
#define INT_MCU_PORT       &porta

//*********************************************************************
/**
 * \def INT_MCU_PIN
 * \brief Index of the INT_MCU pin on its port
 */
//*********************************************************************
#define INT_MCU_PIN        0

//*********************************************************************
/**
 * \def INTCON_REF_HI_PORT
 * \brief Port of the INTCON_REF_HI pin
 */
//*********************************************************************
#define INTCON_REF_HI_PORT &porta

//*********************************************************************
/**
 * \def INTCON_REF_HI_PIN
 * \brief Index of the INTCON_REF_HI pin on its port
 */
//*********************************************************************
#define INTCON_REF_HI_PIN  15

/**
 * @brief WKUP pin number for INT_IN
 */
#define INT_IN_WKUP        6

static void enable_interconnect_int(void)
{
    enable_wkup_pin(INT_IN_WKUP, wkup_rising_edge);
    exti_enable_rising_edge_interrupt(exti_line5);
}

void interconnect_init(void)
{
    rcc_enable_gpioa_clock(true);
    rcc_enable_gpiob_clock(true);

    gpio_set_input(INT_IN_PORT, INT_IN_PIN);
    gpio_pull_updown_mode(INT_IN_PORT, pupd_none, INT_IN_PIN);

    gpio_drive_low(INT_MCU_PORT, INT_MCU_PIN);
    gpio_set_output(INT_MCU_PORT, INT_MCU_PIN);
    gpio_set_output_type(INT_MCU_PORT, gpio_out_type_pp, INT_MCU_PIN);
    gpio_set_pin_speed(INT_MCU_PORT, pinspeed_very_low, INT_MCU_PIN);
    gpio_pull_updown_mode(INT_MCU_PORT, pupd_none, INT_MCU_PIN);

    gpio_drive_low(INTCON_REF_HI_PORT, INTCON_REF_HI_PIN);
    gpio_set_output(INTCON_REF_HI_PORT, INTCON_REF_HI_PIN);
    gpio_set_output_type(INTCON_REF_HI_PORT, gpio_out_type_pp, INTCON_REF_HI_PIN);
    gpio_set_pin_speed(INTCON_REF_HI_PORT, pinspeed_very_low, INTCON_REF_HI_PIN);
    gpio_pull_updown_mode(INTCON_REF_HI_PORT, pupd_none, INTCON_REF_HI_PIN);

    exti_config_port(exti5, cfg_portb);
    exti_config_wakeonint_mask(exti5_wkp, exti_enable);
    exti_enable_rising_edge_interrupt(exti_line5);
    nvic_enable_irq(irq_exti4_15);
}

void drive_high(void)
{
    disable_interconnect_int();
    gpio_drive_high(INTCON_REF_HI_PORT, INTCON_REF_HI_PIN);
    gpio_drive_low(INT_MCU_PORT, INT_MCU_PIN);
}

void drive_low(void)
{
    disable_interconnect_int();
    gpio_drive_high(INTCON_REF_HI_PORT, INTCON_REF_HI_PIN);
    gpio_drive_high(INT_MCU_PORT, INT_MCU_PIN);
}

void set_tristate(void)
{
    enable_interconnect_int();
    gpio_drive_low(INTCON_REF_HI_PORT, INTCON_REF_HI_PIN);
    gpio_drive_low(INT_MCU_PORT, INT_MCU_PIN);
}

bool read_interconnect(void)
{
    disable_interconnect_int();
    return gpio_read(INT_IN_PORT, INT_IN_PIN);
}

void disable_interconnect_int(void)
{
    disable_wkup_pin(INT_IN_WKUP);
    exti_disable_rising_edge_interrupt(exti_line5);
}

void interconnect_irq_task(void)
{
    clear_wkup_pin(INT_IN_WKUP);
    exti_clear_rising_edge(INT_IN_PIN);

    if (read_interconnect())
    {
        // Schedule Interconnect Detect Interrupt Service Task
        ////schedule_message(0, task_interconnect, msg_continue, 0);
    }
    else
    {
        // Must have been a glitch
        // Re-enable the interrupt
        set_tristate();
    }
}

void exti5_handler(void)
{
    interconnect_irq_task();
}
