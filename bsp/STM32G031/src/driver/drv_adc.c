//******************************************************************
/*! \file drv_adc.c
 *
 * \brief Kidde: Mongoose-fw - Implement the ADC driver functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#include "drv_adc.h"
#include "app_database.h"
#include "hal_adc.h"

/**
 * @brief Sort an array of data from least to greatest value
 * @param[in] samples_array The array of samples to sort through
 * @param[in] sample_count The number of samples in the array
 */
void insertion_sort(uint32_t samples_array[], uint8_t const sample_count)
{
    for (uint8_t iteration = 0; iteration < sample_count; iteration++)
    {
        /*storing current element whose left side is checked for its
                 correct position .*/
        uint32_t current_element = samples_array[iteration];
        uint8_t current_index    = iteration;

        /* check whether the adjacent element in left side is greater or
             less than the current element. */
        while (current_index > 0 && current_element < samples_array[current_index - 1])
        {
            // moving the left side element to one position forward.
            samples_array[current_index] = samples_array[current_index - 1];
            current_index                = current_index - 1;
        }

        // moving current element to its correct position.
        samples_array[current_index] = current_element;
    }
}


void adc_take_multiple_samples(uint32_t samples_array[], uint8_t const sample_count)
{
    uint8_t const num_samples = sample_count >= SAMPLE_COUNT_MAX ? SAMPLE_COUNT_MAX : sample_count;

    // take samples
    for (uint8_t i = 0; i < num_samples; i++)
    {
        samples_array[i] = adc_sample();
        // Approximately 2us between samples is acceptable
    }
}
