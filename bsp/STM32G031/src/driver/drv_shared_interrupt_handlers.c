//*********************************************************************
/*! \file drv_shared_interrupt_handlers.c
 *
 * \brief Kidde: Mongoose-fw - Implement interrupt handlers shared by
 *        multiple drivers or tasks
 *
 * \author Daniel Dilts
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************
#include "drv_timer.h"
#include "hal_exti.h"
#include "hal_interrupt.h"
#include "hal_rtc.h"

static void interrupt_handler(void)
{
}

__attribute__((weak, alias("interrupt_handler"))) void exti0_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti1_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti2_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti3_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti4_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti5_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti6_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti7_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti8_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti9_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti10_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti11_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti12_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti13_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti14_handler(void);
__attribute__((weak, alias("interrupt_handler"))) void exti15_handler(void);

__attribute__((interrupt)) void exti4_15_handler(void)
{
    uint64_t const old_time = get_current_message_time();
    set_current_message_time(time_rtc_to_64bit(rtc_get_subminute()));
    // Test button handler
    if (exti_falling_edge_occured(4) || exti_rising_edge_occured(4))
    {
        exti4_handler();
    }
    // Interconnect handler
    if (exti_falling_edge_occured(5) || exti_rising_edge_occured(5))
    {
        exti5_handler();
    }
    set_current_message_time(old_time);
}
