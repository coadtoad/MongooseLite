/**
 * @file drv_timer.c
 * @brief Implement the timer support functions
 */

#include "drv_timer.h"
#include "critical_section.h"
#include "drv_led.h"
#include "hal_rtc.h"
#include "hal_tim14.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

/**
 * @brief The current time for the executing code.  This may differ from actual time.
 *
 * This is The time that the current message or interrupt handler is running against.
 * Interrupt handlers will set this to RTC time when they begin and set it to the old
 * time when the interrupt handler completes.  For message dispatch, this is set to
 * the time that the lessage was supposed to be dispatched at.
 *
 * @warning This time may go backwards or forwards.  This @b MUST not be used to
 * enqueue messages or the heap could be corrupted.
 */
static uint64_t current_message_time;

void timer_init_vars(void)
{
    // Initialize housekeeping variables.
    current_message_time = 0;
    rtc_reset_clock();
}

bool timer_init(void)
{
    // Initialize the hardware.
    return rtc_init();
}

void timer_delay_us(uint16_t count)
{
    /** @todo Should this be in a critical section? */
    if (count > 1)
    {
        count -= 1;
    }
    tim14_start_count(count);
    while (!tim14_check_timer_expiration())
        ;
}

void timer_irq_task(void)
{
    // This interrupt handler is called to wake the processor up.
    // It does not need to do any actual work.
}

uint64_t time_32bit_to_64bit(uint32_t const time)
{
    uint32_t const lower = (uint32_t)current_message_time;
    uint32_t const upper = (uint32_t)(current_message_time >> 32);

    uint64_t result;
    if (lower <= time)
    {
        // No rollover
        result = (uint64_t)upper << 32;
    }
    else
    {
        // Rollover
        result = (uint64_t)(upper + 1) << 32;
    }
    result |= time;

    return result;
}

uint32_t time_64bit_to_32bit(uint64_t const time)
{
    return (uint32_t)time;
}

uint16_t time_64bit_to_rtc(uint64_t const time)
{
    return time % 60000;
}

uint16_t time_32bit_to_rtc(uint32_t const time)
{
    return time_64bit_to_rtc(time_32bit_to_64bit(time));
}

uint32_t time_rtc_to_32bit(uint16_t const time)
{
    return time_64bit_to_32bit(time_rtc_to_64bit(time));
}

uint64_t time_rtc_to_64bit(uint16_t const time)
{
    lldiv_t parts = lldiv(current_message_time, 60000);

    uint64_t result;
    if (parts.rem <= time)
    {
        // No rollover
        result = (uint64_t)parts.quot * 60000;
    }
    else
    {
        result = (uint64_t)(parts.quot + 1) * 60000;
    }
    result += time;

    return result;
}

void set_current_message_time(uint64_t const time)
{
    current_message_time = time;
}

uint64_t get_current_message_time(void)
{
    return current_message_time;
}
