//******************************************************************
/*! \file drv_co.c
 *
 * \brief Kidde: Mongoose-fw - Implement the CO driver functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#include "drv_co.h"
#include "app_serial.h"
#include "drv_adc.h"
#include "drv_afe.h"
#include "drv_sleep.h"
#include "drv_vboost.h"
#include "hal_adc.h"
#include "hal_gpio.h"
#include "hal_tim3.h"

//*********************************************************************
/**
 * \def CO_TEST_ENABLE_PORT
 * \brief Port of the CO_TEST_ENABLE pin
 */
//*********************************************************************
#define CO_TEST_ENABLE_PORT     &portb

//*********************************************************************
/**
 * \def CO_TEST_ENABLE_PIN
 * \brief Index of the CO_TEST_ENABLE pin on its port
 */
//*********************************************************************
#define CO_TEST_ENABLE_PIN      1

//*********************************************************************
/**
 * \def ADC_CHANNEL_IN9
 * \brief Used to configure the ADC channel for AMUX input.
 */
//*********************************************************************
#define ADC_CHANNEL_IN1         1

//*********************************************************************
/**
 * \def CO_SAMPLE_COUNT
 * \brief Number of ADC samples to take when taking a CO measurement
 */
//*********************************************************************
#define CO_SAMPLE_COUNT         6

//*********************************************************************
/**
 * \def CO_SAMPLE_DISCARD_COUNT
 * \brief Number of CO samples to discard before calculating the average
 */
//*********************************************************************
#define CO_SAMPLE_DISCARD_COUNT 1

#define AFT_OUT_PORT            &portb /**< Define the port for AFT_OUT signal */
#define AFT_OUT_PIN             8      /**< Define the pin for AFT_OUT signal */

void co_init(void)
{
    // Configure CO_TEST_ENABLE pin as a PWM output
    gpio_set_pin_mode(CO_TEST_ENABLE_PORT, gpio_mode_altfunc, CO_TEST_ENABLE_PIN);
    gpio_set_afr_pin(CO_TEST_ENABLE_PORT, gpio_pb1_af_tim3_ch4, CO_TEST_ENABLE_PIN);

    // Configure the CO_TEST pin for 8kHz, 50% duty cycle output
    tim3_init();
}

bool initialize_afe_co(void)
{
    // Configure the AFE's CO registers
    if (!afe_configure_co())
    {
        return false;
    }

    // Enable the AFE's CO amplifier
    if (!afe_co_amplifier_enable())
    {
        return false;
    }

    return true;
}

bool co_sample(uint16_t *const sample)
{
    // Turn on AFT_OUT before taking a CO measurement
    gpio_drive_high(AFT_OUT_PORT, AFT_OUT_PIN);

    // Disable the vboost before sampling AMUX
    afe_boost_disable();

    // Select CO output from AMUX of AFE chip.
    if (!afe_amux_select(coo))
    {
        return false;
    }

    // Wake afe to enable AMUX block
    if (!afe_wake())
    {
        return false;
    }

    // The gpio_amux register must be refreshed after sleeping.
    if (!afe_write_shadow(gpio_amux))
    {
        return false;
    }

    // Enable adc for 10-bit resolution during CO readings
    adc_enable(resolution_10bits);
    // Set the ADC to the AMUX channel
    adc_set_channel(ADC_CHANNEL_IN1);
    // Take the ADC reading
    uint32_t samples_array[CO_SAMPLE_COUNT];
    adc_take_multiple_samples(samples_array, CO_SAMPLE_COUNT);
    // Turn off the ADC
    adc_disable();

    // Turn off AFT_OUT after taking a CO measurement
    gpio_drive_low(AFT_OUT_PORT, AFT_OUT_PIN);

    // Enable the vboost if it was enabled before disabling it
    if (get_vboost_state())
    {
        afe_boost_enable();
    }

    // Take the ADC reading
    *sample = sort_and_average_samples(samples_array, CO_SAMPLE_COUNT, CO_SAMPLE_DISCARD_COUNT, serial_enable_co);

    // Make afe enter the sleep mode.
    if (!afe_sleep())
    {
        return false;
    }
    return true;
}

void co_start_test_pulse(void)
{
    // Turn on the 8kHz clock output
    start_8khz_pwm_output();
    // Set the sleep flag for PWM because PWM cannot output during standby mode
    sleep_set_sleep_flag(SLEEP_MODE_PWM);
}

void co_stop_test_pulse(void)
{
    // Turn off the 8kHz clock output
    stop_8khz_pwm_output();
    // Clear the sleep flag for PWM
    sleep_clear_sleep_flag(SLEEP_MODE_PWM);
}
