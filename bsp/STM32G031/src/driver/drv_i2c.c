//******************************************************************
/*! \file drv_i2c.c
 *
 * \brief Kidde: Mongoose-fw - Implement the I2C driver functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#include "drv_i2c.h"
#include "hal_gpio.h"
#include "hal_i2c.h"
#include "hal_interrupt.h"
#include "hal_rcc.h"
#include "utility.h"

//*********************************************************************
/**
 * \def MAIN_BOARD_ADDRESS
 * \brief The slave address of this module's I2C interface
 */
//*********************************************************************
#define MAIN_BOARD_ADDRESS    0x1D

//*********************************************************************
/**
 * \def I2C_SCL_PORT
 * \brief Port of the I2C_CLK pin
 */
//*********************************************************************
#define I2C_SCL_PORT          &portb

//*********************************************************************
/**
 * \def I2C_SCL_PIN
 * \brief Index of the I2C_CLK pin on its port
 */
//*********************************************************************
#define I2C_SCL_PIN           6

//*********************************************************************
/**
 * \def I2C_SDA_PORT
 * \brief Port of the I2C_DATA pin
 */
//*********************************************************************
#define I2C_SDA_PORT          &portb

//*********************************************************************
/**
 * \def I2C_SDA_PIN
 * \brief Index of the I2C_DATA pin on its port
 */
//*********************************************************************
#define I2C_SDA_PIN           7

//*********************************************************************
/**
 * \def RETRY_COUNT
 * \brief Number of transmit attempts before an operation fails
 */
//*********************************************************************
#define RETRY_COUNT           3

//*********************************************************************
/**
 * \def WRITE_REG_OUTPUT_SIZE
 * \brief Number of bytes transmitted during a single register write using i2c_write()
 */
//*********************************************************************
#define WRITE_REG_OUTPUT_SIZE 2

//*********************************************************************
/**
 * \def READ_REG_OUTPUT_SIZE
 * \brief Number of bytes read during a single register write using i2c_read()
 */
//*********************************************************************
#define READ_REG_OUTPUT_SIZE  1

//*********************************************************************
/**
 * \brief Molecule register map
 */
//*********************************************************************
static uint8_t register_map[register_index_count];

void i2c_init(void)
{
    // Configure SCL pin
    gpio_set_output_type(I2C_SCL_PORT, gpio_mode_open_drain, I2C_SCL_PIN);
    gpio_set_pin_mode(I2C_SCL_PORT, gpio_mode_altfunc, I2C_SCL_PIN);
    gpio_set_afr_pin(I2C_SCL_PORT, gpio_pb6_af_i2c1_scl, I2C_SCL_PIN);
    // Configure SDA pin
    gpio_set_output_type(I2C_SDA_PORT, gpio_mode_open_drain, I2C_SDA_PIN);
    gpio_set_pin_mode(I2C_SDA_PORT, gpio_mode_altfunc, I2C_SDA_PIN);
    gpio_set_afr_pin(I2C_SDA_PORT, gpio_pb7_af_i2c1_sda, I2C_SDA_PIN);
    // Select the I2C clock source
    rcc_enable_i2c1_clock(i2c_hsi16, true);
    // Set the I2C baud rate
    i2c_set_fast_mode_baud(true);
    // Set own address
    i2c_enable_slave(MAIN_BOARD_ADDRESS, false);
    // Enable I2C
    i2c_enable();
}

bool i2c_write(uint8_t const module_address, uint8_t const register_address, uint8_t const data)
{
    uint8_t output[WRITE_REG_OUTPUT_SIZE] = {register_address, data};
    for (uint8_t i = 0; i < RETRY_COUNT; ++i)
    {
        // Send the slave address, the register address, and then the data to write
        if (i2c_write_read(module_address, output, WRITE_REG_OUTPUT_SIZE, NULL, NULL))
        {
            return true;
        }
    }
    return false;
}

bool i2c_read(uint8_t const module_address, uint8_t const register_address, uint8_t *const data)
{
    uint8_t output[READ_REG_OUTPUT_SIZE] = {register_address};
    uint8_t input[READ_REG_OUTPUT_SIZE];
    for (uint8_t i = 0; i < RETRY_COUNT; ++i)
    {
        // Send the slave address, the register address, and then the slave address with a Read operation
        if (i2c_write_read(module_address, output, READ_REG_OUTPUT_SIZE, input, READ_REG_OUTPUT_SIZE))
        {
            *data = input[0];
            return true;
        }
    }
    return false;
}

void set_register_value(register_index const index, uint8_t const value)
{
    register_map[index] = value;
}

uint8_t get_register_value(register_index const index)
{
    return register_map[index];
}

void print_i2c_register_values(void)
{
    serial_send_string("I2C Registers:\r");
    for (uint8_t index = 0; index < register_index_count; index++)
    {
        serial_send_int_ex(0, index, 16, ": ");
        serial_send_int_ex(0, register_map[index], 16, "\r");
    }
}

//*********************************************************************
/**
 * \brief This function handles I2C interrupts
 */
//*********************************************************************
__attribute__((interrupt)) void i2c1_handler(void)
{
    static bool read       = false; // Write/Read request
    static bool is_address = true;  // Expecting the next received byte to be a register address
    static uint8_t address = 0;     // Received register address

    if (i2c_get_slave_address_matched())
    {
        // Received slave address matched the enabled owned address
        read       = i2c_get_transfer_direction();

        // If DIR bit is set to write, prepare to receive an address byte
        is_address = !read;

        // Clear the flag
        i2c_clear_address_matched_flag();

        serial_send_int_ex("Rx: ", MAIN_BOARD_ADDRESS, 16, "\r");

        if (read)
        {
            // Send data from last received register
            i2c_master_write_byte(get_register_value(address), false);
        }
    }
    else if (!read && i2c_get_received_status())
    {
        // Received data loaded into RXDR register
        uint8_t const data = i2c_read_receive_register();

        serial_send_int_ex("Rx: ", data, 16, "\r");
        // Determine if byte is a register address or data value
        if (is_address)
        {
            address    = data;

            // Next byte is a data value
            is_address = false;
        }
        else
        {
            // If data, assign to register
            set_register_value(address, data);

            // Next byte is an address if multiple bytes are sent
            is_address = true;
        }
    }
}
