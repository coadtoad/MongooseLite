#include "app_database.h"
#include "app_events.h"
#include "drv_afe.h"
#include "drv_battery.h"
#include "drv_button.h"
#include "drv_co.h"
#include "drv_i2c.h"
#include "drv_intercon.h"
#include "drv_led.h"
#include "drv_serial.h"
#include "drv_sleep.h"
#include "drv_sounder.h"
#include "drv_timer.h"
#include "drv_voice.h"
#include "drv_watchdog.h"
#include "hal_adc.h"
#include "hal_gpio.h"
#include "hal_iwdg.h"
#include "hal_nvic.h"
#include "hal_pwr.h"
#include "hal_rcc.h"
#include "hal_rtc.h"
#include "hal_tim14.h"
#include "main.h"
#include "task_list.h"
#include "hal_electronic_signature.h"
#include <arm_acle.h>

#define AFT_OUT_PORT &portb /**< Define the port for AFT_OUT signal */
#define AFT_OUT_PIN  8      /**< Define the pin for AFT_OUT signal */

//*********************************************************************
/**
 * \brief Holds bits representing the cause of the last reset.
 */
//*********************************************************************
static uint8_t reset_cause;

void debug_pulse(unsigned len)
{
    rcc_enable_gpioa_clock(true);
    gpio_set_pin_mode(&porta, gpio_mode_output, 9);
    gpio_pull_updown_mode(&porta, pupd_none, 9);

    for (unsigned i = 0; i < len; i++)
    {
        gpio_drive_high(&porta, 9);
    }
    gpio_drive_low(&porta, 9);
}

void wakeup_pulse(void)
{
    if (wkup_occurred_standby())
    {
        debug_pulse(20);
    }
}

void main_init_powerup(void)
{
    // This was caused by a soft, BOR, POR or some other reset besides the
    // standby wakeup. Since this only occurrs during powerup or BOR, all
    // variable initialization should happen in this section.

    // Initialize timers so that all task initialization is relative to time 0
    timer_init_vars();

    // Reset timers so that time spent initializing tasks won't cause problems
    timer_init_vars();

    // This needs to be reset here because it is checked on wake from standby
    // to detect a soft reset.
    reset_cause = 0;


}

//*********************************************************************
/**
 * \brief Initializes the peripherals.
 */
//*********************************************************************
static bool main_init_peripherals(void)
{
    rcc_enable_gpioa_clock(true);
    rcc_enable_gpiob_clock(true);
    rcc_enable_gpioc_clock(true);

    led_init();

    // Make sure that NO variables that are contained in RAM are initialized
    // here.  This should only be peripheral register initialization.

    // The RTC should be reset/initialized when not waking up from standby
    if (!timer_init())
    {
        // 32k Xtal failed to stabilize during timer initialization
        // The timer is not reliable. Don't proceed with initializations
        return false;
    }

    i2c_init();
    adc_init();
    serial_init();
    button_init();
    tim14_init();

    // Any other port initializations go here.
    return true;
}

static bool main_init(void)
{
    rcc_enable_pwr_clock(false);

    // All peripherals must be initialized on power up and if exit from standby mode.
    if (!main_init_peripherals())
    {
        return false;
    }

    return true;
}

int main(void)
{
    if (main_init())
    {
        main_loop();
    }
    else
    {
        // death squeal
    }
    return 0;
}

void force_soft_reset(void)
{
    nvic_system_reset();
}

void disable_external_interrupts(void)
{
    /** \todo IMPLEMENT ME */
}

uint8_t get_reset_event(void)
{

    if (reset_cause & obl_rstf)
    {
        return event_reset_opt_byte;
    }
    else if (reset_cause & pin_rstf)
    {
        return event_reset_pin;
    }
    else if (reset_cause & pwr_rstf)
    {
        return event_reset_borf_por;
    }
    else if (reset_cause & sft_rstf)
    {
        return event_reset_swrf;
    }
    else if (reset_cause & iwwg_rstf)
    {
        return event_reset_iwdtrf;
    }
    else if (reset_cause & wwdg_rstf)
    {
        return event_reset_wdtrf;
    }
    else if (reset_cause & lpwr_rstf)
    {
        return event_reset_illgl_mode;
    }

    return event_reset_unknown;
}

void enter_standby_mode(void)
{
#ifdef DEBUG
    // Configure the debugger for operation in standby mode.
    *(unsigned volatile *)0x4002103c |= (unsigned)0x08000000; // turn on debug clock
    *(unsigned volatile *)0x40015804 = (unsigned)0x04;        // enable debug clock in standby
#else
    // Configure debug pins as input with no pullup as they are not effected by
    // standby mode.
    gpio_set_pin_mode(&porta, gpio_mode_input, 13);
    gpio_pull_updown_mode(&porta, pupd_none, 13);
    gpio_set_pin_mode(&porta, gpio_mode_input, 14);
    gpio_pull_updown_mode(&porta, pupd_none, 14);
#endif

    // This section sets the pullup/pulldown bits on the ports for standby mode.
    rcc_enable_pwr_clock(true);
    unsigned pullup, pulldown;
    process_pulls(&porta, &pullup, &pulldown);
    set_pullupdown(porta_pullup, pullup);
    set_pullupdown(porta_pulldown, pulldown);
    process_pulls(&portb, &pullup, &pulldown);
    set_pullupdown(portb_pullup, pullup);
    set_pullupdown(portb_pulldown, pulldown);
    process_pulls(&portc, &pullup, &pulldown);
    set_pullupdown(portc_pullup, pullup);
    set_pullupdown(portc_pulldown, pulldown);

    // Enable the pullup/pulldown configuration.
    pwr_enable_pullupdown();

    // Set the core deep sleep mode
    nvic_set_deepsleep();

    // Clear wakup flags before entering stanby mode.
    clear_wkup_pin_all();

    // Enable the RAM for standby mode.
    pwr_ram_on_in_standby();

    // Set standby mode
    pwr_set_low_power(mode_standby);

    // Preparation is complete, enter standby.
    __wfi();
}

void enter_stop_mode(void)
{
    // Set the core deep sleep mode
    nvic_set_deepsleep();

    // Clear wakup flags before entering stop mode.
    clear_wkup_pin_all();

    // Set stop mode
    pwr_set_low_power(mode_stop0);

    // Preparation is complete, enter stop.
    __wfi();
}

void enter_sleep_mode(void)
{
    // Set the core deep sleep mode
    nvic_clear_deepsleep();

    // Clear wakup flags before entering stanby mode.
    clear_wkup_pin_all();

    // Preparation is complete, enter sleep.
    __wfi();
}

//*********************************************************************
/**
 * \brief Sends 8 bits in SPI format on the serial pins.
 * \note  Serial cannot be enabled and serial pins must be configured
 *      for output before this function is called.  Bits are sent
 *      MSB first.
 */
//*********************************************************************
void main_send_byte(uint8_t byte)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        if (byte & 1 << (7 - i))
        {
            // set data high
            gpio_drive_high(&porta, 9);
        }
        else
        {
            gpio_drive_low(&porta, 9);
        }

        // clear clock.
        for (uint8_t j = 0; j < 3; j++)
        {
            gpio_drive_low(&porta, 10);
        }

        // data is set, raise clock edge
        for (uint8_t j = 0; j < 5; j++)
        {
            gpio_drive_high(&porta, 10);
        }

        // clear clock.
        for (uint8_t j = 0; j < 5; j++)
        {
            gpio_drive_low(&porta, 10);
        }
    }
}


////////////////////////////////////////////////////////////////////////////
// Move to utility file.
char const hex_digit[] = "0123456789ABCDEF";
void uint32_to_hex(char *dest, uint32_t const data)
{
    dest[0] = hex_digit[(data >> 28) & 0x0f];
    dest[1] = hex_digit[(data >> 24) & 0x0f];
    dest[2] = hex_digit[(data >> 20) & 0x0f];
    dest[3] = hex_digit[(data >> 16) & 0x0f];
    dest[4] = hex_digit[(data >> 12) & 0x0f];
    dest[5] = hex_digit[(data >> 8) & 0x0f];
    dest[6] = hex_digit[(data >> 4) & 0x0f];
    dest[7] = hex_digit[(data >> 0) & 0x0f];
}

//*********************************************************************
/**
 * \brief output device model information
 */
//*********************************************************************
void print_device_model(void)
{
    serial_send_string("Template");
    char buffer[26];
    buffer[24] = '\r';
    buffer[25] = '\0';
    uid_to_hex(buffer);
    serial_send_string("MCU UID: ");
    serial_send_string(buffer);
    serial_send_string("Revision: 0.1\r");
}



//*********************************************************************
/**
 * \brief Send prompt character out the serial port (after the carriage return character)
 */
//*********************************************************************
void serial_send_prompt(void)
{
    serial_send_string(">");
}

/*
+------------------------------------------------------------------------------
| Purpose:        Support routine for converting ints to ascii
+------------------------------------------------------------------------------
| Parameters:     u16 for conversion, pointer to destination, base
+------------------------------------------------------------------------------
| Return Value:   None
+------------------------------------------------------------------------------
*/
void serial_uart_itoa(uint16_t value, char *pResult, uint8_t base)
{
    uint16_t absQModB;
    char *pStart = pResult; // Save pointer to start of string

    do
    { // Start getting digits, starting from least significant
        absQModB   = value % base;
        *pResult++ = "0123456789ABCDEF"[absQModB];
        value /= base;
    } while (value);

    *pResult = 0; // End of string character

    // The string is reversed. Now put the characters in the correct order
    pResult--;
    while (pStart < pResult)
    {
        char temp;
        temp     = *pStart; // Swap first and last characters
        *pStart  = *pResult;
        *pResult = temp;
        pStart++; // Move pointers towards the middle
        pResult--;
    }
}

/*
+------------------------------------------------------------------------------
| Purpose:        Support routine for formatting serial output.
+------------------------------------------------------------------------------
|  Parameters:
|
|   prefix:  String to send before sending the value, or NULL if no
|               prefix is desired.
|   val:     value to be sent
|   base:    10 for output in decimal, 16 for output in hex
|   prompt:  If non-zero, a prompt will be sent after the integer
|
|   Typical output:  S1:123<CR>
+------------------------------------------------------------------------------
| Return Value:   None
+------------------------------------------------------------------------------
*/
void serial_send_int_ex(char *pPrefix, uint16_t val, uint8_t base, char *suffix)
{
    char buff[6]; // Max length with val=65535 -> 5 characters plus End of String

    serial_send_string(pPrefix); // Send prefix. Does nothing if string is NULL

    serial_uart_itoa(val, buff, base); // Convert number to null terminated string
    serial_send_string(buff);          // Send the string

    // Send the prompt if parameter is non-zero.
    serial_send_string(suffix);
}


