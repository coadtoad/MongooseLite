#include "drv_watchdog.h"
#include "hal_iwdg.h"

void watchdog_init(void)
{
    // Initialize the Watch Dog timeout to 4 seconds
    // app_supervisor.c will kick() the Watch Dog every 2 seconds
    iwdg_set_prescaler(iwdg_divide_by_32);
    iwdg_start_timer();
}

void kick(void)
{
    iwdg_reset_timer();
}
