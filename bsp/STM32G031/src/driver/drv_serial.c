#include "drv_serial.h"
#include "app_database.h"
#include "drv_sleep.h"
#include "drv_timer.h"
#include "hal_gpio.h"
#include "hal_interrupt.h"
#include "hal_nvic.h"
#include "hal_rcc.h"
#include "hal_rtc.h"
#include "hal_usart.h"
#include "stddef.h"

/**
 * @brief Define the size of the receive circular buffer.
 * @note This must be a power of 2, maximum size 256.
 */
#define USART_RX_BUFFER_SIZE 64
/**
 * @brief Define the size of the transmit circular buffer.
 * @note This must be a power of 2, maximum size 256.
 */
#define USART_TX_BUFFER_SIZE 128
/**
 * @brief This mask is used for making sure the receive buffer starts over at
 *          the beginning when full.
 */
#define USART_RX_BUFFER_MASK (USART_RX_BUFFER_SIZE - 1)
/**
 * @brief This mask is used for making sure the receive buffer starts over at
 *          the beginning when full.
 */
#define USART_TX_BUFFER_MASK (USART_TX_BUFFER_SIZE - 1)

/**
 * @brief Maximum serial command that can be accepted.
 */
#define MAX_COMMAND          (USART_RX_BUFFER_SIZE + 1)

/**
 * @brief Define the port for the receiver.
 */
#define RX_PORT              &porta
/**
 * @brief Define the pin for the receiver.
 */
#define RX_PIN               10
/**
 * @brief Define the port for the transmitter.
 */
#define TX_PORT              &porta
/**
 * @brief Define the pin for the transmitter.
 */
#define TX_PIN               9

static uint8_t usart_rxbuf[USART_RX_BUFFER_SIZE]; /**< Receive buffer */
static volatile uint8_t usart_rx_head;            /**< Points to the head of the receive buffer */
static volatile uint8_t usart_rx_tail;            /**< Points to the end of the receive buffer */
static volatile uint8_t usart_rx_elements;        /**< The number of elements received since last <CR> */
static uint8_t usart_txbuf[USART_TX_BUFFER_SIZE]; /**< Transmit buffer */
static volatile uint8_t usart_tx_head;            /**< Points to the head of the transmit buffer */
static volatile uint8_t usart_tx_tail;            /**< Points to the end of the transmit buffer */
static volatile uint8_t usart_tx_elements;        /**< The number of elements left to transmit */

/**
 * @brief Holds the last command received for use by the serial_get_rx_buffer() function.
 */
static uint8_t command_buffer[MAX_COMMAND];

/**
 * @brief Holds the status of the serial port.
 *          true if serial port was connected when serial port was initialized, otherwise false.
 */
static bool uart_enabled = false;

/**
 * @brief Called by interrupt handler when a character is received by the serial port.
 */
static void usart_rx_buffer_full_handler(void)
{
    // Read the received data.
    uint8_t const data         = usart_read_byte();

    // Store new index
    usart_rx_head              = (usart_rx_head + 1) & USART_RX_BUFFER_MASK;

    // Store received data in buffer
    usart_rxbuf[usart_rx_head] = data;
    usart_rx_elements++;

    // A <CR> indicates the end of a command string.  Signal the OS that a command has been received.
    if ('\r' == data)
    {
        // Let the serial task know a command has been received.
        ////schedule_message(0, task_serial, msg_continue, 0);
    }
}

/**
 * @brief Called by interrupt handler when a character has just been sent.
 */
static void usart_tx_empty_handler(void)
{
    // Check if all data is transmitted
    if (usart_tx_elements != 0)
    {
        // Store new index
        usart_tx_tail = (usart_tx_tail + 1) & USART_TX_BUFFER_MASK;

        // Start transmission
        usart_write_tx(usart_txbuf[usart_tx_tail]);

        usart_tx_elements--;
    }

    if (usart_tx_elements == 0)
    {
        // Disable UDRE interrupt.
        usart_tx_empty_int_disable();

        sleep_clear_sleep_flag(SLEEP_MODE_UART_TX);
    }
}

/**
 * @brief Read one character from the receive buffer.
 *
 * @param[in] ch Pointer to receive next character in the receive buffer.
 * @return Number of characters left in receive buffer.
 */
static uint8_t serial_read_buffer_char(uint8_t *ch)
{
    uint8_t tmptail;

    // Calculate buffer index
    tmptail       = (usart_rx_tail + 1) & USART_RX_BUFFER_MASK;

    // Store new index
    usart_rx_tail = tmptail;

    // Disable receive interrupt while decrementing the element counter.
    usart_rx_buffer_full_int_disable();
    volatile const uint8_t temp = --usart_rx_elements;
    usart_rx_buffer_full_int_enable();

    // Set the received character.
    *ch = usart_rxbuf[tmptail];

    // Return the number of characters left in the command.
    return temp;
}

/**
 * @brief Write one character to the ring buffer
 *      Function will block if ring buffer is full until there is
 *      space available.
 *
 * @param[in] data The character to put in ring buffer
 */
static void serial_send_char(char data)
{
    // Wait for free space in buffer
    while (usart_tx_elements == USART_TX_BUFFER_SIZE)
        ;

    // Store new index
    usart_tx_head              = (usart_tx_head + 1) & USART_TX_BUFFER_MASK;

    // Store data in buffer
    usart_txbuf[usart_tx_head] = data;

    // Disable the Tx empty interrupt while manipulating the counter.
    usart_tx_empty_int_disable();
    usart_tx_elements++;
    usart_tx_empty_int_enable();

    // MCU can't go into standby during xmit.
    sleep_set_sleep_flag(SLEEP_MODE_UART_TX);
}

void serial_send_string(char const *str)
{
    if (uart_enabled)
    {
        if (str)
        {
            // Loop until null.
            while (*str)
            {
                serial_send_char(*str);
                ++str;
            }
        }
    }
}

/**
 * @brief Initialize USART interface
 *      If module is configured to disabled state, the clock to the USART is disabled
 *      if this is supported by the device's clock system.
 *
 */
void serial_init(void)
{
    // Configure Rx pin as input
    gpio_set_output_type(RX_PORT, gpio_out_type_pp, RX_PIN);
    gpio_set_pin_speed(RX_PORT, pinspeed_very_low, RX_PIN);
    gpio_pull_updown_mode(RX_PORT, pupd_none, RX_PIN);
    gpio_set_pin_mode(RX_PORT, gpio_mode_input, RX_PIN);

    // If the serial port is connected, don't sleep as the UART cannot wake
    // the controller from sleep.
    if (gpio_read(RX_PORT, RX_PIN))
    {
        // Cable is attached, set status.
        uart_enabled = true;

        // Since there is a serial cable attached.  Don't sleep.
        sleep_set_sleep_flag(SLEEP_MODE_UART_RX);
    }
    else
    {
        // No cable attached, set status.
        uart_enabled = false;

        // Set Tx and Rx pins to output low to conserve power.
        gpio_set_pin_mode(RX_PORT, gpio_mode_output, RX_PIN);
        gpio_drive_low(RX_PORT, RX_PIN);

        gpio_set_pin_mode(TX_PORT, gpio_mode_output, TX_PIN);
        gpio_drive_low(TX_PORT, TX_PIN);

        // Return here without initializing serial port below.
        return;
    }

    // Enable the usart clock from the 16 MHz clock.
    rcc_enable_usart1_clock(uart_hsi16, true);

    // USART interrupt Init
    nvic_enable_irq(irq_usart1);

    // These must be set before enabling the USART.
    usart_set_word_length(usart_8nx);
    usart_set_parity(usart_clear);
    usart_set_baudrate(115200);
    usart_set_stop_bits(usart_stop_1);
    usart_set_clock_prescale(prescale_div_1);

    // Init the Tx and Rx port pins
    gpio_set_afr_pin(RX_PORT, gpio_pa10_af_usart1_rx, RX_PIN);
    gpio_set_pin_mode(RX_PORT, gpio_mode_altfunc, RX_PIN);

    gpio_set_afr_pin(TX_PORT, gpio_pa9_af_usart1_tx, TX_PIN);
    gpio_set_pin_mode(TX_PORT, gpio_mode_altfunc, TX_PIN);

    // Enable transmitter and receiver.
    usart_tx_enable();
    usart_rx_enable();
    usart_enable();

    // Enable the receive interrupt.
    usart_rx_buffer_full_int_enable();

    // Initialize ringbuffers
    usart_rx_tail = usart_rx_head = usart_rx_elements = usart_tx_tail = usart_tx_head = usart_tx_elements = 0;
}

bool serial_enabled(void)
{
    return uart_enabled;
}

uint8_t *serial_get_rx_buffer(void)
{
    uint8_t i = 0;
    uint8_t rcv_char;

    // Read characters until the whole command is read.
    while (serial_read_buffer_char(&rcv_char))
    {
        command_buffer[i++] = rcv_char;
    }

    // Add a null
    command_buffer[i] = '\0';

    return command_buffer;
}

void serial_enable_tx_int(void)
{
    usart_tx_empty_int_enable();
}

void serial_disable_tx_int(void)
{
    usart_tx_empty_int_disable();
}

/**
 * @brief This function handles USART1 global interrupt / USART1 wake-up interrupt through EXTI line 25.
 */
__attribute__((interrupt)) void usart1_handler(void)
{
    uint64_t const old_time = get_current_message_time();
    set_current_message_time(time_rtc_to_64bit(rtc_get_subminute()));
    // If the Rx buffer is full and the interrupt is enabled, then service the Rx interrupt.
    if (usart_is_rx_buffer_full() && usart_is_rcv_interrupt_enabled())
    {
        usart_rx_buffer_full_handler();
    }

    // If the Tx buffer is empty and the interrupt is enabled, then service the Tx interrupt.
    if (usart_is_tx_buffer_empty() && usart_is_tx_interrupt_enabled())
    {
        usart_tx_empty_handler();
    }
    set_current_message_time(old_time);
}
