//******************************************************************
/*! \file drv_voice.c
 *
 * \brief Kidde: STM32G031 - Implement voice driver functions
 *
 * \author Benjamin Merrick
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************
#include "drv_voice.h"
#include "hal_gpio.h"
#include "hal_rcc.h"

//*********************************************************************
/**
 * \brief 240 milliseconds voice module initialization time
 */
//*********************************************************************
#define VOICE_MODULE_INIT_TIME 240

//*********************************************************************
/**
 * \brief Set Port A for voice clock
 */
//*********************************************************************
#define VOICE_CLOCK_PORT       &porta

//*********************************************************************
/**
 * \brief Set Pin 5 for voice clock
 */
//*********************************************************************
#define VOICE_CLOCK_PIN        5

//*********************************************************************
/**
 * \brief Set Port A for voice data
 */
//*********************************************************************
#define VOICE_DATA_PORT        &porta

//*********************************************************************
/**
 * \brief Set Pin 10 for voice data
 */
//*********************************************************************
#define VOICE_DATA_PIN         12

//*********************************************************************
/**
 * \brief List of possible voice options to be output
 */
//*********************************************************************
static uint8_t voice_data[number_of_voices] = {
    0x12, // voice_fire
    0x56, // voice_co
    0x0a, // voice_hush_active
    0x5a, // voice_co_prev_det
    0xff, // voice_push_test_btn
    0x5e, // voice_low_battery - Instead of "Low Battery", say "Replace Alarm"
    0x58, // voice_smk_prev_det
    0x5e, // voice_replace_alarm
    0x50, // voice_too_much_smk

    /* MORPHEUS VOICE CHIP DOES NOTE HAVE FAULT OR FRENCH VOICES LOADED */

    0x5c, // voice_co_sens_error  - say "Error, see troubleshooting guide"
    0x5c, // voice_smk_sens_error
    0x5c, // voice_intcon_error
    0x5c, // voice_memory_error
    0x78, // voice_activate_battery

    0xff, // voice_fr_fire
    0xff, // voice_fr_co
    0xff, // voice_fr_hush_active
    0xff, // voice_fr_co_prev_det
    0xff, // voice_fr_push_test_btn
    0xff, // voice_fr_low_battery
    0xff, // voice_fr_smk_prev_det
    0xff, // voice_fr_replace_alarm
    0xff, // voice_fr_too_much_smk
    0xff, // voice_fr_co_sens_error
    0xff, // voice_fr_smk_sens_error
    0xff, // voice_fr_intcon_error
    0xff, // voice_fr_memory_error
    0xff, // voice_fr_activate_battery

    0x00, // voice_english_selected

    0x02, // voice_ready_to_connect
    0x04, // voice_setup_complete
    0x06, // voice_devices_connected
    0x08, // voice_success
    0x0c, // voice_no_devices_found
    0x46, // voice_search_for_devices
    0x4c, // voice_reset_wireless
    0x4e, // voice_now_connected
    0x52, // voice_not_connected
    0x62, // voice_connection_lost

    0x16, // voice_one
    0x18, // voice_two
    0x1a, // voice_three
    0x1c, // voice_four
    0x1e, // voice_five
    0x20, // voice_six
    0x22, // voice_seven
    0x24, // voice_eight
    0x26, // voice_nine
    0x28, // voice_ten
    0x2a, // voice_eleven
    0x2c, // voice_twelve
    0x2e, // voice_thirteen
    0x30, // voice_fourteen
    0x32, // voice_fifteen
    0x34, // voice_sixteen
    0x36, // voice_seventeen
    0x38, // voice_eighteen
    0x3a, // voice_nineteen
    0x3c, // voice_twenty
    0x3e, // voice_twenty_one
    0x40, // voice_twenty_two
    0x42, // voice_twenty_three
    0x44, // voice_twenty_four

    0x0e, // voice_hush_cancel
    0x10, // voice_temporary_silence
    0x14, // voice_test_cancel
    0x48, // voice_testing_very_loud
    0x4a, // voice_test_complete
    0x54, // voice_press_to_cancel
    0x5c, // voice_error_see_guide
    0x60, // voice_press_to_silence

    0x64, // voice_emergency
    0x66, // voice_weather
    0x68, // voice_water_leak
    0x6a, // voice_explosive_gas
    0x6c, // voice_temperature_drop
    0x6e, // voice_intruder

    0x70, // voice_tone_sonar_ping
    0x71, // voice_tone_button_press
    0x72, // voice_tone_network_close
    0x73, // voice_tone_network_error
    0x74, // voice_tone_power_on
    0x75, // voice_tone_push_to_test
    0x76, // voice_tone_out_of_box
    0x78  // voice_tone_activate_batt
};

//*********************************************************************
/**
 * \brief Length of voice messages based on message output
 */
//*********************************************************************
static uint8_t voice_playback_length[number_of_voices] = {
    6,  // voice_fire
    22, // voice_co
    14, // voice_hush_active
    24, // voice_co_prev_det
    15, // voice_push_test_btn
    15, // voice_low_battery
    18, // voice_smk_prev_det
    11, // voice_replace_alarm
    28, // voice_too_much_smk
    25, // voice_co_sens_error
    25, // voice_smk_sens_error
    25, // voice_intcon_error
    25, // voice_memory_error
    15, // voice_activate_battery

    15, // voice_fr_fire
    15, // voice_fr_co
    15, // voice_fr_hush_active
    15, // voice_fr_co_prev_det
    15, // voice_fr_push_test_btn
    15, // voice_fr_low_battery
    15, // voice_fr_smk_prev_det
    15, // voice_fr_replace_alarm
    15, // voice_fr_too_much_smk
    15, // voice_fr_co_sens_error
    15, // voice_fr_smk_sens_error
    15, // voice_fr_intcon_error
    15, // voice_fr_memory_error
    15, // voice_fr_activate_battery

    19, // voice_english_selected

    34, // voice_ready_to_connect
    14, // voice_setup_complete
    14, // voice_devices_connected
    9,  // voice_success
    16, // voice_no_devices_found
    20, // voice_search_for_devices
    20, // voice_reset_wireless
    12, // voice_now_connected
    12, // voice_not_connected
    12, // voice_connection_lost

    5,  // voice_one
    5,  // voice_two
    5,  // voice_three
    6,  // voice_four
    6,  // voice_five
    6,  // voice_six
    6,  // voice_seven
    6,  // voice_eight
    7,  // voice_nine
    5,  // voice_ten
    7,  // voice_eleven
    6,  // voice_twelve
    8,  // voice_thirteen
    8,  // voice_fourteen
    9,  // voice_fifteen
    10, // voice_sixteen
    10, // voice_seventeen
    8,  // voice_eighteen
    9,  // voice_nineteen
    6,  // voice_twenty
    9,  // voice_twenty_one
    9,  // voice_twenty_two
    9,  // voice_twenty_three
    10, // voice_twenty_four

    13, // voice_hush_cancel
    16, // voice_temporary_silence
    11, // voice_test_cancel
    27, // voice_testing_very_loud
    12, // voice_test_complete
    20, // voice_press_to_cancel
    25, // voice_error_see_guide
    23, // voice_press_to_silence

    9,  // voice_emergency
    21, // voice_weather
    14, // voice_water_leak
    18, // voice_explosive_gas
    15, // voice_temperature_drop
    13, // voice_intruder

    7,  // voice_tone_sonar_ping
    4,  // voice_tone_button_press
    6,  // voice_tone_network_close
    15, // voice_tone_network_error
    9,  // voice_tone_power_on
    6,  // voice_tone_push_to_test
    11, // voice_tone_out_of_box
    15  // voice_tone_activate_batt
};

//*********************************************************************
/**
 * \brief Initialize voice power to be used for other voice functions
 */
//*********************************************************************
void voice_init(void)
{
    gpio_set_pin_mode(VOICE_CLOCK_PORT, gpio_mode_output, VOICE_CLOCK_PIN);
    gpio_pull_updown_mode(VOICE_CLOCK_PORT, pupd_none, VOICE_CLOCK_PIN);
    voice_clk_lo();

    gpio_set_pin_mode(VOICE_DATA_PORT, gpio_mode_output, VOICE_DATA_PIN);
    gpio_pull_updown_mode(VOICE_DATA_PORT, pupd_none, VOICE_DATA_PIN);
    voice_data_lo();
}

//*********************************************************************
/**
 * \brief Sets voice power to on so voice messages can be output
 */
//*********************************************************************
void voice_pwr_on(void)
{
}

//*********************************************************************
/**
 * \brief Sets voice power to off so voice messages can't be output
 */
//*********************************************************************
void voice_pwr_off(void)
{
}

//*********************************************************************
/**
 * \brief Sets voice clock to high so a clock value can be retained
 */
//*********************************************************************
void voice_clk_hi(void)
{
    gpio_drive_high(VOICE_CLOCK_PORT, VOICE_CLOCK_PIN);
}

//*********************************************************************
/**
 * \brief Sets voice clock to low so a clock value can't be retained
 */
//*********************************************************************
void voice_clk_lo(void)
{
    gpio_drive_low(VOICE_CLOCK_PORT, VOICE_CLOCK_PIN);
}

//*********************************************************************
/**
 * \brief Sets voice data to high so data can be sent with a voice message
 */
//*********************************************************************
void voice_data_hi(void)
{
    gpio_drive_high(VOICE_DATA_PORT, VOICE_DATA_PIN);
}

//*********************************************************************
/**
 * \brief Sets voice data to low so data can't be sent with a voice message
 */
//*********************************************************************
void voice_data_lo(void)
{
    gpio_drive_low(VOICE_DATA_PORT, VOICE_DATA_PIN);
}

//*********************************************************************
/**
 * \brief Get the voice data value from an existing list of options
 * \param voice THe voice message that will be selected to output
 * \returns voice data[voice] to be output
 */
//*********************************************************************
uint8_t get_voice_data(voice_message voice)
{
    return voice_data[voice];
}

//*********************************************************************
/**
 * \brief Set startup time for voice module
 * \returns Initialized time value for voice startup
 */
//*********************************************************************
uint16_t get_voice_startup_time(void)
{
    return VOICE_MODULE_INIT_TIME;
}

//*********************************************************************
/**
 * \brief Set voice playback time to space out voice messages
 * \param voice THe voice message that will be selected to output
 * \returns voice_playback_length[voice] length of time to output voice info
 */
//*********************************************************************
uint16_t get_voice_playback_time(voice_message voice)
{
    return voice_playback_length[voice] * 100;
}
