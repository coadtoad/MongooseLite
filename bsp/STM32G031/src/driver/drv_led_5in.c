//*********************************************************************
/*! \file drv_led.c
 *
 * \brief Kidde: Mongoose-fw - Implement the LED driver functions
 *
 * \author
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************

#include "drv_led.h"
#include "drv_sleep.h"
#include "hal_gpio.h"
#include "hal_rcc.h"

/**
 * \def AMBER_LED_PIN
 * \brief Used to configure amber LED pin.
 */
#define AMBER_LED_PIN  6

/**
 * \def AMBER_LED_PORT
 * \brief Used to configure amber LED.
 */
#define AMBER_LED_PORT &porta

/**
 * \def GREEN_LED_PIN
 * \brief Used to configure green LED pin.
 */
#define GREEN_LED_PIN  7

/**
 * \def GREEN_LED_PORT
 * \brief Used to configure green LED.
 */
#define GREEN_LED_PORT &porta

/**
 * \def RED_LED_PIN
 * \brief Used to configure RED LED pin.
 */
#define RED_LED_PIN    0

/**
 * \def RED_LED_PORT
 * \brief Used to configure RED LED.
 */
#define RED_LED_PORT   &portb

void led_init(void)
{
    // Enable GPIO port clock
    rcc_enable_gpiob_clock(true);

    // Initialize amber LED
    // Set the pin high first to prevent a glitch during initialization.
    gpio_drive_high(AMBER_LED_PORT, AMBER_LED_PIN);
    gpio_set_output(AMBER_LED_PORT, AMBER_LED_PIN);
    gpio_set_output_type(AMBER_LED_PORT, gpio_out_type_pp, AMBER_LED_PIN);
    gpio_set_pin_speed(AMBER_LED_PORT, pinspeed_very_low, AMBER_LED_PIN);
    gpio_pull_updown_mode(AMBER_LED_PORT, pupd_none, AMBER_LED_PIN);

    // Initialize green LED
    // Set the pin high first to prevent a glitch during initialization.
    gpio_drive_high(GREEN_LED_PORT, GREEN_LED_PIN);
    gpio_set_output(GREEN_LED_PORT, GREEN_LED_PIN);
    gpio_set_output_type(GREEN_LED_PORT, gpio_out_type_pp, GREEN_LED_PIN);
    gpio_set_pin_speed(GREEN_LED_PORT, pinspeed_very_low, GREEN_LED_PIN);
    gpio_pull_updown_mode(GREEN_LED_PORT, pupd_none, GREEN_LED_PIN);

    // Initialize red LED
    // Set the pin high first to prevent a glitch during initialization.
    gpio_drive_high(RED_LED_PORT, RED_LED_PIN);
    gpio_set_output(RED_LED_PORT, RED_LED_PIN);
    gpio_set_output_type(RED_LED_PORT, gpio_out_type_pp, RED_LED_PIN);
    gpio_set_pin_speed(RED_LED_PORT, pinspeed_very_low, RED_LED_PIN);
    gpio_pull_updown_mode(RED_LED_PORT, pupd_none, RED_LED_PIN);
}

// LEDs are inverted logic, set HI to turn off
void amber_led_on(void)
{
    gpio_drive_low(AMBER_LED_PORT, AMBER_LED_PIN);
    sleep_set_sleep_flag(SLEEP_MODE_LED);
}

void amber_led_off(void)
{
    gpio_drive_high(AMBER_LED_PORT, AMBER_LED_PIN);
    sleep_clear_sleep_flag(SLEEP_MODE_LED);
}

void amber_led_pwm_begin(void)
{
}

void amber_led_pwm_end(void)
{
}

void amber_led_pwm(uint8_t const duty)
{
    (void)duty;
}

void red_led_on(void)
{
    gpio_drive_low(RED_LED_PORT, RED_LED_PIN);
    sleep_set_sleep_flag(SLEEP_MODE_LED);
}

void red_led_off(void)
{
    gpio_drive_high(RED_LED_PORT, RED_LED_PIN);
    sleep_clear_sleep_flag(SLEEP_MODE_LED);
}

void red_led_pwm_begin(void)
{
}

void red_led_pwm_end(void)
{
}

void red_led_pwm(uint8_t const duty)
{
    (void)duty;
}

void green_led_on(void)
{
    gpio_drive_low(GREEN_LED_PORT, GREEN_LED_PIN);
    sleep_set_sleep_flag(SLEEP_MODE_LED);
}

void green_led_off(void)
{
    gpio_drive_high(GREEN_LED_PORT, GREEN_LED_PIN);
    sleep_clear_sleep_flag(SLEEP_MODE_LED);
}

void green_led_pwm_begin(void)
{
}

void green_led_pwm_end(void)
{
}

void green_led_pwm(uint8_t const duty)
{
    (void)duty;
}

void blue_led_on(void)
{
}

void blue_led_off(void)
{
}
