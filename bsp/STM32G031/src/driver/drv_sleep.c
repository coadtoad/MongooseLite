#include "drv_sleep.h"
#include "drv_serial.h"
#include "drv_timer.h"
#include "hal_gpio.h"
#include "hal_nvic.h"
#include "hal_pwr.h"
#include "hal_usart.h"
#include "main.h"

static uint8_t sleep_mode = 0; /**< Holds flags that control low power mode entry */

void enter_low_power(void)
{
    // Clear the wakeup flags before entering either low power mode.
    clear_wkup_pin_standby();
    clear_wkup_pin(WKUP_FLAG_TEST_BUTTON);

    // If the serial port is connected, we run in sleep mode
    if (serial_enabled() || (bool)sleep_mode)
    {
        nvic_enable_irq(irq_rtc_tamp);
        enter_sleep_mode();
    }
    else
    {
        // No task has reqested stop mode, enter standby.
        enter_standby_mode();
    }
}

void sleep_clear_sleep_flag(sleep_mode_flags const flag)
{
    /** @todo Do we need a critical section here? */
    sleep_mode &= ~flag;
}

void sleep_set_sleep_flag(sleep_mode_flags const flag)
{
    /** @todo Do we need a critical section here? */
    sleep_mode |= flag;
}
