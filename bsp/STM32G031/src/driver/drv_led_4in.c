//*********************************************************************
/*! \file drv_led.c
 *
 * \brief Kidde: Mongoose-fw - Implement the LED driver functions
 *
 * \author
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************

#include "drv_led.h"
#include "drv_sleep.h"
#include "hal_gpio.h"
#include "hal_rcc.h"

/**
 * \def RED_LED_PIN
 * \brief Used to configure RED LED pin.
 */
#define RED_LED_PIN  0

/**
 * \def RED_LED_PORT
 * \brief Used to configure RED LED.
 */
#define RED_LED_PORT &portb

void led_init(void)
{
    // Enable GPIO port clock
    rcc_enable_gpiob_clock(true);

    // Initialize RED LED.
    // Set the pin high first to prevent a glitch during initialization.
    gpio_drive_high(RED_LED_PORT, RED_LED_PIN);
    gpio_set_output(RED_LED_PORT, RED_LED_PIN);
    gpio_set_output_type(RED_LED_PORT, gpio_out_type_pp, RED_LED_PIN);
    gpio_set_pin_speed(RED_LED_PORT, pinspeed_very_low, RED_LED_PIN);
    gpio_pull_updown_mode(RED_LED_PORT, pupd_none, RED_LED_PIN);
}

// LEDs are inverted logic, set HI to turn off
void amber_led_on(void)
{
    // Amber drives red LED to avoid having to have a separate set of expression tables for 4" Iris
    red_led_on();
}

void amber_led_off(void)
{
    // Amber drives red LED to avoid having to have a separate set of expression tables for 4" Iris
    red_led_off();
}

void amber_led_pwm_begin(void)
{
}

void amber_led_pwm_end(void)
{
}

void amber_led_pwm(uint8_t const duty)
{
    (void)duty;
}

void red_led_on(void)
{
    gpio_drive_low(RED_LED_PORT, RED_LED_PIN);
    sleep_set_sleep_flag(SLEEP_MODE_LED);
}

void red_led_off(void)
{
    gpio_drive_high(RED_LED_PORT, RED_LED_PIN);
    sleep_clear_sleep_flag(SLEEP_MODE_LED);
}

void red_led_pwm_begin(void)
{
}

void red_led_pwm_end(void)
{
}

void red_led_pwm(uint8_t const duty)
{
    (void)duty;
}

void green_led_on(void)
{
    // Green drives red LED to avoid having to have a separate set of expression tables for 4" Iris
    red_led_on();
}

void green_led_off(void)
{
    // Green drives red LED to avoid having to have a separate set of expression tables for 4" Iris
    red_led_off();
}

void green_led_pwm_begin(void)
{
}

void green_led_pwm_end(void)
{
}

void green_led_pwm(uint8_t const duty)
{
    (void)duty;
}

void blue_led_on(void)
{
    // do not turn on red led here. no blue led on 4" devices
}

void blue_led_off(void)
{
    // do not turn on red led here. no blue led on 4" devices
}
