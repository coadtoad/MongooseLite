#include "arm_compat.h"
#include "critical_section.h"

void critical_section(void (*section)(void *), void *data)
{
    unsigned const previously_masked = __disable_irq();
    section(data);
    if (!previously_masked)
    {
        __enable_irq();
    }
}
