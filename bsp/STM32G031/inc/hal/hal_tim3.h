//******************************************************************
/*! \file hal_tim3.h
 *
 * \brief Kidde: Mongoose-fw - Declare the TIM3 HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#ifndef HAL_INCLUDE_HAL_TIM3_H_
#define HAL_INCLUDE_HAL_TIM3_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Initialize TIM3
 */
//*********************************************************************
void tim3_init(void);

//*********************************************************************
/**
 * \brief Start the 8kHz PWM output on TIM3 Channel 4
 */
//*********************************************************************
void start_8khz_pwm_output(void);

//*********************************************************************
/**
 * \brief Stop the 8kHz PWM output on TIM3 Channel 4
 */
//*********************************************************************
void stop_8khz_pwm_output(void);

#endif
