/**
 * @file hal_electronic_signature.h
 * @brief Declare electronic signature HAL functions
 */
#ifndef HAL_INCLUDE_HAL_ELECTRONIC_SIGNATURE_H_
#define HAL_INCLUDE_HAL_ELECTRONIC_SIGNATURE_H_

#include <stdint.h>

/**
 * @brief Fill @p buffer with the UID
 *
 * @param[in] buffer Pointer to 24 characters that will be filled with the UID
 *
 * @note @p buffer must point to a preallocated buffer of 24 characters
 */
void uid_to_hex(char *const buffer);

#endif
