/**
 * @file hal_exti.h
 * @brief Declare the EXTI HAL functions
 */
#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Define the exti mux numbering.
 */
typedef enum
{
    exti0,
    exti1,
    exti2,
    exti3,
    exti4,
    exti5,
    exti6,
    exti7,
    exti8,
    exti9,
    exti10,
    exti11,
    exti12,
    exti13,
    exti14,
    exti15,
} exti_mux_num;

/**
 * @brief Define the exti lines for edge triggering.
 */
typedef enum
{
    exti_line0,
    exti_line1,
    exti_line2,
    exti_line3,
    exti_line4,
    exti_line5,
    exti_line6,
    exti_line7,
    exti_line8,
    exti_line9,
    exti_line10,
    exti_line11,
    exti_line12,
    exti_line13,
    exti_line14,
    exti_line15,
    pvd
} exti_edge_trigger;

/**
 * @brief Constants for configuring wakeup with interrupt/event.
 */
typedef enum
{
    exti0_wkp     = 0,
    exti1_wkp     = 1,
    exti2_wkp     = 2,
    exti3_wkp     = 3,
    exti4_wkp     = 4,
    exti5_wkp     = 5,
    exti6_wkp     = 6,
    exti7_wkp     = 7,
    exti8_wkp     = 8,
    exti9_wkp     = 9,
    exti10_wkp    = 10,
    exti11_wkp    = 11,
    exti12_wkp    = 12,
    exti13_wkp    = 13,
    exti14_wkp    = 14,
    exti15_wkp    = 15,
    rtc_wakeup    = 19,
    tamp_wakeup   = 21,
    i2c2_wakeup   = 22,
    i2c1_wakeup   = 23,
    usart1_wakeup = 25,
    lse_css       = 31
} exti_wakeup;

/**
 * @brief Define the port constants to configure the exti mux.
 */
typedef enum
{
    cfg_porta = 0,
    cfg_portb = 1,
    cfg_portc = 2,
    cfg_portd = 3,
    cfg_portf = 5
} exti_port_cfg;

/**
 * @brief Constants to set or clear bits.
 */
typedef enum
{
    exti_disable = 0,
    exti_enable  = 1,
} exti_bit_cfg;

/**
 * @brief Enable rising edge interrupt on pin @p line_num
 *
 * @param[in] line_num the pin to enable rising edge interrupt on
 */
void exti_enable_rising_edge_interrupt(exti_edge_trigger const line_num);

/**
 * @brief Disable rising edge interrupt on pin @p line_num
 *
 * @param[in] line_num the pin to disable rising edge interrupt on
 */
void exti_disable_rising_edge_interrupt(exti_edge_trigger const line_num);

/**
 * @brief Enable falling edge interrupt on pin @p line_num
 *
 * @param[in] line_num the pin to enable falling edge interrupt on
 */
void exti_enable_falling_edge_interrupt(exti_edge_trigger const line_num);

/**
 * @brief Disable falling edge interrupt on pin @p line_num
 *
 * @param[in] line_num the pin to disable falling edge interrupt on
 */
void exti_disable_falling_edge_interrupt(exti_edge_trigger const line_num);

/**
 * @brief Triggers a rising edge event on the line
 *
 * @param[in] line_num The line to set (0-16).
 */
void exti_force_rising_edge_interrupt(exti_edge_trigger const line_num);

/**
 * @brief Returns if @p line_num has a pending rising edge interrupt
 *
 * @param[in] line_num The line to read (0-15).
 * @return if @p line_num has a pending rising edge interrupt
 */
bool exti_rising_edge_occured(exti_edge_trigger const line_num);

/**
 * @brief Returns if @p line_num has a pending falling edge interrupt
 *
 * @param[in] line_num The line to read (0-15).
 * @return if @p line_num has a pending falling edge interrupt
 */
bool exti_falling_edge_occured(exti_edge_trigger const line_num);

/**
 * @brief Clear the single rising edge pending bit
 *
 * @param[in] line_num The line to set (0-15).
 */
void exti_clear_rising_edge(exti_edge_trigger const line_num);

/**
 * @brief Clear the single falling edge pending bit
 *
 * @param[in] line_num The line to set (0-15).
 */
void exti_clear_falling_edge(exti_edge_trigger const line_num);

/**
 * @brief Configures the port of the exti mux.
 *
 * @param[in] exti_mux The exti mux to configure, @ref exti_mux_num.
 * @param[in] port The port to which to configure the mux.
 */
void exti_config_port(exti_mux_num const exti_mux, exti_port_cfg const port);

/**
 * @brief Configures a wakeup source to be wake on interrupt.
 *
 * @param[in] wake_src The wakeup source to configure, @ref exti_wakeup.
 * @param[in] mode Enables or disables wake function @ref exti_bit_cfg.
 */
void exti_config_wakeonint_mask(exti_wakeup const wake_src, exti_bit_cfg const mode);

/**
 * @brief Configures a wakeup source to be wake on event.
 *
 * @param[in] wake_src The wakeup source to configure, @ref exti_wakeup.
 * @param[in] mode Enables or disables wake function @ref exti_bit_cfg.
 */
void exti_config_wakeonevt_mask(exti_wakeup const wake_src, exti_bit_cfg const mode);
