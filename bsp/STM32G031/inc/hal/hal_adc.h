/**
 * @file hal_adc.h
 * @brief Declare ADC HAL functions
 */
#ifndef HAL_INCLUDE_HAL_ADC_H_
#define HAL_INCLUDE_HAL_ADC_H_

#include <stdint.h>

/**
 * @brief Used to configure ADC resolution
 */
typedef enum
{
    resolution_12bits,
    resolution_10bits,
    resolution_8bits,
    resolution_6bits
} resolution_type;

/**
 * @brief Initialize the ADC
 */
void adc_init(void);

/**
 * @brief Configure the channel for ADC conversion
 *
 * @param[in] channel The channel to convert the voltage
 */
void adc_set_channel(uint8_t const channel);

/**
 * @brief Take one sample on @p channel
 *
 * @returns The value of the measured signal
 */
uint16_t adc_sample(void);

/**
 * @brief Enable the ADC
 * @param resolution The resolution of the conversion of data either in 6, 8, 10, or 12 bits
 */
void adc_enable(resolution_type resolution);

/**
 * @brief Disable the ADC
 */
void adc_disable(void);

/**
 * @brief Calibrate ADC
 */
void adc_calibrate(void);

#endif
