//*********************************************************************
/*! \file hal_rtc.h
 *
 * \brief Kidde: Mongoose-fw - Declare the RTC HAL functions.
 *
 * \author
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*********************************************************************

#ifndef HAL_INCLUDE_HAL_RTC_H_
#define HAL_INCLUDE_HAL_RTC_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Used to configure the clock for periodic wake up timer.
 */
//*********************************************************************
typedef enum
{
    rtc_by_16,
    rtc_by_8,
    rtc_by_4,
    rtc_by_2
} wake_up_clock;

//*********************************************************************
/**
 * \brief Initialize RTC
 */
//*********************************************************************
bool rtc_init(void);

//*********************************************************************
/**
 * \brief Get the number of milliseconds since the minute rolled over
 *
 * \returns the number of milliseconds since the minute rolled over
 */
//*********************************************************************
uint16_t rtc_get_subminute(void);

//*********************************************************************
/**
 * \brief Get the number of milliseconds since the minute rolled over
 *
 * \returns the number of milliseconds since the minute rolled over
 */
//*********************************************************************
uint16_t rtc_get_subminute_with_bypshad(void);

//*********************************************************************
/**
 * \brief Set ALARM A for subminute interrupt.
 *
 * \param tick The number of milliseconds since the minute rollover for the interrupt
 */
//*********************************************************************
void rtc_set_alarma_subminute(uint16_t const tick);

//*********************************************************************
/**
 * \brief Set ALARM B for subsecond interrupt.
 *
 * \param tick The no.of ticks to be given for the interrupt.
 */
//*********************************************************************
void rtc_set_alarmb_subsecond(uint16_t const tick);

/**
 * \brief Set wake up timer
 * \param wake_up_counter The counter value to set for the timer.
 *        if it is for 5 sec wake up, counter value = 5 sec / 1 clock tick.
 *        for eg: if clock is rcc_by_16 1 tick ~ 16/32k = 0.0005, counter value ~ 5/0.0005 = 10000.
 * \param clock The clock value to set for the timer.
 */
//*********************************************************************
void rtc_set_wake_up_timer(unsigned const wake_up_counter, wake_up_clock const clock);

//*********************************************************************
/**
 * \brief Get wake up timer value
 */
//*********************************************************************
unsigned rtc_get_wake_up_timer(void);

//*********************************************************************
/**
 * \brief Disable ALARM A
 */
//*********************************************************************
void rtc_disable_alarma(void);

//*********************************************************************
/**
 * \brief Disable ALARM B
 */
//*********************************************************************
void rtc_disable_alarmb(void);

//*********************************************************************
/**
 * \brief Disable wake up timer
 */
//*********************************************************************
void rtc_disable_wake_up_timer(void);

//*********************************************************************
/**
 * \brief ISR routine for ALARM A interrupt.
 * \note The code that uses RTC HAL should implement this function as needed.
 */
//*********************************************************************
void rtc_alarma_irq(void);

//*********************************************************************
/**
 * \brief ISR routine for ALARM B interrupt.
 * \note The code that uses RTC HAL should implement this function as needed.
 */
//*********************************************************************
void rtc_alarmb_irq(void);

//*********************************************************************
/**
 * \brief ISR routine for wake up timer interrupt.
 * \note The code that uses RTC HAL should implement this function as needed.
 */
//*********************************************************************
void rtc_wake_up_timer_irq(void);

//*********************************************************************
/**
 * \brief Resets the RTC to time 0
 */
//*********************************************************************
void rtc_reset_clock(void);

void rtc_enable_wakeup_timer(uint16_t const setting);
#endif
