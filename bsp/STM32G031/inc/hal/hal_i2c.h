//******************************************************************
/*! \file hal_i2c.h
 *
 * \brief Kidde: Mongoose-fw - Declare the I2C HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#ifndef HAL_INCLUDE_HAL_I2C_H_
#define HAL_INCLUDE_HAL_I2C_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Configure and enable the I2C peripheral clock
 * \param fast_mode Sets I2C speed to 400kHz if true or to 100kHz if false
 */
//*********************************************************************
void i2c_set_fast_mode_baud(bool const fast_mode);

//*********************************************************************
/**
 * \brief Enable I2C peripheral
 */
//*********************************************************************
void i2c_enable(void);

//*********************************************************************
/**
 * \brief Waits for the transmit register to be clear then writes a byte of data
 * \param data The single byte of data to be written
 * \param stop Sends STOP after write/read transmit completes if true. Holds line if false
 * \returns True if the operation was a success or false if an error occurred during operation
 */
//*********************************************************************
bool i2c_master_write_byte(uint8_t const data, bool const stop);

//*********************************************************************
/**
 * \brief Enable I2C peripheral
 * \param address Own device address
 * \param ten_bit_mode Own device address is 10-bits if true, or 7-bits if false
 */
//*********************************************************************
void i2c_enable_slave(uint16_t const address, bool const ten_bit_mode);

//*********************************************************************
/**
 * \brief Read the addressed matched bit in the I2C status register
 * \returns True if slave address matched
 */
//*********************************************************************
bool i2c_get_slave_address_matched(void);

//*********************************************************************
/**
 * \brief Read the transfer direction bit in the I2C status register
 * \returns True if a read request was sent or false if a write request was sent
 */
//*********************************************************************
bool i2c_get_transfer_direction(void);

//*********************************************************************
/**
 * \brief Clears the address matched interrupt flag
 */
//*********************************************************************
void i2c_clear_address_matched_flag(void);

//*********************************************************************
/**
 * \brief Read the received data status bit in the I2C control register
 * \returns True if received data is stored in the received register or false if the register is empty
 */
//*********************************************************************
bool i2c_get_received_status(void);

//*********************************************************************
/**
 * \brief Read the value of the received data register
 * \returns The byte stored in the received data register
 */
//*********************************************************************
uint8_t i2c_read_receive_register(void);

//*********************************************************************
/**
 * \brief Read the stop detection bit in the I2C status register
 * \returns True if a stop condition was detected
 */
//*********************************************************************
bool i2c_get_stop_status(void);

//*********************************************************************
/**
 * \brief Writes and/or reads a series of bytes to/from a target module address
 * \param module_address Specifies the address to initiate an I2C communication with
 * \param output Pointer to the array of bytes to transmit
 * \param output_size Number of bytes to transmit
 * \param input Pointer to the array to store the read bytes
 * \param input_size Number of bytes to read
 * \returns True if the operation was a success or false if an error occurred during operation
 * \note This function left-shifts the \p module_address parameter by 1 to send as a 7-bit address
 * \note Restarts are sent with the slave address instead of repeating Stop and Start. Stop is sent during the final
 * Write/Read operation
 */
//*********************************************************************
bool i2c_write_read(uint8_t const module_address, uint8_t const *const output, size_t const output_size,
                    uint8_t *const input, size_t const input_size);

#endif
