/**
 * @file hal_interrupt.h
 * @brief Declare all interrupt handlers with appropriate attributes
 */
#ifndef BSP_STM32G031_INC_HAL_HAL_INTERRUPT_H_
#define BSP_STM32G031_INC_HAL_HAL_INTERRUPT_H_

/**
 * @brief The entry point for program execution
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * Prior to this function being called, .bss and .data are initialized.
 *
 * @returns The return value is ignored.
 */
int main(void);

/**
 * @brief The first function that is executed when the device powers on
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.  It is expected that this will not
 * be redefined.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | -        | -3       | fixed            | Reset   | Reset       | 0x0000_0003 |
 *
 * This
 *  -# Initializes .data
 *  -# Initializes .bss
 *  -# Calls main()
 *  -# Spin loop if main ever returns
 */
void reset_handler(void) __attribute__((noreturn));

/**
 * @brief NMI interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | -        | -2       | fixed            | NMI_Handler | Non maskable interrupt.  The SRAM parity err., Flash ECC
 * double err., HSE CSS and LSE CSS are linked to the NMI vector. | 0x0000_0008 |
 */
void nmi_handler(void) __attribute__((interrupt));

/**
 * @brief Hard fault interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | -        | -1       | fixed            | HardFault_Handler | All class of fault | 0x0000_000C |
 */
void hardfault_handler(void) __attribute__((interrupt));

/**
 * @brief SVC interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | -        | 3        | settable         | SVC_Handler | System service call via SWI instruction | 0x0000_002C |
 */
void svc_handler(void) __attribute__((interrupt));

/**
 * @brief PendSV interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | -        | 5        | settable         | PendSV_Handler | Pendable request for system service | 0x0000_0038 |
 */
void pendsv_handler(void) __attribute__((interrupt));

/**
 * @brief SysTick interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | -        | 6        | settable         | SysTick_Handler | System tick timer | 0x0000_003C |
 */
void systick_handler(void) __attribute__((interrupt));

/**
 * @brief WWDG interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 0        | 7        | settable         | WWDG    | Window watchdog interrupt | 0x0000_0040 |
 */
void wwdg_handler(void) __attribute__((interrupt));

/**
 * @brief RTC/TAMP interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 2        | 9        | settable         | RTC/TAMP | RTC and TAMP interrupts (combined EXTI lines 19 & 21) |
 * 0x0000_0048 |
 */
void rtc_tamp_handler(void) __attribute__((interrupt));

/**
 * @brief Flash interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 3        | 10       | settable         | FLASH   | Flash global interrupt | 0x0000_004C |
 */
void flash_handler(void) __attribute__((interrupt));

/**
 * @brief RCC interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 4        | 11       | settable         | RCC     | RCC global interrupt | 0x0000_0050 |
 */
void rcc_handler(void) __attribute__((interrupt));

/**
 * @brief EXTI0 and EXTI1 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 5        | 12       | settable         | EXTI0_1 | EXTI line 0 & 1 interrupt | 0x0000_0054 |
 */
void exti0_1_handler(void) __attribute__((interrupt));

/**
 * @brief EXTI2 and EXTI3 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 6        | 13       | settable         | EXTI2_3 | EXTI line 2 & 3 interrupt | 0x0000_0058 |
 */
void exti2_3_handler(void) __attribute__((interrupt));

/**
 * @brief EXTI4 to EXTI15 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 7        | 14       | settable         | EXTI4_15 | EXTI line 4 to 15 interrupt | 0x0000_005C |
 */
void exti4_15_handler(void) __attribute__((interrupt));

/**
 * @brief DMA 1 channel 1 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 9        | 16       | settable         | DMA1_Channel1 | DMA1 channel 1 interrupt | 0x0000_0064 |
 */
void dma1_channel1_handler(void) __attribute__((interrupt));

/**
 * @brief DMA 1 channels 2 and 3 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 10       | 17       | settable         | DMA1_Channel2_3 | DMA1 channel 2 & 3 interrupts | 0x0000_0068 |
 */
void dma1_channel2_3_handler(void) __attribute__((interrupt));

/**
 * @brief DMA 1 channels 4 to 7, DMAMUX, and DMA 2 channels 1 to 5 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 11       | 18       | settable         | DMA1_Channel4_5_6_7 / DMAMUX / DMA2_Channel1_2_3_4_5 | DMA1 channel 4, 5,
 * 6, 7, DMAMUX, DMA2 channel 1, 2, 3, 4, 5 interrupts | 0x0000_006C |
 */
void dma1_channel4_7_dmamux_dma2_channel1_5_handler(void) __attribute__((interrupt));

/**
 * @brief DMA 1 channels 4 to 7, DMAMUX, and DMA 2 channels 1 to 5 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 12       | 19       | settable         | ADC     | ADC interrupt (ADC combined with EXTI 17 & 18) | 0x0000_0070 |
 */
void adc_handler(void) __attribute__((interrupt));

/**
 * @brief TIM1 break, update, trigger, and commutation interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 13       | 20       | settable         | TIM1_BRK_UP_TRG_COM | TIM1 break, update, trigger and commutation
 * interrupts | 0x0000_0074 |
 */
void tim1_brk_up_trg_com_handler(void) __attribute__((interrupt));

/**
 * @brief TIM1 capture compare interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 14       | 21       | settable         | TIM1_CC | TIM1 Capture Compare interrupt | 0x0000_0078 |
 */
void tim1_cc_handler(void) __attribute__((interrupt));

/**
 * @brief TIM3 and TIM4 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 16       | 23       | settable         | TIM3+TIM4 | TIM3 global interrupt | 0x0000_0080 |
 */
void tim3_4_handler(void) __attribute__((interrupt));

/**
 * @brief TIM6 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 17       | 24       | settable         | TIM6    | TIM6 global interrupt | 0x0000_0084 |
 */
void tim6_handler(void) __attribute__((interrupt));

/**
 * @brief TIM7 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 18       | 25       | settable         | TIM7    | TIM6 global interrupt | 0x0000_0088 |
 */
void tim7_handler(void) __attribute__((interrupt));

/**
 * @brief TIM14 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 19       | 26       | settable         | TIM14   | TIM14 global interrupt | 0x0000_008C |
 */
void tim14_handler(void) __attribute__((interrupt));

/**
 * @brief TIM15 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 20       | 27       | settable         | TIM15   | TIM15 global interrupt | 0x0000_0090 |
 */
void tim15_handler(void) __attribute__((interrupt));

/**
 * @brief TIM16 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 21       | 28       | settable         | TIM16   | TIM16 global interrupt | 0x0000_0094 |
 */
void tim16_handler(void) __attribute__((interrupt));

/**
 * @brief TIM17 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 22       | 29       | settable         | TIM17   | TIM17 global interrupt | 0x0000_0098 |
 */
void tim17_handler(void) __attribute__((interrupt));

/**
 * @brief I2C1 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 23       | 30       | settable         | I2C1    | I2C1 global interrupt (combined with EXTI 23) | 0x0000_009C |
 */
void i2c1_handler(void) __attribute__((interrupt));

/**
 * @brief I2C1 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 24       | 31       | settable         | I2C2 / I2C3 | I2C2 and I2C3 global interrupt | 0x0000_00A0 |
 */
void i2c2_3_handler(void) __attribute__((interrupt));

/**
 * @brief SPI1 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 25       | 32       | settable         | SPI1    | SPI1 global interrupt | 0x0000_00A4 |
 */
void spi1_handler(void) __attribute__((interrupt));

/**
 * @brief SPI2 and SPI3 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 26       | 33       | settable         | SPI2 / SPI3 | SPI2 global interrupt | 0x0000_00A8 |
 */
void spi2_3_handler(void) __attribute__((interrupt));

/**
 * @brief USART1 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 27       | 34       | settable         | USART1  | USART1 global interrupt (combined with EXTI 25) | 0x0000_00AC |
 */
void usart1_handler(void) __attribute__((interrupt));

/**
 * @brief USART2 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 28       | 35       | settable         | USART2  | USART2 global interrupt (combined with EXTI 26) | 0x0000_00B0 |
 */
void usart2_handler(void) __attribute__((interrupt));

/**
 * @brief USART3, USART4, USART5, and USART6 interrupt handler
 *
 * @note A weak symbol is already defined.  The weak symbol will be discarded by
 * the linker if it is redefined elsewhere.
 *
 * | Position | Priority | Type of priority | Acronym | Description | Address     |
 * | :------: | :------: | :--------------- | :------ | :---------- | :         : |
 * | 29       | 36       | settable         | USART3 / USART4 / USART5 / USART6  | USART3/4/5/6 global interrupt
 * (combined with EXTI 28) | 0x0000_00B4 |
 */
void usart3_6_handler(void) __attribute__((interrupt));

#endif
