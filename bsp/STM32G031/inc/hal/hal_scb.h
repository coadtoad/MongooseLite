//******************************************************************
/*! \file hal_scb.h
 *
 * \brief Kidde: Mongoose-fw - Declare HAL functions for Cortex-M0+
 *  system control block functions.
 * \note This is included as a placeholder.  There should be SCB
 *  functions in the future.
 *
 * \author Daniel Dilts
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef BSP_STM32G031_INC_HAL_HAL_SCB_H_
#define BSP_STM32G031_INC_HAL_HAL_SCB_H_

#endif
