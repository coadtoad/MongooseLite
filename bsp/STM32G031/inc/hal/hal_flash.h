/**
 * @file hal_flash.h
 * @brief Declare the Flash support functions.
 */
#ifndef HAL_FLASH_H
#define HAL_FLASH_H

#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Page definitions
 */
typedef enum
{
    page0,  /**< Starting address 0x08000000 */
    page1,  /**< Starting address 0x08000800  */
    page2,  /**< Starting address 0x08001000  */
    page3,  /**< Starting address 0x08001800  */
    page4,  /**< Starting address 0x08002000  */
    page5,  /**< Starting address 0x08002800  */
    page6,  /**< Starting address 0x08003000  */
    page7,  /**< Starting address 0x08003800  */
    page8,  /**< Starting address 0x08004000  */
    page9,  /**< Starting address 0x08004800  */
    page10, /**< Starting address 0x08005000  */
    page11, /**< Starting address 0x08005800  */
    page12, /**< Starting address 0x08006000  */
    page13, /**< Starting address 0x08006800  */
    page14, /**< Starting address 0x08007000  */
    page15, /**< Starting address 0x08007800  */
    page16, /**< Starting address 0x08008000  */
    page17, /**< Starting address 0x08008800  */
    page18, /**< Starting address 0x08009000  */
    page19, /**< Starting address 0x08009800  */
    page20, /**< Starting address 0x0800A000  */
    page21, /**< Starting address 0x0800A800  */
    page22, /**< Starting address 0x0800B000  */
    page23, /**< Starting address 0x0800B800  */
    page24, /**< Starting address 0x0800C000  */
    page25, /**< Starting address 0x0800C800  */
    page26, /**< Starting address 0x0800D000  */
    page27, /**< Starting address 0x0800D800  */
    page28, /**< Starting address 0x0800E000  */
    page29, /**< Starting address 0x0800E800  */
    page30, /**< Starting address 0x0800F000  */
    page31, /**< Starting address 0x0800F800  */
} pages;

/**
 * @brief Writes data from a RAM buffer to flash
 *
 * @param buffer Pointer to a buffer from which to write the data
 * @param flash_address The address in flash to begin writing.
 * @param size number of bytes to be written.
 * @note Buffer must be aligned on a word boundary or a hardfault will be produced.
 * @note flash_address must be double word aligned.
 * @return Returns true if successful.
 */
bool flash_write(unsigned *const buffer, unsigned *const flash_address, unsigned const size);

/**
 * @brief Erases a page of flash.
 *
 * @param page The page in flash to erase.
 *
 * @return Returns true if successful.
 */
bool flash_erase_page(pages const page);
void flash_unlock(void);
bool flash_set_rdp_level(const uint8_t level);
unsigned flash_get_optionbyte(void);

#endif
