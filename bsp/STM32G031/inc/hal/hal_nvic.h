//******************************************************************
/*! \file hal_nvic.c
 *
 * \brief Kidde: Mongoose-fw - NVIC function declarations
 *
 * Functions in this file are based on CMSIS functions as specified at
 * \link https://www.keil.com/pack/doc/CMSIS/Core/html/group__NVIC__gr.html
 * CMSIS NVIC \endlink.
 *
 * \author Daniel Dilts
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef BSP_STM32G031_INC_HAL_HAL_NVIC_H_
#define BSP_STM32G031_INC_HAL_HAL_NVIC_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Define the IRQ numbers
 * \note The negative numbers come from the NVIC CMSIS spec.
 *
 * The negative IRQ numbers are from ARM specifications.  The non-negative
 * IRQ numbers are from the STM32G031K8 documentation.
 */
//*********************************************************************
enum irq
{
    irq_nmi                                              = -14,
    irq_hard_fault                                       = -13,
    irq_svcall                                           = -5,
    irq_pendsv                                           = -2,
    irq_systick                                          = -1,
    irq_wwdg                                             = 0,
    irq_pvd                                              = 1,
    irq_rtc_tamp                                         = 2,
    irq_flash                                            = 3,
    irq_rcc_crs                                          = 4,
    irq_exti0_1                                          = 5,
    irq_exti2_3                                          = 6,
    irq_exti4_15                                         = 7,
    irq_ucpd1_ucpd2_usb                                  = 8,
    irq_dma1_channel1                                    = 9,
    irq_dma1_channel2_3                                  = 10,
    irq_dma1_channel4_5_6_7_dmamux_dma2_channel1_2_3_4_5 = 11,
    irq_adc_comp                                         = 12,
    irq_tim1_brk_up_trg_com                              = 13,
    irq_tim1_cc                                          = 14,
    irq_tim2                                             = 15,
    irq_tim3_tim4                                        = 16,
    irq_tim6_dac_lptim1                                  = 17,
    irq_tim7_lptim2                                      = 18,
    irq_tim14                                            = 19,
    irq_tim15                                            = 20,
    irq_tim16_fdcan_it0                                  = 21,
    irq_tim17_fdcan_it1                                  = 22,
    irq_i2c1                                             = 23,
    irq_i2c2_i2c3                                        = 24,
    irq_spi1                                             = 25,
    irq_spi2_spi3                                        = 26,
    irq_usart1                                           = 27,
    irq_usart2_lpuart2                                   = 28,
    irq_usart3_usart4_usart5_usart6_lpuart1              = 29,
    irq_cec                                              = 30,
    irq_aes_rng                                          = 31
};

void nvic_enable_irq(enum irq const irq);
bool nvic_get_enable_irq(enum irq const irq);
void nvic_disable_irq(enum irq const irq);
bool nvic_get_pending_irq(enum irq const irq);
void nvic_set_pending_irq(enum irq const irq);
void nvic_clear_pending_irq(enum irq const irq);
void nvic_set_priority(enum irq const irq, uint8_t const priority);
uint8_t nvic_get_priority(enum irq const irq);
void (*nvic_get_vector(enum irq const irq))(void);
void nvic_set_vector(enum irq const irq, void (*const vector)(void));
void nvic_system_reset(void);
void nvic_set_deepsleep(void);
void nvic_clear_deepsleep(void);

#endif
