//******************************************************************
/*! \file hal_pwr.h
 *
 * \brief Kidde: Mongoose-fw - Declare the PWR HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#ifndef HAL_INCLUDE_HAL_PWR_H_
#define HAL_INCLUDE_HAL_PWR_H_

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
    wkup_rising_edge  = 0,
    wkup_falling_edge = 1
} wkup_polarity;

typedef enum
{
    porta_pullup,
    porta_pulldown,
    portb_pullup,
    portb_pulldown,
    portc_pullup,
    portc_pulldown,
} pullupdown_control;

/**
 * \brief Low power modes.
 */
typedef enum
{
    mode_stop0,   //< Stop 0 mode
    mode_stop1,   //< Stop 1 mode
    mode_rsrvd,   //< Do not use
    mode_standby, //< Standby mode
    mode_shutdown //< Shutdown mode
} lpms_mode;

//*********************************************************************
/**
 * \brief Hardcoded address for the wakeup occurred function
 * \note This address is specified so we don't corrupt the stack when this is called from the reset vector.
 * \note 8000 is the 15th bit of sr1 and 7010 as offset for sr1.
 */
//*********************************************************************
#define ANY_WAKEUP_OCCURRED() ((bool)(*(unsigned *)0x40007010 & 0x00008000))

void pwr_enable_rtc_access(void);

void enable_wkup_pin(uint8_t const pin_number, wkup_polarity const polarity);

void disable_wkup_pin(uint8_t const pin_number);

bool wkup_occurred(uint8_t const pin_number);

void clear_wkup_pin(uint8_t const pin_number);

bool wkup_occurred_standby(void);

void clear_wkup_pin_standby(void);

void pwr_set_low_power(lpms_mode const mode);

void clear_wkup_pin_all(void);

void set_pullupdown(pullupdown_control const reg, unsigned setting);

void pwr_ram_on_in_standby(void);

void pwr_enable_pullupdown(void);

#endif
