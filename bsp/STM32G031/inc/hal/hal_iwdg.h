/**
 * @file hal_iwdg.h
 * @brief Declare the Watch Dog Timer HAL functions
 */
#ifndef HAL_IWDG_H
#define HAL_IWDG_H

#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Define the WDT prescaler values.
 */
typedef enum
{
    iwdg_divide_by_4,   /**< Divide clock by 4 */
    iwdg_divide_by_8,   /**< Divide clock by 8 */
    iwdg_divide_by_16,  /**< Divide clock by 16 */
    iwdg_divide_by_32,  /**< Divide clock by 32 */
    iwdg_divide_by_64,  /**< Divide clock by 64 */
    iwdg_divide_by_128, /**< Divide clock by 128 */
    iwdg_divide_by_256, /**< Divide clock by 256 */
} iwdg_prescale;

/**
 * @brief Resets the IWDG.
 *
 */
void iwdg_reset_timer(void);

/**
 * @brief Starts the IWDG.
 *
 */
void iwdg_start_timer(void);

/**
 * @brief Set the IWDG reload register.
 *
 * @param[in] val The value to set the reload register to.
 */
void iwdg_set_reload(uint16_t const val);

/**
 * @brief Set the IWDG prescaler
 *
 * @param[in] val The value to set the prescale to. @ref iwdg_prescale
 */
void iwdg_set_prescaler(iwdg_prescale const val);

/**
 * @brief Set the IWDG window register.
 *
 * @param[in] val The value to set the window register to.
 */
void iwdg_set_window(uint16_t const val);

#endif
