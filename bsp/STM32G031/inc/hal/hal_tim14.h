//******************************************************************
/*! \file hal_tim14.h
 *
 * \brief Kidde: Mongoose-fw - Declare the TIM14 HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#ifndef HAL_INCLUDE_HAL_TIM14_H_
#define HAL_INCLUDE_HAL_TIM14_H_

#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Initialize TIM14 to be a us timer
 */
void tim14_init(void);

//*********************************************************************
/**
 * \brief Start counting on TIM14 at a frequency of 1 count per microsecond
 * \param count Number of microseconds to count to
 * \note Frequency of TIM14 must be sourced from a 16MHz clock and prescaled to 1MHz
 */
//*********************************************************************
void tim14_start_count(uint16_t const count);

//*********************************************************************
/**
 * \brief Returns the update generation event status that is set when TIM14 has finished counting
 * \returns True if the count has completed or false otherwise
 */
//*********************************************************************
bool tim14_check_timer_expiration(void);

#endif
