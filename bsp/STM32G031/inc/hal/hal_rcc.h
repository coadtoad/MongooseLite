//******************************************************************
/*! \file hal_rcc.h
 *
 * \brief Kidde: Mongoose-fw - Declare the RCC HAL functions
 *
 * \author Brandon Widow
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#ifndef HAL_INCLUDE_HAL_RCC_H_
#define HAL_INCLUDE_HAL_RCC_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Reset flags.
 */
//*********************************************************************
typedef enum
{
    obl_rstf  = 0x01, ///< Option byte loader reset flag
    pin_rstf  = 0x02, ///< Pin reset flag
    pwr_rstf  = 0x04, ///< POR/PDR flag
    sft_rstf  = 0x08, ///< Software reset flag
    iwwg_rstf = 0x10, ///< Independent window watchdog reset flag
    wwdg_rstf = 0x20, ///< Window watchdog reset flag
    lpwr_rstf = 0x40, ///< Low-power reset flag
} rst_cause;

//*********************************************************************
/**
 * \brief Clock sources available to the LPTIM
 */
//*********************************************************************
typedef enum
{
    lptim_pclk,  ///< Used to set PCLK as the LPTIM clock source
    lptim_lsi,   ///< Used to set LSI as the LPTIM clock source
    lptim_hsi16, ///< Used to set HSI16 as the LPTIM clock source
    lptim_lse,   ///< Used to set LSE as the LPTIM clock source
} lptim_clock;

//*********************************************************************
/**
 * \brief Clock sources available to the ADC
 */
//*********************************************************************
typedef enum
{
    adc_sysclk = 0, ///< Used to set SYSCLK as the ADC clock source
    adc_hsi16  = 2, ///< Used to set HSI16 as the ADC clock source
} adc_clock;

//*********************************************************************
/**
 * \brief Clock sources available to the I2C
 */
//*********************************************************************
typedef enum
{
    i2c_pclk,   ///< Used to set PCLK as the I2C clock source
    i2c_sysclk, ///< Used to set SYSCLK as the I2C clock source
    i2c_hsi16,  ///< Used to set HSI16 as the I2C clock source
} i2c_clock;

//*********************************************************************
/**
 * \brief Clock sources available to the SPI/I2S
 */
//*********************************************************************
typedef enum
{
    i2s_sysclk   = 0, ///< Used to set SYSCLK as the SPI/I2S clock source
    i2s_hsi16    = 2, ///< Used to set HSI16 as the SPI/I2S clock source
    i2s_external = 3, ///< Used to set an external clock source as the SPI/I2S clock source
} i2s_clock;

//*********************************************************************
/**
 * \brief Clock sources available to the USART
 */
//*********************************************************************
typedef enum
{
    uart_pclk,   ///< Used to set PCLK as the USART clock source
    uart_sysclk, ///< Used to set SYSCLK as the USART clock source
    uart_hsi16,  ///< Used to set HSI16 as the USART clock source
    uart_lse,    ///< Used to set LSE as the USART clock source
} uart_clock;

//*********************************************************************
/**
 * \brief Clock sources available to the LPUART
 */
//*********************************************************************
typedef enum
{
    lpuart_pclk,   ///< Used to set PCLK as the LPUART clock source
    lpuart_sysclk, ///< Used to set SYSCLK as the LPUART clock source
    lpuart_hsi16,  ///< Used to set HSI16 as the LPUART clock source
    lpuart_lse,    ///< Used to set LSE as the LPUART clock source
} lpuart_clock;

//*********************************************************************
/**
 * \brief Clock sources available to the RTC
 */
//*********************************************************************
typedef enum
{
    rtc_no_clock,         ///< Used to clear input sources from the RTC
    rtc_lse,              ///< Used to set LSE as the RTC clock source
    rtc_lsi,              ///< Used to set LSI as the RTC clock source
    rtc_hse_divide_by_32, ///< Used to set HSE divided by 32 as the RTC clock source
} rtc_clock;

//*********************************************************************
/**
 * \brief Clock sources available for use as the system clock (SYSCLK)
 */
//*********************************************************************
typedef enum
{
    sysclk_hsisys = 0, ///< Used to set HSISYS as the system clock source
    sysclk_hse    = 1, ///< Used to set HSE as the system clock source
    sysclk_lsi    = 3, ///< Used to set LSI as the system clock source
    sysclk_lse    = 4, ///< Used to set LSE as the system clock source
} system_clock;

//*********************************************************************
/**
 * \brief HSI16 clock division factors
 */
//*********************************************************************
typedef enum
{
    hsi_divide_by_1,   ///< Used to set HSISYS equal to HSI16 frequency divided by 1
    hsi_divide_by_2,   ///< Used to set HSISYS equal to HSI16 frequency divided by 2
    hsi_divide_by_4,   ///< Used to set HSISYS equal to HSI16 frequency divided by 4
    hsi_divide_by_8,   ///< Used to set HSISYS equal to HSI16 frequency divided by 8
    hsi_divide_by_16,  ///< Used to set HSISYS equal to HSI16 frequency divided by 16
    hsi_divide_by_32,  ///< Used to set HSISYS equal to HSI16 frequency divided by 32
    hsi_divide_by_64,  ///< Used to set HSISYS equal to HSI16 frequency divided by 64
    hsi_divide_by_128, ///< Used to set HSISYS equal to HSI16 frequency divided by 128
} hsi_divide;

//*********************************************************************
/**
 * \brief SYSCLK clock division factors
 */
//*********************************************************************
typedef enum
{
    sysclk_divide_by_1   = 0,  ///< Used to set HCLK equal to SYSCLK frequency divided by 1
    sysclk_divide_by_2   = 8,  ///< Used to set HCLK equal to SYSCLK frequency divided by 2
    sysclk_divide_by_4   = 9,  ///< Used to set HCLK equal to SYSCLK frequency divided by 4
    sysclk_divide_by_8   = 10, ///< Used to set HCLK equal to SYSCLK frequency divided by 8
    sysclk_divide_by_16  = 11, ///< Used to set HCLK equal to SYSCLK frequency divided by 16
    sysclk_divide_by_64  = 12, ///< Used to set HCLK equal to SYSCLK frequency divided by 64
    sysclk_divide_by_128 = 13, ///< Used to set HCLK equal to SYSCLK frequency divided by 128
    sysclk_divide_by_256 = 14, ///< Used to set HCLK equal to SYSCLK frequency divided by 256
    sysclk_divide_by_512 = 15, ///< Used to set HCLK equal to SYSCLK frequency divided by 512
} sysclk_divide;

//*********************************************************************
/**
 * \brief HCLK clock division factors
 */
//*********************************************************************
typedef enum
{
    hclk_divide_by_1  = 0, ///< Used to set PCLK equal to HCLK frequency divided by 1
    hclk_divide_by_2  = 4, ///< Used to set PCLK equal to HCLK frequency divided by 2
    hclk_divide_by_4  = 5, ///< Used to set PCLK equal to HCLK frequency divided by 4
    hclk_divide_by_8  = 6, ///< Used to set PCLK equal to HCLK frequency divided by 8
    hclk_divide_by_16 = 7, ///< Used to set PCLK equal to HCLK frequency divided by 16
} hclk_divide;

//*********************************************************************
/**
 * \brief LSE oscillator drive capability
 */
//*********************************************************************
typedef enum
{
    lse_low_drive,         ///< Used to set the LSE drive capability low
    lse_medium_low_drive,  ///< Used to set the LSE drive capability medium-low
    lse_medium_high_drive, ///< Used to set the LSE drive capability medium-high
    lse_high_drive,        ///< Used to set the LSE drive capability high
} lse_drive_capability;

//*********************************************************************
/**
 * \brief Enable or disable the HSI16, high-speed internal 16MHz oscillator
 * \param enable Enables the HSI if true, disables if false
 * \param on_in_stop HSI16 clock remains on during Stop if true, or will turn off in Stop if false
 * \returns True if the operation succeeded or false if the wait period timed out
 */
//*********************************************************************
bool rcc_hsi16_clock_source_control(bool const enable, bool const on_in_stop);

//*********************************************************************
/**
 * \brief Enable or disable the HSE, high-speed external oscillator, with a crystal or clock source of 4MHz up to 48MHz
 * \param enable Enables the HSE if true, disables if false
 * \param bypass An external clock is used as the source if true, or an external crystal is used if false
 * \returns True if the operation succeeded or false if the wait period timed out
 */
//*********************************************************************
bool rcc_hse_clock_source_control(bool const enable, bool const bypass);

//*********************************************************************
/**
 * \brief Enable the HSE clock security system, HSE CSS
 */
//*********************************************************************
void rcc_set_hse_clock_security(void);

//*********************************************************************
/**
 * \brief Clears the HSE clock security system interrupt flag
 * \note If a failure is detected by the HSE CSS, an interrupt will continue to occur until this flag is cleared
 */
//*********************************************************************
void rcc_clear_hse_clock_security_interrupt(void);

//*********************************************************************
/**
 * \brief Enable or disable the LSI, low-speed internal 32kHz oscillator
 * \param enable Enables the LSI if true, disables if false
 * \returns True if the operation succeeded or false if the wait period timed out
 */
//*********************************************************************
bool rcc_lsi_clock_source_control(bool const enable);

//*********************************************************************
/**
 * \brief Enable or disable the LSE, low-speed external oscillator, with a crystal or a clock source of 32kHz up to 1MHz
 * \param enable Enables the LSE if true, disables if false
 * \param bypass An external clock is used as the source if true, or an external crystal is used if false
 * \param drive The LSE oscillator drive capability
 * \returns True if the operation succeeded or false if the wait period timed out
 * \note If the LSE is on, its temporarily disabled, the configurations are set, then the LSE is re-enabled
 */
//*********************************************************************
bool rcc_lse_clock_source_control(bool const enable, bool const bypass, lse_drive_capability const drive);

//*********************************************************************
/**
 * \brief Enables the LSE clock security system, LSE CSS
 * \note Can only be enabled if the LSE and LSI clocks are on and the RTC clock source has been selected
 * \note Once enabled, the LSE CSS can only be disabled by a hardware reset or RTC reset
 */
//*********************************************************************
void rcc_set_lse_clock_security(void);

//*********************************************************************
/**
 * \brief Clears the LSE clock security system interrupt flag
 * \note If a failure is detected by the LSE CSS, an interrupt will continue to occur until this flag is cleared
 */
//*********************************************************************
void rcc_clear_lse_clock_security_interrupt(void);

//*********************************************************************
/**
 * \brief Sets the division factor of the HSI16 clock divider to produce the HSISYS clock
 * \param factor The division factor of the HSI16 clock
 */
//*********************************************************************
void rcc_configure_hsi_division_factor(hsi_divide const factor);

//*********************************************************************
/**
 * \brief Selects the clock source of the system clock, SYSCLK
 * \param clock The clock source to use for the system clock
 * \note The HSISYS clock is automatically set by hardware when exiting Stop or Standby
 */
//*********************************************************************
void rcc_select_system_clock(system_clock const clock);

//*********************************************************************
/**
 * \brief Sets the division factor of the SYSCLK clock divider to produce the HCLK clock
 * \param factor The division factor of the SYSCLK clock
 */
//*********************************************************************
void rcc_configure_sysclk_division_factor(sysclk_divide const factor);

//*********************************************************************
/**
 * \brief Sets the division factor of the HCLK clock divider to produce the PCLK clock
 * \param factor The division factor of the HCLK clock
 */
//*********************************************************************
void rcc_configure_hclk_division_factor(hclk_divide const factor);

//*********************************************************************
/**
 * \brief Select and enable the RTC and TAMP clocks
 * \param clock The clock source to use for the RTC clock
 * \note This resets the RTC domain if the RTC clock is changing from one source to another
 */
//*********************************************************************
void rcc_enable_rtc_clock(rtc_clock const clock);

//*********************************************************************
/**
 * \brief Enable the TIM1 clock
 * \param on_in_sleep TIM1 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_tim1_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the TIM2 clock
 * \param on_in_sleep TIM2 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_tim2_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the TIM3 clock
 * \param on_in_sleep TIM3 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_tim3_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the TIM14 clock
 * \param on_in_sleep TIM14 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_tim14_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the TIM16 clock
 * \param on_in_sleep TIM16 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_tim16_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the TIM17 clock
 * \param on_in_sleep TIM17 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_tim17_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Select and enable the LPTIM1 clock
 * \param clock The clock source to use for the LPTIM1 clock
 * \param on_in_stop LPTIM1 clock remains on during Stop/Sleep if true, or will turn off in Stop/Sleep if false
 */
//*********************************************************************
void rcc_enable_lptim1_clock(lptim_clock const clock, bool const on_in_stop);

//*********************************************************************
/**
 * \brief Select and enable the LPTIM2 clock
 * \param clock The clock source to use for the LPTIM2 clock
 * \param on_in_stop LPTIM2 clock remains on during Stop/Sleep if true, or will turn off in Stop/Sleep if false
 */
//*********************************************************************
void rcc_enable_lptim2_clock(lptim_clock const clock, bool const on_in_stop);

//*********************************************************************
/**
 * \brief Select and enable the USART1 clock
 * \param clock The clock source to use for the USART1 clock
 * \param on_in_stop USART1 clock remains on during Stop/Sleep if true, or will turn off in Stop/Sleep if false
 */
//*********************************************************************
void rcc_enable_usart1_clock(uart_clock const clock, bool const on_in_stop);

//*********************************************************************
/**
 * \brief Enable the USART2 clock
 * \param on_in_stop USART2 clock remains on during Stop/Sleep if true, or will turn off in Stop/Sleep if false
 */
//*********************************************************************
void rcc_enable_usart2_clock(bool const on_in_stop);

//*********************************************************************
/**
 * \brief Select and enable the LPUART1 clock
 * \param clock The clock source to use for the LPUART1 clock
 * \param on_in_stop LPUART1 clock remains on during Stop/Sleep if true, or will turn off in Stop/Sleep if false
 */
//*********************************************************************
void rcc_enable_lpuart1_clock(lpuart_clock const clock, bool const on_in_stop);

//*********************************************************************
/**
 * \brief Select and enable the I2C1 clock
 * \param clock The clock source to use for the I2C1 clock
 * \param on_in_stop I2C1 clock remains on during Stop/Sleep if true, or will turn off in Stop/Sleep if false
 */
//*********************************************************************
void rcc_enable_i2c1_clock(i2c_clock const clock, bool const on_in_stop);

//*********************************************************************
/**
 * \brief Enable the I2C2 clock
 * \param on_in_sleep I2C2 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_i2c2_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Select and enable the SPI1/I2S1 clock
 * \param clock The clock source to use for the SPI1/I2S1 clock
 * \param on_in_sleep SPI1/I2S1 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_spi1_clock(i2s_clock const clock, bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the SPI2/I2S2 clock
 * \param on_in_sleep SPI2/I2S2 clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_spi2_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the RTC APB clock
 * \param on_in_sleep RTC APB clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_rtcapb_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the window watchdog (WWDG) clock
 * \param on_in_stop WWDG clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_wwdg_clock(bool const on_in_stop);

//*********************************************************************
/**
 * \brief Enable the debug support (DBG) clock
 * \param on_in_sleep DBG clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_dbg_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the power interface (PWR) clock
 * \param on_in_sleep PWR clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_pwr_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the system configuration controller (SYSCFG) clock
 * \param on_in_stop SYSCFG clock remains on during Stop/Sleep if true, or will turn off in Stop/Sleep if false
 */
//*********************************************************************
void rcc_enable_syscfg_clock(bool const on_in_stop);

//*********************************************************************
/**
 * \brief Select and enable the ADC clock
 * \param clock The clock source to use for the ADC clock
 * \param on_in_sleep PWR clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_adc_clock(adc_clock const clock, bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the hardware CRC clock
 * \param on_in_sleep CRC clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_crc_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Disable the hardware CRC clock
 */
//*********************************************************************
void rcc_disable_crc_clock(void);

//*********************************************************************
/**
 * \brief Enable the flash memory interface clock
 * \param on_in_sleep Flash memory clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_flash_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the DMA1 and DMAMUX clock
 * \param on_in_sleep DMA clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_dma1_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the GPIOA clock
 * \param on_in_sleep GPIOA clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_gpioa_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the GPIOB clock
 * \param on_in_sleep GPIOB clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_gpiob_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the GPIOC clock
 * \param on_in_sleep GPIOC clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_gpioc_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the GPIOD clock
 * \param on_in_sleep GPIOD clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_enable_gpiod_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Enable the GPIOF clock
 * \param on_in_sleep GPIOF clock remains on during Sleep if true, or will turn off in Sleep if false
 */
//*********************************************************************
void rcc_enable_enable_gpiof_clock(bool const on_in_sleep);

//*********************************************************************
/**
 * \brief Gets the reset flags.
 * \return The reset flags right shifted by 25 bits.
 * \note Use the reset_cause enum to determine the cause of the reset.
 */
//*********************************************************************
uint8_t rcc_get_reset_flags(void);

//*********************************************************************
/**
 * \brief Clears the reset flags.
 */
//*********************************************************************
void rcc_clear_reset_flags(void);

#endif
