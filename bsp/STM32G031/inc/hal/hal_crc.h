//******************************************************************
/*! \file hal_crc.h
 *
 * \brief Kidde: STM32G031 - Declare hal CRC functions
 *
 * \author Benjamin Merrick
 * \author (c) 2021 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
#include <stdbool.h>
#include <stdint.h>

void crc_init(uint32_t const seed);

void crc_reset(void);

void crc_32(uint32_t const bytes);

void crc_16(uint16_t const bytes);

void crc_8(uint8_t const bytes);

uint32_t get_crc(void);
