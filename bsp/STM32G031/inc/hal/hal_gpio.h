#ifndef HAL_GPIO_H_
#define HAL_GPIO_H_

/**
 * @file hal_gpio.h
 * @brief Declare the GPIO HAL functions
 */
#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Pin modes
 */
typedef enum
{
    gpio_mode_input    = 0, /**< Sets pin as input */
    gpio_mode_output   = 1, /**< Sets pin as output */
    gpio_mode_altfunc  = 2, /**< Sets pin as alternate function */
    gpio_mode_input_an = 3  /**< Sets pin as analog */
} pin_modes;

/**
 * @brief Pin output types.
 */
typedef enum
{
    gpio_out_type_pp     = 0, /**< Sets pin as push pull */
    gpio_mode_open_drain = 1, /**< Sets pin as open drain */
} pin_out_type;

/**
 * @brief Pin pullup, pulldown modes.
 */
typedef enum
{
    pupd_none = 0, /**< Pin configured for no pullup */
    pupd_pu   = 1, /**< Pin configured for pullup */
    pupd_pd   = 2, /**< Pin configured for pulldown */
} pin_pupd;

/**
 * @brief Pin speed modes.
 */
typedef enum
{
    pinspeed_very_low  = 0, /**< Pin configured for lowest speed */
    pinspeed_low       = 1, /**< Pin configured for low speed */
    pinspeed_high      = 2, /**< Pin configured for high speed */
    pinspeed_very_high = 3  /**< Pin configured for very high speed */
} pin_speed;

/**
 * @brief Pin Alternate functions.
 *
 * Format:
 *      gpio_<pin>_af_<pin function>
 *      where:  <pin> is the port pin number.
 *              <pin function> is the function to set the pin to.
 *      @par Example:
 *          Set pin PA3 to pin function EVENTOUT
 *          gpio_pa3_af_eventout
 */
typedef enum
{
    gpio_pa0_af_spi2_sck    = 0,
    gpio_pa0_af_usart2_cts  = 1,

    gpio_pa1_af_spi1_sck    = 0,
    gpio_pa1_af_usart2_rts  = 1,
    gpio_pa1_af_i2c1_smba   = 6,
    gpio_pa1_af_eventout    = 7,

    gpio_pa2_af_spi1_mosi   = 0,
    gpio_pa2_af_usart2_tx   = 1,

    gpio_pa3_af_spi2_miso   = 0,
    gpio_pa3_af_usart2_rx   = 1,
    gpio_pa3_af_eventout    = 7,

    gpio_pa4_af_spi1_nss    = 0,
    gpio_pa4_af_spi2_mosi   = 1,
    gpio_pa4_af_tim14_ch1   = 4,
    gpio_pa4_af_eventout    = 7,

    gpio_pa5_af_spi1_sck    = 0,
    gpio_pa5_af_eventout    = 7,

    gpio_pa6_af_spi1_miso   = 0,
    gpio_pa6_af_tim3_ch1    = 1,
    gpio_pa6_af_tim1_bkin   = 2,
    gpio_pa6_af_tim16_ch1   = 5,

    gpio_pa7_af_spi1_mosi   = 0,
    gpio_pa7_af_tim3_ch2    = 1,
    gpio_pa7_af_tim1_ch1n   = 2,
    gpio_pa7_af_tim14_ch1   = 4,
    gpio_pa7_af_tim17_ch1   = 5,

    gpio_pa8_af_mco         = 0,
    gpio_pa8_af_spi2_nss    = 1,
    gpio_pa8_af_tim1_ch1    = 2,
    gpio_pa8_af_eventout    = 7,

    gpio_pa9_af_mco         = 0,
    gpio_pa9_af_usart1_tx   = 1,
    gpio_pa9_af_tim1_ch2    = 2,
    gpio_pa9_af_spi2_miso   = 4,
    gpio_pa9_af_i2c1_scl    = 6,
    gpio_pa9_af_eventout    = 7,

    gpio_pa10_af_spi2_mosi  = 0,
    gpio_pa10_af_usart1_rx  = 1,
    gpio_pa10_af_tim1_ch3   = 2,
    gpio_pa10_af_tim17_bkin = 5,
    gpio_pa10_af_i2c1_sda   = 6,
    gpio_pa10_af_eventout   = 7,

    gpio_pa11_af_spi1_miso  = 0,
    gpio_pa11_af_usart1_cts = 1,
    gpio_pa11_af_tim1_ch4   = 2,
    gpio_pa11_af_tim1_bkin2 = 5,
    gpio_pa11_af_i2c2_scl   = 6,

    gpio_pa12_af_spi1_mosi  = 0,
    gpio_pa12_af_usart1_rts = 1,
    gpio_pa12_af_tim1_etr   = 2,
    gpio_pa12_af_i2s_ckin   = 5,
    gpio_pa12_af_i2c2_sda   = 6,

    gpio_pa13_af_swdio      = 0,
    gpio_pa13_af_ir_out     = 1,
    gpio_pa13_af_eventout   = 7,

    gpio_pa14_af_swclk      = 0,
    gpio_pa14_af_usart2_tx  = 1,
    gpio_pa14_af_eventout   = 7,

    gpio_pa15_af_spi1_nss   = 0,
    gpio_pa15_af_usart2_rx  = 1,
    gpio_pa15_af_eventout   = 7,

    gpio_pb0_af_spi1_nss    = 0,
    gpio_pb0_af_tim3_ch3    = 1,
    gpio_pb0_af_tim1_ch2n   = 2,

    gpio_pb1_af_tim14_ch1   = 0,
    gpio_pb1_af_tim3_ch4    = 1,
    gpio_pb1_af_tim1_ch3n   = 2,
    gpio_pb1_af_eventout    = 7,

    gpio_pb2_af_spi2_miso   = 2,
    gpio_pb2_af_eventout    = 7,

    gpio_pb3_af_spi1_sck    = 0,
    gpio_pb3_af_tim1_ch2    = 1,
    gpio_pb3_af_usart1_rts  = 4,
    gpio_pb3_af_eventout    = 7,

    gpio_pb4_af_spi1_miso   = 0,
    gpio_pb4_af_tim3_ch1    = 1,
    gpio_pb4_af_usart1_cts  = 4,
    gpio_pb4_af_tim17_bkin  = 5,
    gpio_pb4_af_eventout    = 7,

    gpio_pb5_af_spi1_mosi   = 0,
    gpio_pb5_af_tim3_ch2    = 1,
    gpio_pb5_af_tim16_bkin  = 2,
    gpio_pb5_af_i2c1_smba   = 6,

    gpio_pb6_af_usart1_tx   = 0,
    gpio_pb6_af_tim1_ch3    = 1,
    gpio_pb6_af_tim16_ch1n  = 2,
    gpio_pb6_af_spi2_miso   = 4,
    gpio_pb6_af_i2c1_scl    = 6,
    gpio_pb6_af_eventout    = 7,

    gpio_pb7_af_usart1_rx   = 0,
    gpio_pb7_af_spi2_mosi   = 1,
    gpio_pb7_af_tim17_ch1n  = 2,
    gpio_pb7_af_i2c1_sda    = 6,
    gpio_pb7_af_eventout    = 7,

    gpio_pb8_af_spi2_sck    = 1,
    gpio_pb8_af_tim16_ch1   = 2,
    gpio_pb8_af_i2c1_scl    = 6,
    gpio_pb8_af_eventout    = 7,

    gpio_pb9_af_ir_out      = 0,
    gpio_pb9_af_tim17_ch1   = 2,
    gpio_pb9_af_spi2_nss    = 5,
    gpio_pb9_af_i2c1_sda    = 6,
    gpio_pb9_af_eventout    = 7,

    gpio_pc6_af_tim3_ch1    = 1,

    gpio_pc14_af_tim1_bkin2 = 2,

    gpio_pc15_af_osc32_en   = 0,
    gpio_pc15_af_osc_en     = 1,
} pin_afs;

struct gpio_port;

/**
 * @brief porta is a gpio_port.
 */
extern struct gpio_port volatile porta;
/**
 * @brief portb is a gpio_port.
 */
extern struct gpio_port volatile portb;
/**
 * @brief portc is a gpio_port.
 */
extern struct gpio_port volatile portc;

/**
 * @brief An opaque pointer type used to identify which port to operate on.
 */
typedef struct gpio_port volatile *const port_type;

/**
 * @brief Set the mode for the pin
 *
 * @param[in] port The port to set mode
 * @param[in] pin The pin to set mode
 * @param[in] mode The mode to set @ref pin_modes "pin_modes"
 */
void gpio_set_pin_mode(port_type port, pin_modes const mode, uint8_t const pin);

/**
 * @brief Set the output type for the pin
 *
 * @param[in] port The port to set mode
 * @param[in] pin The pin to set mode
 * @param[in] type The output type to set
 */
void gpio_set_output_type(port_type port, pin_out_type const type, uint8_t const pin);

/**
 * @brief Set the port/pin combination to output
 *
 * @param[in] port The port to set a pin as output
 * @param[in] pin The pin to set as output
 */
void gpio_set_output(port_type port, uint8_t const pin);

/**
 * @brief Set the port/pin combination to input
 *
 * @param[in] port The port to set a pin as input
 * @param[in] pin The pin to set as input
 */
void gpio_set_input(port_type port, uint8_t const pin);
/**
 * @brief Set the port/pin combination to drive the output high
 *
 * @param[in] port The port to drive a pin high
 * @param[in] pin The pin to drive high (0-15)
 */
void gpio_drive_high(port_type port, uint8_t const pin);
/**
 * @brief Set the port/pin combination to drive the output low
 *
 * @param[in] port The port to drive a pin low
 * @param[in] pin The pin to drive low (0-15)
 */
void gpio_drive_low(port_type port, uint8_t const pin);

/**
 * @brief Set the port/pin combination to configure pullup mode
 *
 * @param[in] port The port to set pullup/pulldown mode
 * @param[in] pin  The pin to set pullup/pulldown mode
 * @param[in] mode The mode to set @ref pin_pupd "pullup modes"
 */
void gpio_pull_updown_mode(port_type port, pin_pupd const mode, uint8_t const pin);

/**
 * @brief Read the port/pin combination
 *
 * @param[in] port The port to read a pin
 * @param[in] pin The pin to read
 * @returns The value of the port/pin combination
 */
bool gpio_read(port_type port, uint8_t const pin);

/**
 * @brief Set the port/pin combination to configure pin speed
 *
 * @param[in] port The port to set the pin speed
 * @param[in] pin  The pin to set the pin speed
 * @param[in] mode The mode to set @ref pin_speed "pin speed"
 */
void gpio_set_pin_speed(port_type port, pin_speed const mode, uint8_t const pin);

/**
 * @brief Sets the alternate function for pins
 *
 * @param[in] port The port to set the alternate function
 * @param[in] mode The mode to set.
 * @param[in] pin  The pin to set.
 */
void gpio_set_afr_pin(port_type port, uint32_t const mode, uint8_t const pin);

/**
 * @brief Lock I/O configuration on port
 *
 * @param[in] port The port to lock
 * @param[in] lock_mask The pins to lock.
 * @return 1 if lock was successful, 0 otherwise.
 * @note Each pin to lock has the corresponding bit set.
 *       From the datasheet the lock procedure is:
 *          LOCK key write sequence:
 *          WR LCKR[16] = ‘1’ + LCKR[15:0]
 *          WR LCKR[16] = ‘0’ + LCKR[15:0]
 *          WR LCKR[16] = ‘1’ + LCKR[15:0]
 *          RD LCKR
 *          RD LCKR[16] = ‘1’ (this read operation is optional but it confirms that the lock is active)
 */
bool gpio_lock(port_type port, uint16_t const lock_mask);

/**
 * @brief Calculates the pullups/pulldowns to be used during standby mode.
 *
 * @param[in] port The port to process
 * @param[out] pullup_config The bits to set in the power port pull-up control register.
 * @param[out] pulldown_config The bits to set in the power port pull-down control register
 */
void process_pulls(port_type port, unsigned *pullup_config, unsigned *pulldown_config);

#endif
