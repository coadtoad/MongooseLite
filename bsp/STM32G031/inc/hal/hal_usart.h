/**
 * @file hal_usart.h
 * @brief Declare the USART HAL functions
 */
#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Define the USART word length.
 * @note: Stop bit must be configured separately.
 */
typedef enum
{
    usart_8nx, /**< 1 start bit, 8 Data bits, n Stop bit */
    usart_9nx, /**< 1 start bit, 9 Data bits, n Stop bit */
    usart_7nx  /**< 1 start bit, 7 Data bits, n Stop bit */
} usart_word_len;

/**
 * @brief Define the USART stop bits.
 */
typedef enum
{
    usart_stop_1,   /**< 1 stop bit */
    usart_stop_0_5, /**< 0.5 stop bit */
    usart_stop_2,   /**< 2 stop bits */
    usart_stop_1_5  /**< 1.5 stop bits */
} usart_stop_bits;

/**
 * @brief Constants to set or clear bits.
 */
typedef enum
{
    usart_clear = 0,
    usart_set   = 1,
} usart_bit_cfg;

/**
 * @brief Constants to set USART prescale.
 */
typedef enum
{
    prescale_div_1,   /**< input clock not divided */
    prescale_div_2,   /**< input clock divided by 2 */
    prescale_div_4,   /**< input clock divided by 4 */
    prescale_div_6,   /**< input clock divided by 6 */
    prescale_div_8,   /**< input clock divided by 8 */
    prescale_div_10,  /**< input clock divided by 10 */
    prescale_div_12,  /**< input clock divided by 12 */
    prescale_div_16,  /**< input clock divided by 16 */
    prescale_div_32,  /**< input clock divided by 32 */
    prescale_div_64,  /**< input clock divided by 64 */
    prescale_div_128, /**< input clock divided by 128 */
    prescale_div_256, /**< input clock divided by 256 */
} usart_prescale_factor;

/**
 * @brief Initalizea USART
 */
void usart_init(void);

/**
 * @brief Enables the USART
 */
void usart_enable(void);

/**
 * @brief Sets the USART word length.
 *
 * @param[in] len length enum @ref usart_word_len
 */
void usart_set_word_length(usart_word_len const len);

/**
 * @brief Sets the USART parity mode.
 *
 * @param[in] parity Parity setting. @ref usart_bit_cfg
 */
void usart_set_parity(usart_bit_cfg const parity);

/**
 * @brief Enables the USART transmitter.
 *
 */
void usart_tx_enable(void);

/**
 * @brief Disables the USART transmitter.
 *
 */
void usart_tx_disable(void);

/**
 * @brief Enables the USART receiver.
 *
 */
void usart_rx_enable(void);

/**
 * @brief Disables the USART receiver.
 *
 */
void usart_rx_disable(void);

/**
 * @brief Sets the baudrate.
 *
 * @param[in] baud The desired baudrate.
 * @note This function assumes that the oversampling is set to 16x,
 *          the prescaler is set to zero and the peripheral clock
 *          is set to 16 MHz.
 */
void usart_set_baudrate(uint32_t const baud);

/**
 * @brief Enables the USART Tx buffer empty interrupt.
 *
 */
void usart_tx_empty_int_enable(void);

/**
 * @brief Disables the USART Tx buffer empty interrupt.
 *
 */
void usart_tx_empty_int_disable(void);

/**
 * @brief Sets the USART stop bits.
 *
 * @param[in] bits The desired number of stop bits. @ref usart_stop_bits
 */
void usart_set_stop_bits(usart_stop_bits const bits);

/**
 * @brief Sets the USART clock prescale.
 *
 * @param[in] factor The desired prescale value. @ref usart_prescale_factor
 */
void usart_set_clock_prescale(usart_prescale_factor const factor);

/**
 * @brief Writes to the transmit buffer.
 *
 * @param[in] data The byte to be sent.
 */
void usart_write_tx(uint8_t const data);

/**
 * @brief Reads from the receive buffer.
 *
 * @return The data read from buffer.
 */
uint8_t usart_read_byte(void);

/**
 * @brief Gets the state of the receive buffer not empty bit.
 *
 * @return True if a character has been received.
 */
bool usart_is_rx_buffer_full(void);

/**
 * @brief Gets the state of the receive buffer not empty interrupt enable bit.
 *
 * @return True if the receive buffer full interrupt enable bit is set.
 */
bool usart_is_rcv_interrupt_enabled(void);

/**
 * @brief Enables the Rx buffer full interrupt
 *
 */
void usart_rx_buffer_full_int_enable(void);

/**
 * @brief Disables the Rx buffer full interrupt
 *
 */
void usart_rx_buffer_full_int_disable(void);

/**
 * @brief Gets the state of the transmit buffer empty interrupt flag.
 *
 * @return True if the transmit buffer empty bit is set.
 */
bool usart_is_tx_buffer_empty(void);

/**
 * @brief Gets the state of the transmit buffer empty interrupt enable bit.
 *
 * @return True if the transmit buffer empty interrupt enable bit is set.
 */
bool usart_is_tx_interrupt_enabled(void);
