//******************************************************************
/*! \file drv_afe_serial_commands.c
 *
 * \brief Kidde: Mongoose-fw - Declare the afe serial driver functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_serial.h"
#include "drv_afe.h"
#include "drv_i2c.h"
#include <stddef.h>

void afe_serial_output_regs(void)
{
    // I\r  - read all AFE registers
    uint8_t registers[num_afe_regs];
    for (uint8_t i = 0; i < sizeof(registers) / sizeof(registers[0]); ++i)
    {
        afe_read_register((afe_reg_addr)i, &registers[i]);
    }

    char buffer[] = "XX: XX\r";
    for (uint8_t i = 0; i < sizeof(registers) / sizeof(registers[0]); ++i)
    {
        uint8_to_hex(buffer, i);
        uint8_to_hex(buffer + 4, registers[i]);
        serial_send_string(buffer);
    }
    serial_send_string("\r");
}

//*********************************************************************
/**
 * \brief Watch rx to determine what serial commands to send to th user
 * \param rx_buffer rx connection buffer so data is recieved in correct bytes/bits
 */
//*********************************************************************
void afe_serial_command(uint8_t const *rx_buffer)
{
    switch (*rx_buffer)
    {
        // Reply with a prompt.
    case '\0':
        send_AFE_prompt();
        break;

    case 'i':
    {
        // i XX YY \r  -- Set register XX on device to YY
        rx_buffer += 2; // point to first register address character
        uint8_t const reg_addr = serial_uart_atou8(rx_buffer);

        rx_buffer += 3; // point to the first value character
        uint8_t const value = serial_uart_atou8(rx_buffer);

        bool const success  = afe_write_register(reg_addr, value);
        if (success)
        {
            serial_send_int_ex_debug("I2C wrote ", value, 16, NULL, serial_enable_echo);
            serial_send_int_ex_debug(" to the AFE at register ", reg_addr, 16, ".", serial_enable_echo);
        }
        else
        {
            serial_send_string_debug("I2C write failed\n", serial_enable_echo);
        }
        break;
    }

    case 'I':
        afe_serial_output_regs();
        break;

    default:
        // If no commands are matched, that's an error.
        serial_send_string(ERR_STRING);
        break;
    }
}
