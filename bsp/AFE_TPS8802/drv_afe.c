//******************************************************************
/*! \file drv_afe.c
 *
 * \brief Kidde: Mongoose-fw - Implement functions for AFE driver
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************
#include "drv_afe.h"
#include "drv_i2c.h"
#include "drv_photo.h"

//*********************************************************************
/**
 * \brief The number of times to try a register write.
 */
//*********************************************************************
#define WRITE_COUNT         3
//*********************************************************************
/**
 * \brief I2C address of AFE, configured using CSEL pin.  The address is shifted in
 *          the I2C driver
 */
//*********************************************************************
#define TPS8802_I2C_ADDRESS ((uint8_t)(0x3F))

//*********************************************************************
/**
 * \brief Mask of the write access bits in the AFE registers
 */
//*********************************************************************
static const uint8_t afe_reg_mask[num_afe_regs] = {
    0x00, ///<   Device Information (read only)
    0x00, ///<   Status 1 (read only)
    0x00, ///<   Status 2 (read only)
    0xff, ///<   Interrupt Mask
    0xff, ///<   Config 1
    0x3f, ///<   Config 2
    0x7d, ///<   Enable 1
    0xcd, ///<   Enable 2
    0x3f, ///<   Control
    0xff, ///<   Sleep Timer 1
    0xff, ///<   Sleep Timer 2
    0xb7, ///<   GPIO and AMUX
    0xf7, ///<   CO and Battery Test
    0xff, ///<   CO Amplifier
    0xff, ///<   Boost Converter
    0x0e, ///<   LED LDO
    0x7f, ///<   Photo Amplifier
    0xff, ///<   LED DAC A
    0xff  ///<   LED DAC B
};

//*********************************************************************
/**
 * \brief revid register
 */
//*********************************************************************
struct revid
{
    unsigned char         : 6; ///<   Reserved
    unsigned char csa_bin : 2; ///< CSA voltage bin for TEMPCOA=11, PDAC_A=00 setting
};

//*********************************************************************
/**
 * \brief mask register
 */
//*********************************************************************
union mask
{
    struct
    {
        unsigned char status_int  : 1; ///<   Status interrupt on the INT_MCU pin
        unsigned char bst_errm    : 1; ///<   Boost converter power good error interrupt mask
        unsigned char bst_nactm   : 1; ///<   Boost activity monitor interrupt mask
        unsigned char ots_wrnm    : 1; ///<   Thermal warning flag interrupt mask
        unsigned char ots_errm    : 1; ///<   Thermal shutdown error interrupt mask
        unsigned char mculdo_errm : 1; ///<   MCU LDO power good error interrupt mask
        unsigned char vcclowm     : 1; ///<   VCC low warning interrupt mask
        unsigned char slp_donem   : 1; ///<   Sleep timer wakeup interrupt mask
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief config1 register
 */
//*********************************************************************
union config1
{
    struct
    {
        unsigned char slp_mcu    : 1; ///<   Disable MCULDO in sleep mode
        unsigned char slp_analog : 1; ///<   Disable analog blocks in sleep mode.
        unsigned char slp_bst    : 1; ///<   Disable boost converter in sleep mode
        unsigned char vmcuset    : 2; ///<   MCU LDO voltage. Default value is set by MCUSEL on power-up.
        unsigned char int_pd     : 1; ///<   INT_UNIT pulldown resistor enable
        unsigned char int_deg    : 2; ///<   INT_UNIT deglitch control
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief config2 register
 */
//*********************************************************************
union config2
{
    struct
    {
        unsigned char t_bstact : 2; ///<   Boost activity monitor alert time.
        unsigned char horn_thr : 2; ///<   Horn driver setting for three-terminal piezo duty cycle tuning
        unsigned char horn_sel : 1; ///<   Horn block piezo select
        unsigned char int_hys  : 1; ///<   Interconnect comparator hysteresis
        unsigned char          : 2; ///<   Reserved
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief enable1 register
 */
//*********************************************************************
union enable1
{
    struct
    {
        unsigned char ledldo_en  : 1; ///<   LED LDO enable
        unsigned char            : 1; ///<   Reserved
        unsigned char pgain_en   : 1; ///<   Photo Gain amplifier enable
        unsigned char pamp_en    : 1; ///<   Photo input amplifier enable
        unsigned char bst_en     : 1; ///<   Boost converter enable
        unsigned char int_en     : 1; ///<   Control of interconnect interface
        unsigned char battest_en : 1; ///<   Battery test enable
        unsigned char            : 1; ///<   Reserved
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief enable2 register
 */
//*********************************************************************
union enable2
{
    struct
    {
        unsigned char slp_en     : 1; ///<   Sleep timer enable
        unsigned char horn_en    : 1; ///<   Horn block enable
        unsigned char ledpin_en  : 1; ///<   LEDEN pin enable
        unsigned char int_dir    : 1; ///<   Interconnect direction control
        unsigned char            : 2; ///<   Reserved
        unsigned char bst_charge : 1; ///<   Photo Gain amplifier enable
        unsigned char ledsel     : 1; ///<   LED LDO enable
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief control register
 */
//*********************************************************************
union control
{
    struct
    {
        unsigned char vcclow_bst : 1; ///<   VCCLOW boost control
        unsigned char softreset  : 1; ///<   Set registers to the default value
        unsigned char ots_en     : 1; ///<   Over-temperature shutdown mode enable
        unsigned char mcuerr_dis : 1; ///<   MCULDO error mode disable
        unsigned char vcclow_dis : 1; ///<   VCCLOW brown-out monitor disable
        unsigned char mcu_dis    : 1; ///<   MCU LDO disable
        unsigned char            : 2; ///<   Reserved
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief gpio_amux register
 */
//*********************************************************************
union gpio_amux
{
    struct
    {
        unsigned char gpio     : 3; ///<   Multi-purpose digital input and output
        unsigned char          : 1; ///<   Reserved
        unsigned char amux_sel : 2; ///<   Analog multiplexer input select
        unsigned char          : 1; ///<   Reserved
        unsigned char amux_byp : 1; ///<   Analog multiplexer bypass
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief co_battest register
 */
//*********************************************************************
union co_battest
{
    struct
    {
        unsigned char i_battest : 3; ///<   Battery test current
        unsigned char           : 1; ///<   Reserved
        unsigned char coswref   : 1; ///<   CO reference switch enable
        unsigned char coswri    : 1; ///<   CO input resistor (inverting input of amplifier to CON pin) enable
        unsigned char coswrg : 1; ///<   CO gain resistor (output of amplifier to inverting input of amplifier) enable
        unsigned char coswro : 1; ///<   CO amplifier output resistor (output of amplifier to COO pin) enable
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief co register
 */
//*********************************************************************
union co
{
    struct
    {
        unsigned char coamp_en   : 1; ///<   CO amplifier enable
        unsigned char cotest_en  : 1; ///<   Enable COTEST output on PREF
        unsigned char cotest_dir : 1; ///<   CO test output direction
        unsigned char cogain     : 2; ///<   CO amplifier feedback resistance
        unsigned char coref      : 2; ///<   Reference voltage for CO amplifier
        unsigned char ref0p3_en  : 1; ///<   300mV reference enable
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief vboost register
 */
//*********************************************************************
union vboost
{
    struct
    {
        unsigned char vbst     : 4; ///<   Boost converter output voltage setting.
        unsigned char bst_clim : 4; ///<   Boost converter inductor peak current setting
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief ledldo register
 */
//*********************************************************************
union ledldo
{
    struct
    {
        unsigned char        : 1; ///<   Reserved
        unsigned char ledldo : 3; ///<   LED LDO settings
        unsigned char        : 4; ///<   Reserved
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief ph_ctrl register
 */
//*********************************************************************
union ph_ctrl
{
    struct
    {
        unsigned char pgain    : 2; ///<   Photo Gain setting
        unsigned char pref_sel : 1; ///<   Photo Reference setting
        unsigned char tempcoa  : 2; ///<   LED A Temperature Coefficient Setting
        unsigned char tempcob  : 2; ///<   LED B Temperature Coefficient Setting
        unsigned char          : 1; ///<   Reserved
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief AFE Shadow Registers
 */
//*********************************************************************
union afe_shadow_reg
{
    struct
    {
        struct revid revid;          ///<   Device Information
        union status1 status1;       ///<   Status 1
        union status2 status2;       ///<   Status 2
        union mask mask;             ///<   Interrupt Mask
        union config1 config1;       ///<   Config 1
        union config2 config2;       ///<   Config 2
        union enable1 enable1;       ///<   Enable 1
        union enable2 enable2;       ///<   Enable 2
        union control control;       ///<   Control
        uint8_t slptmr1;             ///<   Sleep timer most significant bits.1
        uint8_t slptmr2;             ///<   Sleep timer duration.
        union gpio_amux gpio_amux;   ///<   GPIO and AMUX
        union co_battest co_battest; ///<   CO and Battery Test
        union co co;                 ///<   CO Amplifier
        union vboost vboost;         ///<   Boost Converter
        union ledldo ledldo;         ///<   LED LDO
        union ph_ctrl ph_ctrl;       ///<   Photo Amplifier
        uint8_t led_dac_a;           ///<   LED DAC A setting
        uint8_t led_dac_b;           ///<   LED DAC B setting
    };

    uint8_t reg_array[num_afe_regs];
};

//*********************************************************************
/**
 * \brief Initialize the shadow registers
 * \note   These will be initialized from the database in production code.
 *          Just init from values for now.
 */
//*********************************************************************
static union afe_shadow_reg shadow_reg = {
    /**
     * \brief Read only, no init needed.
     */
    .revid.csa_bin        = 0, ///<  Revision ID
    /**
     * \brief Read only, no init needed.
     */
    .status1.byte         = 0, ///<  Byte for status1
    /**
     * \brief Read only, no init needed.
     */
    .status2.byte         = 0, ///<  Byte for status2
    /**
     * \brief We're not using GPIO as an interrupt line so mask all errors.
     */
    .mask.byte            = 0xff, ///<  Byte for mask
    /**
     * \brief config1 initialization
     */
    .config1.int_deg      = 2, ///<  1ms deglitch
    .config1.int_pd       = 1, ///<  100K pulldown
    .config1.vmcuset      = 2, ///<  MCU LDO=2.5v
    .config1.slp_bst      = 0, ///< boost on in sleep if boost enabled
    .config1.slp_analog   = 1, ///<  analog off in sleep
    .config1.slp_mcu      = 0, ///<  MCU LDO on in sleep

    /**
     * \brief Config2 initialization
     *
     */
    .config2.int_hys      = 0, ///<   Intercon Hyst = 1.2v
    .config2.horn_sel     = 1, ///<   3 terminal horn
    .config2.horn_thr     = 2, ///<   horn duty cycle nominal
    .config2.t_bstact     = 1, ///<   1ms boost monitor
    /**
     * \brief Enable1 initialization
     */
    .enable1.ledldo_en    = 1, ///<   Batter test disabled
    .enable1.pgain_en     = 1, ///<   Photo Gain amplifier enabled
    .enable1.pamp_en      = 1, ///<   Photo input amplifier enabled
    .enable1.bst_en       = 0, ///<   Boost converter enable
    .enable1.int_en       = 0, ///<   Control of interconnect interface
    .enable1.battest_en   = 0, ///<   Battery test enable
    /**
     * \brief enable2 initialization
     *
     */
    .enable2.ledsel       = 1, ///<   LEDEN drives LEDB
    .enable2.bst_charge   = 0, ///<   BST_EN controls boost
    .enable2.int_dir      = 0, ///<   Intcon Rx
    .enable2.ledpin_en    = 1, ///<   enable LEDEN
    .enable2.horn_en      = 1, ///<   HBEN controls horn block
    .enable2.slp_en       = 0, ///<   Disable sleep
                               /**
                                * \brief Control initialization:
                                *
                                */
    .control.mcu_dis      = 0, ///<   MCU LDO enabled
    .control.vcclow_dis   = 1, ///<   Vcc low moinitor disabled
    .control.mcuerr_dis   = 0, ///<   Fault entered on MCULDO fault
    .control.ots_en       = 1, ///<   Over temperature fault enabled
    .control.softreset    = 0, ///<   do not reset registers
    .control.vcclow_bst   = 0, ///<   boost controlled by BST_EN
    /**
     * \brief Set sleep timer for maximum timeout
     */
    .slptmr1              = 0xff, ///<   Sleep timer most significant bits.1
    .slptmr2              = 0xff, ///<   Sleep timer duration.
    /**
     * \brief gpio_amux initialization
     */
    .gpio_amux.amux_byp   = 0,   ///<  Analog bypass disabled
    .gpio_amux.amux_sel   = 2,   ///<   Photo selected
    .gpio_amux.gpio       = 0x4, ///<   GPIO or LEDENA enables LED A
    /**
     * \brief CO section initially disabled
     */
    .co_battest.coswro    = 0, ///<   Output resistor 0 Ohms
    .co_battest.coswrg    = 0, ///<   CO gain resistor Hi-Z
    .co_battest.coswri    = 0, ///<   CO input resistor 0 Ohms
    .co_battest.coswref   = 0, ///<   positive input of amplifier connected to COP
    .co_battest.i_battest = 0, ///<   Battery Test current 10 mA
    /**
     * \brief CO register initialization
     */
    .co.ref0p3_en         = 0, ///<   Buffer disabled
    .co.coref             = 0, ///<   1.25 mV
    .co.cogain            = 0, ///<   1100 kOhm feedback
    .co.cotest_dir        = 0, ///<   CO test is pull-down
    .co.cotest_en         = 0, ///<   CO Test output disabled
    .co.coamp_en          = 0, ///<   CO amplifier disabled
    /**
     * \brief vboost register initialization
     */
    .vboost.vbst          = 4, ///<  output 9V
    .vboost.bst_clim      = 5, ///<  peak current 100 mA
    /**
     * \brief 7.5 volts
     */
    .ledldo.ledldo        = 0, ///<   7.5 volts
    /**
     * \brief photo control initialization
     */
    .ph_ctrl.tempcob      = 3,   ///<   2 bits mV/C, 0 = 0.347, 1 = 0.416, 2 = 0.693, 3 = 1.040
    .ph_ctrl.tempcoa      = 3,   ///<   2 bits mV/C, 0 = 0.347, 1 = 0.416, 2 = 0.693, 3 = 1.040
    .ph_ctrl.pref_sel     = 1,   ///<   1 bit,       0 = 0mV, 1 = 50mV
    .ph_ctrl.pgain        = 3,   ///<   2 bits gain, 0 = 5 , 1 = 11, 2 = 20, 3 = 35
                                 /**
                                  * \brief Init to 200ma
                                  */
    .led_dac_a            = 170, ///<   LED DAC A setting
    .led_dac_b            = 170  ///<   LED DAC B setting
};

/* ---------------- AFE read Functions ---------------- */

bool afe_read_register(afe_reg_addr const reg_address, uint8_t *const data)
{
    return i2c_read(TPS8802_I2C_ADDRESS, reg_address, data);
}

bool afe_get_revid(uint8_t *const data)
{
    bool const ret_val = afe_read_register(revid, data);
    *data              = *data >> 6;
    return ret_val;
}

bool afe_get_status1(union status1 *const data)
{
    bool const ret_val = afe_read_register(status1, &(data->byte));
    shadow_reg.status1 = *data;
    return ret_val;
}

bool afe_get_status2(union status2 *const data)
{
    bool const ret_val = afe_read_register(status2, &(data->byte));
    shadow_reg.status2 = *data;
    return ret_val;
}

//*********************************************************************
/**
 * \brief Verifies if the write accessible bits in the read data matches the written data
 * \param reg_address The AFE register that was accessed
 * \param written_data The data written to the AFE register
 * \param read_data The data read from the AFE register
 * \return True if the values match, false otherwise
 */
//*********************************************************************
static bool verify_afe_write(afe_reg_addr const reg_address, uint8_t const written_data, uint8_t const read_data)
{
    // Filter out the read only bits per register
    uint8_t const rw_mask = afe_reg_mask[reg_address];
    return (read_data & rw_mask) == (written_data & rw_mask);
}

/* ---------------- AFE write Functions ---------------- */
bool afe_init(void)
{
    // Initialize the write accessible registers
    for (uint8_t i = mask; i < num_afe_regs; i++)
    {
        // dont use afe_write_register to avoid init case.
        i2c_write(TPS8802_I2C_ADDRESS, (afe_reg_addr)i, shadow_reg.reg_array[i]);
        // Read back the register value to verify the data was processed by the AFE
        uint8_t read_value;
        i2c_read(TPS8802_I2C_ADDRESS, (afe_reg_addr)i, &read_value);
        if (!verify_afe_write(i, shadow_reg.reg_array[i], read_value))
        {
            return false;
        }
    }
    return true;
}

bool afe_write_register(afe_reg_addr const reg_address, uint8_t const data)
{
    if (shadow_reg.reg_array[reg_address] != data)
    {
        for (uint8_t i = 0; i < WRITE_COUNT; i++)
        {
            i2c_write(TPS8802_I2C_ADDRESS, reg_address, data);
            // Read back the register value to verify the data was processed by the AFE
            uint8_t read_value;
            i2c_read(TPS8802_I2C_ADDRESS, reg_address, &read_value);
            if (verify_afe_write(reg_address, data, read_value))
            {
                // Value was set successfully.

                // Update the shadow register with the verified value
                shadow_reg.reg_array[reg_address] = data;
                return true;
            }
        }

        // Value could not be successfully written.
        return false;
    }

    return true; // value already set correctly
}

bool afe_set_leda_current(uint8_t const led_current)
{
    return afe_write_register(led_dac_a, led_current);
}

bool afe_set_ledb_current(uint8_t const led_current)
{
    return afe_write_register(led_dac_b, led_current);
}

bool afe_boost_enable(void)
{
    union enable1 en1 = shadow_reg.enable1;
    en1.bst_en        = 1;
    return afe_write_register(enable1, en1.byte);
}

bool afe_boost_disable(void)
{
    union enable1 en1 = shadow_reg.enable1;
    en1.bst_en        = 0;
    return afe_write_register(enable1, en1.byte);
}

bool afe_amux_select(tps8802_amux const setting)
{
    union gpio_amux ga = shadow_reg.gpio_amux;
    ga.amux_sel        = setting;
    return afe_write_register(gpio_amux, ga.byte);
}

bool afe_sleep(void)
{
    union enable2 e2 = shadow_reg.enable2;
    e2.slp_en        = 1;
    return afe_write_register(enable2, e2.byte);
}

bool afe_wake(void)
{
    union enable2 e2 = shadow_reg.enable2;
    e2.slp_en        = 0;
    return afe_write_register(enable2, e2.byte);
}

bool afe_set_intcon_dir(intcon_dir const dir)
{
    union enable2 e2 = shadow_reg.enable2;
    e2.int_dir       = dir;
    return afe_write_register(enable2, e2.byte);
}

bool afe_set_sleep_time(uint16_t const time)
{
    return afe_write_register(slptmr1, (uint8_t)(time >> 8)) && afe_write_register(slptmr2, (uint8_t)time);
}

bool afe_reset(void)
{
    union control ctrl = shadow_reg.control;
    ctrl.softreset     = 1;
    return afe_write_register(control, ctrl.byte);
}

uint8_t afe_get_value(afe_reg_addr const reg_address)
{
    return shadow_reg.reg_array[reg_address];
}

bool afe_write_shadow(afe_reg_addr const reg_address)
{
    for (uint8_t i = 0; i < WRITE_COUNT; i++)
    {
        i2c_write(TPS8802_I2C_ADDRESS, reg_address, shadow_reg.reg_array[reg_address]);
        // Read back the register value to verify the data was processed by the AFE
        uint8_t read_value;
        i2c_read(TPS8802_I2C_ADDRESS, reg_address, &read_value);
        if (verify_afe_write(reg_address, shadow_reg.reg_array[reg_address], read_value))
        {
            // Value was set successfully.
            return true;
        }
    }
    // Value could not be successfully written.
    return false;
}

bool afe_configure_co(void)
{
    union co_battest co_config = shadow_reg.co_battest;
    co_config.coswro           = 1; ///<   Output resistor 100k Ohms
    co_config.coswrg           = 1; ///<   CO gain set by COGAIN bits in CO register
    co_config.coswri           = 1; ///<   CO input resistor 1k Ohms
    co_config.coswref          = 1; ///<   positive input of amplifier connected to 1.25mV - 5mV
    return afe_write_register(co_battest, co_config.byte);
}

bool afe_co_amplifier_enable(void)
{
    union co co_enable = shadow_reg.co;
    co_enable.coref    = 3;
    co_enable.coamp_en = 1;
    return afe_write_register(co, co_enable.byte);
}

bool afe_vcc_low(void)
{
    union status1 status;
    if (!afe_read_register(status1, &(status.byte)))
    {
        return true;
    }
    return status.vcclow;
}
