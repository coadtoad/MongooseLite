//******************************************************************
/*! \file message.h
 *
 * \brief Kidde: Mongoose-fw - Declare the message functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_MESSAGE_H_
#define INC_APP_MESSAGE_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief scheduler 1 minute - in milliseconds
 */
//*********************************************************************
#define ONE_SECOND (uint32_t)(1000)

//*********************************************************************
/**
 * \brief scheduler 1 minute - in milliseconds
 */
//*********************************************************************
#define ONE_MINUTE (uint32_t)(60 * ONE_SECOND)

//*********************************************************************
/**
 * \brief scheduler 1 hour - in milliseconds
 */
//*********************************************************************
#define ONE_HOUR   (uint32_t)(60 * ONE_MINUTE)

//*********************************************************************
/**
 * \brief scheduler 1 day - in Milliseconds
 */
//*********************************************************************
#define ONE_DAY    (uint32_t)(24 * ONE_HOUR) // 24 hours

//*********************************************************************
/**
 * \brief schedule a message to be sent
 * \param time_ms time in ms for message to occur
 * \param task task to be called, task id
 * \param message message to be sent, message id
 * \param data any data sent with the message
 */
//*********************************************************************
//void //schedule_message(uint32_t const time_ms, task_id const task, message_id const message, uint16_t const data);

//*********************************************************************
/**
 * \brief schedule a message to be sent at a certain time
 * \param desired_period_ms time in ms when message will occur
 * \param task task to be called, task id
 * \param message message to be sent, message id
 * \param data any data sent with the message
 */
//*********************************************************************
void schedule_timed_message(uint32_t const desired_period_ms, task_id const task, message_id const message,
                            uint16_t const data);

//*********************************************************************
/**
 * \brief unschedule a message that was scheduled to occur
 * \param task task to be unscheduled, task id
 */
//*********************************************************************
//void un//schedule_messages_t(task_id const task);

//*********************************************************************
/**
 * \brief unschedule a message that was scheduled to occur
 * \param task task to be unscheduled, task id
 * \param message message to be unscheduled, message id
 */
//*********************************************************************
//void un//schedule_messages_tm(task_id const task, message_id const message);

//*********************************************************************
/**
 * \brief unschedule a message, with data, that was scheduled to occur
 * \param task task to be unscheduled, task id
 * \param message message to be unscheduled, message id
 * \param data data to be unscheduled and no longer sent
 */
//*********************************************************************
//void un//schedule_messages_tmd(task_id const task, message_id const message, uint16_t const data);

//*********************************************************************
/**
 * \brief obliterate ends any task or event
 */
//*********************************************************************
void obliterate(void);

#endif /* INC_APP_MESSAGE_H_ */
