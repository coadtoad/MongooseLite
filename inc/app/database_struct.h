//******************************************************************
/*! \file database_struct.h
 *
 * \brief Kidde: Mongoose-fw - The main header file for mongoose-fw
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#define DB_NORMAL_LINE(index, val) (val),
#define DB_LAST_LINE(index, val)   (val)

DB_NORMAL_LINE(0, CONFIG_CANARY)                                 ///< db_canary
DB_NORMAL_LINE(1, CONFIG_MARKET)                                 ///< db_market
DB_NORMAL_LINE(2, (uint8_t)(CONFIG_SERIAL_ENABLE >> 8))          ///< db_serial_enable_msb
DB_NORMAL_LINE(3, (uint8_t)CONFIG_SERIAL_ENABLE)                 ///< db_serial_enable_lsb
DB_NORMAL_LINE(4, CONFIG_SERIAL_FLAGS)                           ///< db_serial_flags
DB_NORMAL_LINE(5, (uint8_t)(CONFIG_CALIBRATION >> 8))            ///< db_calibration_msb
DB_NORMAL_LINE(6, (uint8_t)CONFIG_CALIBRATION)                   ///< db_calibration_lsb
DB_NORMAL_LINE(7, CONFIG_MFG_DATA_RESERVED)                      ///< db_mfg_data_0
DB_NORMAL_LINE(8, CONFIG_MFG_DATA_RESERVED)                      ///< db_mfg_data_1
DB_NORMAL_LINE(9, CONFIG_MFG_DATA_RESERVED)                      ///< db_mfg_data_2
DB_NORMAL_LINE(10, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_3
DB_NORMAL_LINE(11, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_4
DB_NORMAL_LINE(12, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_5
DB_NORMAL_LINE(13, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_6
DB_NORMAL_LINE(14, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_7
DB_NORMAL_LINE(15, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_8
DB_NORMAL_LINE(16, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_9
DB_NORMAL_LINE(17, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_10
DB_NORMAL_LINE(18, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_11
DB_NORMAL_LINE(19, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_12
DB_NORMAL_LINE(20, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_13
DB_NORMAL_LINE(21, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_14
DB_NORMAL_LINE(22, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_15
DB_NORMAL_LINE(23, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_16
DB_NORMAL_LINE(24, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_17
DB_NORMAL_LINE(25, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_18
DB_NORMAL_LINE(26, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_19
DB_NORMAL_LINE(27, CONFIG_MFG_DATA_RESERVED)                     ///< db_mfg_data_20
DB_NORMAL_LINE(28, CONFIG_CURRENT_IR_F)                          ///< db_current_ir_f
DB_NORMAL_LINE(29, CONFIG_CURRENT_IR_B)                          ///< db_current_ir_b
DB_NORMAL_LINE(30, CONFIG_CURRENT_BL_F)                          ///< db_current_bl_f
DB_NORMAL_LINE(31, CONFIG_PHOTO_SW_WAIT)                         ///< db_photo_sw_wait
DB_NORMAL_LINE(32, CONFIG_PTT_CURRENT_IR_F)                      ///< db_ptt_current_ir_f
DB_NORMAL_LINE(33, CONFIG_PTT_CURRENT_IR_B)                      ///< db_ptt_current_ir_b
 DB_NORMAL_LINE(34, UNUSED_VALUE)                     ///< db_ptt_fault_history
DB_NORMAL_LINE(35, CONFIG_PHOTO_ALARM_EQA_THRESHOLD)             ///< db_photo_alarm_eqa_threshold
DB_NORMAL_LINE(36, CONFIG_PHOTO_HUSH_THRESHOLD)                  ///< db_photo_hush_threshold
DB_NORMAL_LINE(37, CONFIG_PHOTO_FAIL_SAFE_THRESHOLD)             ///< db_photo_fail_safe_threshold
DB_NORMAL_LINE(38, CONFIG_IR_F_CLEAN_AIR)                        ///< db_ir_f_factory_clean_air
DB_NORMAL_LINE(39, CONFIG_IR_B_CLEAN_AIR)                        ///< db_ir_b_factory_clean_air
DB_NORMAL_LINE(40, CONFIG_BLUE_F_CLEAN_AIR)                      ///< db_blue_factory_clean_air
DB_NORMAL_LINE(41, CONFIG_PHOTO_CAL_IR_F_MIN)                    ///< db_photo_cal_ir_f_min
DB_NORMAL_LINE(42, CONFIG_PHOTO_CAL_IR_F_MAX)                    ///< db_photo_cal_ir_f_max
DB_NORMAL_LINE(43, CONFIG_PHOTO_CAL_IR_B_MIN)                    ///< db_photo_cal_ir_b_min
DB_NORMAL_LINE(44, CONFIG_PHOTO_CAL_IR_B_MAX)                    ///< db_photo_cal_ir_b_max
DB_NORMAL_LINE(45, CONFIG_PHOTO_CAL_BLUE_MIN)                    ///< db_photo_cal_blue_min
DB_NORMAL_LINE(46, CONFIG_PHOTO_CAL_BLUE_MAX)                    ///< db_photo_cal_blue_max
DB_NORMAL_LINE(47, CONFIG_IR_F_CLEAN_AIR)                        ///< db_ir_f_clean_air
DB_NORMAL_LINE(48, CONFIG_IR_B_CLEAN_AIR)                        ///< db_ir_b_clean_air
DB_NORMAL_LINE(49, CONFIG_BLUE_F_CLEAN_AIR)                      ///< db_blue_clean_air
DB_NORMAL_LINE(50, CONFIG_PHOTO_CAL_BLUE_BACK_SUPERVISION)       ///< db_photo_cal_blue_back_supervision
DB_NORMAL_LINE(51, CONFIG_CO_CIRCUIT_CAL)                        ///< db_co_circuit_cal
DB_NORMAL_LINE(52, (uint8_t)(CONFIG_CO_OFFSET >> 8))             ///< db_co_offset_msb
DB_NORMAL_LINE(53, (uint8_t)CONFIG_CO_OFFSET)                    ///< db_co_offset_lsb
DB_NORMAL_LINE(54, (uint8_t)(CONFIG_CO_SCALE >> 8))              ///< db_co_scale_msb
DB_NORMAL_LINE(55, (uint8_t)CONFIG_CO_SCALE)                     ///< db_co_scale_lsb
DB_NORMAL_LINE(56, (uint8_t)(CONFIG_BATTERY_VOLTAGE_MIN >> 8))   ///< db_battery_voltage_min_msb
DB_NORMAL_LINE(57, (uint8_t)CONFIG_BATTERY_VOLTAGE_MIN)          ///< db_battery_voltage_min_lsb
DB_NORMAL_LINE(58, (uint8_t)(CONFIG_HUSH_MULTIPLIER >> 8))       ///< db_hush_multiplier_msb
DB_NORMAL_LINE(59, (uint8_t)CONFIG_HUSH_MULTIPLIER)              ///< db_hush_multiplier_lsb
DB_NORMAL_LINE(60, (uint8_t)(CONFIG_FAILSAFE_MULTIPLIER >> 8))   ///< db_failsafe_multiplier_msb
DB_NORMAL_LINE(61, (uint8_t)CONFIG_FAILSAFE_MULTIPLIER)          ///< db_failsafe_multiplier_lsb
DB_NORMAL_LINE(62, (uint8_t)(AMBIENT_LIGHT_CAL_MULTIPLIER >> 8)) ///< db_ambient_light_cal_multiplier_msb
DB_NORMAL_LINE(63, (uint8_t)AMBIENT_LIGHT_CAL_MULTIPLIER)        ///< db_ambient_light_cal_multiplier_lsb
DB_NORMAL_LINE(64, CONFIG_FAILSAFE_MIN)                          ///< db_failsafe_min
DB_NORMAL_LINE(65, CONFIG_FAILSAFE_MAX)                          ///< db_failsafe_max
DB_NORMAL_LINE(66, CONFIG_PHOTO_READ_SAMPLE_DELAY)               ///< db_photo_read_sample_delay
DB_NORMAL_LINE(67, CONFIG_PHOTO_READ_SAMPLE_COUNT)               ///< db_photo_read_sample_count
DB_NORMAL_LINE(68, CONFIG_PHOTO_READ_SAMPLE_DISCARD)             ///< db_photo_read_sample_discard

#if defined(CONFIG_PHOTO_MWMA)
// MWMA photo database elements
DB_NORMAL_LINE(69, CONFIG_FLM_IR_B_THRESHOLD)                          ///< db_flm_ir_b_threshold
DB_NORMAL_LINE(70, (uint8_t)(CONFIG_FLAMING_IR_F_IR_B_THRESHOLD >> 8)) ///< db_flaming_ir_f_ir_b_threshold_msb
DB_NORMAL_LINE(71, (uint8_t)CONFIG_FLAMING_IR_F_IR_B_THRESHOLD)        ///< db_flaming_ir_f_ir_b_threshold_lsb
DB_NORMAL_LINE(72, (uint8_t)(CONFIG_FLAMING_BL_F_IR_B_THRESHOLD >> 8)) ///< db_flaming_bl_f_ir_b_threshold_msb
DB_NORMAL_LINE(73, (uint8_t)CONFIG_FLAMING_BL_F_IR_B_THRESHOLD)        ///< db_flaming_bl_f_ir_b_threshold_lsb
DB_NORMAL_LINE(74, (uint8_t)(CONFIG_PHOTO_IR_F_IR_B_THRESHOLD >> 8))   ///< db_photo_ir_f_ir_b_threshold_msb
DB_NORMAL_LINE(75, (uint8_t)CONFIG_PHOTO_IR_F_IR_B_THRESHOLD)          ///< db_photo_ir_f_ir_b_threshold_lsb
DB_NORMAL_LINE(76, (uint8_t)(CONFIG_PHOTO_BL_F_IR_B_THRESHOLD >> 8))   ///< db_photo_bl_f_ir_b_threshold_msb
DB_NORMAL_LINE(77, (uint8_t)CONFIG_PHOTO_BL_F_IR_B_THRESHOLD)          ///< db_photo_bl_f_ir_b_threshold_lsb
DB_NORMAL_LINE(78, CONFIG_FWD_OPEN_TH)                                 ///< db_fwd_open_th
DB_NORMAL_LINE(79, CONFIG_BCK_OPEN_TH)                                 ///< db_bck_open_th
DB_NORMAL_LINE(80, CONFIG_FWD_HI_AMB_TH)                               ///< db_fwd_hi_amb_th
DB_NORMAL_LINE(81, CONFIG_BCK_HI_AMB_TH)                               ///< db_bck_hi_amb_th
DB_NORMAL_LINE(82, UNUSED_VALUE)                                       //
DB_NORMAL_LINE(83, CONFIG_PHOTO_SMOLDERING_COUNT_THRESHOLD)            ///< db_photo_smoldering_count_threshold
DB_NORMAL_LINE(84, CONFIG_NORMAL_MODE_PERIOD)                          ///< db_normal_mode_period
DB_NORMAL_LINE(85, CONFIG_PHOTO_IR_F_DELTA)                            ///< db_photo_ir_f_delta
DB_NORMAL_LINE(86, CONFIG_PHOTO_IR_B_DELTA)                            ///< db_photo_ir_b_delta
DB_NORMAL_LINE(87, CONFIG_PHOTO_BLUE_F_DELTA)                          ///< db_photo_bl_f_delta
DB_NORMAL_LINE(88, CONFIG_FIRE_COUNT)                                  ///< db_fire_count
DB_NORMAL_LINE(89, CONFIG_FIRE_DEBOUNCE_COUNT)                         ///< db_fire_debounce_count

#elif defined(CONFIG_PHOTO_SINGLE)
// Single Channel database elements  need to total the same as above
DB_NORMAL_LINE(69, CONFIG_NORMAL_MODE_PERIOD)                   ///< db_normal_mode_period
DB_NORMAL_LINE(70, CONFIG_PHOTO_IR_F_DELTA)                     ///< db_photo_ir_f_delta
DB_NORMAL_LINE(71, CONFIG_PHOTO_IR_B_DELTA)                     ///< db_photo_ir_b_delta
DB_NORMAL_LINE(72, CONFIG_PHOTO_LOW_DARK_FAULT_TH)              ///< db_photo_low_dark_fault_threshold
DB_NORMAL_LINE(73, CONFIG_PHOTO_HI_DARK_FAULT_TH)               ///< db_photo_hi_dark_fault_threshold
DB_NORMAL_LINE(74, CONFIG_PHOTO_LOW_FWD_LIGHT_TH)               ///< db_photo_low_fwd_light_threshold
DB_NORMAL_LINE(75, CONFIG_PHOTO_LOW_BCK_LIGHT_TH)               ///< db_photo_low_bck_light_threshold
DB_NORMAL_LINE(76, CONFIG_SMOLD_COUNT_THRESHOLD)                ///< db_smold_count_threshold
DB_NORMAL_LINE(77, (uint8_t)(CONFIG_SMOLD_TIME_THRESHOLD >> 8)) ///< db_smold_time_threshold_msb
DB_NORMAL_LINE(78, (uint8_t)CONFIG_SMOLD_TIME_THRESHOLD)        ///< db_smold_time_threshold_lsb
DB_NORMAL_LINE(79, (uint8_t)(CONFIG_ROR_THRESHOLD >> 8))        ///< db_ror_threshold_msb
DB_NORMAL_LINE(80, (uint8_t)CONFIG_ROR_THRESHOLD)               ///< db_ror_threshold_lsb
DB_NORMAL_LINE(81, CONFIG_ROR_CONFIDENCE_CHECK)                 ///< db_ror_confidence_check
DB_NORMAL_LINE(82, CONFIG_ROR_MIN_THRESHOLD)                    ///< db_ror_min_threshold
DB_NORMAL_LINE(83, (uint8_t)(CONFIG_ACCEL_THRESHOLD >> 8))      ///< db_accel_threshold_msb
DB_NORMAL_LINE(84, (uint8_t)CONFIG_ACCEL_THRESHOLD)             ///< db_accel_threshold_lsb
DB_NORMAL_LINE(85, CONFIG_ACCEL_ROR_THRESHOLD)                  ///< db_accel_ror_threshold
DB_NORMAL_LINE(86, CONFIG_ACCEL_CONFIDENCE_CHECK)               ///< db_accel_confidence_check
DB_NORMAL_LINE(87, CONFIG_ACCEL_MIN_THRESHOLD)                  ///< db_accel_min_threshold
DB_NORMAL_LINE(88, CONFIG_FAIL_SAFE_HIGH_THRESHOLD)             ///< db_fail_safe_high_threshold
DB_NORMAL_LINE(89, CONFIG_FAIL_SAFE_CONFIDENCE_CHECK)           ///< db_fail_safe_confidence_check

#endif
DB_NORMAL_LINE(90, CONFIG_PHOTO_NORMAL_SENSORS)                   ///< db_photo_normal_sensors
DB_NORMAL_LINE(91, CONFIG_INLINE_START_IRF_DELTA_TH)              ///< inline_start_irf_delta_th
DB_NORMAL_LINE(92, CONFIG_INLINE_RATIO_STABILIZATION_TIME)        ///< inline_ratio_stabilization_time
DB_NORMAL_LINE(93, CONFIG_INLINE_RATIO_SAMPLE_COUNT)              ///< inline_ratio_sample_count
DB_NORMAL_LINE(94, (uint8_t)(CONFIG_IRF_IRB_RATIO_TARGET >> 8))   ///< irf_irb_ratio_target_msb
DB_NORMAL_LINE(95, (uint8_t)CONFIG_IRF_IRB_RATIO_TARGET)          ///< irf_irb_ratio_target_lsb
DB_NORMAL_LINE(96, (uint8_t)(CONFIG_BLF_IRB_RATIO_TARGET >> 8))   ///< blf_irb_ratio_target_msb
DB_NORMAL_LINE(97, (uint8_t)CONFIG_BLF_IRB_RATIO_TARGET)          ///< blf_irb_ratio_target_lsb
DB_NORMAL_LINE(98, CONFIG_IRF_IRB_FACTOR_EQUATION_K1)             ///< irf_irb_factor_equation_k1
DB_NORMAL_LINE(99, CONFIG_IRF_IRB_FACTOR_EQUATION_K2)             ///< irf_irb_factor_equation_k2
DB_NORMAL_LINE(100, CONFIG_BLF_IRB_FACTOR_EQUATION_K3)            ///< blf_irb_factor_equation_k3
DB_NORMAL_LINE(101, CONFIG_BLF_IRB_FACTOR_EQUATION_K4)            ///< blf_irb_factor_equation_k4
DB_NORMAL_LINE(102, CONFIG_IRF_IRB_RATIO_FACTOR)                  ///< irf_irb_ratio_factor
DB_NORMAL_LINE(103, CONFIG_BLF_IRB_RATIO_FACTOR)                  ///< blf_irb_ratio_factor
DB_NORMAL_LINE(104, UNUSED_VALUE)                                 ///<
DB_NORMAL_LINE(105, UNUSED_VALUE)                                 ///<
DB_NORMAL_LINE(106, UNUSED_VALUE)                                 ///<
DB_NORMAL_LINE(107, UNUSED_VALUE)                                 ///<
DB_NORMAL_LINE(108, UNUSED_VALUE)                                 ///<
DB_NORMAL_LINE(109, (uint8_t)(CONFIG_COUNTER_NO_OF_DAYS >> 8))    ///< db_counter_no_of_days_msb
DB_NORMAL_LINE(110, (uint8_t)CONFIG_COUNTER_NO_OF_DAYS)           ///< db_counter_no_of_days_lsb
DB_NORMAL_LINE(111, (uint8_t)(CONFIG_COUNTER_SENSOR_HEALTH >> 8)) ///< db_counter_sensor_health_msb
DB_NORMAL_LINE(112, (uint8_t)CONFIG_COUNTER_SENSOR_HEALTH)        ///< db_counter_sensor_health_lsb
DB_NORMAL_LINE(113, (uint8_t)(CONFIG_COUNTER_SENSOR_SHORT >> 8))  ///< db_counter_sensor_short_msb
DB_NORMAL_LINE(114, (uint8_t)CONFIG_COUNTER_SENSOR_SHORT)         ///< db_counter_sensor_short_lsb
DB_NORMAL_LINE(115, (uint8_t)(CONFIG_COUNTER_SMOKE_CHAMBER >> 8)) ///< db_counter_smoke_chamber_msb
DB_NORMAL_LINE(116, (uint8_t)CONFIG_COUNTER_SMOKE_CHAMBER)        ///< db_counter_smoke_chamber_lsb
DB_NORMAL_LINE(117, (uint8_t)(CONFIG_COUNTER_PTT >> 8))           ///< db_counter_ptt_msb
DB_NORMAL_LINE(118, (uint8_t)CONFIG_COUNTER_PTT)                  ///< db_counter_ptt_lsb
DB_NORMAL_LINE(119, UNUSED_VALUE)                                 ///<
DB_NORMAL_LINE(120, UNUSED_VALUE)                                 ///<
DB_NORMAL_LINE(121, (uint8_t)(CONFIG_ERASE_COUNT >> 8))           ///< db_erase_count_msb
DB_NORMAL_LINE(122, (uint8_t)CONFIG_ERASE_COUNT)                  ///< db_erase_count_lsb
DB_LAST_LINE(123, CONFIG_PAGE_NUMBER)                             ///< db_page_number

#undef DB_NORMAL_LINE
#undef DB_LAST_LINE
