//******************************************************************
/*! \file app_supervisor.h
 *
 * \brief Kidde: Mongoose-fw - Declare app supervisor task functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_SUPERVISOR_H_
#define INC_APP_SUPERVISOR_H_

#include "message_id.h"
#include <stdint.h>

/**
 * @brief Counter index enum
 */
typedef enum
{
    counter_photo,
    counter_co,
    counter_expressions,
    counter_size
} counter_index;

//*********************************************************************
/**
 * \brief Get counter value
 * \param index Counter index variable
 */
//*********************************************************************
uint16_t supervisor_counter_get_value(counter_index const index);

//*********************************************************************
/**
 * \brief Increase value of counter
 * \param index Counter index variable
 */
//*********************************************************************
void supervisor_counter_increase(counter_index const index);

//*********************************************************************
/**
 * \brief Clear counter value
 * \param index Counter index variable
 */
//*********************************************************************
void supervisor_counter_clear(counter_index const index);

//*********************************************************************
/**
 * \brief Supervisor task assignment function
 * \param message message ID passed to supervisor task
 * \param data Any data sent with each supervisor task
 */
//*********************************************************************
void supervisor_task(message_id const message, uint16_t const data);

#endif /* INC_APP_SUPERVISOR_H_ */
