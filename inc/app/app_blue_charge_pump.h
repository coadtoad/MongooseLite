//******************************************************************
/*! \file app_blue_charge_pump.h
 *
 * \brief Kidde: Mongoose-fw - Define the blue charge pump function
 *
 * \author unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_APP_BLUE_CHARGE_PUMP_H_
#define INC_APP_APP_BLUE_CHARGE_PUMP_H_

#include "message_id.h"
#include <stdint.h>

void blue_charge_pump_task(message_id const message, uint16_t const data);

#endif /* APP_BLUE_CHARGE_PUMP_H_ */
