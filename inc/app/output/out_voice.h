//******************************************************************
/*! \file out_voice.h
 *
 * \brief Kidde: Mongoose-fw - Declare Voice output functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_OUT_VOICE_H_
#define INC_APP_OUT_VOICE_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief Enum of possible voice message outputs to the user
 */
//*********************************************************************
typedef enum
{
    voice_fire,
    voice_co,
    voice_hush_active,
    voice_co_prev_det,
    voice_push_test_btn,
    voice_low_battery,
    voice_smk_prev_det,
    voice_replace_alarm,
    voice_too_much_smk,
    voice_co_sens_error,
    voice_smk_sens_error,
    voice_intcon_error,
    voice_memory_error,
    voice_activate_battery,

    voice_fr_fire,
    voice_fr_co,
    voice_fr_hush_active,
    voice_fr_co_detected,
    voice_fr_push_test_btn,
    voice_fr_low_battery,
    voice_fr_smk_prev_det,
    voice_fr_replace_alarm,
    voice_fr_too_much_smk,
    voice_fr_co_sens_error,
    voice_fr_smk_sens_error,
    voice_fr_intcon_error,
    voice_fr_memory_error,
    voice_fr_activate_battery,

    voice_english_selected,

    voice_ready_to_connect,
    voice_setup_complete,
    voice_devices_connected,
    voice_success,
    voice_no_devices_found,
    voice_search_for_devices,
    voice_reset_wireless,
    voice_now_connected,
    voice_not_connected,
    voice_connection_lost,

    voice_one,
    voice_two,
    voice_three,
    voice_four,
    voice_five,
    voice_six,
    voice_seven,
    voice_eight,
    voice_nine,
    voice_ten,
    voice_eleven,
    voice_twelve,
    voice_thirteen,
    voice_fourteen,
    voice_fifteen,
    voice_sixteen,
    voice_seventeen,
    voice_eighteen,
    voice_nineteen,
    voice_twenty,
    voice_twenty_one,
    voice_twenty_two,
    voice_twenty_three,
    voice_twenty_four,

    voice_hush_cancel,
    voice_temporary_silence,
    voice_test_cancel,
    voice_testing_very_loud,
    voice_test_complete,
    voice_press_to_cancel,
    voice_error_see_guide,
    voice_press_to_silence,

    voice_emergency,
    voice_weather,
    voice_water_leak,
    voice_explosive_gas,
    voice_temperature_drop,
    voice_intruder,

    voice_tone_sonar_ping,
    voice_tone_button_press,
    voice_tone_network_close,
    voice_tone_network_error,
    voice_tone_power_on,
    voice_tone_push_to_test,
    voice_tone_out_of_box,
    voice_tone_activate_batt,

    // end of enum list
    number_of_voices
} voice_message;

//*********************************************************************
/**
 * \brief Voice output task function
 * \param message message ID passed to Voice output task
 * \param data Any data sent with the Voice output task
 */
//*********************************************************************
void voice_task(message_id const message, uint16_t const data);

#endif /* INC_APP_OUT_VOICE_H_ */
