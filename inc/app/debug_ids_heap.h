//******************************************************************
/*! \file debug_ids_heap.h
 *
 * \brief Kidde: Mongoose-fw - Declare heap functions
 *
 * \author Justin Henry
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_DEBUG_IDS_H_
#define INC_APP_DEBUG_IDS_H_

#include "app_serial.h"
#include "message_id.h"
#include "task_id.h"
#include <stdint.h>

//*********************************************************************
/**	\brief Output debug message
 * \param task Task ID passed to debug output
 * \param message Message ID passed to debug output
 * \param data Any data sent with each debug output
 */
//*********************************************************************
void message_debug_out(task_id const task, message_id const message, uint16_t data);

//*********************************************************************
/**	\brief Send debug message over serial
 * \param message Message ID sent to serial output
 * \param output_flag Output flag type to serial
 */
//*********************************************************************
void serial_send_msg_debug(message_id const message, Serial_output_flags_type_e output_flag);

#endif
