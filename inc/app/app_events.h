//******************************************************************
/*! \file app_events.h
 *
 * \brief Kidde: Mongoose-fw - Define the app event functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_APP_EVENTS_H_
#define INC_APP_APP_EVENTS_H_

#include "drv_events.h"
#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief Event ID list enum
 */
//*********************************************************************
typedef enum
{
    event_ac_power_off           = 0x02, ///< AC Power Off detected
    event_ac_power_on            = 0x03, ///< AC Power On detected
    event_ptt                    = 0x04, ///< Push to Test Pressed
    event_battery_low            = 0x05, ///< Low Battery Mode Entered
    event_battery_low_fatal      = 0x06, ///< Low Battery Fatal Mode Entered
    event_battery_none           = 0x07, ///< No or Abnormally low Battery
    event_reset_por              = 0x10, ///< Power On Reset Event
    event_reset_opt_byte         = 0x11, ///< Option byte caused reset
    event_reset_wdtrf            = 0x12, ///< Window WDT caused reset
    event_reset_illgl_mode       = 0x13, ///< Power On Reset Event
    event_reset_pin              = 0x14, ///< Reset occurred on NRST pin
    event_reset_unknown          = 0x15, ///< Unknown cause of reset
    event_reset_iwdtrf           = 0x16, ///< Independent Window WDT reset
    event_reset_swrf             = 0x17, ///< Soft Reset
    event_reset_extrf            = 0x18, ///< Power On Reset Event
    event_reset_borf_por         = 0x19, ///< Power On Reset or BOR Event
    event_enter_analysis         = 0x20, ///< Entered Analysis Mode from Normal Mode
    event_exit_analysis          = 0x21, ///< Exited Analysis Mode

    event_fault_co_sensor_health = 0x82, ///< CO Sensor Health Fault
    event_fault_co_sensor_short  = 0x83, ///< CO Sensor Short Fault
    event_fault_drift_comp       = 0x84, ///< Drift Comp Limit Fault
    event_fault_intcon_super     = 0x85, ///< INT Supervision Fault
    event_fault_ptt              = 0x86, ///< Push to Test Fault
    event_fault_fatal_mem        = 0x87, ///< Memory Fatal Fault
    event_life_expire            = 0x88, ///< Life Expiration Fault 'E09'
    event_fault_co_cleared       = 0x8A, ///< CO Fault Clear
    event_ram_database           = 0x8B, ///< Memory Specific Fault Events
    event_nv_data_primary        = 0x8C, ///< New value primary data
    event_nv_data_backup         = 0x8D, ///< New value backup data
    event_fault_co_sensor_test   = 0x8E, ///< CO Sensor TestFault (PTT)
    event_fault_button_stuck     = 0x8F, ///< Button Stuck
    event_fault_32khz_xtal       = 0x90, ///< 32khz didn't start in time
    event_smoke_alarm_on         = 0x91, ///< smoke alarm on unit
    event_smoke_alarm_intcon_on  = 0x92, ///< smoke alarm from interconnect
    event_co_alarm_on            = 0x93, ///< CO alarm on unit
    event_co_alarm_intcon_on     = 0x94, ///< CO alarm from interconnect
    event_smoke_alarm_off        = 0x95, ///< smoke alarm on unit
    event_smoke_alarm_intcon_off = 0x96, ///< smoke alarm from interconnect
    event_fault_fatal_afe        = 0x97, ///< Fatal AFE fault
    event_fault_photo_cav     = 0xA0, ///< Photo PhotoCavFault              // clean air - 2 hours      no longer used
    event_fault_photo_fwd_amb = 0xA1, ///< Photo ForwardAmbientFault        // normal and analasys
    event_fault_photo_bck_amb = 0xA2, ///< Photo BackAmbientFault           // clean air and analasys
    event_fault_photo_fwd     = 0xA3, ///< Photo ForwardFault               // normal and analysis
    event_fault_photo_bck     = 0xA4, ///< Photo BackFault                  // clean air and analasys
    event_fault_photo_blue =
        0xA5, ///< Photo BlueLedSupervision         // clean air - 2 hours      clear when all other cav's are cleared.
    event_fault_photo_cav_clr      = 0xA6, ///< Photo PhotoCavFault              // clear
    event_fault_photo_fwd_amb_clr  = 0xA7, ///< Photo ForwardAmbientFault        // clear
    event_fault_photo_bck_amb_clr  = 0xA8, ///< Photo BackAmbientFault           // clear
    event_fault_photo_fwd_clr      = 0xA9, ///< Photo ForwardFault               // clear
    event_fault_photo_bck_clr      = 0xAA, ///< Photo BackFault                  // clear
    event_fault_photo_blue_clr     = 0xAB, ///< Photo BlueLedSupervision         // clear
    event_fault_photo_clr          = 0xAC, ///< ALL photo Fault Clear
    event_fault_fatal_no_cal       = 0xAD, ///< No calibration fault
    event_fault_photo_AFE          = 0xAE, ///< AFE photo fault
    event_fault_photo_cav_ir_f_hi  = 0xB0, ///< CAV IR F too high
    event_fault_photo_cav_ir_f_lo  = 0xB1, ///< CAV IR F too low
    event_fault_photo_cav_ir_b_hi  = 0xB2, ///< CAV IR B too high
    event_fault_photo_cav_ir_b_lo  = 0xB3, ///< CAV IR B too low
    event_fault_photo_cav_bl_f_hi  = 0xB4, ///< CAV BL F too high
    event_fault_photo_cav_bl_f_lo  = 0xB5, ///< CAV BL F too low
    event_smoke_alarm_wireless_on  = 0xB6, ///< Smoke alarm wireless on
    event_smoke_alarm_wireless_off = 0xB7, ///< Smoke alarm wireless off
    event_co_alarm_wireless_on     = 0xB8, ///< CO alarm wireless on
    event_co_alarm_wireless_off    = 0xB9, ///< CO alarm wireless off
    event_fault_wireless_network   = 0xBA, ///< Wireless network fault
    event_wireless_search          = 0xBB, ///< Wireles search
    event_fault_cci_failure        = 0xBC, ///< CCI fault
    event_fault_photo_amb_high     = 0xBD, ///< fault for dark reading too high - single channel
    event_fault_photo_amb_high_clr = 0xBE, ///< fault for dark reading too high clear - single channel
    event_fault_photo_amb_low      = 0xBF, ///< fault for dark reading too low - single channel
    event_fault_photo_amb_low_clr  = 0xC0, ///< fault for dark reading too low clear - single channel
} event_id;

//*********************************************************************
/** \brief Event task assignment function
 * \param message message ID passed to event task
 * \param data Any data sent with each event task
 */
//*********************************************************************
void events_task(message_id const message, uint16_t const data);

//*********************************************************************
/** \brief Write a single event to flash with syncrhonous flash actions
 * \param id Event ID to write to flash
 * \returns True if the event was successfully written and verified in flash
 * \note The purpose of this function is to record events while the scheduler is unable to process messages
 */
//*********************************************************************
bool synchronous_event_write(event_id const id);

#endif /* INC_APP_APP_EVENTS_H_ */
