//******************************************************************
/*! \file photo_common_single.h
 *
 * \brief Kidde: Mongoose-fw - Declare photo single led chamber common functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_PHOTO_COMMON_H_
#define INC_APP_PHOTO_COMMON_H_

#include "drv_photo.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief OP AMP time for stability
 */
//*********************************************************************
#define PHOTO_OP_AMP_STABLE_TIME 0x80

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define IN_NORMAL_MODE_MASK      0x1000

//*********************************************************************
/**
 * \brief Used to specify the photo mode.
 */
//*********************************************************************
typedef enum
{
    NORMAL_MODE,
    ANALYSIS_MODE,
    PTT_MODE
} photo_mode_e;

//*********************************************************************
/**
 * \brief Enum to determine photo gain ammount
 * \note used in read_light() function
 */
//*********************************************************************
typedef enum
{
    high_gain,
    low_gain,
    no_gain,
} gain_level;

typedef enum
{
    dark_reading,
    ir_f_reading,
    ir_b_reading,
    num_photo_data,
} photo_data;

//*********************************************************************
/**
 * \brief Used to place the different fire modes in the fire flag.
 */
//*********************************************************************
typedef enum
{
    SLOW_SMOLDERING_FIRE_BIT,
    FAST_FLAIMG_FIRE_BIT,
    FAST_ACCELERATING_FIRE_BIT,
    FAIL_SAFE_FIRE_BIT,
    LEGACY_FAILSAFE_BIT,
    HUSHABLE_BIT,
} photo_single_fire_flags_bits;

void set_ir_f_override(uint8_t value);
void set_ir_b_override(uint8_t value);
void set_bl_f_override(uint8_t value);
void clear_photo_override(void);

bool dark_faulted(void);

void update_ir_photo_fault_kvs(void);

bool read_photo(const uint8_t ir_f_dac, const uint8_t ir_b_dac, uint8_t return_array[]);

#endif /* INC_APP_PHOTO_COMMON_H_ */
