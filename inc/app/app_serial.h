//******************************************************************
/*! \file app_serial.h
 *
 * \brief Kidde: Mongoose-fw - Declare the APP SERIAL functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_APP_SERIAL_H_
#define INC_APP_APP_SERIAL_H_


#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Mode type enum, Normal vs AFE
 */
//*********************************************************************
typedef enum
{
    MODE_NORMAL,
    MODE_AFE,
} Mode_type_e;

/** Error string text output     */
#define ERR_STRING    "Error!"
/** Invalid Command text output     */
#define INVALID_CMD   "Invalid Command"
/** Invalid Entry text output     */
#define INVALID_ENTRY "Invalid Entry"

//*********************************************************************
/**
 * \brief Serial task type enum
 */
//*********************************************************************
typedef enum
{
    SERIAL_CMD_PROCESS,
    SERIAL_PERIODIC_TASK_CO,
    SERIAL_SND_DB_RAM,
    SERIAL_SND_DB_PRIMARY,
    SERIAL_SND_DB_BACKUP,
    SERIAL_SND_DB_EVENTS,
    SERIAL_SND_DB_ALL
} Serial_task_type_e;

//*********************************************************************
/**
 * \brief serial output flags enum
 */
//*********************************************************************
typedef enum
{
    serial_enable_always       = 0x0000,
    serial_enable_priority     = 0x0001, // B00
    serial_enable_calibration  = 0x0002, // B01
    serial_enable_co           = 0x0004, // B02
    serial_enable_battery      = 0x0008, // B03
    serial_enable_debug        = 0x0010, // B04
    serial_enable_ptt          = 0x0020, // B05
    serial_enable_echo         = 0x0040, // B06
    serial_enable_edwards_mode = 0x0080, // B07
    serial_enable_AFE          = 0x0100, // B08
    serial_enable_wireless     = 0x0200, // B09
    serial_enable_amp          = 0x0400, // B0A
    serial_enable_temperature  = 0x0800, // B0B
    serial_enable_supervision  = 0x1000, // B0C
    serial_enable_photo        = 0x2000, // B0D
    serial_enable_i2c          = 0x4000, // B0E
    serial_enable_heap         = 0x8000, // B0F
} Serial_output_flags_type_e;

//*********************************************************************
/**
 * \brief Serial flag types enum
 */
//*********************************************************************
typedef enum
{
    enable_force_analysis_mode = 0x01,
    disable_dark_compensation  = 0x02,
    disable_dark_read          = 0x04,
    disable_sounder            = 0x08,
    enable_unused_4            = 0x10,
    enable_unused_5            = 0x20,
    enable_unused_6            = 0x40,
    enable_unused_7            = 0x80,
} Serial_flags_type_e;

//*********************************************************************
/**
 * \brief Serial task assignment function
 * \param message message ID passed to serial task
 * \param data Any data sent with each serial task
 */
//*********************************************************************
void serial_task(message_id const message, uint16_t const data);

//*********************************************************************
/**
 * \brief Send serial string
 * \param str string to send
 */
//*********************************************************************
void serial_send_string(char const *str);

//*********************************************************************
/**
 * \brief Send serial string for debugging
 * \param pString Pointer to string to send [in]
 * \param flag The serial output flag [out]
 */
//*********************************************************************
void serial_send_string_debug(char *pString, Serial_output_flags_type_e flag);

//*********************************************************************
/**
 * \brief Convert unit8 to hex value
 * \param dest Destination pointer
 * \param data Uint8_t data to convert to hex
 */
//*********************************************************************
void uint8_to_hex(char *dest, uint8_t const data);

//*********************************************************************
/**
 * \brief Convert unit16 to hex value
 * \param dest Destination pointer
 * \param data Uint16_t data to convert to hex
 */
//*********************************************************************
void uint16_to_hex(char *dest, uint16_t const data);

//*********************************************************************
/**
 * \brief Convert unit32 to hex value
 * \param dest Destination pointer
 * \param data Uint32_t data to convert to hex
 */
//*********************************************************************
void uint32_to_hex(char *dest, uint32_t const data);

//*********************************************************************
/**
 * \brief Support routine for formatting serial output
 * \param pPrefix Pointer to prefix, NOTHING if NULL
 * \param val Uint16 value to convert
 * \param base Base to convert to (dec, hex, etc)
 * \param suffix Suffix if value non-zero
 */
//********************************************************************
void serial_send_int_ex(char *pPrefix, uint16_t val, uint8_t base, char *suffix);

//*********************************************************************
/**
 * \brief Support routine for formatting serial output with debug flag
 * \param pPrefix Pointer to prefix, NOTHING if NULL
 * \param val Uint16 value to convert
 * \param base Base to convert to (dec, hex, etc)
 * \param suffix Suffix if value non-zero
 * \param flag serial output debug flag type
 */
//********************************************************************
void serial_send_int_ex_debug(char *pPrefix, uint16_t val, uint8_t base, char *suffix, Serial_output_flags_type_e flag);

//*********************************************************************
/**
 * \brief Support routine for converting ints to ascii
 * \param value uint16 value to convert
 * \param pResult pointer to destination
 * \param base Base to convert to (dec, hex, etc)
 */
//********************************************************************
void serial_uart_itoa(uint16_t value, char *pResult, uint8_t base);

//*********************************************************************
/**
 * \brief upport routine for ASCII to uint8
 * \param pString pointer to source string
 * \returns uint8_t value
 */
//********************************************************************
uint8_t serial_uart_atou8(uint8_t const *pString);

//*********************************************************************
/**
 * \brief Output device model to user
 */
//*********************************************************************
void print_device_model(void);

//*********************************************************************
/**
 * \brief Send serial character return to user
 */
//*********************************************************************
void serial_send_cr_prompt(void);

//*********************************************************************
/**
 * \brief Send AFE character return to user
 */
//*********************************************************************
void send_AFE_CR_prompt(void);

//*********************************************************************
/**
 * \brief Send AFE prompt to user
 */
//*********************************************************************
void send_AFE_prompt(void);

#endif /* INC_APP_APP_SERIAL_H_ */
