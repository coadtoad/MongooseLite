//******************************************************************
/*! \file Debug_ids_priority.h
 *
 * \brief Kidde: Mongoose-fw - Declare debug ids priority functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_DEBUG_IDS_H_
#define INC_APP_DEBUG_IDS_H_

#include "app_serial.h"
#include "state.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief Send debug priority message
 * \param priority The priority value
 * \param output_flag The output flag value
 */
//*********************************************************************
void serial_send_msg_priority_debug(priority_name const priority, Serial_output_flags_type_e output_flag);

//*********************************************************************
/**
 * \brief Send serial flag message
 * \param new_flag new flag vlaue
 * \param output_flag The output flag value
 */
//*********************************************************************
void serial_send_msg_serial_flags_debug(uint8_t const new_flag, Serial_output_flags_type_e output_flag);

#endif
