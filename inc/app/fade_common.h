//******************************************************************
/*! \file fade_common.h
 *
 * \brief Kidde: Mongoose-fw - Declare the LED Fade functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_FADE_COMMON_H_
#define INC_APP_FADE_COMMON_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief LED fade variable
 */
//*********************************************************************
struct fade_data_t;

//*********************************************************************
/**
 * \brief LED fade data
 */
//*********************************************************************
typedef struct fade_data_t fade_data;

//*********************************************************************
/**
 * \brief LED fade data
 * \param fast The port to set pulldown
 * \param begin begin fade value
 * \param end end fade value
 * \param pwm pulse width modulation value
 */
//*********************************************************************
fade_data *fade_begin(bool const fast, void (*begin)(void), void (*end)(void), void (*pwm)(uint8_t const duty));

//*********************************************************************
/**
 * \brief End LED fade function
 * \param fade Pointer for fade data
 */
//*********************************************************************
void fade_end(fade_data *fade);

//*********************************************************************
/**
 * \brief Step LED fade function
 * \param fade Pointer for fade data
 */
//*********************************************************************
uint16_t fade_step(fade_data *fade);

#endif
