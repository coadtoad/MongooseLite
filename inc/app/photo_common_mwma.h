//******************************************************************
/*! \file photo_common_mwma.h
 *
 * \brief Kidde: Mongoose-fw - Declare photo chamber common functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_PHOTO_COMMON_H_
#define INC_APP_PHOTO_COMMON_H_

#include <stdint.h>
//*********************************************************************
/**
 * \brief OP AMP time for stability
 */
//*********************************************************************
#define PHOTO_OP_AMP_STABLE_TIME 0x80

//*********************************************************************
/**
 * \brief Time to take 5 samples of clean ait
 */
//*********************************************************************
#define PHOTO_CLEANAIR_READTIME  0x08 // 5 samples (was 30 samples)

//*********************************************************************
/**
 * \brief Light signal between 4 options of light sensors
 */
//*********************************************************************
typedef enum
{
    IR_F,
    IR_B,
    BL_F,
    BL_B,
} light_signal;

//*********************************************************************
/**
 * \brief This function lets the photo module make a dark measurement
 *
 * \note Make sure right signal (IR/BL, FWD/BCK) is selected before calling
 *
 * \par First the DAC IR LED is disabled, then the 8 bit ADC is read for ADC_HI_SMOKE
 *          If this ambient value read is less than the database value OFFSET_DAC_CNT return it and a comp value of 0
 *          Otherwise the compensation value = ambient - OFFSET_DAC_CNT from the database
 *          The DAC is then enabled with this compensation value on the DAC_CAV_OFFST channel
 *          After a delay, a new dark read is made and returned
 *
 * PRE-CONDITION: the photo sensor and led are selected  <br>
 *
 * POST-CONDITION: comp points to the compensation value for all to use
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
uint8_t read_dark(light_signal light_reading, uint8_t *const comp);

//*********************************************************************
/**
 * \brief This function lets the photo module make a light measurement
 *
 * Function : read_light()
 *
 * \note Make sure right signal (IR/BL, FWD/BCK) is selected before calling
 * \note The DAC is enabled for the IR and the CAV then a measurement is taken after some microseconds
 *
 * PRE-CONDITION: the photo sensor and led are selected  <br>
 * PRE-CONDITION: the dac_count is taken from the database
 * PRE-CONDITION: the dark_f is set to an appropriate compensation value
 *
 * POST-CONDITION:
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
uint8_t read_light(light_signal light_reading, uint8_t const dark_f, uint8_t const dac_count);

//*********************************************************************
/**
 * \brief Read low gain value for a sample
 */
//*********************************************************************
uint8_t read_light_low_gain(void);

_Bool validate_light_ir_f(uint8_t const light_ir_f);

_Bool validate_light_ir_b(uint8_t const light_ir_b);

_Bool validate_dark_f(uint8_t const dark_f);

_Bool validate_dark_b(uint8_t const dark_b);

void update_ir_photo_fault_kvs(void);

#endif /* INC_APP_PHOTO_COMMON_H_ */
