//******************************************************************
/*! \file state.h
 *
 * \brief Kidde: Mongoose-fw - Define the state functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_STATE_H_
#define INC_APP_STATE_H_

#include "message_id.h"
#include "task_id.h"
#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Priority name and order of high priority expressions enum
 */
//*********************************************************************
// needs to match "expression_id" in state.c
typedef enum
{
    // high priority expressions
    prior_fault_fatal, // special case, if device enters fatal fault, obliterate() is called to stop all normal
                       // operations.
    prior_power_on,
    prior_smoke,
    prior_remote_smoke,
    prior_co,
    prior_co_conserve,
    prior_co_remote,
    prior_ptt_init,
    prior_ptt,
    prior_smoke_hush,
    prior_eol_fatal,
    prior_battery_low_no_hush,
    prior_fault_photo,
    prior_fault_co,
    prior_eol,
    prior_button_stuck,
    // low priority expressions
    prior_battery_none,
    prior_battery_low,
    prior_alarm_memory_smoke,
    prior_alarm_memory_co,
    prior_alarm_co_peak,
    prior_battery_low_hush,
    prior_eol_hush,
    // calibration states
    prior_cal_pass,
    prior_cal_pass_co_circuit,
    prior_cal_pass_400_ppm,
    prior_cal_fail,
    prior_cal_co_circuit_no_button,
    prior_cal_co_circuit,
    prior_cal_battery_no_button,
    prior_cal_battery,
    prior_cal_lic,
    prior_cal_2_point,
    prior_cal_0_ppm,
    prior_cal_150_ppm,
    prior_cal_400_ppm,
    prior_cal_inline_wait,
    prior_cal_inline_stabilize,
    prior_cal_inline_average,
    prior_cal_inline_verify,
    prior_cal_inline_error,
    // power on state
    prior_dc_blink,
    // no state
    prior_name_count
} priority_name;

//*********************************************************************
/**
 * \brief State task function
 * \param message message ID passed to state task
 * \param data Any data sent with each state task
 */
//*********************************************************************
void state_task(message_id const message, uint16_t const data);

//*********************************************************************
/**
 * \brief Set progress of memory flash
 * \param status Current status of in progress flash programming
 */
//*********************************************************************
void set_flash_in_progress(bool status);

//*********************************************************************
/**
 * \brief Return memory flash progress
 */
//*********************************************************************
bool get_flash_in_progress(void);

#endif /* INC_APP_STATE_H_ */
