//******************************************************************
/*! \file task_list.h
 *
 * \brief Kidde: Mongoose-fw - Declare task list functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_TASK_LIST_H_
#define INC_APP_TASK_LIST_H_

#include "message_id.h"
#include "task_id.h"
#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Defining a given task_type with a message and any applicable data
 */
//*********************************************************************
typedef void (*task_type)(message_id const, uint16_t const);

//*********************************************************************
/**
 * \brief an external variable where task types must come from the task_list enum
 */
//*********************************************************************
extern task_type const task_list[];

//*********************************************************************
/**
 * \brief Loop through the task list initializing each task with a soft reset in between each task
 * \param soft_reset A constant boolean to initialize a soft reset
 */
//*********************************************************************
void initialize_tasks(bool const soft_reset);

#endif /* INC_APP_TASK_LIST_H_ */
