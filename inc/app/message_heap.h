//******************************************************************
/*! \file message_heap.h
 *
 * \brief Kidde: Mongoose-fw - Declare the message heap functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_MESSAGE_HEAP_H_
#define INC_APP_MESSAGE_HEAP_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief add time and data info to message heap
 * \param time time to be added to heap
 * \param task task to be called, task id
 * \param message message to be sent, message id
 * \param data any data sent with the message
 */
//*********************************************************************
void add_timed(uint32_t const time, task_id const task, message_id const message, uint16_t const data);

//*********************************************************************
/**
 * @brief Push left heap (main timer queue)
 * @param[in] time time to push info in heap
 * @param[in] task task to be called, task id
 * @param[in] message message to be sent, message id
 * @param[in] data any data sent with the message
 */
//*********************************************************************
void push_left(uint32_t const time, task_id const task, message_id const message, uint16_t const data);

//*********************************************************************
/**
 * \brief pop info from heap
 */
//*********************************************************************
/**
 * @brief pop info from heap
 */
void pop(void);

//*********************************************************************
/**
 * @brief delete task from left heap
 * @param[in] task task to be deleted, task id
 */
//*********************************************************************
void delete_left_t(task_id const task);

//*********************************************************************
/**
 * @brief delete task from current heap location
 * @param[in] task task to be deleted, task id
 */
//*********************************************************************
void delete_timed_t(task_id const task);

//*********************************************************************
/**
 * @brief delete task from left heap
 * @param[in] task task to be deleted, task id
 * @param[in] message message to be deleted, message id
 */
//*********************************************************************
void delete_left_tm(task_id const task, message_id const message);

//*********************************************************************
/**
 * @brief delete task from current heap location
 * @param[in] task task to be deleted, task id
 * @param[in] message message to be deleted, message id
 */
//*********************************************************************
void delete_timed_tm(task_id const task, message_id const message);

//*********************************************************************
/**
 * @brief delete task, that has data, from left of current heap location
 * @param[in] task task to be deleted, task id
 * @param[in] message message to be deleted, message id
 * @param[in] data data to be deleted
 */
//*********************************************************************

void delete_left_tmd(task_id const task, message_id const message, uint16_t const data);

//*********************************************************************
/**
 * @brief delete task, that has data, from current heap location
 * @param[in] task task to be deleted, task id
 * @param[in] message message to be deleted, message id
 * @param[in] data data to be deleted
 */
//*********************************************************************
void delete_timed_tmd(task_id const task, message_id const message, uint16_t const data);

//*********************************************************************
/**
 * @brief Wait until the next message is ready to dispatch OR one hour
 *
 * -# determine how long until next message
 * -# schedule 1x timed message if ready to run one
 * -# determine sleep period
 * -# if no time to sleep,
 *     - then done
 * -# if sleep for <3ms,
 *     - then spin until RTC hits wakeup time
 * -# else if should sleep,
 *     - then sleep
 * -# else
 *     - then standby
 */
//*********************************************************************
void wait_until_next_message(void);

//*********************************************************************
/**
 * @brief boolean if a message is available
 * @returns true or false
 */
//*********************************************************************
bool messages_available(void);

/**
 * @brief Reset the reference time
 *
 * This needs to be called whenever the RTC is reset
 */
void reset_reference_time(void);

#endif /* INC_APP_MESSAGE_HEAP_H_ */
