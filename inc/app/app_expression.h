/**
 * @file app_expression.h
 * @brief The expression execution task
 */

#ifndef INC_APP_APP_EXPRESSION_H_
#define INC_APP_APP_EXPRESSION_H_

#include "message.h"
#include <stdint.h>

/**
 * @brief An ID passed to expression_task() as data
 *
 * This is passed as data for @link message_id::msg_begin_express @endlink
 *
 * @note This must be synced with expression in app_expression.c
 */
typedef enum
{
    exp_default,
    exp_master_smoke,
    exp_remote_smoke,
    exp_smoke_hush,
    exp_fault_photo_first_hour,
    exp_fault_photo_with_voice,
    exp_fault_photo_without_voice,
    exp_alarm_memory_smoke,
    exp_master_co,
    exp_remote_co,
    exp_co_conserve,
    exp_fault_co_first_hour,
    exp_fault_co_with_voice,
    exp_fault_co_without_voice,
    exp_alarm_memory_co,
    exp_eol_first_hour,
    exp_eol_with_voice,
    exp_eol_without_voice,
    exp_eol_hush,
    exp_battery_none_first_hour,
    exp_battery_none_with_voice,
    exp_battery_none_without_voice,
    exp_battery_low_first_hour,
    exp_battery_low_with_voice,
    exp_battery_low_without_voice,
    exp_battery_low_hush,
    exp_cal_pass,
    exp_cal_pass_with_chirp,
    exp_cal_fail,
    exp_cal_fail_with_chirp,
    exp_cal_fail_without_chirp,
    exp_cal_battery,
    exp_cal_battery_no_button,
    exp_cal_pass_co_circuit,
    exp_cal_co_circuit,
    exp_cal_co_circuit_no_button,
    exp_cal_0_ppm,
    exp_cal_150_ppm,
    exp_cal_pass_400_ppm,
    exp_cal_400_ppm,
    exp_cal_photo_fast,
    exp_cal_photo_slow,
    exp_cal_inline_smoke_wait,
    exp_cal_inline_smoke_stabilize,
    exp_cal_inline_smoke_average,
    exp_cal_inline_smoke_verify,
    exp_ptt_initialize,
    exp_fault_fatal_first_hour,
    exp_fault_fatal_with_voice,
    exp_fault_fatal_without_voice,
    exp_button_stuck,
    exp_power_on,
    exp_count /**< The number of expressions */
} expression_id;

/**
 * @brief Forces the current expression to end
 *
 * This overrides the uninterruptability of the current expression and current
 * expression line.
 *
 * @note This is only intended to be called from obliterate().
 */
void force_expression_end(void);

/**
 * @brief The task for executing expressions
 *
 * @param message The message to process
 * @param data The data associated with @p message
 */
void expression_task(message_id const message, uint16_t const data);

#endif
