//******************************************************************
/*! \file ptt_co.h
 *
 * \brief Kidde: Mongoose-fw - Declare PTT CO functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_PTT_CO_H_
#define INC_APP_PTT_CO_H_

#include "message_id.h"

//*********************************************************************
/**
 * \brief PTT CO function
 * \param message message ID passed to PTT CO task
 * \param data Any data sent with each PTT CO task
 */
//*********************************************************************
void ptt_co_task(message_id const message, uint16_t const data);

#endif /* INC_APP_PTT_CO_H_ */
