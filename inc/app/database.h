//******************************************************************
/*! \file database.h
 *
 * \brief Kidde: Mongoose-fw - Define database information
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_DATABASE_H_
#define INC_APP_DATABASE_H_

#define CONFIG_CANARY       0xa5

// Configuration bits for Market
#define MARKET_US           0x01
#define MARKET_CANADA       0x02
#define MARKET_UK           0x03
#define MARKET_CHINA        0x04

// Configuration bits for Calibration
#define CAL_CO_CIRCUIT      0x0000
#define NO_CAL_CO_CIRCUIT   0x0001
#define CAL_BATTERY         0x0000
#define NO_CAL_BATTERY      0x0002
#define CAL_LIC             0x0000
#define NO_CAL_LIC          0x0004
#define CAL_2_POINT         0x0000
#define NO_CAL_2_POINT      0x0008
#define CAL_0_PPM           0x0000
#define NO_CAL_0_PPM        0x0010
#define CAL_150_PPM         0x0000
#define NO_CAL_150_PPM      0x0020
#define CAL_400_PPM         0x0000
#define NO_CAL_400_PPM      0x0040
#define CAL_INLINE_SMOKE    0x0000
#define NO_CAL_INLINE_SMOKE 0x0080

//
// Macro to calculate fixed point 8.8
// A is the integer part of 8.8 number 0-255
// B is decimal part, MUST be set to 2 decimal places
// Example: 2.45 => FIXED8_8CONST(2,45) = 0x0273
// Example: 18.6 => FIXED8_8CONST(18,60) = 0x1299
//
#define FIXED8_8CONST(A, B) (uint32_t)(((uint32_t)A * 256) + (((uint32_t)B * 256) / 100))

#define POSITIVE            (uint8_t)0
#define NEGATIVE            (uint8_t)1

#define CONFIG_CALIBRATION  0xFFFF

/** \todo IMPLEMENT ME */
/*  this will need to be placed bacck when calibration is defined,  though alot will need to be updated / changed
#define CONFIG_CALIBRATION                                                                                             \
    (0xff00 | CONFIG_CAL_CO_CIRCUIT | CONFIG_CAL_BATTERY | CONFIG_CAL_LIC | CONFIG_CAL_2_POINT | CONFIG_CAL_0_PPM |    \
     CONFIG_CAL_150_PPM | CONFIG_CAL_400_PPM | CONFIG_CAL_INLINE_SMOKE)
*/
#endif /* INC_APP_DATABASE_H_ */
