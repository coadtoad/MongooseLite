//******************************************************************
/*! \file message_id.h
 *
 * \brief Kidde: Mongoose-fw - Declare the message ID functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
/**
 * \file message_id.h
 * \brief Declare the message ID functions
 */

#ifndef INC_APP_MESSAGE_ID_H_
#define INC_APP_MESSAGE_ID_H_

#include <stdint.h>

//*********************************************************************
/**
 * \brief Complete message_id function declaration.
 */
//*********************************************************************
typedef enum
{
    /**
     * \brief Sent to a task in \link task_list \endlink when the device boots
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0    | If the reboot was a soft reboot |
     * | 1-15 | **Unused** |
     */
    msg_init,
    /**
     * \brief Sent by a task to itself to indicate further processing
     *
     * This should **never** be sent by a task except to itself.
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-15 | Use is task specific |
     */
    msg_continue,
    /**
     * \brief Sent by state_task() to an expression task to begin expressing
     *
     * An expression task will not receive this message while expressing.
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-15 | **Unused** |
     */
    msg_begin_express,
    /**
     * \brief An expression task switches between this and
     * \link message_id::msg_start_wait_express msg_start_wait_express \endlink
     *
     * This and \link message_id::msg_start_wait_express msg_start_wait_express \endlink are used as states to permit
     * stopping an expression at any off-point, rather than at a single point during the expression.
     *
     * When this message is dispatched, the receiving task schedules the sequence it expresses and then schedules
     * \link message_id::msg_start_wait_express msg_start_wait_express \endlink at the point that the sequence is
     * finished.
     *
     * Properly implemented, the expression task will not return \link message_id::msg_ready msg_ready \endlink to
     * state_task() after this is dispatched until
     * \link message_id::msg_start_wait_express msg_start_wait_express \endlink is dispatched.
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | Used to prevent messages from previous runs of the expression from affecting the current run |
     * | 8-15 | **Unused** |
     */
    msg_start_express,
    /**
     * \brief An expression task switches between this and
     * \link message_id::msg_start_express msg_start_express \endlink
     *
     * This and \link message_id::msg_start_express msg_start_express \endlink are used as states to permit stopping an
     * expression at any off-point, rather than at a single point during the expression.
     *
     * When this message is dispatched, the receiving task schedules
     * \link message_id::msg_start_express msg_start_express \endlink at the point that the next sequence to be
     * expressed.
     *
     * Properly implemented, the expression task will return \link message_id::msg_ready msg_ready \endlink to
     * state_task() after this is dispatched but before \link message_id::msg_start_express msg_start_express \endlink
     * is dispatched.
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | Used to prevent messages from previous runs of the expression from affecting the current run |
     * | 8-15 | **Unused** |
     */
    msg_start_wait_express,
    msg_start_wait_express_voice,
    msg_start_express_voice,
    /**
     * \brief Sent by task_state() to indicate that an expression should stop when it is not currently expressing
     *
     * The expression task should respond with \link message_id::msg_ready msg_ready \endlink as soon as possible.
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-15 | **Unused** |
     */
    msg_end_express,
    /**
     * @brief Sent to interrupt the current expression and chirp 1 or more times
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The number of times to chirp |
     * | 8-15 | **Unused** |
     *
     * @warning A chirp count of 0 is not supported and may chirp 256 times
     */
    msg_button_chirp,
    /**
     * \brief Sent to the expression engine when it should resume processing after a button chirp
     * to state_task()
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-15 | **Unused** |
     */
    msg_button_complete,
    /**
     * @brief Sent to an output task to turn its output on
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-15 | **Unused** |
     */
    msg_turn_on,
    /**
     * \brief Sent to an output task to turn its output off
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-15 | **Unused** |
     */
    msg_turn_off,
    msg_vboost_on,
    msg_vboost_off,
    /**
     * \brief Sent as a response to another message
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The \link task_id \endlink that sent the message |
     * | 8-15 | **Unused** |
     *
     * \todo Sent by button_irq_task() with \p data set to 0, should probably be
     * \link message_id::msg_continue msg_continue \endlink
     * \todo express_co_task() handles it, but does not check data
     * \todo express_smoke_task() handles it, but does not check data
     * \todo button_task() handles it, but does not check data, likely related to message sent by button_irq_task()
     * \todo ptt_co_task() in no-variant sends it with \link task_id::task_express_co task_express_co \endlink
     */
    msg_ready,
    /**
     * \brief Sent to voice_task() to begin playing a voice
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The byte to send to the voice chip |
     * | 8-15 | **Unused** |
     */
    msg_voice,
    /**
     * \brief Sent to request a value from a datastore
     *
     * This is sent to either key_value_store_task() or database_task() to request a value
     *
     * \link message_id::msg_value msg_value \endlink is sent as a response
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The \link database_index \endlink or \link key_name \endlink to read |
     * | 8-15 | The \link task_id \endlink requesting the value |
     */
    msg_get_value,
    /**
     * \brief Sent as a response to \link message_id::msg_get_value msg_get_value \endlink
     * or when a callback sends a value
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The \link database_index \endlink or \link key_name \endlink of the value |
     * | 8-15 | The value |
     */
    msg_value,
    /**
     * \brief Sent to set a value in a datastore
     *
     * This is sent to either key_value_store_task() or database_task() to update a value
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The \link database_index \endlink or \link key_name \endlink of the value to set |
     * | 8-15 | The value |
     */
    msg_set_value,
    /**
     * \brief Adds a callback to key_value_store_task()
     *
     * The callback is called whenever the value of the entry is set
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The \link key_name \endlink to add a callback for |
     * | 8-15 | The \link task_id \endlink to receive callbacks |
     *
     * \todo Remove unneeded (all?) callbacks from serial_task()
     */
    msg_add_callback,
    /**
     * \brief Removes a callback from key_value_store_task()
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The \link key_name \endlink to add a callback for |
     * | 8-15 | The \link task_id \endlink to receive callbacks |
     */
    msg_remove_callback,
    /**
     * \brief Set a single bit in key_value_store_task()
     *
     * It is impossible to set a single bit in a key_value_store_task() entry atomically using
     * \link message_id::msg_set_value msg_set_value \endlink and
     * \link message_id::msg_get_value msg_get_value \endlink.  This avoids the issue.
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0-7  | The \link key_name \endlink to set a bit in |
     * | 8-10 | The 0-based index of the bit to set |
     * | 11   | The value to set the bit to |
     *
     * \todo This is used in serial_command_processor() for the 'L' command, should be something else
     * \todo This is used in serial_command_processor() for the 'P' command, should be something else
     * \todo This is used in serial_command_processor() for the 'S' command, should be something else
     * \todo This is used in voice_task() where \link message_id::msg_continue msg_continue \endlink should be used
     */
    msg_set_flag,
    msg_enter_smoke_alarm,
    msg_exit_smoke_alarm,
    msg_enter_low_battery,
    msg_exit_low_battery,
    msg_enter_fatal_battery,
    msg_exit_fatal_battery,
    msg_enter_no_battery,
    msg_exit_no_battery,
    msg_enter_eol,
    msg_exit_eol,
    msg_enter_eol_fatal,
    msg_exit_eol_fatal,
    msg_battery_volt,
    msg_button_down,
    msg_button_stuck,
    msg_enter_co_alarm,
    msg_enter_co_alarm_conserve,
    msg_exit_co_alarm,
    msg_fault_co_sensor_short,
    msg_fault_co_sensor_open,
    msg_fault_co_clear,
    msg_cal_begin,
    msg_cal_pass,
    msg_cal_fail,
    msg_ptt,
    msg_record_history,
    msg_update_primary_db,
    msg_update_backup_db,
    msg_co_high,
    msg_ac_on,
    msg_ac_off,
    msg_entered_hush,
    msg_begin_drive_smoke,
    msg_end_drive_smoke,
    msg_begin_drive_co,
    msg_end_drive_co,
    msg_charge_blue_led,
    msg_fault_fatal,
    msg_expression_timeout,
    msg_smoke_hush_timeout,
    msg_battery_hush_timeout,
    msg_eol_hush_timeout,
    msg_reset_flags,
    msg_exit_sampling,
    msg_strobe_sync,
    msg_cancel_strobe_light,
    msg_interconnect_message,
    /**
     * \brief Tell LED to begin a fade
     *
     * Returns \link message_id::msg_ready msg_ready \endlink to the caller once
     * the fade has completed.  This is always a full transition from off to
     * off.
     *
     * | Bits | Use |
     * | :--: | :-- |
     * | 0    | True if fast fade |
     * | 8-15 | The \link task_id \endlink to receive callbacks |
     */
    msg_led_fade,
    msg_locate_timeout,
    msg_amp_resend,
    msg_voice_multiple_button_tones,
    msg_override,
    msg_firmware_crc,
    msg_start_push_to_test,
    msg_read_primary_entry,
    msg_read_backup_entry,
    msg_dump_flash_contents,
    msg_inline_smoke_cal,
    msg_sample,
} message_id;

#endif /* INC_APP_MESSAGE_ID_H_ */
