//******************************************************************
/*! \file ptt_photo.h
 *
 * \brief Kidde: Mongoose-fw - Define the PTT photo functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_PTT_H_
#define INC_APP_PTT_H_

#include "message_id.h"

//*********************************************************************
/**
 * \brief CO fault condition value enum
 */
//*********************************************************************
typedef enum
{
    ptt_photo_init,
    ptt_photo_complete,
    ptt_photo_pass,
    ptt_photo_fail,
    ptt_complete
} ptt_message;

//*********************************************************************
/**
 * \brief PTT photo task assignment function
 * \param message message ID passed to PTT photo task
 * \param data Any data sent with each PTT photo task
 */
//*********************************************************************
void ptt_task(message_id const message, uint16_t const data);

#endif /* INC_APP_PTT_H_ */
