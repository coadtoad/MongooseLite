//******************************************************************
/*! \file in_photo_single_decision.h
 *
 * \brief Kidde: Mongoose-fw - Declare functions for single channel photo algorithm
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_INPUT_IN_PHOTO_SINGLE_DECISION_H_
#define INC_APP_INPUT_IN_PHOTO_SINGLE_DECISION_H_

#include "message_id.h"
#include "photo_common_single.h"
#include <stdint.h>

uint8_t determine_fire_modes_scp(photo_mode_e const photo_mode, uint8_t const photo_fault_flags,
                                 uint8_t const delta_val);

uint16_t get_smold_counter_value(void);
uint16_t get_rate_of_rise_value(void);
uint16_t get_acceleration_value(void);
uint8_t get_buf_index(void);
bool is_bit_n_set(uint8_t c, int n);

#endif
