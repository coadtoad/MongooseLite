//******************************************************************
/*! \file in_ac.h
 *
 * \brief Kidde: Mongoose-fw - Declare ac input functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_IN_AC_H_
#define INC_APP_IN_AC_H_

#include "message.h"

//*********************************************************************
/**
 * \brief Set AC input enum
 */
//*********************************************************************
typedef enum
{
    AC_CHANGED = 0x1000
} Ac_input_task_e;

//*********************************************************************
/**
 * \brief AC input task function
 * \param message message ID passed to AC input task
 * \param data Any data sent with the AC input task
 */
//*********************************************************************
void ac_input_task(message_id const message, uint16_t const data);

#endif /* INC_APP_IN_AC_H_ */
