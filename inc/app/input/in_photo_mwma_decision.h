//******************************************************************
/*! \file in_photo_mwma_decision.h
 *
 * \brief Kidde: Mongoose-fw - Declare the photo MWMA decision input functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_IN_PHOTO_MWMA_DECISION_H_
#define INC_APP_IN_PHOTO_MWMA_DECISION_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief This function returns true if there is a multiwave fire, false otherwise
 * \param photo_fault_flags Check photo fault flags to see if device is in a photo fault state
 * \param ir_f_delta IR forward delta value
 * \param ir_b_delta IR back delta value
 * \param bl_f_delta BL forward delta value
 * \param ir_f_ir_b_ratio Ratio from IR F to IR B to check if fire count will increment
 * \param bl_f_ir_b_ratio Ratio from BL F to IR B to check if fire count will increment
 * \param fire_flags Check fire flags for type of fire state
 *
 * \note first all of the deltas are compared to database values (normally all set to 5)
 *          -# The ir_f_ir_b_threshold is pulled from the database.
 *          -# Otherwise if the fail safe fire count is high enough and the 5 MW_FLAGs are true then it will be set to a
 * fail safe fire threshold.
 *          -# Else it will be set to the default threshold.
 *
 *  \note If the bl_f_ir_b or ir_f_ir_b ratios exceed these thresholds then the fire count will be incremented.
 *          -# If neither exceeds their threshold then the fire count will be set to 0
 *          -# If the fire count exceeds a threshold from the database then there is a MW fire.
 *
 * \note PRE-CONDITION: the deltas and ratios have all been calculated
 *
 * \note POST-CONDITION: The returned value is true if there should be a mw fire
 *
 * \see reset_program.asm
 */
//*********************************************************************

bool determine_fire_modes(uint8_t photo_fault_flags, uint8_t ir_f_delta, uint8_t ir_b_delta, uint8_t bl_f_delta,
                          uint16_t ir_f_ir_b_ratio, uint16_t bl_f_ir_b_ratio, uint8_t *fire_flags);

//*********************************************************************
/**
 * \brief Boolean to see if device is in a fire state
 * \param fire_flags Check fire flags for type of fire state
 * \param photo_fault_flags Check photo fault flags to see if device is in a photo fault state
 */
//*********************************************************************
bool is_in_fire(uint8_t fire_flags, uint8_t photo_fault_flags);

//*********************************************************************
/**
 * \brief Threshold to enter an alarm state based on IR F delta
 */
//*********************************************************************
bool alarm_threshold(uint8_t ir_f_delta);

//*********************************************************************
/**
 * \brief Check if the specific bit in a byte is set
 *
 * \param c The byte to check
 * \param n The position of the bit to check
 *
 * \returns Return true if the specific bit is set, false otherwise.
 */
//*********************************************************************
bool is_bit_n_set(uint8_t c, int n);

//*********************************************************************
/**
 * \brief Sets decision counters zero value
 */
//*********************************************************************
void reset_photo_decision_counters(void);

#endif /* INC_APP_IN_PHOTO_MWMA_DECISION_H_ */
