//******************************************************************
/*! \file in_co.h
 *
 * \brief Kidde: Mongoose-fw - Declare co input functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_IN_CO_H_
#define INC_APP_IN_CO_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief CO input task function
 * \param message message ID passed to CO input task
 * \param data Any data sent with the CO input task
 */
//*********************************************************************
void co_task(message_id const message, uint16_t const data);

#endif /* INC_APP_IN_CO_H_ */
