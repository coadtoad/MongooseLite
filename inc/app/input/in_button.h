//******************************************************************
/*! \file in_button.h
 *
 * \brief Kidde: Mongoose-fw - include file for CO measurement module
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_IN_BUTTON_H_
#define INC_APP_IN_BUTTON_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief button task function
 * \param message message ID passed to button task
 * \param data Any data sent with the button task
 */
//*********************************************************************
void button_task(message_id const message, uint16_t const data);

#endif /* INC_APP_IN_BUTTON_H_ */
