//******************************************************************
/*! \file in_day_count.h
 *
 * \brief Kidde: Mongoose-fw - Declare day count input functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_IN_DAY_COUNT_H_
#define INC_APP_IN_DAY_COUNT_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief End of life day count threshold
 */
//*********************************************************************
#define EOL_TH (365 * 10 + 100)

//*********************************************************************
/**
 * \brief Accelerate timer options list enum
 */
//*********************************************************************
typedef enum
{
    accel_day_timer,
    accel_hour_timer,
    accel_minute_timer,
    restart_timer
} accel_life_type_e;

//*********************************************************************
/**
 * \brief Day count task function
 * \param message message ID passed to Day count task
 * \param data Any data sent with the Day count task
 */
//*********************************************************************
void day_count_task(message_id const message, uint16_t const data);

#endif /* INC_APP_IN_DAY_COUNT_H_ */
