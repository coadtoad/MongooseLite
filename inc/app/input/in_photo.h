//******************************************************************
/*! \file in_photo.h
 *
 * \brief Kidde: Mongoose-fw - Declare the photo input functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_IN_PHOTO_H_
#define INC_APP_IN_PHOTO_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief Normal mode minimum delay value
 */
//*********************************************************************
#define NORMAL_MODE_DELAY_MIN 5

//*********************************************************************
/**
 * \brief Normal mode maximum delay value
 */
//*********************************************************************
#define NORMAL_MODE_DELAY_MAX 15

//*********************************************************************
/**
 * \brief Enum list of photo fault bit order
 */
//*********************************************************************
typedef enum
{
    fwd_ambient_fault = 0,
    bck_ambient_fault,
    fwd_IR_fault,
    bck_IR_fault,
    bck_blue_fault,
    afe_fault,
} photo_fault_bits;

//*********************************************************************
/**
 * \brief Flag values for various fires and thresholds enum
 */
//*********************************************************************
typedef enum
{
    alarm_threshold_fire_bit = 0,
    mw_fire_bit,
    flaming_fire_bit,
    nuisance_fire_bit,
    fail_safe_fire_bit,
    hushable_fire_bit,
    smoldering_fire_bit,
    below_min_delta_bit
} fire_flag_bits;

//*********************************************************************
/**
 * \brief Photo task function
 * \param message message ID passed to Photo task
 * \param data Any data sent with the Photo task
 */
//*********************************************************************
void photo_task(message_id const message, uint16_t const data);

#endif /* INC_APP_IN_PHOTO_H_ */
