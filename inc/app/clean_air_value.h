//******************************************************************
/*! \file clean_air_value.h
 *
 * \brief Kidde: Mongoose-fw - Declare the clean air value functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_CLEAN_AIR_VALUE_H_
#define INC_CLEAN_AIR_VALUE_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief Clean air value reason enum
 * \ingroup
 */
//*********************************************************************
typedef enum
{
    cav_reason_normal,               ///< normal timed cav
    cav_reason_serial_reset,         ///< cav due to serial reset
    cav_reason_serial_reset_factory, ///< cav due to serial reset + factory
} cav_reason_t;

//*********************************************************************
/**
 * \brief CAV task assignment function
 * \param message message ID passed to CAV task
 * \param data Any data sent with each CAV task
 * \ingroup
 */
//*********************************************************************
void clean_air_value_task(message_id const message, uint16_t const data);

#endif /* INC_CLEAN_AIR_VALUE_H_ */
