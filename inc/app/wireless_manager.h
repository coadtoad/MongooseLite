//******************************************************************
/*! \file wireless_manager.h
 *
 * \brief Kidde: Mongoose-fw - Implement wireless_manager functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_WIRELESS_MANAGER_H_
#define INC_APP_WIRELESS_MANAGER_H_

#include "message.h"
#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Wireless manager task  function
 * \param message message ID passed to wireless manager task
 * \param data Any data sent with each wireless manager task
 */
//*********************************************************************
void wireless_manager_task(message_id const message, uint16_t const data);

#endif