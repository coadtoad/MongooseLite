//******************************************************************
/*! \file key_value_store.h
 *
 * \brief Kidde: Mongoose-fw - Declare key value enums
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_KEY_VALUE_STORE_H_
#define INC_APP_KEY_VALUE_STORE_H_

#include "message_id.h"
#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Key value name enum
 * \note Each value is a bit index, so values 0-7 are available
 */
//*********************************************************************
typedef enum
{
    key_light_ir_f,
    key_light_ir_b,
    key_light_bl_f,
    key_ir_f_filter,
    key_ir_b_filter,
    key_bl_f_filter,
    key_dark_f,
    key_dark_b,
    key_ir_f_ir_b_ratio_lsb,
    key_ir_f_ir_b_ratio_msb,
    key_bl_f_ir_b_ratio_lsb,
    key_bl_f_ir_b_ratio_msb,
    key_bl_f_ir_f_ratio_lsb,
    key_bl_f_ir_f_ratio_msb,
    key_comp_f,
    key_comp_b,
    key_ir_f_delta,
    key_ir_b_delta,
    key_bl_f_delta,
    key_filter_idx_lsb,
    key_filter_idx_msb,
    key_fire_flags,
    key_fault_flags,
    key_smoke_status,
    key_co_msb,
    key_co_lsb,
    key_accumulator_msb,
    key_accumulator_lsb,
    key_co_volt_msb,
    key_co_volt_lsb,
    key_co_status,
    key_status_lb,
    key_status_hb,
    key_battery_volt,
    key_battery_thresh,
    key_max_message_queue_size,
    key_serial_buffer_ptr,
    key_is_day_time,
    key_avoidance_flags,
    key_ir_photo_faults,
    key_blue_led_fault,
    key_clean_air_fault,
    key_AFE_fault,
    key_day_count_msb,
    key_day_count_lsb,
    key_wm_battery_status,
    key_wm_timeslot,
    key_wm_fault,
    key_temperature_f,
    key_smoke_alarm_percent,
    key_smoke_comp,
    key_timeslot,
    key_power_led_dim,
    key_name_count // end of key_name
} key_name;

//*********************************************************************
/**
 * \brief key value store sounder avoidance flag
 * \note Each value is a bit index, so values 0-7 are available
 * \note This is bit indices into key_avoidance_flags
 */
//*********************************************************************
typedef enum
{
    kvs_avoid_sounder,
    kvs_avoid_smoke_alarm
} kvs_avoidance_flag;

//*********************************************************************
/**
 * \brief key value ir led faults
 */
//*********************************************************************
typedef enum
{
    kvs_fwd_ambient_fault,
    kvs_bck_ambient_fault,
    kvs_fwd_ir_led_fault,
    kvs_bck_ir_led_fault
} kvs_ir_led_faults_flag;

//*********************************************************************
/**
 * \brief Used to pack data into schedule message msg_set_flag
 * \param key_id The specified Key Value Store index to write to
 * \param flag The bit of the KVS index to be set or cleared
 * \param value true or false of desired flag state.
 * \returns uint16_t in the correct format from input parameters.
 */
//*********************************************************************
uint16_t pack_kvs_set_flag_data(key_name const key_id, uint8_t const flag, bool value);

//*********************************************************************
/**
 * \brief Used to retrieve the value of a specified Key Value Store index
 * \param key_id The specified Key Value Store index to write to
 * \param bit The bit of the KVS index to be set or cleared
 * \param set True if the bit is being set or false if the bit is being cleared
 * \returns The value of the specified Key Value Store index
 */
//*********************************************************************
void set_key_value_store_flag(key_name const key_id, uint8_t const bit, bool const set);

//*********************************************************************
/**
 * \brief Used to retrieve the value of a specified Key Value Store index
 * \param key_id The specified Key Value Store index to read from
 * \param bit The bit of the KVS to return the value of
 * \returns True if the bit of the flag is set or false if the bit is cleared
 */
//*********************************************************************
bool get_key_value_store_flag(key_name const key_id, uint8_t const bit);

//*********************************************************************
/**
 * \brief Key value store task function
 * \param message message ID passed to kvs task
 * \param data Any data sent with each kvs task
 */
//*********************************************************************
void key_value_store_task(message_id const message, uint16_t const data);

#endif /* INC_APP_KEY_VALUE_STORE_H_ */
