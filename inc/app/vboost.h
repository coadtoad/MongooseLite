//******************************************************************
/*! \file vboost.h
 *
 * \brief Kidde: Mongoose-fw - Declare vboost functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_VBOOST_H_
#define INC_APP_VBOOST_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief Vboost types, between horn or interconnect
 */
//*********************************************************************
typedef enum
{
    vboost_horn,
    vboost_interconnect
} vboost_type;

//*********************************************************************
/**
 * \brief Vboost task function
 * \param message message ID passed to vboost task
 * \param data Any data sent with each vboost task
 */
//*********************************************************************
void vboost_task(message_id const message, uint16_t const data);

#endif /* INC_APP_VBOOST_H_ */
