//******************************************************************
/*! \file task_id.h
 *
 * \brief Kidde: Mongoose-fw - Declare the task ID functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_TASK_ID_H_
#define INC_APP_TASK_ID_H_

#include <stdint.h>

//*********************************************************************
/**
 * \brief Enum of all task IDs in specific order to match task_list.c
 * \note IMPORTANT must match task_list.c, task_id.h, debug_ids_heap.c
 */
//*********************************************************************
typedef enum
{
    task_supervisor,
    task_database,
    task_key_value_store,
    task_voice_output,
    task_state,
    task_photo,
    task_co,
    task_battery,
    task_day_count,
    task_button,
    task_events,
    task_serial,
    task_interconnect,
    task_cal_0_ppm,
    task_cal_150_ppm,
    task_cal_400_ppm,
    task_cal_circuit,
    task_cal_2_point,
    task_cal_lic,
    task_ac_input,
    task_ptt,
    task_battery_cal,
    task_blue_charge_pump,
    task_clean_air_value,
    task_cal_doe,
    task_vboost,
    task_cal_inline_smoke,
    task_expression
} task_id;

#endif /* INC_APP_TASK_ID_H_ */
