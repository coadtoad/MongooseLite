//******************************************************************
/*! \file io_interconnect.h
 *
 * \brief Kidde: Mongoose-fw - Declare the IO interconnect functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_IO_INTERCONNECT_H_
#define INC_APP_IO_INTERCONNECT_H_

#include "message.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief Interconnect message, begin ptt
 */
//*********************************************************************
typedef enum
{
    interconnect_begin_ptt = 0
} interconnect_message;

//*********************************************************************
/**
 * \brief Passed as data when entering or exiting remote smoke/CO.
 */
//*********************************************************************
#define INT_REMOTE 0x10

//*********************************************************************
/**
 * \brief The task for handling the interconnect
 * \param message The message to process
 * \param data The data associated with @p message
 */
//*********************************************************************
void interconnect_task(message_id const message, uint16_t const data);

#endif /* INC_APP_IO_INTERCONNECT_H_ */
