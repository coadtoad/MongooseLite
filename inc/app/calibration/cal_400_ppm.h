//******************************************************************
/*! \file cal_400_ppm.h
 *
 * \brief Kidde: Mongoose-fw - Declare co cal 400 ppm functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_400_PPM_H_
#define INC_APP_CAL_400_PPM_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_400_ppm task function
 * \param message message ID passed to cal_400_ppm task
 * \param data Any data sent with the cal_400_ppm task
 */
//*********************************************************************
void cal_400_ppm_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_400_PPM_H_ */
