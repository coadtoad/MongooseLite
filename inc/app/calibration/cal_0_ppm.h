//******************************************************************
/*! \file cal_0_ppm.h
 *
 * \brief Kidde: Mongoose-fw - Declare co cal 0 ppm functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_0_PPM_H_
#define INC_APP_CAL_0_PPM_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_0_ppm task function
 * \param message message ID passed to cal_0_ppm task
 * \param data Any data sent with the cal_0_ppm task
 */
//*********************************************************************
void cal_0_ppm_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_0_PPM_H_ */
