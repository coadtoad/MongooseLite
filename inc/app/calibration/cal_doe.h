//******************************************************************
/*! \file cal_doe.h
 *
 * \brief Kidde: Mongoose-fw - Declare doe calibration functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_DOE_H_
#define INC_APP_CAL_DOE_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_doe task function
 * \param message message ID passed to cal_doe task
 * \param data Any data sent with the cal_doe task
 */
//*********************************************************************
void cal_doe_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_DOE_H_ */
