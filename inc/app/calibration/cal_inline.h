//******************************************************************
/*! \file cal_inline.h
 *
 * \brief Kidde: Mongoose-fw - Declare inline calibration functions
 *
 * \author Stan Burnette
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_INLINE_H_
#define INC_APP_CAL_INLINE_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_inline_smoke task function
 * \param message message ID passed to cal_inline_smoke task
 * \param data Any data sent with the cal_inline_smoke task
 */
//*********************************************************************
void cal_inline_smoke_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_INLINE_H_ */
