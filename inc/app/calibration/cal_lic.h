//******************************************************************
/*! \file cal_lic.h
 *
 * \brief Kidde: Mongoose-fw - Declare lic calibration functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_LIC_H_
#define INC_APP_CAL_LIC_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_lic task function
 * \param message message ID passed to cal_lic task
 * \param data Any data sent with the cal_lic task
 */
//*********************************************************************
void cal_lic_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_LIC_H_ */
