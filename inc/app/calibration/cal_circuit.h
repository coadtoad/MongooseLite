//******************************************************************
/*! \file cal_circuit.h
 *
 * \brief Kidde: Mongoose-fw - Declare circuit calibration functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_CIRCUIT_H_
#define INC_APP_CAL_CIRCUIT_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_circuit task function
 * \param message message ID passed to cal_circuit task
 * \param data Any data sent with the cal_circuit task
 */
//*********************************************************************
void cal_circuit_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_CIRCUIT_H_ */
