//******************************************************************
/*! \file cal_battery.h
 *
 * \brief Kidde: Mongoose-fw - Declare battery calibration functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_BATTERY_H_
#define INC_APP_CAL_BATTERY_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_battery task function
 * \param message message ID passed to cal_battery task
 * \param data Any data sent with the cal_battery task
 */
//*********************************************************************
void cal_battery_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_BATTERY_H_ */
