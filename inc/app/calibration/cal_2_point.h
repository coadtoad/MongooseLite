//******************************************************************
/*! \file cal_2_point.h
 *
 * \brief Kidde: Mongoose-fw - Declare 2 point cal functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CAL_2_POINT_H_
#define INC_APP_CAL_2_POINT_H_

#include "message_id.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief cal_2_point task function
 * \param message message ID passed to cal_2_point task
 * \param data Any data sent with the cal_2_point task
 */
//*********************************************************************
void cal_2_point_task(message_id const message, uint16_t const data);

#endif /* INC_APP_CAL_2_POINT_H_ */
