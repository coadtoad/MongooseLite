//******************************************************************
/*! \file co_common.h
 *
 * \brief Kidde: Mongoose-fw - Declare the common CO functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_CO_COMMON_H_
#define INC_APP_CO_COMMON_H_

#include "message_id.h"
#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Bit mask of all CO fault conditions possible for the external fault_status variable
 */
//*********************************************************************
#define ANY_FAULT 0x0F

//*********************************************************************
/**
 * \brief CO fault status enumerations
 */
//*********************************************************************
typedef enum
{
    no_fault        = 0x00,
    fault_pending   = 0x01,
    fault_open      = 0x02,
    fault_short     = 0x04,
    fault_other     = 0x08,
    status_co_alarm = 0x10,
    health_test     = 0x20,
    stabilize       = 0x80
} fault_status_t;

//*********************************************************************
/**
 * \brief External fault status variable
 */
//*********************************************************************
extern fault_status_t fault_status;

//*********************************************************************
/**
 * \brief Make a CO measurement
 * \param counts A pointer to a 16-bit variable to store the measurement data
 * \return True if the configuration for taking measurements successfully completed
 */
//*********************************************************************
bool co_make_measurement(uint16_t *const counts);

//*********************************************************************
/**
 * \brief Detect CO sensor short
 * \param co_raw_counts The unmodified CO counts
 */
//*********************************************************************
void co_short_detect(uint16_t co_raw_counts);

//*********************************************************************
/**
 * \brief CO stable measurement
 * \param co_raw_counts The unmodified CO counts
 */
//*********************************************************************
void co_meas_stable(uint16_t co_raw_counts);

//*********************************************************************
/**
 * \brief Calculate co PPM
 * \param raw_counts unmodified CO ppm counts
 * \returns The CO PPM calculation
 */
//*********************************************************************
uint16_t co_calc_PPM(uint16_t raw_counts);

//*********************************************************************
/**
 * \brief CO alarm boolean
 */
//*********************************************************************
bool co_alarm(void);

//*********************************************************************
/**
 * \brief Override the CO PPM calculation to a manually set value
 * \param co_value The PPM value to use when determining CO alarm
 */
//*********************************************************************
void set_manual_co(uint16_t co_value);

//*********************************************************************
/**
 * \brief Detect if the CO measurement surpasses the peak CO memory threshold
 * \param ppm CO PPM value
 */
//*********************************************************************
void co_peak_detect(uint16_t const ppm);

//*********************************************************************
/**
 * \brief Initialize CO accumulator
 */
//*********************************************************************
void accumulator_init(void);

//*********************************************************************
/**
 * \brief Lower the CO accumulator when CO alarm has been reset
 */
//*********************************************************************
void lower_co_accumulator(void);

#endif /* INC_APP_CO_COMMON_H_ */
