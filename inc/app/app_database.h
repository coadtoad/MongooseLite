//******************************************************************
/*! \file app_database.h
 *
 * \brief Kidde: Mongoose-fw - Define the database functions
 *
 * \note items added to database require changes in the following
 *       - app_database.h
 *       - database.h
 *       - database_struct.h
 *       - database.cpp Ccitt_16<
 *
 * \author unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_APP_APP_DATABASE_H_
#define INC_APP_APP_DATABASE_H_

#include "message_id.h"
#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Database byte locations and index
 */
//*********************************************************************
typedef enum
{
    db_canary,                           ///< canary value
    db_market,                           ///< market country
    db_serial_enable_msb,                ///< serial output 1
    db_serial_enable_lsb,                ///< serial output 2
    db_serial_flags,                     ///< serial state commands
    db_calibration_msb,                  ///< DB cal 1
    db_calibration_lsb,                  ///< DB cal 2               ///< Calibration Flags
    db_mfg_data_0,                       ///< LIC Calibration (value read)
    db_mfg_data_1,                       ///<
    db_mfg_data_2,                       ///<
    db_mfg_data_3,                       ///<
    db_mfg_data_4,                       ///<
    db_mfg_data_5,                       ///<
    db_mfg_data_6,                       ///<
    db_mfg_data_7,                       ///<
    db_mfg_data_8,                       ///<
    db_mfg_data_9,                       ///<
    db_mfg_data_10,                      ///<
    db_mfg_data_11,                      ///<
    db_mfg_data_12,                      ///<
    db_mfg_data_13,                      ///<
    db_mfg_data_14,                      ///< LIC Calibration - Number of times calibration has been run
    db_mfg_data_15,                      ///<
    db_mfg_data_16,                      ///<
    db_mfg_data_17,                      ///<
    db_mfg_data_18,                      ///<
    db_mfg_data_19,                      ///<
    db_mfg_data_20,                      ///<
    db_current_ir_f,                     ///< LIC Calibration 28
    db_current_ir_b,                     ///< LIC Calibration
    db_current_bl_f,                     ///< LIC Calibration
    db_photo_sw_wait,                    ///< LIC Calibration
    db_ptt_current_ir_f,                 ///< LIC Calibration
    db_ptt_current_ir_b,                 ///< LIC Calibration
    db_ptt_fault_history,                ///<
    db_photo_alarm_eqa_threshold,        ///< 2 Point Calibration
    db_photo_hush_threshold,             ///< 2 Point Calibration
    db_photo_fail_safe_threshold,        ///< 2 Point Calibration
    db_ir_f_factory_clean_air,           ///< 2 Point Calibration & clean air
    db_ir_b_factory_clean_air,           ///< 2 Point Calibration & clean air
    db_blue_factory_clean_air,           ///< 2 Point Calibration & clean air
    db_photo_cal_ir_f_min,               ///< 2 Point Calibration - read only
    db_photo_cal_ir_f_max,               ///< 2 Point Calibration - read only
    db_photo_cal_ir_b_min,               ///< 2 Point Calibration - read only
    db_photo_cal_ir_b_max,               ///< 2 Point Calibration - read only
    db_photo_cal_blue_min,               ///< 2 Point Calibration - read only
    db_photo_cal_blue_max,               ///< 2 Point Calibration - read only
    db_ir_f_clean_air,                   ///< 2 Point Calibration & clean air
    db_ir_b_clean_air,                   ///< 2 Point Calibration & clean air
    db_blue_clean_air,                   ///< 2 Point Calibration & clean air
    db_photo_cal_blue_back_supervision,  ///< 2 Point Calibration - Blue supervision
    db_co_circuit_cal,                   ///< CO Calibration Circuit
    db_co_offset_msb,                    ///< CO Calibration 0
    db_co_offset_lsb,                    ///< CO Calibration 0
    db_co_scale_msb,                     ///< CO Calibration 150
    db_co_scale_lsb,                     ///< CO Calibration 150
    db_battery_voltage_min_msb,          ///< Battery Calibration
    db_battery_voltage_min_lsb,          ///< Battery Calibration
    db_hush_multiplier_msb,              ///< value used to calculate hush for unit msb
    db_hush_multiplier_lsb,              ///< value used to calculate hush for unit lsb
    db_failsafe_multiplier_msb,          ///< value used for failsafe multiplier for unit msb
    db_failsafe_multiplier_lsb,          ///< value used for failsafe multiplier for unit lsb
    db_ambient_light_cal_multiplier_msb, ///< value used to calculate ambient light for unit msb
    db_ambient_light_cal_multiplier_lsb, ///< value used to calculate ambient light for unit lsb
    db_failsafe_min,                     ///< value used for DB failsafe for unit msb
    db_failsafe_max,                     ///< value used for DB failsafe for unit lsb
    db_photo_read_sample_delay,          ///< time in us between photo reads
    db_photo_read_sample_count,          ///< number of photo reads taken
    db_photo_read_sample_discard,        ///< number of high / low photo reads discarded (total = 2*)

#if defined(CONFIG_PHOTO_MWMA)
    // MWMA phot database elements
    db_flm_ir_b_threshold,               ///< photo - flaming fire     ----    min ir-b for alarm
    db_flaming_ir_f_ir_b_threshold_msb,  ///< photo - Flaming fire     ----    ratio 1
    db_flaming_ir_f_ir_b_threshold_lsb,  ///< photo - Flaming Fire     ----    ratio 1
    db_flaming_bl_f_ir_b_threshold_msb,  ///< photo - Flaming Fire     ----    ratio 2
    db_flaming_bl_f_ir_b_threshold_lsb,  ///< photo - Flaming Fire     ----    ratio 2
    db_photo_ir_f_ir_b_threshold_msb,    ///< photo - MW Fire          ----    ratio 3
    db_photo_ir_f_ir_b_threshold_lsb,    ///< photo - MW Fire          ----    ratio 3
    db_photo_bl_f_ir_b_threshold_msb,    ///< photo - MW Fire          ----    ratio 4
    db_photo_bl_f_ir_b_threshold_lsb,    ///< photo - MW Fire          ----    ratio 4
    db_fwd_open_th,                      ///< photo
    db_bck_open_th,                      ///< photo
    db_fwd_hi_amb_th,                    ///< photo
    db_bck_hi_amb_th,                    ///< photo
    unused_82,                           ///<
    db_photo_smoldering_count_threshold, ///< photo
    db_normal_mode_period,               ///< photo
    db_photo_ir_f_delta,                 ///< photo - All Fire // value also used to get in and out of analysis mode
    db_photo_ir_b_delta,                 ///< photo - All Fire
    db_photo_bl_f_delta,                 ///< photo - All Fire
    db_fire_count,                       ///< photo
    db_fire_debounce_count,              ///< photo

#elif defined(CONFIG_PHOTO_SINGLE)
    // Single Channel database elements  need to total the same as above
    db_normal_mode_period,             ///< photo
    db_photo_ir_f_delta,               ///< photo - All Fire // value also used to get in and out of analysis mode
    db_photo_ir_b_delta,               ///< photo - All Fire // value also used to get in and out of analysis mode
    db_photo_low_dark_fault_threshold, ///< photo
    db_photo_hi_dark_fault_threshold,  ///< photo
    db_photo_low_fwd_light_threshold,  ///< photo
    db_photo_low_bck_light_threshold,  ///< photo
    db_smold_count_threshold,          ///< photo smoldering fire
    db_smold_time_threshold_msb,       ///< photo smoldering fire
    db_smold_time_threshold_lsb,       ///< photo smoldering fire
    db_ror_threshold_msb,              ///< photo ror fire
    db_ror_threshold_lsb,              ///< photo ror fire
    db_ror_confidence_check,           ///< photo ror fire
    db_ror_min_threshold,              ///< photo ror fire
    db_accel_threshold_msb,            ///< photo accel fire
    db_accel_threshold_lsb,            ///< photo accel fire
    db_accel_ror_threshold,            ///< photo accel fire
    db_accel_confidence_check,         ///< photo accel fire
    db_accel_min_threshold,            ///< photo accel fire
    db_fail_safe_high_threshold,       ///< photo failsafe fire
    db_fail_safe_confidence_check,     ///< photo failsafe fire
#endif
    db_photo_normal_sensors,         ///< photo - requirement to enter analysis mode.
    inline_start_irf_delta_th,       ///< start IR F delta
    inline_ratio_stabilization_time, ///< Time period for ratio stabalization
    inline_ratio_sample_count,       ///< Number of sample counts for ratio
    irf_irb_ratio_target_msb,        ///< IR F IR B ratio target value msb
    irf_irb_ratio_target_lsb,        ///< IR F IR B ratio target value lsb
    blf_irb_ratio_target_msb,        ///< BL F IR B ratio target value msb
    blf_irb_ratio_target_lsb,        ///< BL F IR B ratio target value lsb
    irf_irb_factor_equation_k1,      ///< IR F IR B k1 equation vlaue
    irf_irb_factor_equation_k2,      ///< IR F IR B k2 equation vlaue
    blf_irb_factor_equation_k3,      ///< IR F IR B k3 equation vlaue
    blf_irb_factor_equation_k4,      ///< IR F IR B k4 equation vlaue
    irf_irb_ratio_factor,            ///< Ratio factor IR F and IR B
    blf_irb_ratio_factor,            ///< Ratio factor BL F and IR B
    db_unused_104,                   ///<
    db_unused_105,                   ///<
    db_unused_106,                   ///<
    db_unused_107,                   ///<
    db_unused_108,                   ///<
    db_counter_no_of_days_msb,       ///< Number of days msb
    db_counter_no_of_days_lsb,       ///< Number of days lsb
    db_counter_sensor_health_msb,    ///< Sensor health counter msb
    db_counter_sensor_health_lsb,    ///< Sensor health counter lsb
    db_counter_sensor_short_msb,     ///< Sensor short counter msb
    db_counter_sensor_short_lsb,     ///< Sensor short counter lsb
    db_counter_smoke_chamber_msb,    ///< Sensor chamber counter msb
    db_counter_smoke_chamber_lsb,    ///< Sensor chamber counter lsb
    db_counter_ptt_msb,              ///< Sensor PTT counter msb
    db_counter_ptt_lsb,              ///< Sensor PTT counter lsb
    db_unused_119,                   ///<
    db_unused_120,                   ///<
    db_erase_count_msb,              ///< DB erase counter msb
    db_erase_count_lsb,              ///< DB erase counter lsb
    db_page_number,                  ///< DB page number value
    db_crc_1,                        ///< crc value msb
    db_crc_2,                        ///< crc value lsb
    db_crc_3,                        ///< crc value msb
    db_crc_4                         ///< crc value lsb
} database_index;

//*********************************************************************
/**
 * \brief Choice of market flag byte
 */
//*********************************************************************
typedef enum
{
    market_us     = 0x01, ///< US market flag value
    market_canada = 0x02, ///< Canada market flag value
    market_uk     = 0x04  ///< UK market flag value
} market_flag;

//*********************************************************************
/**
 * \brief calibration flag enum list
 */
//*********************************************************************
typedef enum
{
    cal_co_circuit   = 0x01, ///< CO circuit cal flag value
    cal_battery      = 0x02, ///< Battery cal flag value
    cal_lic          = 0x04, ///< LIC cal flag value
    cal_2_point      = 0x08, ///< 2 Point cal flag value
    cal_0_ppm        = 0x10, ///< 0 PPM cal flag value
    cal_150_ppm      = 0x20, ///< 150 PPM cal flag value
    cal_400_ppm      = 0x40, ///< 400 PPM cal flag value
    cal_inline_smoke = 0x80  ///< Inline smoke cal flag value
} calibration_flag;

//*********************************************************************
/**
 * \brief Database task assignment function
 * \param message message ID passed to database task
 * \param data Any data sent with this database task
 */
//*********************************************************************
void database_task(message_id const message, uint16_t const data);

//*********************************************************************
/**
 * \brief Get database value from index
 * \param index location of desired database value
 * \returns database value at given index
 */
//*********************************************************************
uint8_t db_get_value(database_index const index);

//*********************************************************************
/**
 * \brief Get 16 bit database value from index
 * \param msb msb location of desired database value
 * \param lsb lsb location of desired database value
 * \returns 16 bit database value at given msb + lsb
 */
//*********************************************************************
uint16_t db_get_value16(database_index const msb, database_index const lsb);

//*********************************************************************
/**
 * \brief Set database value at given index
 * \param index location of desired database value
 * \param value desired value to set at given index
 */
//*********************************************************************
void db_set_value(database_index const index, uint8_t const value);

//*********************************************************************
/**
 * \brief Set 16 bit database value at given index
 * \param msb msb location of desired database value
 * \param lsb lsb location of desired database value
 * \param value desired 16 bit value to set at given msb + lsb
 */
//*********************************************************************
void db_set_value16(database_index const msb, database_index const lsb, uint16_t const value);

//*********************************************************************
/**
 * \brief Set database value at given index
 * \param index location of desired database value
 * \param values desired values to set at given index locations
 * \param count number of sequential values to set
 */
//*********************************************************************
void db_set_sequential_values(database_index const index, uint8_t *values, uint8_t const count);

#endif
