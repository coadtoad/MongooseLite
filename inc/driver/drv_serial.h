//******************************************************************
/*! \file drv_serial.h
 *
 * \brief Kidde: Mongoose-fw - Declare serial drier functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_SERIAL_DRV_H_
#define INC_SERIAL_DRV_H_

#include <stdbool.h>
#include <stdint.h>
//*********************************************************************
/**
 * \brief Any configuration necessary to configure the UART
 * \note If RX is high, this does not enable the UART
 */
//*********************************************************************
void serial_init(void);

//*********************************************************************
/**
 * \brief Disable the TX interrupt
 */
//*********************************************************************
void serial_enable_tx_int(void);

//*********************************************************************
/**
 * \brief Enable the TX interrupt
 */
//*********************************************************************
void serial_disable_tx_int(void);

//*********************************************************************
/**
 * \brief Is the UART enabled
 * \returns If the UART is enabled
 */
//*********************************************************************
bool serial_enabled(void);

//*********************************************************************
/**
 * \brief Retrieve the RX buffer for processing
 * \returns A pointer to the RX buffer
 */
//*********************************************************************
uint8_t *serial_get_rx_buffer(void);

//*********************************************************************
/**
 * \brief Send the NULL terminated string over the UART
 * \note This should be a non-blocking call
 * \param str The Null terminated string to transmit
 */
//*********************************************************************
void serial_send_string(char const *str);

#endif /* INC_SERIAL_DRV_H_ */
