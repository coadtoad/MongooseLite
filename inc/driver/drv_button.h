//******************************************************************
/*! \file drv_button.h
 *
 * \brief Kidde: Mongoose-fw - Declare the button driver functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_BUTTON_DRV_H_
#define INC_BUTTON_DRV_H_

#include <stdbool.h>

//*********************************************************************
/**
 * \brief Configure pins related to the interconnect
 *
 * \note The following pins should be configured:
 *   - TEST_BUTTON
 *     - Digital input
 *     - Interrupt on product specific edge
 */
//*********************************************************************
void button_init(void);

//*********************************************************************
/**
 * \brief Read the value of TEST_BUTTON
 *
 * \returns If TEST_BUTTON indicates that the button is down
 */
//*********************************************************************
bool button_is_down(void);

//*********************************************************************
/**
 * \brief Clear the TEST_BUTTON edge interrupt
 */
//*********************************************************************
void button_clear(void);

//*********************************************************************
/**
 * \brief Called by the TEST_BUTTON edge interrupt handler
 */
//*********************************************************************
void button_irq_task(void);

//*********************************************************************
/**
 * \brief Enable the TEST_BUTTON edge interrupt
 */
//*********************************************************************
void enable_button_intr(void);

//*********************************************************************
/**
 * \brief Disable the TEST_BUTTON edge interrupt
 */
//*********************************************************************
void disable_button_intr(void);

#endif /* INC_BUTTON_DRV_H_ */
