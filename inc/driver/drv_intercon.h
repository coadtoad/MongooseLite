//******************************************************************
/*! \file drv_intercon.h
 *
 * \brief Kidde: Mongoose-fw - Declare interconnect driver functions
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_INTERCON_DRV_H_
#define INC_INTERCON_DRV_H_

#include <stdbool.h>

//*********************************************************************
/**
 * \brief Configure pins related to the interconnect
 * \note The following pins should be configured:
 *   - Discrete
 *     - INT_HI_REF
 *       - Digital output
 *       - Output low
 *     - INT_LOW_REF
 *       - Digital output
 *       - Output low
 *     - INT_OUT
 *       - Digital output
 *       - Output low
 *     - INT_IN
 *       - Digital input
 *       - Interrupt on rising edge
 *   - AFE
 *     - IODIR_TX
 *     - TXRX
 */
//*********************************************************************
void interconnect_init(void);

//*********************************************************************
/**
 * \brief Drive the interconnect line high
 *   - Discrete
 *     -# Disable INT_IN interrupt
 *     -# Drive INT_LOW_REF low
 *     -# Drive INT_HI_REF high
 *     -# Drive INT_OUT low
 */
//*********************************************************************
void drive_high(void);

//*********************************************************************
/**
 * \brief Drive the interconnect line low
 *   - Discrete
 *     -# Disable INT_IN interrupt
 *     -# Drive INT_LOW_REF low
 *     -# Drive INT_HI_REF high
 *     -# Drive INT_OUT high
 */
//*********************************************************************
void drive_low(void);

//*********************************************************************
/**
 * \brief Tristate the interconnect line
 *   - Discrete
 *     -# Enable INT_IN interrupt
 *     -# Drive INT_LOW_REF low
 *     -# Drive INT_HI_REF low
 *     -# Drive INT_OUT low
 * \note Enables the interrupt and tristates the interconnect
 */
//*********************************************************************
void set_tristate(void);

//*********************************************************************
/**
 * \brief Read the value of INT_IN
 *   - Discrete
 *     -# Disable INT_IN interrupt
 *     -# Read the value of INT_IN
 * \note Should only be called when the interconnect has been tristated
 * \returns If INT_IN is high
 */
//*********************************************************************
bool read_interconnect(void);

//*********************************************************************
/**
 * \brief Disable the INT_IN interrupt
 *   - Discrete
 *     -# Disable INT_IN interrupt
 */
//*********************************************************************
void disable_interconnect_int(void);

//*********************************************************************
/**
 * \brief Called by the INT_IN interrupt handler
 */
//*********************************************************************
void interconnect_irq_task(void);

#endif /* INC_INTERCON_DRV_H_ */
