//******************************************************************
/*! \file drv_afe_serial_commands.h
 *
 * \brief Kidde: Mongoose-fw - Define afe serial commands
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef DRIVER_AFE_SERIAL_COMMANDS_H_
#define DRIVER_AFE_SERIAL_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

void afe_serial_command(uint8_t const *rx_buffer);
void afe_serial_output_regs(void);

#endif /* DRIVER_AFE_SERIAL_COMMANDS_H_ */
