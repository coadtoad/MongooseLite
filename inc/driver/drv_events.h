//******************************************************************
/*! \file drv_events.h
 *
 * \brief Kidde: Mongoose-fw - Declare functions for the event handler
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef DRIVER_EVENTS_H_
#define DRIVER_EVENTS_H_

#include "drv_database.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Defines the members of the event_entry structure.
 * @note This structure must remain at a total size of 8 bytes.  The
 *          padding member may be broken into other members as necessary as
 *          long as the overall size remains the same.
 */
//*********************************************************************
typedef struct
{
    uint8_t event_id;
    uint8_t sequence_number;
    uint16_t day;
    unsigned padding;
} event_entry;

//*********************************************************************
/**
 * \brief Get the maximum number of events that can be stored
 * \returns The maximum number of events that can be stored
 */
//*********************************************************************
uint16_t max_events(void);

//*********************************************************************
/**
 * \brief Write \p count events into the event history
 * \param buffer A pointer to events to write into the event history
 * \param offset The zero-based index of the first event to write
 * \param count The number of events to write into the event history
 * \returns The number of events written, -1 on error
 */
//*********************************************************************
int16_t write_events(event_entry const *const buffer, size_t const offset, size_t const count);

/**
 * \brief Read @p count events from the event history
 *
 * \param[out] buffer A pointer to enough memory to hold @p count events
 * \param offset The zero-based index of the first event to read
 * \param count The number of events to read
 * \returns The number of events that were read
 */
int16_t read_events(event_entry *const buffer, size_t const offset, size_t const count);

/**
 * \brief Function to run event erase
 * \returns true on success
 */
bool begin_event_erase(void);

/**
 * \brief Verify @p count events
 *
 * \param[in] buffer A pointer to the buffer to compare
 * \param offset The zero-based index of the first event to write
 * \param count The number of events to compare
 * \returns true on success
 */
bool begin_event_verify(event_entry const *const buffer, size_t const offset, size_t const count);

#endif /* DRIVER_EVENTS_H_ */
