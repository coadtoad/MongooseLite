//******************************************************************
/*! \file drv_timer.h
 *
 * \brief Kidde: Mongoose-fw - Declare the Timer support functions
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 * There are currently three time formats that the system uses.  All formats
 * count time in milliseconds.
 *   - 64-bit time - This is cannonical time.  This has a rollover period of
 *                   584,554,531 years and should never roll over.  All time
 *                   calculations must be done in 64-bit time or rollover can
 *                   cause incorrect results
 *   - 32-bit time - This is stored in the message queue to save space.  This
 *                   has a rollover period of 49 days.  Storing messages with
 *                   64-bit time would add 4 or 8 bytes to each message
 *   - RTC time - This is counted by the RTC clock.  It counts to exactly one
 *                minute.  This counts from 0 to 59,999.  Given the limits, this
 *                is stored in a uint16_t
 *
 * There are currently three times that the system tracks.  Each serves
 * a purpose, and it is critical that the correct time is used in each
 * situation.
 *   - RTC - This is tracked by the RTC.  It is used to:
 *     - Schedule the next wakeup
 *     - Determine what the time is when the processor wakes up
 *     - Determine if a message should be dispatched
 *   - current_message_time - This is stored in current_message_time in drv_timer.c.
 *     - Set to the dispatch time of the currently executing message.  This
 *       ensures that timing does not drift even if a long running message
 *       delays the execution of a message by several milliseconds
 *     - Set to RTC time when entering an interrupt handler.  This ensures that
 *       any messages scheduled by the interrupt handler will be dispatched
 *       within 1ms of the desired time
 *   - reference_time - This is stored in reference_time in message_heap.c.
 *     - Set to the dispatch time of the last dispatched message
 *     - Is used to calculate message priority
 *     - @b Must never go backwards or the message queue could be corrupted
 */
//*******************************************************************

#ifndef INC_TIMER_DRV_H_
#define INC_TIMER_DRV_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Configure the timers for the main timer
 * \returns True if the main timer was successfully initialized
 * \note The main timer must operate in the lowest power mode
 * \note The main timer mest be able to interrupt in millisecond intervals
 *       up to 1 minute
 */
//*********************************************************************
bool timer_init(void);

//*********************************************************************
/**
 * \brief Delay execution for a number of microseconds
 * \note This has a resolution of 1us.
 * \param count The duration, in us, to delay for
 */
//*********************************************************************
void timer_delay_us(uint16_t count);

//*********************************************************************
/**
 * \brief Called by the main timer interrupt handler
 */
//*********************************************************************
void timer_irq_task(void);

//*********************************************************************
/**
 * \brief Initalizes the timer housekeeping variables.
 */
//*********************************************************************
void timer_init_vars(void);

/**
 * @brief Convert from 32-bit time to 64-bit time
 *
 * @param time The 32-bit time point to convert
 * @returns @p time in 64-bit time
 */
uint64_t time_32bit_to_64bit(uint32_t const time);

/**
 * @brief Convert from 64-bit time to 32-bit time
 *
 * @param time The 64-bit time point to convert
 * @returns @p time in 32-bit time
 */
uint32_t time_64bit_to_32bit(uint64_t const time);

/**
 * @brief Convert from 64-bit time to RTC time
 *
 * @param time The 64-bit time point to convert
 * @returns @p time in RTC time
 */
uint16_t time_64bit_to_rtc(uint64_t const time);

/**
 * @brief Convert from 32-bit time to RTC time
 *
 * @param time The 32-bit time point to convert
 * @returns @p time in RTC time
 */
uint16_t time_32bit_to_rtc(uint32_t const time);

/**
 * @brief Convert from RTC time to 32-bit time
 *
 * @param time The RTC time point to convert
 * @returns @p time in 32-bit time
 */
uint32_t time_rtc_to_32bit(uint16_t const time);

/**
 * @brief Convert from RTC time to 64-bit time
 *
 * @param time The RTC time point to convert
 * @returns @p time in 64-bit time
 */
uint64_t time_rtc_to_64bit(uint16_t const time);

/**
 * @brief Set the current time
 *
 * @param time The new time
 */
void set_current_message_time(uint64_t const time);

/**
 * @brief Get the current time
 *
 * @returns the current time
 */
uint64_t get_current_message_time(void);

#endif /* INC_TIMER_DRV_H_ */
