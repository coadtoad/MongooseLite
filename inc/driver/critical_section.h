//******************************************************************
/*! \file critical_section.h
 *
 * \brief Kidde: Mongoose-fw - Declare the critical section interrupts and functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef DRIVER_CRITICAL_SECTION_H_
#define DRIVER_CRITICAL_SECTION_H_

//*********************************************************************
/**
 * \brief Disable interrupts while @p section is called
 * \note Disable interrupts and then call section.  Interrupts will be returned to
 *          the previous state before returning.
 * \param section The function to call with interrupts disabled
 * \param data The pointer to be passed as an argument to @p section
 */
//*********************************************************************
void critical_section(void (*section)(void *), void *data);

#endif /* DRIVER_CRITICAL_SECTION_H_ */
