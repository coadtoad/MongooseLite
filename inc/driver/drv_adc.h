//******************************************************************
/*! \file drv_adc.h
 *
 * \brief Kidde: Mongoose-fw - Declare the ADC driver functions
 *
 * \author Brandon Widow
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************
#ifndef DRV_INCLUDE_DRV_ADC_H_
#define DRV_INCLUDE_DRV_ADC_H_


#include <stdbool.h>
#include <stdint.h>

/**
 * @def SAMPLE_COUNT_MAX
 * @brief The maximum number of ADC samples to take within a single function call of adc_take_multiple_samples()
 */
#define SAMPLE_COUNT_MAX 12


#endif
