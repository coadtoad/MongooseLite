//******************************************************************
/*! \file drv_sounder.h
 *
 * \brief Kidde: Mongoose-fw - Declare sounder driver functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_SOUNDER_DRV_H_
#define INC_SOUNDER_DRV_H_

//*********************************************************************
/**
 * \brief Configure pins related to the sounder
 *
 * \note The following pins should be configured:
 *   - HORN_DISABLE
 *     - Digital output
 *     - Output high
 */
//*********************************************************************
void sounder_init(void);

//*********************************************************************
/**
 * \brief Drive HORN_DISABLE low
 */
//*********************************************************************
void sounder_on(void);

//*********************************************************************
/**
 * \brief Drive HORN_DISABLE high
 */
//*********************************************************************
void sounder_off(void);

#endif /* INC_SOUNDER_DRV_H_ */
