// utility.h

#ifndef INCLUDE_UTILITY_H
#define INCLUDE_UTILITY_H
void serial_send_string(char const *str);
void uint32_to_hex(char *dest, uint32_t const data);
void serial_send_int_ex(char *pPrefix, uint16_t val, uint8_t base, char *suffix);
void print_device_model(void);


#endif


