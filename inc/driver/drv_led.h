//******************************************************************
/*! \file drv_led.h
 *
 * \brief Kidde: Mongoose-fw - Declare LED driver functions
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_DRIVER_DRV_LED_H_
#define INC_DRIVER_DRV_LED_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Configure pins related to the interconnect
 * \note The following pins should be configured:
 *   - POWER_LED_HIGH
 *     - Digital output
 *     - Output product specific
 *   - POWER_LED_LOW
 *     - Digital output
 *     - Output product specific
 *   - GREEN_LED
 *     - Digital output
 *     - Output product specific
 *   - RED_LED
 *     - Digital output
 *     - Output product specific
 *   - AMBER_LED
 *     - Digital output
 *     - Output product specific
 *   - CO_LED
 *     - Digital output
 *     - Output product specific
 *   - ESCAPE_LIGHT
 *     - Digital output
 *     - Output product specific
 */
//*********************************************************************
void led_init(void);

//*********************************************************************
/**
 * \brief Turn the amber LED on
 * \note This drives AMBER_LED to turn the amber LED on.  On some products this may
 *          require turning on both the red and green LEDs.
 */
//*********************************************************************
void amber_led_on(void);

//*********************************************************************
/**
 * \brief Turn the amber LED off
 * \note This drives AMBER_LED to turn the amber LED off.  On some products this may
 *          require turning off both the red and green LEDs.
 */
//*********************************************************************
void amber_led_off(void);

//*********************************************************************
/**
 * \brief Configure AMBER_LED as PWM
 * \note On some products this may require configuring both the red and green LEDs as PWM.
 */
//*********************************************************************
void amber_led_pwm_begin(void);

//*********************************************************************
/**
 * \brief Configure AMBER_LED as digital output
 * \note On some products this may require configuring both the red and green LEDs as PWM.
 */
//*********************************************************************
void amber_led_pwm_end(void);

//*********************************************************************
/**
 * \brief Set the PWM duty cycle on AMBER_LED
 * \note On some products this may require setting the duty cycle on both the red and green LEDs.
 * \param duty The duty cycle, in percent, to set the PWM to
 */
//*********************************************************************
void amber_led_pwm(uint8_t const duty);

//*********************************************************************
/**
 * \brief Turn the green LED on
 * \note This drives GREEN_LED to turn the green LED on
 */
//*********************************************************************
void green_led_on(void);

//*********************************************************************
/**
 * \brief Turn the green LED off
 * \note This drives GREEN_LED to turn the green LED off
 */
//*********************************************************************
void green_led_off(void);

//*********************************************************************
/**
 * \brief Configure GREEN_LED as PWM
 */
//*********************************************************************
void green_led_pwm_begin(void);

//*********************************************************************
/**
 * \brief Configure GREEN_LED as digital output
 */
//*********************************************************************
void green_led_pwm_end(void);

//*********************************************************************
/**
 * \brief Set the PWM duty cycle on GREEN_LED
 * \param duty The duty cycle, in percent, to set the PWM to
 */
//*********************************************************************
void green_led_pwm(uint8_t const duty);

//*********************************************************************
/**
 * \brief Turn the red LED on
 * \note This drives RED_LED to turn the red LED on
 */
//*********************************************************************
void red_led_on(void);

//*********************************************************************
/**
 * \brief Turn the red LED off
 * \note This drives RED_LED to turn the red LED off
 */
//*********************************************************************
void red_led_off(void);

//*********************************************************************
/**
 * \brief Configure RED_LED as PWM
 */
//*********************************************************************
void red_led_pwm_begin(void);

//*********************************************************************
/**
 * \brief Configure RED_LED as digital output
 */
//*********************************************************************
void red_led_pwm_end(void);

//*********************************************************************
/**
 * \brief Set the PWM duty cycle on RED_LED
 * \param duty The duty cycle, in percent, to set the PWM to
 */
//*********************************************************************
void red_led_pwm(uint8_t const duty);

//*********************************************************************
/**
 * \brief Turn the CO LED on
 * \note This drives CO_LED to turn the CO LED on
 */
void blue_led_on(void);

//*********************************************************************
/**
 * \brief Turn the CO LED off
 * \note This drives CO_LED to turn the CO LED off
 */
void blue_led_off(void);

#endif
