//******************************************************************
/*! \file drv_i2c.h
 *
 * \brief Kidde: Mongoose-fw - Declare the I2C driver functions
 *
 * \author Brandon Widow
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************
#ifndef DRV_INCLUDE_DRV_I2C_H_
#define DRV_INCLUDE_DRV_I2C_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief List of molecule register entries
 */
//*********************************************************************
typedef enum
{
    remote_alarm_status  = 0x00,
    co_ppm_lsb           = 0x01,
    co_ppm_msb           = 0x02,
    voice_command        = 0x03,
    temperature          = 0x04,
    day_night_status     = 0x05,
    register_index_count = 0x06
} register_index;

//*********************************************************************
/**
 * \brief Configures the I2C for use
 */
//*********************************************************************
void i2c_init(void);

//*********************************************************************
/**
 * \brief Writes a byte to a specific register address to a target module address
 * \param module_address Specifies the slave module to initiate an I2C communication with
 * \param register_address Specifies the register of the slave module to write to
 * \param data Specifies the single byte of data to be written
 * \returns True if the operation was a success or false if an error occurred during operation
 * \note This function left-shifts the module_address parameter by 1 to send as a 7-bit address
 */
//*********************************************************************
bool i2c_write(uint8_t const module_address, uint8_t const register_address, uint8_t const data);

//*********************************************************************
/**
 * \brief Reads the value of a specific register address from a target module address
 * \param module_address Specifies the address to initiate an I2C communication with
 * \param register_address Specifies the register of the slave module to read from
 * \param data Pointer to a byte to store the read data
 * \returns True if the operation was a success or false if an error occurred during operation
 * \note This function left-shifts the module_address parameter by 1 to send as a 7-bit address
 */
//*********************************************************************
bool i2c_read(uint8_t const module_address, uint8_t const register_address, uint8_t *const data);

//*********************************************************************
/**
 * \brief Sets the value of a specific register address within the module
 * \param index The register index to write to
 * \param value The value of the data written to the register
 */
//*********************************************************************
void set_register_value(register_index const index, uint8_t const value);

//*********************************************************************
/**
 * \brief Returns the value of a specific register address within the module
 * \param index The register index to write to
 * \returns value The value of the specified register index
 */
//*********************************************************************
uint8_t get_register_value(register_index const index);

//*********************************************************************
/**
 * \brief Print the I2C register values
 */
//*********************************************************************
void print_i2c_register_values(void);

#endif
