//******************************************************************
/*! \file drv_photo.h
 *
 * \brief Kidde: Mongoose-fw - "Brief sentence about file"
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_PHOTO_DRV_H_
#define INC_PHOTO_DRV_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Configure pins related to the photo chamber
 *
 * \note The following pins should be configured:
 *   - Discrete
 *     - SW_VBLUE
 *       - Digital output
 *       - 8kHz, 50% duty cycle PWM
 *       - Output low
 *     - BK_FW_SEL
 *       - Digital output
 *       - Output low
 *     - BLUE_IR_SEL
 *       - Digital output
 *       - Output low
 *     - PHOTO_LED_ON
 *       - Analog output
 *     - LED_I_SENSE
 *       - Analog input
 *       - 8-bit reads
 *     - PHOTO_CAV_OFFSET
 *       - Analog output
 *     - LO_SMOKE_OUT
 *       - Analog input
 *       - 8-bit reads
 *     - HI_SMK_OUT
 *       - Analog input
 *       - 8-bit reads
 */
//*********************************************************************

//*********************************************************************
/**
 * \brief Enum to determine photo reading source
 * \note used in read_light() function
 */
//*********************************************************************
typedef enum
{
    IR_F,
    IR_B,
    number_of_light_signals
} light_signal;

bool photo_init(void);

//*********************************************************************
/**
 * \brief Any configuration necessary to execute a photo read sequence
 * \returns If the configuration succeeded
 */
//*********************************************************************
bool begin_photo_read(void);

//*********************************************************************
/**
 * \brief Any configuration necessary when finishing a photo read sequence
 */
//*********************************************************************
bool end_photo_read(void);

//*********************************************************************
/**
 * \brief Select the forward photo diode
 *   - Discrete
 *     -# Drive BK_FW_SEL high
 */
//*********************************************************************
void select_led_direction(const light_signal photo_reading);

//*********************************************************************
/**
 * \brief Select to do a high gain read
 *   - Discrete
 *     - Select to read HI_SMK_OUT
 */
//*********************************************************************
void select_high_gain(void);

//*********************************************************************
/**
 * \brief Select to do a low gain read
 *   - Discrete
 *     - Select to read LOW_SMOKE_OUT
 */
//*********************************************************************
void select_low_gain(void);

//*********************************************************************
/**
 * \brief Read the photo section
 *   - Discrete
 *     - Read either HI_SMK_OUT or LOW_SMOKE_OUT based on select_high_gain() and
 *       select_low_gain()
 */
//*********************************************************************
uint8_t sample(bool led_on);

//*********************************************************************
/**
 * \brief Drive a voltage on PHOTO_LED_ON
 */
//*********************************************************************
void led_on(void);

//*********************************************************************
/**
 * \brief Turn off PHOTO_LED_ON
 */
//*********************************************************************
void led_off(void);

//*********************************************************************
/**
 * \brief Drive a voltage on PHOTO_CAV_OFFSET
 * \param value The value, in DAC counts, to drive on PHOTO_CAV_OFFSET
 */
//*********************************************************************
void set_clean_air(uint8_t const value);

//*********************************************************************
/**
 * \brief Begin driving SW_VBLUE
 * \note Begins driving the SW_VBLUE line as an 8kHz, 50% dutcy cycle PWM pin.
 */
//*********************************************************************
void begin_charge_blue_led(void);

//*********************************************************************
/**
 * \brief Stop driving SW_VBLUE
 * \note PWM on SW_VBLUE is disabled.  A low signal is output after this function.
 */
//*********************************************************************
void end_charge_blue_led(void);

//*********************************************************************
/**
 * \brief Set current value for ir-b led
 * \param value The current value to drive IR-B led
 * \return true if successful
 */
//*********************************************************************
bool set_drive_current_ir_b_led(uint8_t const value);

//*********************************************************************
/**
 * \brief Set current value for ir-f led
 * \param value The current value to drive IR forward led
 * \return true if successful
 */
//*********************************************************************
bool set_drive_current_ir_f_led(uint8_t const value);

//*********************************************************************
/**
 * \brief Set current value IRED
 * \param photo_reading The IRED to set.
 * \param value The value to set the IRED drive to.
 * \return true if successful
 */
//*********************************************************************
bool set_drive_current(const light_signal photo_reading, uint8_t const value);

//*********************************************************************
/**
 * \brief Turn on the ir led in the photo chamber
 */
//*********************************************************************
void led_on_single(void);

#endif /* INC_PHOTO_DRV_H_ */
