//******************************************************************
/*! \file drv_co.h
 *
 * \brief Kidde: Mongoose-fw - Declare the CO driver functions
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_CO_DRV_H_
#define INC_CO_DRV_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Configure pins related to CO
 *
 * \note The following pins should be configured:
 *   -# CO_OUT
 *     - Analog input
 *     - 10-bit reads
 *   -# CO_TEST
 *     - Digital output
 *     - 8kHz, 50% duty cycle PWM
 *     - Output low
 */
//*********************************************************************
void co_init(void);

//*********************************************************************
/**
 * \brief  Configure the AFE's CO registers and enable the CO amplifier
 * \return True for success, false for error
 */
//*********************************************************************
bool initialize_afe_co(void);

//*********************************************************************
/**
 * \brief Read the CO input
 * \param sample A pointer to a 16-bit variable to store the measurement data
 * \return True if the configuration for taking measurements successfully completed
 */
//*********************************************************************
bool co_sample(uint16_t *const sample);

//*********************************************************************
/**
 * \brief Begin driving CO_TEST
 * \note Begins driving the CO_TEST line as an 8kHz, 50% duty cycle PWM pin.
 */
//*********************************************************************
void co_start_test_pulse(void);

//*********************************************************************
/**
 * \brief Stop driving CO_TEST
 * \note PWM on CO_TEST is disabled.  A low signal is output after this function.
 */
//*********************************************************************
void co_stop_test_pulse(void);

#endif /* INC_CO_DRV_H_ */
