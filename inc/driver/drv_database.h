//******************************************************************
/*! \file drv_database.h
 *
 * \brief Kidde: Mongoose-fw - Declare database driver functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 *  * \note The database is a nonvolatile array of bytes.  There is a primary and a
 *          backup copy of the database that must be writable completely independently of
 *          each other.
 *
 *          The primary and backup database copies are read and written independently of
 *          each other.
 *
 *          If possible, the database copies should be stored in EEPROM.  If they cannot
 *          be stored in EEPROM, then they can be stored in flash.
 */
//*******************************************************************

#ifndef DRIVER_DATABASE_H_
#define DRIVER_DATABASE_H_

#include "drv_memory.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Database target enum, primary or backup
 */
//*********************************************************************
typedef enum
{
    primary_db = 1,
    backup_db  = 2
} db_target;

//////////////////////////////////////////////////////////////////////////////
/// FLASH BLOCK A AND B DEFINES - CALIBRATION AND MANUFACTURING DATA
//////////////////////////////////////////////////////////////////////////////
//
// Modify these memory defines for each platform
//
#define MEM_EEPROM_SIZE    128
#define mem_EEPROM_CRC_LOC MEM_EEPROM_SIZE - 4

/**
 * @brief Validate the flash database CRC and copy it into @p buffer
 *
 *   -# Validate primary and backup database CRCs
 *   -# If bad primary and good backup, copy backup to primary
 *   -# Copy primary database to \p buffer
 * \param[out] buffer A pointer to an allocated buffer of size
 *                    \ref MEM_EEPROM_SIZE to copy the database into
 * \param size The size of the buffer pointed to by \p buffer in bytes.
 * \returns true on success
 */
//*********************************************************************
bool database_init(uint8_t *const buffer, size_t const size);

//*********************************************************************
/**
 * \brief Read a byte from the primary database copy
 * \param index The byte offset of the database entry from the beginning of
 *                  the database
 * \returns The value at offset \p index
 */
//*********************************************************************
uint8_t read_primary_db(uint8_t const index);

//*********************************************************************
/**
 * \brief Read a byte from the backup database copy
 * \param index The byte offset of the database entry from the beginning of
 *                  the database
 * \returns The value at offset \p index
 */
//*********************************************************************
uint8_t read_backup_db(uint8_t const index);

/**
 * @brief Writes a buffer to flash in either the primary or backup database position
 *
 * @param[in] block  Primary or Backup database.
 * @param[in] buffer The buffer to write
 * @param[in] size The length of the buffer in bytes.
 * \returns true on success
 *
 */
bool begin_flash_write(db_target const block, uint8_t *const buffer, size_t const size);

/**
 * @brief Compares a flash database to a buffer.
 *
 * @param[in] block  Primary or Backup database.
 * @param[in] buffer The buffer to compare
 * @param[in] size The length of the buffer in bytes.
 * \returns true on success
 *
 */
bool begin_flash_verify(db_target const block, uint8_t *const buffer, size_t const size);

/**
 * @brief Determines if the flash page holding the \p target is full.
 *
 * @return True if the flash page is full and needs to be erased.
 * @param[in] target primary or backup database.
 *
 */
bool is_flash_full(db_target const target);

/**
 * @brief Erases the page containing the primary or backup database.
 *
 * @param[in] block  Primary or Backup database.
 * \returns true on success
 *
 */
bool begin_flash_erase(db_target const block);

//*********************************************************************
/**
 * \brief Increments the offset counter of the database block within
 *  the database page.
 * @param[in] block Primary or Backup database
 */
//*********************************************************************
void update_page_offset(db_target const block);

#endif /* DRIVER_DATABASE_H_ */
