//******************************************************************
/*! \file drv_sleep.h
 *
 * \brief Kidde: Mongoose-fw - Declare sleep driver functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_SLEEP_DRV_H_
#define INC_SLEEP_DRV_H_

#include <stdbool.h>
#include <stdint.h>

#define WKUP_FLAG_TEST_BUTTON 2 /**< Wakeup source is the Test/Reset Button */
#define WKUP_FLAG_INT_IN      6 /**< Wakeup source is the interconnect */

//*********************************************************************
/**
 * \brief Hault Mode flags values enum
 * \note low-power scheduler related defines
 */
//*********************************************************************
typedef enum
{
    SLEEP_MODE_UART_TX  = 0x01, ///< TX hault
    SLEEP_MODE_UART_RX  = 0x02, ///< RX hault
    SLEEP_MODE_FORCE    = 0x04, ///< A way to force sleep mode.
    SLEEP_MODE_LED      = 0x08, ///< LED
    SLEEP_MODE_HORN     = 0x10, ///< Horn
    SLEEP_MODE_BATCHECK = 0x20, ///< Battery Check
    SLEEP_MODE_PWM      = 0x40  ///< PWM timer output
} sleep_mode_flags;

//*********************************************************************
/**
 * \brief Enter low power mode
 * \note The low power mode entered is the lowest power mode that is permitted by
 *          the active timers and UARTs.
 * \note Active peripherals are indicated by the current halt flags.
 */
//*********************************************************************
void enter_low_power(void);

//*********************************************************************
/**
 * \brief Clear a flag that the given peripheral is active
 * \note This should use a critical_section() to avoid race conditions
 */
//*********************************************************************
void sleep_clear_sleep_flag(sleep_mode_flags flag);

//*********************************************************************
/**
 * \brief Set a flag that the given peripheral is active
 * \note This should use a critical_section() to avoid race conditions
 */
//*********************************************************************
void sleep_set_sleep_flag(sleep_mode_flags flag);

#endif /* INC_SLEEP_DRV_H_ */
