//******************************************************************
/*! \file drv_afe.h
 *
 * \brief Kidde: Mongoose-fw - Declare functions for AFE driver
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef HAL_INCLUDE_AFE_DRV_H_
#define HAL_INCLUDE_AFE_DRV_H_
#include "drv_photo.h"
#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief AFE faults.
 */
//*********************************************************************
typedef enum
{
    afe_flt_vcc_low,
} afe_fault;

//*********************************************************************
/**
 * \brief Define the addresses of the CO registers.
 */
//*********************************************************************
typedef enum
{
    revid        = 0x00, ///< Device Information
    status1      = 0x01, ///<   Status 1
    status2      = 0x02, ///<   Status 2
    mask         = 0x03, ///<   Interrupt Mask
    config1      = 0x04, ///<   Config 1
    config2      = 0x05, ///<   Config 2
    enable1      = 0x06, ///<   Enable 1
    enable2      = 0x07, ///<   Enable 2
    control      = 0x08, ///<   Control
    slptmr1      = 0x09, ///<   Sleep Timer 1
    slptmr2      = 0x0a, ///<   Sleep Timer 2
    gpio_amux    = 0x0b, ///<   GPIO and AMUX
    co_battest   = 0x0c, ///<   CO and Battery Test
    co           = 0x0d, ///<   CO Amplifier
    vboost       = 0x0e, ///<   Boost Converter
    ledldo       = 0x0f, ///<   LED LDO
    ph_ctrl      = 0x10, ///<   Photo Amplifier
    led_dac_a    = 0x11, ///<   LED DAC A
    led_dac_b    = 0x12, ///<   LED DAC B
    num_afe_regs = 0x13  ///< Number of AFE Registers
} afe_reg_addr;

//*********************************************************************
/**
 * \brief  Different byte values of status1 struct
 */
//*********************************************************************
union status1
{
    struct
    {
        unsigned char int_unit   : 1; ///<   Value of INT_UINT pin
        unsigned char bst_err    : 1; ///<   Boost converter power good error
        unsigned char bst_nact   : 1; ///<   Boost activity monitor
        unsigned char ots_wrn    : 1; ///<   Thermal warning flag
        unsigned char ots_err    : 1; ///<   Thermal shutdown error
        unsigned char mculdo_err : 1; ///<   MCU LDO power good error
        unsigned char vcclow     : 1; ///<   VCC low warning
        unsigned char slp_done   : 1; ///<   Sleep timer wakeup flag
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief  Different byte values of status2 struct
 */
//*********************************************************************
union status2
{
    struct
    {
        unsigned char bst_pg : 1; ///<   Boost power good indicator
        unsigned char mcu_pg : 1; ///<   MCU LDO power good indicator
        unsigned char        : 6; ///<   Reserved
    };

    uint8_t byte;
};

//*********************************************************************
/**
 * \brief enum of the settings options for AMUX.
 */
//*********************************************************************
typedef enum
{
    amux_off,
    coo,
    aout_ph,
    pdo
} tps8802_amux;

//*********************************************************************
/**
 * \brief Define the settings INT_DIR.
 */
//*********************************************************************
typedef enum
{
    intcon_rx = 0, ///< Put AFE into receive mode, i.e. listening on interconnect wire.
    intcon_tx = 1  ///< Put AFE into receive mode, i.e. driving interconnect wire.
} intcon_dir;

//*********************************************************************
/**
 * \brief Sets the current for LEDA
 *          0-255 counts equal 0-300ma
 * \param led_current New LEDA drive current setting
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_set_leda_current(uint8_t const led_current);

//*********************************************************************
/**
 * \brief Sets the current for LEDB (0-255)
 *          0-255 counts equal 0-300ma
 * \param led_current New LEDB drive current setting
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_set_ledb_current(uint8_t const led_current);

//*********************************************************************
/**
 * \brief Enables the boost converter.
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_boost_enable(void);

//*********************************************************************
/**
 * \brief Disables the boost converter.
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_boost_disable(void);

//*********************************************************************
/**
 * \brief Gets the data from the revid register
 * \param data pointer to location to put value read
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_get_revid(uint8_t *const data);

//*********************************************************************
/**
 * \brief Gets the data from the status1 register
 * \param data pointer to location to put value read
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_get_status1(union status1 *const data);

//*********************************************************************
/**
 * \brief Gets the data from the status2 register
 * \param data pointer to location to put value read
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_get_status2(union status2 *const data);

//*********************************************************************
/**
 * \brief Initalizes AFE registers.
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_init(void);

//*********************************************************************
/**
 * \brief Selects the output from the AMUX.
 * \param setting type tps8802_amux to set the amux to.
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_amux_select(tps8802_amux const setting);

//*********************************************************************
/**
 * \brief Puts AFE into sleep mode.
 * \return True for success, false for error
 * @note should only be used by sounder, photo and device init
 */
//*********************************************************************
bool afe_sleep(void);

//*********************************************************************
/**
 * \brief Puts AFE into active mode.
 * \return True for success, false for error
 * @note should only be used by sounder, photo and device init
 */
//*********************************************************************
bool afe_wake(void);

//*********************************************************************
/**
 * \brief Sets the interconnect mode.
 * \param dir Receive or Transmit.
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_set_intcon_dir(intcon_dir const dir);

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
/**
 * \brief Sets the AFE sleep timer.
 *
 * \param time 0 to 65535 milliseconds
 * \return True for success, false for error
 */
bool afe_set_sleep_time(uint16_t const time);

//*********************************************************************
/**
 * \brief Sets the AFE registers to their default state.
 *          BST_EN, BST_CHARGE, VBST, VMCUSET bits and STATUS1 register are
 *          unchanged.
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_reset(void);

//*********************************************************************
/**
 * \brief Reads a register from AFE.
 * \param reg_address Register in AFE from which to read.
 * \param data pointer to location to put data read.
 * \return True for success, False for error.
 */
//*********************************************************************
bool afe_read_register(afe_reg_addr const reg_address, uint8_t *const data);

//*********************************************************************
/**
 * \brief Writes a register to the AFE.
 * \param reg_address Register address in AFE to write.
 * \param data Value to write to register.
 * \return True for success, False for error.
 */
//*********************************************************************
bool afe_write_register(afe_reg_addr const reg_address, uint8_t const data);

//*********************************************************************
/**
 * \brief Gets the value of an AFE shadow register
 * \param reg_address Shadow register address to read from
 * \return The value of the AFE shadow register
 */
//*********************************************************************
uint8_t afe_get_value(afe_reg_addr const reg_address);

//*********************************************************************
/**
 * \brief Writes the current shadow register to the AFE.
 * \param reg_address AFE register address
 * \return true for success.
 */
//*********************************************************************
bool afe_write_shadow(afe_reg_addr const reg_address);

//*********************************************************************
/**
 * \brief Enables the CO amplifier circuit
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_co_amplifier_enable(void);

//*********************************************************************
/**
 * \brief Congifures the AFE's internal CO circuit
 * \return True for success, false for error
 */
//*********************************************************************
bool afe_configure_co(void);

//*********************************************************************
/**
 * \brief Reads the STATUS1 register and returns Vcc status
 * \return true if Vcc is low
 */
//*********************************************************************
bool afe_vcc_low(void);
#endif
