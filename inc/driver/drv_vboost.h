//******************************************************************
/*! \file drv_vboost.h
 *
 * \brief Kidde: Mongoose-fw - Declare the vboost driver functions
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_DRVIVER_DRV_VBOOST_H_
#define INC_DRVIVER_DRV_VBOOST_H_

#include <stdint.h>

//*********************************************************************
/**
 * \brief Configure AFE functions related to VBOOST
 * \note Turn off Vboost on initialization for low power operation.
 */
//*********************************************************************
void vboost_init(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV high if needed for the sounder
 */
//*********************************************************************
void vboost_horn_on(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV low if needed for the sounder
 */
//*********************************************************************
void vboost_horn_off(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV high if needed for the interconnect
 */
//*********************************************************************
void vboost_interconnect_on(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV low if needed for the interconnect
 */
//*********************************************************************
void vboost_interconnect_off(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV high if needed for the photo samples
 */
//*********************************************************************
void vboost_photo_on(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV low if needed for the photo samples
 */
//*********************************************************************
void vboost_photo_off(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV high if needed for the battery test
 */
//*********************************************************************
void vboost_battery_on(void);

//*********************************************************************
/**
 * \brief Drive VBOOST_DRV low if needed for the battery test
 */
//*********************************************************************
void vboost_battery_off(void);

//*********************************************************************
/**
 * \brief Force the VBOOST signal to be enabled
 */
//*********************************************************************
void vboost_enable(void);

//*********************************************************************
/**
 * \brief Gets the current state of the enabled VBOOST peripherals
 * \returns The byte of VBOOST peripheral flags that are enabled
 */
//*********************************************************************
uint8_t get_vboost_state(void);

#endif /* INC_DRVIVER_DRV_VBOOST_H_ */
