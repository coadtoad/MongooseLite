//******************************************************************
/*! \file drv_memory.h
 *
 * \brief Kidde: Mongoose-fw - Declare memory functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef DRV_MEMORY_H_
#define DRV_MEMORY_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * @brief Calculate a CRC from @p begin to @p end
 *
 * This calculates a CRC from @p begin to @p end .  The location pointed to by
 * @p end is not included in the CRC.  This is the range [ @p begin , @p end ).
 *
 * @note @p begin and @p end must be 4-byte aligned
 *
 * @param begin The address of the first location to CRC
 * @param end The address of the location past the end of the range to CRC
 * @returns The calculated CRC
 */
uint32_t memory_calculate_crc(uint32_t const *const begin, uint32_t const *const end);

/**
 * @brief Calculate a CRC from @p begin to @p end
 *
 * This calculates a CRC from @p begin to @p end .  The location pointed to by
 * @p end is not included in the CRC.  This is the range [ @p begin , @p end ).
 *
 * @note @p begin and @p end do no have to be 4-byte aligned
 * @note The number of bytes to CRC may be any number of bytes from 0 and up
 *
 * @param begin The address of the first location to CRC
 * @param end The address of the location past the end of the range to CRC
 * @returns The calculated CRC
 */
uint32_t memory_calculate_crc_unaligned(uint8_t const *const begin, uint8_t const *const end);

/**
 * @brief Calculate a CRC from @p begin to @p end
 *
 * This calculates a CRC from @p begin to @p end .  The location pointed to by
 * @p end is not included in the CRC.  This is the range [ @p begin , @p end ).
 *
 * Passing a CRC into @p seed allows the CRC calculation to be continued.
 *
 * Passing 0xffffffff to @p seed starts a new CRC.
 *
 * @note @p begin and @p end must be 4-byte aligned
 *
 * @param begin The address of the first location to CRC
 * @param end The address of the location past the end of the range to CRC
 * @param seed The initial value for the CRC
 * @returns The calculated CRC
 */
uint32_t memory_calculate_crc_running(uint32_t const *const begin, uint32_t const *const end, uint32_t const seed);

/**
 * @brief Calculate a CRC from @p begin to @p end
 *
 * This calculates a CRC from @p begin to @p end .  The location pointed to by
 * @p end is not included in the CRC.  This is the range [ @p begin , @p end ).
 *
 * Passing a CRC into @p seed allows the CRC calculation to be continued.
 *
 * Passing 0xffffffff to @p seed starts a new CRC.
 *
 * @note @p begin and @p end do no have to be 4-byte aligned
 * @note The number of bytes to CRC may be any number of bytes from 0 and up
 *
 * @param begin The address of the first location to CRC
 * @param end The address of the location past the end of the range to CRC
 * @param seed The initial value for the CRC
 * @returns The calculated CRC
 */
uint32_t memory_calculate_crc_unaligned_running(uint8_t const *const begin, uint8_t const *const end,
                                                uint32_t const seed);

/**
 * @brief Validates that the CRCs match
 *
 * @note This assumes that @p calculated is in system endian (little endian on ARM)
 * and that @p expected is in big endian.
 *
 * @param calculated A calculated CRC to validate
 * @param expected A CRC to validate against
 * @returns True if the CRCs match
 */
bool memory_crc_valid(uint32_t const calculated, uint32_t const expected);

/**
 * @brief Update the CRC stored at @p destination
 *
 * @note This assumes that @p destination is in big endian and that @p crc is in
 * system endian (little endian on ARM).
 *
 * @param destination The location to write the CRC to
 * @param crc A CRC to write to memory
 */
void memory_set_crc(uint32_t *const destination, uint32_t const crc);

#endif
