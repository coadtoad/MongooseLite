//******************************************************************
/*! \file drv_battery.h
 *
 * \brief Kidde: Mongoose-fw - Declare driver battery functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_BATTERY_DRV_H_
#define INC_BATTERY_DRV_H_

#include <stdbool.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief msec time to stabalize the battery
 */
//*********************************************************************
#define BATTERY_STABILIZATION_TIME 50 // time msec the battery load is applied

//*********************************************************************
/**
 * \brief Battery status states enum
 */
//*********************************************************************
typedef enum
{
    no_battery,           ///< no battery value
    hushable_low_battery, ///< low battery at hushable level
    low_battery,          ///< low battery non hushable level
    good_battery,         ///< good battery value for idle operation
} battery_status;

//*********************************************************************
/**
 * \brief Configure pins related to the battery test and AC detect
 *
 *
 * \note The following pins should be configured:
 *   - Discrete
 *     - BATTERY_TEST
 *       - Digital output
 *       - Output low
 *     - BATTERY_VOLT
 *       - Analog input
 *       - 8-bit reads
 *     - AC_DETECT
 *       - Digital input
 *
 * \param cal_value A calibration value used to determine battery status
 */
//*********************************************************************
void battery_init(void);

//*********************************************************************
/**
 * \brief Drive BATTERY_TEST high
 */
//*********************************************************************
void battery_test_on(void);

//*********************************************************************
/**
 * \brief Read the value of the battery voltage
 * \returns A 12-bit ADC reading of the battery voltage
 * \note This function sets the battery test pin low after sampling the battery
 */
//*********************************************************************
uint16_t battery_sample(void);

//*********************************************************************
/**
 * \brief Read BATTERY_VOLT and determine the battery status
 *
 * \param[out] batt_value The value read from BATTERY_VOLT
 * \returns The status of the battery
 */
//*********************************************************************
battery_status get_battery_status(uint16_t *const batt_value);

//*********************************************************************
/**
 * \brief Read the value of AC_DETECT
 * \returns The value of AC_DETECT
 */
//*********************************************************************
bool battery_is_ac_detected(void);

//*********************************************************************
/**
 * \brief Sets the low battery threshold.
 * \param[in] cal_value The value to which to set the low battery threshold
 */
//*********************************************************************
void battery_set_threshold(uint16_t const cal_value);

#endif /* INC_BATTERY_DRV_H_ */
