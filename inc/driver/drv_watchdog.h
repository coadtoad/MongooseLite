//******************************************************************
/*! \file drv_watchdog.h
 *
 * \brief Kidde: Mongoose-fw - Declare watchdog timer kick function
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_DRIVER_DRV_WATCHDOG_H_
#define INC_DRIVER_DRV_WATCHDOG_H_

//*********************************************************************
/**
 * \brief Initialize the watchdog timer
 */
//*********************************************************************
void watchdog_init(void);

//*********************************************************************
/**
 * \brief Reset the watchdog timer
 */
//*********************************************************************
void kick(void);

#endif /* INC_DRIVER_DRV_WATCHDOG_H_ */
