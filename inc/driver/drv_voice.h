//******************************************************************
/*! \file drv_voice.h
 *
 * \brief Kidde: Mongoose-fw - Declare voice driver functions
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_VOICE_DRV_H_
#define INC_VOICE_DRV_H_

#include "out_voice.h"

//*********************************************************************
/**
 * \brief Configure pins related to the voice chip
 * \note The following pins should be configured:
 *   - VOICE_EN
 *     - Digital output
 *     - Output low
 *   - VOICE_CLOCK
 *     - Digital output
 *     - Output low
 *   - VOICE_DATA
 *     - Digital output
 *     - Output low
 */
//*********************************************************************
void voice_init(void);

//*********************************************************************
/**
 * \brief Drive VOICE_EN high
 */
//*********************************************************************
void voice_pwr_on(void);

//*********************************************************************
/**
 * \brief Drive VOICE_EN low
 */
//*********************************************************************
void voice_pwr_off(void);

//*********************************************************************
/**
 * \brief Drive VOICE_CLOCK high
 */
//*********************************************************************
void voice_clk_hi(void);

//*********************************************************************
/**
 * \brief Drive VOICE_CLOCK low
 */
//*********************************************************************
void voice_clk_lo(void);

//*********************************************************************
/**
 * \brief Drive VOICE_DATA high
 */
//*********************************************************************
void voice_data_hi(void);

//*********************************************************************
/**
 * \brief Drive VOICE_DATA low
 */
//*********************************************************************
void voice_data_lo(void);

//*********************************************************************
/**
 * \brief Get message byte for the voice
 * \param voice The voice to send
 * \returns The message byte for the voice
 */
//*********************************************************************
uint8_t get_voice_data(voice_message voice);

//*********************************************************************
/**
 * \brief Get the time, in ms, between enabling the voice chip and sending a
 *        voice message
 * \returns The time between enabling the voice chip and sending a voice message
 */
//*********************************************************************
uint16_t get_voice_startup_time(void);

//*********************************************************************
/**
 * \brief Get the duration, in ms, of the voice message
 * \returns The duration, in ms, of the voice message
 */
//*********************************************************************
uint16_t get_voice_playback_time(voice_message voice);

#endif /* INC_VOICE_DRV_H_ */
