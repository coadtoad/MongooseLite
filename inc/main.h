//******************************************************************
/*! \file main.h
 *
 * \brief Kidde: Mongoose-fw - The main header file for mongoose-fw
 *
 * \author Justin Henry
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

#include <stdint.h>

//*********************************************************************
/**
 * \brief Start of main loop
 *
 * \ingroup
 *
 */
//*********************************************************************
void main_loop(void);

//*********************************************************************
/**
 * \brief Causes soft reset
 *
 * Force a soft reset on the device to reset values and get out of stuck conditions
 *
 * \ingroup
 *
 */
//*********************************************************************
void force_soft_reset(void);

//*********************************************************************
/**
 * \brief Stops external interrupts from occuring
 *
 * Disables interrupts from external events or sources
 *
 * \ingroup
 *
 */
//*********************************************************************
void disable_external_interrupts(void);

/**
 * \brief Returns the event that caused the reset to occur.
 *
 * \return The reset event.
 */
uint8_t get_reset_event(void);

/**
 * \brief Causes the controller to enter standby mode.
 * \note This is the lowest power mode.
 *
 */
void enter_standby_mode(void);

/**
 * \brief Causes the controller to enter stop mode.
 * \note This is medium power.
 *
 */
void enter_stop_mode(void);

/**
 * \brief Causes the controller to enter sleep mode.
 * \note This mode is the highest power sleep mode but
 *      most peripherals can be run.
 *
 */
void enter_sleep_mode(void);

void main_send_byte(uint8_t byte);
void debug_pulse(unsigned);

#endif
