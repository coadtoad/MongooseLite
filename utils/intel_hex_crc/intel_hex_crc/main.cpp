#include "cintelhex.h"
#include <Windows.h>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <memoryapi.h>
#include <string>

#define CRC_POLYNOMIAL 0x04C11DB7
#define CRC_INIT       0xFFFFFFFF

constexpr uint32_t sw_crc32(uint8_t const *const begin, uint8_t const *const end)
{
    uint32_t crc = CRC_INIT;
    for (uint8_t const *b = begin; b < end; ++b)
    {
        crc ^= *b << 24;
        for (int j = 0; j < 8; ++j)
        {
            bool const msb_set = crc & 0x80000000;
            crc <<= 1;
            if (msb_set)
            {
                crc ^= CRC_POLYNOMIAL;
            }
        }
    }
    return crc;
}

constexpr size_t address_space_size = 4ull * 1024 * 1024 * 1024;

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        std::cout << "Usage: intel_hex_crc.exe <File> <Flash Start Address> <CRC Address>\n";
        std::cout << "       <File> - Path to the file to set the CRC of\n";
        std::cout << "       <Flash Start Address> - Hex address of the start of flash\n";
        std::cout << "       <CRC Address> - Hex address of the start of the CRC\n";
        std::cout << "\n";
        std::cout << "This calculates a CRC from <Flash Start Address> to <CRC Address>\n";
        std::cout << "The 32-bit CRC is stored in big-endian beginning at <CRC_Address>\n";
        std::cout << "The input file must have lines that cover all four CRC bytes\n";
        std::cout << "<File> is modified in place\n";
        return 0;
    }

    std::uint32_t const flash_base  = std::strtoul(argv[2], nullptr, 16);
    std::uint32_t const crc_address = std::strtoul(argv[3], nullptr, 16);

    std::cout << "<File>=" << argv[1] << '\n';
    std::cout << "<Flash Start Address>=" << argv[2] << '\n';
    std::cout << "<CRC Address>=" << argv[3] << '\n';

    // Load file into RAM
    std::ifstream hex{argv[1]};
    std::string content(std::istreambuf_iterator<char>{hex}, {});
    hex.close();
    // Parse Intel HEX file
    auto rs = ihex_rs_from_mem(content.data(), content.size());

    // Allocate and init 4GB of RAM to 0xFF
    uint8_t *address_space =
        reinterpret_cast<uint8_t *>(VirtualAlloc(NULL, address_space_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE));
    if (!address_space)
    {
        std::cerr << "Failed to allocate buffer\n";
    }
    std::fill(address_space, address_space + address_space_size, static_cast<uint8_t>(0xff));

    // Update address_space with all bytes stored in the HEX file
    uint_t i = 0;
    ihex_record_t *record;
    uint32_t crc_offsets[sizeof(uint32_t)]       = {0};
    ihex_record_t *crc_records[sizeof(uint32_t)] = {0};
    uint32_t offset;
    do
    {
        auto err = ihex_rs_iterate_data(rs, &i, &record, &offset);
        if (err || !record)
        {
            break;
        }
        err = ihex_check_record(record);
        if (err)
        {
            std::cerr << "Checksum failed\n";
            return 1;
        }

        switch (record->ihr_type)
        {
        case IHEX_DATA:
            for (size_t i = 0; i < sizeof(uint32_t); ++i)
            {
                if (offset + record->ihr_address <= crc_address + i &&
                    crc_address + i < offset + record->ihr_address + record->ihr_length)
                {
                    // Track all records that have a piece of the CRC in them
                    crc_offsets[i] = offset;
                    crc_records[i] = record;
                }
            }
            std::copy(record->ihr_data, record->ihr_data + record->ihr_length,
                      address_space + offset + record->ihr_address);
            break;
        case IHEX_EOF:
            std::cout << "IHEX_EOF\n";
            break;
        case IHEX_ESA:
            std::cout << "IHEX_ESA\n";
            break;
        case IHEX_SSA:
            std::cout << "IHEX_SSA\n";
            break;
        case IHEX_ELA:
            std::cout << "IHEX_ELA\n";
            break;
        case IHEX_SLA:
            std::cout << "IHEX_SLA\n";
            break;
        }
    } while (i > 0);

    // Calculate the correct CRC
    auto const crc = sw_crc32(address_space + flash_base, address_space + crc_address);

    std::cout << std::setfill('0') << std::right << "***Start:0x" << std::hex << std::setw(8) << flash_base
              << " Stop:0x" << std::hex << std::setw(8) << crc_address << " CRC:0x" << std::hex << std::setw(8) << crc
              << '\n';

    VirtualFree(address_space, 0, MEM_RELEASE);

    // Update the records with the CRC
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
    {
        if (!crc_records[i])
        {
            std::cerr << "No record corresponding to byte " << i << " of the CRC\n";
            return 1;
        }
        auto &record                = *crc_records[i];
        // Copy CRC into record in big-endian
        size_t const data_index     = crc_address + i - (crc_offsets[i] + record.ihr_address);
        record.ihr_data[data_index] = static_cast<uint8_t>(crc >> ((sizeof(uint32_t) - (i + 1)) * 8));
        // Update the record checksum
        uint8_t checksum =
            record.ihr_length + (record.ihr_address >> 8) + static_cast<uint8_t>(record.ihr_address) + record.ihr_type;
        for (size_t i = 0; i < record.ihr_length; ++i)
        {
            checksum += record.ihr_data[i];
        }
        record.ihr_checksum = ~checksum + 1;
    }

    std::ofstream output{argv[1], std::ios_base::out | std::ios_base::trunc};
    // Write the updated HEX file back out to the disk
    output << std::setfill('0') << std::right << std::uppercase;
    for (size_t i = 0; i < rs->ihrs_count; ++i)
    {
        auto &record = rs->ihrs_records[i];
        output << ':';
        output << std::setw(2) << std::hex << static_cast<int>(record.ihr_length & 0xff);
        output << std::setw(4) << std::hex << static_cast<int>(record.ihr_address & 0xffff);
        output << std::setw(2) << std::hex << static_cast<int>(record.ihr_type & 0xff);
        for (size_t i = 0; i < record.ihr_length; ++i)
        {
            output << std::setw(2) << std::hex << static_cast<int>(record.ihr_data[i] & 0xff);
        }
        output << std::setw(2) << std::hex << static_cast<int>(record.ihr_checksum & 0xff);
        output << '\n';
    }

    ihex_rs_free(rs);

    return 0;
}
