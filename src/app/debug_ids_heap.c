//******************************************************************
/*! \file debug_ids_heap.c
 *
 * \brief Kidde: Mongoose-fw - Define debug heap information
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "debug_ids_heap.h"
#include "app_serial.h"
#include "state.h"

//*********************************************************************
/**
 * \brief Series of message strings
 */
//*********************************************************************
static char *message_strings[] = {
    /*00 msg_*/ "init",
    /*01 msg_*/ "continue",
    /*02 msg_*/ "begin_express",
    /*03 msg_*/ "start_express",
    /*04 msg_*/ "start_wait_express",
    /*05 msg_*/ "start_wait_express_voice",
    /*06 msg_*/ "start_express_voice",
    /*07 msg_*/ "end_express",
    /*08 msg_*/ "exit_express",
    /*09 msg_*/ "button_chirp",
    /*10 msg_*/ "button_complete",
    /*11 msg_*/ "turn_on",
    /*12 msg_*/ "turn_off",
    /*13 msg_*/ "vboost_on",
    /*14 msg_*/ "vboost_off",
    /*15 msg_*/ "ready",
    /*16 msg_*/ "voice",
    /*17 msg_*/ "get_value",
    /*18 msg_*/ "value",
    /*19 msg_*/ "set_value",
    /*20 msg_*/ "add_callback",
    /*21 msg_*/ "remove_callback",
    /*22 msg_*/ "set_flag",
    /*23 msg_*/ "enter_smoke_alarm",
    /*24 msg_*/ "exit_smoke_alarm",
    /*25 msg_*/ "enter_low_battery",
    /*26 msg_*/ "exit_low_battery",
    /*27 msg_*/ "enter_fatal_battery",
    /*28 msg_*/ "exit_fatal_battery",
    /*29 msg_*/ "enter_no_battery",
    /*30 msg_*/ "exit_no_battery",
    /*31 msg_*/ "enter_eol",
    /*32 msg_*/ "exit_eol",
    /*33 msg_*/ "enter_eol_fatal",
    /*34 msg_*/ "exit_eol_fatal",
    /*35 msg_*/ "battery_volt",
    /*36 msg_*/ "button_down",
    /*37 msg_*/ "button_stuck",
    /*38 msg_*/ "enter_co_alarm",
    /*39 msg_*/ "enter_co_alarm_conserve",
    /*40 msg_*/ "exit_co_alarm",
    /*41 msg_*/ "fault_co_sensor_short",
    /*42 msg_*/ "fault_co_sensor_open",
    /*43 msg_*/ "fault_co_clear",
    /*44 msg_*/ "cal_begin",
    /*45 msg_*/ "cal_pass",
    /*46 msg_*/ "cal_fail",
    /*47 msg_*/ "ptt",
    /*48 msg_*/ "record_history",
    /*49 msg_*/ "update_primary_db",
    /*50 msg_*/ "update_backup_db",
    /*51 msg_*/ "co_high",
    /*52 msg_*/ "ac_on",
    /*53 msg_*/ "ac_off",
    /*54 msg_*/ "entered_hush",
    /*55 msg_*/ "begin_drive_smoke",
    /*56 msg_*/ "end_drive_smoke",
    /*57 msg_*/ "begin_drive_co",
    /*58 msg_*/ "end_drive_co",
    /*59 msg_*/ "charge_blue_led",
    /*60 msg_*/ "fault_fatal",
    /*61 msg_*/ "expression_timeout",
    /*62 msg_*/ "smoke_hush_timeout",
    /*64 msg_*/ "battery_hush_timeout",
    /*65 msg_*/ "eol_hush_timeout",
    /*66 msg_*/ "reset_flags",
    /*67 msg_*/ "exit_sampling",
    /*68 msg_*/ "strobe_sync",
    /*69 msg_*/ "cancel_strobe_light",
    /*70 msg_*/ "interconnect_message",
    /*71 msg_*/ "led_fade",
    /*72 msg_*/ "locate_timeout",
    /*73 msg_*/ "amp_resend",
    /*74 msg_*/ "voice_multiple_button_tones",
    /*75 msg_*/ "override",
    /*76 msg_*/ "firmware_crc",
    /*77 msg_*/ "start_push_to_test",
    /*78 msg_*/ "read_primary_entry",
    /*79 msg_*/ "read_backup_entry",
    /*80 msg_*/ "dump_flash_contents",
    /*81 msg_*/ "inline_smoke_cal",
    /*82 msg_*/ "sample"};

//*********************************************************************
/**
 * \brief Series of task ID strings
 * \note IMPORTANT must match task_list.c, task_id.h, debug_ids_heap.c
 */
//*********************************************************************
static char *task_id_strings[] = {
    /*00 task_*/ "supervisor",
    /*01 task_*/ "database",
    /*02 task_*/ "key_value_store",
    /*03 task_*/ "voice_output",
    /*04 task_*/ "state",
    /*05 task_*/ "photo",
    /*06 task_*/ "co",
    /*07 task_*/ "battery",
    /*08 task_*/ "day_count",
    /*09 task_*/ "button",
    /*10 task_*/ "events",
    /*11 task_*/ "serial",
    /*12 task_*/ "interconnect",
    /*13 task_*/ "cal_0_ppm",
    /*14 task_*/ "cal_150_ppm",
    /*15 task_*/ "cal_400_ppm",
    /*16 task_*/ "cal_circuit",
    /*17 task_*/ "cal_2_point",
    /*18 task_*/ "cal_lic",
    /*19 task_*/ "ac_input",
    /*20 task_*/ "ptt",
    /*21 task_*/ "battery_cal",
    /*22 task_*/ "blue_charge_pump",
    /*23 task_*/ "clean_air_value",
    /*24 task_*/ "cal_doe",
    /*25 task_*/ "vboost",
    /*26 task_*/ "cal_inline_smoke",
    /*27 task_*/ "expression"};

void message_debug_out(task_id const task, message_id const message, uint16_t data)
{
    serial_send_string_debug("task - task_,", serial_enable_heap);
    serial_send_string_debug(task_id_strings[task], serial_enable_heap);

    serial_send_string_debug(", - message - msg_,", serial_enable_heap);
    serial_send_string_debug(message_strings[message], serial_enable_heap);

    serial_send_int_ex_debug(", - data - ,", data, 16, "\r", serial_enable_heap);
}

void serial_send_msg_debug(message_id const message, Serial_output_flags_type_e output_flag)
{
    serial_send_string_debug(message_strings[message], output_flag);
}
