//******************************************************************
/*! \file co_common.c
 *
 * \brief Kidde: Mongoose-fw - Define the common co functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "co_common.h"
#include "app_database.h"
#include "app_events.h"
#include "app_supervisor.h"
#include "drv_co.h"
#include "drv_serial.h"
#include "drv_timer.h"
#include "in_co.h"
#include "key_value_store.h"
#include "message.h"
#include "task_list.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

//*********************************************************************
/**
 * \brief Fault status
 */
//*********************************************************************

// This is the number of samples that will be averaged during CO measurement.
#define SENSOR_SHORT_LIMIT               (uint16_t)250

// 400 counts (Output is Off the Rail)
#define VMEAS_CIRCUIT_STABILIZE          (uint16_t)400

// Annual age correction constants
#define ANNUAL_PERCENT_INCREASE          2.5
#define PERCENT_PER_QTR                  ((ANNUAL_PERCENT_INCREASE * 1000) / 4)
#define DAYS_IN_YEAR                     (uint16_t)365
#define QTRS_IN_YEAR                     (uint16_t)4

// Measurement task is called every 2 seconds.  All timer functions should
// be based on this interval in case it is changed in the future.
#define CO_TASK_EXECUTION_INTERVAL       (uint32_t)5

#define CO_SHORTS_TIME                   20

//*********************************************************************
/**
 * \brief Number of consecutive CO sensor short conditions before entering CO sensor short fault
 */
//*********************************************************************
#define CO_SHORTS_THRESHOLD              CO_SHORTS_TIME / CO_TASK_EXECUTION_INTERVAL

//*********************************************************************
/**
 * \brief CO PPM threshold required to enter CO peak alarm memory
 */
//*********************************************************************
#define PEAK_THRESHOLD_PPM               (uint8_t)100

//*********************************************************************
/**
 * \brief CO accumulator threshold required to enter CO alarm
 */
//*********************************************************************
#define AL_ALARM_THRESHOLD               (uint16_t)8260

//*********************************************************************
/**
 * \brief Maximum limit of the CO accumulator after the alarm threshold has been passed
 * \note The threshold limit constant sets the accumulator limit.  The accumulator limit is not set to the alarm
 * threshold because algorithm isn't allowed to decay quickly enough.  The limit is set by subtracting the PPM value or
 * a multiple thereof where the decay should begin, from the threshold.
 */
//*********************************************************************
#define AL_ALARM_THRESHOLD_LIMIT         (uint16_t)8108

//*********************************************************************
/**
 * \brief The amount of accumulation to subtract from the alarm_sum after a button press / soft reset during CO alarm
 */
//*********************************************************************
#define CO_THOLD_REDUCTION_RESET         (uint16_t)1

#define AL_ACCUMULATOR_MINIMUM_THRESHOLD (uint16_t)45
#define AL_ACCUMULATOR_COMP_THRESHOLD    (uint16_t)38
#define AL_ACCUMULATOR_DECAY_CONSTANT    (uint16_t)13
#define AL_ACCUMULATOR_DECAY_THRESHOLD   (uint16_t)97

#define AL_THRESHOLD_SLOPE_MULTIPLE      (uint16_t)4
#define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE (uint16_t)8
#define AL_THRESHOLD_SLOPE_THRESHOLD     (uint16_t)228

// These constants are used with the Alarm Curve calculations.
// A value of 1.0 is equal to 0x10000, so these hex values are fractional
// constants.
#define AL_ALARM_SUM_CONSTANT            (uint16_t)0xb80b // == 0.7189
#define AL_PREALARM_SUM_CONSTANT         (uint16_t)0x49fb // == 0.289

//*********************************************************************
/**
 * \brief Variables used for CO measurement
 */
//*********************************************************************
static uint16_t co_wppm;

//*********************************************************************
/**
 * \brief Count number of consecutive CO sensor shorts
 */
//*********************************************************************
static uint8_t co_short_counter;

//*********************************************************************
/**
 * \brief PPM level of CO when manually overriding the CO measurements via serial
 */
//*********************************************************************
static uint16_t manual_co;

//*********************************************************************
/**
 * \brief Boolean to check if circuit is stable
 */
//*********************************************************************
static bool circuit_stable = false;

/** @todo do they need to be global to this file? */
static uint16_t alarm_past_sum __attribute__((section(".bss.noinit")));
static uint16_t alarm_sum __attribute__((section(".bss.noinit")));
static uint16_t alarm_interim_sum __attribute__((section(".bss.noinit")));

void accumulator_init(void)
{
    // first initialization of accumulator values
    alarm_past_sum    = 0;
    alarm_interim_sum = 0;
    alarm_sum         = 0;
}

void lower_co_accumulator(void)
{
    // Lower the alarm_sum below the alarm threshold to prevent immediate alarm after reset
    if (alarm_sum >= AL_ALARM_THRESHOLD_LIMIT)
    {
        alarm_sum = AL_ALARM_THRESHOLD_LIMIT - CO_THOLD_REDUCTION_RESET;
    }
}

void set_manual_co(uint16_t co_value)
{
    manual_co = co_value;
}

bool co_make_measurement(uint16_t *const counts)
{
    supervisor_counter_increase(counter_co);

    if (co_sample(counts))
    {
        schedule_message(0, task_key_value_store, msg_set_value, (uint16_t)fault_status << 8 | key_co_status);
        schedule_message(0, task_key_value_store, msg_set_value, (*counts & 0x00FF) << 8 | key_co_volt_lsb);
        schedule_message(0, task_key_value_store, msg_set_value, (*counts & 0xFF00) | key_co_volt_msb);
        return true;
    }
    else
    {
        return false;
    }
}

void co_short_detect(uint16_t co_raw_counts)
{
    static uint16_t co_prev_raw_co_count;

    if (!(fault_status & fault_short))
    {
        // initialize here to avoid error on power up in high CO
        if (co_prev_raw_co_count == 0)
        {
            co_prev_raw_co_count = co_raw_counts;
        }

        // If the rate of rise is greater than the limit, start short processing.
        if (co_raw_counts > co_prev_raw_co_count && co_raw_counts - co_prev_raw_co_count > SENSOR_SHORT_LIMIT)
        {
            // increase short counter and set fault in progress if needed
            if (CO_SHORTS_THRESHOLD < ++co_short_counter)
            {
                // CO sensor short fault active
                schedule_message(0, task_state, msg_fault_co_sensor_short, 0);
                fault_status |= fault_short;
                fault_status &= ~fault_pending;
                // Record the fault in diagnostic history
                schedule_message(0, task_events, msg_record_history, event_fault_co_sensor_short);
                // Update the sensor short count in the db.
                uint16_t short_count = db_get_value16(db_counter_sensor_short_msb, db_counter_sensor_short_lsb);
                db_set_value16(db_counter_sensor_short_msb, db_counter_sensor_short_lsb, ++short_count);
            }
            else
            {
                fault_status |= fault_pending;
            }
        }
        else
        {
            // Save last raw reading for future fault detection
            co_prev_raw_co_count = co_raw_counts;
            co_short_counter     = 0;
        }
        schedule_message(0, task_key_value_store, msg_set_value, (uint16_t)fault_status << 8 | key_co_status);
    }
}

void co_meas_stable(uint16_t co_raw_counts)
{
    // If circuit not stable active check for sensor recovered from high voltage measurement
    if (!circuit_stable)
    {
        if (VMEAS_CIRCUIT_STABILIZE > co_raw_counts)
        {
            circuit_stable = true;
        }
    }
}

uint16_t co_comp_age_correct_PPM(uint16_t ppm)
{
    uint32_t const days          = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);

    // Calc. number of Qtrs of Life
    uint32_t const num_qtrs      = (days * 100) / ((uint32_t)DAYS_IN_YEAR * 100 / QTRS_IN_YEAR);

    uint16_t const ppm_corrected = ppm + (((uint32_t)ppm * (num_qtrs * (uint32_t)PERCENT_PER_QTR)) / 100000);

    return ppm_corrected;
}

uint16_t co_calc_PPM(uint16_t raw_counts)
{
    if (fault_status & ANY_FAULT)
    {
        return 0;
    }

    // if manual CO is enabled
    if (manual_co)
    {
        co_wppm = manual_co;
        return co_wppm;
    }

    uint16_t const offset = db_get_value16(db_co_offset_msb, db_co_offset_lsb);

    // check for underflow first
    if (raw_counts < offset)
    {
        co_wppm = 0;
    }
    else
    {
        uint16_t const scale = db_get_value16(db_co_scale_msb, db_co_scale_lsb);
        // subtract the offset and multipy by the scale.
        co_wppm              = raw_counts - offset;

        // Calculate ppm using scale (slope)
        co_wppm              = (uint16_t)(((uint32_t)co_wppm * scale) >> 8);

        // Correct the ppm value by the age correction.
        co_wppm              = co_comp_age_correct_PPM(co_wppm);
    }

    schedule_message(0, task_key_value_store, msg_set_value, (co_wppm & 0x00FF) << 8 | key_co_lsb);
    schedule_message(0, task_key_value_store, msg_set_value, (co_wppm & 0xFF00) | key_co_msb);

    return co_wppm;
}

bool co_alarm(void)
{
    if (fault_status & ANY_FAULT)
    {
        return 0;
    }

    // Store the last accumulation value
    alarm_past_sum       = alarm_interim_sum;

    //
    //  Multiply previous accumulation value by fraction.
    //
    alarm_interim_sum    = alarm_sum;

    uint32_t alarm_temp1 = (uint32_t)alarm_interim_sum * AL_ALARM_SUM_CONSTANT;
    uint16_t local       = alarm_temp1 >> 16;

    //
    //  If the PPM level is not greater than the accumulator decay constant,
    //  do not perform the curve calculation as the accumulator will not decay
    //  after it has reached a certain value.
    //
    if (co_wppm >= AL_ACCUMULATOR_MINIMUM_THRESHOLD)
    {
        alarm_temp1 = (uint32_t)alarm_past_sum * AL_PREALARM_SUM_CONSTANT;
        alarm_temp1 >>= 16;
        alarm_sum = local + alarm_temp1;
    }

    alarm_sum += co_wppm;
    if (AL_ACCUMULATOR_DECAY_THRESHOLD < co_wppm)
    {
        alarm_sum += AL_ACCUMULATOR_DECAY_CONSTANT;
    }

    uint16_t decay_value = AL_ACCUMULATOR_COMP_THRESHOLD;

    /** @todo aasdf */
    // Subtract AL_ACCUMULATOR_COMP_THRESHOLD
    bool alarm           = false;
    if (alarm_sum > AL_ACCUMULATOR_COMP_THRESHOLD)
    {
        alarm_sum -= decay_value;

        // check for alarm condition
        if (alarm_sum > AL_ALARM_THRESHOLD)
        {
            alarm_sum = AL_ALARM_THRESHOLD_LIMIT;
            alarm     = true;
        }
        else
        {
            uint16_t const multiplier =
                co_wppm > AL_THRESHOLD_SLOPE_THRESHOLD ? AL_THRESHOLD_SLOPE_HIGH_MULTIPLE : AL_THRESHOLD_SLOPE_MULTIPLE;
            alarm = AL_ALARM_THRESHOLD - alarm_sum < co_wppm * multiplier;
            if (alarm)
            {
                alarm_sum = AL_ALARM_THRESHOLD_LIMIT;
            }
        }
    }
    else
    {
        alarm_sum         = 0;
        alarm_interim_sum = 0;
        alarm             = false;
    }

    schedule_message(0, task_key_value_store, msg_set_value, (alarm_sum & 0x00FF) << 8 | key_accumulator_lsb);
    schedule_message(0, task_key_value_store, msg_set_value, (alarm_sum & 0xFF00) | key_accumulator_msb);

    return alarm;
}

void co_peak_detect(uint16_t const ppm)
{
    // if peak co ppm is seen, but co circuit short is absent
    // enter co peak ppm memory immediately
    if (ppm > PEAK_THRESHOLD_PPM && !co_short_counter)
    {
        // Set peak memory, if not in PTT, if power up stabilization is
        // complete and calibration is complete.
        schedule_message(0, task_state, msg_co_high, ppm);
    }
}
