//******************************************************************
/*! \file message.c
 *
 * \brief Kidde: Mongoose-fw - Define the message functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "message.h"
#include "message_heap.h"

//*********************************************************************
/**
 * \brief schedule a message to be sent
 * \param time_ms time in ms for message to occur
 * \param task task to be called, task id
 * \param message message to be sent, message id
 * \param data any data sent with the message
 */
//*********************************************************************
void schedule_message(uint32_t const time_ms, task_id const task, message_id const message, uint16_t const data)
{
    push_left(time_ms, task, message, data);
}

//*********************************************************************
/**
 * \brief schedule a message to be sent at a certain time
 * \param desired_period_ms time in ms when message will occur
 * \param task task to be called, task id
 * \param message message to be sent, message id
 * \param data any data sent with the message
 */
//*********************************************************************
void schedule_timed_message(uint32_t const desired_period_ms, task_id const task, message_id const message,
                            uint16_t const data)
{
    add_timed(desired_period_ms, task, message, data);
}

//*********************************************************************
/**
 * \brief unschedule a message that was scheduled to occur
 * \param task task to be unscheduled, task id
 */
//*********************************************************************
void unschedule_messages_t(task_id const task)
{
    delete_left_t(task);
    delete_timed_t(task);
}

//*********************************************************************
/**
 * \brief unschedule a message that was scheduled to occur
 * \param task task to be unscheduled, task id
 * \param message message to be unscheduled, message id
 */
//*********************************************************************
void unschedule_messages_tm(task_id const task, message_id const message)
{
    delete_left_tm(task, message);
    delete_timed_tm(task, message);
}

//*********************************************************************
/**
 * \brief unschedule a message, with data, that was scheduled to occur
 * \param task task to be unscheduled, task id
 * \param message message to be unscheduled, message id
 * \param data data to be unscheduled and no longer sent
 */
//*********************************************************************
void unschedule_messages_tmd(task_id const task, message_id const message, uint16_t const data)
{
    delete_left_tmd(task, message, data);
    delete_timed_tmd(task, message, data);
}
