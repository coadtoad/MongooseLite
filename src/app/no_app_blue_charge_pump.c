//******************************************************************
/*! \file no_app_blue_charge_pump.c
 *
 * \brief Kidde: Mongoose-fw - Declare when there is no blue charge pump function
 *
 * \author unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_blue_charge_pump.h"
#include "message.h"

void blue_charge_pump_task(message_id const message, uint16_t const data)
{
    (void)message;
    (void)data;
}
