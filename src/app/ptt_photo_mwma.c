//******************************************************************
/*! \file ptt_photo_mwma.c
 *
 * \brief Kidde: Mongoose-fw - Implement the PTT photo functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "drv_events.h"
#include "drv_photo.h"
#include "drv_serial.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_mwma.h"
#include "ptt_photo.h"
#include "task_list.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief Boolean to determine which state to avoid
 */
//*********************************************************************
static bool avoid = false;

//*********************************************************************
/**
 * \brief Boolean to determine which state to run
 */
//*********************************************************************
static bool run   = false;

//*********************************************************************
/**
 * \brief Recreate photo PTT alarm with pass/fail grade
 */
//*********************************************************************
static bool photo_push_to_test(void)
{
    //
    // simulate alarm currents and send a pass/fail grade to ptt module
    //

    if (!begin_photo_read())
    {
        return false;
    }
    // wait microseconds for the circuit to stabilize
    timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

    // take a dark measurement
    uint8_t comp_f;
    read_dark(IR_F, &comp_f);
    // take a light measurement
    uint8_t const ptt_ir_f = read_light(IR_F, comp_f, db_get_value(db_ptt_current_ir_f));

    // photo power, the IR DAC, and the CAV DAC are all disabled
    select_infrared();
    end_photo_read();

    // take a clean air reading
    uint8_t const clean_air_count = db_get_value(db_ir_f_clean_air);
    uint8_t const delta           = (ptt_ir_f > clean_air_count) ? (ptt_ir_f - clean_air_count) : 0;

    // Send IR-F reading during PTT
    char buffer[]                 = "\rIR-F:   \r";
    uint8_to_hex(buffer + 7, ptt_ir_f);
    serial_send_string_debug(buffer, serial_enable_ptt);

    // if delta is greater than 5 counts, passed PTT
    return delta >= IR_F_PTT_TH;
}

void ptt_photo_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        avoid = false;
        schedule_message(0, task_key_value_store, msg_add_callback,
                         ((uint16_t)task_ptt_photo << 8) | key_avoidance_flags);
        break;
    case msg_value:
        if ((key_name)data == key_avoidance_flags)
        {
            avoid = data >> 8;
            if (!avoid && run)
            {
                schedule_message(0, task_ptt_photo, msg_ptt, 0);
            }
        }
        break;
    case msg_ptt:
        if (avoid)
        {
            run = true;
            break;
        }
        run = false;
        if (photo_push_to_test())
        {
            serial_send_string_debug("Photo test success\r", serial_enable_ptt);
            schedule_message(0, task_state, msg_ptt, ((uint16_t)task_ptt_photo << 8) | PTT_PASSED);
        }
        else
        {
            serial_send_string_debug("Photo test failure\r", serial_enable_ptt);
            schedule_message(0, task_state, msg_ptt, ((uint16_t)task_ptt_photo << 8) | PTT_FAILED);
        }
        break;
    default:
        break;
    }
}
