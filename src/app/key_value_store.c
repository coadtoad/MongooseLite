//******************************************************************
/*! \file key_value_store.c
 *
 * \brief Kidde: Mongoose-fw - Define key value enums
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

// library includes
#include <stddef.h>
#include <string.h>

// local includes
#include "key_value_store.h"
#include "message.h"
#include "task_id.h"

#define KVS_SET_FLAG_MASK 0x07

#define ID_KEY_LOC        0
#define ID_KEY_BIT_LOC    8
#define VALUE_LOC         11

//*********************************************************************
/**
 * \brief callback entry, key and task IDs
 */
//*********************************************************************
typedef struct
{
    uint8_t key_id;
    uint8_t task_id;
} callback_entry;

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
static uint8_t values[key_name_count] = {0};

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
static callback_entry callbacks[key_name_count * 2];

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
static uint8_t callbacks_size     = key_name_count * 2;

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
static uint8_t callbacks_mem_size = key_name_count * 2 * sizeof(callback_entry);

//*********************************************************************
/**
 * \brief pack value to send msg_set_flag
 */
//*********************************************************************
uint16_t pack_kvs_set_flag_data(key_name const key_id, uint8_t const bit, bool value)
{
    return (key_id << ID_KEY_LOC) | ((bit & KVS_SET_FLAG_MASK) << ID_KEY_BIT_LOC) | (value << VALUE_LOC);
}

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
/**
 * @brief schedule message for callbacks
 */
static void call_callbacks(uint8_t const key_id)
{
    for (uint8_t i = 0; i < callbacks_size; ++i)
    {
        if (callbacks[i].key_id == key_id)
        {
            //schedule_message(0, (task_id)callbacks[i].task_id, msg_value, ((uint16_t)values[key_id]) << 8 | key_id);
        }
    }
}

//*********************************************************************
/**
 * \brief sets a flag in key value store.
 * key_id is the key value set
 * bit is the bit within the key that is et.
 */
//*********************************************************************
void set_key_value_store_flag(key_name const key_id, uint8_t const bit, bool const set)
{
    values[key_id] &= ~(1 << bit);
    values[key_id] |= set << bit;

    call_callbacks(key_id);
}

//*********************************************************************
/**
 * \brief returns flag value in key value store
 * \returns true or false of bit @ key_id-->bit
 */
//*********************************************************************
bool get_key_value_store_flag(key_name const key_id, uint8_t const bit)
{
    return values[key_id] & (1 << bit);
}

//*********************************************************************
/**
 * \brief Check next available index key and task ID
 * \returns loops until IDs aren't value 0xFF
 */
//*********************************************************************
static uint8_t next_available_index()
{
    uint8_t i = 0;
    while (callbacks[i].key_id != 0xFF && callbacks[i].task_id != 0xFF)
    {
        i++;
    }
    return i;
}

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
/**
 * @brief insert callback at given index
 */
static void insert_callback(uint8_t const key_id, uint8_t const task_id)
{
    uint8_t index            = next_available_index();
    callbacks[index].key_id  = key_id;
    callbacks[index].task_id = task_id;
}

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
/**
 * @brief
 */
static void remove_callback(uint8_t const key_id, uint8_t const task_id)
{
    for (uint8_t i = 0; i < callbacks_size; ++i)
    {
        if ((callbacks[i].task_id == task_id) && (callbacks[i].key_id == key_id))
        {
            callbacks[i].key_id  = 0xFF;
            callbacks[i].task_id = 0xFF;
            return;
        }
    }
}

//*********************************************************************
/**
 * \brief "brief sentence about function/enum/static/etc"
 */
//*********************************************************************
void key_value_store_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        memset(values, 0, sizeof(values));
        memset(callbacks, 0xFF, callbacks_mem_size);
        break;
    case msg_get_value:
    {
//        task_id const task   = (task_id)(uint8_t)(data >> 8);
//        key_name const index = (key_name)(uint8_t)data;
        //schedule_message(0, task, msg_value, (values[index] << 8) | index);
        break;
    }

    case msg_set_value:
    {
        key_name const index = (key_name)(uint8_t)data;
        uint8_t const value  = (uint8_t)(data >> 8);
        values[index]        = value;
        call_callbacks(index);
        break;
    }

    case msg_add_callback:
    {
        uint8_t const index = (uint8_t)data;
        task_id const task  = (task_id)(uint8_t)(data >> 8);
        insert_callback(index, task);
        break;
    }

    case msg_remove_callback:
    {
        uint8_t const index = (uint8_t)data;
        task_id const task  = (task_id)(uint8_t)(data >> 8);
        remove_callback(index, task);
        break;
    }

    case msg_set_flag:
    {
        key_name const index = (key_name)(uint8_t)data;
        uint8_t const bit    = (uint8_t)((data >> ID_KEY_BIT_LOC) & KVS_SET_FLAG_MASK);
        uint8_t const value  = (data >> VALUE_LOC) & 0x01;
        values[index] &= ~(1 << bit);
        values[index] |= value << bit;
        call_callbacks(index);
        break;
    }
    default:
        break;
    }
}
