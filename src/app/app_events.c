//******************************************************************
/*! \file app_events.c
 *
 * \brief Kidde: Mongoose-fw - Declare the app event functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup headers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_events.h"
#include "app_database.h"
#include "drv_database.h"
#include "drv_events.h"
#include "message.h"
#include "state.h"
#include "task_list.h"

#define EVENT_RETAIN_COUNT 64
// the +1 accounts for the new event that is causing the flash erase
#define BUFFER_SIZE        (EVENT_RETAIN_COUNT + 1)

//*********************************************************************
/**
 * \brief flash action event and status enum
 */
//*********************************************************************
typedef enum
{
    pack_events,
    erase_flash,
    write_flash,
    verify_write
} flash_action;

//*********************************************************************
/**
 * \brief Declare a 2K RAM buffer.
 */
//*********************************************************************
static unsigned general_purpose_ram[512];

//*********************************************************************
/**
 * \brief The event buffer.
 */
//*********************************************************************
static event_entry *event_buffer   = (event_entry *)general_purpose_ram;

//*********************************************************************
/**
 * \brief The latest day that has an event logged
 */
//*********************************************************************
static uint16_t latest_day         = 0;

//*********************************************************************
/**
 * \brief The next sequence number to be used
 */
//*********************************************************************
static uint8_t next_sequence       = 0;

//*********************************************************************
/**
 * \brief The next sequence number to be used
 */
//*********************************************************************
static uint16_t event_count        = 0;

//*********************************************************************
/**
 * \brief This will cause the event history to be erased if true.
 */
//*********************************************************************
static bool write_retained_entries = false;

//*********************************************************************
/**
 * \brief Updates the module variables for managing event creation.
 * \param read_events pointer to a function to read the event
 * \param max_events maximum number of events to read.
 */
//*********************************************************************
static inline uint16_t update_latest(int16_t (*read_events)(event_entry *const, size_t const, size_t const),
                                     uint16_t const max_events)
{
    bool adjusted = true;
    uint16_t count;
    // Try to read each event
    for (count = 0; count < max_events; ++count)
    {
        event_entry event;
        int16_t const read_count = read_events(&event, count, 1);
        if (read_count > 0 && event.event_id != 0xff)
        {
            // Update the latest day and sequence number if this is newer
            if (event.day == latest_day && event.sequence_number > next_sequence)
            {
                next_sequence = event.sequence_number;
            }
            else if (event.day > latest_day)
            {
                latest_day    = event.day;
                next_sequence = event.sequence_number;
            }
            adjusted = false;
        }
        else
        {
            // Either there was a read error or there are no more events
            if (read_count < 0)
            {
                // TODO: event read failed, how to react
            }
            break;
        }
    }
    // This currently has the information for the most recent event
    // It needs to be the next event number after that
    if (!adjusted)
    {
        ++next_sequence;
    }
    return count;
}

event_entry make_event(event_id const id)
{
    uint16_t const day = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);
    if (day > latest_day)
    {
        latest_day    = day;
        next_sequence = 0;
    }

    event_entry const event = {id, next_sequence++, day, 0};
    return event;
}

void events_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        latest_day    = 0;
        next_sequence = 0;
        event_count   = update_latest(read_events, max_events());
        break;

    case msg_record_history:
//        if (!get_flash_in_progress())
//        {
//            set_flash_in_progress(true);
//            // check if there is space in flash to write the next event
//            //schedule_message(0, task_events, msg_continue, (uint16_t)data << 8 | pack_events);
//        }
//        else
//        {
//            //schedule_message(100, task_events, msg_record_history, data);
//        }
        break;
    case msg_continue:
    {
        event_id const id         = (event_id)(uint8_t)(data >> 8);
        flash_action const action = (flash_action)(uint8_t)data;
        switch (action)
        {
        case pack_events:
            // if events are maxed out, save the latest event entries then record the new event
            if (event_count >= max_events())
            {
                read_events(event_buffer, event_count - EVENT_RETAIN_COUNT, EVENT_RETAIN_COUNT);
                event_buffer[EVENT_RETAIN_COUNT] = make_event(id);
                write_retained_entries           = true;
                ////schedule_message(0, task_events, msg_continue, (uint16_t)id << 8 | erase_flash);
            }
            else
            {
                // draft new event
                event_buffer[0] = make_event(id);
                ////schedule_message(0, task_events, msg_continue, (uint16_t)id << 8 | write_flash);
            }
            break;
        case erase_flash:
            if (begin_event_erase())
            {
                // write complete, verify the health of the flash
                ////schedule_message(0, task_events, msg_continue, (uint16_t)id << 8 | write_flash);
            }
            else
            {
                write_retained_entries = false;
                //set_flash_in_progress(false);
            }
            break;
        case write_flash:
        {
            uint16_t offset;
            uint16_t count;
            if (write_retained_entries)
            {
                offset      = 0;
                count       = BUFFER_SIZE;
                event_count = count;
            }
            else
            {
                count  = 1;
                offset = event_count;
                ++event_count;
            }

            // start writing the events
            if (write_events(event_buffer, offset, count) != -1)
            {
                // write complete, verify the health of the flash
                ////schedule_message(0, task_events, msg_continue, (uint16_t)id << 8 | verify_write);
            }
            else
            {
                write_retained_entries = false;
                //set_flash_in_progress(false);
            }
            break;
        }
        case verify_write:
        {
            uint16_t offset;
            uint16_t count;
            if (write_retained_entries)
            {
                offset = 0;
                count  = BUFFER_SIZE;
            }
            else
            {
                // Event count was incremented above. We want to verify the last event written.
                offset = event_count - 1;
                count  = 1;
            }

            if (begin_event_verify(event_buffer, offset, count))
            {
                // verify complete or errored out
                write_retained_entries = false;
                //set_flash_in_progress(false);
            }
            else
            {
                // Error verifying flash.
                write_retained_entries = false;
                //set_flash_in_progress(false);
            }
            break;
        }
        }
    default:
        break;
    }
    }
}

bool synchronous_event_write(event_id const id)
{
    // Initialize variables
    event_count     = update_latest(read_events, max_events());

    uint16_t offset = 0;
    uint16_t count  = 1; // minimum of 1 event to write

    // Event page is full
    if (event_count >= max_events())
    {
        // Save the lastest entries to rewrite after erasing flash
        read_events(event_buffer, event_count - EVENT_RETAIN_COUNT, EVENT_RETAIN_COUNT);
        // Add the new event to the buffer
        event_buffer[EVENT_RETAIN_COUNT] = make_event(id);

        // Erase the full page of flash
        if (!begin_event_erase())
        {
            // Erase failed
            return false;
        }

        count       = BUFFER_SIZE;
        event_count = count;
    }
    else
    {
        // Add the new event to the buffer
        event_buffer[0] = make_event(id);
        offset          = event_count;
        ++event_count;
    }

    // Write the event buffer to flash
    if (write_events(event_buffer, offset, count) == -1)
    {
        // Write failed
        return false;
    }

    // Event count was incremented above. We want to verify the last event written.
    if (offset == event_count)
    {
        offset = event_count - 1;
    }

    // Verify that the contents were written to flash
    return begin_event_verify(event_buffer, offset, count);
}
