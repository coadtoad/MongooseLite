//******************************************************************
/*! \file Debug_ids_priority.c
 *
 * \brief Kidde: Mongoose-fw - Define debug ids priority functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "debug_ids_priority.h"
#include "app_serial.h"
#include "state.h"

//*********************************************************************
/**
 * \brief List of priority names in priority order
 */
//*********************************************************************
static char *priority_name_strings[] = {
    // high priority expressions
    /* 00     prior_*/ "fault_fatal",
    /* 01     prior_*/ "power_on",
    /* 02     prior_*/ "smoke",
    /* 03     prior_*/ "remote_smoke",
    /* 04     prior_*/ "co",
    /* 05     prior_*/ "co_conserve",
    /* 06     prior_*/ "co_remote",
    /* 07     prior_*/ "ptt_init",
    /* 08     prior_*/ "ptt",
    /* 09     prior_*/ "smoke_hush",
    /* 10     prior_*/ "eol_fatal",
    /* 11     prior_*/ "battery_low_no_hush",
    /* 12     prior_*/ "fault_photo",
    /* 13     prior_*/ "fault_co",
    /* 14     prior_*/ "eol",
    /* 15     prior_*/ "button_stuck",
    // low priority expressions
    /* 16     prior_*/ "battery_none",
    /* 17     prior_*/ "battery_low",
    /* 18     prior_*/ "alarm_memory_smoke",
    /* 19     prior_*/ "alarm_memory_co",
    /* 20     prior_*/ "alarm_co_peak",
    /* 21     prior_*/ "battery_low_hush",
    /* 22     prior_*/ "eol_hush",
    // calibration states
    /* 23     prior_*/ "cal_pass",
    /* 24     prior_*/ "cal_pass_co_circuit",
    /* 25     prior_*/ "cal_pass_400_ppm",
    /* 26     prior_*/ "cal_fail",
    /* 27     prior_*/ "cal_co_circuit_no_button",
    /* 28     prior_*/ "cal_co_circuit",
    /* 29     prior_*/ "cal_battery_no_button",
    /* 30     prior_*/ "cal_battery",
    /* 31     prior_*/ "cal_lic",
    /* 32     prior_*/ "cal_2_point",
    /* 33     prior_*/ "cal_0_ppm",
    /* 34     prior_*/ "cal_150_ppm",
    /* 35     prior_*/ "cal_400_ppm",
    /* 36     prior_*/ "cal_inline_wait",
    /* 37     prior_*/ "cal_inline_stabilize",
    /* 38     prior_*/ "cal_inline_average",
    /* 39     prior_*/ "cal_inline_verify",
    /* 40     prior_*/ "cal_inline_error",
    /* 41     prior_*/ "power_blink",
    /* 42     prior_*/ "name_count - Nothing Being Expressed"};

//*********************************************************************
/**
 * \brief List of serial output flags
 */
//*********************************************************************
static char *Serial_output_flags_type_strings[] = {
    /*serial_enable_*/ "priority",
    /*serial_enable_*/ "calibration",
    /*serial_enable_*/ "co",
    /*serial_enable_*/ "battery",
    /*serial_enable_*/ "debug",
    /*serial_enable_*/ "ptt",
    /*serial_enable_*/ "echo",
    /*serial_enable_*/ "edwards_mode",
    /*serial_enable_*/ "AFE",
    /*serial_enable_*/ "wireless",
    /*serial_enable_*/ "amp",
    /*serial_enable_*/ "temperature",
    /*serial_enable_*/ "supervision",
    /*serial_enable_*/ "2_6",
    /*serial_enable_*/ "2_7",
    /*serial_enable_*/ "heap",
};

void serial_send_msg_priority_debug(priority_name const priority, Serial_output_flags_type_e output_flag)
{
    serial_send_string_debug("prior_", output_flag);
    serial_send_string_debug(priority_name_strings[priority], output_flag);
}

void serial_send_msg_serial_flags_debug(uint8_t const new_flag, Serial_output_flags_type_e output_flag)
{
    serial_send_string_debug("serial_enable_", output_flag);
    serial_send_string_debug(Serial_output_flags_type_strings[new_flag], output_flag);
}
