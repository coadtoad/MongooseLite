//******************************************************************
/*! \file message_heap.c
 *
 * \brief Kidde: Mongoose-fw - This is the message queue, an implementation of a timer queue.
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "message_heap.h"
#include "app_expression.h"
#include "app_serial.h"
#include "critical_section.h"
#include "debug_ids_heap.h"
#include "drv_button.h"
#include "drv_intercon.h"
#include "drv_led.h"
#include "drv_sleep.h"
#include "drv_sounder.h"
#include "drv_timer.h"
#include "drv_voice.h"
#include "hal_rtc.h"
#include "key_value_store.h"
#include "task_id.h"
#include "task_list.h"
#include <stdbool.h>
#include <stddef.h>

//*********************************************************************
/**
 * \brief Parent index of heap locaiton
 */
//*********************************************************************
#define PARENT_INDEX(index)       (((index)-1) >> 1)

//*********************************************************************
/**
 * \brief First child of heap location
 */
//*********************************************************************
#define FIRST_CHILD_INDEX(index)  (((index) << 1) + 1)

//*********************************************************************
/**
 * \brief Second child of heap location
 */
//*********************************************************************
#define SECOND_CHILD_INDEX(index) (((index) << 1) + 2)

/**
 * \brief The next sequence number to use when pushing a timer queue entry
 *
 * Given a 1ms maximum timer resolution, and given that no message in the
 * system is scheduled out further that 24 hours, 49 messages can be
 * enqueued every millisecond with no chance of a duplicate sequence number
 * being in the message queue.  49 messages per millisecond is nearly
 * 500 times faster than messages are enqueued in practice.
 */
static uint32_t next_sequence_number = 0;

//*********************************************************************
/**
 * \brief Defines an entry in the message queue.
 */
//*********************************************************************
typedef struct
{
    uint32_t time;
    uint32_t sequence;
    uint16_t data;
    task_id task;
    message_id message;
} timer_queue_entry;

//*********************************************************************
/**
 * \brief This is a heap of queued messages.
 */
//*********************************************************************
static timer_queue_entry heap[128];

//*********************************************************************
/**
 * \brief Next location in heap to insert at.
 */
//*********************************************************************
static size_t next_entry       = 0;

/**
 * @brief The time that the last message that was executed at was run.
 *
 * This is set to the time that the most recent message was supposed to
 * be dispatched at.  As such, this will never go backwards.
 *
 * Message priorities are calculated relative to this time.
 *
 * @warning If this ever goes backward, then the message queue could be
 * corrupted.  So long as this is only set when a message has finished
 * executing, this should never happen.
 */
static uint64_t reference_time = 0;

//*********************************************************************
/**
 * \brief Period message timer queue array
 */
//*********************************************************************
static timer_queue_entry period_messages[10];

//*********************************************************************
/**
 * \brief Next time period in period messages
 */
//*********************************************************************
static size_t next_period_entry = 0;

//*********************************************************************
/**
 * \brief Specific task ID for printing serial output
 */
//*********************************************************************
task_id task_serial_print;

/**
 * @brief This stores the current RTC time when running pop
 *
 * This optimization saves ~20us per messages dispatched
 */
static uint64_t pop_current_rtc = 0;

//*********************************************************************
/**
 * \brief Next time period in period messages
 */
//*********************************************************************
static void add_timed_helper(void *data)
{
    period_messages[next_period_entry++] = *(timer_queue_entry *)data;
}

//*********************************************************************
/**
 * \brief add time and data info to message heap
 * \param time time to be added to heap
 * \param task task to be called, task id
 * \param message message to be sent, message id
 * \param data any data sent with the message
 */
//*********************************************************************
void add_timed(uint32_t const time, task_id const task, message_id const message, uint16_t const data)
{
    timer_queue_entry entry = {time, next_sequence_number++, data, task, message};
    critical_section(&add_timed_helper, &entry);
}

static uint64_t message_priority(timer_queue_entry const *const entry)
{
    uint32_t const time_part     = entry->time - time_64bit_to_32bit(reference_time);
    uint32_t const sequence_part = entry->sequence - next_sequence_number;
    return ((uint64_t)time_part << 32) | sequence_part;
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
static void push_left_helper(void *data)
{
    timer_queue_entry *arguments = data;

    arguments->time              = time_64bit_to_32bit(get_current_message_time() + arguments->time);

    uint64_t const priority      = message_priority(arguments);

    size_t index                 = next_entry++;
    ///< \todo heap can overflow

    ///< O(log(n)) swaps is actually 1*log(n) copies and compares worst case
    // Move new item up the tree until heap requirements hold
    for (size_t parent = PARENT_INDEX(index); index && message_priority(heap + parent) > priority;
         parent        = PARENT_INDEX(index))
    {
        // Move parent down
        heap[index] = heap[parent];
        index       = parent;
    }

    // index is now at the correct location for the new entry
    heap[index] = *arguments;
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void push_left(uint32_t const time, task_id const task, message_id const message, uint16_t const data)
{
    timer_queue_entry arguments = {time, next_sequence_number++, data, task, message};
    critical_section(&push_left_helper, &arguments);
}

//*********************************************************************
/**
 * \brief struct of all the contents of the pop helper
 */
//*********************************************************************
typedef struct
{
    void (*task)(message_id const, uint16_t const);
    uint32_t time;
    message_id message;
    uint16_t data;
    bool is_message_to_dispatch;
} pop_helper_data;

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void pop_helper(void *data)
{
    pop_helper_data *arguments        = data;

    arguments->is_message_to_dispatch = next_entry && time_32bit_to_64bit(heap->time) <= pop_current_rtc;
    if (arguments->is_message_to_dispatch)
    {
        arguments->task                 = task_list[heap->task];
        task_serial_print               = heap->task;
        arguments->time                 = heap->time;
        arguments->message              = heap->message;
        arguments->data                 = heap->data;

        // Delta of the node to move up
        uint64_t const current_priority = message_priority(heap + next_entry - 1);

        // Move last item to the head of the heap
        size_t index                    = 0;

        // O(log(n)) is 1*log(n) copies and comparisons in the worst case
        while (index < next_entry)
        {
            size_t const first_child       = FIRST_CHILD_INDEX(index);
            size_t const second_child      = SECOND_CHILD_INDEX(index);
            uint64_t const first_priority  = message_priority(heap + first_child);
            uint64_t const second_priority = message_priority(heap + second_child);

            // Which child would potentially be moved up?
            size_t lesser_child            = index;
            uint64_t lesser_priority       = current_priority;
            if (second_child < next_entry && second_priority < first_priority)
            {
                lesser_child    = second_child;
                lesser_priority = second_priority;
            }
            else if (first_child < next_entry)
            {
                lesser_child    = first_child;
                lesser_priority = first_priority;
            }

            if (lesser_priority < current_priority)
            {
                // Move higher priority child up the tree
                heap[index] = heap[lesser_child];
                index       = lesser_child;
            }
            else
            {
                // Heap requirements are already met
                break;
            }
        }

        // index is now at the correct location for the last entry
        heap[index] = heap[--next_entry];
    }
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void pop(void)
{
    pop_helper_data data = {NULL, 0, msg_continue, 0, false};

    critical_section(pop_helper, &data);
    while (data.is_message_to_dispatch)
    {
        set_current_message_time(time_32bit_to_64bit(data.time));
        // Voice data toggled for debug purposes
        voice_data_hi();
        data.task(data.message, data.data);
        // Voice data toggled for debug purposes
        voice_data_lo();
        reference_time = get_current_message_time();
        message_debug_out(task_serial_print, data.message, data.data);
        critical_section(pop_helper, &data);
    }
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void delete_left_message(size_t index)
{
    uint64_t const current_priority =
        message_priority(heap + next_entry - 1); // The last item is replacing the deleted item
    while (true)
    {
        size_t const parent            = PARENT_INDEX(index);
        size_t const first_child       = FIRST_CHILD_INDEX(index);
        size_t const second_child      = SECOND_CHILD_INDEX(index);
        uint64_t const first_priority  = message_priority(heap + first_child);
        uint64_t const second_priority = message_priority(heap + second_child);
        if (index && current_priority < message_priority(heap + parent))
        {
            // Move up
            heap[index] = heap[parent];
            index       = parent;
        }
        else if (first_child < next_entry && first_priority < current_priority && first_priority <= second_priority)
        {
            // Move down left if left child is less than this and less than or equal to the right child
            heap[index] = heap[first_child];
            index       = first_child;
        }
        else if (second_child < next_entry && second_priority < current_priority)
        {
            // Move down right if right child is less than this and less than the left child
            heap[index] = heap[second_child];
            index       = second_child;
        }
        else
        {
            // Done!  Heap restored!
            heap[index] = heap[--next_entry];
            return;
        }
    }
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
typedef struct
{
    task_id task;
    uint16_t message;
    uint16_t data;
} delete_helper_data;

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void delete_left_helper(void *data)
{
    delete_helper_data *arguments = data;
    for (size_t i = 0; i < next_entry; ++i)
    {
        if (heap[i].task == arguments->task &&
            (arguments->message == (uint16_t)-1 || heap[i].message == arguments->message) &&
            (arguments->data == (uint16_t)-1 || heap[i].data == arguments->data))
        {
            delete_left_message(i);
            // Heap was rebalanced, messages to delete may have moved
            i = 0;
        }
    }
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void delete_timed_helper(void *data)
{
    delete_helper_data *arguments = data;
    for (size_t i = 0; i < next_period_entry; ++i)
    {
        if (period_messages[i].task == arguments->task &&
            (arguments->message == (uint16_t)-1 || period_messages[i].message == arguments->message) &&
            (arguments->data == (uint16_t)-1 || period_messages[i].data == arguments->data))
        {
            // period_messages is unordered, so just pull the last entry to here
            // also, go back one index so that the pulled back message is checked
            period_messages[i--] = period_messages[--next_period_entry];
        }
    }
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void delete_left_t(task_id const task)
{
    delete_helper_data data = {task, -1, -1};
    critical_section(delete_left_helper, &data);
}

void delete_timed_t(task_id const task)
{
    delete_helper_data data = {task, -1, -1};
    critical_section(delete_timed_helper, &data);
}

void delete_left_tm(task_id const task, message_id const message)
{
    delete_helper_data data = {task, message, -1};
    critical_section(delete_left_helper, &data);
}

void delete_timed_tm(task_id const task, message_id const message)
{
    delete_helper_data data = {task, message, -1};
    critical_section(delete_timed_helper, &data);
}

void delete_left_tmd(task_id const task, message_id const message, uint16_t const data)
{
    delete_helper_data d = {task, message, data};
    critical_section(delete_left_helper, &d);
}

void delete_timed_tmd(task_id const task, message_id const message, uint16_t const data)
{
    delete_helper_data d = {task, message, data};
    critical_section(delete_timed_helper, &d);
}

//*********************************************************************
/**
 * \brief Determines the time point that the device should wake up at
 *
 * \param[in,out] data the current 64-bit time passed in and
 *                     the next wakeup time returned
 */
//*********************************************************************
static void next_message_time_helper(void *data)
{
    uint64_t *next_wakeup = data;
    uint64_t const now    = *next_wakeup;
    // If nothing to run, then sleep for one hour
    *next_wakeup          = next_entry ? time_32bit_to_64bit(heap->time) : get_current_message_time() + 60 * 60 * 1000;

    // ms until the next message to be dispatched
    // 10ms safety margin
    uint32_t const available = now + 10 >= *next_wakeup ? 0 : *next_wakeup - (now + 10);
    // See if anything fits in the delta
    for (size_t i = 0; i < next_period_entry; ++i)
    {
        if (period_messages[i].time < available)
        {
            set_current_message_time(now);
            // Schedule this message and then move last message to this position
            push_left(0, period_messages[i].task, period_messages[i].message, period_messages[i].data);
            period_messages[i] = period_messages[--next_period_entry];
            *next_wakeup       = time_32bit_to_64bit(heap->time);
            break;
        }
    }
}

void wait_until_next_message(void)
{
    uint64_t const now   = time_rtc_to_64bit(rtc_get_subminute());
    uint64_t next_wakeup = now;
    critical_section(&next_message_time_helper, &next_wakeup);
    if (next_wakeup > now)
    {
        // Voice clock toggled for debug purposes
        voice_clk_hi();
        uint32_t const delta = next_wakeup - now;
        if (delta < 3)
        {
            // Spin until the RTC has reached the time for the next message
            // There is no need for a flag to indicate that the interrupt occured
            while (time_rtc_to_64bit(rtc_get_subminute()) < next_wakeup)
            {
            }
        }
        else
        {
            // From creating the `now` variable to entering low power should
            // never take longer than a few hundred microseconds.  Thia means
            // that this should never hit a case where we enter low power after
            // the RTC interrupt occurs.
            rtc_set_alarma_subminute(time_64bit_to_rtc(next_wakeup));
            enter_low_power();
        }
    }
}

bool messages_available(void)
{
    pop_current_rtc = time_rtc_to_64bit(rtc_get_subminute());
    return next_entry && time_32bit_to_64bit(heap->time) <= pop_current_rtc;
}

void reset_reference_time(void)
{
    reference_time = 0;
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
static void obliterate_helper(void *data)
{
    (void)data;
    // Effectively clear the message queue
    next_entry        = 0;
    next_period_entry = 0;
}

//*********************************************************************
/**
 * \brief ///< Daniel Dilts needs to do this
 */
//*********************************************************************
void obliterate(void)
{
    critical_section(&obliterate_helper, NULL);

    // turn all output off when obliterating
    amber_led_off();
    amber_led_pwm_end();
    red_led_off();
    red_led_pwm_end();
    green_led_off();
    green_led_pwm_end();
    sounder_off();

    // Disable interrupts that could cause processing
    disable_interconnect_int();
    disable_button_intr();

    // The device is no longer expressing.  Make sure any current expression can
    // be interrupted.
    force_expression_end();

    schedule_message(0, task_supervisor, msg_init, 0);
}
