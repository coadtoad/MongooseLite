//******************************************************************
/*! \file no_io_interconnect.c
 *
 * \brief Kidde: Mongoose-fw - IO interconnect function file for non-interconnect units
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "io_interconnect.h"

void interconnect_task(message_id const message, uint16_t const data)
{

}
