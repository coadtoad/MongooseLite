//******************************************************************
/*! \file ptt.c
 *
 * \brief Kidde: Mongoose-fw - Implement the PTT single photo functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "ptt.h"
#include "app_database.h"
#include "app_events.h"
#include "app_expression.h"
#include "app_serial.h"
#include "message.h"
#include "task_list.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief Recreate photo PTT alarm with pass/fail grade
 */
//*********************************************************************

void ptt_task(message_id const message, uint16_t const data)
{
    ptt_message ptt_state_msg = data;

    switch (message)
    {
    case msg_init:
        break;
    case msg_ptt:
    {

        switch (ptt_state_msg)
        {
        case ptt_photo_init:
            serial_send_string_debug("PTT INIT \r", serial_enable_ptt);
            // record PTT event
            //schedule_message(0, task_events, msg_record_history, event_ptt);
            //un//schedule_messages_t(task_photo);
            //schedule_message(0, task_photo, msg_ptt, ptt_photo_init);
            //schedule_message(100, task_photo, msg_continue, 0);
            // if alarm not detected for 10 seconds, PTT fail
            //schedule_message(10000, task_ptt, msg_ptt, ptt_photo_fail);
            break;
        case ptt_photo_pass:
            serial_send_string_debug("PTT PASS\r", serial_enable_ptt);
            // remove timer used to set ptt fail
            //un//schedule_messages_tmd(task_ptt, msg_ptt, ptt_photo_fail);
            // tell photo task ptt is complete - turn off ptt photo driver
            //schedule_message(0, task_photo, msg_ptt, ptt_photo_complete);
            // this will drive the expression to stop, when expression stops,
            // state will send message to ptt task that smoke expression ended
            // below, ptt will then tell state PTT complete.

            // save PTT state to database
            db_set_value(db_ptt_fault_history, 0);
            //schedule_message(0, task_database, msg_update_primary_db, 0);
            break;
        case ptt_photo_fail:
            serial_send_string_debug("PTT FAIL\r", serial_enable_ptt);
            // record ptt failure
            //schedule_message(0, task_events, msg_record_history, event_fault_ptt);
            // tell photo task ptt is complete - turn off ptt photo driver
            //schedule_message(0, task_photo, msg_ptt, ptt_photo_complete);

            // no CO, have device soft reset
            //schedule_message(0, task_state, msg_ptt, ptt_complete);

            // save PTT state to database(will set photo fault on init)
            db_set_value(db_ptt_fault_history, 1);
            //schedule_message(0, task_database, msg_update_primary_db, 0);
            break;
        default:
            break;
        }
    }
    case msg_end_express:
        switch ((expression_id)data)
        {
        case exp_master_smoke:
            // have state exit ptt priority
            //schedule_message(0, task_state, msg_ptt, ptt_complete);
        // case exp_master_co :   Future
        default:
            break;
        }

        break;
    default:
        break;
    }
}
