//******************************************************************
/*! \file ptt_co.c
 *
 * \brief Kidde: Mongoose-fw - Implement PTT CO functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_serial.h"
#include "co_common.h"
#include "drv_co.h"
#include "drv_events.h"
#include "drv_serial.h"
#include "message.h"
#include "task_list.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief PTT counts value
 */
//*********************************************************************
#define PTT_COUNTS       500

//*********************************************************************
/**
 * \brief CO threshold for PTT
 */
//*********************************************************************
#define PTT_CO_THRESHOLD 100

//*********************************************************************
/**
 * \brief PTT CO process enum
 */
//*********************************************************************
typedef enum
{
    co_test_charging,
    co_test_sample
} ptt_co_process;

void ptt_co_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        break;
    case msg_ptt:
        // Start the CO pulsing - CO task simulate
        co_start_test_pulse();
        schedule_message(10, task_ptt_co, msg_continue, (3u << 14) | co_test_charging);
        break;
    case msg_continue:
        switch ((ptt_co_process)data)
        {
        case co_test_charging:
        {
            // Take a CO measurement
            uint16_t adc = 0;
            if (!co_sample(&adc))
            {
                ///\todo: Handle failure
            }
            uint16_t const count = ((data >> 14) & 0x3) - 1;
            serial_send_int_ex_debug("charge_counts:", adc, 10, "\n", serial_enable_ptt);
            if (!count || adc > PTT_COUNTS)
            {
                co_stop_test_pulse();
                schedule_message(100, task_ptt_co, msg_continue, (4u << 13) | co_test_sample);
            }
            else
            {
                schedule_message(10, task_ptt_co, msg_continue, (count << 14) | co_test_charging);
            }
        }
        break;
        case co_test_sample:
        {
            // Take a CO measurement
            uint16_t counts = 0;
            if (!co_make_measurement(&counts))
            {
                ///\todo: Handle failure
            }
            uint16_t const timeout = ((data >> 14) & 0x7) - 1;
            uint16_t const ppm     = co_calc_PPM(counts);
            serial_send_int_ex_debug("ptt_co_ppm:", ppm, 10, "\n", serial_enable_ptt);

            if (ppm > PTT_CO_THRESHOLD)
            {
                // Pass
                schedule_message(0, task_state, msg_ptt, ((uint16_t)task_ptt_co << 8) | 1);
                serial_send_string_debug("CO test success\r", serial_enable_ptt);
            }
            else if (timeout != 0)
            {
                // Next sample in 1 second
                schedule_message(1000, task_ptt_co, msg_continue, (timeout << 14) | co_test_sample);
            }
            else
            {
                // Fail
                schedule_message(0, task_state, msg_ptt, ((uint16_t)task_ptt_co << 8) | 2);
                serial_send_string_debug("CO test failure\r", serial_enable_ptt);
            }
        }
        break;
        }
        break;
    default:
        break;
    }
}
