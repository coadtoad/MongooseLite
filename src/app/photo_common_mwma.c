//******************************************************************
/*! \file photo_common_mwma.c
 *
 * \brief Kidde: Mongoose-fw - Implement photo chamber common functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "photo_common_mwma.h"
#include "app_database.h"
#include "app_serial.h"
#include "drv_events.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include <limits.h>
#include <stdint.h>

//*********************************************************************
/**
 * \brief Offset value for photo dark reading
 */
//*********************************************************************
#define PHOTO_DARK_OFFSET        0x00

//*********************************************************************
/**
 * \brief Photo trouble count value
 */
//*********************************************************************
#define PHOTO_TROUBLE_COUNT      0x03

//*********************************************************************
/**
 * \brief Trouble count for high amber light values
 */
//*********************************************************************
#define HI_AMB_LGT_TROUBLE_COUNT 0x02

//*********************************************************************
/**
 * \brief Fwd Amber LED fault
 */
//*********************************************************************
_Bool fwd_ambient_fault = 0;

//*********************************************************************
/**
 * \brief Bck Amber LED fault
 */
//*********************************************************************
_Bool bck_ambient_fault = 0;

//*********************************************************************
/**
 * \brief Fwd LED fault
 */
//*********************************************************************
_Bool fwd_fault         = 0;

//*********************************************************************
/**
 * \brief Bck LED fault
 */
//*********************************************************************
_Bool bck_fault         = 0;

uint8_t read_dark(light_signal light_reading, uint8_t *const comp)
{
    switch (light_reading)
    {
    case IR_F:
    case BL_F:
        select_forward();
        break;
    case BL_B:
    case IR_B:
        select_backward();
        break;
    default:
        break;
    }

    led_off();
    select_high_gain();
    uint8_t const dark_offset = PHOTO_DARK_OFFSET;

    set_clean_air(dark_offset);

    timer_delay_us(db_get_value(db_photo_sw_wait));
    uint8_t ambient = sample();

    if (db_get_value(db_serial_flags) & disable_dark_read)
    {
        *comp = 0;
        return 0;
    }

    /// Note that Neo checks DB.Struct.MW_FLAG.AmbentLigt_Can_IR
    if ((ambient > OFFSET_DAC_CNT) && !(db_get_value(db_serial_flags) & disable_dark_compensation))
    {
        // The idea here is that if the dark measurement is greater than the
        // expected dark output (OFFSET_DAC_CNT), then we'll find the difference,
        // multiply it by a factor and set the compensation to that value.  That
        // should subtract out the contribution of any ambient light.
        uint32_t compensation = ambient - OFFSET_DAC_CNT;
        compensation *= db_get_value16(db_ambient_light_cal_multiplier_msb, db_ambient_light_cal_multiplier_lsb);

        if (compensation > USHRT_MAX)
        {
            compensation = USHRT_MAX;
        }

        *comp = (uint8_t)(compensation >> 8);
        set_clean_air(*comp);
        timer_delay_us(db_get_value(db_photo_sw_wait));
        ambient = sample();
    }
    else
    {
        *comp = 0;
    }

    return ambient;
}

uint8_t read_light(light_signal light_reading, uint8_t const dark_f, uint8_t const dac_count)
{
    switch (light_reading)
    {
    case IR_F:
        select_forward();
        select_infrared();
        break;
    case IR_B:
        select_backward();
        select_infrared();
        break;
    case BL_F:
        select_forward();
        select_blue();
        break;
    case BL_B:
        select_backward();
        select_blue();
        break;
    default:
        break;
    }

    select_high_gain();
    set_clean_air(dark_f);
    set_current(dac_count);
    led_on();
    timer_delay_us(db_get_value(db_photo_sw_wait));

    return sample();
}

uint8_t read_light_low_gain(void)
{
    // use all previous values
    select_low_gain();

    return sample();
}

//*********************************************************************
/**
 * \brief This function returns true if the measurement is valid and false otherwise if the measurement is invalid then
 * the fault flag is set
 *
 * Function : validate_light_ir_f()
 *
 * PRE-CONDITION: measurement is a light ir forward reading  <br>
 *
 * POST-CONDITION: a fault flag is set true if the measurement is invalid
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
_Bool validate_light_ir_f(uint8_t const light_ir_f)
{
    static uint8_t fwd_fault_debounce_count = 0;

    if (!fwd_ambient_fault)
    {
        if (light_ir_f < db_get_value(db_fwd_open_th))
        {
            if (fwd_fault_debounce_count >= PHOTO_TROUBLE_COUNT)
            {
                if (!fwd_fault)
                {
                    // record event history only on fault entry
                    schedule_message(0, task_events, msg_record_history, event_fault_photo_fwd);
                }
                fwd_fault = 1;
            }
            else
            {
                ++fwd_fault_debounce_count;
            }
        }
        else
        {
            if (fwd_fault_debounce_count)
            {
                // Debounce check
                --fwd_fault_debounce_count;
            }
            else
            {
                if (fwd_fault)
                {
                    // record event history only on fault exit
                    schedule_message(0, task_events, msg_record_history, event_fault_photo_fwd_clr);
                }
                fwd_fault = 0;
            }
        }
    }

    return fwd_fault_debounce_count == 0;
}

//*********************************************************************
/**
 * \brief This function returns true if the measurement is valid and false otherwise if the measurement is invalid then
 * the fault flag is set
 *
 * Function : validate_light_ir_b()
 *
 * PRE-CONDITION: measurement is a light ir bck reading  <br>
 *
 * POST-CONDITION: a fault flag is set true if the measurement is invalid
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
_Bool validate_light_ir_b(uint8_t const light_ir_b)
{
    static uint8_t bck_fault_debounce_count = 0;

    if (!bck_ambient_fault)
    {
        if (light_ir_b < db_get_value(db_bck_open_th))
        {
            if (bck_fault_debounce_count >= PHOTO_TROUBLE_COUNT)
            {
                if (!bck_fault)
                {
                    // record event history only on fault entry
                    schedule_message(0, task_events, msg_record_history, event_fault_photo_bck);
                }
                bck_fault = 1;
            }
            else
            {
                ++bck_fault_debounce_count;
            }
        }
        else
        {
            if (bck_fault_debounce_count)
            {
                // Debounce check
                --bck_fault_debounce_count;
            }
            else
            {
                if (bck_fault)
                {
                    // record event history only on fault exit
                    schedule_message(0, task_events, msg_record_history, event_fault_photo_bck_clr);
                }
                bck_fault = 0;
            }
        }
    }

    return bck_fault_debounce_count == 0;
}

//*********************************************************************
/**
 * \brief This function returns true if the measurement is valid and false otherwise if the measurement is invalid then
 * the fault flag is set
 *
 * Function : validate_dark_f()
 *
 * PRE-CONDITION: measurement is a dark forward reading  <br>
 *
 * POST-CONDITION: a fault flag is set true if the measurement is invalid
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
_Bool validate_dark_f(uint8_t const dark_f)
{
    static uint8_t fwd_ambient_debounce_counter = 0;

    if (dark_f > db_get_value(db_fwd_hi_amb_th))
    {
        if (fwd_ambient_debounce_counter >= HI_AMB_LGT_TROUBLE_COUNT)
        {
            if (!fwd_ambient_fault)
            {
                // record event history only on fault entry
                schedule_message(0, task_events, msg_record_history, event_fault_photo_fwd_amb);
            }
            fwd_ambient_fault = 1;
        }
        else
        {
            ++fwd_ambient_debounce_counter;
        }
    }
    else
    {
        if (fwd_ambient_debounce_counter)
        {
            // Debounce check
            --fwd_ambient_debounce_counter;
        }
        else
        {
            if (fwd_ambient_fault)
            {
                // record event history only on fault exit
                schedule_message(0, task_events, msg_record_history, event_fault_photo_fwd_amb_clr);
            }
            fwd_ambient_fault = 0;
        }
    }

    return fwd_ambient_debounce_counter == 0;
}

//*********************************************************************
/**
 * \brief "This function returns true if the measurement is valid and false otherwise if the measurement is invalid then
 * the fault flag is set
 *
 * Function : validate_dark_b()
 *
 * PRE-CONDITION: measurement is a back dark reading  <br>
 *
 * POST-CONDITION: a fault flag is set true if the measurement is invalid
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
_Bool validate_dark_b(uint8_t const dark_b)
{
    static uint8_t bck_ambient_debounce_counter = 0;

    if (dark_b > db_get_value(db_bck_hi_amb_th))
    {
        if (bck_ambient_debounce_counter >= HI_AMB_LGT_TROUBLE_COUNT)
        {
            if (!bck_ambient_fault)
            {
                // record event history only on fault entry
                schedule_message(0, task_events, msg_record_history, event_fault_photo_bck_amb);
            }
            bck_ambient_fault = 1;
        }
        else
        {
            ++bck_ambient_debounce_counter;
        }
    }
    else
    {
        if (bck_ambient_debounce_counter)
        {
            // Debounce check
            --bck_ambient_debounce_counter;
        }
        else
        {
            if (bck_ambient_fault)
            {
                // record event history only on fault exit
                schedule_message(0, task_events, msg_record_history, event_fault_photo_bck_amb_clr);
            }
            bck_ambient_fault = 0;
        }
    }

    return bck_ambient_debounce_counter == 0;
}

//*********************************************************************
/**
 * \brief This function updates the 4 photo IR LED statuses in the Key Value Store
 *
 * Function : update_ir_photo_fault_kvs()
 *
 * PRE-CONDITION: validate_dark_f, validate_dark_b, validate_light_ir_f, validate_light_ir_b <br>
 *
 * POST-CONDITION: key_ir_photo_faults is updated
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 02/01/2021 </td><td> 21.0.3           </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
void update_ir_photo_fault_kvs(void)
{
    uint8_t ir_photo_faults =
        ((uint8_t)fwd_ambient_fault << kvs_fwd_ambient_fault) | ((uint8_t)bck_ambient_fault << kvs_bck_ambient_fault) |
        ((uint8_t)fwd_fault << kvs_fwd_ir_led_fault) | ((uint8_t)bck_fault << kvs_bck_ir_led_fault);

    schedule_message(0, task_key_value_store, msg_set_value, (uint16_t)ir_photo_faults << 8 | key_ir_photo_faults);
}
