//******************************************************************
/*! \file static_asserts.c
 *
 * \brief Kidde: Mongoose-fw - Create static values to assert on specified variables
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "drv_events.h"
#include "key_value_store.h"
#include "message_id.h"
#include "out_voice.h"
#include "task_id.h"

//*********************************************************************
/**
 * \brief Set size of key_name variable
 */
//*********************************************************************
_Static_assert(sizeof(key_name) == sizeof(uint8_t), "key_name must be 1 byte");

//*********************************************************************
/**
 * \brief Set size of message_id variable
 */
//*********************************************************************
_Static_assert(sizeof(message_id) == sizeof(uint8_t), "message_id must be 1 byte");

//*********************************************************************
/**
 * \brief Set size of voice_message variable
 */
//*********************************************************************
_Static_assert(sizeof(voice_message) == sizeof(uint8_t), "voice_message must be 1 byte");

//*********************************************************************
/**
 * \brief Set size of task_id variable
 */
//*********************************************************************
_Static_assert(sizeof(task_id) == sizeof(uint8_t), "task_id must be 1 byte");

//*********************************************************************
/**
 * \brief Set size of task_id variable
 */
//*********************************************************************
_Static_assert(sizeof(event_entry) == 8, "event_entry must be 8 byte");
