//******************************************************************
/*! \file clean_air_value_mwma.c
 *
 * \brief Kidde: Mongoose-fw - Define the clean air value functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "clean_air_value.h"
#include "database.h"
#include "drv_events.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_mwma.h"
#include "stdlib.h"
#include <stdbool.h>

#define MAX_CLEAN_AIR_CHANGE       30
#define BLUE_SUPERVISION_CHECKS    3

#define CAV_UPDATE_INTERVAL        120 // two hours // debug 2 mins

#define SENSITIVITY_DECREASE       2
#define SENSITIVITY_DECREASE_LIMIT 3
#define SENSITIVITY_INCREASE       5

//*********************************************************************
/**
 * \brief Prompt to update clean air value
 * \param cav_reason the reason for a clean air count event
 * \ingroup
 */
//*********************************************************************
static void update_clean_air_count(cav_reason_t const cav_reason);

//*********************************************************************
/**
 * \brief IR forward average
 * \ingroup
 */
//*********************************************************************
static uint16_t ir_f_avg = 0;

//*********************************************************************
/**
 * \brief IR backward average
 * \ingroup
 */
//*********************************************************************
static uint16_t ir_b_avg = 0;

//*********************************************************************
/**
 * \brief Blue forward average
 * \ingroup
 */
//*********************************************************************
static uint16_t bl_f_avg = 0;

//*********************************************************************
/**
 * \brief IR forward override boolean
 * \ingroup
 */
//*********************************************************************
static bool IR_F_override;

//*********************************************************************
/**
 * \brief IR backward override boolean
 * \ingroup
 */
//*********************************************************************
static bool IR_B_override;

//*********************************************************************
/**
 * \brief Blue forward override boolean
 * \ingroup
 */
//*********************************************************************
static bool Bl_F_override;

//*********************************************************************
/**
 * \brief IR forward override
 * \ingroup
 */
//*********************************************************************
static uint8_t override_ir_f;

//*********************************************************************
/**
 * \brief IR backward override
 * \ingroup
 */
//*********************************************************************
static uint8_t override_bl_f;

//*********************************************************************
/**
 * \brief Blue forward override
 * \ingroup
 */
//*********************************************************************
static uint8_t override_ir_b;

//*********************************************************************
/**
 * \brief Avoid boolean
 * \ingroup
 */
//*********************************************************************
static bool avoid        = false;

//*********************************************************************
/**
 * \brief Run boolean
 * \ingroup
 */
//*********************************************************************
static bool run          = false;

//*********************************************************************
/**
 * \brief Clean air value override boolean
 * \ingroup
 */
//*********************************************************************
static bool cav_override = false;

//*********************************************************************
/**
 * \brief Alarm state boolean
 * \ingroup
 */
//*********************************************************************
static bool alarm_state  = false;

//*********************************************************************
/**
 * \brief Push to test clean air value boolean
 * \ingroup
 */
//*********************************************************************
static bool ptt_cav      = false;

//*********************************************************************
/**
 * \brief State enum, clean air or blue supervision
 * \ingroup
 */
//*********************************************************************
typedef enum
{
    clean_air,
    blue_supervision
} state;

//*********************************************************************
/**
 * \brief Performs smoke measurements for clean air mode
 * \note Function takes IR forward and back measurements and blue forward measurements.
 *          The filters and filter chain are then updated
 * \note PRE-CONDITION: The system is in normal mode and will call this function 5 times in 5 seconds
 * \note POST-CONDITION: The filters and filter chain are updated with the new measurements
 * \ingroup
 */
//*********************************************************************
static bool run_clean_air_mode(void)
{
    if (!begin_photo_read())
    {
        return false;
    }
    // wait microseconds for the circuit to stabilize
    timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);
    // select the forward scatter photo sensor and the IR LED to prepare for measurements

    uint8_t comp_f;
    // take a dark measurement
    uint8_t const dark_f = read_dark(IR_F, &comp_f);
    // take a light measurement
    uint8_t light_ir_f;

    if (IR_F_override)
    {
        light_ir_f = override_ir_f;
    }
    else
    {
        light_ir_f = read_light(IR_F, comp_f, db_get_value(db_current_ir_f));
    }
    uint8_t const light_ir_f_lo = read_light_low_gain();
    led_off();

    // take a blue led forward measurement
    uint8_t light_bl_f;

    if (Bl_F_override)
    {
        light_bl_f = override_bl_f;
    }
    else
    {
        light_bl_f = read_light(BL_F, comp_f, db_get_value(db_current_bl_f));
    }

    uint8_t const light_bl_f_lo = read_light_low_gain();
    led_off();

    uint8_t comp_b;
    // take dark and light ir back measurements
    uint8_t const dark_b = read_dark(IR_B, &comp_b);
    uint8_t light_ir_b;

    if (IR_B_override)
    {
        light_ir_b = override_ir_b;
    }
    else
    {
        light_ir_b = read_light(IR_B, comp_b, db_get_value(db_current_ir_b));
    }

    uint8_t const light_ir_b_lo = read_light_low_gain();
    led_off();

    // photo power, the IR DAC, and the CAV DAC are all disabled
    select_forward();
    end_photo_read();

    // Validate measurements
    validate_dark_f(dark_f);
    validate_dark_b(dark_b);
    validate_light_ir_f(light_ir_f);
    validate_light_ir_b(light_ir_b);

    // update cav values for averaging
    ir_f_avg += light_ir_f;
    ir_b_avg += light_ir_b;
    bl_f_avg += light_bl_f;

    return true;
}

//*********************************************************************
/**
 * \brief Updates the database clean air values
 * \note Function compares the filter x0 values from ir_f, ir_b, and bl_f to database clean air values (qusi)
 *          -# If the value forany is greater than the respective database value the database value will be incremented
 * by one
 *          -# If a value is less the database value will be decremented by one.
 *          -# These database values will not be permitted to vary more than the specifies 'clean air change' from the
 * database \note PRE-CONDITION: run_clean_air_mode() was just called 5 times in 5 seconds \note POST-CONDITION: The
 * database values for ir_f_qusi, ir_b_qusi, and blue_qusi are all updated if any x0 wasn't equal \param cav_reason the
 * reason for a clean air count event \ingroup
 */
//*********************************************************************
void update_clean_air_count(cav_reason_t const cav_reason)
{
    bool photo_cav_fault                       = false;
    static bool photo_cav_fault_prev           = false;

    bool photo_cav_fault_ir_f_high             = false;
    bool photo_cav_fault_ir_f_low              = false;
    bool photo_cav_fault_ir_b_high             = false;
    bool photo_cav_fault_ir_b_low              = false;
    bool photo_cav_fault_bl_f_high             = false;
    bool photo_cav_fault_bl_f_low              = false;
    static bool photo_cav_fault_ir_f_high_prev = false;
    static bool photo_cav_fault_ir_f_low_prev  = false;
    static bool photo_cav_fault_ir_b_high_prev = false;
    static bool photo_cav_fault_ir_b_low_prev  = false;
    static bool photo_cav_fault_bl_f_high_prev = false;
    static bool photo_cav_fault_bl_f_low_prev  = false;

    ir_f_avg                                   = (ir_f_avg / PHOTO_CLEANAIR_READTIME);
    ir_b_avg                                   = (ir_b_avg / PHOTO_CLEANAIR_READTIME);
    bl_f_avg                                   = (bl_f_avg / PHOTO_CLEANAIR_READTIME);

    serial_send_int_ex_debug("CAV Update - ", cav_reason, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("ir_f_avg - ", ir_f_avg, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("ir_b_avg - ", ir_b_avg, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("bl_f_avg - ", bl_f_avg, 16, "\r", serial_enable_calibration);

    uint8_t const ir_f_clean_air_db = db_get_value(db_ir_f_clean_air);
    uint8_t const ir_b_clean_air_db = db_get_value(db_ir_b_clean_air);
    uint8_t const blue_clean_air_db = db_get_value(db_blue_clean_air);

    serial_send_int_ex_debug("ir_f_current - ", ir_f_clean_air_db, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("ir_b_current - ", ir_b_clean_air_db, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("bl_f_current - ", blue_clean_air_db, 16, "\r", serial_enable_calibration);

    if (cav_reason == cav_reason_normal)
    {
        // 1. Examine IR_F for CAV Changes and adjust compensated CAV value
        if (ir_f_avg > ir_f_clean_air_db)
        {
            ir_f_avg = ir_f_clean_air_db + 1;
        }
        else if (ir_f_avg < ir_f_clean_air_db)
        {
            ir_f_avg = ir_f_clean_air_db - 1;
        }

        // 2.Examine IR_B for CAV Changes and adjust compensated CAV value
        if (ir_b_avg > ir_b_clean_air_db)
        {
            ir_b_avg = ir_b_clean_air_db + 1;
        }
        else if (ir_b_avg < ir_b_clean_air_db)
        {
            ir_b_avg = ir_b_clean_air_db - 1;
        }

        // 3.  Examine BL_F for CAV Changes and adjust compensated CAV value
        if (bl_f_avg > blue_clean_air_db)
        {
            bl_f_avg = blue_clean_air_db + 1;
        }
        else if (bl_f_avg < blue_clean_air_db)
        {
            bl_f_avg = blue_clean_air_db - 1;
        }
    }
    else if (cav_reason == cav_reason_ptt)
    {
        // 1. Examine IR_F for CAV Changes and adjust compensated CAV value
        if (ir_f_avg > ir_f_clean_air_db)
        {
            uint8_t ir_f_clean_air_difference = ir_f_avg - ir_f_clean_air_db;
            if (ir_f_clean_air_difference > SENSITIVITY_DECREASE_LIMIT)
            {
                ir_f_clean_air_difference = 1;
            }
            else if (ir_f_clean_air_difference > SENSITIVITY_DECREASE)
            {
                ir_f_clean_air_difference = SENSITIVITY_DECREASE;
            }
            ir_f_avg = ir_f_clean_air_db + ir_f_clean_air_difference;
        }
        else if (ir_f_avg < ir_f_clean_air_db)
        {
            uint8_t ir_f_clean_air_difference = ir_f_clean_air_db - ir_f_avg;
            if (ir_f_clean_air_difference > SENSITIVITY_INCREASE)
            {
                ir_f_clean_air_difference = SENSITIVITY_INCREASE;
            }
            ir_f_avg = ir_f_clean_air_db - ir_f_clean_air_difference;
        }

        // 2.Examine IR_B for CAV Changes and adjust compensated CAV value
        if (ir_b_avg > ir_b_clean_air_db)
        {
            uint8_t ir_b_clean_air_difference = ir_b_avg - ir_b_clean_air_db;
            if (ir_b_clean_air_difference > SENSITIVITY_DECREASE_LIMIT)
            {
                ir_b_clean_air_difference = 1;
            }
            else if (ir_b_clean_air_difference > SENSITIVITY_DECREASE)
            {
                ir_b_clean_air_difference = SENSITIVITY_DECREASE;
            }
            ir_b_avg = ir_b_clean_air_db + ir_b_clean_air_difference;
        }
        else if (ir_b_avg < ir_b_clean_air_db)
        {
            uint8_t ir_b_clean_air_difference = ir_b_clean_air_db - ir_b_avg;
            if (ir_b_clean_air_difference > SENSITIVITY_INCREASE)
            {
                ir_b_clean_air_difference = SENSITIVITY_INCREASE;
            }
            ir_b_avg = ir_b_clean_air_db - ir_b_clean_air_difference;
        }

        // 3.  Examine BL_F for CAV Changes and adjust compensated CAV value
        if (bl_f_avg > blue_clean_air_db)
        {
            uint8_t bl_f_clean_air_difference = bl_f_avg - blue_clean_air_db;
            if (bl_f_clean_air_difference > SENSITIVITY_DECREASE_LIMIT)
            {
                bl_f_clean_air_difference = 1;
            }
            else if (bl_f_clean_air_difference > SENSITIVITY_DECREASE)
            {
                bl_f_clean_air_difference = SENSITIVITY_DECREASE;
            }
            bl_f_avg = blue_clean_air_db + bl_f_clean_air_difference;
        }
        else if (bl_f_avg < blue_clean_air_db)
        {
            uint8_t bl_f_clean_air_difference = blue_clean_air_db - bl_f_avg;
            if (bl_f_clean_air_difference > SENSITIVITY_INCREASE)
            {
                bl_f_clean_air_difference = SENSITIVITY_INCREASE;
            }
            bl_f_avg = blue_clean_air_db - bl_f_clean_air_difference;
        }
    }

    // Fault Checks
    if (ir_f_avg > db_get_value(db_ir_f_factory_clean_air))
    {
        if ((ir_f_avg - db_get_value(db_ir_f_factory_clean_air)) > MAX_CLEAN_AIR_CHANGE)
        {
            ir_f_avg                  = ir_f_clean_air_db;
            photo_cav_fault_ir_f_high = true;
        }
    }
    else if (ir_f_avg < db_get_value(db_ir_f_factory_clean_air))
    {
        if ((db_get_value(db_ir_f_factory_clean_air) - ir_f_avg) > MAX_CLEAN_AIR_CHANGE)
        {
            ir_f_avg                 = ir_f_clean_air_db;
            photo_cav_fault_ir_f_low = true;
        }
    }

    if (ir_b_avg > db_get_value(db_ir_b_factory_clean_air))
    {
        if ((ir_b_avg - db_get_value(db_ir_b_factory_clean_air)) > MAX_CLEAN_AIR_CHANGE)
        {
            ir_b_avg                  = ir_b_clean_air_db;
            photo_cav_fault_ir_b_high = true;
        }
    }
    else if (ir_b_avg < db_get_value(db_ir_b_factory_clean_air))
    {
        if ((db_get_value(db_ir_b_factory_clean_air) - ir_b_avg) > MAX_CLEAN_AIR_CHANGE)
        {
            ir_b_avg                 = ir_b_clean_air_db;
            photo_cav_fault_ir_b_low = true;
        }
    }

    if (bl_f_avg > db_get_value(db_blue_factory_clean_air))
    {
        if ((bl_f_avg - db_get_value(db_blue_factory_clean_air)) > MAX_CLEAN_AIR_CHANGE)
        {
            bl_f_avg                  = blue_clean_air_db;
            photo_cav_fault_bl_f_high = true;
        }
    }
    else if (bl_f_avg < db_get_value(db_blue_factory_clean_air))
    {
        if ((db_get_value(db_blue_factory_clean_air) - bl_f_avg) > MAX_CLEAN_AIR_CHANGE)
        {
            bl_f_avg                 = blue_clean_air_db;
            photo_cav_fault_bl_f_low = true;
        }
    }

    /// if any changed write all to the database
    if (ir_f_avg != ir_f_clean_air_db || ir_b_avg != ir_b_clean_air_db || bl_f_avg != blue_clean_air_db)
    {
        db_set_value(db_ir_f_clean_air, ir_f_avg);
        db_set_value(db_ir_b_clean_air, ir_b_avg);
        db_set_value(db_blue_clean_air, bl_f_avg);
        schedule_message(0, task_database, msg_update_primary_db, 0);
    }
    serial_send_int_ex_debug("ir-f-CAV - ", ir_f_avg, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("ir_b_CAV - ", ir_b_avg, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("bl_f_CAV - ", bl_f_avg, 16, "\r", serial_enable_calibration);

    // Save Events for each CAV fault
    if (photo_cav_fault_ir_f_high != photo_cav_fault_ir_f_high_prev)
    {
        photo_cav_fault_ir_f_high_prev = photo_cav_fault_ir_f_high;
        if (photo_cav_fault_ir_f_high)
        {
            schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_f_hi);
        }
    }
    if (photo_cav_fault_ir_f_low != photo_cav_fault_ir_f_low_prev)
    {
        photo_cav_fault_ir_f_low_prev = photo_cav_fault_ir_f_low;
        if (photo_cav_fault_ir_f_low)
        {
            schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_f_lo);
        }
    }
    if (photo_cav_fault_ir_b_high != photo_cav_fault_ir_b_high_prev)
    {
        photo_cav_fault_ir_b_high_prev = photo_cav_fault_ir_b_high;
        if (photo_cav_fault_ir_f_low)
        {
            schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_b_hi);
        }
    }
    if (photo_cav_fault_ir_b_low != photo_cav_fault_ir_b_low_prev)
    {
        photo_cav_fault_ir_b_low_prev = photo_cav_fault_ir_b_low;
        if (photo_cav_fault_ir_b_low)
        {
            schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_b_lo);
        }
    }
    if (photo_cav_fault_bl_f_high != photo_cav_fault_bl_f_high_prev)
    {
        photo_cav_fault_bl_f_high_prev = photo_cav_fault_bl_f_high;
        if (photo_cav_fault_bl_f_high)
        {
            schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_bl_f_hi);
        }
    }
    if (photo_cav_fault_bl_f_low != photo_cav_fault_bl_f_low_prev)
    {
        photo_cav_fault_bl_f_low_prev = photo_cav_fault_bl_f_low;
        if (photo_cav_fault_bl_f_low)
        {
            schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_bl_f_lo);
        }
    }

    photo_cav_fault = photo_cav_fault_ir_f_high || photo_cav_fault_ir_f_low || photo_cav_fault_ir_b_high ||
                      photo_cav_fault_ir_b_low || photo_cav_fault_bl_f_high || photo_cav_fault_bl_f_low;

    // Clear clean air fault master (if any other faults recorded)
    if (photo_cav_fault_prev != photo_cav_fault)
    {
        photo_cav_fault_prev = photo_cav_fault;

        // send state message to express CAV fault if any.
        schedule_message(0, task_key_value_store, msg_set_value, (uint16_t)photo_cav_fault << 8 | key_clean_air_fault);

        if (!photo_cav_fault)
        {
            schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_clr);
        }
    }

    // Clear Averages for next CAV update.
    ir_f_avg = 0;
    ir_b_avg = 0;
    bl_f_avg = 0;

} // end update_clean_air_count

_Bool blue_led_supervision(void)
{
    uint8_t threshold = db_get_value(db_photo_cal_blue_back_supervision);

    if (!begin_photo_read())
    {
        return true;
    }
    // wait microseconds for the circuit to stabilize
    timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

    // take a dark measurement
    uint8_t comp_f;
    uint8_t dark_f     = read_dark(BL_B, &comp_f);

    // take a blue led backward measurement
    uint8_t light_bl_b = read_light(BL_B, comp_f, db_get_value(db_current_bl_f));

    end_photo_read();

    char buffer[] = "   - Dark Sup -    - Blue Sup -    - Threshold\r";
    uint8_to_hex(buffer, dark_f);
    uint8_to_hex(buffer + 16, light_bl_b);
    uint8_to_hex(buffer + 32, threshold);
    serial_send_string_debug(buffer, serial_enable_calibration);

    return (light_bl_b < threshold);
}

void clean_air_value_task(message_id const message, uint16_t const data)
{
    static uint8_t cav_count        = 0;
    static uint8_t blue_count       = 0;
    static uint8_t blue_fail_count  = 0;
    static bool bck_blue_fault_prev = false;
    static cav_reason_t cav_reason  = cav_reason_normal;
    static state current_state      = clean_air;
    switch (message)
    {
    case msg_init:
        // schedule a clean air update at power on
        // start the timer to handle tasks that exceed one minute
        schedule_message(1000, task_clean_air_value, msg_count_minutes, 2);

        schedule_message(0, task_key_value_store, msg_add_callback,
                         ((uint16_t)task_clean_air_value) << 8 | key_alarm_flag);
        break;
    case msg_continue:
        if (!cav_count)
        {
            cav_reason = (cav_reason_t)data;
            if (cav_reason == cav_reason_ptt)
            {
                ptt_cav = true;
            }
            cav_count = PHOTO_CLEANAIR_READTIME;
            // charge blue led
            schedule_timed_message(250, task_blue_charge_pump, msg_charge_blue_led, task_clean_air_value);
        }
        break;
    case msg_ready:
        if ((task_id)data == task_blue_charge_pump)
        {
            if (avoid)
            {
                run = true;
                break;
            }
            run = false;

            if (current_state == clean_air)
            {
                if (!alarm_state && run_clean_air_mode())
                {
                    // Sample was captured
                    if (!--cav_count)
                    {
                        update_clean_air_count(cav_reason);
                        update_ir_photo_fault_kvs();
                        current_state   = blue_supervision;
                        blue_count      = BLUE_SUPERVISION_CHECKS;
                        blue_fail_count = 0;
                    }
                }
                else
                {
                    // Sample not captured
                    // Abort clean air update
                    // Move on to blue supervision
                    current_state   = blue_supervision;
                    blue_count      = BLUE_SUPERVISION_CHECKS;
                    blue_fail_count = 0;
                }

                schedule_message(100, task_blue_charge_pump, msg_charge_blue_led, task_clean_air_value);
            }
            else // current_state = blue_supervision
            {
                if (blue_count)
                {
                    if (blue_led_supervision())
                    {
                        blue_fail_count++;
                    }
                    blue_count--;

                    schedule_message(300, task_blue_charge_pump, msg_charge_blue_led, task_clean_air_value);
                }
                else
                {
                    char buffer[] = "   - Fail Count \r";
                    uint8_to_hex(buffer, blue_fail_count);
                    serial_send_string_debug(buffer, serial_enable_calibration);

                    // Set or Clear blue supervision fault
                    _Bool bck_blue_fault = blue_fail_count > 1;

                    if (bck_blue_fault_prev != bck_blue_fault)
                    {
                        bck_blue_fault_prev = bck_blue_fault;
                        schedule_message(0, task_key_value_store, msg_set_value,
                                         (uint16_t)bck_blue_fault << 8 | key_blue_led_fault);

                        if (bck_blue_fault)
                        {
                            schedule_message(0, task_events, msg_record_history, event_fault_photo_blue);
                        }
                        else
                        {
                            schedule_message(0, task_events, msg_record_history, event_fault_photo_blue_clr);
                        }
                    }

                    current_state = clean_air;
                }
            }
        }
        break;
    case msg_count_minutes:
    {
        uint16_t minute_counter = data;

        if (ptt_cav)
        {
            // reset minute counter to full 2 hour update.
            schedule_message(ONE_MINUTE, task_clean_air_value, msg_count_minutes, CAV_UPDATE_INTERVAL);
            ptt_cav = false;
            break;
        }

        // increment the minute counter
        if (cav_override)
        {
            if (minute_counter > 2)
            {
                // Set to 3, so that the decrement below takes it to 2
                minute_counter = 3;
            }
        }

        --minute_counter;
        // if two hours have passed, perform a clean air update
        if (minute_counter == 0)
        {
            schedule_message(0, task_clean_air_value, msg_continue, cav_reason_normal);
            // reset the minute counter
            minute_counter = CAV_UPDATE_INTERVAL;
            if (cav_override)
            {
                minute_counter = 2;
            }
        }
        // reschedule the one minute timer
        schedule_message(ONE_MINUTE, task_clean_air_value, msg_count_minutes, minute_counter);
    }
    break;
    case msg_set_flag:
        if (!data)
        {

            IR_F_override = 0;
            IR_B_override = 0;
            Bl_F_override = 0;
            override_ir_f = 0;
            override_ir_b = 0;
            override_bl_f = 0;
        }
        else if ((uint8_t)(data >> 8) == 0x01)
        {
            IR_F_override = 1;
            override_ir_f = (uint8_t)data;
        }
        else if ((uint8_t)(data >> 8) == 0x02)
        {
            IR_B_override = 1;
            override_ir_b = (uint8_t)data;
        }
        else if ((uint8_t)(data >> 8) == 0x04)
        {
            Bl_F_override = 1;
            override_bl_f = (uint8_t)data;
        }
        else if ((uint8_t)(data >> 8) == 0x08)
        {
            cav_override = true;
        }
        else if ((uint8_t)(data >> 8) == 0x09)
        {
            cav_override = false;
        }
        break;

    case msg_value:
    {
        // sort value and task_id from key_value_store subscriptions
        uint8_t const value = (uint8_t)(data >> 8);
        key_name const key  = (key_name)(uint8_t)(data);

        // Set the appropriate member of the data struct.
        switch (key)
        {
        case key_avoidance_flags:
            avoid = value;
            if (!avoid && run)
            {
                schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_clean_air_value);
            }
            break;
        case key_alarm_flag:
            alarm_state = value;
            break;
        }
        break;
    }
    default:
        break;
    }
}