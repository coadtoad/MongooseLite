//******************************************************************
/*! \file database.cpp
 *
 * \brief Kidde: Mongoose-fw - Database operation info
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "database.h"
#include "drv_database.h"
#include <stddef.h>
#include <stdint.h>

#define CRC_POLYNOMIAL 0x04C11DB7
#define CRC_INIT       0xFFFFFFFF

constexpr uint32_t sw_crc(uint32_t crc)
{
    return crc;
}

template <typename T, typename... Ts> constexpr uint32_t sw_crc(uint32_t crc, T const t, Ts... ts)
{
    crc ^= static_cast<uint8_t>(t) << 24;
    for (int i = 0; i < 8; ++i)
    {
        bool const msb_set = crc & 0x80000000;
        crc <<= 1;
        if (msb_set)
        {
            crc ^= CRC_POLYNOMIAL;
        }
    }
    return sw_crc(crc, ts...);
}

constexpr uint32_t crc_value = sw_crc(CRC_INIT,
#include "database_struct.h"
);

extern "C" uint8_t const database_struct[MEM_EEPROM_SIZE] __attribute__((section(".db_primary"))) = {
#include "database_struct.h"
    , static_cast<uint8_t>(crc_value >> 24), static_cast<uint8_t>(crc_value >> 16),
    static_cast<uint8_t>(crc_value >> 8), static_cast<uint8_t>(crc_value)};

extern "C" uint8_t const backup_database_struct[MEM_EEPROM_SIZE] __attribute__((section(".db_backup"))) = {
#include "database_struct.h"
    , static_cast<uint8_t>(crc_value >> 24), static_cast<uint8_t>(crc_value >> 16),
    static_cast<uint8_t>(crc_value >> 8), static_cast<uint8_t>(crc_value)};
