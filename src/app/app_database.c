//******************************************************************
/*! \file app_database.c
 *
 * \brief Kidde: Mongoose-fw - Declare the database functions
 *
 * \author unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_events.h"
#include "app_serial.h"
#include "database.h"
#include "drv_database.h"
#include "drv_memory.h"
#include "key_value_store.h"
#include "message.h"
#include "state.h"
#include "string.h"
#include "task_list.h"

#define RESCHEDULE_DB_UPDATE      0xffff
#define CODE_SPACE_CRC_BLOCK_SIZE 1024
#define CODE_SPACE_CRC_INTERVAL   (uint32_t)(2 * ONE_MINUTE)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdollar-in-identifier-extension"
extern char Image$$ER_IROM1$$Base;
extern char Image$$ER_CRC$$Base;
char const *const crc_start = &Image$$ER_IROM1$$Base;
char const *const crc_end   = &Image$$ER_CRC$$Base;
#pragma clang diagnostic pop

//*********************************************************************
/**
 * \brief Database action choice enum
 */
//*********************************************************************
typedef enum
{
    ram_checksum,      ///< Checksum for RAM action
    check_flash_size,  ///< Check flash size action
    erase_flash,       ///< Start mem erase action
    calc_new_checksum, ///< Calculate new checksum action
    write_flash,       ///< Begin writing to memory action
    verify_write,      ///< Verify memory write was successful
} db_action;

static uint8_t ram_db[MEM_EEPROM_SIZE] __attribute__((aligned(4)));
static uint8_t buffer_db[MEM_EEPROM_SIZE] __attribute__((aligned(4)));
static bool primary_db_waiting = false;
static bool backup_db_waiting  = false;

//*********************************************************************
/**
 * \brief Validates canary and CRC and recovers if possible
 *
 * If the database cannot be recovered, then it will put the device in
 * fatal fault.
 *
 * \returns If the database is valid or was successfully recovered
 */
//*********************************************************************
static bool validate_database(void)
{
    uint8_t const *const crc_address = ram_db + sizeof(ram_db) / sizeof(ram_db[0]) - 4;
    uint32_t const crc               = memory_calculate_crc((uint32_t const *)ram_db, (uint32_t const *)crc_address);
    bool const crc_valid             = memory_crc_valid(crc, *(uint32_t const *)crc_address);
    if (ram_db[db_canary] != CONFIG_CANARY || !crc_valid)
    {
        if (!database_init(ram_db, sizeof(ram_db)))
        {
            //schedule_message(0, task_state, msg_fault_fatal, event_fault_fatal_mem);
            return false;
        }
    }
    return true;
}

static void update_crc(uint8_t *const db, size_t const size)
{
    memory_set_crc((uint32_t *)(db + size - 4),
                   memory_calculate_crc((uint32_t const *)db, (uint32_t const *)(db + size - 4)));
}

void database_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        // initialize database, check codespace crc, then handle any faults
        if (!database_init(ram_db, sizeof(ram_db)))
        {
            //schedule_message(0, task_state, msg_fault_fatal, event_fault_fatal_mem);
        }

        // CRC full code space at device startup
        if (!memory_crc_valid(memory_calculate_crc((uint32_t const *)crc_start, (uint32_t const *)crc_end),
                              *(uint32_t const volatile *)crc_end))
        {
            // CRC does not match
            //schedule_message(0, task_state, msg_fault_fatal, event_fault_fatal_mem);
        }

        // Schedule running code space CRC
        //schedule_message(CODE_SPACE_CRC_INTERVAL, task_database, msg_firmware_crc, 0);
        break;
    case msg_continue:
    {
        db_target const target = (db_target)(uint8_t)(data >> 8);
        db_action const action = (db_action)(uint8_t)data;
        switch (action)
        {
        case ram_checksum:
            // copy ram_db into a buffer
            memcpy(buffer_db, ram_db, MEM_EEPROM_SIZE);
            // checksum the buffer DB before attempting a write
            if (memory_crc_valid(memory_calculate_crc((uint32_t const *)buffer_db,
                                                      (uint32_t const *)(buffer_db + sizeof(buffer_db) - 4)),
                                 *(uint32_t const *)(buffer_db + sizeof(buffer_db) - 4)))
            {
                // checksum passed, schedule flash erase check
                //schedule_message(0, task_database, msg_continue, (uint16_t)target << 8 | check_flash_size);
            }
            else
            {
                //set_flash_in_progress(false);
                //schedule_message(0, task_events, msg_record_history, event_ram_database);
            }
            break;
        case check_flash_size:
            if (is_flash_full(target))
            {
                // flash is full, schedule an erase before writing
                //schedule_message(0, task_database, msg_continue, (uint16_t)target << 8 | erase_flash);
            }
            else
            {
                // increment the page offset in buffer_db to save the location of the new DB in flash
                update_page_offset(target);
                // schedule a new checksum of the buffer ram DB
                //schedule_message(0, task_database, msg_continue, (uint16_t)target << 8 | calc_new_checksum);
            }
            break;
        case erase_flash:
            if (primary_db == target)
            {
                // Update the erase count here.  We only update the erase count for the primary db since that's
                // the worst case.
                uint16_t count = db_get_value16(db_erase_count_msb, db_erase_count_lsb);
                count++;
                db_set_value16(db_erase_count_msb, db_erase_count_lsb, count);
            }
            if (begin_flash_erase(target))
            {
                // increment the page offset in buffer_db to save the location of the new DB in flash
                update_page_offset(target);

                // schedule a new checksum of the buffer ram DB
                //schedule_message(0, task_database, msg_continue, (uint16_t)target << 8 | calc_new_checksum);
            }
            else
            {
                //set_flash_in_progress(false);
                if (target == primary_db)
                {
                    //schedule_message(0, task_events, msg_record_history, event_nv_data_primary);
                }
                else
                {
                    //schedule_message(0, task_events, msg_record_history, event_nv_data_backup);
                }
            }
            break;
        case calc_new_checksum:
            // calculate a new crc that includes the incremented counters
            update_crc(buffer_db, sizeof(buffer_db));

            // schedule the DB write
            //schedule_message(0, task_database, msg_continue, (uint16_t)target << 8 | write_flash);
            break;
        case write_flash:
            // begin the write process
            if (begin_flash_write(target, buffer_db, sizeof(buffer_db)))
            {
                //schedule_message(0, task_database, msg_continue, (uint16_t)target << 8 | verify_write);
            }
            else
            {
                //set_flash_in_progress(false);
                if (target == primary_db)
                {
                    //schedule_message(0, task_events, msg_record_history, event_nv_data_primary);
                }
                else
                {
                    //schedule_message(0, task_events, msg_record_history, event_nv_data_backup);
                }
            }
            break;
        case verify_write:
            if (begin_flash_verify(target, buffer_db, sizeof(buffer_db)))
            {
                // write complete
                //set_flash_in_progress(false);
            }
            else
            {
                //set_flash_in_progress(false);
                if (target == primary_db)
                {
                    //schedule_message(0, task_events, msg_record_history, event_nv_data_primary);
                }
                else
                {
                    //schedule_message(0, task_events, msg_record_history, event_nv_data_backup);
                }
            }
            break;
        }
        break;
    }
    case msg_update_primary_db:
        if (!primary_db_waiting || data == RESCHEDULE_DB_UPDATE)
        {
//            if (!get_flash_in_progress())
//            {
//                if (validate_database())
//                {
//                    primary_db_waiting = false;
//                    //set_flash_in_progress(true);
//                    // begin update process with the ram_db checksum
//                    //schedule_message(0, task_database, msg_continue, (uint16_t)primary_db << 8 | ram_checksum);
//                }
//            }
//            else
//            {
//                // update in progress, postpone additional update requests
//                primary_db_waiting = true;
//                //schedule_message(100, task_database, msg_update_primary_db, RESCHEDULE_DB_UPDATE);
//            }
        }
        break;
    case msg_update_backup_db:
        if (!backup_db_waiting || data == RESCHEDULE_DB_UPDATE)
        {
//            if (!get_flash_in_progress())
//            {
//                if (validate_database())
//                {
//                    backup_db_waiting = false;
//                    //set_flash_in_progress(true);
//                    // begin update process with the ram_db checksum
//                    //schedule_message(0, task_database, msg_continue, (uint16_t)backup_db << 8 | ram_checksum);
//                }
//            }
//            else
//            {
//                // update in progress, postpone additional update requests
//                backup_db_waiting = true;
//                //schedule_message(100, task_database, msg_update_backup_db, RESCHEDULE_DB_UPDATE);
//            }
        }
        break;
    case msg_firmware_crc:
    {
        static uint32_t running_crc             = 0xffffffff;
        static uint32_t const *next_block_begin = (uint32_t const *)crc_start;

        uint32_t const *const block_end         = (char const *)next_block_begin + CODE_SPACE_CRC_BLOCK_SIZE > crc_end
                                                      ? (uint32_t const *)crc_end
                                                      : next_block_begin + CODE_SPACE_CRC_BLOCK_SIZE / 4;

        running_crc = memory_calculate_crc_running(next_block_begin, block_end, running_crc);

        if ((char const *)block_end == crc_end)
        {
            // Last block was CRCed
            next_block_begin = (uint32_t const *)crc_start;

            if (memory_crc_valid(running_crc, *(uint32_t const *)crc_end))
            {
                // CRC matches
                running_crc = 0xffffffff;
            }
            else
            {
                // CRC does not match
                //schedule_message(0, task_state, msg_fault_fatal, event_fault_fatal_mem);
                break;
            }
        }
        else
        {
            // More blocks to CRC
            next_block_begin = block_end;
        }
        //schedule_message(CODE_SPACE_CRC_INTERVAL, task_database, msg_firmware_crc, 0);
        break;
    }
    case msg_read_primary_entry:
//        if (!get_flash_in_progress())
//        {
//            uint8_t value = read_primary_db((uint8_t)data);
//            serial_send_int_ex_debug(NULL, value, 16, NULL, serial_enable_always); // always send C outputs
//        }
//        else
//        {
//            //schedule_message(100, task_database, msg_read_primary_entry, data);
//        }
        break;
    case msg_read_backup_entry:
//        if (!get_flash_in_progress())
//        {
//            uint8_t value = read_backup_db((uint8_t)data);
//            serial_send_int_ex_debug(NULL, value, 16, NULL, serial_enable_always); // always send C outputs
//        }
//        else
//        {
//            //schedule_message(100, task_database, msg_read_primary_entry, data);
//        }
        break;
    default:
        break;
    }
}

uint8_t db_get_value(database_index const index)
{
    return ram_db[index];
}

uint16_t db_get_value16(database_index const msb, database_index const lsb)
{
    uint16_t value = ram_db[msb];
    value <<= 8;
    return value | ram_db[lsb];
}

void db_set_value(database_index const index, uint8_t const value)
{
    if (validate_database())
    {
        ram_db[index] = value;

        update_crc(ram_db, sizeof(ram_db));
    }
}

void db_set_value16(database_index const msb, database_index const lsb, uint16_t const value)
{
    if (validate_database())
    {
        ram_db[msb] = (uint8_t)(value >> 8);
        ram_db[lsb] = (uint8_t)value;

        update_crc(ram_db, sizeof(ram_db));
    }
}

void db_set_sequential_values(database_index const index, uint8_t *values, uint8_t const count)
{
    if (validate_database())
    {
        memcpy(ram_db + index, values, count * sizeof(ram_db[0]));

        update_crc(ram_db, sizeof(ram_db));
    }
}
