//******************************************************************
/*! \file io_interconnect.h
 *
 * \brief Kidde: Mongoose-fw - Define the IO interconnect functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "io_interconnect.h"
#include "app_database.h"
#include "app_serial.h"
#include "drv_intercon.h"
#include "drv_vboost.h"
#include "message.h"
#include "task_list.h"
#include <stdbool.h>
#include <string.h>

//*********************************************************************
/**
 * \brief Realarm delay to prevent Morpheus multi-gateway lockup
 */
//*********************************************************************
#define INTERCONNECT_CO_HOLDOFF    30000

//*********************************************************************
/**
 * \brief Realarm delay to prevent Morpheus multi-gateway lockup
 */
//*********************************************************************
#define INTERCONNECT_SMOKE_HOLDOFF 30000

//*********************************************************************
/**
 * \brief The period to wait, in seconds, before remote state times out
 */
//*********************************************************************
#define REMOTE_TIMEOUT             12

//*********************************************************************
/**
 * \brief The number of hi samples for a bit period to be considered high
 */
//*********************************************************************
#define VALID_SAMPLE_COUNT         (uint8_t)4

//*********************************************************************
/**
 * \brief The number of samples in one bit period
 */
//*********************************************************************
#define MAX_SAMPLE_COUNT           (uint8_t)10

//*********************************************************************
/**
 * \brief The width of a bit period, in milliseconds
 */
//*********************************************************************
#define BIT_WIDTH                  50

//*********************************************************************
/**
 * \brief The period, in milliseconds, between samples
 */
//*********************************************************************
#define SAMPLING_PERIOD            (BIT_WIDTH / MAX_SAMPLE_COUNT)

//*********************************************************************
/**
 * \brief Master Egress seems to be 140ms after Remote CO Pattern has been detected
 */
//*********************************************************************
#define CO_STROBE_LIGHT_DELAY      140

//*********************************************************************
/**
 * \brief The width of the message header in bits
 */
//*********************************************************************
#define HEADER_WIDTH               6

//*********************************************************************
/**
 * \brief The width of the message address in bits
 */
//*********************************************************************
#define ADDRESS_WIDTH              5

//*********************************************************************
/**
 * \brief The width of the message command in bits
 */
//*********************************************************************
#define COMMAND_WIDTH              10

//*********************************************************************
/**
 * \brief The width of the message data in bits
 */
//*********************************************************************
#define DATA_WIDTH                 10

//*********************************************************************
/**
 * \brief The width of the smoke message, excluding header, in bits
 */
//*********************************************************************
#define SMOKE_WIDTH                10

//*********************************************************************
/**
 * \brief The width of the CO message, excluding header, in bits
 */
//*********************************************************************
#define CO_WIDTH                   10

//*********************************************************************
/**
 * \brief The pattern for a header indicating a smoke message
 */
//*********************************************************************
#define SMOKE_HEADER               0x20

//*********************************************************************
/**
 * \brief The pattern for a header indicating a CO message
 */
//*********************************************************************
#define CO_HEADER                  0x33

//*********************************************************************
/**
 * \brief The pattern for a header indication an other message
 */
//*********************************************************************
#define OTHER_HEADER               0x30

//*********************************************************************
/**
 * \brief The pattern for a smoke message body
 */
//*********************************************************************
#define SMOKE_BODY                 0x0000

//*********************************************************************
/**
 * \brief The pattern for a CO message body
 */
//*********************************************************************
#define CO_BODY                    0x0033

//*********************************************************************
/**
 * \brief The set of bits to transfer for a smoke message
 * \note Using NRZ-M, this is the same as driving the line high
 */
//*********************************************************************
#define SMOKE_MESSAGE              0x00018000

//*********************************************************************
/**
 * \brief The set of bits to transfer for a CO message
 * \note Using NRZ-M, this is the same as the 0xa5 legacy message
 */
//*********************************************************************
#define CO_MESSAGE                 0x0001cc33

//*********************************************************************
/**
 * \brief The data that is sent with \link msg_continue \endlink.
 * \link PACK_DATA \endlink packs this into the lowest 12 bits of data.
 */
//*********************************************************************
typedef enum
{
    /**
     * \brief An interrupt has occured.
     *
     * Also used in remote smoke to force sampling to prevent a timeout.
     */
    continue_interrupt,
    /**
     * \brief Send the next bit over the interconnect.
     */
    continue_next_bit,
    /**
     * \brief Send the start transition of the strobe sync pulse.
     */
    continue_start_sync,
    /**
     * \brief Send the end transition of the strobe sync pulse.
     */
    continue_stop_sync,
    /**
     * \brief Start sending a message over the interconnect.
     *
     * This should only be sent from the current state to the current state.
     */
    continue_start_message,
    /**
     * \brief Start sending an other message.
     *
     * This should only be sent from interconnect_task().  This should only be
     * handled by nothing_state().
     */
    continue_start_other_message,
    /**
     * \brief Tristate the interconnect.
     *
     * This is useful to ensure that the interconnect is driven low sufficiently
     * long after transmitting a message.
     */
    continue_tristate,
    /**
     * \brief Samples and processes the interconnect.
     *
     * This should schedule the next sample if one is needed.
     */
    continue_sample,
    /**
     * \brief A CO message has been received.
     */
    continue_message_co,
    /**
     * \brief A smoke message has been received.
     */
    continue_message_smoke,
    /**
     * \brief Begin considering a rising edge a strobe sync pulse.
     */
    continue_strobe_search_begin,
    /**
     * \brief Stop considering a rising edge a strobe sync pulse.
     */
    continue_strobe_search_end,
    /**
     * \brief Countdown until remote smoke/CO times out.
     */
    continue_timeout,
    /**
     * \brief Interconnect smoke holdoff has timed out.
     */
    continue_smoke_holdoff,
    /**
     * \brief Interconnect CO holdoff has timed out.
     */
    continue_co_holdoff,
    /**
     * \brief Sent when the interconnect state is entered.
     */
    continue_init = 0xf000,
    /**
     * \brief Sent when the interconnect state is exited.
     */
    continue_end  = 0xffff
} continue_state;

//*********************************************************************
/**
 * \brief The mapping from addresses to address encodings
 *
 * The encoding for broadcast is not included in this table.
 *
 * These encodings are chosen to ensure at least two transitions.
 */
//*********************************************************************
static uint8_t const address_table[24]     = {0x05, 0x06, 0x07, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x11, 0x12,
                                          0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e};

//*********************************************************************
/**
 * \brief The mapping from a nibble of data to a 4B5B encoded 5 bits.
 */
//*********************************************************************
static uint8_t const encoding_4b5b_table[] = {0x1e, 0x09, 0x14, 0x15, 0x0a, 0x0b, 0x0e, 0x0f,
                                              0x12, 0x13, 0x16, 0x17, 0x1a, 0x1b, 0x1c, 0x1d};

//*********************************************************************
/**
 * \brief remote smoke state
 * \param data data to continue state or change state
 */
//*********************************************************************
void remote_smoke_state(continue_state const data);

//*********************************************************************
/**
 * \brief master smoke state
 * \param data data to continue state or change state
 */
//*********************************************************************
void master_smoke_state(continue_state const data);

//*********************************************************************
/**
 * \brief remote co state
 * \param data data to continue state or change state
 */
//*********************************************************************
void remote_co_state(continue_state const data);

//*********************************************************************
/**
 * \brief master co state
 * \param data data to continue state or change state
 */
//*********************************************************************
void master_co_state(continue_state const data);

//*********************************************************************
/**
 * \brief nothing state
 * \param data data to continue state or change state
 */
//*********************************************************************
void nothing_state(continue_state const data);

//*********************************************************************
/**
 * \brief This provides names to the entries in \link state_functions \endlink
 * \note This must remain in sync with \link state_functions \endlink.
 */
//*********************************************************************
typedef enum
{
    in_remote_smoke,
    in_master_smoke,
    in_remote_co,
    in_master_co,
    in_nothing,
    in_total
} in_states;

//*********************************************************************
/**
 * \brief An ordered list of interconnect priorities.
 *
 * The earlier in the list, the higher the priority.
 *
 * \note This must remain in sync with \link in_states \endlink.
 */
//*********************************************************************
static void (*state_functions[])(continue_state const data) = {remote_smoke_state, master_smoke_state, remote_co_state,
                                                               master_co_state, nothing_state};

//*********************************************************************
/**
 * \brief A pointer to the current state from \link state_functions \endlink.
 *
 * This is the current function that handles \link msg_continue \endlink.
 */
//*********************************************************************
static void (*current_function)(continue_state const data)  = nothing_state;

//*********************************************************************
/**
 * \brief Booleans that indicate if the interconnect is in the associated state.
 *
 * There will be one entry for each entry in \link state_functions \endlink.
 * The last entry, \link in_nothing \endlink, will always be true so that there
 * is always a current state.
 */
//*********************************************************************
static bool current_state[sizeof(state_functions) / sizeof(state_functions[0])] = {false, false, false, false, true};

//*********************************************************************
/**
 * \brief A counter to indicate if a message is for the current state.
 *
 * This is incremented whenever \link current_function \endlink is updated.
 * Only the lowest nibble is used.  \link PACK_DATA \endlink packs this into the
 * highest nibble of the \link msg_continue \endlink data.
 */
//*********************************************************************
static uint8_t state_index                                                      = 0;

//*********************************************************************
/**
 * \brief Used to pass an outgoing message from interconnect_task().
 */
//*********************************************************************
static uint32_t outbound_message                                                = 0;

//*********************************************************************
/**
 * \brief Used to pass the task that is requesting to stop drive
 */
//*********************************************************************
static task_id stopping_task                                                    = task_database;

//*********************************************************************
/**
 * \brief Disable interconnect while in remote PTT
 */
//*********************************************************************
static bool remote_ptt                                                          = false;

//*********************************************************************
/**
 * \brief A count of how many times the device is in CO holdoff
 *
 * This should never be above 1 in normal situations
 */
//*********************************************************************
static uint8_t co_holdoff                                                       = 0;

//*********************************************************************
/**
 * \brief A count of how many times the device is in smoke holdoff
 *
 * This should never be above 1 in normal situations
 */
//*********************************************************************
static uint8_t smoke_holdoff                                                    = 0;

//*********************************************************************
/**
 * \brief The part of the interconnect message that is being received
 */
//*********************************************************************
typedef enum
{
    /**
     * \brief A six bit header that is used to determine if a smoke, co, or
     *        other message is being received
     */
    receive_header,
    /**
     * \brief The header indicates that this is a co message, receiving the
     *        rest of the message to make sure that this is co
     */
    receive_co,
    /**
     * \brief The header indicates that this is a smoke message, receiving the
     *        rest of the message to make sure that this is smoke
     */
    receive_smoke,
    /**
     * \brief The header indicates that this is an other message, receiving the
     *        address that the message is sent to
     */
    receive_address,
    /**
     * \brief The other address has been received, receiving the command that
     *        the message for
     */
    receive_command,
    /**
     * \brief The other address and command have been received, receiving the
     *        data for the command
     */
    receive_data
} message_receive_state;

//*********************************************************************
/**
 * \brief Packs the \link continue_state \endlink and \link state_index \endlink
 * into a 16-bit value.
 */
//*********************************************************************
#define PACK_DATA(x) ((state_index << 12) | (x))

//*********************************************************************
/**
 * \brief Update \link current_function \endlink if necessary.
 *
 * This sends \link continue_int \endlink and \link continue_end \endlink when
 * \link current_function \endlink changes.
 */
//*********************************************************************
static void update_state(void)
{
    for (int i = 0; i < in_total; ++i)
    {
        if (current_state[i])
        {
            if (state_functions[i] != current_function)
            {
                ++state_index;
                if (state_index > 0x0f)
                {
                    state_index = 0;
                }
                // Clean up old state
                current_function(continue_end);
                current_function = state_functions[i];
                // Initialize new state
                current_function(continue_init);
            }
            return;
        }
    }
}

//*********************************************************************
/**
 * \brief Set an entry in \link current_state \endlink.
 */
//*********************************************************************
static void set_state(in_states const state)
{
    current_state[state] = true;
    update_state();
}

//*********************************************************************
/**
 * \brief Clear an entry in \link current_state \endlink.
 */
//*********************************************************************
static void clear_state(in_states const state)
{
    current_state[state] = false;
    update_state();
}

//*********************************************************************
/**
 * \brief Count the number of set bits in value
 *
 * This uses Brian Kernighan's algorithm.  This is O(n) in the number of set
 * bits rather than O(n) in the total number of bits.
 *
 * \param value The item to count set bits in
 * \returns The number of set bits in value
 */
//*********************************************************************
static uint8_t count_set_bits(uint16_t value)
{
    int count = 0;
    while (value)
    {
        value &= value - 1;
        ++count;
    }
    return count;
}

//*********************************************************************
/**
 * \brief Determine if this new bit represents a one or a zero
 *
 * \param prev The value of the previous interconnect value
 * \param current The value of the current interconnect value
 * \returns The value of the new bit as determined from the current and previous
 *          values read from the interconnect
 */
//*********************************************************************
static bool get_bit_value(bool const prev, bool const current)
{
    /** NRZ-M
     *
     * A transition is a 1
     *
     * No transition is a 0
     */
    return current != prev;
}

//*********************************************************************
/**
 * \brief Encode the address field of a message
 *
 * \param address The unencoded address of a message
 * \returns the encoded address field associated with @p address
 */
//*********************************************************************
static uint8_t encode_address(uint8_t const address)
{
    if (address == 0xff)
    {
        return 0x1f;
    }
    else if (address < sizeof(address_table) / sizeof(address_table[0]))
    {
        return address_table[address];
    }
    else
    {
        return 0xfe;
    }
}

//*********************************************************************
/**
 * \brief Decode the address field of a message
 *
 * \param address The encoded address of a message
 * \returns The decoded address field associated with @p address
 */
//*********************************************************************
static uint8_t decode_address(uint8_t const address)
{
    if (address == 0x1f)
    {
        return 0xff;
    }
    else
    {
        for (uint8_t i = 0; i < sizeof(address_table) / sizeof(address_table[0]); ++i)
        {
            if (address_table[i] == (address & 0x1f))
            {
                return i;
            }
        }
        // address was not in the table
        return 0xfe;
    }
}

//*********************************************************************
/**
 * \brief Encode a nibble using 4b5b encoding
 *
 * \param nibble The lower nibble contains the data to be encoded
 * \returns The 5 bit encoded value in the lowest 5 bits
 */
//*********************************************************************
static uint8_t encode_4b5b(uint8_t const nibble)
{
    return encoding_4b5b_table[nibble & 0x0f];
}

//*********************************************************************
/**
 * \brief Decode a nibble using 4b5b encoding
 *
 * \param nibble The lower 5 bits contains the data to be decoded
 * \returns The decoded nibble in the lower nibble
 */
//*********************************************************************
static uint8_t decode_4b5b(uint8_t const nibble)
{
    for (uint8_t i = 0; i < sizeof(encoding_4b5b_table) / sizeof(encoding_4b5b_table[0]); ++i)
    {
        if (encoding_4b5b_table[i] == (nibble & 0x1f))
        {
            return i;
        }
    }
    // nibble not in table
    return 0xfe;
}

//*********************************************************************
/**
 * \brief Handle a non-smoke/CO interconnect message
 *
 * \param address The decoded address of the target device
 * \param command The decoded message command
 * \param data The decoded message data
 */
//*********************************************************************
static void process_other_message(uint8_t const address, uint8_t const command, uint8_t const data)
{
    (void)data;
    if (address == 0xff && command == interconnect_begin_ptt)
    {
        // This is a remote PTT, interconnect will be disabled
        // until the PTT ends with a soft reset
        // serial_send_string_debug("INT: remote PTT\r", serial_enable_debug);
        remote_ptt = true;
        disable_interconnect_int();
        clear_state(in_master_co);
        clear_state(in_remote_co);
        clear_state(in_master_smoke);
        clear_state(in_remote_smoke);
        schedule_message(0, task_state, msg_ptt, task_interconnect << 8);
    }
}

//*********************************************************************
/**
 * \brief Processes an interconnect sample.
 *
 * This sends either \link continue_message_co \endlink or
 * \link continue_message_smoke \endlink if necessary.
 *
 * \param sample The value of the interconnect sample to process
 * \returns If the sample completed a message
 */
//*********************************************************************
static bool process_sample(bool const sample)
{
    // Sample the interconnect line until 10 samples are collected
    // The 10 samples are used to determine if the received Bit is high or low
    // Continue until a full byte is received

    // Store samples, initial bit is a sentinal to determine when all samples
    // have been read
    static uint16_t samples = 0x0001;

    // Add individual sample into buffer
    samples                 = (samples << 1) | sample;

    /**************************************************************
     *	\todo Handle Smoke and CO Sync Pulses
     *	Must detect Sync Pulse within 10ms of Master Alarm
     *
     *	Smoke Sync Pulse: 100ms (Falling Edge) sent 1s after initial interconnect drive, then
     *	every 19.6s from the beginning of the previous pulse's falling edge
     *
     *	CO Sync Pulse: 80ms (Rising Edge) sent 1s after the previous CO pattern's first
     *	rising edge
     **************************************************************/

    bool complete           = false;
    // When all samples have been collected, determine if the received bit was high or low
    if (samples & (1 << MAX_SAMPLE_COUNT))
    {
        static message_receive_state state = receive_header;
        static bool previous_bit           = false;
        // Subtract 1 to account for the sentinel bit
        uint8_t const high_samples         = count_set_bits(samples & 0x0fc);
        // char buffer[] = "INT: bit        \r";
        // uint16_to_hex(buffer + 9, samples & 0x3ff);
        // uint8_to_hex(buffer + 14, high_samples);
        // serial_send_string_debug(buffer, serial_enable_debug);
        bool const bit                     = high_samples >= VALID_SAMPLE_COUNT;

        // Record 8 bits until a byte has been received
        // initial bit is a sentinal to determine when all samples
        // have been read
        static uint16_t received_byte      = 0x0001;

        // Add the received bit into the buffer
        // Place bit in next empty bit space
        received_byte                      = (received_byte << 1) | get_bit_value(previous_bit, bit);
        previous_bit                       = bit;

        static uint8_t address;
        static uint8_t command;
        static uint8_t data;
        switch (state)
        {
        case receive_header:
            if (received_byte & (1 << HEADER_WIDTH))
            {
                // char buffer[] = "INT: header     \r";
                // uint16_to_hex(buffer + 12, received_byte & ((1 << HEADER_WIDTH) - 1));
                // serial_send_string_debug(buffer, serial_enable_debug);
                switch (received_byte & ((1 << HEADER_WIDTH) - 1))
                {
                case SMOKE_HEADER:
                    state = receive_smoke;
                    break;
                case CO_HEADER:
                    state = receive_co;
                    break;
                case OTHER_HEADER:
                    state = receive_address;
                    break;
                default:
                    state         = receive_header;
                    received_byte = 0x0001;
                    // Re-enable tri-state to sample immediately on interrupt
                    set_tristate();
                    previous_bit = false;
                    complete     = true;
                    break;
                }
                received_byte = 0x0001;
            }
            break;
        case receive_co:
            if (received_byte & (1 << CO_WIDTH))
            {
                // char buffer[] = "INT: co     \r";
                // uint16_to_hex(buffer + 8, received_byte & ((1 << CO_WIDTH) - 1));
                // serial_send_string_debug(buffer, serial_enable_debug);
                state = receive_header;
                if (!co_holdoff && (received_byte & ((1 << CO_WIDTH) - 1)) == CO_BODY)
                {
                    clear_state(in_remote_smoke);
                    set_state(in_remote_co);
                    schedule_message(0, task_interconnect, msg_continue, PACK_DATA(continue_message_co));
                }
                else if (co_holdoff)
                {
                    // serial_send_string_debug("INT: co holdoff\r", serial_enable_debug);
                }
                received_byte = 0x0001;
                // Re-enable tri-state to sample immediately on interrupt
                set_tristate();
                previous_bit = false;
                complete     = true;
            }
            break;
        case receive_smoke:
            if (received_byte & (1 << SMOKE_WIDTH))
            {
                // char buffer[] = "INT: smoke     \r";
                // uint16_to_hex(buffer + 11, received_byte & ((1 << SMOKE_WIDTH) - 1));
                // serial_send_string_debug(buffer, serial_enable_debug);
                state = receive_header;
                if (!smoke_holdoff && (received_byte & ((1 << SMOKE_WIDTH) - 1)) == SMOKE_BODY)
                {
                    clear_state(in_remote_co);
                    set_state(in_remote_smoke);
                    schedule_message(0, task_interconnect, msg_continue, PACK_DATA(continue_message_smoke));
                }
                else if (smoke_holdoff)
                {
                    // serial_send_string_debug("INT: smoke holdoff\r", serial_enable_debug);
                }
                received_byte = 0x0001;
                // Re-enable tri-state to sample immediately on interrupt
                set_tristate();
                previous_bit = false;
                complete     = true;
            }
            break;
        case receive_address:
            if (received_byte & (1 << ADDRESS_WIDTH))
            {
                state         = receive_command;
                address       = decode_address(received_byte & ((1 << ADDRESS_WIDTH) - 1));
                // char buffer[] = "INT: address        \r";
                // uint16_to_hex(buffer + 13, received_byte & ((1 << ADDRESS_WIDTH) - 1));
                // uint8_to_hex(buffer + 18, address);
                // serial_send_string_debug(buffer, serial_enable_debug);
                received_byte = 0x0001;
            }
            break;
        case receive_command:
            if (received_byte & (1 << COMMAND_WIDTH))
            {
                state   = receive_data;
                command = decode_4b5b(received_byte);
                command |= decode_4b5b(received_byte >> 5) << 4;
                // char buffer[] = "INT: command        \r";
                // uint16_to_hex(buffer + 13, received_byte & ((1 << COMMAND_WIDTH) - 1));
                // uint8_to_hex(buffer + 18, command);
                // serial_send_string_debug(buffer, serial_enable_debug);
                received_byte = 0x0001;
            }
            break;
        case receive_data:
            if (received_byte & (1 << DATA_WIDTH))
            {
                state = receive_header;
                data  = decode_4b5b(received_byte);
                data |= decode_4b5b(received_byte >> 5) << 4;
                process_other_message(address, command, data);
                // Re-enable tri-state to sample immediately on interrupt
                set_tristate();
                // char buffer[] = "INT: data        \r";
                // uint16_to_hex(buffer + 10, received_byte & ((1 << DATA_WIDTH) - 1));
                // uint8_to_hex(buffer + 15, data);
                // serial_send_string_debug(buffer, serial_enable_debug);
                previous_bit  = false;
                received_byte = 0x0001;
                complete      = true;
            }
            break;
        default:
            break;
        }

        // Clear sample history
        samples = 0x0001;
    }
    return complete;
}

//*********************************************************************
/**
 * \brief Encodes the message
 *
 * \param address The target address for the message
 * \param command The command to send
 * \param data The data associated with the command
 * \returns The encoded command, ready to be sent to transmit_message()
 */
//*********************************************************************
static uint32_t encode_other_message(uint8_t const address, uint8_t const command, uint8_t const data)
{
    uint32_t encoded = 0x00000001;

    // Add the header
    encoded <<= HEADER_WIDTH;
    encoded |= OTHER_HEADER;

    // Add the address
    encoded <<= ADDRESS_WIDTH;
    encoded |= encode_address(address) & ((1 << ADDRESS_WIDTH) - 1);

    // Add the command
    encoded <<= COMMAND_WIDTH;
    encoded |= (encode_4b5b(command >> 4) & 0x1f) << 5;
    encoded |= encode_4b5b(command) & 0x1f;

    // Add the data
    encoded <<= DATA_WIDTH;
    encoded |= (encode_4b5b(data >> 4) & 0x1f) << 5;
    encoded |= encode_4b5b(data) & 0x1f;

    // char buffer[] = "INT: encoded         \r";
    // uint32_to_hex(buffer + 13, encoded);
    // serial_send_string_debug(buffer, serial_enable_debug);
    return encoded;
}

//*********************************************************************
/**
 * \brief Transmit the message over the interconnect
 *
 * \warning This is a non-reentrant function.
 *
 * Calling this with a non-zero value resets the internal state and transmits
 * the first bit of the message.  The most significant set bit is used as a
 * sentinel value to determine the number of bits to transmit.  This means that
 * a maximum of 31 bits can be transmitted.
 *
 * Calling this with a zero value transmits the next bit of the message.
 *
 * \param message The message to transmit over the interconnect
 * \returns If message transmission completed
 */
//*********************************************************************
static bool transmit_message(uint32_t message)
{
    static uint32_t output_buffer = 0x01;
    static bool previous_bit      = false;
    if (message)
    {
        previous_bit  = false;
        /** Reverse bit order */
        output_buffer = 0x01;
        while (message != 0x01)
        {
            output_buffer <<= 1;
            output_buffer |= message & 0x01;
            message >>= 1;
        }
    }

    if (output_buffer & 0x01)
    {
        previous_bit = !previous_bit;
    }
    if (previous_bit)
    {
        drive_high();
    }
    else
    {
        drive_low();
    }
    output_buffer >>= 1;
    return output_buffer == 0x01;
}

//*********************************************************************
/**
 * \brief Handle \link msg_continue \endlink when in remote smoke.
 *
 * This handles received messages and strobe sync pulses.  This state is exited
 * when a sufficiently long period has elapsed without a smoke message.
 *
 * \param data The \link continue_state \endlink to process
 */
//*********************************************************************
void remote_smoke_state(continue_state const data)
{
    static bool strobe_search      = false;
    static uint8_t timeout_seconds = REMOTE_TIMEOUT;
    switch (data)
    {
    case continue_init:
        // serial_send_string_debug("INT: begin remote_smoke\r", serial_enable_debug);
        strobe_search   = false;
        timeout_seconds = REMOTE_TIMEOUT;
        schedule_message(1000, task_interconnect, msg_continue, PACK_DATA(continue_timeout));
        schedule_message(0, task_state, msg_enter_smoke_alarm, INT_REMOTE);
        schedule_message(200, task_interconnect, msg_continue, PACK_DATA(continue_strobe_search_begin));
        schedule_message(300, task_interconnect, msg_continue, PACK_DATA(continue_strobe_search_end));
        break;
    case continue_end:
        // serial_send_string_debug("INT: end_remote_smoke\r", serial_enable_debug);
        schedule_message(0, task_state, msg_exit_smoke_alarm, INT_REMOTE);
        ++smoke_holdoff;
        schedule_message(INTERCONNECT_SMOKE_HOLDOFF, task_interconnect, msg_continue,
                         PACK_DATA(continue_smoke_holdoff));
        break;
    case continue_interrupt:
        if (strobe_search)
        {
            // serial_send_string_debug("INT: strobe sync\r", serial_enable_debug);
            schedule_message(19550, task_interconnect, msg_continue, PACK_DATA(continue_strobe_search_begin));
            schedule_message(19650, task_interconnect, msg_continue, PACK_DATA(continue_strobe_search_end));
            // schedule_message(0, task_strobe_light_output, msg_strobe_sync, STROBE_DELAY_SMOKE);
            strobe_search = false;
        }
        if (read_interconnect())
        {
            schedule_message(1600, task_interconnect, msg_turn_on, 0);
            disable_interconnect_int();
            process_sample(true);
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
        else
        {
            set_tristate();
        }
        break;
    case continue_sample:
        if (process_sample(read_interconnect()))
        {
            set_tristate(); // Enable interrupt
        }
        else
        {
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
    case continue_message_smoke:
        timeout_seconds = REMOTE_TIMEOUT;
        break;
    case continue_strobe_search_begin:
        // serial_send_string_debug("INT: begin strobe search\r", serial_enable_debug);
        strobe_search = true;
        break;
    case continue_strobe_search_end:
        // serial_send_string_debug("INT: end strobe search\r", serial_enable_debug);
        strobe_search = false;
        break;
    case continue_timeout:
        if (timeout_seconds == 2)
        {
            // Check to see if interconnect line is still remote smoke
            schedule_message(0, task_interconnect, msg_continue, 0);
        }
        if (--timeout_seconds)
        {
            schedule_message(1000, task_interconnect, msg_continue, PACK_DATA(continue_timeout));
        }
        else
        {
            clear_state(in_remote_smoke);
        }
    default:
        break;
    }
}

//*********************************************************************
/**
 * \brief Handle \link msg_continue \endlink when in master smoke.
 *
 * This drives the interconnect high and transmits the strobe sync pulse.  As
 * the interconnect is always high when in master smoke, this does not attempt
 * to read interconnect messages.
 *
 * \param data The \link continue_state \endlink to process
 */
//*********************************************************************
void master_smoke_state(continue_state const data)
{
    switch (data)
    {
    case continue_init:
        // Start smoke state
        // serial_send_string_debug("INT: begin master_smoke\r", serial_enable_debug);
        transmit_message(SMOKE_MESSAGE);
        schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_next_bit));
        schedule_message(1000, task_interconnect, msg_continue, PACK_DATA(continue_start_sync));
        break;
    case continue_end:
        // End smoke state
        // serial_send_string_debug("INT: end master_smoke\r", serial_enable_debug);
        schedule_message(0, stopping_task, msg_ready, task_interconnect);
        drive_low();
        break;
    case continue_next_bit:
        // Send next bit
        if (!transmit_message(0))
        {
            // Smoke message is not finished
            schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_next_bit));
        }
        break;
    case continue_start_sync:
        // Start strobe sync pulse
        drive_low();
        schedule_message(100, task_interconnect, msg_continue, PACK_DATA(continue_stop_sync));
        break;
    case continue_stop_sync:
        // Stop strobe sync pulse
        drive_high();
        schedule_message(19500, task_interconnect, msg_continue, PACK_DATA(continue_start_sync));
        break;
    default:
        break;
    }
}

//*********************************************************************
/**
 * \brief Handle \link msg_continue \endlink when in remote CO.
 *
 * This handles received messages and strobe sync pulses.  This state is exited
 * when a sufficiently long period has elapsed without a CO message.
 *
 * \param data The \link continue_state \endlink to process
 */
//*********************************************************************
void remote_co_state(continue_state const data)
{
    static uint8_t pattern_count   = 0;
    static bool strobe_search      = false;
    static uint8_t timeout_seconds = REMOTE_TIMEOUT;
    switch (data)
    {
    case continue_init:
        // serial_send_string_debug("INT: begin remote_co\r", serial_enable_debug);
        pattern_count   = 0;
        strobe_search   = false;
        timeout_seconds = REMOTE_TIMEOUT;
        schedule_message(1000, task_interconnect, msg_continue, PACK_DATA(continue_timeout));
        schedule_message(0, task_state, msg_enter_co_alarm, INT_REMOTE);
        break;
    case continue_end:
        // serial_send_string_debug("INT: end remote_co\r", serial_enable_debug);
        schedule_message(0, task_state, msg_exit_co_alarm, INT_REMOTE);
        ++co_holdoff;
        schedule_message(INTERCONNECT_CO_HOLDOFF, task_interconnect, msg_continue, PACK_DATA(continue_co_holdoff));
        break;
    case continue_interrupt:
        if (strobe_search)
        {
            // serial_send_string_debug("INT: strobe sync\r", serial_enable_debug);
            // schedule_message(0, task_strobe_light_output, msg_strobe_sync, STROBE_DELAY_CO);
            pattern_count = 1;
            strobe_search = false;
        }
        if (read_interconnect())
        {
            schedule_message(1600, task_interconnect, msg_turn_on, 0);
            disable_interconnect_int();
            process_sample(true);
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
        else
        {
            set_tristate();
        }
        break;
    case continue_sample:
        if (process_sample(read_interconnect()))
        {
            set_tristate(); // Enable interrupt
        }
        else
        {
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
        break;
    case continue_message_co:
        timeout_seconds = REMOTE_TIMEOUT;
        if (++pattern_count >= 7)
        {
            pattern_count = 0;
            // schedule_message(CO_STROBE_LIGHT_DELAY, task_strobe_light_output, msg_strobe_sync, HARD_RESET_STROBE);
        }
        schedule_message(200, task_interconnect, msg_continue, PACK_DATA(continue_strobe_search_begin));
        schedule_message(300, task_interconnect, msg_continue, PACK_DATA(continue_strobe_search_end));
        break;
    case continue_strobe_search_begin:
        // serial_send_string_debug("INT: begin strobe search\r", serial_enable_debug);
        strobe_search = true;
        break;
    case continue_strobe_search_end:
        // serial_send_string_debug("INT: end strobe search\r", serial_enable_debug);
        strobe_search = false;
        break;
    case continue_timeout:
        if (--timeout_seconds)
        {
            schedule_message(1000, task_interconnect, msg_continue, PACK_DATA(continue_timeout));
        }
        else
        {
            clear_state(in_remote_co);
        }
    default:
        break;
    }
}

//*********************************************************************
/**
 * \brief Handle \link msg_continue \endlink when in master CO.
 *
 * This transmits the CO interconnect pattern and the strobe sync pulse.  This
 * also reads interconnect messages when it is not transmitting a pattern or
 * strobe sync pulse.
 *
 * \param data The \link continue_state \endlink to process
 */
//*********************************************************************
void master_co_state(continue_state const data)
{
    static uint8_t patterns_till_sync = 7;
    static bool driving               = false;
    static bool receiving             = false;
    switch (data)
    {
    case continue_init:
        // Start CO state
        // serial_send_string_debug("INT: begin master_co\r", serial_enable_debug);
        patterns_till_sync = 7;
        driving            = true;
        receiving          = false;
        transmit_message(CO_MESSAGE);
        schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_next_bit));
        break;
    case continue_end:
        // End CO state
        // serial_send_string_debug("INT: end master_co\r", serial_enable_debug);
        schedule_message(0, stopping_task, msg_ready, task_interconnect);
        if (driving)
        {
            drive_low();
        }
        if (receiving)
        {
            set_tristate(); // Enable interrupt
        }
        break;
    case continue_interrupt:
        if (!driving && read_interconnect())
        {
            schedule_message(1600, task_interconnect, msg_turn_on, 0);
            disable_interconnect_int();
            receiving = true;
            process_sample(true);
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
        break;
    case continue_next_bit:
        // Send next bit
        if (transmit_message(0))
        {
            // Finished the message
            if (!--patterns_till_sync)
            {
                patterns_till_sync = 7;
                schedule_message(250, task_interconnect, msg_continue, PACK_DATA(continue_start_sync));
            }
            schedule_message(200, task_interconnect, msg_continue, PACK_DATA(continue_tristate));
            schedule_message(4950, task_interconnect, msg_continue, PACK_DATA(continue_start_message));
        }
        else
        {
            // CO message is not finished
            schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_next_bit));
        }
        break;
    case continue_start_sync:
        // Start strobe sync pulse
        if (!receiving)
        {
            // schedule_message(0, task_strobe_light_output, msg_strobe_sync, HARD_RESET_STROBE);
            driving = true;
            drive_high();
            schedule_message(80, task_interconnect, msg_continue, PACK_DATA(continue_stop_sync));
        }
        break;
    case continue_stop_sync:
        // Stop strobe sync pulse
        drive_low();
        schedule_message(200, task_interconnect, msg_continue, PACK_DATA(continue_tristate));
        break;
    case continue_start_message:
        // Begin transmitting the next message
        if (receiving)
        {
            // Skip the message if receiving, but schedule strobe sync and next message
            if (!--patterns_till_sync)
            {
                patterns_till_sync = 7;
                schedule_message(1000, task_interconnect, msg_continue, PACK_DATA(continue_start_sync));
            }
            schedule_message(5700, task_interconnect, msg_continue, PACK_DATA(continue_start_message));
        }
        else
        {
            driving = true;
            transmit_message(CO_MESSAGE);
            schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_next_bit));
        }
        break;
    case continue_tristate:
        driving = false;
        set_tristate();
        break;
    case continue_sample:
        if (process_sample(read_interconnect()))
        {
            set_tristate(); // Enable interrupt
        }
        else
        {
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
        break;
    default:
        break;
    }
}

//*********************************************************************
/**
 * \brief Handle \link msg_continue \endlink when in notin smoke/CO
 * master/remote.
 *
 * This receives any messages that are transmitted over the interconnect.
 *
 * \param data The \link continue_state \endlink to process
 */
//*********************************************************************
void nothing_state(continue_state const data)
{
    static bool vboost       = false;
    static bool transmitting = false;
    static bool sampling     = false;
    static bool last_bit     = false;
    switch (data)
    {
    case continue_init:
        // serial_send_string_debug("INT: begin nothing\r", serial_enable_debug);
        vboost       = false;
        transmitting = true;
        sampling     = false;
        last_bit     = false;
        drive_low();
        schedule_message(200, task_interconnect, msg_continue, PACK_DATA(continue_tristate));
        break;
    case continue_end:
        if (vboost)
        {
            vboost = false;
            vboost_interconnect_off();
        }
        // serial_send_string_debug("INT: end nothing\r", serial_enable_debug);
        break;
    case continue_interrupt:
        if (read_interconnect())
        {
            schedule_message(1600, task_interconnect, msg_turn_on, 0);
            sampling = true;
            disable_interconnect_int();
            process_sample(true);
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
        else
        {
            set_tristate();
        }
        break;
    case continue_next_bit:
        // Send next bit
        if (transmit_message(0))
        {
            last_bit = true;
            schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_tristate));
            vboost = false;
            vboost_interconnect_off();
        }
        else
        {
            // Smoke message is not finished
            schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_next_bit));
        }
        break;
    case continue_start_other_message:
        if (vboost && !transmitting && !sampling)
        {
            transmitting = true;
            transmit_message(outbound_message);
            schedule_message(BIT_WIDTH, task_interconnect, msg_continue, PACK_DATA(continue_next_bit));
        }
        else if (sampling || transmitting)
        {
            schedule_message(100, task_interconnect, msg_continue, PACK_DATA(continue_start_other_message));
        }
        else
        {
            vboost = true;
            vboost_interconnect_on();
            schedule_message(50, task_interconnect, msg_continue, PACK_DATA(continue_start_other_message));
        }
        break;
    case continue_tristate:
        if (last_bit)
        {
            last_bit = false;
            drive_low();
            schedule_message(200, task_interconnect, msg_continue, PACK_DATA(continue_tristate));
        }
        else
        {
            transmitting = false;
            set_tristate();
        }
        break;
    case continue_sample:
        if (process_sample(read_interconnect()))
        {
            sampling = false;
            set_tristate(); // Enable interrupt
        }
        else
        {
            schedule_message(SAMPLING_PERIOD, task_interconnect, msg_continue, PACK_DATA(continue_sample));
        }
        break;
    default:
        break;
    }
}

void interconnect_task(message_id const message, uint16_t const data)
{
    if (remote_ptt)
    {
        if (message == msg_end_drive_co || message == msg_end_drive_smoke)
        {
            // Have to ACK these to continue the remote PTT
            schedule_message(0, (task_id)data, msg_ready, task_interconnect);
        }
        // Ignore everything else if in remote PTT
        return;
    }
    switch (message)
    {
    case msg_init:
        state_index = 0;
        for (int i = 0; i < in_total; ++i)
        {
            current_state[i] = false;
        }
        current_state[in_nothing] = true;
        current_function          = nothing_state;
        remote_ptt                = false;
        co_holdoff                = 0;
        smoke_holdoff             = 0;

        interconnect_init();
        nothing_state(continue_init);
        // sample interconnect on power up
        schedule_message(1000, task_interconnect, msg_continue, 0);
        break;
    case msg_begin_drive_co:
        set_state(in_master_co);
        break;
    case msg_end_drive_co:
        stopping_task = (task_id)data;
        clear_state(in_master_co);
        break;
    case msg_begin_drive_smoke:
        set_state(in_master_smoke);
        break;
    case msg_end_drive_smoke:
        stopping_task = (task_id)data;
        clear_state(in_master_smoke);
        break;
    case msg_continue:
        // Ignore messages from old states
        if ((data & 0x0fff) == continue_smoke_holdoff)
        {
            if (!--smoke_holdoff)
            {
                // Force sample in case the interconnect is in smoke alarm
                schedule_message(0, task_interconnect, msg_continue, 0);
            }
        }
        else if ((data & 0x0fff) == continue_co_holdoff)
        {
            --co_holdoff;
        }
        else if (((data >> 12) & 0x0f) == state_index || // msg_continue from current function
                 data == 0)                              // Interrupt handler message
        {
            current_function((continue_state)(data & 0x0fff));
        }
        break;
    case msg_interconnect_message:
        outbound_message = encode_other_message(0xff, data >> 8, data);
        schedule_message(0, task_interconnect, msg_continue, PACK_DATA(continue_start_other_message));
        break;
    default:
        break;
    }
}
