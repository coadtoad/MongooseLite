//******************************************************************
/*! \file state.c
 *
 * \brief Kidde: Mongoose-fw - Implement the state functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "state.h"
#include "app_database.h"
#include "app_events.h"
#include "app_expression.h"
#include "app_serial.h"
#include "app_supervisor.h"
#include "cal_0_ppm.h"
#include "cal_150_ppm.h"
#include "cal_400_ppm.h"
#include "cal_circuit.h"
#include "clean_air_value.h"
#include "database.h"
#include "debug_ids_priority.h"
#include "drv_button.h"
#include "drv_serial.h"
#include "drv_sounder.h"
#include "drv_timer.h"
#include "drv_vboost.h"
#include "drv_watchdog.h"
#include "in_ac.h"
#include "in_co.h"
#include "in_day_count.h"
#include "in_photo.h"
#include "io_interconnect.h"
#include "key_value_store.h"
#include "main.h"
#include "message.h"
#include "out_voice.h"
#include "ptt.h"
#include "task_list.h"
#include <stddef.h>
#include <stdint.h>
#include <string.h>

//*********************************************************************
/**
 * \brief Value for completed calibration
 */
//*********************************************************************
#define CALIBRATION_DONE           0xa5

//*********************************************************************
/**
 * \brief General smoke hush timeout for our products
 */
//*********************************************************************
#define SMOKE_HUSH_TIMEOUT         (9 * ONE_MINUTE)

//*********************************************************************
/**
 * \brief Network error hush timeout after 24 hours
 */
//*********************************************************************
#define NETWORK_ERROR_HUSH_TIMEOUT (24ul * 60) // 24 hours

//*********************************************************************
/**
 * \brief 4 minute interval for CO conserve
 */
//*********************************************************************
#define CO_CONSERVE_INTERVAL       (4 * ONE_MINUTE)

//*********************************************************************
/**
 * \brief "Soft reset hex value
 */
//*********************************************************************
#define SOFT_RESET                 0x8000

//*********************************************************************
/**
 * \brief Initial voice delay time interval
 */
//*********************************************************************
#define DEFAULT_VOICE_DELAY        500

//*********************************************************************
/**
 * \brief Namee of the current priority state
 */
//*********************************************************************
static priority_name current_priority = prior_name_count;

//*********************************************************************
/**
 * \brief Boolean if a boot up has completed
 */
//*********************************************************************
static bool boot_up_complete          = false;

//*********************************************************************
/**
 * \brief Boolean if the final test has completed
 */
//*********************************************************************
static bool final_test_complete       = true;

// this holds the ac and dc versions of each exp, fill in the same one

// This volatile part of priority should end up in .bss
//*********************************************************************
/**
 * \brief Photo fault condition value enum
 * \note This holds the ac and dc versions of each exp, fill in the same one
 * \note This volatile part of priority should end up in .bss
 */
//*********************************************************************
typedef enum
{
    photo_fault_none                 = 0x0000,
    photo_fault_forward_ambient      = 0x0001,
    photo_fault_back_ambient         = 0x0002,
    photo_fault_forward_ir_led       = 0x0004,
    photo_fault_back_ir_led          = 0x0008,
    photo_fault_clean_air_update     = 0x0010,
    photo_fault_ptt_failure          = 0x0020,
    photo_fault_blue_led_supervision = 0x0040,
    photo_fault_afe                  = 0x0080
} photo_fault_condition;

//*********************************************************************
/**
 * \brief CO fault condition value enum
 */
//*********************************************************************
typedef enum
{
    co_fault_none         = 0x0000,
    co_fault_sensor_open  = 0x0001,
    co_fault_sensor_short = 0x0002,
    co_fault_ptt_failure  = 0x0004,
} co_fault_condition;

//*********************************************************************
/**
 * \brief Type of priority, desired priority or current expressed priority
 */
//*********************************************************************
typedef enum
{
    DesiredPriority          = 0x00,
    CurrentExpressedPriority = 0x01,
} priority_type;

//*********************************************************************
/**
 * \brief Wireless Module fault condition states enum
 */
//*********************************************************************
typedef enum
{
    wm_no_fault           = 0x00,
    wm_network_error_hush = 0x01,
    wm_remote_fault       = 0x02,
    wm_battery_eol        = 0x04,
    wm_cci_failure        = 0x08,
} wm_fault_condition;

// This non-volatile part of priority should end up in .rodata
// needs to match "priority_name" in state.h
static expression_id const priorities[prior_name_count] = {
    // high priority expressions
    exp_fault_fatal_first_hour, // prior_fault_fatal
    exp_power_on,               // prior_power_on
    exp_master_smoke,           // prior_smoke
    exp_remote_smoke,           // prior_remote_smoke
    exp_master_co,              // prior_co
    exp_co_conserve,            // prior_co_conserve
    exp_remote_co,              // prior_co_remote
    exp_ptt_initialize,         // prior_ptt_init
    exp_default,                // prior_ptt
    exp_smoke_hush,             // prior_smoke_hush
    exp_eol_first_hour,         // prior_eol_fatal
    exp_battery_low_first_hour, // prior_battery_low_no_hush
    exp_fault_photo_first_hour, // prior_fault_photo
    exp_fault_co_first_hour,    // prior_fault_co
    exp_eol_first_hour,         // prior_eol
    exp_button_stuck,           // prior_button_stuck
    // low priority expressions
    exp_battery_none_first_hour, // prior_battery_none
    exp_battery_low_first_hour,  // prior_battery_low
    exp_alarm_memory_smoke,      // prior_alarm_memory_smoke
    exp_alarm_memory_co,         // prior_alarm_memory_co
    exp_alarm_memory_co,         // prior_alarm_co_peak
    exp_battery_low_hush,        // prior_battery_low_hush
    exp_eol_hush,                // prior_eol_hush
    // calibration states
    exp_cal_pass,                   // prior_cal_pass
    exp_cal_pass_co_circuit,        // prior_cal_pass_co_circuit
    exp_cal_pass_400_ppm,           // prior_cal_pass_400_ppm
    exp_cal_fail,                   // prior_cal_fail
    exp_cal_co_circuit_no_button,   // prior_cal_co_circuit_no_button
    exp_cal_co_circuit,             // prior_cal_co_circuit
    exp_cal_battery_no_button,      // prior_cal_battery_no_button
    exp_cal_battery,                // prior_cal_battery
    exp_cal_photo_fast,             // prior_cal_lic
    exp_cal_photo_fast,             // prior_cal_2_point
    exp_cal_0_ppm,                  // prior_cal_0_ppm
    exp_cal_150_ppm,                // prior_cal_150_ppm
    exp_cal_400_ppm,                // prior_cal_400_ppm
    exp_cal_inline_smoke_wait,      // prior_cal_inline_wait
    exp_cal_inline_smoke_stabilize, // prior_cal_inline_stabilize
    exp_cal_inline_smoke_average,   // prior_cal_inline_average
    exp_cal_inline_smoke_verify,    // prior_cal_inline_verify
    exp_cal_fail,                   // prior_cal_inline_error
    exp_default};                   // prior_dc_blink
                                    // prior_name_count
//*********************************************************************
/**
 * \brief Current priority boolean, priority state or no state
 */
//*********************************************************************
static bool current[prior_name_count] = {0};

//*********************************************************************
/**
 * \brief variable to hold current photo fault states. 
 */
//*********************************************************************
static photo_fault_condition fault_photo = photo_fault_none;

//*********************************************************************
/**
 * \brief Retain CO push to test (CO sensor) fault after a forced soft reset
 * \todo IMPLEMENT ME
 *
 * These variables should be no-init.  This needs to be implemented for Keil
 */
//*********************************************************************
static co_fault_condition fault_co __attribute__((section(".bss.noinit")));

//*********************************************************************
/**
 * \brief Locate an active alarm in interconnect or no alarm active
 */
//*********************************************************************
static bool locate_active        = false;

//*********************************************************************
/**
 * \brief Determine if smoke is in hushable range boolean
 */
//*********************************************************************
static bool smoke_hushable       = true;

//*********************************************************************
/**
 * \brief Determine if AC power is present or not boolean
 */
//*********************************************************************
static bool ac_present           = 0;

//*********************************************************************
/**
 * \brief Conserve time passed or not boolean
 */
//*********************************************************************
static bool conserve_time_passed = false;

//*********************************************************************
/**
 * \brief Determine if the button voice activates
 */
//*********************************************************************
static bool button_voice         = 0;

//*********************************************************************
/**
 * \brief Determine if memory flash is in progress or not
 */
//*********************************************************************
static bool flash_in_progress    = false;

//*********************************************************************
/**
 * \brief Determine if a PTT reset has occured
 * \todo IMPLEMENT ME
 *
 * These variables should be no-init.  This needs to be implemented for Keil
 */
//*********************************************************************

void set_flash_in_progress(bool status)
{
    flash_in_progress = status;
}

bool get_flash_in_progress(void)
{
    return flash_in_progress;
}

// ------------------------  priority state support functions

//*********************************************************************
/**
 * \brief Determine the highest priority task and returns that task count
 * \returns prior_name_count : The count value of the highest priority task found
 */
//*********************************************************************
static priority_name get_highest_current_priority(void)
{
    // Search for highest priority that should be expressed
    priority_name i = prior_name_count;
    for (i = (priority_name)0; i < prior_name_count && !current[i]; ++i)
        ;
    return i;
}

//*********************************************************************
/**
 * \brief Type of priority, desired priority or current expressed priority
 */
//*********************************************************************

void clear_priority(priority_name priority);

bool get_priority(priority_name priority)
{
    return current[priority];
}

void set_priority(priority_name priority)
{
    switch (priority)
    {
    case prior_fault_fatal:
        obliterate();
        memset(current, 0, sizeof(current));
        break;
    case prior_power_on:
        break;
    case prior_smoke:
        clear_priority(prior_alarm_memory_smoke);
        break;
    case prior_remote_smoke:
        break;
    case prior_co:
        clear_priority(prior_alarm_memory_co); // also clears alarm memory peak
        // start co - battery conserve on first entry of co alarm
        unschedule_messages_tm(task_state, msg_enter_co_alarm_conserve); // clear any old conserves
        schedule_message(CO_CONSERVE_INTERVAL, task_state, msg_enter_co_alarm_conserve, 0);
        break;
    case prior_co_conserve:
        break;
    case prior_co_remote:
        break;
    case prior_ptt_init:
        break;
    case prior_ptt:
        break;
    case prior_smoke_hush:
        unschedule_messages_tm(task_state, msg_smoke_hush_timeout);
        schedule_message(SMOKE_HUSH_TIMEOUT, task_state, msg_smoke_hush_timeout, prior_smoke_hush);
        break;
    case prior_eol_fatal:
        break;
    case prior_battery_low_no_hush:
        break;
    case prior_fault_photo:
        break;
    case prior_fault_co:
        break;
    case prior_eol:
        break;
    case prior_button_stuck:
        break;
        // low priority expressions
    case prior_battery_none:
        break;
    case prior_battery_low:
        clear_priority(prior_battery_none);
        break;
    case prior_alarm_memory_smoke:
        unschedule_messages_tm(task_state, msg_expression_timeout);
        schedule_message(ONE_DAY, task_state, msg_expression_timeout, prior_alarm_memory_smoke);
        break;
    case prior_alarm_memory_co:
        unschedule_messages_tm(task_state, msg_expression_timeout);
        schedule_message(ONE_DAY, task_state, msg_expression_timeout, prior_alarm_memory_co);
        break;
    case prior_alarm_co_peak:
        unschedule_messages_tm(task_state, msg_expression_timeout);
        schedule_message(ONE_DAY, task_state, msg_expression_timeout, prior_alarm_co_peak);
        break;
    case prior_battery_low_hush:
        unschedule_messages_tm(task_state, msg_battery_hush_timeout);
        schedule_message(ONE_DAY, task_state, msg_battery_hush_timeout, prior_battery_low_hush);
        break;
    case prior_eol_hush:
        unschedule_messages_tm(task_state, msg_eol_hush_timeout);
        schedule_message(ONE_DAY, task_state, msg_eol_hush_timeout, prior_eol_hush);
        break;
        // calibration states
    case prior_cal_pass:
        break;
    case prior_cal_pass_co_circuit:
        break;
    case prior_cal_pass_400_ppm:
        break;
    case prior_cal_fail:
        break;
    case prior_cal_co_circuit_no_button:
        break;
    case prior_cal_co_circuit:
        schedule_message(0, task_cal_circuit, msg_cal_begin, 0);
        break;
    case prior_cal_battery_no_button:
        break;
    case prior_cal_battery:
        schedule_message(0, task_battery_cal, msg_cal_begin, 0);
        break;
    case prior_cal_lic:
        schedule_message(0, task_cal_lic, msg_cal_begin, 0);
        break;
    case prior_cal_2_point:
        schedule_message(0, task_cal_2_point, msg_cal_begin, 0);
        break;
    case prior_cal_0_ppm:
        schedule_message(0, task_cal_0_ppm, msg_cal_begin, 0);
        break;
    case prior_cal_150_ppm:
        schedule_message(0, task_cal_150_ppm, msg_cal_begin, 0);
        break;
    case prior_cal_400_ppm:
        schedule_message(0, task_cal_400_ppm, msg_cal_begin, 0);
        break;
    // special case for all cal inline settings.
    // Clear all priorities, then set the one sent.
    case prior_cal_inline_wait:
        schedule_message(0, task_cal_inline_smoke, msg_cal_begin, 0);
    case prior_cal_inline_stabilize:
    case prior_cal_inline_average:
    case prior_cal_inline_verify:
    case prior_cal_inline_error:
        current[prior_cal_inline_wait]      = 0;
        current[prior_cal_inline_stabilize] = 0;
        current[prior_cal_inline_average]   = 0;
        current[prior_cal_inline_verify]    = 0;
        current[prior_cal_inline_error]     = 0;
        break;
    case prior_dc_blink:
        break;
    default:
        break;
    }

    // actually set the priority of the request.
    current[priority] = true;
}

void clear_priority(priority_name priority)
{
    switch (priority)
    {
    case prior_fault_fatal:
        // error.. this is not an exitable state
        break;
    case prior_power_on:
        // automatically called by the expression itself.
        break;
    case prior_smoke:
        break;
    case prior_remote_smoke:
        break;
    case prior_co:
        break;
    case prior_co_conserve:
        break;
    case prior_co_remote:
        break;
    case prior_ptt_init:
        break;
    case prior_ptt:
        break;
    case prior_smoke_hush:
        break;
    case prior_eol_fatal:
        break;
    case prior_battery_low_no_hush:
        break;
    case prior_fault_photo:
        break;
    case prior_fault_co:
        break;
    case prior_eol:
        break;
    case prior_button_stuck:
        break;

        // low priority expressions
    case prior_battery_none:
        break;
    case prior_battery_low:
        break;
    case prior_alarm_memory_smoke:
    case prior_alarm_memory_co:
    case prior_alarm_co_peak:
        // special case - alarm memory clear, clears all alarm memories at once
        unschedule_messages_tm(task_state, msg_expression_timeout);
        current[prior_alarm_memory_smoke] = 0;
        current[prior_alarm_memory_co]    = 0;
        current[prior_alarm_co_peak]      = 0;
        break;
    case prior_battery_low_hush:
        break;
    case prior_eol_hush:
        break;

        // calibration states
    case prior_cal_pass:
        break;
    case prior_cal_pass_co_circuit:
        break;
    case prior_cal_pass_400_ppm:
        break;
    case prior_cal_fail:
        break;
    case prior_cal_co_circuit_no_button:
        break;
    case prior_cal_co_circuit:
        schedule_message(0, task_cal_circuit, msg_cal_begin, 0);
        break;
    case prior_cal_battery_no_button:
        break;
    case prior_cal_battery:
        break;
    case prior_cal_lic:
        break;
    case prior_cal_2_point:
        break;
    case prior_cal_0_ppm:
        break;
    case prior_cal_150_ppm:
        break;
    case prior_cal_400_ppm:
        break;
    case prior_cal_inline_wait:
        break;
    case prior_cal_inline_stabilize:
        break;
    case prior_cal_inline_average:
        break;
    case prior_cal_inline_verify:
        break;
    case prior_cal_inline_error:
        break;
    case prior_dc_blink:
        break;
    default:
        break;
    }

    current[priority] = false;
}

// current[---------------------------------------------------]

//*********************************************************************
/**
 * \brief Output priority information via serial data
 * \param priority The listed priority being expressed
 * \param type The type of priority being expressed
 */
//*********************************************************************
void serial_print_priority(priority_name priority, priority_type type)
{
    static priority_name previous_priority[2];

    uint16_t const serial_flags = db_get_value16(db_serial_enable_msb, db_serial_enable_lsb);
    if (serial_flags && serial_enable_priority) // enable_serial_priority
    {
        if (type == CurrentExpressedPriority)
        {
            serial_send_string_debug("Express Priority  -  ", serial_enable_priority);
            serial_send_msg_priority_debug(priority, serial_enable_priority);
            serial_send_string_debug("\r", serial_enable_priority);
        }
        else if (priority != previous_priority[type]) // prevent print every time.
        {
            previous_priority[type] = priority;
            if (type == DesiredPriority)
            {
                serial_send_string_debug("Desired Priority  -  ", serial_enable_priority);
                serial_send_msg_priority_debug(priority, serial_enable_priority);
                serial_send_string_debug("\r", serial_enable_priority);
            }
        }
    }
}

//*********************************************************************
/**
 * \brief Update the priority for a photo fault event
 */
//*********************************************************************
static void update_photo_fault_priority(void)
{
    if (fault_photo)
    {
        // if we are currently not in Photo fault, update the fault counter.
        if (!get_priority(prior_fault_photo))
        {
            uint16_t fault_count = db_get_value16(db_counter_smoke_chamber_msb, db_counter_smoke_chamber_lsb);
            db_set_value16(db_counter_smoke_chamber_msb, db_counter_smoke_chamber_lsb, ++fault_count);
        }

        // Set photo fault priority
        set_priority(prior_fault_photo);

        serial_send_string_debug("Photo Fault: ", serial_enable_debug);
        char buffer[] = "  \r";
        uint8_to_hex(buffer, fault_photo);
        serial_send_string_debug(buffer, serial_enable_debug);
    }
    else
    {
        if (get_priority(prior_fault_photo))
        {
            // Clear photo fault priority
            clear_priority(prior_fault_photo);
            serial_send_string_debug("Photo Fault Clear\r", serial_enable_debug);
            schedule_message(0, task_events, msg_record_history, event_fault_photo_clr);
        }
    }
}

//*********************************************************************
/**
 * \brief Update the alarm status with key value store updates
 * \param priority The listed priority being expressed
 */
//*********************************************************************
static void update_alarm_status_kvs(priority_name priority)
{
    switch (priority)
    {
    case prior_eol_fatal:
        break;
    case prior_fault_photo:
        break;
    case prior_fault_co:
        break;
    case prior_fault_fatal:
        break;
    default:
        // Any non-fault priority will clear alarm status
        schedule_message(0, task_key_value_store, msg_set_value, key_fault_flags);
        break;
    }
}

//*********************************************************************
/**
 * \brief Change the current expression to the highest priority expression being expressed
 */
//*********************************************************************
static void update_expression(void)
{
    priority_name const highest_priority = get_highest_current_priority();
    serial_print_priority(highest_priority, DesiredPriority);
    update_alarm_status_kvs(highest_priority);

    if (current_priority != highest_priority)
    {
        if (current_priority == prior_smoke)
        {
            schedule_message(0, task_interconnect, msg_end_drive_smoke, task_state);
        }
        else if (current_priority == prior_smoke)
        {
            schedule_message(0, task_interconnect, msg_end_drive_co, task_state);
        }

        if (highest_priority == prior_smoke)
        {
            schedule_message(0, task_interconnect, msg_begin_drive_smoke, 0);
        }
        else if (highest_priority == prior_co)
        {
            schedule_message(0, task_interconnect, msg_begin_drive_co, 0);
        }

        if (highest_priority == prior_name_count)
        {
            // Stop the current expression
            schedule_message(0, task_expression, msg_end_express, 0);
        }
        else
        {
            // Start expression or change the current expression
            schedule_message(0, task_expression, msg_begin_express, priorities[highest_priority]);
        }
        current_priority = highest_priority;
    }
}

//*********************************************************************
/**
 * \brief schedule event for button voice to activate
 * \param message THe chosen message to be sent and voiced
 */
//*********************************************************************
void schedule_button_voice(voice_message message)
{
    // do not schedule multiple voices on button presses.
    if (!button_voice)
    {
        schedule_message(DEFAULT_VOICE_DELAY, task_voice_output, msg_voice, message);
        button_voice = 1;
        schedule_message(2500, task_state, msg_voice, 0);
    }
}

//*********************************************************************
/**
 * \brief Schedule a soft reset event
 * \param reset_time The time value for when the soft reset will occur
 */
//*********************************************************************
static void schedule_soft_reset(uint16_t reset_time)
{
    disable_external_interrupts(); // button, interconnect, wireless, tamper, reed switch
    schedule_message(reset_time, task_state, msg_continue, SOFT_RESET);
}

//*********************************************************************
/**
 * \brief initiate_push_to_test()
 * this function starts push to test
 */
//*********************************************************************
void initiate_push_to_test(void)
{
    // Wait some time to allow for button press processes to end
    //  then begin the push to test.
    // This clears the message_heap!
    schedule_message(200, task_ptt, msg_ptt, ptt_photo_init);
    set_priority(prior_ptt);
}

//*********************************************************************
/**
 * \brief Handler function for the state change of the button
 * \param priority Current priority for a state change
 */
//*********************************************************************
static void handle_button_state_change(priority_name const priority)
{
    switch (priority)
    {
    case prior_smoke:
        // master smoke -> hush
        if (smoke_hushable)
        {
            serial_send_string_debug("\rSmoke Hush\r", serial_enable_debug);
            set_priority(prior_smoke_hush);
            clear_priority(prior_smoke);
        }
        else
        {
            serial_send_string_debug("\rToo much smoke\r", serial_enable_debug);
            schedule_button_voice(voice_too_much_smk);
        }
        break;
    case prior_remote_smoke:
        break;
    case prior_co:
    case prior_co_conserve:
        // Delete the heap and clear priorities to prevent other priorities from setting before the soft reset occurs
        obliterate();
        // Force a soft reset when the button is pressed during CO alarm
        // After soft reset, CO task will lower the CO accumulator and re-enter CO alarm after a short period based on
        // accumulation
        schedule_soft_reset(2000);
        break;
    case prior_co_remote:
        break;
    case prior_ptt_init:
        break;
    case prior_ptt:
        break;
    case prior_smoke_hush:
        clear_priority(prior_smoke_hush);
        set_priority(prior_smoke);
        break;
    case prior_eol_fatal:
        // Restarting the expression will output the proper blinks and chirps,
        // no need to do anything here
        break;
    case prior_fault_photo:
        if (db_get_value(db_ptt_fault_history))
        {
            initiate_push_to_test();
        }
        else
        {
            // Delete the heap and clear priorities to prevent other priorities from setting before the soft reset
            // occurs
            obliterate();
            serial_send_string_debug("photo fault cleared\r", serial_enable_debug);
            // Record the fault clear in event history
            schedule_message(0, task_events, msg_record_history, event_fault_photo_clr);
            // Soft reset
            schedule_soft_reset(2000);
        }
        break;
    case prior_fault_co:
        // Delete the heap and clear priorities to prevent other priorities from setting before the soft reset occurs
        obliterate();
        serial_send_string_debug("CO fault cleared\r", serial_enable_debug);
        // Reset the no-init fault variable
        fault_co = co_fault_none;
        // Clear the fault variables in the CO task
        schedule_message(0, task_co, msg_reset_flags, 0);
        // Record the fault clear in event history
        schedule_message(0, task_events, msg_record_history, event_fault_co_cleared);
        // Soft reset
        schedule_soft_reset(2000);
        break;
    case prior_fault_fatal:
        schedule_button_voice(voice_replace_alarm);
        break;
    case prior_button_stuck:
        // do nothing - technically impossible
        break;
    case prior_eol:
        clear_priority(prior_eol);
        set_priority(prior_eol_hush);
        break;
    case prior_battery_none:
        // do nothing.  activate battery
        /// @todo low battery led blink?
        schedule_button_voice(voice_activate_battery);
        break;
    case prior_battery_low_no_hush:
        // do nothing. battery low
        schedule_button_voice(voice_low_battery);
        break;
    case prior_battery_low:
        // low battery -> hush
        set_priority(prior_battery_low_hush);
        clear_priority(prior_battery_low);
        schedule_button_voice(voice_hush_active);
        break;
    case prior_alarm_memory_smoke:
    case prior_alarm_memory_co:
    case prior_alarm_co_peak:
        // SPECIAL CASE - clear All Alarm Memorys
        clear_priority(prior_alarm_memory_smoke);
        break;
    case prior_eol_hush:
        // eol -> hush
        set_priority(prior_eol);
        clear_priority(prior_eol_hush);
        break;
    case prior_cal_pass:
    case prior_cal_pass_co_circuit:
    case prior_cal_pass_400_ppm:
    case prior_cal_fail:
    case prior_cal_co_circuit_no_button:
    case prior_cal_co_circuit:
    case prior_cal_battery_no_button:
    case prior_cal_battery:
    case prior_cal_lic:
    case prior_cal_2_point:
    case prior_cal_0_ppm:
    case prior_cal_150_ppm:
    case prior_cal_400_ppm:
    case prior_cal_inline_wait:
    case prior_cal_inline_stabilize:
    case prior_cal_inline_average:
    case prior_cal_inline_verify:
        // Do Nothing
        break;
    case prior_name_count:       // Nothing in priority queue
    case prior_battery_low_hush: /// @todo should we allow you to do a PTT on low battery?
    default:
        initiate_push_to_test();
        break;
    }
}

//*********************************************************************
/**
 * \brief Output button ignored statement over serial
 * \param priority The listed priority being expressed
 */
//*********************************************************************
void print_button_ignored(priority_name priority)
{
    serial_send_string_debug("button ignored - priority = ", serial_enable_debug);
    serial_send_msg_priority_debug(priority, serial_enable_debug);
    serial_send_string_debug("\r", serial_enable_debug);
}

void state_task(message_id const message, uint16_t const data)
{
    if (!final_test_complete) // do nothing in state if final test is not complete
    {
        return;
    }

    switch (message)
    {
    case msg_init:
    {
        if (!data)
        {
            // A hard reset inits the no-init variables.
            fault_co    = co_fault_none;
        }

        // PTT fault needs to persist through all resets
        // only cleared through a sucsessfull PTT
        if (db_get_value(db_ptt_fault_history))
        {
            fault_photo |= photo_fault_ptt_failure;
        }

        uint8_t reset_event = get_reset_event();
        schedule_message(0, task_events, msg_record_history, reset_event);
        current_priority = prior_name_count;
        memset(current, 0, sizeof(current));
        set_priority(prior_dc_blink);
        ac_present = 0;

        // subscribe to necessary callbacks
        schedule_message(0, task_key_value_store, msg_add_callback, (uint16_t)task_state << 8 | key_ir_photo_faults);
        schedule_message(0, task_key_value_store, msg_add_callback, (uint16_t)task_state << 8 | key_blue_led_fault);
        schedule_message(0, task_key_value_store, msg_add_callback, (uint16_t)task_state << 8 | key_clean_air_fault);
        schedule_message(0, task_key_value_store, msg_add_callback, (uint16_t)task_state << 8 | key_AFE_fault);

        // power on fade or chirp
        if (event_reset_wdtrf != reset_event || BEEP_ON_RESET)
        {
            set_priority(prior_power_on);
        }
        schedule_message(10, task_state, msg_continue, data);
        break;
    }
    break;
    case msg_continue:
        switch (data)
        {
        case SOFT_RESET:
            if (flash_in_progress)
            {
                // reschedule soft reset until the db updates have completed
                schedule_message(100, task_state, msg_continue, SOFT_RESET);
            }
            else
            {
                force_soft_reset();
            }
            break;
        default:
        {
            uint16_t const calibration = db_get_value16(db_calibration_msb, db_calibration_lsb);

            if (calibration != 0xffff) // If any calibration has not been done
            {
                if (flash_in_progress)
                {
                    schedule_message(100, task_state, msg_continue, data);
                    return;
                }
                else
                {
                    // don't express power on expression if cal is needed
                    clear_priority(prior_power_on);
                    obliterate();
                }
            }

            if (!(calibration & cal_co_circuit))
            {
                if (button_is_down())
                {
                    set_priority(prior_cal_co_circuit);
                }
                else
                {
                    set_priority(prior_cal_co_circuit_no_button);
                }
            }
            else if (!(calibration & cal_battery))
            {
                if (button_is_down())
                {
                    set_priority(prior_cal_battery);
                }
                else
                {
                    set_priority(prior_cal_battery_no_button);
                }
            }
            else if (!(calibration & cal_lic))
            {
                set_priority(prior_cal_lic);
            }
            else if (!(calibration & cal_2_point))
            {
                set_priority(prior_cal_2_point);
            }
            else if (!(calibration & cal_0_ppm))
            {
                set_priority(prior_cal_0_ppm);
            }
            else if (!(calibration & cal_150_ppm))
            {
                set_priority(prior_cal_150_ppm);
            }
            else if (!(calibration & cal_400_ppm))
            {
                set_priority(prior_cal_400_ppm);
            }
            else if (!(calibration & cal_inline_smoke))
            {
                set_priority(prior_cal_inline_wait);
            }
            else if (!data)
            {
                schedule_message(1000, task_voice_output, msg_voice, voice_push_test_btn);
                // no soft reset or failed ptt

                // Send message to DOE task
                schedule_message(0, task_cal_doe, msg_cal_begin, 0);
            }

            // Previous CO fault before reset
            if (data && fault_co)
            {
                set_priority(prior_fault_co);
            }
            else
            {
                fault_co = co_fault_none;
            }

            // enable update_expression()
            boot_up_complete = true;
        }
        break;
        }
        break;
    case msg_cal_fail:
        set_priority(prior_cal_fail);
        break;
    case msg_cal_pass:
        if (data == task_cal_2_point)
        {
            clear_priority(prior_cal_2_point);
            schedule_soft_reset(100);
        }
        else if (data == task_cal_inline_smoke)
        {
            schedule_soft_reset(100);
        }
        else if (data == task_cal_circuit)
        {
            set_priority(prior_cal_pass_co_circuit);
        }
        else if (data == task_cal_400_ppm)
        {
            set_priority(prior_cal_pass_400_ppm);
        }
        else
        {
            set_priority(prior_cal_pass);
        }
        break;
    case msg_enter_smoke_alarm:
    {
        set_key_value_store_flag(key_avoidance_flags, kvs_avoid_smoke_alarm, true);
        smoke_hushable          = data & 0x01;
        bool const remote_smoke = data & INT_REMOTE;

        if (remote_smoke)
        {
            if (!get_priority(prior_remote_smoke) && !locate_active)
            {
                // record smoke alarm event history
                schedule_message(0, task_events, msg_record_history, event_smoke_alarm_intcon_on);
                set_priority(prior_remote_smoke);
            }
        }
        else
        {
            // Only enter master smoke if remote smoke is not set
            if (!get_priority(prior_smoke))
            {
                // record smoke alarm event history
                schedule_message(0, task_events, msg_record_history, event_smoke_alarm_on);

                set_priority(prior_smoke);
            }
        }
    }
    break;
    case msg_exit_smoke_alarm:
    {
        set_key_value_store_flag(key_avoidance_flags, kvs_avoid_smoke_alarm, false);
        bool const remote_smoke = data & INT_REMOTE;

        if (remote_smoke)
        {
            // record smoke alarm event history
            schedule_message(0, task_events, msg_record_history, event_smoke_alarm_intcon_off);
            clear_priority(prior_remote_smoke);
        }
        else
        {
            clear_priority(prior_smoke);
            clear_priority(prior_smoke_hush);
            // record smoke alarm event history
            schedule_message(0, task_events, msg_record_history, event_smoke_alarm_off);
            set_priority(prior_alarm_memory_smoke);
        }
        break;
    }
    case msg_enter_co_alarm:
    {
        bool const remote_co = data & INT_REMOTE;

        // Wired Interconnect remote CO
        if (remote_co)
        {
            if (!get_priority(prior_co_remote) && !locate_active)
            {
                // record co alarm event history
                schedule_message(0, task_events, msg_record_history, event_co_alarm_intcon_on);
                set_priority(prior_co_remote);
            }
        }
        else
        {
            if (!get_priority(prior_co) && !get_priority(prior_co_conserve))
            {
                // record co alarm event history
                schedule_message(0, task_events, msg_record_history, event_co_alarm_on);
                set_priority(prior_co);
            }
        }
    }
    break;
    case msg_enter_co_alarm_conserve:
        conserve_time_passed = true;
        if (ac_present == false)
        {
            if (get_priority(prior_co))
            {
                clear_priority(prior_co);
                set_priority(prior_co_conserve);
            }
        }
        break;
    case msg_exit_co_alarm:
    {
        bool const remote_co = data & INT_REMOTE;
        conserve_time_passed = false;

        if (remote_co)
        {
            clear_priority(prior_co_remote);
        }
        else
        {
            clear_priority(prior_co);
            clear_priority(prior_co_conserve);
            set_priority(prior_alarm_memory_co);
        }
        break;
    }
    case msg_co_high:
        set_priority(prior_alarm_co_peak);
        break;
    case msg_fault_co_sensor_short:
        // CO sensor short fault detected
        fault_co |= co_fault_sensor_short;
        set_priority(prior_fault_co);
        break;

    case msg_fault_co_sensor_open:
        fault_co |= co_fault_sensor_open;
        set_priority(prior_fault_co);
        break;

    case msg_fault_co_clear:
        fault_co = co_fault_none;
        clear_priority(prior_fault_co);
        break;

    case msg_fault_fatal:
        set_priority(prior_fault_fatal);
        schedule_message(0, task_events, msg_record_history, data);
        break;

    case msg_expression_timeout:
        // only clear priority if priority still exists
        if (get_priority((priority_name)data))
        {
            clear_priority((priority_name)data);
        }
        break;

    case msg_smoke_hush_timeout:
        if (get_priority(prior_smoke_hush))
        {
            clear_priority(prior_smoke_hush);
            set_priority(prior_smoke);
        }
        break;
    case msg_battery_hush_timeout:
        if (get_priority(prior_battery_low_hush))
        {
            clear_priority(prior_battery_low_hush);
            set_priority(prior_battery_low);
        }
        break;
    case msg_eol_hush_timeout:
        if (get_priority(prior_eol_hush))
        {
            clear_priority(prior_eol_hush);
            set_priority(prior_eol);
        }
        break;

    case msg_button_down:
    {
        priority_name const highest_priority = get_highest_current_priority();

        if (data)
        {
            // Chirp to acknowledge the button press
            schedule_message(0, task_expression, msg_button_chirp, 1);
        }
        else
        {
            // Delay msg_button_complete so that any new expression is set as the next expression
            schedule_message(10, task_expression, msg_button_complete, 0);
            if (highest_priority == current_priority)
            {
                // Button released
                handle_button_state_change(highest_priority);
            }
        }
        break;
    }
    case msg_button_stuck:
        if (!get_priority(prior_button_stuck))
        {
            // Delay msg_button_complete so that any new expression is set as the next expression
            schedule_message(10, task_expression, msg_button_complete, 0);
            schedule_message(0, task_events, msg_record_history, event_fault_button_stuck);
        }
        if (data)
        {
            set_priority(prior_button_stuck);
        }
        else
        {
            clear_priority(prior_button_stuck);
        }
        break;
    case msg_enter_eol:
        if (!get_priority(prior_eol_hush))
        {
            set_priority(prior_eol);
        }
        break;
    case msg_enter_eol_fatal:
        if (!get_priority(prior_eol_fatal))
        {
            schedule_message(0, task_events, msg_record_history, event_life_expire);
        }
        set_priority(prior_eol_fatal);
        clear_priority(prior_eol_hush);
        clear_priority(prior_eol);
        break;
    case msg_exit_eol:
        clear_priority(prior_eol);
        clear_priority(prior_eol_hush);
        break;
    case msg_exit_eol_fatal:
        clear_priority(prior_eol_fatal);
        break;
    case msg_voice:
        button_voice = 0;
        break;

    case msg_end_express:
        switch ((expression_id)data)
        {
        case exp_power_on:
            // Priority power on only used for initial chirp
            clear_priority(prior_power_on);
            break;
        case exp_master_smoke:
            if (get_priority(prior_ptt))
            {
                schedule_message(0, task_ptt, msg_end_express, exp_master_smoke);
            }
            break;
        case exp_master_co:
            // if (ptt_active)
            // {
            //     uint16_t const day_count = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);

            //     // day_count = db_get_value16();
            //     // check if the life counter is at 0, indicating the first factory ptt
            //     if (day_count == 0)
            //     {
            //         db_set_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb, day_count + 1);
            //         schedule_message(0, task_database, msg_update_primary_db, 0);
            //         schedule_message(1000, task_state, msg_continue, SOFT_RESET);
            //     }
            //     else
            //     {
            //         schedule_message(0, task_voice_output, msg_voice, voice_test_complete);
            //         schedule_message(1000, task_state, msg_continue, SOFT_RESET);
            //     }
            // }
            break;
        case exp_ptt_initialize:
            // clear_priority(prior_ptt_init);
            // if (!ptt_holdoff_after_cancel)
            // {
            //     schedule_message(0, task_state, msg_start_push_to_test, 0);
            // }
            break;
        default:
            break;
        }
        break;
    case msg_ptt:
        switch ((ptt_message)data)
        {
        case ptt_complete:
            schedule_soft_reset(2000);
        default:
            break;
        }
    case msg_enter_low_battery:
        if (!get_priority(prior_battery_low_hush))
        {
            set_priority(prior_battery_low);
            clear_priority(prior_battery_none);
        }
        break;
    case msg_exit_low_battery:
        // this will exit low and hush
        clear_priority(prior_battery_low);
        clear_priority(prior_battery_low_hush);
        break;
    case msg_enter_fatal_battery:
        set_priority(prior_battery_low_no_hush);
        break;
    case msg_exit_fatal_battery:
        clear_priority(prior_battery_low_no_hush);
        break;
    case msg_enter_no_battery:
        set_priority(prior_battery_none);
        break;
    case msg_exit_no_battery:
        clear_priority(prior_battery_none);
        break;
    case msg_ac_on:
        /// @todo implement AC on expression
        ac_present = 1;

        if (get_priority(prior_co_conserve))
        {
            set_priority(prior_co);
        }
        break;
    case msg_ac_off:
        /// @todo implement AC off expression  (this should be handled by standby)
        ac_present = 0;

        if (get_priority(prior_co) && conserve_time_passed)
        {
            clear_priority(prior_co);
            set_priority(prior_co_conserve);
        }
        break;
    case msg_locate_timeout:
        if (locate_active)
        {
            if (data)
            {
                schedule_message(ONE_MINUTE, task_state, msg_locate_timeout, data - 1);
            }
            else
            {
                locate_active = false;
            }
        }
        break;
    case msg_inline_smoke_cal:
        set_priority((priority_name)data);
        break;
    case msg_value:
    {
        // sort value and task_id from key_value_store subscriptions
        uint8_t const value = (uint8_t)(data >> 8);
        key_name const key  = (key_name)(uint8_t)(data);

        switch (key)
        {
        case key_ir_photo_faults:
            // Check for Forward ambient fault   - set in validate_dark_f() - normal mode, analysis mode
            if (value & ((uint8_t)1 << kvs_fwd_ambient_fault))
            {
                fault_photo |= photo_fault_forward_ambient;
                serial_send_string_debug("photo_fault_forward_ambient - ", serial_enable_debug);
            }
            else
            {
                fault_photo &= ~(photo_fault_forward_ambient);
            }

            // Check for Backward ambient fault     - set in validate_dark_b()  - analysis mode, run_clean_air_mode()
            // every 2 hours
            if (value & ((uint8_t)1 << kvs_bck_ambient_fault))
            {
                fault_photo |= photo_fault_back_ambient;
                serial_send_string_debug("photo_fault_back_ambient - ", serial_enable_debug);
            }
            else
            {
                fault_photo &= ~(photo_fault_back_ambient);
            }

            // Check for Forward IR LED fault       - set in validate_light()
            if (value & ((uint8_t)1 << kvs_fwd_ir_led_fault))
            {
                fault_photo |= photo_fault_forward_ir_led;
                serial_send_string_debug("photo_fault_forward_ir_led - ", serial_enable_debug);
            }
            else
            {
                fault_photo &= ~(photo_fault_forward_ir_led);
            }

            // Check for Backward IR LED fault      - set in validate_light()
            if (value & ((uint8_t)1 << kvs_bck_ir_led_fault))
            {
                fault_photo |= photo_fault_back_ir_led;
                serial_send_string_debug("photo_fault_back_ir_led - ", serial_enable_debug);
            }
            else
            {
                fault_photo &= ~(photo_fault_back_ir_led);
            }
            update_photo_fault_priority();
            break;

        case key_blue_led_fault:
            // Check for Back blue fault (blue LED supervision)
            if (value)
            {
                fault_photo |= photo_fault_blue_led_supervision;
                serial_send_string_debug("photo_fault_blue_led_supervision\r", serial_enable_debug);
            }
            else
            {
                fault_photo &= ~(photo_fault_blue_led_supervision);
            }
            update_photo_fault_priority();
            break;

        case key_clean_air_fault:
            // Check for Clean air fault  - set in update_clean_air_count() - done on boot and every 2 hours
            if (value)
            {
                fault_photo |= photo_fault_clean_air_update;
                serial_send_string_debug("photo_fault_clean_air_update\r", serial_enable_debug);
            }
            else
            {
                fault_photo &= ~(photo_fault_clean_air_update);
            }
            update_photo_fault_priority();
            break;

        case key_AFE_fault:
            // Check for AFE fault
            if (value)
            {
                fault_photo |= photo_fault_afe;
                serial_send_string_debug("AFE Fault\r", serial_enable_debug);
            }
            else
            {
                fault_photo &= ~(photo_fault_afe);
            }
            update_photo_fault_priority();
            break;
        default:
            break;
        }
    }
    break;
    default:
        break;
    }
    if (boot_up_complete)
    {
        update_expression();
    }
}
