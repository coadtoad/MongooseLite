//******************************************************************
/*! \file Clean_air_value_single.c
 *
 * \brief Kidde: Mongoose-fw - Define the clean air value functions
 *
 * \author Justin Henry
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_events.h"
#include "app_serial.h"
#include "clean_air_value.h"
#include "database.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "drv_vboost.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_single.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief max CAV changes that is allowed before causing fault
 */
//*********************************************************************
#define MAX_CLEAN_AIR_CHANGE       30

//*********************************************************************
/**
 * \brief time (in milliseconds) to retry CAV reading if CAV update fails or was delayed
 */
//*********************************************************************
#define CAV_RETRY_TIME             100

//*********************************************************************
/**
 * \brief time (in milliseconds) of time with nothing else happening to do cav update
 */
//*********************************************************************
#define CAV_CLEAR_TIME             500

//*********************************************************************
/**
 * \brief time (in minutes) between CAV readings
 */
//*********************************************************************
#define CAV_UPDATE_INTERVAL        10

//*********************************************************************
/**
 * \brief number of CAV readings per hour
 */
//*********************************************************************
#define CAV_PER_HOUR               (60 / CAV_UPDATE_INTERVAL)

//*********************************************************************
/**
 * \brief size of buffer required to hold 1 day of cav readings
 */
//*********************************************************************
#define CAV_BUFFER_SIZE            (24 * CAV_PER_HOUR)

//*********************************************************************
/**
 * \brief Difference in current CAV reading and current CAV value
 *          used to determine if large update check should be done
 */
//*********************************************************************
#define CAV_DIFF_THRESHOLD         4

//*********************************************************************
/**
 * \brief maximum difference allowed beween CAV readings before large CAV update can be made.
 */
//*********************************************************************
#define CAV_MIN_MAX_DIFF_THRESHOLD 4

//*********************************************************************
/**
 * \brief Buffer to hold CAVs taken to make large changes.
 *      Defined as 2 hours worth of CAV readings.
 */
//*********************************************************************
#define CAV_DIFF_BUFFER_SIZE       (2 * CAV_PER_HOUR)

//*********************************************************************
/**
 * \brief ///< Mask for msg_continue to indicate that the VBST is enabled for photo
 */
//*********************************************************************
#define VBOOST_ENABLE_MASK         0x2000

//*********************************************************************
/**
 * \brief Clean air value override boolean
 */
//*********************************************************************
static bool accelerated_cav = false;

//*********************************************************************
/**
 * \brief buffer that contains CAV for the last 24 horus
 */
//*********************************************************************
static uint8_t cav_24_buffer[number_of_light_signals][CAV_BUFFER_SIZE];

//*********************************************************************
/**
 * \brief keep record of last X time CAV values
 */
//*********************************************************************
static uint8_t cav_large_change_buffer[number_of_light_signals][CAV_DIFF_BUFFER_SIZE];

//*********************************************************************
/**
 * \brief list of different cav fault types
 */
//*********************************************************************
typedef enum
{
    cav_fault_none,
    cav_fault_low,
    cav_fault_high,
} cav_fault_type;

void reset_cav_buffers(void)
{
    uint8_t const ir_f_cav_db = db_get_value(db_ir_f_clean_air);
    uint8_t const ir_b_cav_db = db_get_value(db_ir_b_clean_air);
    for (uint8_t i = 0; i < CAV_BUFFER_SIZE; ++i)
    {
        cav_24_buffer[IR_F][i] = ir_f_cav_db;
        cav_24_buffer[IR_B][i] = ir_b_cav_db;
    }

    for (uint8_t i = 0; i < CAV_DIFF_BUFFER_SIZE; ++i)
    {
        cav_large_change_buffer[IR_F][i] = ir_f_cav_db;
        cav_large_change_buffer[IR_B][i] = ir_b_cav_db;
    }
}

uint8_t increase_rolling_buffer(uint8_t buffer_value, uint8_t buffer_size)
{
    buffer_value++;
    // rollover
    if (buffer_value >= buffer_size)
    {
        buffer_value = 0;
    }

    return buffer_value;
}

//*********************************************************************
/**
 * \brief updates the database clean air values
 * \note Function compares the value to database value and updates database value by 1 if allowed
 *
 * returns fault type if a fualt is detected
 */
//*********************************************************************
cav_fault_type update_clean_air_signal_count(cav_reason_t const cav_reset, light_signal const signal, uint8_t cav_value)
{
    static uint8_t cav_24_buffer_index[number_of_light_signals] = {0, 0};
    static uint8_t cav_buffer_index[number_of_light_signals]    = {0, 0};

    cav_fault_type cav_fault                                    = cav_fault_none;

    database_index cav_signal_db                                = 0;
    database_index cav_signal_factory_db                        = 0;

    bool cav_changed                                            = false;

    switch (signal)
    {
    case IR_F:
        cav_signal_db         = db_ir_f_clean_air;
        cav_signal_factory_db = db_ir_f_factory_clean_air;
        serial_send_string_debug("CAV UPDATE IR_F\r", serial_enable_calibration);
        break;
    case IR_B:
        cav_signal_db         = db_ir_b_clean_air;
        cav_signal_factory_db = db_ir_b_factory_clean_air;
        serial_send_string_debug("CAV UPDATE IR_B\r", serial_enable_calibration);
        break;
    default:
        // ERROR
        break;
    }
    uint8_t const signal_cav_db_value         = db_get_value(cav_signal_db);
    uint8_t const signal_cav_db_value_factory = db_get_value(cav_signal_factory_db);

    if (!cav_reset)
    {
        // do not update bfufers on manual CAV update
        cav_large_change_buffer[signal][cav_buffer_index[signal]] = cav_value;
        cav_24_buffer[signal][cav_24_buffer_index[signal]]        = signal_cav_db_value;

        // update CAV if value is constant for CAV_DIFF_BUFFER_SIZE
        uint8_t const cav_diff =
            (cav_value >= signal_cav_db_value) ? cav_value - signal_cav_db_value : signal_cav_db_value - cav_value;

        if ((cav_24_buffer_index[signal] % (CAV_PER_HOUR * 2) == 0) // only do every 2 hours
            || accelerated_cav)
        {
            serial_send_string_debug("CAV - normal 2 hour update\r", serial_enable_calibration);
            // NORMAL CAV UPDATE + or - 1
            // 1. Examine ir_b for CAV Changes and adjust compensated CAV value
            if (cav_value > signal_cav_db_value)
            {
                cav_value = signal_cav_db_value + 1;
            }
            else if (cav_value < signal_cav_db_value)
            {
                cav_value = signal_cav_db_value - 1;
            }

            cav_changed = true;
        }

        // if cav can change larger, overwrite 24 hour cav
        if (cav_diff > CAV_DIFF_THRESHOLD)
        {
            serial_send_string_debug("CAV - Large Diff\r", serial_enable_calibration);
            uint8_t min = 0xFF;
            uint8_t max = 0x00;

            for (uint8_t i = 0; i < CAV_DIFF_BUFFER_SIZE; ++i)
            {
                serial_send_int_ex_debug(NULL, cav_large_change_buffer[signal][i], 16, " - ",
                                         serial_enable_calibration);

                if (cav_large_change_buffer[signal][i] < min)
                {
                    min = cav_large_change_buffer[signal][i];
                }

                if (cav_large_change_buffer[signal][i] > max)
                {
                    max = cav_large_change_buffer[signal][i];
                }
            }

            uint8_t const max_min = max - min;
            serial_send_int_ex_debug("min - ", min, 16, " - ", serial_enable_calibration);
            serial_send_int_ex_debug("max - ", max, 16, " - ", serial_enable_calibration);
            serial_send_int_ex_debug("diff - ", max_min, 16, "\r", serial_enable_calibration);
            // if value is consistant for CAV_DIFF_BUFFER_SIZE in time, set CAV to average between min / max
            if (max_min <= CAV_MIN_MAX_DIFF_THRESHOLD)
            {
                cav_changed = true;
                cav_value   = (min + max) / 2;
                serial_send_int_ex_debug("CAV - Large Diff, new cav value - ", cav_value, 16, "\r",
                                         serial_enable_calibration);
            }
        }

        // increase index's to next rolling index (will overwrite next run, but now points to last element in array)
        cav_24_buffer_index[signal] = increase_rolling_buffer(cav_24_buffer_index[signal], CAV_BUFFER_SIZE);
        cav_buffer_index[signal]    = increase_rolling_buffer(cav_buffer_index[signal], CAV_DIFF_BUFFER_SIZE);

        uint8_t oldest_cav_value    = cav_24_buffer[signal][cav_24_buffer_index[signal]];

        // abs of difference of cav from 24 hours ago and latest set CAV.
        uint8_t const cav_time_diff =
            (cav_value >= oldest_cav_value) ? cav_value - oldest_cav_value : oldest_cav_value - cav_value;

        // limit CAV change to 50% of total sensitivity of unit
        uint8_t const max_cav_chagne = db_get_value(db_fail_safe_high_threshold) / 2;
        if (cav_time_diff > max_cav_chagne)
        {
            if (cav_value > oldest_cav_value)
            {
                cav_value = oldest_cav_value + max_cav_chagne;
            }
            else
            {
                cav_value = oldest_cav_value - max_cav_chagne;
            }
            serial_send_int_ex_debug("CAV - cav limited to 24 hour limit, cav_time_diff - ", cav_time_diff, 10, " - ",
                                     serial_enable_calibration);
            serial_send_int_ex_debug("oldest cav value - ", oldest_cav_value, 16, " - ", serial_enable_calibration);
            serial_send_int_ex_debug("new cav valuie -  ", cav_value, 16, "\r", serial_enable_calibration);
        }

        if (cav_value > signal_cav_db_value_factory)
        {
            if ((cav_value - signal_cav_db_value_factory) > MAX_CLEAN_AIR_CHANGE)
            {
                cav_value = signal_cav_db_value;
                cav_fault = cav_fault_high;
                serial_send_int_ex_debug("CAV - Cav fault high - new cav valuie -  ", cav_value, 16, "\r",
                                         serial_enable_calibration);
            }
        }
        else if (cav_value < signal_cav_db_value_factory)
        {
            if ((signal_cav_db_value_factory - cav_value) > MAX_CLEAN_AIR_CHANGE)
            {
                cav_value = signal_cav_db_value;
                cav_fault = cav_fault_low;
                serial_send_int_ex_debug("CAV - Cav fault low - new cav valuie -  ", cav_value, 16, "\r",
                                         serial_enable_calibration);
            }
        }
    }

    /// \todo fault manager?

    if (cav_reset == cav_reason_serial_reset || cav_reset == cav_reason_serial_reset_factory || cav_changed)
    {
        db_set_value(cav_signal_db, cav_value);
        serial_send_int_ex_debug("CAV - CAV Updated - ", cav_value, 16, "\r", serial_enable_calibration);
    }

    if (cav_reset == cav_reason_serial_reset_factory)
    {
        db_set_value(cav_signal_factory_db, cav_value);
        serial_send_int_ex_debug("CAV - CAV Update Factory - ", cav_value, 16, "\r", serial_enable_calibration);
    }

    return cav_fault;
}

void update_cav_faults(cav_fault_type ir_f_cav_fault, cav_fault_type ir_b_cav_fault)
{
    static bool photo_cav_fault_prev                = false;
    static cav_fault_type photo_cav_fault_ir_f_prev = cav_fault_none;
    static cav_fault_type photo_cav_fault_ir_b_prev = cav_fault_none;

    // Save Events for each CAV fault
    if (ir_b_cav_fault != photo_cav_fault_ir_b_prev)
    {
        photo_cav_fault_ir_b_prev = ir_b_cav_fault;
        if (ir_b_cav_fault == cav_fault_high)
        {
            //schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_b_hi);
            serial_send_string_debug("event_fault_photo_cav_ir_b_hi\r", serial_enable_calibration);
        }
        if (ir_b_cav_fault == cav_fault_low)
        {
            //schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_b_lo);
            serial_send_string_debug("event_fault_photo_cav_ir_b_lo\r", serial_enable_calibration);
        }
    }

    // Save Events for each CAV fault
    if (ir_f_cav_fault != photo_cav_fault_ir_f_prev)
    {
        photo_cav_fault_ir_f_prev = ir_f_cav_fault;
        if (ir_f_cav_fault == cav_fault_high)
        {
            //schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_f_hi);
            serial_send_string_debug("event_fault_photo_cav_ir_f_hi\r", serial_enable_calibration);
        }
        if (ir_f_cav_fault == cav_fault_low)
        {
            //schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_ir_f_lo);
            serial_send_string_debug("event_fault_photo_cav_ir_f_lo\r", serial_enable_calibration);
        }
    }

    // -------------------------------- total cav fault --------------------------------
    bool photo_cav_fault = (ir_b_cav_fault != cav_fault_none) || (ir_f_cav_fault != cav_fault_none);

    // Clear clean air fault master (if any other faults recorded)
    if (photo_cav_fault_prev != photo_cav_fault)
    {
        photo_cav_fault_prev = photo_cav_fault;

        // send state message to express CAV fault if any.
        //schedule_message(0, task_key_value_store, msg_set_value, (uint16_t)photo_cav_fault << 8 | key_clean_air_fault);

        if (!photo_cav_fault)
        {
            //schedule_message(0, task_events, msg_record_history, event_fault_photo_cav_clr);
        }
    }
}

void update_clean_air_count(cav_reason_t const cav_reset, uint8_t const ir_f_avg, uint8_t const ir_b_avg)
{
    cav_fault_type const ir_f_cav_fault = update_clean_air_signal_count(cav_reset, IR_F, ir_f_avg);

    cav_fault_type const ir_b_cav_fault = update_clean_air_signal_count(cav_reset, IR_B, ir_b_avg);

    if (cav_reset != cav_reason_normal)
    {
        reset_cav_buffers();
    }

    update_cav_faults(ir_f_cav_fault, ir_b_cav_fault);
}

//*********************************************************************
/**
 * \brief performs smoke measurements for clean air mode.
 * \note Function takes IR forward and back measurements and blue forward measurements.
 *          The filters and filter chain are then updated
 * \note PRE-CONDITION: The system is in normal mode and will call this function 5 times in 5 seconds
 * \note POST-CONDITION: The filters and filter chain are updated with the new measurements
 */
//*********************************************************************
static bool run_clean_air_mode(cav_reason_t cav_reset)
{
    uint8_t photo_reading_arr[num_photo_data];
    if (!read_photo(db_get_value(db_current_ir_f), db_get_value(db_current_ir_b), photo_reading_arr))
    {
        return false; // will cause to re-schedule
    }

    // DARK COMPENSATION
    uint8_t const light_ir_f = (photo_reading_arr[ir_f_reading] >= photo_reading_arr[dark_reading])
                                   ? photo_reading_arr[ir_f_reading] - photo_reading_arr[dark_reading]
                                   : 0;

    uint8_t const light_ir_b = (photo_reading_arr[ir_b_reading] >= photo_reading_arr[dark_reading])
                                   ? photo_reading_arr[ir_b_reading] - photo_reading_arr[dark_reading]
                                   : 0;

    // update cav values for averaging
    update_clean_air_count(cav_reset, light_ir_f, light_ir_b);

    update_ir_photo_fault_kvs();

    return true;
}

void clean_air_value_task(message_id const message, uint16_t const data)
{
    static cav_reason_t cav_reset = cav_reason_normal;
    switch (message)
    {
    case msg_init:
        // schedule a clean air update at power on, 1 min delay
        //schedule_message(ONE_MINUTE, task_clean_air_value, msg_ready, cav_reason_normal);

        // init bufer to previous CAV value before powerdown.
        reset_cav_buffers();

        /// \todo remove when calibration does this.
        if (db_get_value(db_ir_f_clean_air) == 0xFF)
        {
            //schedule_message(2000, task_clean_air_value, msg_ready, cav_reason_serial_reset_factory);
        }
        break;
    case msg_ready:
        if (get_key_value_store_flag(key_avoidance_flags, kvs_avoid_smoke_alarm) ||
            get_key_value_store_flag(key_avoidance_flags, kvs_avoid_sounder))
        {
            // Delay CAV update if smoke alarm is active or sounder is on
            //schedule_message(CAV_RETRY_TIME, task_clean_air_value, msg_ready, data);
        }
        else
        {
            // Schedule 20ms to avoid analysis mode photo samples
            schedule_timed_message(CAV_CLEAR_TIME, task_clean_air_value, msg_sample, data);
        }
        break;
    case msg_sample:
        if (get_key_value_store_flag(key_avoidance_flags, kvs_avoid_smoke_alarm) ||
            get_key_value_store_flag(key_avoidance_flags, kvs_avoid_sounder))
        {
            // Delay CAV update if sounder or alarm happened while waiting for timed message.
            //schedule_message(CAV_RETRY_TIME, task_clean_air_value, msg_ready, data);
        }
        else
        {
            // schedule sample 1/2 through clear sample time
            //schedule_message(CAV_CLEAR_TIME / 2, task_clean_air_value, msg_continue, data);
        }
        break;
    case msg_continue:
    {
        bool const vboost_on = data & VBOOST_ENABLE_MASK;

        if (!vboost_on)
        {
            // Enable vboost
            vboost_photo_on();
            // Run clean air after vboost has charged for some time
            ///@todo Minimum of 2ms of VBST on time is needed before disabling
            //schedule_message(3, task_clean_air_value, msg_continue, data | VBOOST_ENABLE_MASK);
        }
        else
        {
            cav_reset = data & 0xFF;
            ///@todo: This can only fail if the AFE doesn't wakeup during begin_photo_read(), which is a bigger
            /// problem.
            vboost_photo_off();
            if (!run_clean_air_mode(cav_reset))
            {
                // try again
                //schedule_message(CAV_RETRY_TIME, task_clean_air_value, msg_ready, data & 0xFF);
            }
            else if ((data & 0xFF) == cav_reason_normal)
            {
                // Schedule the next CAV update
                //schedule_message(CAV_UPDATE_INTERVAL * ONE_MINUTE, task_clean_air_value, msg_ready, cav_reason_normal);
            }
        }
    }
    break;

    case msg_set_flag: // accelerated time serial command
        if ((uint8_t)(data >> 8) == 0x08)
        {
            accelerated_cav = true;
        }
        else if ((uint8_t)(data >> 8) == 0x09)
        {
            accelerated_cav = false;
        }
        break;
    default:
        break;
    }
}
