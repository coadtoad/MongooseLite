//******************************************************************
/*! \file in_photo_single_decision.c
 *
 * \brief Kidde: Mongoose-fw - Implement functions for single channel photo algorithm
 *
 * \author
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_photo_single_decision.h"
#include "app_database.h"
#include "app_serial.h"
#include "in_photo.h"
#include "photo_common_single.h"

//*********************************************************************
/**
 * \brief ///< Used as a counter threshold in the normal mode to reset smoldering counter
 */
//*********************************************************************
#define SMOLD_DECOUNT_THRESH 5

//*********************************************************************
/**
 * \def SAMPLE_BUF_SIZE
 * \brief Used to specify the size of the signal buffer used for acceleration criterion
 */
//*********************************************************************
#define SAMPLE_BUF_SIZE      90

//*********************************************************************
/**
 * \def ROR_DIFF_TIME
 * \brief Used as a time window for rate of rise criterion
 */
//*********************************************************************
#define ROR_DIFF_TIME        15

//*********************************************************************
/**
 * \def ACC_AVERAGE_COUNT
 * \brief Max number of counts to sum together to obtain an acceleration average
 */
//*********************************************************************
#define ACC_AVERAGE_COUNT    30

//*********************************************************************
/**
 * \brief sample_buf is an array that holds a quantity of the latest photo samples
 */
//*********************************************************************
static uint16_t sample_buf[SAMPLE_BUF_SIZE];

//*********************************************************************
/**
 * \brief averages_buf is an array that holds a quantity of the average of the latest photo
 *        samples from the sample buffer
 */
//*********************************************************************
static uint16_t averages_buf[SAMPLE_BUF_SIZE];

//*********************************************************************
/**
 * \brief rr_start is a flag to determine if enough samples have been
 *        taken to enable rate of rise calculation
 */
//*********************************************************************
static bool rr_start                   = false;

//*********************************************************************
/**
 * \brief acc_start is a flag to determine if enough samples have been
 *        taken to enable acceleration calculation
 */
//*********************************************************************
static bool acc_start                  = false;

//*********************************************************************
/**
 * \brief avg_start is a flag to determine if enough samples have been
 *        taken to enable averaging the maximum number of samples for
 *        acceleration calculation
 * \note The max number of samples is defined as ACC_AVERAGE_COUNT
 */
//*********************************************************************
static bool avg_start                  = false;

//*********************************************************************
/**
 * \brief Confidence check for acceleration for fast accelerating fire
 */
//*********************************************************************
static uint8_t conf_check_acceleration = 0;

//*********************************************************************
/**
 * \brief Confidence check for fail safe fire
 */
//*********************************************************************
static uint8_t conf_check_fail_Safe    = 0;

//*********************************************************************
/**
 * \brief Confidence check for rate of rise increment
 */
//*********************************************************************
static uint8_t conf_check_rate_of_rise = 0;

//*********************************************************************
/**
 * \brief Rate of rise to determine fast flaming fire
 */
//*********************************************************************
static uint16_t rate_of_rise           = 0;

//*********************************************************************
/**
 * \brief acceleration value to determine fast accelerating fire
 */
//*********************************************************************
static uint32_t simple_acc             = 0;

//*********************************************************************
/**
 * \brief Count value for smoldering fire compared to smoldering threshold
 */
//*********************************************************************
static uint16_t smold_count            = 0;

//*********************************************************************
/**
 * \brief index for CURRENT buffer location
 */
//*********************************************************************
static uint8_t buf_index               = 0xFF; // will reset to 0 on next increase

//*********************************************************************
/**
 * \brief This function returns true if the alarm is hushable, false otherwise
 * \note if the alarm equation value is below a threshold return true because the alarm is hushable
 *          otherwise return false
 *
 * PRE-CONDITION: the alarm equation value has been updated
 *
 * \see reset_program.asm
 */
//*********************************************************************
static bool is_hushable(uint8_t const delta)
{
    return delta <= db_get_value(db_photo_hush_threshold) &&
           // this should be higher than hush, but extra saftey measure
           delta <= db_get_value(db_fail_safe_high_threshold);
}

//*********************************************************************
/**
 * \brief Clear all confidence check counter to a 0 value
 */
//*********************************************************************
static void reset_confcheck_counters(void)
{
    conf_check_acceleration = 0;
    conf_check_fail_Safe    = 0;
    conf_check_rate_of_rise = 0;
}

//*********************************************************************
/**
 * \brief Find the index in an array based on the window
 *        This is used for rate of rise and acceleration criteria to find the previous signal value
 *
 * \param buffer_size The size of the buffer at creation
 * \param current_index the current index the rolling buffer is currently at
 * \param distance_back how far we want to look into the past of the buffer
 *
 * \returns The index at which the intended signal value is present
 */
//*********************************************************************
uint8_t find_previous_index(const uint8_t buffer_size, uint8_t current_index, uint8_t distance_back)
{
    if (current_index >= distance_back)
    {
        return current_index - distance_back;
    }
    else
    {
        distance_back = distance_back - current_index;

        current_index = buffer_size - distance_back;
    }

    return current_index;
}

//*********************************************************************
/**
 * \brief Check if the time is more than the time limit given by the fire mode criterion
 *
 * \param buff_index current buffer index for samples of te last SAMPLE_BUF_SIZE in time
 * \param acc_start global used to track if time running > SAMPLE_BUF_SIZE
 * \param rr_start global used to track if time running > ROR_DIFF_TIME
 *
 * \returns Return true if it goes beyon the time limit, false otherwise
 */
//*********************************************************************
uint8_t increase_buf_index(uint8_t buf_index_local)
{
    buf_index_local++;
    // Set buf index to zero before it reaches its size
    if (buf_index_local >= SAMPLE_BUF_SIZE)
    {
        buf_index_local = 0;
        if (!acc_start)
        {
            acc_start = true;
        }
    }

    if (!rr_start)
    {
        if (buf_index_local >= ROR_DIFF_TIME)
        {
            rr_start = true;
        }
    }

    if (!avg_start)
    {
        if (buf_index_local >= ACC_AVERAGE_COUNT)
        {
            avg_start = true;
        }
    }

    return buf_index_local;
}

//*********************************************************************
/**
 * \brief Average the latest samples from the sample buffer and place the result into the averages buffer.
 *        If the sample buffer has less than ACC_AVERAGE_COUNT samples, then average however many samples
 *        there are at the moment
 * \returns The average of the latest samples from the sample buffer
 * \note This function should be called everytime a sample is added to the sample buffer
 */
//*********************************************************************
uint16_t calculate_buffer_average(void)
{
    // Array index starts at 0. Increment by 1 to get number of samples
    uint8_t const samples_count = avg_start ? ACC_AVERAGE_COUNT : (buf_index + 1);
    uint32_t mean               = 0;
    for (uint8_t i = 1; i <= samples_count; i++)
    {
        uint8_t const location = find_previous_index(SAMPLE_BUF_SIZE, buf_index, i);
        mean += sample_buf[location];
    }
    return mean / samples_count;
}

//*********************************************************************
/**
 * \brief Calculates the square root of the given value to the greatest whole number
 * \param square The number to derive the square root from
 * \returns The square root of the given value
 */
//*********************************************************************
uint16_t floorSqrt(uint16_t const x)
{
    // narrow down sqaure root
    uint16_t max        = 0x100; // start outside the range of uint8_t so that high values return 255 instead of 254
    uint8_t min         = 0x00;

    uint8_t check_value = 0xFF >> 1;

    while (max - min > 1)
    {
        uint16_t const result = check_value * check_value;
        if (result > x)
        {
            max = check_value;
        }
        else
        {
            min = check_value;
        }
        check_value = (min + max) >> 1;
    }

    // find which square is closer.
    uint16_t const min_diff = x - min * min;
    uint16_t const max_diff = max * max - x;

    if (min_diff < max_diff)
    {
        return min;
    }
    else if (max > 0xff)
    {
        return 0xff;
    }
    else
    {
        return max;
    }
}

//*********************************************************************
/**
 * \brief determine_fire_modes_scp Determines if device is in alarm or not
 *        Also maintains the buffers used to determine if device is in fire.
 *
 * \param photo_mode device is in normal or analysis mode
 * \param photo_fault_flags device is in any photo fault
 * \param delta_val current delta value
 *
 * \returns fire_flags
 */
//*********************************************************************
uint8_t determine_fire_modes_scp(photo_mode_e const photo_mode,   // normal vs analysis
                                 uint8_t const photo_fault_flags, // photo faults
                                 uint8_t const delta_val)         // delta value
{
    static uint8_t smold_decount = 0;
    rate_of_rise                 = 0;
    simple_acc                   = 0;

    //------------------------------------------  Buffers ------------------------------------------
    static uint16_t prev_delta;
    uint16_t const delta_val16        = delta_val << 8;
    uint8_t const normal_mode_seconds = db_get_value(db_normal_mode_period);

    buf_index                         = increase_buf_index(buf_index);

    if (photo_mode == NORMAL_MODE)
    {

        // normal mode needs to input db_normal_mode_period of data into the buffer
        int16_t slope;
        bool positive_slope = false;
        if (delta_val16 >= prev_delta) // slope is positive
        {
            slope          = (delta_val16 - prev_delta) / normal_mode_seconds;
            positive_slope = true;
        }
        else // slope is negative
        {
            slope          = (prev_delta - delta_val16) / normal_mode_seconds;
            positive_slope = false;
        }

        for (uint8_t i = 1; i < normal_mode_seconds; i++)
        {
            if (positive_slope)
            {
                sample_buf[buf_index] = prev_delta + (slope * i);
            }
            else // negative slope
            {
                sample_buf[buf_index] = prev_delta - (slope * i);
            }

            // Average latest samples and place into averages buffer for acceleration
            averages_buf[buf_index] = calculate_buffer_average();

            // Increment the buffer index for the next sample
            buf_index               = increase_buf_index(buf_index);
        }

        sample_buf[buf_index] = delta_val16; // 9 estimated values, 1 current value
    }
    else // photo_mode == ANALYSIS_MODE // PTT_MODE
    {
        sample_buf[buf_index] = delta_val16;
    }

    // Average latest samples and place into averages buffer for acceleration
    averages_buf[buf_index] = calculate_buffer_average();

    // Save the new delta as the next "previous" delta
    prev_delta              = delta_val16;

    //------------------------------------------ END Buffers ------------------------------------------
    if ((photo_mode == NORMAL_MODE) || // dont do fire determination in normal mode, just update buffers
        photo_fault_flags)             // Single channel algorithm is called only if there is no photo faults.
    {
        // Start smold decounting if it was triggered from analysis mode
        if (++smold_decount > SMOLD_DECOUNT_THRESH) // SMOLD_DECOUNT_THRESH * db_normal_mode_period (seconds)
        {
            smold_count = 0;
        }

        reset_confcheck_counters(); // Confidence check values are reset to zero.
        return 0;                   // all fire flags false
    }
    //------------------------------------------  run algorithm ------------------------------------------
    //------------------------------------------ ANALYSIS MODE ------------------------------------------
    bool slow_smoldering_fire   = false;
    bool fast_flaming_fire      = false;
    bool fast_accelerating_fire = false;
    bool fail_safe_fire         = false;

    //----------------------------------- smoldering -----------------------------------
    smold_decount               = 0; // Reset smold_decount when in analysis mode

    // Slow smolding counter values increases
    smold_count++;
    if ((smold_count >= db_get_value16(db_smold_time_threshold_msb, db_smold_time_threshold_lsb)) &&
        (delta_val >= db_get_value(db_smold_count_threshold)))
    {
        slow_smoldering_fire = true;
    }

    //----------------------------------- rate of rise -----------------------------------
    // fast flaming fire condition is checked
    if (rr_start)
    {
        // rate of rise is slope over 15 seconds
        uint16_t current_delta = averages_buf[buf_index];
        uint16_t old_delta     = averages_buf[find_previous_index(SAMPLE_BUF_SIZE, buf_index, ROR_DIFF_TIME)];

        if (current_delta <= old_delta)
        {
            rate_of_rise = 0;
        }
        else
        {
            rate_of_rise = (current_delta - old_delta) / ROR_DIFF_TIME;
        }

        if (delta_val > db_get_value(db_ror_min_threshold))
        {
            if (rate_of_rise > db_get_value16(db_ror_threshold_msb, db_ror_threshold_lsb))
            {
                if (++conf_check_rate_of_rise >= db_get_value(db_ror_confidence_check))
                {
                    fast_flaming_fire = true;
                }
            }
            else
            {
                conf_check_rate_of_rise = 0;
            }
        }
        else
        {
            conf_check_rate_of_rise = 0;
        }
    }

    //----------------------------------- accelleration -----------------------------------
    if (acc_start)
    {
        // consider if slope is negative.  - currently checked below. if slope is negative
        uint8_t const middle_time  = SAMPLE_BUF_SIZE >> 1;
        uint16_t const early_point = averages_buf[find_previous_index(SAMPLE_BUF_SIZE, buf_index, SAMPLE_BUF_SIZE - 1)];
        uint16_t const middle_point  = averages_buf[find_previous_index(SAMPLE_BUF_SIZE, buf_index, middle_time)];
        uint16_t const current_point = averages_buf[buf_index];
        uint16_t const slope         = (int16_t)(current_point - early_point) / SAMPLE_BUF_SIZE;
        uint16_t const point_fit     = (int16_t)(slope * middle_time) + early_point;

        if (point_fit >= middle_point)
        {
            // Calculate the acceleration
            simple_acc = point_fit - middle_point;
            // Smooth the acceleration value a bit
            simple_acc = (simple_acc * 128) / floorSqrt(current_point);
        }
        // else simple_acc = 0

        if (delta_val > db_get_value(db_accel_min_threshold))
        {
            if (simple_acc > db_get_value16(db_accel_threshold_msb, db_accel_threshold_lsb) &&
                (current_point >= early_point)) // check to see if acceleration is durring increasing smoke
            {
                if (++conf_check_acceleration >= db_get_value(db_accel_confidence_check))
                {
                    fast_accelerating_fire = true;
                }
            }
            else
            {
                conf_check_acceleration = 0;
            }
        }
        else
        {
            conf_check_acceleration = 0;
        }
    }

    //----------------------------------- fail safe fire -----------------------------------
    // Check fail safe fire if it hits
    const uint8_t failsafe_delta = photo_mode == PTT_MODE ? 0x20 : db_get_value(db_fail_safe_high_threshold);
    const uint8_t failsafe_confidence_checks = photo_mode == PTT_MODE ? 1 : db_get_value(db_fail_safe_confidence_check);

    if (delta_val > failsafe_delta)
    {
        if (++conf_check_fail_Safe >= failsafe_confidence_checks)
        {
            fail_safe_fire = true;
        }
    }
    else
    {
        conf_check_fail_Safe = 0;
    }

    //----------------------------------- return values -----------------------------------

    uint8_t in_alarm    = ((uint8_t)slow_smoldering_fire) |                              // smoldering fire bit
                       ((uint8_t)fast_flaming_fire << FAST_FLAIMG_FIRE_BIT) |            // fast flaming fire bit
                       ((uint8_t)fast_accelerating_fire << FAST_ACCELERATING_FIRE_BIT) | // acceleration fire bit
                       ((uint8_t)fail_safe_fire << FAIL_SAFE_FIRE_BIT);                  // fail safe fire bit

    ///< \todo temporary latch.  will need to be re-thought / updated
    static uint8_t in_alarm_prev = 0;
    if (delta_val >= db_get_value(db_accel_min_threshold) || delta_val >= db_get_value(db_ror_min_threshold))
    {
        in_alarm |= in_alarm_prev;
    }
    else
    {
        in_alarm      = 0;
        in_alarm_prev = 0;
    }
    
    in_alarm_prev = in_alarm;
    ///< \todo END temporary latch.  will need to be re-thought / updated

    if (in_alarm)  // only set hushable bit if in alarm. 
    {
        in_alarm |= ((uint8_t)is_hushable(delta_val) << HUSHABLE_BIT);
    }

    return in_alarm;
}

//*********************************************************************
/**
 * \brief Take the current value of smoldering counter for serial output
 *
 * \returns The current value of smoldering counter
 */
//*********************************************************************
uint16_t get_smold_counter_value(void)
{
    return smold_count;
}

//*********************************************************************
/**
 * \brief Take rate of rise value from the algorithm for serial output
 *
 * \returns The rate of rise value
 */
//*********************************************************************
uint16_t get_rate_of_rise_value(void)
{
    return rate_of_rise;
}

//*********************************************************************
/**
 * \brief Take acceleration value from the algorithm for serial output
 *
 * \returns The acceleration value
 */
//*********************************************************************
uint16_t get_acceleration_value(void)
{
    return simple_acc;
}

//*********************************************************************
/**
 * \brief get_buf_index for serial output
 *
 * \returns the current buffer index
 */
//*********************************************************************
uint8_t get_buf_index(void)
{
    return buf_index;
}

//*********************************************************************
/**
 * \brief Check if the specific bit in a byte is set
 *
 * \param c The byte to check
 * \param n The position of the bit to check
 *
 * \returns Return true if the specific bit is set, false otherwise.
 */
//*********************************************************************
bool is_bit_n_set(uint8_t c, int n)
{
    // bit count from right to left.  bit 0 = 1
    return (c & (1 << n)) != 0;
}
