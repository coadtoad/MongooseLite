//******************************************************************
/*! \file in_photo_mwma.c
 *
 * \brief Kidde: Mongoose-fw - Implement the photo MWMA input functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "app_supervisor.h"
#include "drv_events.h"
#include "drv_photo.h"
#include "drv_serial.h"
#include "drv_timer.h"
#include "in_photo.h"
#include "in_photo_mwma_decision.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_mwma.h"
#include "task_list.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define MASTER_SMOKE_HOLDOFF             40000

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define NORMAL_MODE_DELAY                (CONFIG_NORMAL_MODE_PERIOD * 1000u) // normal mode delay in ms

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define ANALYSIS_MODE_DELAY              1000

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define HANDLE_MASTER_SMOKE_HOLDOFF_MASK 0x4000

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define DO_FILTER_CHAIN_UPDATE_MASK      0x2000

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define IN_NORMAL_MODE_MASK              0x1000

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define FILTER_INDEX_MASK                0x0FFF

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define NORMAL_IR_F_MASK                 0x01

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define NORMAL_IR_B_MASK                 0x02

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define NORMAL_BL_F_MASK                 0x04

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define PHOTO_AMP_S2_SAT_CNT             0xF5

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define MAX_CLEAN_AIR_CHANGE             30

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define LOW_GAIN_QUSI                    0x8A

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint16_t get_normal_mode_delay(void);

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
typedef struct
{
    uint16_t pre_a;
    uint16_t pre_b;
    uint16_t x0;
} filter_info;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
typedef enum
{
    NORMAL_MODE,
    ANALYSIS_MODE,
    ALARM_MODE
} photo_mode_e;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
// calculation and measurment related globals.
filter_info ir_f_filters     = {0, 0, 0};

filter_info ir_b_filters     = {0, 0, 0};
filter_info bl_f_filters     = {0, 0, 0};

filter_info ir_f_filters_lo  = {0, 0, 0};
filter_info ir_b_filters_lo  = {0, 0, 0};
filter_info bl_f_filters_lo  = {0, 0, 0};

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static bool IR_F_override    = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static bool IR_B_override    = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static bool Bl_F_override    = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint8_t override_ir_f = 0;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint8_t override_bl_f = 0;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint8_t override_ir_b = 0;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint8_t ir_f_qusi_lo;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint8_t ir_b_qusi_lo;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint8_t bl_f_qusi_lo;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
volatile static uint16_t max_filter_value = 0;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
// probably doesnt need to be a global
static bool alarm_active                  = 0;
static bool in_analysis_mode              = 0;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
// photo fault globals
bool in_fwd_ambient_fault                 = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
bool in_bck_ambient_fault                 = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
bool in_fwd_IR_fault                      = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
bool in_bck_IR_fault                      = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
bool in_bck_blue_fault                    = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static bool in_AFE_fault                  = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
// sounder avoid globals
static bool avoid                         = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static bool run                           = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static bool normal_mode                   = true;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint8_t smoke_holdoff              = 0;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
/******************************************************************************
 * Function : update_filters()
 *
 * \b Description:
 *
 * The filters are updated
 *
 *This update is solely based on the filter update equations:
 * pre_a : a[t] = (RawADC[t] + (3*A[t-1]))/4
 * pre_b : b[t] = (RawADC[t] + B[t-1])/2
 * Filter X0 : X0[t] = 2*a[t] - b[t]
 *
 * PRE-CONDITION: filter info is pointing at the filter to update  <br>
 *
 * POST-CONDITION:the filters are updated
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 *****************************************************************************/
void update_filters(uint8_t const value, filter_info *const filters)
{
    // Expand to 24.8 fixed point
    uint32_t const raw            = ((uint16_t)value) << 8;
    uint32_t const previous_pre_a = filters->pre_a;
    uint32_t const previous_pre_b = filters->pre_b;

    // Calculate prefilter A
    uint32_t const pre_a          = (raw + 3 * previous_pre_a) >> 2;
    filters->pre_a                = pre_a;

    // Calculate prefilter B
    uint32_t const pre_b          = (raw + previous_pre_b) >> 1;
    filters->pre_b                = pre_b;

    // Calculate filter X0
    if ((2 * pre_a) > pre_b)
    {
        filters->x0 = 2 * pre_a - pre_b;
    }
    else
    {
        filters->x0 = 0;
    }

    ///< \todo Store historical IR-F X0 max/min values
}

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
void reset_data(void)
{
    uint16_t const bl_f_qusi = (uint16_t)db_get_value(db_blue_clean_air) << 8;
    bl_f_filters.pre_a       = bl_f_qusi;
    bl_f_filters.pre_b       = bl_f_qusi;
    bl_f_filters.x0          = bl_f_qusi;

    bl_f_filters_lo.pre_a    = bl_f_qusi_lo << 8;
    bl_f_filters_lo.pre_b    = bl_f_qusi_lo << 8;
    bl_f_filters_lo.x0       = bl_f_qusi_lo << 8;

    uint16_t const ir_b_qusi = (uint16_t)db_get_value(db_ir_b_clean_air) << 8;
    ir_b_filters.pre_a       = ir_b_qusi;
    ir_b_filters.pre_b       = ir_b_qusi;
    ir_b_filters.x0          = ir_b_qusi;

    ir_b_filters_lo.pre_a    = ir_b_qusi_lo << 8;
    ir_b_filters_lo.pre_b    = ir_b_qusi_lo << 8;
    ir_b_filters_lo.x0       = ir_b_qusi_lo << 8;

    reset_photo_decision_counters();
}

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
/******************************************************************************
 * Function : send_enter_exit_alarm()
 *
 * \b Description:
 *
 * This function either sends msg_enter_smoke_alarm or msg_exit_smoke_alarm to state
 *
 * If the boolean alarm_state is true send enter smoke, if it is false send exit_smoke
 * Send data hushable which indicates to state if the smoke is hushable (from is_hushable)
 *
 * PRE-CONDITION:
 *
 * POST-CONDITION: The returned value is true if there should be a fail safe flag
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 *****************************************************************************/
void send_enter_exit_alarm(bool const alarm_state, bool hushable)
{
    static bool alarm_hushable_shadow = false;
    if (alarm_state && !smoke_holdoff)
    {
        if (!alarm_active)
        {
            // Entering a new fire event
            ///< \todo Record history
            alarm_active = 1;
            schedule_message(0, task_state, msg_enter_smoke_alarm, hushable);
            alarm_hushable_shadow = hushable;
        }
        else if (hushable != alarm_hushable_shadow)
        {
            // Hushable state has changed, inform state
            schedule_message(0, task_state, msg_enter_smoke_alarm, hushable);
            alarm_hushable_shadow = hushable;
        }
    }
    else if (alarm_active)
    {
        alarm_active          = 0;
        alarm_hushable_shadow = 0;
        schedule_message(0, task_state, msg_exit_smoke_alarm, 0);
        ++smoke_holdoff;
        schedule_message(MASTER_SMOKE_HOLDOFF, task_photo, msg_continue, HANDLE_MASTER_SMOKE_HOLDOFF_MASK);
    }
}

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint16_t get_normal_mode_delay(void)
{
    uint16_t normal_mode_delay = db_get_value(db_normal_mode_period);
    /*make sure the delay is in between min and max limit*/
    if (normal_mode_delay >= NORMAL_MODE_DELAY_MIN && normal_mode_delay <= NORMAL_MODE_DELAY_MAX)
    {
        // return the value in ms
        return normal_mode_delay * 1000;
    }
    else
    {
        // if value is not within max and min limit, return default normal mode delay
        return NORMAL_MODE_DELAY;
    }
}

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
/******************************************************************************
 * Function : run_normal_mode()
 *
 * \b Description:
 *
 * This function performs smoke measurements in normal (standby) mode.
 *
 * Function takes LED forward measurements with IR LED off (dark measurement) and on.
 * The measurement is compared to previous measurement and if delta is bid then analysis
 * mode is entered.
 * If delta is very big alarm message is sent immediately.
 *
 * PRE-CONDITION: All filter chains are initialized
 *
 * POST-CONDITION: Next run_normal_mode() function call is scheduled in 10sec.
 * or run_analysis_mode() is scheduled.
 *
 * @see
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 *****************************************************************************/
void run_normal_mode(uint16_t filter_index)
{
    supervisor_counter_increase(counter_photo);

    in_analysis_mode         = 0;
    uint8_t sensors_to_check = db_get_value(db_photo_normal_sensors);
    if (!sensors_to_check)
    {
        sensors_to_check = NORMAL_IR_F_MASK; // always make sure at least one sensor is used.
    }

    if (!begin_photo_read())
    {
        uint16_t new_data = filter_index & FILTER_INDEX_MASK;
        new_data |= IN_NORMAL_MODE_MASK;
        schedule_message(get_normal_mode_delay(), task_photo, msg_continue, new_data);
        return;
    }

    // wait microseconds for the circuit to stabilize
    timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

    // --------------------- Dark forward read ---------------------
    uint8_t dark_f, comp_f = 0;
    if ((sensors_to_check & NORMAL_IR_F_MASK) || (sensors_to_check & NORMAL_BL_F_MASK))
    {
        // take a forward dark measurement
        dark_f = read_dark(IR_F, &comp_f);
    }

    // --------------------- LIGHT IR - F - Read ---------------------
    uint8_t light_ir_f = 0;
    if (sensors_to_check & NORMAL_IR_F_MASK)
    {
        if (IR_F_override)
        {
            light_ir_f = override_ir_f;
        }
        else
        {
            light_ir_f = read_light(IR_F, comp_f, db_get_value(db_current_ir_f));
            led_off();
        }
    }

    // --------------------- LIGHT BL - F - Read ---------------------
    uint8_t light_bl_f = 0;
    if (sensors_to_check & NORMAL_BL_F_MASK)
    {
        if (Bl_F_override)
        {
            light_bl_f = override_bl_f;
        }
        else
        {
            light_bl_f = read_light(BL_F, comp_f, db_get_value(db_current_bl_f));
            led_off();
        }
    }

    // --------------------- Dark backward read ---------------------
    uint8_t dark_b, comp_b = 0;
    uint8_t light_ir_b = 0;
    if ((sensors_to_check & NORMAL_IR_B_MASK))
    {
        dark_b = read_dark(IR_B, &comp_b);

        // --------------------- LIGHT IR - B - Read ---------------------
        if (IR_B_override)
        {
            light_ir_b = override_ir_b;
        }
        else
        {
            light_ir_b = read_light(IR_B, comp_b, db_get_value(db_current_ir_b));
            led_off();
        }
    }
    // --------------------- END IR - B - Read ---------------------

    ++filter_index;

    // M2 circuit requires 200us based on the circuit time constant.
    // Earlier photo stabilization added to this is a little over 200us.
    timer_delay_us(100);

    // photo power, the IR DAC, and the CAV DAC are all disabled
    end_photo_read();
    // --------------------- END PHOTO Read ---------------------
    // --------------------- Validate Dark ---------------------
    bool dark_b_valid, dark_f_valid = false;

    if ((sensors_to_check & NORMAL_IR_F_MASK) || (sensors_to_check & NORMAL_BL_F_MASK))
    {
        dark_f_valid = validate_dark_f(dark_f);
    }
    if ((sensors_to_check & NORMAL_IR_B_MASK))
    {
        dark_b_valid = validate_dark_b(dark_b);
    }

    // --------------------- LIGHT IR - F - validate ---------------------
    uint8_t ir_f_delta           = 0;
    uint8_t ir_f_clean_air_count = db_get_value(db_ir_f_clean_air);
    if (sensors_to_check & NORMAL_IR_F_MASK)
    {
        // Validate measurements
        validate_light_ir_f(light_ir_f);

        // if the dark measurement is valid update the filters and the filter chain
        if (dark_f_valid)
        {
            update_filters(light_ir_f, &ir_f_filters);
        }

        // calculate Deltas
        if ((uint8_t)(ir_f_filters.x0 >> 8) > ir_f_clean_air_count)
        {
            ir_f_delta = (uint8_t)(ir_f_filters.x0 >> 8) - ir_f_clean_air_count;
        }
    }
    // --------------------- LIGHT BL - F - Read ---------------------
    uint16_t bl_f_delta          = 0;
    uint8_t bl_f_clean_air_count = db_get_value(db_blue_clean_air);
    if (sensors_to_check & NORMAL_BL_F_MASK)
    {
        // Validate measurements
        // blue forward never validated

        if (dark_f_valid) // no if (ir_b_delta_analysis > 0) because false if so.
        {
            update_filters(light_bl_f, &bl_f_filters);
        }

        // calculate Deltas
        if ((uint8_t)(bl_f_filters.x0 >> 8) > bl_f_clean_air_count)
        {
            bl_f_delta = (uint8_t)(bl_f_filters.x0 >> 8) - bl_f_clean_air_count;
        }
    }

    // --------------------- LIGHT IR - B - Validate ---------------------
    uint16_t ir_b_delta          = 0;
    uint8_t ir_b_clean_air_count = db_get_value(db_ir_b_clean_air);
    if (sensors_to_check & NORMAL_IR_B_MASK)
    {
        // Validate measurements
        validate_light_ir_b(light_ir_b);

        if (dark_b_valid) // no if (ir_b_delta_analysis > 0) because false if so.
        {
            update_filters(light_ir_b, &ir_b_filters);
        }

        // calculate Deltas
        if ((uint8_t)(ir_b_filters.x0 >> 8) > ir_b_clean_air_count)
        {
            ir_b_delta = (uint8_t)(ir_b_filters.x0 >> 8) - ir_b_clean_air_count;
        }
    }

    // update ir led faults after all validations have completed
    update_ir_photo_fault_kvs();

    // Alarm - Sudden jump in IR-F -> raise the alarm_threshold_fire flag
    bool const alarm_threshold_fire = alarm_threshold(ir_f_delta);

    if ((ir_f_delta >= db_get_value(db_photo_ir_f_delta)) || (ir_b_delta >= db_get_value(db_photo_ir_b_delta)) ||
        (bl_f_delta >= db_get_value(db_photo_bl_f_delta)) ||
        (db_get_value(db_serial_flags) & enable_force_analysis_mode))
    {
        // Enter analysis mode if the delta is large enough
        uint16_t new_data = filter_index & FILTER_INDEX_MASK;
        schedule_message(ANALYSIS_MODE_DELAY, task_photo, msg_continue, new_data);
        // Log event to note analysis mode entry
        schedule_message(0, task_events, msg_record_history, event_enter_analysis);
    }
    else
    {
        // Still in normal mode if the delta isn't sufficiently large
        uint16_t new_data = filter_index & FILTER_INDEX_MASK;
        new_data |= IN_NORMAL_MODE_MASK;
        schedule_message(get_normal_mode_delay(), task_photo, msg_continue, new_data);
    }

    uint8_t threshold = db_get_value(db_photo_alarm_eqa_threshold);
    uint8_t smoke_percent_fs;
    if (threshold > ir_f_clean_air_count)
    {
        // Calculate the key value for the WM smoke percent of full scale.
        smoke_percent_fs = (uint8_t)(((uint16_t)ir_f_delta * 100) / ((uint16_t)threshold - ir_f_clean_air_count));
    }
    else
    {
        smoke_percent_fs = 0;
    }

    // To find the percentage of compensation, find the delta between the
    // factory CAV and the current compensated CAV.  Then divide by the
    // factory CAV to get a percentage.
    int16_t factory_cav = (int16_t)db_get_value(db_ir_f_factory_clean_air);
    int16_t comp_delta  = (int16_t)ir_f_clean_air_count - factory_cav;

    // Convert to percentage.
    comp_delta          = (uint8_t)((comp_delta * 100) / factory_cav);

    // Set the key values for both percent and comp.
    schedule_message(0, task_key_value_store, msg_set_value,
                     ((uint16_t)smoke_percent_fs << 8) | key_smoke_alarm_percent);
    schedule_message(0, task_key_value_store, msg_set_value, ((uint16_t)comp_delta << 8) | key_smoke_comp);

    // Update values in the key value store for serial output
    if (serial_enabled())
    {
        uint8_t fire_flags = (uint8_t)alarm_threshold_fire;
        uint8_t photo_fault_flags =
            ((uint8_t)in_fwd_ambient_fault) | ((uint8_t)in_bck_ambient_fault << bck_ambient_fault) |
            ((uint8_t)in_fwd_IR_fault << fwd_IR_fault) | ((uint8_t)in_bck_IR_fault << bck_IR_fault) |
            ((uint8_t)in_bck_blue_fault << bck_blue_fault) | ((uint8_t)in_AFE_fault << afe_fault);

        uint16_t serial_flags = db_get_value16(db_serial_enable_msb, db_serial_enable_lsb);
        if (serial_flags & serial_enable_edwards_mode)
        {
            // Use Normal mode if smoke_status bit is not set
            // Variables that are not calculated during Normal Sampling mode
            //  are sent as zero values
            char buffer[97] = {0};
            uint8_to_hex(buffer + 0, light_ir_f); // 1
            buffer[2] = ' ';
            uint8_to_hex(buffer + 3, light_ir_b); // 2
            buffer[5] = ' ';
            uint8_to_hex(buffer + 6, light_bl_f); // 3
            buffer[8] = ' ';
            uint8_to_hex(buffer + 9, ir_f_filters.x0 >> 8); // 4
            buffer[11] = ' ';
            uint8_to_hex(buffer + 12, ir_b_filters.x0 >> 8); // 5
            buffer[14] = ' ';
            uint8_to_hex(buffer + 15, bl_f_filters.x0 >> 8); // 6
            buffer[17] = ' ';
            uint8_to_hex(buffer + 18, ir_f_filters_lo.x0 >> 8); // 7
            buffer[20] = ' ';
            uint8_to_hex(buffer + 21, ir_b_filters_lo.x0 >> 8); // 8
            buffer[23] = ' ';
            uint8_to_hex(buffer + 24, bl_f_filters_lo.x0 >> 8); // 9
            buffer[26] = ' ';
            uint8_to_hex(buffer + 27, dark_f); // 10
            buffer[29] = ' ';
            uint8_to_hex(buffer + 30, dark_b); // 11
            buffer[32] = ' ';
            uint8_to_hex(buffer + 33, 0); // 12   ratio
            buffer[35] = ' ';
            uint8_to_hex(buffer + 36, 0); // 13   ratio
            buffer[38] = ' ';
            uint8_to_hex(buffer + 39, 0); // 14   ratio
            buffer[41] = ' ';
            uint8_to_hex(buffer + 42, 0); // 15   ratio
            buffer[44] = ' ';
            uint8_to_hex(buffer + 45, 0); // 16   ratio
            buffer[47] = ' ';
            uint8_to_hex(buffer + 48, 0); // 17   ratio
            buffer[50] = ' ';
            uint8_to_hex(buffer + 51, fire_flags); // 18
            buffer[53] = ' ';
            uint8_to_hex(buffer + 54, photo_fault_flags); // 19
            buffer[56] = ' ';
            uint8_to_hex(buffer + 57, ir_f_clean_air_count); // 20
            buffer[59] = ' ';
            uint8_to_hex(buffer + 60, 0); // 21
            buffer[62] = ' ';
            uint8_to_hex(buffer + 63, ir_b_clean_air_count); // 22
            buffer[65] = ' ';
            uint8_to_hex(buffer + 66, bl_f_clean_air_count); // 23
            buffer[68] = ' ';
            uint8_to_hex(buffer + 69, comp_f); // 24
            buffer[71] = ' ';
            uint8_to_hex(buffer + 72, comp_b); // 25
            buffer[74] = ' ';
            uint8_to_hex(buffer + 75, 0); // 26
            buffer[77] = ' ';
            uint8_to_hex(buffer + 78, ir_f_delta); // 27
            buffer[80] = ' ';
            uint8_to_hex(buffer + 81, ir_b_delta); // 28
            buffer[83] = ' ';
            uint8_to_hex(buffer + 84, bl_f_delta); // 29
            buffer[86] = ' ';
            uint8_to_hex(buffer + 87, filter_index >> 8); // 30
            buffer[89] = ' ';
            uint8_to_hex(buffer + 90, filter_index); // 31
            buffer[92] = ' ';
            buffer[93] = ' ';
            buffer[94] = alarm_active ? 'A' : 'N';
            buffer[95] = '\r';
            serial_send_string(buffer);
        }
    }
}

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
/******************************************************************************
 * Function : run_normal_mode()
 *
 * \b Description:
 *
 * This function performs smoke measurements in normal (standby) mode.
 *
 * Function takes LED forward measurements with IR LED off (dark measurement) and on.
 * The measurement is compared to previous measurement and if delta is bid then analysis
 * mode is entered.
 * If delta is very big alarm message is sent immediately.
 *
 * PRE-CONDITION: All filter chains are initialized
 *
 * POST-CONDITION: Next run_normal_mode() function call is scheduled in 10sec.
 * or run_analysis_mode() is scheduled.
 *
 * @see
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 *****************************************************************************/
void run_analysis_mode(uint16_t filter_index, bool const do_filter_update)
{
    if (!begin_photo_read())
    {
        uint16_t new_data = filter_index & FILTER_INDEX_MASK;
        if (!do_filter_update)
        {
            new_data |= DO_FILTER_CHAIN_UPDATE_MASK; // update the filter chain every other analysis
        }
        schedule_message(ANALYSIS_MODE_DELAY, task_photo, msg_continue, new_data);
        return;
    }
    supervisor_counter_increase(counter_photo);

    // wait microseconds for the circuit to stabilize
    timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

    // take a dark measurement
    uint8_t comp_f;
    uint8_t const dark_f = read_dark(IR_F, &comp_f);
    // take a light measurement
    uint8_t light_ir_f;
    if (IR_F_override)
    {
        light_ir_f = override_ir_f;
    }
    else
    {
        light_ir_f = read_light(IR_F, comp_f, db_get_value(db_current_ir_f));
    }
    uint8_t const light_ir_f_lo = read_light_low_gain();
    led_off();

    // take a blue led forward measurement
    uint8_t light_bl_f;
    if (Bl_F_override)
    {
        light_bl_f = override_bl_f;
    }
    else
    {
        light_bl_f = read_light(BL_F, comp_f, db_get_value(db_current_bl_f));
    }
    uint8_t const light_bl_f_lo = read_light_low_gain();
    led_off();

    uint8_t comp_b;
    // take dark and light ir back measurements
    uint8_t const dark_b = read_dark(IR_B, &comp_b);
    uint8_t light_ir_b;
    if (IR_B_override)
    {
        light_ir_b = override_ir_b;
    }
    else
    {
        light_ir_b = read_light(IR_B, comp_b, db_get_value(db_current_ir_b));
    }

    uint8_t const light_ir_b_lo = read_light_low_gain();
    led_off();

    // M2 circuit requires 200us based on the circuit time constant.
    // Earlier photo stabilization added to this is a little over 200us.
    timer_delay_us(100);

    // photo power, the IR DAC, and the CAV DAC are all disabled
    end_photo_read();

    // Validate measurements
    bool valid_dark_f = validate_dark_f(dark_f);
    bool valid_dark_b = validate_dark_b(dark_b);
    validate_light_ir_f(light_ir_f);
    validate_light_ir_b(light_ir_b);
    update_ir_photo_fault_kvs();

    // Update filters
    if (valid_dark_f && valid_dark_b)
    {
        if (!in_analysis_mode)
        { // if it is the first time in analysis mode, initialize all filters
            in_analysis_mode      = 1;

            ir_f_filters.pre_a    = (uint16_t)(light_ir_f << 8);
            ir_f_filters.pre_b    = (uint16_t)(light_ir_f << 8);
            ir_f_filters.x0       = (uint16_t)(light_ir_f << 8);

            ir_b_filters.pre_a    = (uint16_t)(light_ir_b << 8);
            ir_b_filters.pre_b    = (uint16_t)(light_ir_b << 8);
            ir_b_filters.x0       = (uint16_t)(light_ir_b << 8);

            bl_f_filters.pre_a    = (uint16_t)(light_bl_f << 8);
            bl_f_filters.pre_b    = (uint16_t)(light_bl_f << 8);
            bl_f_filters.x0       = (uint16_t)(light_bl_f << 8);

            ir_f_filters_lo.pre_a = (uint16_t)(light_ir_f_lo << 8);
            ir_f_filters_lo.pre_b = (uint16_t)(light_ir_f_lo << 8);
            ir_f_filters_lo.x0    = (uint16_t)(light_ir_f_lo << 8);

            ir_b_filters_lo.pre_a = (uint16_t)(light_ir_b_lo << 8);
            ir_b_filters_lo.pre_b = (uint16_t)(light_ir_b_lo << 8);
            ir_b_filters_lo.x0    = (uint16_t)(light_ir_b_lo << 8);

            bl_f_filters_lo.pre_a = (uint16_t)(light_bl_f_lo << 8);
            bl_f_filters_lo.pre_b = (uint16_t)(light_bl_f_lo << 8);
            bl_f_filters_lo.x0    = (uint16_t)(light_bl_f_lo << 8);
        }

        update_filters(light_ir_f, &ir_f_filters);
        update_filters(light_ir_b, &ir_b_filters);
        update_filters(light_bl_f, &bl_f_filters);
        update_filters(light_ir_f_lo, &ir_f_filters_lo);
        update_filters(light_ir_b_lo, &ir_b_filters_lo);
        update_filters(light_bl_f_lo, &bl_f_filters_lo);

        if (do_filter_update)
        { // every other pass through analysis mode, update the filter chain
            ++filter_index;
        }
    }

    // Calculate deltas for ir_f, ir_b, and bl_f
    uint8_t const ir_f_clean_air_count = db_get_value(db_ir_f_clean_air);
    uint8_t ir_f_delta                 = 1;
    if ((uint8_t)(ir_f_filters.x0 >> 8) > ir_f_clean_air_count)
    {
        ir_f_delta = (uint8_t)(ir_f_filters.x0 >> 8) - ir_f_clean_air_count;
    }

    uint8_t const ir_b_clean_air_count = db_get_value(db_ir_b_clean_air);
    uint8_t ir_b_delta                 = 1;
    if ((uint8_t)(ir_b_filters.x0 >> 8) > ir_b_clean_air_count)
    {
        ir_b_delta = (uint8_t)(ir_b_filters.x0 >> 8) - ir_b_clean_air_count;
    }

    uint8_t const bl_f_clean_air_count = db_get_value(db_blue_clean_air);
    uint8_t bl_f_delta                 = 1;
    if ((uint8_t)(bl_f_filters.x0 >> 8) > bl_f_clean_air_count)
    {
        bl_f_delta = (uint8_t)(bl_f_filters.x0 >> 8) - bl_f_clean_air_count;
    }

    uint8_t ir_f_delta_lo = 1;
    if ((uint8_t)(ir_f_filters_lo.x0 >> 8) > ir_f_qusi_lo)
    {
        ir_f_delta_lo = (uint8_t)(ir_f_filters_lo.x0 >> 8) - ir_f_qusi_lo;
    }

    uint8_t ir_b_delta_lo = 1;
    if ((uint8_t)(ir_b_filters_lo.x0 >> 8) > ir_b_qusi_lo)
    {
        ir_b_delta_lo = (uint8_t)(ir_b_filters_lo.x0 >> 8) - ir_b_qusi_lo;
    }

    uint8_t bl_f_delta_lo = 1;
    if ((uint8_t)(bl_f_filters_lo.x0 >> 8) > bl_f_qusi_lo)
    {
        bl_f_delta_lo = (uint8_t)(bl_f_filters_lo.x0 >> 8) - bl_f_qusi_lo;
    }

    uint16_t ir_f_ir_b_ratio;
    uint16_t bl_f_ir_b_ratio;
    uint16_t bl_f_ir_f_ratio;

    // calculate ratios from deltas
    if ((light_ir_f >= PHOTO_AMP_S2_SAT_CNT) || (light_ir_b >= PHOTO_AMP_S2_SAT_CNT))
    {
        ir_f_ir_b_ratio = ir_f_delta_lo * 100 / ir_b_delta_lo;
    }
    else
    {
        ir_f_ir_b_ratio = ir_f_delta * 100 / ir_b_delta;

        // Adjust the ratio based on the Ratio Adjustment Factors calculated
        // During in-line cal.
        ir_f_ir_b_ratio = ((uint32_t)ir_f_ir_b_ratio * db_get_value(irf_irb_ratio_factor)) / 100;
    }

    if ((light_bl_f >= PHOTO_AMP_S2_SAT_CNT) || (light_ir_b >= PHOTO_AMP_S2_SAT_CNT))
    {
        bl_f_ir_b_ratio = bl_f_delta_lo * 100 / ir_b_delta_lo;
    }
    else
    {
        bl_f_ir_b_ratio = bl_f_delta * 100 / ir_b_delta;

        // Adjust the ratio based on the Ratio Adjustment Factors calculated
        // During in-line cal.
        bl_f_ir_b_ratio = ((uint32_t)bl_f_ir_b_ratio * db_get_value(blf_irb_ratio_factor)) / 100;
    }

    if ((light_bl_f >= PHOTO_AMP_S2_SAT_CNT) || (light_ir_f >= PHOTO_AMP_S2_SAT_CNT))
    {
        bl_f_ir_f_ratio = bl_f_delta_lo * 100 / ir_f_delta_lo;
    }
    else
    {
        bl_f_ir_f_ratio = bl_f_delta * 100 / ir_f_delta;
    }

    uint8_t photo_fault_flags = ((uint8_t)in_fwd_ambient_fault) | ((uint8_t)in_bck_ambient_fault << bck_ambient_fault) |
                                ((uint8_t)in_fwd_IR_fault << fwd_IR_fault) |
                                ((uint8_t)in_bck_IR_fault << bck_IR_fault) |
                                ((uint8_t)in_bck_blue_fault << bck_blue_fault) | ((uint8_t)in_AFE_fault << afe_fault);

    //------------------------------------------  calculations complete ------------------------------------------
    uint8_t fire_flags;
    bool in_fire_alarm = determine_fire_modes(photo_fault_flags, ir_f_delta, ir_b_delta, bl_f_delta, ir_f_ir_b_ratio,
                                              bl_f_ir_b_ratio, &fire_flags);

    bool hushable_fire = is_bit_n_set(fire_flags, hushable_fire_bit);
    send_enter_exit_alarm(in_fire_alarm, hushable_fire);

    //------------------------------------------  determine if Analysis mode or normal mode
    //-----------------------------------------

    uint8_t const sensors_to_check = db_get_value(db_photo_normal_sensors);

    const bool below_ir_f_delta =
        (ir_f_delta < (db_get_value(db_photo_ir_f_delta) - 1) || !(sensors_to_check & NORMAL_IR_F_MASK));
    const bool below_ir_b_delta =
        (ir_b_delta < (db_get_value(db_photo_ir_b_delta) - 1) || !(sensors_to_check & NORMAL_IR_B_MASK));
    const bool below_bl_f_delta =
        (bl_f_delta < (db_get_value(db_photo_bl_f_delta) - 1) || !(sensors_to_check & NORMAL_BL_F_MASK));
    // if the ir_f_delta is less than the normal threshold or if there was any fault return to normal
    if (!in_fire_alarm &&                                                    // dont exit analisis if in alarm.
            !(db_get_value(db_serial_flags) & enable_force_analysis_mode) && // and not in forced mode
            (below_ir_f_delta & below_ir_b_delta &
             below_bl_f_delta) ||                 // if ir f delta is low and ir b delta is low (if enabled)
        (in_fwd_ambient_fault || in_fwd_IR_fault) // or a ir f fault
    )
    {
        // reset a bunch of data pertaining to analysis mode and go back to normal mode
        reset_data();
        uint16_t new_data = filter_index & FILTER_INDEX_MASK;
        new_data |= IN_NORMAL_MODE_MASK;
        schedule_message(get_normal_mode_delay(), task_photo, msg_continue, new_data);
        // Log event to note analysis mode exit
        schedule_message(0, task_events, msg_record_history, event_exit_analysis);
    }
    else
    {
        // Continue in analysis mode
        uint16_t new_data = filter_index & FILTER_INDEX_MASK;
        if (!do_filter_update)
        {
            new_data |= DO_FILTER_CHAIN_UPDATE_MASK; // update the filter chain every other analysis
        }
        schedule_message(ANALYSIS_MODE_DELAY, task_photo, msg_continue, new_data);
    }

    //------------------------------------------  Serial Output ------------------------------------------

    // Update values in the key value store for serial output
    if (serial_enabled())
    {

        uint16_t serial_flags = db_get_value16(db_serial_enable_msb, db_serial_enable_lsb);
        if (serial_flags & serial_enable_edwards_mode)
        {
            char buffer[97] = {0};
            uint8_to_hex(buffer + 0, light_ir_f); ///< 1	Photo.RawADC_S2[PHOTO_IR_FW]
            buffer[2] = ' ';
            uint8_to_hex(buffer + 3, light_ir_b); ///< 2	Photo.RawADC_S2[PHOTO_IR_BW]
            buffer[5] = ' ';
            uint8_to_hex(buffer + 6, light_bl_f); ///< 3	Photo.RawADC_S2[PHOTO_BLUE_FW]
            buffer[8] = ' ';
            uint8_to_hex(buffer + 9, ir_f_filters.x0 >> 8); ///< 4	Photo.X_S2[PHOTO_IR_FW][0] >> 8
            buffer[11] = ' ';
            uint8_to_hex(buffer + 12, ir_b_filters.x0 >> 8); ///< 5	Photo.X_S2[PHOTO_IR_BW][0] >> 8
            buffer[14] = ' ';
            uint8_to_hex(buffer + 15, bl_f_filters.x0 >> 8); ///< 6	Photo.X_S2[PHOTO_BLUE_FW][0] >> 8
            buffer[17] = ' ';
            uint8_to_hex(buffer + 18, ir_f_filters_lo.x0 >> 8); ///< 7	Photo.X_S1[PHOTO_IR_FW][0] >> 8
            buffer[20] = ' ';
            uint8_to_hex(buffer + 21, ir_b_filters_lo.x0 >> 8); ///< 8	Photo.X_S1[PHOTO_IR_BW][0] >> 8
            buffer[23] = ' ';
            uint8_to_hex(buffer + 24, bl_f_filters_lo.x0 >> 8); ///< 9	Photo.X_S1[PHOTO_BLUE_FW][0] >> 8
            buffer[26] = ' ';
            uint8_to_hex(buffer + 27, dark_f); ///< 10	Photo.AmbientLight[PHOTO_IR_FW]
            buffer[29] = ' ';
            uint8_to_hex(buffer + 30, dark_b); ///< 11	Photo.AmbientLight[PHOTO_IR_BW]
            buffer[32] = ' ';
            uint8_to_hex(buffer + 33, ir_f_ir_b_ratio >> 8); ///< 12	IrF_IrB_ratio >> 8
            buffer[35] = ' ';
            uint8_to_hex(buffer + 36, ir_f_ir_b_ratio); ///< 13	IrF_IrB_ratio & 0xFF
            buffer[38] = ' ';
            uint8_to_hex(buffer + 39, bl_f_ir_b_ratio >> 8); ///< 14	BlF_IrB_ratio >> 8
            buffer[41] = ' ';
            uint8_to_hex(buffer + 42, bl_f_ir_b_ratio); ///< 15	BlF_IrB_ratio & 0xFF
            buffer[44] = ' ';
            uint8_to_hex(buffer + 45, bl_f_ir_f_ratio >> 8); ///< 16	BlF_IrF_ratio >> 8
            buffer[47] = ' ';
            uint8_to_hex(buffer + 48, bl_f_ir_f_ratio); ///< 17	BlF_IrF_ratio & 0xFF
            buffer[50] = ' ';
            uint8_to_hex(buffer + 51, fire_flags); ///< 18	MW_Flage.Byte
            buffer[53] = ' ';
            uint8_to_hex(buffer + 54, photo_fault_flags); ///< 19	Photo_Status.Word >> 8
            buffer[56] = ' ';
            uint8_to_hex(buffer + 57, ir_f_clean_air_count); ///< 20
            buffer[59] = ' ';
            uint8_to_hex(buffer + 60, 0); ///< 21
            buffer[62] = ' ';
            uint8_to_hex(buffer + 63, ir_b_clean_air_count); ///< 22
            buffer[65] = ' ';
            uint8_to_hex(buffer + 66, bl_f_clean_air_count); ///< 23
            buffer[68] = ' ';
            uint8_to_hex(buffer + 69, comp_f); ///< 24	IR_Fwd_offset
            buffer[71] = ' ';
            uint8_to_hex(buffer + 72, comp_b); ///< 25	IR_Back_offset
            buffer[74] = ' ';
            uint8_to_hex(buffer + 75, 0); ///< 26
            buffer[77] = ' ';
            uint8_to_hex(buffer + 78, ir_f_delta); ///< 27	(uint8_t)IrF_change_S2
            buffer[80] = ' ';
            uint8_to_hex(buffer + 81, ir_b_delta); ///< 28	(uint8_t)IrB_change_S2
            buffer[83] = ' ';
            uint8_to_hex(buffer + 84, bl_f_delta); ///< 29	(uint8_t)BlF_change_S2
            buffer[86] = ' ';
            uint8_to_hex(buffer + 87, filter_index >> 8); ///< 30	Photo.FilterCounter >> 8
            buffer[89] = ' ';
            uint8_to_hex(buffer + 90, filter_index); ///< 31	Photo.FilterCounter & 0xFF
            buffer[92] = ' ';
            buffer[93] = ' ';
            buffer[94] = alarm_active ? 'A' : 'Z';
            buffer[95] = '\r';
            serial_send_string(buffer);
        }
    }
}

void photo_task(message_id const message, uint16_t const data)
{
    static bool do_filter_update = 0;
    static uint16_t filter_index = 0;
    switch (message)
    {
    case msg_init:
        smoke_holdoff = 0;
        // Initialize photo settings
        if (!photo_init())
        {
            // Schedule a fatal fault on failure
            schedule_message(1000, task_state, msg_fault_fatal, event_fault_fatal_afe);
        }

        ir_f_qusi_lo          = LOW_GAIN_QUSI;
        ir_b_qusi_lo          = LOW_GAIN_QUSI;
        bl_f_qusi_lo          = LOW_GAIN_QUSI;

        // Initialize filters
        ir_f_filters.pre_a    = (uint16_t)db_get_value(db_ir_f_clean_air) << 8;
        ir_f_filters.pre_b    = ir_f_filters.pre_a;
        ir_f_filters.x0       = ir_f_filters.pre_a;
        ///< \todo change the low gain clean air?
        ir_f_filters_lo.pre_a = ir_f_qusi_lo << 8;
        ir_f_filters_lo.pre_b = ir_f_filters_lo.pre_a;
        ir_f_filters_lo.x0    = ir_f_filters_lo.pre_a;

        ir_b_filters.pre_a    = (uint16_t)db_get_value(db_ir_b_clean_air) << 8;
        ir_b_filters.pre_b    = ir_b_filters.pre_a;
        ir_b_filters.x0       = ir_b_filters.pre_a;
        ///< \todo change the low gain clean air?
        ir_b_filters_lo.pre_a = ir_b_qusi_lo << 8;
        ir_b_filters_lo.pre_b = ir_b_filters_lo.pre_a;
        ir_b_filters_lo.x0    = ir_b_filters_lo.pre_a;

        bl_f_filters.pre_a    = (uint16_t)db_get_value(db_blue_clean_air) << 8;
        bl_f_filters.pre_b    = bl_f_filters.pre_a;
        bl_f_filters.x0       = bl_f_filters.pre_a;
        ///< \todo change the low gain clean air?
        bl_f_filters_lo.pre_a = bl_f_qusi_lo << 8;
        bl_f_filters_lo.pre_b = bl_f_filters_lo.pre_a;
        bl_f_filters_lo.x0    = bl_f_filters_lo.pre_a;

        ///< \todo Look at history data (ir-f min/max)

        reset_data();

        if (db_get_value(db_photo_hush_threshold) > 0xFA)
        {
            max_filter_value = 0xFA;
        }
        else
        {
            max_filter_value = 255 - (db_get_value(db_photo_hush_threshold) + 5);
        }
        max_filter_value <<= 8;

        // Schedule first periodic callback.
        // Initialization msg_value messages should already have finished
        schedule_message(1000, task_photo, msg_continue, 0x1000);
        schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_photo << 8) | key_avoidance_flags);
        schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_photo << 8) | key_ir_photo_faults);
        schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_photo << 8) | key_blue_led_fault);
        schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_photo << 8) | key_AFE_fault);
        break;
    case msg_continue:
    {
        bool const in_normal_mode = data & IN_NORMAL_MODE_MASK;
        do_filter_update          = data & DO_FILTER_CHAIN_UPDATE_MASK;
        filter_index              = data & FILTER_INDEX_MASK;
        bool const holdoff        = data & HANDLE_MASTER_SMOKE_HOLDOFF_MASK;

        if (holdoff)
        {
            --smoke_holdoff;
            break;
        }
        if (avoid)
        {
            run         = true;
            normal_mode = in_normal_mode;
            break;
        }

        if (in_normal_mode)
        {
            run_normal_mode(filter_index);
        }
        else
        {
            schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_photo);
        }
        break;
    }
    case msg_value:
    {
        // sort value and task_id from key_value_store subscriptions
        uint8_t const value = (uint8_t)(data >> 8);
        key_name const key  = (key_name)(uint8_t)(data);
        switch (key)
        {
        case key_avoidance_flags:
            avoid = data >> 8;
            if (!avoid && run)
            {
                if (normal_mode)
                {
                    run = false;
                    run_normal_mode(filter_index);
                }
                else
                {
                    schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_photo);
                }
            }
            break;
        case key_ir_photo_faults:
            in_fwd_ambient_fault = value & ((uint8_t)1 << kvs_fwd_ambient_fault);
            in_bck_ambient_fault = value & ((uint8_t)1 << kvs_bck_ambient_fault);
            in_fwd_IR_fault      = value & ((uint8_t)1 << kvs_fwd_ir_led_fault);
            in_bck_IR_fault      = value & ((uint8_t)1 << kvs_bck_ir_led_fault);
            break;
        case key_blue_led_fault:
            in_bck_blue_fault = value;
            break;
        case key_AFE_fault:
            in_AFE_fault = value;
            break;
        default:
            break;
        }
        break;
    }
    case msg_ready:
        if ((task_id)data == task_blue_charge_pump)
        {
            if (avoid)
            {
                normal_mode = false;
                run         = true;
                break;
            }
            run = false;
            run_analysis_mode(filter_index, do_filter_update);
        }
        break;
    case msg_set_flag:
        if (!data)
        {

            IR_F_override = 0;
            IR_B_override = 0;
            Bl_F_override = 0;
            override_ir_f = 0;
            override_ir_b = 0;
            override_bl_f = 0;
        }
        else if ((uint8_t)(data >> 8) == 0x01)
        {
            IR_F_override = 1;
            override_ir_f = (uint8_t)data;
        }
        else if ((uint8_t)(data >> 8) == 0x02)
        {
            IR_B_override = 1;
            override_ir_b = (uint8_t)data;
        }
        else if ((uint8_t)(data >> 8) == 0x04)
        {
            Bl_F_override = 1;
            override_bl_f = (uint8_t)data;
        }
        break;
    default:
        break;
    }
}
