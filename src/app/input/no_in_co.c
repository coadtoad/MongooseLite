//******************************************************************
/*! \file no_in_co.c
 *
 * \brief Kidde: Mongoose-fw - Implement co input functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_co.h"

void co_task(message_id const message, uint16_t const data)
{
    (void)message;
    (void)data;
}
