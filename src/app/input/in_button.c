//******************************************************************
/*! \file in_button.c
 *
 * \brief Kidde: Mongoose-fw - Input module for processing button presses+
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_button.h"
#include "app_serial.h"
#include "drv_button.h"
#include "drv_timer.h"
#include "hal_rtc.h"
#include "message.h"
#include "task_list.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief Period of button input sampling
 */
//*********************************************************************
#define BUTTON_SAMPLE_PERIOD 100

//*********************************************************************
/**
 * \brief period of stuck button press and hold
 */
//*********************************************************************
#define BUTTON_STUCK_PERIOD  12000u

//*********************************************************************
/**
 * \brief Defines the possible data values that can be returned by message_continue sent to task_button
 */
//*********************************************************************
typedef enum
{
    button_up,
    button_debounce,
    button_stuck_wait,
    button_stuck,
} button_data;

//*********************************************************************
/**
 * \brief The following variable is used to store time of the button held down
 */
//*********************************************************************
static uint64_t button_down_time = 0;

//*********************************************************************
/**
 * \brief The following variable is used to indicate the button processing is complete
 */
//*********************************************************************
static uint8_t button_state      = button_up;

void button_task(const message_id message, const uint16_t data)
{
    (void)data; // data parameter not used for button processing
    switch (message)
    {
    case msg_init:
        button_down_time = 0;
        button_state     = button_up;
        if (button_is_down())
        {
            //schedule_message(0, task_button, msg_ready, 0);
        }
        break;
    case msg_ready:
        // time stamp the button press
        button_down_time = time_rtc_to_64bit(rtc_get_subminute());
        // 100ms debounce
        button_state     = button_debounce;
        serial_send_string_debug("Button debounce.\r", serial_enable_debug);
        //schedule_message(BUTTON_SAMPLE_PERIOD, task_button, msg_continue, 0);
        break;
    case msg_continue:
        switch (button_state)
        {
        case button_debounce:
            // set button down state and monitor button activity
            if (button_is_down())
            {
                button_state = button_stuck_wait;
                // Let state handle the button being down
                //schedule_message(0, task_state, msg_button_down, 1);
                serial_send_string_debug("Button down.\r", serial_enable_debug);
                // Poll to see when button releases
                //schedule_message(BUTTON_SAMPLE_PERIOD, task_button, msg_continue, 0);
            }
            else
            {
                button_state = button_up;

                enable_button_intr();
                // reset and do nothing.
            }
            break;
        case button_stuck_wait:
            if (button_is_down())
            {
                /** @todo Check for other standard integer promotion crap */
                if (time_rtc_to_64bit(rtc_get_subminute()) - button_down_time >= BUTTON_STUCK_PERIOD)
                {
                    button_state = button_stuck;
                    // Let state handle the button being down for more than 10 seconds
                    //schedule_message(0, task_state, msg_button_stuck, 1);
                    serial_send_string_debug("Error. Button is stuck.\r", serial_enable_debug);
                }
                //schedule_message(BUTTON_SAMPLE_PERIOD, task_button, msg_continue, 0);
            }
            else
            {
                button_state = button_up;
                // Button released
                //schedule_message(0, task_state, msg_button_down, 0);
                serial_send_string_debug("Button pressed.\r", serial_enable_debug);

                enable_button_intr();
            }
            break;
        case button_stuck:
            if (button_is_down())
            {
                //schedule_message(BUTTON_SAMPLE_PERIOD, task_button, msg_continue, 0);
            }
            else
            {
                button_state = button_up;
                // Button released
                //schedule_message(0, task_state, msg_button_stuck, 0);
                serial_send_string_debug("Error. Button Unstuck.\r", serial_enable_debug);

                enable_button_intr();
            }
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}
