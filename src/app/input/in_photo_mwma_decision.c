//******************************************************************
/*! \file in_photo_mwma_decision.c
 *
 * \brief Kidde: Mongoose-fw - Implement the photo MWMA decision input functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "in_photo.h"

//*********************************************************************
/**
 * \brief Compare to below_min_delta_count, IR F waiting while IR B or BL not rising
 */
//*********************************************************************
#define IR_F_WAIT_IR_B_BL_NOT_RISING 30

//*********************************************************************
/**
 * \brief Hysteresis value removed from DB alarm threshold for alarm equation based on IR F delta
 */
//*********************************************************************
#define PHOTO_ALM_EQA_HYST           3 // Hysteresis

//*********************************************************************
/**
 * \brief Fire count to enter alarm after 5 consecutive fire readings
 * \note flags reset to 0 in reset_photo_decision_counters()
 */
//*********************************************************************
// flags reset to 0 in reset_photo_decision_counters()
static uint8_t fire_count             = 0;

//*********************************************************************
/**
 * \brief Debounce counter for fire count to ensure non-consecutive values are cleared correctly
 */
//*********************************************************************
static uint8_t fire_debounce_count    = 0;

//*********************************************************************
/**
 * \brief "Incremented to check for fault alarm based on delta counts of LEDs
 */
//*********************************************************************
static uint16_t below_min_delta_count = 0;

//*********************************************************************
/**
 * \brief Smoldering count to compare to smoldereing threshold to enter smoldering fire
 */
//*********************************************************************
static uint8_t smoldering_count       = 0;

bool is_bit_n_set(uint8_t c, int n)
{
    // bit count from right to left.  bit 0 = 1
    return (c & (1 << n)) != 0;
}

bool alarm_threshold(uint8_t ir_f_delta)
{
    static bool above_alarm_threshold = false;
    if (ir_f_delta > db_get_value(db_photo_alarm_eqa_threshold))
    {
        above_alarm_threshold = true;
    }
    else if (ir_f_delta <= db_get_value(db_photo_alarm_eqa_threshold) - PHOTO_ALM_EQA_HYST)
    {
        above_alarm_threshold = false;
    }

    return above_alarm_threshold;
}

bool determine_fire_modes(uint8_t photo_fault_flags, uint8_t ir_f_delta, uint8_t ir_b_delta, uint8_t bl_f_delta,
                          uint16_t ir_f_ir_b_ratio, uint16_t bl_f_ir_b_ratio, uint8_t *fire_flags)
{

    bool const alarm_threshold_fire = alarm_threshold(ir_f_delta);
    bool const fail_safe_fire       = ir_f_delta > db_get_value(db_photo_fail_safe_threshold);
    bool const hushable_fire        = ir_f_delta <= db_get_value(db_photo_hush_threshold) && !fail_safe_fire;

    // check_ratios, if there are no faults check if there is a mw_fire or a flaming_fire
    bool mw_fire                    = false;
    bool nuisance_fire              = false;
    bool flaming_fire               = false;
    bool smoldering_disable         = db_get_value(db_photo_smoldering_count_threshold) == 0xFF ||
                              db_get_value(db_photo_smoldering_count_threshold) == 0x00;

    // Only do MW MA if not in fault
    // if there are no faults check if there is a mw_fire or a flaming_fire
    if (photo_fault_flags == 0) // no faults
    {
        if (ir_f_delta >= db_get_value(db_photo_ir_f_delta) && ir_b_delta >= db_get_value(db_photo_ir_b_delta) &&
            bl_f_delta >= db_get_value(db_photo_bl_f_delta))
        {
            // ratio check
            flaming_fire =
                ir_f_ir_b_ratio < db_get_value16(db_flaming_ir_f_ir_b_threshold_msb,
                                                 db_flaming_ir_f_ir_b_threshold_lsb) && // ratio 1 > ratio 1 threshold
                bl_f_ir_b_ratio < db_get_value16(db_flaming_bl_f_ir_b_threshold_msb,
                                                 db_flaming_bl_f_ir_b_threshold_lsb); // ratio 2 > ratio 2 threshold

            if ((ir_f_ir_b_ratio > db_get_value16(db_photo_ir_f_ir_b_threshold_msb,
                                                  db_photo_ir_f_ir_b_threshold_lsb)) && // ratio 1 > ratio 3 threshold
                (bl_f_ir_b_ratio > db_get_value16(db_photo_bl_f_ir_b_threshold_msb,
                                                  db_photo_bl_f_ir_b_threshold_lsb))) // ratio 2 > ratio 4 threshold
            {
                nuisance_fire = true;
            }
            else
            {
                mw_fire = true;
            }
        }
        else
        {
            if (!smoldering_disable && smoldering_count < 0xFF)
            {
                smoldering_count++;
            }
        }
    }

    bool const smoldering_fire = smoldering_count > db_get_value(db_photo_smoldering_count_threshold);

    // Fault detection alarm Below Min Delta
    if (ir_f_delta < db_get_value(db_photo_ir_f_delta) || ir_b_delta < db_get_value(db_photo_ir_b_delta) ||
        bl_f_delta < db_get_value(db_photo_bl_f_delta))
    {
        if (below_min_delta_count != 0xffff)
        {
            ++below_min_delta_count;
        }
    }
    else
    {
        below_min_delta_count = 0;
    }
    bool below_min_delta      = below_min_delta_count > IR_F_WAIT_IR_B_BL_NOT_RISING; // normally false

    bool const alarm_flm_fire = ir_b_delta > db_get_value(db_flm_ir_b_threshold);
    //  Fire determination.
    bool mw_photo_faults      = is_bit_n_set(photo_fault_flags, bck_IR_fault) ||
                           is_bit_n_set(photo_fault_flags, bck_blue_fault) ||
                           is_bit_n_set(photo_fault_flags, bck_ambient_fault);

    bool fire = ((alarm_flm_fire && flaming_fire) ||  // min 5 counts to fire.
                 (alarm_threshold_fire && mw_fire) || // min 5 counts to fire   // smoldering fire included in mw_fire
                 (alarm_threshold_fire && smoldering_fire) || // min 1 count to fire
                 (alarm_threshold_fire && mw_photo_faults) || // min 1 count to fire
                 (alarm_threshold_fire && below_min_delta) || // min 30 counts to fire
                 fail_safe_fire);

    //  fire debounce for return and entrance into alarm
    // to enter alarm, fire must be detected 5 times, without 5 consecutive non fire readings.
    if (fire)
    {
        fire_debounce_count = 0;
        if (fire_count != 0xff)
        {
            ++fire_count;
        }
    }
    else
    {
        if (fire_count > 0)
        {
            ++fire_debounce_count;
            if (fire_debounce_count >= db_get_value(db_fire_debounce_count))
            {
                fire_count          = 0;
                fire_debounce_count = 0;
            }
        }
    }

    // set all fire flags for serial output
    *fire_flags = ((uint8_t)alarm_threshold_fire) | ((uint8_t)mw_fire << mw_fire_bit) |
                  ((uint8_t)flaming_fire << flaming_fire_bit) | ((uint8_t)nuisance_fire << nuisance_fire_bit) |
                  ((uint8_t)fail_safe_fire << fail_safe_fire_bit) | ((uint8_t)hushable_fire << hushable_fire_bit) |
                  ((uint8_t)smoldering_fire << smoldering_fire_bit) | ((uint8_t)below_min_delta << below_min_delta_bit);

    return fire_count >= db_get_value(db_fire_count);
}

void reset_photo_decision_counters(void)
{
    fire_count            = 0;
    fire_debounce_count   = 0;
    below_min_delta_count = 0;
    smoldering_count      = 0;
}