//******************************************************************
/*! \file in_battery.c
 *
 * \brief Kidde: Mongoose-fw - Declare battry input functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_battery.h"
#include "app_database.h"
#include "app_events.h"
#include "app_serial.h"
#include "drv_battery.h"
#include "drv_timer.h"
#include "drv_vboost.h"
#include "key_value_store.h"
#include "message.h"
#include "task_list.h"

//*********************************************************************
/**
 * \brief Number of battery samples on power up
 */
//*********************************************************************
#define POWERUP_TEST_COUNT      12 // number of battery samples on power up

//*********************************************************************
/**
 * \brief Interval for powerup tests to occur within
 * One Minute
 */
//*********************************************************************
#define POWERUP_TEST_INTERVAL   ONE_MINUTE

//*********************************************************************
/**
 * \brief No battery test interval alternate value
 * 10 seconds
 */
//*********************************************************************
#define NO_BATT_TEST_INTERVAL   (uint32_t)(10 * ONE_SECOND)

//*********************************************************************
/**
 * \brief Interval for a normal op test to schedule subsequent battery tests
 * 12 hours
 */
//*********************************************************************
#define NORMAL_OP_TEST_INTERVAL (uint32_t)(12 * ONE_HOUR)

//*********************************************************************
/**
 * \brief Wireless Module battery status high
 */
//*********************************************************************
#define WM_BAT_STATUS_HIGH      0x83

//*********************************************************************
/**
 * \brief Wireless Module battery status low
 */
//*********************************************************************
#define WM_BAT_STATUS_LOW       0x81

//*********************************************************************
/**
 * \brief Order of battery test procedures to get battery test data enum
 */
//*********************************************************************
typedef enum
{
    battery_test_ready,
    battery_prepare_measurement,
    battery_take_measurement,
    battery_process_measurment
} battery_task_data;

//*********************************************************************
/**
 * \brief Enum list of various battery state options
 */
//*********************************************************************
typedef enum
{
    battery_good,
    battery_failed_one_time,
    battery_has_failed,
    battery_fatal,
    battery_not_present
} battery_state;

//*********************************************************************
/**
 * \brief Boolean to see if the device is in AFE fault
 */
//*********************************************************************
static bool in_AFE_fault                   = false;

//*********************************************************************
/**
 * \brief Value for current battery state
 */
//*********************************************************************
static battery_state current_battery_state = battery_good;

//*********************************************************************
/**
 * \brief This Function decides what state the battery should be in based on input
 * \note input (bat_volt)
 *        voltage count from ADC
 *
 * \note Possible states:
 *          -# Good Battery				- battery_good
 *          -# Battery Low deteced once	- battery_failed_one_time
 *          -# Low Battery (hushable)		- battery_has_failed
 *          -# Low Battery (un-hushable)	- battery_fatal
 *          -# No Battery					- battery_not_present
 */
//*********************************************************************
static battery_state next_state(battery_state const current_battery_state, battery_status const status)
{
    static uint16_t start_hushable;

    battery_state new_state = current_battery_state;
    switch (current_battery_state)
    {
    case battery_good:
        if (status != good_battery)
        {
            new_state = battery_failed_one_time;
        }
        break;
    case battery_failed_one_time:
        switch (status)
        {
        case no_battery:
            new_state = battery_not_present;
            break;
        case hushable_low_battery:
            start_hushable = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);
            new_state      = battery_has_failed;
            break;
        case low_battery:
            new_state = battery_fatal;
            break;
        case good_battery:
            new_state = battery_good;
            break;
        }
        break;
    case battery_has_failed:
        switch (status)
        {
        case no_battery:
            new_state = battery_not_present;
            break;
        case hushable_low_battery:
            if (start_hushable + 7 < db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb))
            {
                new_state = battery_fatal;
            }
            break;
        case low_battery:
            new_state = battery_fatal;
            break;
        case good_battery:
            new_state = battery_good;
            break;
        }
        break;
    case battery_fatal:
        if (status == good_battery)
        {
            new_state = battery_good;
        }
        else if (status == no_battery)
        {
            new_state = battery_not_present;
        }
        break;
    case battery_not_present:
        switch (status)
        {
        case no_battery:
            break;
        case hushable_low_battery:
            start_hushable = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);
            new_state      = battery_has_failed;
            break;
        case low_battery:
            new_state = battery_fatal;
            break;
        case good_battery:
            new_state = battery_good;
            break;
        }
        break;
    }
    return new_state;
}

//*********************************************************************
/**
 * \brief Function to schedule the next battery test based on current state of battery
 */
//*********************************************************************
void schedule_next_battery_test(void)
{
    static uint8_t powerup_count = POWERUP_TEST_COUNT;

    if (in_AFE_fault)
    {
        //schedule_message(NORMAL_OP_TEST_INTERVAL, task_battery, msg_continue, battery_test_ready);
    }
    else if (current_battery_state == battery_not_present)
    {
        //schedule_message(NO_BATT_TEST_INTERVAL, task_battery, msg_continue, battery_test_ready);
    }
    else if (powerup_count)
    {
        powerup_count--;
        //schedule_message(POWERUP_TEST_INTERVAL, task_battery, msg_continue, battery_test_ready);
    }
    else
    {
        //schedule_message(NORMAL_OP_TEST_INTERVAL, task_battery, msg_continue, battery_test_ready);
    }
}

void battery_task(message_id const message, uint16_t const data)
{
    static battery_status status;
    static uint16_t batt_value = 0;
    uint8_t wm_batt_status     = WM_BAT_STATUS_HIGH;

    //                            012345678901
    static char batt_string[]  = "XX XXXX XXXX\r";

    switch (message)
    {
    case msg_init:
        // key_AFE_fault set during low VCC detect by the AFE
        //schedule_message(0, task_key_value_store, msg_add_callback, (uint16_t)task_battery << 8 | key_AFE_fault);
        //schedule_message(5000, task_battery, msg_continue, battery_test_ready);
        break;
    case msg_continue:
        switch ((battery_task_data)data)
        {
        case battery_test_ready:
            // Set this to 1700 to avoid between T3s
            schedule_timed_message(1700, task_battery, msg_continue, battery_prepare_measurement);
            break;
        case battery_prepare_measurement:
            battery_test_on();
            // Enable the VBoost for the battery
            vboost_battery_on();
            //schedule_message(BATTERY_STABILIZATION_TIME, task_battery, msg_continue, battery_take_measurement);
            break;
        case battery_take_measurement:
            status          = get_battery_status(&batt_value);
            batt_string[0]  = "0123456789ABCDEF"[(status >> 4) & 0x0f];
            batt_string[1]  = "0123456789ABCDEF"[status & 0x0f];
            batt_string[3]  = "0123456789ABCDEF"[(uint8_t)batt_value >> 12];
            batt_string[4]  = "0123456789ABCDEF"[(uint8_t)(batt_value >> 8) & 0x0f];
            batt_string[5]  = "0123456789ABCDEF"[(uint8_t)batt_value >> 4];
            batt_string[6]  = "0123456789ABCDEF"[(uint8_t)batt_value & 0x0f];
            batt_string[8]  = "0123456789ABCDEF"[db_get_value(db_battery_voltage_min_msb) >> 4];
            batt_string[9]  = "0123456789ABCDEF"[db_get_value(db_battery_voltage_min_msb) & 0x0F];
            batt_string[10] = "0123456789ABCDEF"[db_get_value(db_battery_voltage_min_lsb) >> 4];
            batt_string[11] = "0123456789ABCDEF"[db_get_value(db_battery_voltage_min_lsb) & 0x0F];

            //schedule_message(0, task_battery, msg_continue, battery_process_measurment);
            break;
        case battery_process_measurment:
        {
            battery_state const new_state = next_state(current_battery_state, status);

            if (current_battery_state != new_state)
            {
                // exit current state
                switch (current_battery_state)
                {
                case battery_good:
                    break;
                case battery_failed_one_time:
                    break;
                case battery_has_failed:
                    //schedule_message(0, task_state, msg_exit_low_battery, 0);
                    break;
                case battery_fatal:
                    //schedule_message(0, task_state, msg_exit_fatal_battery, 0);
                    break;
                case battery_not_present:
                    //schedule_message(0, task_state, msg_exit_no_battery, 0);
                    break;
                }

                // enter new state
                switch (new_state)
                {
                case battery_good:
                    wm_batt_status = WM_BAT_STATUS_HIGH;
                    break;
                case battery_failed_one_time:
                    break;
                case battery_has_failed:
                    wm_batt_status = WM_BAT_STATUS_LOW;
                    //schedule_message(0, task_state, msg_enter_low_battery, 0);
                    //schedule_message(0, task_events, msg_record_history, event_battery_low);
                    break;
                case battery_fatal:
                    wm_batt_status = WM_BAT_STATUS_LOW;
                    //schedule_message(0, task_state, msg_enter_fatal_battery, 0);
                    //schedule_message(0, task_events, msg_record_history, event_battery_low_fatal);
                    break;
                case battery_not_present:
                    wm_batt_status = WM_BAT_STATUS_LOW;
                    //schedule_message(0, task_state, msg_enter_no_battery, 0);
                    //schedule_message(0, task_events, msg_record_history, event_battery_none);
                    break;
                }

                current_battery_state = new_state;
            }

            // Disable the VBoost for the battery
            vboost_battery_off();

            // schedule the next battery test
            schedule_next_battery_test();

            // Support for serial output of Battery Values
            //schedule_message(0, task_key_value_store, msg_set_value,
                             ((uint16_t)wm_batt_status << 8) |
                                 key_wm_battery_status); // save value for wireless module output

            // send battery voltage and low battery threshold over UART
            // after each battery test
            serial_send_string_debug(batt_string, serial_enable_battery);
            break;
        }
        default:
            break;
        }
        break;
    case msg_value:
    {
        // sort value and task_id from key_value_store subscriptions
        uint8_t const value = (uint8_t)(data >> 8);
        key_name const key  = (key_name)(uint8_t)(data);
        switch (key)
        {
        case key_AFE_fault:
            in_AFE_fault = value;
            break;
        default:
            break;
        }
    }
    default:
        break;
    }
}
