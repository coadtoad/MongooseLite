//******************************************************************
/*! \file in_photo_single.c
 *
 * \brief Kidde: Mongoose-fw - Implement the single photo input functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_events.h"
#include "app_serial.h"
#include "app_supervisor.h"
#include "drv_photo.h"
#include "drv_serial.h"
#include "drv_timer.h"
#include "drv_vboost.h"
#include "in_photo.h"
#include "in_photo_single_decision.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_single.h"
#include "ptt.h"
#include "task_list.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define NORMAL_IR_F_MASK     (0x01 << IR_F)

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define NORMAL_IR_B_MASK     (0x01 << IR_B)

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define NORMAL_MODE_DELAY    (CONFIG_NORMAL_MODE_PERIOD * 1000u) // normal mode delay in ms

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define ANALYSIS_MODE_DELAY  1000

//*********************************************************************
/**
 * \brief ///< Mask for msg_continue to indicate that the VBST is enabled for photo
 */
//*********************************************************************
#define VBOOST_ENABLE_MASK   0x2000

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define FILTER_CHAIN_DEPTH   13

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define PHOTO_AMP_S2_SAT_CNT 0xF5

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
#define MAX_CLEAN_AIR_CHANGE 30

//*********************************************************************
/**
 * \brief size of median buffer
 */
//*********************************************************************
#define MEDIAN_SIZE          5

//*********************************************************************
/**
 * \brief global to hold the IR_F median filter data,
 */
//*********************************************************************
uint8_t filter_ir_f_median[MEDIAN_SIZE];

//*********************************************************************
/**
 * \brief global to hold the IR_B median filter data,
 */
//*********************************************************************
uint8_t filter_ir_b_median[MEDIAN_SIZE];

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
// photo fault globals
bool in_bck_ambient_fault = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
bool in_bck_IR_fault      = false;

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static bool in_AFE_fault  = false;

/******************************************************************************
 * Function : update_median_buffer()
 *
 * \b Description:
 *
 * place new value into the median buffer
 *
 *****************************************************************************/
void update_median_buffer(uint8_t const ir_f_photo_value, uint8_t const ir_b_photo_value)
{
    static uint8_t filter_location = 0;

    ///< @todo place this init with value in CAV on startup, not here
    static bool first_sample       = true;
    if (first_sample)
    {
        first_sample = false;
        for (uint8_t i = 0; i < MEDIAN_SIZE; i++)
        {
            filter_ir_f_median[i] = ir_f_photo_value;
            filter_ir_b_median[i] = ir_b_photo_value;
        }
    }

    filter_ir_f_median[filter_location] = ir_f_photo_value;
    filter_ir_b_median[filter_location] = ir_b_photo_value;

    filter_location++;
    // rollover
    if (filter_location >= MEDIAN_SIZE)
    {
        filter_location = 0;
    }
}

/******************************************************************************
 * Function : get_median()
 *
 * \b Description:
 *
 * Get median from the median buffer.
 *
 * done by creating temporary buffer, sorting it and taking center point
 *
 *****************************************************************************/
uint8_t get_median(uint8_t J[], uint8_t n)
{
    uint8_t A[n];
    for (uint8_t t = 0; t < n; t++)
    {
        A[t] = J[t];
    }

    for (uint16_t i = 0; i < n; i++)
    {
        /*storing current element whose left side is checked for its
                 correct position .*/

        uint8_t temp = A[i];
        uint8_t j    = i;

        /* check whether the adjacent element in left side is greater or
             less than the current element. */

        while (j > 0 && temp < A[j - 1])
        {

            // moving the left side element to one position forward.
            A[j] = A[j - 1];
            j    = j - 1;
        }
        // moving current element to its  correct position.
        A[j] = temp;
    }

    return A[n / 2]; // should be 2 (element 3)
}

//*********************************************************************
/**
 * \brief This function either sends msg_enter_smoke_alarm or msg_exit_smoke_alarm to state
 * \note If the boolean alarm_state is true send enter smoke, if it is false send exit_smoke
 * \note Send data hushable which indicates to state if the smoke is hushable (from is_hushable)
 *
 * PRE-CONDITION:
 *
 * POST-CONDITION: The returned value is true if there should be a fail safe flag
 *
 * @see reset_program.asm
 */
//*********************************************************************
void send_enter_exit_alarm(photo_mode_e const photo_mode, bool const alarm_state, bool hushable)
{
    static bool alarm_state_shadow    = false;
    static bool alarm_hushable_shadow = false;
    if (alarm_state != alarm_state_shadow || hushable != alarm_hushable_shadow)
    {
        if (alarm_state)
        {
            if (!alarm_state_shadow || hushable != alarm_hushable_shadow)
            {
                // Entering a new fire event or update hushable
                //schedule_message(0, task_state, msg_enter_smoke_alarm, hushable);
                if (photo_mode == PTT_MODE)
                {
                //schedule_message(0, task_ptt, msg_ptt, ptt_photo_pass);
                }
            }
        }
        else
        {
            //schedule_message(0, task_state, msg_exit_smoke_alarm, 0);
        }

        alarm_state_shadow    = alarm_state;
        alarm_hushable_shadow = hushable;
    }
}

//*********************************************************************
/**
 * \brief ///< Morgan needs to do this
 */
//*********************************************************************
static uint16_t get_mode_delay(photo_mode_e photo_mode)
{
    if (photo_mode == NORMAL_MODE)
    {
        uint16_t normal_mode_delay = db_get_value(db_normal_mode_period);
        /*make sure the delay is in between min and max limit*/
        if (normal_mode_delay >= NORMAL_MODE_DELAY_MIN && normal_mode_delay <= NORMAL_MODE_DELAY_MAX)
        {
            // return the value in ms
            return normal_mode_delay * 1000;
        }
        else
        {
            // if value is not within max and min limit, return default normal mode delay
            return NORMAL_MODE_DELAY;
        }
    }
    else
    {
        return ANALYSIS_MODE_DELAY;
    }
}

//*********************************************************************
/**
 * \brief enter_analysis_mode_helper()
 *          Function is used in enter_analysis_mode() and exit_analysis_mode()
 *          to determine if device should enter or exit analysis mode.
 *  \note function needs an error check to make suure it is always checking at least one sample
 *
 * \param ir_f_reading input delta reading for IR-F channel
 * \param ir_b_reading input delta reading for IR-B channel
 * \param deadband input deadband size in order to exit analysis mode
 *                  should always be 0 for enter analysis mode
 *
 * \return true if deltas are above values in databbase - deadband
 */
//*********************************************************************
bool enter_analysis_mode_helper(uint8_t const ir_f_reading, uint8_t const ir_b_reading, int const deadband)
{
    uint8_t sensors_to_check = db_get_value(db_photo_normal_sensors);

    // error check
    if (!(sensors_to_check & NORMAL_IR_F_MASK) && !(sensors_to_check & NORMAL_IR_B_MASK))
    {
        // no sensors to check, enable all sensors
        sensors_to_check = (NORMAL_IR_F_MASK | NORMAL_IR_B_MASK);
    }

    if (sensors_to_check & NORMAL_IR_F_MASK)
    {
        if (ir_f_reading >= db_get_value(db_photo_ir_f_delta) - deadband)
        {
            return true;
        }
    }
    if (sensors_to_check & NORMAL_IR_B_MASK)
    {
        if (ir_b_reading >= db_get_value(db_photo_ir_b_delta) - deadband)
        {
            return true;
        }
    }
    return false;
}

//*********************************************************************
/**
 * \brief enter_analysis_mode()
 *          Function is used to determine if device should enter analysis mode
 * \note    deadband should always be 0 for enter analysis mode)
 *
 * \param ir_f_reading input delta reading for IR-F channel
 * \param ir_b_reading input delta reading for IR-B channel
 * \param deadband input deadband size in order to exit analysis mode
 *                  should always be 0 for enter analysis mode
 *
 * \return true if deltas are above values in databbase
 */
//*********************************************************************
bool enter_analysis_mode(uint8_t const ir_f_reading, uint8_t const ir_b_reading)
{
    return enter_analysis_mode_helper(ir_f_reading, ir_b_reading, 0);
}

//*********************************************************************
/**
 * \brief exit_analysis_mode()
 *          Function is used to determine if device should exit analysis mode
 * \note    enter_analysis_mode_helper negated means less than both values are
 *          greater than inputs - deadband
 *
 * \param ir_f_reading input delta reading for IR-F channel
 * \param ir_b_reading input delta reading for IR-B channel
 * \param deadband input deadband size in order to exit analysis mode
 *                  should always be 0 for enter analysis mode
 *
 * \return true if deltas are below values in databbase - deadband
 */
//*********************************************************************
bool exit_analysis_mode(uint8_t const ir_f_reading, uint8_t const ir_b_reading)
{
    return !enter_analysis_mode_helper(ir_f_reading, ir_b_reading, 1);
}

//*********************************************************************
/**
 * \brief calc_smoke_percent()
 *          function calculates smoke percent for wireless
 *
 * \param ir_f_delta input delta reading for IR-F channel
 *
 */
//*********************************************************************
void calc_smoke_percent(uint8_t ir_f_delta)
{
    uint8_t threshold = db_get_value(db_photo_alarm_eqa_threshold);
    uint8_t smoke_percent_fs;
    uint8_t ir_f_clean_air_count = db_get_value(db_ir_f_clean_air);
    if (threshold > ir_f_clean_air_count)
    {
        // Calculate the key value for the WM smoke percent of full scale.
        smoke_percent_fs = (uint8_t)(((uint16_t)ir_f_delta * 100) / ((uint16_t)threshold - ir_f_clean_air_count));
    }
    else
    {
        smoke_percent_fs = 0;
    }

    // To find the percentage of compensation, find the delta between the
    // factory CAV and the current compensated CAV.  Then divide by the
    // factory CAV to get a percentage.
    int16_t factory_cav = (int16_t)db_get_value(db_ir_f_factory_clean_air);
    int16_t comp_delta  = (int16_t)ir_f_clean_air_count - factory_cav;

    // Convert to percentage.
    comp_delta          = (uint8_t)((comp_delta * 100) / factory_cav);

    // Set the key values for both percent and comp.
    //schedule_message(0, task_key_value_store, msg_set_value,
                     //((uint16_t)smoke_percent_fs << 8) | key_smoke_alarm_percent);
    //schedule_message(0, task_key_value_store, msg_set_value, ((uint16_t)comp_delta << 8) | key_smoke_comp);
}

//*********************************************************************
/**
 * \brief This function performs smoke measurements in normal (standby) mode.
 * \note Function takes LED forward measurements with IR LED off (dark measurement) and on.
 * \note The measurement is compared to previous measurement and if delta is bid then analysis
 *          mode is entered.
 * \note If delta is very big alarm message is sent immediately.
 *
 * PRE-CONDITION: All filter chains are initialized
 *
 * POST-CONDITION: Next run_photo_reading() function call is scheduled in normal mode or
 *                  analysis mode time
 */
//*********************************************************************
void run_photo_reading(photo_mode_e photo_mode)
{
    static uint16_t filter_index    = 0;

    static bool first_analysis_mode = true;

    supervisor_counter_increase(counter_photo);

    const uint8_t irf_current_setting =
        photo_mode == PTT_MODE ? db_get_value(db_ptt_current_ir_f) : db_get_value(db_current_ir_f);

    const uint8_t irb_current_setting =
        photo_mode == PTT_MODE ? db_get_value(db_ptt_current_ir_b) : db_get_value(db_current_ir_b);

    /** @todo Should photo reads be in a critical section? */
    uint8_t photo_reading_arr[num_photo_data];
    if (!read_photo(irf_current_setting, irb_current_setting, photo_reading_arr))
    {
        serial_send_string_debug("photo read failure\r", serial_enable_edwards_mode);
        //schedule_message(get_mode_delay(photo_mode), task_photo, msg_continue, IN_NORMAL_MODE_MASK);
        return;
    }

    // DARK COMPENSATION
    uint8_t const light_ir_f = (photo_reading_arr[ir_f_reading] >= photo_reading_arr[dark_reading])
                                   ? photo_reading_arr[ir_f_reading] - photo_reading_arr[dark_reading]
                                   : 0;

    uint8_t const light_ir_b = (photo_reading_arr[ir_b_reading] >= photo_reading_arr[dark_reading])
                                   ? photo_reading_arr[ir_b_reading] - photo_reading_arr[dark_reading]
                                   : 0;

    update_ir_photo_fault_kvs();

    //------------------------------------------  filters ------------------------------------------

    // if the dark measurement is valid update the filters and the filter chain
    if (!dark_faulted())
    {
        if (photo_mode == NORMAL_MODE)
        {
            first_analysis_mode = true; // used to reset X0 filters upon entering analysis
        }
        if (photo_mode == !NORMAL_MODE && first_analysis_mode)
        { // if it is the first time in analysis mode, initialize all filters
            first_analysis_mode = false;
        }

        ++filter_index;

        update_median_buffer(light_ir_f, light_ir_b);
        // do twice in normal mode so median of 3
        if (photo_mode == NORMAL_MODE)
        {
            update_median_buffer(light_ir_f, light_ir_b);
        }
    }

    uint8_t ir_f_median                = get_median(filter_ir_f_median, 5);
    uint8_t ir_b_median                = get_median(filter_ir_b_median, 5);

    //------------------------------------------  Calculate deltas ------------------------------------------
    uint8_t const ir_b_clean_air_count = db_get_value(db_ir_b_clean_air);
    uint8_t const ir_b_delta           = ir_b_median > ir_b_clean_air_count ? ir_b_median - ir_b_clean_air_count : 0;
    uint8_t const ir_b_delta_raw       = light_ir_b > ir_b_clean_air_count ? light_ir_b - ir_b_clean_air_count : 0;

    uint8_t const ir_f_clean_air_count = db_get_value(db_ir_f_clean_air);
    uint8_t const ir_f_delta           = ir_f_median > ir_f_clean_air_count ? ir_f_median - ir_f_clean_air_count : 0;
    uint8_t const ir_f_delta_raw       = light_ir_f > ir_f_clean_air_count ? light_ir_f - ir_f_clean_air_count : 0;

    //------------------------------------------  Calculate ratio ------------------------------------------

    uint16_t ir_f_ir_b_ratio;
    {
        // Convert deltas of value 0 to value 1 for ratio only. Changing real deltas to value 1 will affect slope calc
        uint8_t const tmp_ir_b_delta = ir_b_delta ? ir_b_delta : 1;
        uint8_t const tmp_ir_f_delta = ir_f_delta ? ir_f_delta : 1;
        ir_f_ir_b_ratio              = tmp_ir_f_delta * 100 / tmp_ir_b_delta;
    }

    //------------------------------------------  Photo Faults ------------------------------------------
    uint8_t photo_fault_flags = ((uint8_t)in_bck_ambient_fault) |            // ambient light fault
                                ((uint8_t)in_bck_IR_fault << bck_IR_fault) | // photo faults TODO
                                ((uint8_t)in_AFE_fault << afe_fault);

    //------------------------------------------  Smoke Calculations ------------------------------------------
    ///@TODO: why is this only in normal mode - for wireless ?
    if (photo_mode == NORMAL_MODE)
    {
        calc_smoke_percent(ir_f_delta);
    }

    uint8_t fire_flags = determine_fire_modes_scp(photo_mode, photo_fault_flags, ir_f_delta);
    bool in_fire_alarm = (bool)(fire_flags & ~(1 << HUSHABLE_BIT));
    bool hushable_fire = is_bit_n_set(fire_flags, HUSHABLE_BIT);

    send_enter_exit_alarm(photo_mode, in_fire_alarm, hushable_fire);

    //------------------------------------------  Serial Output ------------------------------------------
    char current_mode_char = 'N';
    if (photo_mode != NORMAL_MODE)
    {
        if (in_fire_alarm)
        {
            current_mode_char = 'A';
        }
        else
        {
            current_mode_char = 'Z';
        }
    }

    if (serial_enabled())
    {
        uint16_t serial_flags = db_get_value16(db_serial_enable_msb, db_serial_enable_lsb);
        if (serial_flags & serial_enable_edwards_mode)
        {
            char buffer[97] =
                "                                                                                               ";
            uint8_to_hex(buffer + 0, light_ir_f);                        // 1
            uint8_to_hex(buffer + 3, light_ir_b);                        // 2
            uint8_to_hex(buffer + 6, 0);                                 // 3
            uint8_to_hex(buffer + 9, ir_f_median);                       // 4
            uint8_to_hex(buffer + 12, ir_b_median);                      // 5
            uint8_to_hex(buffer + 15, 0);                                // 6
            uint8_to_hex(buffer + 18, photo_reading_arr[ir_f_reading]);  // 7
            uint8_to_hex(buffer + 21, photo_reading_arr[ir_b_reading]);  // 8
            uint8_to_hex(buffer + 24, 0);                                // 9
            uint8_to_hex(buffer + 27, photo_reading_arr[dark_reading]);  // 10
            uint8_to_hex(buffer + 30, (get_smold_counter_value() >> 8)); // 11
            uint8_to_hex(buffer + 33, get_smold_counter_value());        // 12
            uint8_to_hex(buffer + 36, 0);                                // 13
            uint8_to_hex(buffer + 39, (get_rate_of_rise_value() >> 8));  // 14
            uint8_to_hex(buffer + 42, get_rate_of_rise_value());         // 15
            uint8_to_hex(buffer + 45, (get_acceleration_value() >> 8));  // 16
            uint8_to_hex(buffer + 48, get_acceleration_value());         // 17
            uint8_to_hex(buffer + 51, fire_flags);                       // 18
            uint8_to_hex(buffer + 54, photo_fault_flags);                // 19
            uint8_to_hex(buffer + 57, ir_f_clean_air_count);             // 20
            uint8_to_hex(buffer + 60, ir_b_clean_air_count);             // 21
            uint8_to_hex(buffer + 63, 0);                                // 22
            uint8_to_hex(buffer + 66, 0);                                // 23
            uint8_to_hex(buffer + 69, ir_f_ir_b_ratio >> 8);             // 24
            uint8_to_hex(buffer + 72, ir_f_ir_b_ratio);                  // 25
            uint8_to_hex(buffer + 75, ir_f_delta);                       // 26
            uint8_to_hex(buffer + 78, ir_f_delta_raw);                   // 27
            uint8_to_hex(buffer + 81, ir_b_delta);                       // 28
            uint8_to_hex(buffer + 84, ir_b_delta_raw);                   // 29
            uint8_to_hex(buffer + 87, 0);                                // 30
            uint8_to_hex(buffer + 90, get_buf_index());                  // 31
            buffer[94] = current_mode_char;                              // 32
            buffer[95] = '\r';
            serial_send_string(buffer);
        }
    }

    //------------------------------------------  determine if Analysis mode or normal mode
    //------------------------------------------
    if (photo_mode == NORMAL_MODE)
    {
        if (enter_analysis_mode(ir_f_delta_raw, ir_b_delta_raw) ||
            (db_get_value(db_serial_flags) & enable_force_analysis_mode))
        {
            // Enter analysis mode if the delta is large enough
            //schedule_message(ANALYSIS_MODE_DELAY, task_photo, msg_continue, 0);
            // Log event to note analysis mode entry
            //schedule_message(0, task_events, msg_record_history, event_enter_analysis);
        }
        else
        {
            // Still in normal mode if the delta isn't sufficiently large
            //schedule_message(get_mode_delay(NORMAL_MODE), task_photo, msg_continue, IN_NORMAL_MODE_MASK);
        }
    }
    else // (photo_mode == ANALYSIS_MODE)
    {
        if ((in_fire_alarm ||                                                // stay analisis if in alarm.
             photo_mode == PTT_MODE ||                                       // stay in analysis if in PTT mode
             (db_get_value(db_serial_flags) & enable_force_analysis_mode) || // or in forced mode
             !exit_analysis_mode(ir_f_delta_raw, ir_b_delta_raw)) &&         // or deltas are not low
            (!photo_fault_flags)                                             // and not in photo faultmake
        )
        {
            // Continue in analysis mode
            //schedule_message(ANALYSIS_MODE_DELAY, task_photo, msg_continue, 0);
        }
        else
        {
            // reset a bunch of data pertaining to analysis mode and go back to normal mode
            //schedule_message(get_mode_delay(NORMAL_MODE), task_photo, msg_continue, IN_NORMAL_MODE_MASK);
            // Log event to note analysis mode exit
            //schedule_message(0, task_events, msg_record_history, event_exit_analysis);
        }
    }
}

void photo_task(message_id const message, uint16_t const data)
{
    static bool ptt_enabled = false;

    switch (message)
    {
    case msg_init:
    {
        // Initialization msg_value messages should already have finished
        //schedule_message(1000, task_photo, msg_continue, IN_NORMAL_MODE_MASK);
        //schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_photo << 8) | key_ir_photo_faults);
        //schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_photo << 8) | key_AFE_fault);
    }
    break;
    case msg_continue:
    {
        bool const in_normal_mode = data & IN_NORMAL_MODE_MASK;
        bool const vboost_on      = data & VBOOST_ENABLE_MASK;

        if (get_key_value_store_flag(key_avoidance_flags, kvs_avoid_sounder))
        {
            // delay the photo sample to allow time for the circuit to stabilize
            //schedule_message(10, task_photo, msg_continue, data);
            break;
        }

        if (!vboost_on)
        {
            // Enable vboost
            vboost_photo_on();
            uint16_t const photo_mode = data | VBOOST_ENABLE_MASK;
            ///@todo Minimum of 2ms of VBST on time is needed before disabling
            //schedule_message(3, task_photo, msg_continue, photo_mode);
        }
        else
        {
            // Disable the vboost before taking a photo sample
            vboost_photo_off();
            if (ptt_enabled)
            {
                run_photo_reading(PTT_MODE);
            }
            else if (in_normal_mode)
            {
                run_photo_reading(NORMAL_MODE);
            }
            else
            {
                run_photo_reading(ANALYSIS_MODE);
            }
        }
        break;
    }
    case msg_value:
    {
        // sort value and task_id from key_value_store subscriptions
        uint8_t const value = (uint8_t)(data >> 8);
        key_name const key  = (key_name)(uint8_t)(data);
        switch (key)
        {
        case key_ir_photo_faults:
            in_bck_ambient_fault = value & ((uint8_t)1 << kvs_bck_ambient_fault);
            in_bck_IR_fault      = value & ((uint8_t)1 << kvs_bck_ir_led_fault);
            break;
        case key_AFE_fault:
            in_AFE_fault = value;
            break;
        default:
            break;
        }
        break;
    }
    case msg_ptt:
    {
        ptt_message ptt_state_msg = data;

        switch (ptt_state_msg)
        {
        case ptt_photo_init:
            ptt_enabled = true;
            break;
        case ptt_photo_complete:
            ptt_enabled = false;
            break;
        default:
            break;
        }
    }
    default:
        break;
    }
}
