//******************************************************************
/*! \file no_in_ac.c
 *
 * \brief Kidde: Mongoose-fw - Implement ac input functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_ac.h"

void ac_input_task(message_id const message, uint16_t const data)
{
    (void)message;
    (void)data;
}
