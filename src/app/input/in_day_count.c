//******************************************************************
/*! \file in_day_count.c
 *
 * \brief Kidde: Mongoose-fw - Implement day count input functions
 *
 * \author J Henry
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_day_count.h"
#include "app_database.h"
#include "app_serial.h"
#include "key_value_store.h"
#include "message.h"
#include "task_list.h"
#include <stdint.h>

//*********************************************************************
/**
 * \brief End of life fatal threshold
 */
//*********************************************************************
#define EOL_FATAL_TH (EOL_TH + 7)

//*********************************************************************
/**
 * \brief End of life state for device enum
 */
//*********************************************************************
typedef enum
{
    not_eol,
    eol,
    eol_fatal
} eol_state;

//*********************************************************************
/**
 * \brief End of life state value
 */
//*********************************************************************
static eol_state state;

//*********************************************************************
/**
 * \brief accelerated time states
 */
//*********************************************************************
typedef enum
{
    one_day_is_one_day,
    one_min_is_one_day,
    one_hour_is_one_day,
} accel_state;

//*********************************************************************
/**
 * \brief Period value set foe accelerated interval
 */
//*********************************************************************
static accel_state period;

//*********************************************************************
/**
 * \brief Check if device has met the EOL or fatal threshold
 */
//*********************************************************************
eol_state check_eol_status(void)
{
    const uint16_t day_count = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);

    if (day_count >= EOL_FATAL_TH)
    {
        return eol_fatal;
    }
    else if (day_count >= EOL_TH)
    {
        return eol;
    }
    else
    {
        return not_eol;
    }
}

void day_count_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        period = one_day_is_one_day;

        // check if currently EOL
        //schedule_message(100, task_day_count, msg_continue, 0);
        // start the timer
        //schedule_message(ONE_DAY, task_day_count, msg_ready, 0);
        break;
    case msg_set_flag:
        if (data == accel_day_timer)
        {
            period = one_day_is_one_day;
        }
        else if (data == accel_hour_timer)
        {
            period = one_hour_is_one_day;
        }
        else if (data == accel_minute_timer)
        {
            period = one_min_is_one_day;
        }

        // remove messages off the stack so they dont stack up
        //un//schedule_messages_tm(task_day_count, msg_ready);

        // check current EOL status.
        ////schedule_message(100, task_day_count, msg_continue, 0);
        // restart minute counter
        ////schedule_message(ONE_MINUTE, task_day_count, msg_ready, 0);

        break;
    case msg_continue:
    {
        eol_state new_state = check_eol_status();
        if (state != new_state)
        {
            state = new_state;
            if (state == eol)
            {
                ////schedule_message(0, task_state, msg_enter_eol, 0);
                ////schedule_message(0, task_state, msg_exit_eol_fatal, 0);
            }
            else if (state == eol_fatal)
            {
                ////schedule_message(0, task_state, msg_exit_eol, 0);
                ////schedule_message(0, task_state, msg_enter_eol_fatal, 0);
            }
            else
            {
                ////schedule_message(0, task_state, msg_exit_eol, 0);
                ////schedule_message(0, task_state, msg_exit_eol_fatal, 0);
            }
        }
        break;
    }
    case msg_ready:
    {
        // Increase day count value and update database.
        uint16_t day_count = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);
        ++day_count;
        db_set_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb, day_count);
        // Update key value store
        //schedule_message(0, task_key_value_store, msg_set_value, (uint8_t)(day_count >> 8) | key_day_count_msb);
        //schedule_message(0, task_key_value_store, msg_set_value, (uint8_t)day_count | key_day_count_lsb);
        // Update databases
        //schedule_message(0, task_database, msg_update_primary_db, 0);
        // Update Backup Database (once a day)
        //schedule_message(1000, task_database, msg_update_backup_db, 0);

        // check new EOL values
        //schedule_message(100, task_day_count, msg_continue, 0);

        // schedule future day count update
        switch (period)
        {
        case one_min_is_one_day:
            //schedule_message(ONE_MINUTE, task_day_count, msg_ready, 0);
            break;
        case one_hour_is_one_day:
            //schedule_message(ONE_HOUR, task_day_count, msg_ready, 0);
            break;
        case one_day_is_one_day:
        default:
            //schedule_message(ONE_DAY, task_day_count, msg_ready, 0);
            break;
        }

        break;
    }
    default:
        break;
    }
}
