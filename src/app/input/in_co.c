//******************************************************************
/*! \file in_co.c
 *
 * \brief Kidde: Mongoose-fw - Implement CO input functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_co.h"
#include "app_database.h"
#include "app_events.h"
#include "app_serial.h"
#include "co_common.h"
#include "drv_co.h"
#include "drv_serial.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include "task_list.h"
#include <stdbool.h>
#include <stdlib.h>

//*********************************************************************
/**
 * \brief In Milliseconds conversion
 */
//*********************************************************************
#define IN_MSEC                    *1000

//*********************************************************************
/**
 * \brief This is the number of samples that will be averaged during CO measurement
 * \note This is averaging across consecutive CO measurement intervals and not referring to the number of samples
 * averaged during a single series of ADC measurements when sampling the output of the CO circuit.
 */
//*********************************************************************
#define NUM_SAMPLES_CO_AVG         (uint16_t)4

//*********************************************************************
/**
 * \brief Measures AD charge threshold value to determine if the device is charging or sampling a high value
 */
//*********************************************************************
#define VMEAS_AD_CHARGE_THRESHOLD  (uint16_t)100

//*********************************************************************
/**
 * \brief Measure if the stabalization needs to continue stabalizing or end
 * \note 400 counts (Output is Off the Rail)
 */
//*********************************************************************
#define VMEAS_CIRCUIT_STABILIZE    (uint16_t)400

//*********************************************************************
/**
 * \brief Time interval for CO tasks to occur within including sampling
 * \note  Measurement task is called every 5 seconds. All timer functions should be based on this interval in case it is
 * changed in the future.
 */
//*********************************************************************
#define CO_TASK_EXECUTION_INTERVAL (uint32_t)5

//*********************************************************************
/**
 * \brief Time interval between CO accumulator updates and CO alarm decisions
 */
//*********************************************************************
#define CO_ACCUMULATOR_TIME        (uint32_t)30

//*********************************************************************
/**
 * \brief Time for a CO test compared to the 90 second interval
 */
//*********************************************************************
#define CO_TEST_RETURN_TIME        (uint32_t)38

//*********************************************************************
/**
 * \brief The low threshold for the CO health test measurement delta and is used to detect an open CO sensor fault.
 */
//*********************************************************************
#define VHIGH_TEST_DELTA_MIN       (uint16_t)50

//*********************************************************************
/**
 * \brief The high threshold for the CO health test measurement delta and is used to detect an open CO sensor fault.
 */
//*********************************************************************
#define VHIGH_TEST_DELTA_MAX       (uint16_t)1024

//*********************************************************************
/**
 * \brief Used to calculate CO health test timing intervals that require a 90 second variable.
 * \note CO health tests should occur every 90 seconds if no fault is active.
 */
//*********************************************************************
#define CO_TEST_INTERVAL_90_SEC    (uint32_t)90

//*********************************************************************
/**
 * \brief Used to calculate CO health test timing intervals that require a 30 second variable.
 * \note CO health tests should occur every 30 seconds if a fault is detected.
 */
//*********************************************************************
#define CO_TEST_INTERVAL_30_SEC    (uint32_t)30

//*********************************************************************
/**
 * \brief The number of CO measurement tasks needed to occur before starting the CO health test when no CO fault is
 * active
 * \note CO measurement tasks occur every 5 seconds. This constant is calculated with respect to the 5 second
 * interval.
 */
//*********************************************************************
#define CO_TEST_INTERVAL_90        ((CO_TEST_INTERVAL_90_SEC - CO_TEST_RETURN_TIME) / (CO_TASK_EXECUTION_INTERVAL))

//*********************************************************************
/**
 * \brief The number of CO measurement tasks needed to occur before starting the CO health test when a CO fault is
 * detected
 * \note CO measurement tasks occur every 5 seconds. This constant is calculated with respect to the 5 second
 * interval.
 */
//*********************************************************************
#define CO_TEST_INTERVAL_30        (CO_TEST_INTERVAL_30_SEC / CO_TASK_EXECUTION_INTERVAL)

//*********************************************************************
/**
 * \brief Maximum amount of time in seconds to stabilize the CO circuit before measurements are taken
 */
//*********************************************************************
#define MAX_STABILIZE_TIME         (uint32_t)45 / CO_TASK_EXECUTION_INTERVAL

//*********************************************************************
/**
 * \brief Minimum CO PPM level needed to skip the CO health test interval count and continue measuring CO due to
 * moderate levels of CO detected
 */
//*********************************************************************
#define MIN_CO_LEVEL               30

//*********************************************************************
/**
 * \brief The ADC measurement of the CO circuit output
 * \note The ADC is configured for 10-bit readings during CO measurements
 */
//*********************************************************************
static uint16_t co_raw_counts;

//*********************************************************************
/**
 * \brief The running average of ADC measurements taken from the CO circuit output
 */
//*********************************************************************
static uint16_t co_raw_avg_counts;

//*********************************************************************
/**
 * \brief The base line or "clean air" measurement of the CO circuit output prior to starting the CO health test
 */
//*********************************************************************
static uint16_t co_vout_baseline;

//*********************************************************************
/**
 * \brief The time interval between the next CO health test
 * \note Decremented at the end of each normal CO measurement, which occurs every 5 seconds. The value of this counter
 * is set with respect to a 5 second update period.
 */
//*********************************************************************
static uint8_t co_test_interval_counter = CO_TEST_INTERVAL_90;

//*********************************************************************
/**
 * \brief Counter variable used to timeout of the CO stabilization process during power on
 */
//*********************************************************************
static uint8_t co_stabilization_timeout;

//*********************************************************************
/**
 * \brief Boolean used to determine if the CO alarm decision needs to use the measured CO circuit output or the manually
 * set CO PPM value
 */
//*********************************************************************
static bool override_flag   = false;

//*********************************************************************
/**
 * \brief External CO fault status variable
 */
//*********************************************************************
fault_status_t fault_status = no_fault;

//*********************************************************************
/**
 * \brief CO task processes
 */
//*********************************************************************
typedef enum
{
    schedule_meas_stabilize,
    meas_stabilize,
    schedule_co_take_measurement,
    co_take_measurement,
    co_health_test_init,
    schedule_co_test_charging,
    co_test_charging,
    schedule_co_test_high_sample,
    co_test_high_sample,
    calculate_alarm,
    alarm_active
} co_task_data;

void co_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        // Initialize the AFE's CO register settings
        if (!initialize_afe_co())
        {
            ///@todo Handle failed CO intialization
        }

        if (data)
        {
            // Lower the CO accumulator if the unit was in CO alarm before soft reset
            lower_co_accumulator();
            // skip stabilize time and go straight to co_take_measurement after soft reset
            schedule_message(CO_TASK_EXECUTION_INTERVAL IN_MSEC, task_co, msg_continue, schedule_co_take_measurement);
            schedule_message(CO_ACCUMULATOR_TIME IN_MSEC, task_co, msg_continue, calculate_alarm);
        }
        else
        {
            // initialize CO stabilization state and schedule first stabilization check
            co_stabilization_timeout = 0;

            // first initialization of accumulator values
            accumulator_init();

            fault_status = stabilize;
            schedule_message(0, task_co, msg_continue, schedule_meas_stabilize);
            schedule_message(0, task_key_value_store, msg_set_value, (uint16_t)fault_status << 8 | key_co_status);
        }
        break;
    case msg_continue:
        switch ((co_task_data)data)
        {
        case schedule_meas_stabilize:
            schedule_timed_message(20, task_co, msg_continue, meas_stabilize);
            break;
        case meas_stabilize:
        {
            if (get_key_value_store_flag(key_avoidance_flags, kvs_avoid_sounder))
            {
                // delay the CO sample if the sounder is on
                schedule_message(10, task_co, msg_continue, schedule_meas_stabilize);
                break;
            }
            // Take a CO measurement
            uint16_t measurement = 0;
            if (!co_make_measurement(&measurement))
            {
                ///\todo: Handle failure
            }
            // check if the CO sensor has stabilized after power up
            // the override flag is true when the serial F command is executed
            if (VMEAS_CIRCUIT_STABILIZE > measurement || MAX_STABILIZE_TIME < co_stabilization_timeout++ ||
                override_flag)
            {
                fault_status             = no_fault;
                co_test_interval_counter = CO_TEST_INTERVAL_90;
                schedule_message(CO_TASK_EXECUTION_INTERVAL IN_MSEC, task_co, msg_continue,
                                 schedule_co_take_measurement);
                schedule_message(CO_ACCUMULATOR_TIME IN_MSEC, task_co, msg_continue, calculate_alarm);
            }
            else
            {
                // stabilization not complete, schedule next stabilization check
                schedule_message(CO_TASK_EXECUTION_INTERVAL IN_MSEC, task_co, msg_continue, schedule_meas_stabilize);
            }
        }
        break;
        case schedule_co_take_measurement:
            schedule_timed_message(20, task_co, msg_continue, co_take_measurement);
            break;
        case co_take_measurement:
        {
            if (get_key_value_store_flag(key_avoidance_flags, kvs_avoid_sounder))
            {
                // delay the CO sample if the sounder is on
                schedule_message(10, task_co, msg_continue, schedule_co_take_measurement);
                break;
            }

            // clear health test status if it was set
            fault_status &= ~health_test;

            // Take a CO measurement
            uint16_t co_raw_counts = 0;
            if (!co_make_measurement(&co_raw_counts))
            {
                ///\todo: Handle failure
            }

            // check if measurements stable (used for UART only)
            co_meas_stable(co_raw_counts);

            // calculate average
            co_raw_avg_counts = (uint16_t)((((uint32_t)co_raw_avg_counts * (NUM_SAMPLES_CO_AVG - 1)) + co_raw_counts) /
                                           NUM_SAMPLES_CO_AVG);

            // calculate if short is detected
            co_short_detect(co_raw_counts);

            // calculate PPM from raw counts and check if CO is detected
            // skip health test if CO is present
            uint16_t const ppm = co_calc_PPM(co_raw_avg_counts);
            if (ppm > MIN_CO_LEVEL)
            {
                co_test_interval_counter = 0;
            }

            // Check if peak value should be set
            co_peak_detect(ppm);

            // schedule next sample or test
            if (--co_test_interval_counter == 0)
            {
                co_vout_baseline = co_raw_counts;
                schedule_message(CO_TASK_EXECUTION_INTERVAL IN_MSEC, task_co, msg_continue, co_health_test_init);

                if (fault_status & 0x0F)
                {
                    co_test_interval_counter = CO_TEST_INTERVAL_30;
                }
                else
                {
                    co_test_interval_counter = CO_TEST_INTERVAL_90;
                }
                fault_status |= health_test;
            }
            else
            {
                schedule_message(CO_TASK_EXECUTION_INTERVAL IN_MSEC, task_co, msg_continue,
                                 schedule_co_take_measurement);
                // Update the CO health test schedule timer
                if (co_test_interval_counter > CO_TEST_INTERVAL_90)
                {
                    co_test_interval_counter = CO_TEST_INTERVAL_90;
                }
            }

            schedule_message(10, task_serial, msg_continue, ((uint16_t)SERIAL_PERIODIC_TASK_CO) << 8);
            break;
        }
        case co_health_test_init:
            co_start_test_pulse();
            schedule_message(10, task_co, msg_continue, schedule_co_test_charging);
            break;
        case schedule_co_test_charging:
            schedule_timed_message(20, task_co, msg_continue, (3 << 14) | co_test_charging);
            break;
        case co_test_charging:
        {
            if (get_key_value_store_flag(key_avoidance_flags, kvs_avoid_sounder))
            {
                // delay the CO sample if the sounder is on
                schedule_message(10, task_co, msg_continue, schedule_co_test_charging);
                break;
            }
            uint16_t const count = ((data >> 14) & 0x3) - 1;
            uint16_t adc         = 0;
            if (!co_sample(&adc))
            {
                ///\todo: Handle failure
            }
            if (!count || adc > co_vout_baseline + VMEAS_AD_CHARGE_THRESHOLD)
            {
                // Signal has moved far enough
                // or we finished waiting
                co_stop_test_pulse();
                schedule_message(2000, task_co, msg_continue, schedule_co_test_high_sample);
            }
            else
            {
                schedule_message(10, task_co, msg_continue, (count << 14) | co_test_charging);
            }
        }
        break;
        case schedule_co_test_high_sample:
            schedule_timed_message(20, task_co, msg_continue, co_test_high_sample);
            break;
        case co_test_high_sample:
            // Take a CO measurement
            if (!co_make_measurement(&co_raw_counts))
            {
                ///\todo: Handle failure
            }

            /*
             * At this test sample, the sensor output should be above the test threshold.
             * Is (Vmeasured - Vbaseline) = Vdifferential > VHIGH_TEST_DELTA_MIN ?
             * If measurement is less than baseline (occurs sometimes with sensor Open)
             * delta will be negative (> VHIGH_TEST_DELTA_MAX)
             */
            if (co_raw_counts - co_vout_baseline < VHIGH_TEST_DELTA_MIN ||
                co_raw_counts - co_vout_baseline > VHIGH_TEST_DELTA_MAX)
            {
                if (!(fault_status & fault_open))
                {
                    if (fault_status & fault_pending)
                    {
                        // CO sensor open fault active
                        schedule_message(0, task_state, msg_fault_co_sensor_open, 0);
                        fault_status |= fault_open;
                        fault_status &= ~fault_pending;
                        // Record the fault in diagnostic history
                        schedule_message(0, task_events, msg_record_history, event_fault_co_sensor_health);
                        // Update the sensor short count in the db.
                        uint16_t health_count =
                            db_get_value16(db_counter_sensor_health_msb, db_counter_sensor_health_lsb);
                        db_set_value16(db_counter_sensor_health_msb, db_counter_sensor_health_lsb, ++health_count);
                    }
                    else
                    {
                        co_test_interval_counter = CO_TEST_INTERVAL_30;
                        fault_status |= fault_pending;
                    }
                }
                // Go back to measurement state.
                schedule_message(CO_TASK_EXECUTION_INTERVAL IN_MSEC, task_co, msg_continue,
                                 schedule_co_take_measurement);
            }
            else
            {
                // CO health test passed. Clear all CO sensor faults
                if (fault_status & ANY_FAULT)
                {
                    schedule_message(0, task_state, msg_fault_co_clear, 0);
                    fault_status &= ~ANY_FAULT;
                    // Record the fault clearing in diagnostic history
                    schedule_message(0, task_events, msg_record_history, event_fault_co_cleared);
                }

                schedule_message(CO_TEST_RETURN_TIME IN_MSEC, task_co, msg_continue, schedule_co_take_measurement);
            }
            // Go back to measurement state.
            schedule_message(10, task_serial, msg_continue, ((uint16_t)SERIAL_PERIODIC_TASK_CO) << 8);
            break;
        case calculate_alarm:
            if (co_alarm())
            {
                schedule_message(0, task_state, msg_enter_co_alarm, 0);
                schedule_message(CO_ACCUMULATOR_TIME IN_MSEC, task_co, msg_continue, alarm_active);
                fault_status |= status_co_alarm;
            }
            else
            {
                schedule_message(CO_ACCUMULATOR_TIME IN_MSEC, task_co, msg_continue, calculate_alarm);
            }
            schedule_message(10, task_serial, msg_continue, ((uint16_t)SERIAL_PERIODIC_TASK_CO) << 8);
            break;
        case alarm_active:
            if (co_alarm())
            {
                schedule_message(CO_ACCUMULATOR_TIME IN_MSEC, task_co, msg_continue, alarm_active);
            }
            else
            {
                fault_status &= ~status_co_alarm;
                schedule_message(0, task_state, msg_exit_co_alarm, 0);
                schedule_message(CO_ACCUMULATOR_TIME IN_MSEC, task_co, msg_continue, calculate_alarm);
            }
            schedule_message(10, task_serial, msg_continue, ((uint16_t)SERIAL_PERIODIC_TASK_CO) << 8);
            break;
        }
        break;
    case msg_set_flag:
        set_manual_co(data);
        break;
    case msg_enter_co_alarm:
        override_flag = !override_flag;
        if (override_flag)
        {
            schedule_message(10, task_state, msg_enter_co_alarm, 0);
        }
        else
        {
            schedule_message(10, task_state, msg_exit_co_alarm, 0);
        }
        break;
    case msg_reset_flags:
        fault_status = no_fault;
        break;
        break;
    default:
        break;
    }
}
