//******************************************************************
/*! \file in_ac.c
 *
 * \brief Kidde: Mongoose-fw - Implement ac input functions
 *
 * \author Brandon W
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "in_ac.h"
#include "app_serial.h"
#include "drv_battery.h"
#include "drv_events.h"

//*********************************************************************
/**
 * \brief Boolean to check if AC is connected to different AC lines of devices
 */
//*********************************************************************
static _Bool ac_connected_memory = false;

//*********************************************************************
/**
 * \brief Check if AC input is connected to unit
 */
//*********************************************************************
void sample_ac_input(void)
{
    _Bool const is_connected = battery_is_ac_detected();

    if (is_connected != ac_connected_memory)
    // ac changed
    {
        ac_connected_memory = is_connected;

        if (is_connected)
        {
            serial_send_string_debug("AC Detected \r", serial_enable_battery);
            schedule_message(0, task_state, msg_ac_on, 0);
            schedule_message(0, task_events, msg_record_history, event_ac_power_on);
        }
        else
        {
            serial_send_string_debug("AC No Longer Detected \r", serial_enable_battery);
            schedule_message(0, task_state, msg_ac_off, 0);
            schedule_message(0, task_events, msg_record_history, event_ac_power_off);
        }
    }

    // perform next ac input check 2 seconds from now
    schedule_message(2000, task_ac_input, msg_continue, 0);
}

void ac_input_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        // Delay 2 seconds to make sure that the message scheduled from
        // state.c gets processed before any AC LED control.
        schedule_message(2000, task_ac_input, msg_continue, 0);
        break;
    case msg_continue:
        sample_ac_input();
        break;
    default:
        break;
    }
}
