//******************************************************************
/*! \file app_serial.c
 *
 * \brief Kidde: Mongoose-fw - Implement the APP SERIAL functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

// library includes
#include "stdio.h"
#include "string.h"
#include <stdbool.h>
#include <stdlib.h>

// local includes
#include "app_database.h"
#include "app_serial.h"
#include "clean_air_value.h"
#include "debug_ids_priority.h"
#include "drv_memory.h"
#include "drv_timer.h"
#include "in_co.h"
#include "in_day_count.h"
#include "in_photo.h"
#include "key_value_store.h"
#include "message.h"
#include "out_voice.h"
#include "photo_common_single.h"
#include "state.h"
#include "task_list.h"

// driver includes
#include "drv_afe_serial_commands.h"
#include "drv_database.h"
#include "drv_events.h"
#include "drv_i2c.h"
#include "drv_serial.h"
#include "hal_electronic_signature.h"
#include "hal_flash.h"

//*********************************************************************
/**
 * \brief The firmware revision to be output over serial
 */
//*********************************************************************
#define FIRMWARE_REVISION "01.00.29"

static bool serial_dump_all;

static uint8_t serial_values[48] = {0};
static uint16_t serial_flags     = 0;
static uint8_t serial_mode       = MODE_NORMAL;

//*********************************************************************
/**
 * \brief output device model information
 */
//*********************************************************************
void print_device_model(void)
{
    serial_send_string("\r-----------------------------------\r"
                       "Device Model: " DEVICE_MODEL "\r");
    char buffer[26];
    buffer[24] = '\r';
    buffer[25] = '\0';
    uid_to_hex(buffer);
    serial_send_string("MCU UID: ");
    serial_send_string(buffer);
    serial_send_string("Revision: " FIRMWARE_REVISION "\r");
    extern char const *const git_commit;
    serial_send_string("Git Commit: ");
    serial_send_string(git_commit);
    serial_send_string("\r");
}

void serial_send_string_debug(char *pString, Serial_output_flags_type_e flag)
{
    if ((serial_flags & flag) || (flag == serial_enable_always))
    {
        serial_send_string(pString);
    }
}

bool isZthBitSet(uint16_t c, int z)
{
    // bit count from right to left.  bit 0 = 1
    return (c & (1 << z)) != 0;
}

//*********************************************************************
/**
 * \brief Send prompt character out the serial port (after the carriage return character)
 */
//*********************************************************************
void serial_send_prompt(void)
{
    serial_send_string(">");
}

//*********************************************************************
/**
 * \brief Send prompt character out the serial port (after the carriage return character)
 */
//*********************************************************************
void serial_send_cr(void)
{
    serial_send_string("\r");
}

//*********************************************************************
/**
 * \brief Send prompt character out the serial port (after the carriage return character)
 */
//*********************************************************************
void serial_send_cr_prompt(void)
{
    serial_send_string("\r>");
}

//*********************************************************************
/**
 * \brief Send prompt character out the serial port (after the carriage return character)
 */
//*********************************************************************
void send_AFE_CR_prompt(void)
{
    serial_send_string("\r$");
}

//*********************************************************************
/**
 * \brief Send prompt character out the serial port (after the carriage return character)
 */
//*********************************************************************
void send_AFE_prompt(void)
{
    serial_send_string("$");
}

//*********************************************************************
/**
 * \brief Sends a number in HEX format to the serial port
 * \param dest Location for number to be sent
 * \param data The number that will be converted to HEX
 * \note Parameters:     u8 number to be sent
 */
//*********************************************************************
char const hex_digit[] = "0123456789ABCDEF";
void uint8_to_hex(char *dest, uint8_t const data)
{
    dest[0] = hex_digit[(data >> 4) & 0x0f];
    dest[1] = hex_digit[(data >> 0) & 0x0f];
}
void uint16_to_hex(char *dest, uint16_t const data)
{
    dest[0] = hex_digit[(data >> 12) & 0x0f];
    dest[1] = hex_digit[(data >> 8) & 0x0f];
    dest[2] = hex_digit[(data >> 4) & 0x0f];
    dest[3] = hex_digit[(data >> 0) & 0x0f];
}
void uint32_to_hex(char *dest, uint32_t const data)
{
    dest[0] = hex_digit[(data >> 28) & 0x0f];
    dest[1] = hex_digit[(data >> 24) & 0x0f];
    dest[2] = hex_digit[(data >> 20) & 0x0f];
    dest[3] = hex_digit[(data >> 16) & 0x0f];
    dest[4] = hex_digit[(data >> 12) & 0x0f];
    dest[5] = hex_digit[(data >> 8) & 0x0f];
    dest[6] = hex_digit[(data >> 4) & 0x0f];
    dest[7] = hex_digit[(data >> 0) & 0x0f];
}

/*
+------------------------------------------------------------------------------
| Purpose:        Support routine for formatting serial output.
+------------------------------------------------------------------------------
|  Parameters:
|
|   prefix:  String to send before sending the value, or NULL if no
|               prefix is desired.
|   val:     value to be sent
|   base:    10 for output in decimal, 16 for output in hex
|   prompt:  If non-zero, a prompt will be sent after the integer
|
|   Typical output:  S1:123<CR>
+------------------------------------------------------------------------------
| Return Value:   None
+------------------------------------------------------------------------------
*/
void serial_send_int_ex(char *pPrefix, uint16_t val, uint8_t base, char *suffix)
{
    char buff[6]; // Max length with val=65535 -> 5 characters plus End of String

    serial_send_string(pPrefix); // Send prefix. Does nothing if string is NULL

    serial_uart_itoa(val, buff, base); // Convert number to null terminated string
    serial_send_string(buff);          // Send the string

    // Send the prompt if parameter is non-zero.
    serial_send_string(suffix);
}

void serial_send_int_ex_debug(char *pPrefix, uint16_t val, uint8_t base, char *suffix, Serial_output_flags_type_e flag)
{
    if ((serial_flags & flag) || (flag == serial_enable_always))
    {
        serial_send_int_ex(pPrefix, val, base, suffix);
    }
}

/*
+------------------------------------------------------------------------------
| Purpose:        Support routine for converting ints to ascii
+------------------------------------------------------------------------------
| Parameters:     u16 for conversion, pointer to destination, base
+------------------------------------------------------------------------------
| Return Value:   None
+------------------------------------------------------------------------------
*/
void serial_uart_itoa(uint16_t value, char *pResult, uint8_t base)
{
    uint16_t absQModB;
    char *pStart = pResult; // Save pointer to start of string

    do
    { // Start getting digits, starting from least significant
        absQModB   = value % base;
        *pResult++ = "0123456789ABCDEF"[absQModB];
        value /= base;
    } while (value);

    *pResult = 0; // End of string character

    // The string is reversed. Now put the characters in the correct order
    pResult--;
    while (pStart < pResult)
    {
        char temp;
        temp     = *pStart; // Swap first and last characters
        *pStart  = *pResult;
        *pResult = temp;
        pStart++; // Move pointers towards the middle
        pResult--;
    }
}

/*
+------------------------------------------------------------------------------
| Purpose:        Support routine for ASCII to integer
+------------------------------------------------------------------------------
| Parameters:     pointer to source string
+------------------------------------------------------------------------------
| Return Value:   integer
+------------------------------------------------------------------------------
*/
uint8_t serial_uart_atou8(uint8_t const *pString)
{
    uint8_t value = 0;
    uint8_t len   = 2;
    do
    {
        value <<= 4;
        if ('A' <= *pString) // 'A' thru 'F' range
        {
            value += (char)((*pString++) + 0xC9); // (char)('A'+0xC9) = 10
        }
        else // '0' thru '9' range
        {
            value += (char)((*pString++) + 0xD0); // (char)('0'+0xD0) = 0
        }
    } while (--len);
    return value;
}

static void serial_update_values(uint16_t data)
{
    // update local value array with values returned from
    // key_value_store callbacks
    serial_values[(uint8_t)(data)] = (uint8_t)(data >> 8);
}

static void serial_format_task_buffer_co(void)
{
    if (serial_flags & serial_enable_co)
    {
        char buffer[23] = {0};
        uint8_to_hex(buffer + 0, serial_values[key_co_volt_msb]);
        buffer[2] = ' ';
        uint8_to_hex(buffer + 3, serial_values[key_co_volt_lsb]);
        buffer[5] = ' ';
        uint8_to_hex(buffer + 6, serial_values[key_co_msb]);
        buffer[8] = ' ';
        uint8_to_hex(buffer + 9, serial_values[key_co_lsb]);
        buffer[11] = ' ';
        uint8_to_hex(buffer + 12, serial_values[key_accumulator_msb]);
        buffer[14] = ' ';
        uint8_to_hex(buffer + 15, serial_values[key_accumulator_lsb]);
        buffer[17] = ' ';
        uint8_to_hex(buffer + 18, serial_values[key_co_status]);
        buffer[20] = '\r';
        serial_send_string(buffer);
    }
}

///< Daniel Dilts needs to do this
static void serial_command_processor(void)
{
    database_index index;
    uint8_t charval;
    uint16_t val;
    event_entry event;

    uint8_t *rx_buffer = serial_get_rx_buffer();
    if (serial_mode == MODE_NORMAL)
    {
        switch (rx_buffer[0])
        {
        case '\0':
            // A return only was sent.  Reply with a prompt.
            break;
        case 'a':
            // Change to AFE test mode.
            serial_mode = MODE_AFE;
            serial_send_string_debug("Serial - AFE mode", serial_enable_echo);
            send_AFE_CR_prompt(); // special case with return
            return;
        case 'A':
        {
            serial_send_string_debug("Serial CMD - ", serial_enable_echo);

            rx_buffer++;

            uint8_t db_serial_flags_value = db_get_value(db_serial_flags);
            if (*rx_buffer == '\0') // null character command "B" entered
            {
                char buffer[3] = {0};
                uint8_to_hex(buffer, db_serial_flags_value);
                serial_send_string_debug(buffer, serial_enable_echo);
                break; // error dont save to database
            }
            else if (*rx_buffer == '1')
            {
                db_serial_flags_value ^= enable_force_analysis_mode;
                if (db_serial_flags_value & enable_force_analysis_mode)
                {
                    serial_send_string_debug("Force Analysis", serial_enable_echo);
                }
                else
                {
                    serial_send_string_debug("disable Force Analysis", serial_enable_echo);
                }
            }
            else if (*rx_buffer == '2')
            {
                db_serial_flags_value ^= disable_dark_compensation;
                if (db_serial_flags_value & disable_dark_compensation)
                {
                    serial_send_string_debug("disable dark comp", serial_enable_echo);
                }
                else
                {
                    serial_send_string_debug("enable dark comp", serial_enable_echo);
                }
            }
            else if (*rx_buffer == '3')
            {
                db_serial_flags_value ^= disable_dark_read;
                if (db_serial_flags_value & disable_dark_read)
                {
                    serial_send_string_debug("disable dark read", serial_enable_echo);
                }
                else
                {
                    serial_send_string_debug("enable dark read", serial_enable_echo);
                }
            }
            /*
            else if (*rx_buffer == '4')
            {
                db_serial_flags_value ^= disable_sounder;
                if (db_serial_flags_value & disable_sounder)
                {
                    serial_send_string_debug("Sounder disabled", serial_enable_echo);
                }
                else
                {
                    serial_send_string_debug("Sounder enabled", serial_enable_echo);
                }
            }*/
            else
            {
                serial_send_string_debug(INVALID_CMD, serial_enable_echo);
                break;
            }

            db_set_value(db_serial_flags, db_serial_flags_value);
            ////schedule_message(0, task_database, msg_update_primary_db, 0);
        }
        break;
        case 'b':
            ////schedule_message(0, task_battery, msg_continue, 0);
            break;
        case 'B':
        {
            serial_send_string_debug("Serial Output - ", serial_enable_echo);

            rx_buffer++;
            // "B"
            if (*rx_buffer == '\0') // null character command "B" entered
            {
                char buffer[5] = {0};
                uint16_to_hex(buffer, db_get_value16(db_serial_enable_msb, db_serial_enable_lsb));
                serial_send_string_debug(buffer, serial_enable_echo);
                break; // error dont save to database
            }
            else if (*rx_buffer == 'A')
            {
                serial_flags = 0x1FFF;
                serial_send_string_debug("All Serial ON - ", serial_enable_echo);
            }
            else if (*rx_buffer == 'Z')
            {
                serial_send_string_debug("All Serial OFF - ", serial_enable_echo);
                serial_flags = 0x0000;
            }
            else if (*rx_buffer == 'E')
            {
                serial_send_string_debug("Edwards Mode - ", serial_enable_echo);
                serial_flags = serial_enable_edwards_mode;
            }
            else if (*rx_buffer == 'C')
            {
                serial_send_string_debug("CO Mode - ", serial_enable_echo);
                serial_flags = serial_enable_co;
            }
            else if (*rx_buffer == 'I')
            {
                serial_send_string_debug("I2C - ", serial_enable_echo);
                serial_flags = serial_enable_i2c;
            }
            else if (*rx_buffer == 'H') // command "Bx(12)" entered, set flag values to (0x12)
            {
                serial_send_string_debug("Heap - ", serial_enable_echo);
                serial_flags = serial_enable_heap; // 0x8000
            }
            else if (*rx_buffer == 'W') // Wireless
            {
                serial_send_string_debug("Wireless - ", serial_enable_echo);
                serial_flags = serial_enable_wireless; // 0x0200
            }
            // "Bx()"
            else if (*rx_buffer == 'x') // command "Bx(12)" entered, set flag values to (0x12)
            {
                /** \todo IMPLEMENT ME */
                // This was commented out due to use of strtok and strtol
                //                    rx_buffer++;

                //                    const char delim[] = ",\r";
                //                    static const char *value;
                //                    uint16_t buffer;
                //                    value = strtok((char *)(rx_buffer), delim ) ;
                //                    buffer = strtol(value,NULL, 16);

                //                    //void serial_send_int_ex_debug(char *pPrefix, uint16_t val, uint8_t base, char
                //                    *suffix, Serial_output_flags_type_e flag); serial_send_string_debug("Bx - value -
                //                    ", serial_enable_echo);

                //                    serial_flags = buffer;
            }
            // "B1"
            else // if Command "B0" to "B15" entered.
            {
                uint8_t new_flag = serial_uart_atou8(rx_buffer);
                if (new_flag <= 15)
                {
                    serial_flags ^= 1UL << new_flag;

                    serial_send_string_debug("serial output - ", serial_enable_echo);
                    serial_send_msg_serial_flags_debug(new_flag, serial_enable_echo);

                    if (isZthBitSet(serial_flags, new_flag))
                    {
                        serial_send_string_debug(" - Toggle On", serial_enable_echo);
                    }
                    else
                    {
                        serial_send_string_debug(" - Toggle Off", serial_enable_echo);
                    }
                    serial_send_string_debug("\r", serial_enable_echo);
                }
                else
                {
                    serial_send_string_debug(INVALID_CMD, serial_enable_echo);
                    break; // error dont save to database
                }
            }
            db_set_value16(db_serial_enable_msb, db_serial_enable_lsb, serial_flags);
            ////schedule_message(0, task_database, msg_update_primary_db, 0);

            serial_send_int_ex_debug(NULL, serial_flags, 16, NULL, serial_enable_echo);
            break;
        }
        case 'c':
            serial_send_string_debug("Database - ", serial_enable_echo);
            //
            // point to next character to determine which database to write to
            //
            rx_buffer++;
            //
            // write to ram-based database, disable any periodic database CRC'ing
            //
            if ('1' == *rx_buffer)
            {
                rx_buffer += 2; // point to first index character
                index = (database_index)serial_uart_atou8(rx_buffer);
                if (index < MEM_EEPROM_SIZE)
                {
                    rx_buffer += 3; // point to first value character
                    val = serial_uart_atou8(rx_buffer);
                    if (index == db_normal_mode_period && (val < NORMAL_MODE_DELAY_MIN || val > NORMAL_MODE_DELAY_MAX))
                    {
                        char ctemp[40] = {0};
                        /** \todo IMPLEMENT ME */
                        // This was commented out due to use of sprintf
                        // sprintf(ctemp,"%s min_val=%d
                        // max_val=%d",INVALID_ENTRY,NORMAL_MODE_DELAY_MIN,NORMAL_MODE_DELAY_MAX);
                        serial_send_string_debug(ctemp, serial_enable_always);
                        break;
                    }
                    db_set_value(index, val);
                    serial_send_int_ex_debug("save value:", val, 16, NULL, serial_enable_echo);
                    serial_send_int_ex_debug(" to index:", index, 16, ".", serial_enable_echo);
                }
                else
                {
                    serial_send_string_debug(INVALID_CMD, serial_enable_echo);
                }
            }
            else if ('2' == *rx_buffer)
            {
                serial_send_string_debug("save primary DB", serial_enable_echo);
                ////schedule_message(0, task_database, msg_update_primary_db, 0);
            }
            break;
        case 'C':
        {
            serial_send_string_debug("Get Database - ", serial_enable_echo);
            rx_buffer++;
            charval = *rx_buffer;
            rx_buffer += 2; // get index in next two bytes
            index = (database_index)serial_uart_atou8(rx_buffer);

            switch (charval)
            {
            case '0':
                print_device_model();
                break;
            case '1':
            case '2':
            case '3':
                if (index < MEM_EEPROM_SIZE)
                {
                    if ('1' == charval) // ram database
                    {
                        charval = db_get_value(index);
                    }
                    else if ('2' == charval) // primary database
                    {
                        ////schedule_message(0, task_database, msg_read_primary_entry, index);
                    }
                    else if ('3' == charval) // secondary database
                    {
                        ////schedule_message(0, task_database, msg_read_backup_entry, index);
                    }
                    serial_send_int_ex_debug(NULL, charval, 16, NULL, serial_enable_always); // always send C outputs
                }
                else
                {
                    serial_send_string_debug(INVALID_CMD, serial_enable_echo);
                }
                break;
            case '4':
            case '5':
                if ('4' == charval)
                {
                    read_events(&event, index, 1);
                }
                else
                {
                    serial_send_string_debug(INVALID_CMD, serial_enable_echo);
                    break;
                }
                serial_send_int_ex_debug(NULL, event.event_id, 16, NULL, serial_enable_always);
                serial_send_string_debug(", ", serial_enable_echo);
                serial_send_int_ex_debug(NULL, event.sequence_number, 16, NULL, serial_enable_always);
                serial_send_string_debug(", ", serial_enable_echo);
                serial_send_int_ex_debug(NULL, event.day, 16, NULL, serial_enable_always);
                break;
            }
        }
        break;
        case 'e':
            // Day/Night override
            rx_buffer++;
            if ('0' == *rx_buffer)
            {
                // Toggle off override
                serial_send_string_debug("Day/Night override disabled.", serial_enable_echo);
            }
            else if ('1' == *rx_buffer)
            {
                // Day mode activated
                serial_send_string_debug("Day override enabled.", serial_enable_echo);
            }
            else if ('2' == *rx_buffer)
            {
                // Night mode activated
                serial_send_string_debug("Night override enabled.", serial_enable_echo);
            }
            break;
        case 'E':
            serial_send_string_debug("Display Light Data", serial_enable_echo);
            break;
        case 'F':
            serial_send_string_debug("Force CO Alarm", serial_enable_echo);
            ////schedule_message(0, task_co, msg_enter_co_alarm, 0);
            break;
        case 'G':
            rx_buffer++;
            //
            // dump the ram-based database
            //
            /** @todo remove after full database is implemented.*/
            if ('1' == *rx_buffer)
            // if (1)
            {
                ////schedule_message(0, task_serial, msg_dump_flash_contents, SERIAL_SND_DB_RAM);
            }

            //
            // dump the primary dataflash database including CRC
            //
            else if ('2' == *rx_buffer)
            {
                ////schedule_message(0, task_serial, msg_dump_flash_contents, SERIAL_SND_DB_PRIMARY);
            }

            //
            // dump the factory backup dataflash database including CRC
            //
            else if ('3' == *rx_buffer)
            {
                ////schedule_message(0, task_serial, msg_dump_flash_contents, SERIAL_SND_DB_BACKUP);
            }

            //
            // dump the events
            //
            else if ('4' == *rx_buffer)
            {
                ////schedule_message(0, task_serial, msg_dump_flash_contents, SERIAL_SND_DB_EVENTS);
            }

            //
            // dump everything
            //
            else if ('\0' == *rx_buffer)
            {
                /* TODO: bool dump_all = true */
                serial_dump_all = true;
                ////schedule_message(0, task_serial, msg_dump_flash_contents, SERIAL_SND_DB_RAM);
            }
            break;
        case 'H':
            //
            // Enable Photo Cal without HD Cal Complete SET
            //  This uses Default Photo Equation
            //
            if (!(db_get_value(db_calibration_lsb) & cal_lic))
            {
                // Set LIC Cal Complete bit
                db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_lic);

                // Save Cal Complete Status
                ////schedule_message(0, task_database, msg_update_primary_db, 0);

                serial_send_string_debug("Enable Photo Cal", serial_enable_echo);
            }
            break;
        case 'i':
        {
            // i XX YY ZZ\r  -- Set register YY on device XX to ZZ
            rx_buffer += 2; // point to first I2C addr character
            uint8_t const i2c_addr = (database_index)serial_uart_atou8(rx_buffer);

            rx_buffer += 3; // point to first register address character
            uint8_t const reg_addr = serial_uart_atou8(rx_buffer);

            rx_buffer += 3; // point to the first value character
            uint8_t const value = serial_uart_atou8(rx_buffer);

            bool const success  = i2c_write(i2c_addr, reg_addr, value);
            if (success)
            {
                serial_send_int_ex_debug("I2C wrote ", value, 16, NULL, serial_enable_echo);
                serial_send_int_ex_debug(" to device ", i2c_addr, 16, NULL, serial_enable_echo);
                serial_send_int_ex_debug(" at register ", reg_addr, 16, ".", serial_enable_echo);
            }
            else
            {
                serial_send_string_debug("I2C write failed\n", serial_enable_echo);
            }
        }
        break;
        case 'I':
        {
            // I XX YY\r  -- Read register YY on device XX
            rx_buffer += 2; // point to first I2C addr character
            uint8_t const i2c_addr = (database_index)serial_uart_atou8(rx_buffer);

            rx_buffer += 3; // point to first register address character
            uint8_t const reg_addr = serial_uart_atou8(rx_buffer);

            uint8_t value;
            bool const success = i2c_read(i2c_addr, reg_addr, &value);
            if (success)
            {
                serial_send_int_ex_debug("I2C read ", value, 16, NULL, serial_enable_echo);
                serial_send_int_ex_debug(" from device ", i2c_addr, 16, NULL, serial_enable_echo);
                serial_send_int_ex_debug(" at register ", reg_addr, 16, ".", serial_enable_echo);
            }
            else
            {
                serial_send_string_debug("I2C read failed\n", serial_enable_echo);
            }
        }
        break;
        case 'K':
            // Print out the I2C registers
            print_i2c_register_values();
            break;
        case 'L':
            serial_send_string_debug("EOL CMD - ", serial_enable_echo);

            rx_buffer++;
            // L0 - Clears Accelerated Life Counter & Life Expiration Test
            // L1 - Enables Life Expiration Test
            // L2 - Set Life counter to Year (L2(year 1-9)) hex value!
            // L3 - Stop Accelerated life counter without resetting Life Count
            // L4 - Enable accelerated hourly counter (1 hour = 1 min, 1 day = 24 min)
            // L5 - Disable accelerated hourly counter
            // L8 - Enables Accelerated Life Counter ( 1 min = 1 day)
            //      (for diag History or other testing)

            switch (*rx_buffer)
            {
            case '\0':
            {
                uint16_t currentDays = db_get_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb);
                serial_send_int_ex_debug("current days - ", currentDays, 10, NULL, serial_enable_echo);
                break;
            }
            case '0':
                db_set_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb, 0);
                ////schedule_message(0, task_database, msg_update_primary_db, 0);
                ////schedule_message(0, task_day_count, msg_set_flag, accel_day_timer);
                ////schedule_message(0, task_clean_air_value, msg_set_flag, ((uint16_t)0x09) << 8);
                serial_send_string_debug("Reset Default", serial_enable_echo);
                break;
            case '1':
                db_set_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb, 3748);
                ////schedule_message(0, task_database, msg_update_primary_db, 0);
                ////schedule_message(0, task_day_count, msg_set_flag, accel_minute_timer);
                serial_send_string_debug("Replace Alarm", serial_enable_echo);
                break;
            case '2':
            {
                rx_buffer++;
                const uint16_t year = serial_uart_atou8(rx_buffer);
                // year must be in hex: 00, 01, 02...0A
                db_set_value16(db_counter_no_of_days_msb, db_counter_no_of_days_lsb, year * 365);
                ////schedule_message(0, task_database, msg_update_primary_db, 0);
                char buffer[] = "year set to ";
                uint8_to_hex(buffer + 12, year);
                serial_send_string_debug(buffer, serial_enable_echo);
                ////schedule_message(0, task_day_count, msg_set_flag, restart_timer);
                break;
            }
            case '3':
            case '5':
                ////schedule_message(0, task_day_count, msg_set_flag, accel_day_timer);
                serial_send_string_debug("stop Accelerated Life", serial_enable_echo);
                break;
            case '4':
                ////schedule_message(0, task_day_count, msg_set_flag, accel_hour_timer);
                serial_send_string_debug("Timer - 1 min = 1 hour", serial_enable_echo);
                break;
            case '8':
                ////schedule_message(0, task_day_count, msg_set_flag, accel_minute_timer);
                serial_send_string_debug("Timer - 1 min = 1 day", serial_enable_echo);
                break;
            case '9':
                ////schedule_message(0, task_clean_air_value, msg_set_flag, ((uint16_t)0x08) << 8);
                serial_send_string_debug("Timer - CAV every 2 mins", serial_enable_echo);
                break;
            default:
                serial_send_string_debug(INVALID_CMD, serial_enable_echo);
                break;
            }
            break;
        case 'M':
            serial_send_string_debug("Message CMD - shedule_message - ", serial_enable_echo);
            // schedule any message M<ms> <task> <msg> <data>
            // the spaces matter, and all parameters are 2 digit hex values - not "msg_continue" or "1"
            // TODO: why does this work for some messages but not others?
            rx_buffer++;
            uint16_t ms;
            task_id task;
            message_id msg;
            uint16_t data;
            ms = serial_uart_atou8(rx_buffer);

            while (*rx_buffer != ' ')
            {
                rx_buffer++;
            }
            rx_buffer++;
            task = (task_id)serial_uart_atou8(rx_buffer);

            while (*rx_buffer != ' ')
            {
                rx_buffer++;
            }
            rx_buffer++;
            msg = (message_id)serial_uart_atou8(rx_buffer);

            while (*rx_buffer != ' ')
            {
                rx_buffer++;
            }
            rx_buffer++;
            /** \todo IMPLEMENT ME */
            // This was commented out due to use of strtol
            // The assignment to data is only until this is fixed
            // data = (uint16_t)strtol((const char *)rx_buffer, NULL, 16) ;
            data = 0;

            ////schedule_message(ms, task, msg, data);
            serial_send_int_ex_debug("ms(10):", ms, 10, NULL, serial_enable_echo);
            serial_send_int_ex_debug(" task(10):", task, 10, NULL, serial_enable_echo);
            serial_send_int_ex_debug(" msg(10):", msg, 10, NULL, serial_enable_echo);
            serial_send_int_ex_debug(" data(16):", data, 16, NULL, serial_enable_echo);
            break;
        case 'P':
        {
        }
        break;
        case 'Q':
            rx_buffer++;
            if ('F' == *rx_buffer)
            {
                serial_send_string_debug("Clean air - CAV_RESET Factory", serial_enable_echo);
                ////schedule_message(0, task_clean_air_value, msg_ready, cav_reason_serial_reset_factory);
            }
            else
            {
                serial_send_string_debug("Clean air - CAV_RESET", serial_enable_echo);
                ////schedule_message(0, task_clean_air_value, msg_ready, cav_reason_serial_reset);
            }
            break;
        // Manufacturing's Product Traceability
        // Mass Read from Database
        case 'R':
        {
            uint8_t const start_address = serial_uart_atou8(rx_buffer + 1);
            uint8_t const n             = serial_uart_atou8(rx_buffer + 3);
            // Pack these in backwards since memory_crc_valid assumes that it will be big endian
            uint32_t const crc = serial_uart_atou8(rx_buffer + 11) << 24 | serial_uart_atou8(rx_buffer + 9) << 16 |
                                 serial_uart_atou8(rx_buffer + 7) << 8 | serial_uart_atou8(rx_buffer + 5) << 0;
            uint8_t const buffer[]  = {'R', start_address, n};
            uint32_t const crc_calc = memory_calculate_crc_unaligned(buffer, buffer + sizeof(buffer));

            if (memory_crc_valid(crc_calc, crc))
            {
                uint8_t const start = start_address;
                uint8_t const count = n <= 21 ? n : 21;
                char buffer[2 * (count + 4) + 1];
                uint32_t crc = 0xffffffff;
                for (uint8_t i = 0; i < count; ++i)
                {
                    uint8_t const value = db_get_value(start + i);
                    uint8_to_hex(buffer + 2 * i, value);
                    crc = memory_calculate_crc_unaligned_running(&value, &value + 1, crc);
                }
                uint8_to_hex(buffer + 2 * count + 0, (uint8_t)(crc >> 24));
                uint8_to_hex(buffer + 2 * count + 2, (uint8_t)(crc >> 16));
                uint8_to_hex(buffer + 2 * count + 4, (uint8_t)(crc >> 8));
                uint8_to_hex(buffer + 2 * count + 6, (uint8_t)(crc >> 0));
                buffer[2 * count + 8] = '\0';
                serial_send_string(buffer);
            }
            break;
        }
        case 's':
        case 'S':
        {
            //
            // point to next character to determine get fake smoke level
            //
            rx_buffer++;

            if ('0' == *rx_buffer)
            {
                // Reset all 3 measurements
                clear_photo_override();
            }

            else if ('1' == *rx_buffer)
            {
                rx_buffer++;

                // IR_F
                uint8_t data = serial_uart_atou8(rx_buffer);
                set_ir_f_override(data);
            }

            else if ('2' == *rx_buffer)
            {
                rx_buffer++;

                // IR_B
                uint8_t data = serial_uart_atou8(rx_buffer);
                set_ir_b_override(data);
            }

            else if ('3' == *rx_buffer)
            {
                rx_buffer++;

                // BL_F
                uint8_t data = serial_uart_atou8(rx_buffer);
                set_bl_f_override(data);
            }

            else if ('4' == *rx_buffer)
            {
                rx_buffer++;

                // IR_F
                uint8_t tmp_ir_f = serial_uart_atou8(rx_buffer);
                set_ir_f_override(tmp_ir_f);

                // IR_B
                rx_buffer += 2;
                uint8_t tmp_ir_b = serial_uart_atou8(rx_buffer);
                set_ir_b_override(tmp_ir_b);

                // BL_F
                rx_buffer += 2;
                uint8_t tmp_bl_f = serial_uart_atou8(rx_buffer);
                set_bl_f_override(tmp_bl_f);
            }
            else
            {
                serial_send_string_debug(INVALID_CMD, serial_enable_echo);
                break;
            }
        }
        break;
        case 'U':
            // Unlock readout protection
            // Syntax Uu<CR>
            rx_buffer++;
            if ('u' == *rx_buffer)
            {
                if (!flash_set_rdp_level(0))
                {
                    serial_send_string("\rError unlocking flash.\r");
                }
            }
            break;
        case 'V':
        {
        }
        break;
        // Manufacturing's Product Traceability
        // Mass Write to Database
        case 'W':
        {
            uint8_t const start_address = serial_uart_atou8(rx_buffer + 1);
            uint8_t const n             = serial_uart_atou8(rx_buffer + 3);
            uint8_t buffer[n];
            for (uint8_t i = 0; i < n; ++i)
            {
                buffer[i] = serial_uart_atou8(rx_buffer + 5 + 2 * i);
            }
            // Pack these in backwards since memory_crc_valid assumes that it will be big endian
            // rx_buffer - base address
            // +5 - 5 bytes before data bytes
            // (2*n) - for n data bytes
            // 6..0 - Index into the CRC word
            uint32_t const crc = serial_uart_atou8(rx_buffer + 5 + (2 * n) + 6) << 24 |
                                 serial_uart_atou8(rx_buffer + 5 + (2 * n) + 4) << 16 |
                                 serial_uart_atou8(rx_buffer + 5 + (2 * n) + 2) << 8 |
                                 serial_uart_atou8(rx_buffer + 5 + (2 * n) + 0) << 0;
            uint32_t crc_calc = memory_calculate_crc_unaligned(rx_buffer, rx_buffer + 1);
            crc_calc          = memory_calculate_crc_unaligned_running(&start_address, &start_address + 1, crc_calc);
            crc_calc          = memory_calculate_crc_unaligned_running(&n, &n + 1, crc_calc);
            crc_calc          = memory_calculate_crc_unaligned_running(buffer, buffer + n, crc_calc);

            if (memory_crc_valid(crc_calc, crc))
            {
                serial_send_cr();
                uint8_t const start = start_address;
                uint8_t const count = n <= 21 ? n : 21;
                db_set_sequential_values((database_index)start, buffer, count);

                // Schedule a DB update
                ////schedule_message(0, task_database, msg_update_primary_db, 0);
                serial_send_string("0D");
            }
            else
            {
                serial_send_string("15");
            }
            break;
        }
        case 'x':
        {
            rx_buffer++;
            uint8_t IRF_set = serial_uart_atou8(rx_buffer);
            rx_buffer += 2;
            uint8_t IRB_set = serial_uart_atou8(rx_buffer);
            rx_buffer += 2;
            uint8_t BLF_set = serial_uart_atou8(rx_buffer);
            db_set_value(db_current_ir_f, IRF_set);
            db_set_value(db_current_ir_b, IRB_set);
            db_set_value(db_current_bl_f, BLF_set);
        }
        break;
        case 'Z':
            rx_buffer++;
            if ('z' == *rx_buffer)
            {
                ////schedule_message(0, task_state, msg_fault_fatal, 0);
            }
            break;
        default:
            // dont send extra CR on bad Command with no output.
            serial_send_string_debug("Serial - Invalid CMD", serial_enable_echo);
            break;
        }

        serial_send_cr_prompt();
    }
    else if (serial_mode == MODE_AFE)
    {
        if (rx_buffer[0] == 'X')
        {
            serial_mode = MODE_NORMAL;
            serial_send_string_debug("Serial - Normal Mode", serial_enable_echo);
            serial_send_cr_prompt();
            return;
        }
        else
        {
            afe_serial_command(rx_buffer);
            send_AFE_CR_prompt();
        }
    }
}

//*********************************************************************
/**
 * \brief Support routine for sending any of the three database to the uart
 * \param task_type Type of database task
 * \param index Index to start tx
 * \returns type for more to go or done
 */
//*********************************************************************
static void serial_send_db_dump(Serial_task_type_e const task_type, uint8_t index)
{
    uint8_t val = 0;

    // start by sending the index number at the left margin
    if (0 == index)
    {
        // Send an initial CR so that all columns line up.
        serial_send_string("\r");
    }

    char buffer[] = "  :                                                 \r";
    uint8_to_hex(buffer, index);

    //
    // 16 bytes displayed per line
    //
    for (uint8_t xmit_idx = 0; xmit_idx < 16; xmit_idx++)
    {
        if (SERIAL_SND_DB_RAM == task_type)
        {
            val = db_get_value((database_index)index);
        }
        else if (SERIAL_SND_DB_PRIMARY == task_type)
        {
            val = read_primary_db(index);
        }
        else if (SERIAL_SND_DB_BACKUP == task_type)
        {
            val = read_backup_db(index);
        }

        uint8_to_hex(buffer + 5 + xmit_idx * 3, val);

        index++;

        if (MEM_EEPROM_SIZE == index) // if (sizeof(DB) == index)
        {
            serial_send_string(buffer);
            serial_send_cr();
            if (!serial_dump_all)
            {
                // db dump complete
                serial_send_cr_prompt();
                //set_flash_in_progress(false);
            }
            else
            {
                serial_send_cr();
                switch (task_type)
                {
                case SERIAL_SND_DB_RAM:
                    ////schedule_message(10, task_serial, msg_continue, ((uint16_t)(SERIAL_SND_DB_PRIMARY) << 8) | 0);
                    break;
                case SERIAL_SND_DB_PRIMARY:
                    ////schedule_message(10, task_serial, msg_continue, ((uint16_t)(SERIAL_SND_DB_BACKUP) << 8) | 0);
                    break;
                case SERIAL_SND_DB_BACKUP:
                    ////schedule_message(10, task_serial, msg_continue, ((uint16_t)(SERIAL_SND_DB_EVENTS) << 8) | 0);
                    break;
                default:
                    break;
                }
            }
            return;
        }
    }
    serial_send_string(buffer);
    ////schedule_message(10, task_serial, msg_continue, ((uint16_t)task_type << 8) | index);
}

//*********************************************************************
/**
 * \brief Support routine for sending any of the three database to the uart
 * \param index Index to start process at
 */
//*********************************************************************
static void serial_send_events_dump(uint8_t index)
{
    event_entry event = {0, 0, 0, 0};

    {
        char buffer[] = "  :  ";
        uint8_to_hex(buffer, index);
        serial_send_string(buffer);
    }

    // read contents of flash into events buffer
    read_events(&event, index, 1);

    //
    // test for this being the last event
    //
    if (0xFF == event.event_id)
    {
        // event history dump complete
        serial_send_cr();
        serial_send_prompt();
        serial_dump_all = false;
        //set_flash_in_progress(false);
    }
    else
    {
        // print out the next event entry
        char buffer[] = "                   ";
        uint8_to_hex(buffer, event.event_id);
        uint8_to_hex(buffer + 3, event.sequence_number);
        uint16_to_hex(buffer + 6, event.day);
        uint16_to_hex(buffer + 11, event.padding >> 16);
        uint16_to_hex(buffer + 15, event.padding);
        serial_send_string(buffer);
        index++;

        serial_send_cr();
        ////schedule_message(10, task_serial, msg_continue, ((uint16_t)SERIAL_SND_DB_EVENTS << 8) | index);
    }
}

//*********************************************************************
/**
 * \brief Initial serial callback messages
 */
//*********************************************************************
static void serial_init_callbacks(void)
{
    ////schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_serial) << 8 | key_co_msb);
    ////schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_serial) << 8 | key_co_lsb);
    ////schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_serial) << 8 | key_accumulator_msb);
    ////schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_serial) << 8 | key_accumulator_lsb);
    ////schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_serial) << 8 | key_co_volt_msb);
    ////schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_serial) << 8 | key_co_volt_lsb);
    ////schedule_message(0, task_key_value_store, msg_add_callback, ((uint16_t)task_serial) << 8 | key_co_status);
}

void serial_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        serial_dump_all = false;
        serial_flags    = db_get_value16(db_serial_enable_msb, db_serial_enable_lsb);

        if (serial_enabled())
        {
            // If UART is enabled

            // initialize key value store callbacks
            serial_init_callbacks();
        }
        break;
    case msg_continue:
    {
        Serial_task_type_e process;
        uint8_t index;
        process = (Serial_task_type_e)(data >> 8);
        index   = (uint8_t)data;

        switch (process)
        {
            // Did we return from a callback?
            //  then process the previous command
        case SERIAL_CMD_PROCESS:
            // No callback, process new serial command
            serial_command_processor();
            break;
        case SERIAL_PERIODIC_TASK_CO:
            serial_format_task_buffer_co();
            break;
        case SERIAL_SND_DB_RAM:
        case SERIAL_SND_DB_PRIMARY:
        case SERIAL_SND_DB_BACKUP:
            serial_send_db_dump(process, index);
            break;
        case SERIAL_SND_DB_EVENTS:
            serial_send_events_dump(index);
            break;
        default:
            break;
        }
    }
    break;
    case msg_dump_flash_contents:
//        if (!get_flash_in_progress())
//        {
//            //set_flash_in_progress(true);
//            print_device_model();
//            //schedule_message(0, task_serial, msg_continue, data << 8);
//        }
//        else
//        {
//            //schedule_message(100, task_serial, msg_dump_flash_contents, data);
//        }
        break;
    case msg_value:
        serial_update_values(data);
        break;
    default:
        break;
    }
}
