//******************************************************************
/*! \file app_supervisor.c
 *
 * \brief Kidde: Mongoose-fw - Define app supervisor task functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_supervisor.h"
#include "app_database.h"
#include "app_serial.h"
#include "drv_watchdog.h"
#include "in_day_count.h"
#include "key_value_store.h"
#include "main.h"
#include "message.h"
#include "task_list.h"
#include <stddef.h>
#include <stdint.h>

// largest schedule_timed_message() window is 1700ms
#define DELAY_NORMAL_TASK 2000
#define DELAY_30_SECONDS  15 // 2000ms * 15 counts = 30 seconds

static uint16_t counter_db[counter_size] = {0};    // initialize to 0
static uint16_t previous_counter_db[counter_size]; // initialize to 0
static uint8_t counter_failures[counter_size];     // initialize to 0

uint16_t supervisor_counter_get_value(counter_index const index)
{
    return counter_db[index];
}

void supervisor_counter_clear(counter_index const index)
{
    counter_db[index] = 0;
}

void supervisor_counter_increase(counter_index const index)
{
    counter_db[index]++;

    if (counter_db[index] == 0) // do not allow the counter to be 0;
    {
        counter_db[index]++;
    }
}

void check_counters()
{

    serial_send_string_debug("Supervision - counter values - ", serial_enable_supervision);

    for (counter_index i = (counter_index)0; i < counter_size; i++)
    {
        uint16_t counter_value = supervisor_counter_get_value(i);

        serial_send_int_ex_debug(NULL, counter_value, 16, " ", serial_enable_supervision);

        // dont run against counters not suppored on device.
        if (counter_value == 0)
        {
            counter_failures[i] = 0;
            continue;
        }

        if (previous_counter_db[i] == counter_value)
        {
            // has not counted up
            counter_failures[i]++;
            // reset if more than one counter failure
            if (counter_failures[i] > 1)
            {
                force_soft_reset();
            }
        }
        else
        {
            counter_failures[i] = 0;
        }

        previous_counter_db[i] = counter_value;
    }
    serial_send_string_debug("\r", serial_enable_supervision);
}

void supervisor_task(message_id const message, uint16_t const data)
{
    (void)data;
    switch (message)
    {
    case msg_init:
        //schedule_message(0, task_supervisor, msg_continue, 0); // start watchdog kick.

        // reset all counters in case of obliterate
        for (counter_index i = (counter_index)0; i < counter_size; i++)
        {
            supervisor_counter_clear(i);
        }

        break;
    case msg_continue:
    {
        static uint16_t counter = 0;
        // watchdog kick in here to make sure supervisor task is always running
        kick();

        // create counter to do supervisor check
        if (counter >= DELAY_30_SECONDS) // every 30 seconds
        {
            check_counters();
            counter = 0;
        }
        else
        {
            counter++;
        }

        //schedule_message(DELAY_NORMAL_TASK, task_supervisor, msg_continue, 0); // kick watchdog every 2 seconds
        break;
    }
    default:
        break;
    }
}
