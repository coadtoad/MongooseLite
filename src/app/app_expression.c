/**
 * @file app_expression.c
 */

#include "app_expression.h"
#include "Project.h"
#include "drv_led.h"
#include "drv_sounder.h"
#include "expression_task.h"
#include "message.h"
#include "out_voice.h"
#include <stdbool.h>

/**
 * @brief The length of a button chirp in ms
 */
#define CHIRP_ON_PERIOD  30

/**
 * @brief The length between button chirps in ms
 */
#define CHIRP_OFF_PERIOD 100

#define LINES_SIZE(x)    (sizeof(x) / sizeof((x)[0]))

/**
 * @brief One step in running an expression
 *
 * An array of this is the set of steps for an expression
 *
 * @note An array of this should be static so that they are placed in flash
 */
struct expression_line
{
    unsigned step_size       : 10; /**< "per" The duration, in 1ms increments, for this step */
    unsigned uninterruptible : 1;  /**< "int" Can the expression not be exited here */
    unsigned sounder         : 1;  /**< "snd" Is the sounder on for this step */
    unsigned blue_led        : 1;  /**< "bl" Is the blue (CO) LED on for this step */
    unsigned blue_fast_fade  : 1;  /**< "blf" Does a fast blue fade start on this step */
    unsigned blue_slow_fade  : 1;  /**< "bls" Does a slow blue fade start on this step */
    unsigned red_led         : 1;  /**< "rd" Is the red LED on for this step */
    unsigned red_fast_fade   : 1;  /**< "rdf" Does a fast red fade start on this step */
    unsigned red_slow_fade   : 1;  /**< "rds" Does a slow red fade start on this step */
    unsigned amber_led       : 1;  /**< "am" Is the amber LED on for this step */
    unsigned amber_fast_fade : 1;  /**< "amf" Does a fast amber fade start on this step */
    unsigned amber_slow_fade : 1;  /**< "ams" Does a slow amber fade start on this step */
    unsigned green_led       : 1;  /**< "gr" Is the green LED on for this step */
    unsigned green_fast_fade : 1;  /**< "grf" Does a fast green fade start on this step */
    unsigned green_slow_fade : 1;  /**< "grs" Does a slow green fade start on this step */
    /** The voice to begin playing on this step, @link voice_message::number_of_voices @endlink for no voice */
    unsigned voice           : 8;
};

/**
 * @brief The upper level structure for an expression
 *
 * @note An array of this should be static so that they are placed in flash
 */
struct expression
{
    struct expression_line const *lines; /**< A pointer to an array of steps for the expression */
    uint8_t line_count;                  /**< The number of steps pointed to by @link lines @endlink */
    /** The duration, in 1ms increments, that the expression cannot be interrupted for */
    uint16_t uninterruptible_for;
    uint16_t sleep_steps; /**< The duration, in 1ms increments, between runs of the expression */
    /**
     * @brief The number of times the expression should repeat
     *
     * A value of 0x0000 will repeat forever.  Any other value gives the number
     * of times that the expression will repeat.  This means that an expression
     * can give an explicit number of iterations up to 65,535.
     */
    uint16_t repeat;
    /** When the expression repeats complete, this expression will run after the current expression */
    struct expression const *next_expression;
};

static struct expression_line lines_default[]      = { //  DC power on blink
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, number_of_voices},
    {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_master_smoke[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 1
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 2
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 3
    {500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {600, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_fire}, // voice   // 600 ms on time
    {400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 1
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 2
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 3
    {400, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_remote_smoke[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 1
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 2
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 3
    {500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {600, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_fire}, // voice   // 600 ms on time
    {400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 1
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 2
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 500 ms blink 3
    {400, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_smoke_hush[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_fault_photo_with_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_smk_sens_error}, // 2500 voice on time
    {1000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_fault_photo_without_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_alarm_memory_smoke[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_master_co[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 1
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 2
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 3
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 4
    {500, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {200, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_co}, // voice // 2200 on time
    {1000, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 1
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 2
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 3
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 4
    {700, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_remote_co[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 1
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 2
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 3
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 4
    {500, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {200, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_co}, // voice // 2200 on time
    {1000, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 1
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 2
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 3
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 4
    {700, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_co_conserve[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 1
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 2
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 3
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 4
    {500, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {200, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_co}, // voice // 2200 on time
    {1000, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 1
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 2
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}, // 100 ms blink 3
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}}; // 100 ms blink 4

static struct expression_line lines_fault_co_with_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, voice_co_sens_error}, // 2500 ms on time
    {1000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {1000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_fault_co_without_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_alarm_memory_co[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_eol_with_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, voice_replace_alarm}, // 1100 ms on time
    {1000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_eol_without_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_eol_hush[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_battery_none_with_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_activate_battery}, // 1500 ms on time
    {1000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
};

static struct expression_line lines_battery_none_without_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_battery_low_with_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, voice_low_battery}, // 1500 ms on time
    {1000, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
};

static struct expression_line lines_battery_low_without_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_battery_low_hush[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_ptt_initialize[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_fault_fatal_with_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, voice_replace_alarm}, // 1100 off time
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {700, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_fault_fatal_without_voice[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_button_stuck[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, number_of_voices},
    {300, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_power_on[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {30, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_pass_with_chirp[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_pass_without_chirp[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_fail_with_chirp[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_fail_without_chirp[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_battery[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_battery_no_button[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_pass_co_circuit[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_co_circuit[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, number_of_voices}};

static struct expression_line lines_cal_co_circuit_no_button[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_0_ppm[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, number_of_voices}};

static struct expression_line lines_cal_150_ppm[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, number_of_voices}};

static struct expression_line lines_cal_pass_400_ppm[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, number_of_voices}};

static struct expression_line lines_cal_400_ppm[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, number_of_voices}};

static struct expression_line lines_cal_photo_fast[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {30, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_photo_slow[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {1000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_inline_smoke_wait[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_inline_smoke_stabilize[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_inline_smoke_average[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices},
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

static struct expression_line lines_cal_inline_smoke_verify[] = {
    // per, int, snd, bl, blf, bls, rd, rdf, rds, am, amf, ams, gr, grf, grs, voice
    {500, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, number_of_voices}};

/**
 * @brief A table of all expressions
 *
 * @note @link expression_id @endlink is indexes into this array
 */
// clang-format off
static struct expression const expressions[] =
{
    // lines,                          line_count,                                     ui,    sl, repeat, next
    {lines_default,                    LINES_SIZE(lines_default),                       0, 59700, 0x0000, NULL},
    {lines_master_smoke,               LINES_SIZE(lines_master_smoke),               6500,  1100, 0x0000, NULL},
    {lines_remote_smoke,               LINES_SIZE(lines_remote_smoke),               6500,  1100, 0x0000, NULL},
    {lines_smoke_hush,                 LINES_SIZE(lines_smoke_hush),                    0,  1600, 0x0000, NULL},
    {lines_fault_photo_with_voice,     LINES_SIZE(lines_fault_photo_with_voice),        0, 57000,     60, expressions + exp_fault_photo_without_voice},
    {lines_fault_photo_with_voice,     LINES_SIZE(lines_fault_photo_with_voice),        0, 57000,      1, expressions + exp_fault_photo_without_voice},
    {lines_fault_photo_without_voice,  LINES_SIZE(lines_fault_photo_without_voice),     0, 59400,     14, expressions + exp_fault_photo_with_voice},
    {lines_alarm_memory_smoke,         LINES_SIZE(lines_alarm_memory_smoke),            0, 14600, 0x0000, NULL},

    {lines_master_co,                  LINES_SIZE(lines_master_co),                  5700,     0, 0x0000, NULL},
    {lines_remote_co,                  LINES_SIZE(lines_remote_co),                  5700,     0, 0x0000, NULL},
    {lines_co_conserve,                LINES_SIZE(lines_co_conserve),                   0, 52900, 0x0000, NULL},
    {lines_fault_co_with_voice,        LINES_SIZE(lines_fault_co_with_voice),           0, 57000,     60, expressions + exp_fault_co_without_voice},
    {lines_fault_co_with_voice,        LINES_SIZE(lines_fault_co_with_voice),           0, 57000,      1, expressions + exp_fault_co_without_voice},
    {lines_fault_co_without_voice,     LINES_SIZE(lines_fault_co_without_voice),        0, 59200,     14, expressions + exp_fault_co_with_voice},
    {lines_alarm_memory_co,            LINES_SIZE(lines_alarm_memory_co),               0, 14200, 0x0000, NULL},

    {lines_eol_with_voice,             LINES_SIZE(lines_eol_with_voice),                0, 58500,     60, expressions + exp_eol_without_voice},
    {lines_eol_with_voice,             LINES_SIZE(lines_eol_with_voice),                0, 58500,      1, expressions + exp_eol_without_voice},
    {lines_eol_without_voice,          LINES_SIZE(lines_eol_without_voice),             0, 59200,     14, expressions + exp_eol_with_voice},
    {lines_eol_hush,                   LINES_SIZE(lines_eol_hush),                      0, 59200, 0x0000, NULL},
    {lines_battery_none_with_voice,    LINES_SIZE(lines_battery_none_with_voice),       0, 58000,     60, expressions + exp_battery_none_without_voice},
    {lines_battery_none_with_voice,    LINES_SIZE(lines_battery_none_with_voice),       0, 58000,      1, expressions + exp_battery_none_without_voice},
    {lines_battery_none_without_voice, LINES_SIZE(lines_battery_none_without_voice),    0, 59600,     14, expressions + exp_battery_none_with_voice},
    {lines_battery_low_with_voice,     LINES_SIZE(lines_battery_low_with_voice),        0, 58000,     60, expressions + exp_battery_low_without_voice},
    {lines_battery_low_with_voice,     LINES_SIZE(lines_battery_low_with_voice),        0, 58000,      1, expressions + exp_battery_low_without_voice},
    {lines_battery_low_without_voice,  LINES_SIZE(lines_battery_low_without_voice),     0, 59600,     14, expressions + exp_battery_low_with_voice},
    {lines_battery_low_hush,           LINES_SIZE(lines_battery_low_hush),              0, 59600, 0x0000, NULL},
    {lines_cal_pass_without_chirp,     LINES_SIZE(lines_cal_pass_without_chirp),        0, 59900,     20, expressions + exp_cal_pass_with_chirp},
    {lines_cal_pass_with_chirp,        LINES_SIZE(lines_cal_pass_with_chirp),           0,  1900, 0x0000, NULL},
    {lines_cal_fail_without_chirp,     LINES_SIZE(lines_cal_fail_without_chirp),        0,     0,  11999, NULL},
    {lines_cal_fail_with_chirp,        LINES_SIZE(lines_cal_fail_with_chirp),           0,     0,      1, NULL},
    {lines_cal_fail_without_chirp,     LINES_SIZE(lines_cal_fail_without_chirp),        0,     0,    298, NULL},
    {lines_cal_battery,                LINES_SIZE(lines_cal_battery),                   0,     0, 0x0000, NULL},
    {lines_cal_battery_no_button,      LINES_SIZE(lines_cal_battery_no_button),         0,   500, 0x0000, NULL},
    {lines_cal_pass_co_circuit,        LINES_SIZE(lines_cal_pass_co_circuit),           0,     0, 0x0000, NULL},
    {lines_cal_co_circuit,             LINES_SIZE(lines_cal_co_circuit),                0,     0, 0x0000, NULL},
    {lines_cal_co_circuit_no_button,   LINES_SIZE(lines_cal_co_circuit_no_button),      0,     0, 0x0000, NULL},
    {lines_cal_0_ppm,                  LINES_SIZE(lines_cal_0_ppm),                     0,    50, 0x0000, NULL},
    {lines_cal_150_ppm,                LINES_SIZE(lines_cal_150_ppm),                   0,   500, 0x0000, NULL},
    {lines_cal_pass_400_ppm,           LINES_SIZE(lines_cal_pass_400_ppm),              0,     0, 0x0000, NULL},
    {lines_cal_400_ppm,                LINES_SIZE(lines_cal_400_ppm),                   0,     0, 0x0000, NULL},
    {lines_cal_photo_fast,             LINES_SIZE(lines_cal_photo_fast),                0,    30,     13, expressions + exp_cal_photo_slow},
    {lines_cal_photo_slow,             LINES_SIZE(lines_cal_photo_slow),                0,  1000, 0x0000, NULL},
    {lines_cal_inline_smoke_wait,      LINES_SIZE(lines_cal_inline_smoke_wait),         0, 19500, 0x0000, NULL},
    {lines_cal_inline_smoke_stabilize, LINES_SIZE(lines_cal_inline_smoke_stabilize),    0, 18500, 0x0000, NULL},
    {lines_cal_inline_smoke_average,   LINES_SIZE(lines_cal_inline_smoke_average),      0, 17500, 0x0000, NULL},
    {lines_cal_inline_smoke_verify,    LINES_SIZE(lines_cal_inline_smoke_verify),       0,  1500, 0x0000, NULL},
    {lines_ptt_initialize,             LINES_SIZE(lines_ptt_initialize),                0,     0,      1, NULL},
    {lines_fault_fatal_with_voice,     LINES_SIZE(lines_fault_fatal_with_voice),        0, 58400,     60, NULL},
    {lines_fault_fatal_with_voice,     LINES_SIZE(lines_fault_fatal_with_voice),        0, 58400,      1, NULL},
    {lines_fault_fatal_without_voice,  LINES_SIZE(lines_fault_fatal_without_voice),     0, 58800,     14, NULL},
    {lines_button_stuck,               LINES_SIZE(lines_button_stuck),                  0, 58400, 0x0000, NULL},
    {lines_power_on,                   LINES_SIZE(lines_power_on),                      0,     0,      1, NULL}
};
// clang-format on

/**
 * @brief The expression currently running
 */
static struct expression const *current_expression = 0;
/**
 * @brief The index of the @link current_line @endlink
 */
static uint8_t current_step                        = -1;
/**
 * @brief The size, in milliseconds, of the executing step
 */
static uint16_t step_size                          = 0;
/**
 * @brief The number of steps until the expression can be exited
 */
static uint16_t remaining_uninterruptible          = 0;
/**
 * @brief The current line of the expression
 */
static struct expression_line const *current_line  = NULL;
/**
 * @brief If the expression is ending due to no repeats and no next expression
 */
static bool exiting                                = false;
/**
 * @brief The number of iterations after the current iteration
 *
 * @note A value of -1 indicates infinite repeats
 */
static int32_t repeats_remaining                   = 0;

/**
 * @brief Set expression execution to begin at @p expression
 *
 * @param[in] expression The new expression to execute
 */
static void set_expression_ptr(struct expression const *expression, bool const reset_uninterruptible);

void discard_continue(void)
{
    //un//schedule_messages_tm(task_expression, msg_continue);
}

void end_expression(void)
{
    //schedule_message(0, task_state, msg_end_express, current_expression - expressions);
}

VS_BOOL has_voice(void)
{
    return current_line->voice != number_of_voices;
}

VS_BOOL is_amber_led_on(void)
{
    return current_line->amber_led;
}

VS_BOOL is_blue_led_on(void)
{
    return current_line->blue_led;
}

VS_BOOL is_exiting(void)
{
    return exiting;
}

VS_BOOL is_green_led_on(void)
{
    return current_line->green_led;
}

VS_BOOL is_interruptible(void)
{
    return exiting || (!current_line->uninterruptible && remaining_uninterruptible == 0);
}

VS_BOOL is_red_led_on(void)
{
    return current_line->red_led;
}

VS_BOOL is_sounder_on(void)
{
    return current_line->sounder;
}

void expression_iteration_done(void)
{
    if (repeats_remaining)
    {
        // Start the expression over
        current_step = 0;
        current_line = current_expression->lines;
        step_size    = current_line->step_size;
        if (repeats_remaining > 0)
        {
            // Don't decrement if it should repeat forever
            // Repeat forever will have a value of -1
            --repeats_remaining;
        }
    }
    else if (current_expression->next_expression)
    {
        // Start the next expression
        end_expression();
        set_expression_ptr(current_expression->next_expression, true);
    }
    else
    {
        // End the expression, no next expression
        end_expression();
        set_expression(exp_default, true);
        exiting = true;
    }
}

void next_table_entry(void)
{
    if (remaining_uninterruptible)
    {
        if (step_size <= remaining_uninterruptible)
        {
            remaining_uninterruptible -= step_size;
        }
        else
        {
            remaining_uninterruptible = 0;
        }
    }

    if (current_step < current_expression->line_count - 1)
    {
        // There is at least one more line

        // Step to next line
        ++current_step;
        current_line = current_expression->lines + current_step;
        step_size    = current_line->step_size;
    }
    else
    {
        // No more lines

        // Do one of the following
        //  - Sleep
        //  - Start next expression
        //  - Start next iteration of this expression
        //  - End this expression

        if (current_step == current_expression->line_count - 1 && current_expression->sleep_steps)
        {
            // This expression needs to sleep
            ++current_step;
            step_size    = current_expression->sleep_steps;
            current_line = lines_default;
        }
        else
        {
            expression_iteration_done();
        }
    }
}

void send_voice(void)
{
    //schedule_message(0, task_voice_output, msg_voice, current_line->voice);
}

void schedule_chirp_off_step(void)
{
    //schedule_message(CHIRP_OFF_PERIOD, task_expression, msg_continue, 0);
}

void schedule_chirp_on_step(void)
{
    //schedule_message(CHIRP_ON_PERIOD, task_expression, msg_continue, 0);
}

void schedule_next_step(void)
{
    //schedule_message(step_size, task_expression, msg_continue, 0);
}

static void set_expression_ptr(struct expression const *expression, bool const reset_uninterruptible)
{
    current_expression = expression;
    current_step       = 0;
    if (reset_uninterruptible)
    {
        remaining_uninterruptible = current_expression->uninterruptible_for;
    }
    current_line      = current_expression->lines;
    step_size         = current_line->step_size;
    repeats_remaining = current_expression->repeat - 1;
    exiting           = false;
}

void set_expression(VS_UINT8 expression_id, VS_BOOL reset_uninterruptible)
{
    set_expression_ptr(expressions + expression_id, reset_uninterruptible);
}

void force_expression_end(void)
{
    exiting = true;
}

void expression_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        VSInitAll();
        break;
    case msg_begin_express:
        VSDeduct(evt_begin_express, (VS_UINT8)data);
        break;
    case msg_end_express:
        VSDeduct(evt_end_express);
        break;
    case msg_continue:
        VSDeduct(evt_continue);
        break;
    case msg_button_chirp:
        VSDeduct(evt_button_press, (VS_UINT8)data);
        break;
    case msg_button_complete:
        VSDeduct(evt_button_complete);
        break;
    default:
        break;
    }
}
