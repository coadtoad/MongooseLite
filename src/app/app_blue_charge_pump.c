//******************************************************************
/*! \file app_blue_charge_pump.c
 *
 * \brief Kidde: Mongoose-fw - Declare the blue charge pump function
 *
 * \author unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_blue_charge_pump.h"
#include "drv_photo.h"
#include "message.h"
#include "task_id.h"

#define CHARGE_PERIOD 30 ///< pump for 30ms period

//*********************************************************************
/**
 * \brief Set the mode for the pin
 *
 * \param message The message id for the function
 * \param data Any data sent with the message
 *
 */
//*********************************************************************
void blue_charge_pump_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        break;
    case msg_charge_blue_led:
        begin_charge_blue_led();
        schedule_message(CHARGE_PERIOD, task_blue_charge_pump, msg_continue, data);
        break;
    case msg_continue:
        end_charge_blue_led();
        schedule_message(0, (task_id)data, msg_ready, task_blue_charge_pump);
        break;
    default:
        break;
    }
}
