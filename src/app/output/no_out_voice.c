//******************************************************************
/*! \file no_out_voice.c
 *
 * \brief Kidde: Mongoose-fw - Implement voice output functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "out_voice.h"

void voice_task(message_id const message, uint16_t const data)
{
    (void)message;
    (void)data;
}
