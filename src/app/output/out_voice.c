//******************************************************************
/*! \file out_voice.c
 *
 * \brief Kidde: Mongoose-fw - Implement voice output functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "out_voice.h"
#include "drv_led.h"
#include "drv_voice.h"
#include "message.h"
#include "out_red_led.h"
#include "task_list.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief Enum for message continue to set certain signals.
 * \par Using message continue:
 *          power is enabled before sending all signals and disabled when done.
 *          clk_hi and clk_low set the clock signal to 1 and 0 respectively
 *          data_hi and data_low set the data signal to 1 and 0 respectively
 */
//*********************************************************************
typedef enum
{
    power_on,
    power_off,
    clock_hi,
    clock_lo,
    data_hi,
    data_lo
} message_data;

//*********************************************************************
/**
 * \brief Boolean if there is a voice message playing or not
 */
//*********************************************************************
static _Bool voice_playing = 0;

//*********************************************************************
/**
 * \brief This function takes an enumerated voice message and sends the appropriate clock and data signals to the voice
 * chip
 *
 * \note REFER TO THE VOICE CHIP DATA SHEET TO SEE EXPECTED WAVEFORMS
 *
 * \par Using message continue:
 *          power is enabled before sending all signals and disabled when done.
 *          clk_hi and clk_low set the clock signal to 1 and 0 respectively
 *          data_hi and data_low set the data signal to 1 and 0 respectively
 *
 * \note The loop is iterated 8 times to get each of 8 data bits
 *
 * \par The voice chip reads data on falling edges of the clock
 *          Becasue of this, data is set on the rising edge of the clock
 *          (note that the clock has a 50% duty cycle with 20ms periods)
 *
 * \param voice Message output to user
 * \param task Related task id to voice message
 * \param repeats Amount of times the voice message needs to repeat
 *
 * \return void
 */
//*********************************************************************
static void schedule_voice_message(voice_message const voice, task_id const task, uint8_t repeats)
{
    uint16_t time            = 0;
    uint16_t playback_length = get_voice_playback_time(voice);
    schedule_message(time, task_voice_output, msg_continue, power_on);
    time += 10 + get_voice_startup_time(); // returns time needed to init voice module
    do
    {
        uint8_t data = get_voice_data(voice); // returns voice enum wrt product
        // Transmit clock and data pattern to voice module
        for (uint8_t i = 0; i < 8; i++)
        {
            schedule_message(time, task_voice_output, msg_continue, clock_lo);
            schedule_message(time + 10, task_voice_output, msg_continue, clock_hi);

            if (data & 0x01)
            {
                schedule_message(time - 10, task_voice_output, msg_continue, data_hi);
            }
            else
            {
                schedule_message(time - 10, task_voice_output, msg_continue, data_lo);
            }

            data >>= 1;

            time += 20;
        }
        // Reset data line at the end of each message
        schedule_message(time - 10, task_voice_output, msg_continue, data_hi);
        // Adjust time for next message if needed
        if (repeats)
        {
            time += 10 + playback_length;
        }
    } while (repeats--);

    schedule_message(time + 10 + playback_length, task_voice_output, msg_continue, power_off);
    schedule_message(time + 10 + playback_length, task_voice_output, msg_set_flag, 0);
    if (task)
    {
        // Don't send msg_ready to task_database...
        schedule_message(time + 10 + playback_length, task, msg_ready, task_voice_output);
    }
}

void voice_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        voice_init();
        voice_data_hi();
        voice_clk_hi();
        break;
    case msg_continue:

        switch (data)
        {
        case power_on:
            voice_pwr_on();
            break;

        case power_off:
            voice_pwr_off();
            break;

        case clock_hi:
            voice_clk_hi();
            break;

        case clock_lo:
            voice_clk_lo();
            break;

        case data_lo:
            voice_data_lo();
            break;

        case data_hi:
            voice_data_hi();
            break;
        }
        break;
    case msg_set_flag:
        voice_playing = false;
        break;
    case msg_voice:
        if (!voice_playing)
        {
            schedule_voice_message((voice_message)(uint8_t)data, (task_id)(uint8_t)(data >> 8), 0);
            voice_playing = true;
        }
        else
        {
            schedule_message(100, task_voice_output, msg_voice, data);
        }
        break;
    case msg_voice_multiple_button_tones:
        if (!voice_playing)
        {
            schedule_voice_message(voice_tone_button_press, task_express_button_chirp, data);
            voice_playing = true;
        }
        else
        {
            schedule_message(100, task_voice_output, msg_voice_multiple_button_tones, data);
        }
        break;
    default:
        break;
    }
}
