//******************************************************************
/*! \file task_list.c
 *
 * \brief Kidde: Mongoose-fw - Initialize the task list
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "task_list.h"
#include "app_blue_charge_pump.h"
#include "app_database.h"
#include "app_events.h"
#include "app_expression.h"
#include "app_serial.h"
#include "app_supervisor.h"
#include "cal_0_ppm.h"
#include "cal_150_ppm.h"
#include "cal_2_point.h"
#include "cal_400_ppm.h"
#include "cal_battery.h"
#include "cal_circuit.h"
#include "cal_doe.h"
#include "cal_inline.h"
#include "cal_lic.h"
#include "clean_air_value.h"
#include "in_ac.h"
#include "in_battery.h"
#include "in_button.h"
#include "in_co.h"
#include "in_day_count.h"
#include "in_photo.h"
#include "io_interconnect.h"
#include "key_value_store.h"
#include "main.h"
#include "message.h"
#include "out_voice.h"
#include "ptt.h"
#include "state.h"
#include "vboost.h"

//*********************************************************************
/**
 * \brief Enum of all task types in specific order to match task_id.h
 * \note IMPORTANT must match task_list.c, task_id.h, debug_ids_heap.c
 */
//*********************************************************************
task_type const task_list[] = {&supervisor_task,       // 0
                               &database_task,         // 1
                               &key_value_store_task,  // 2
                               &voice_task,            // 3
                               &state_task,            // 4
                               &photo_task,            // 5
                               &co_task,               // 6
                               &battery_task,          // 7
                               &day_count_task,        // 8
                               &button_task,           // 9
                               &events_task,           // 10
                               &serial_task,           // 11
                               &interconnect_task,     // 12
                               &cal_0_ppm_task,        // 13
                               &cal_150_ppm_task,      // 14
                               &cal_400_ppm_task,      // 15
                               &cal_circuit_task,      // 16
                               &cal_2_point_task,      // 17
                               &cal_lic_task,          // 18
                               &ac_input_task,         // 19
                               &ptt_task,              // 20
                               &cal_battery_task,      // 21
                               &blue_charge_pump_task, // 22
                               &clean_air_value_task,  // 23
                               &cal_doe_task,          // 24
                               &vboost_task,           // 25
                               &cal_inline_smoke_task, // 26
                               &expression_task};      // 27

void initialize_tasks(bool const soft_reset)
{
    for (size_t i = 0; i < sizeof(task_list) / sizeof(task_list[0]); ++i)
    {
        task_list[i](msg_init, soft_reset);
    }
}
