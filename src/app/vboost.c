//******************************************************************
/*! \file vboost.h
 *
 * \brief Kidde: Mongoose-fw - Implement vboost functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "vboost.h"
#include "drv_vboost.h"

void vboost_task(message_id const message, uint16_t const data)
{
    vboost_type const type = (vboost_type)data;

    switch (message)
    {
    case msg_init:
        vboost_init();
        break;
    case msg_vboost_on:
        if (type == vboost_horn)
        {
            vboost_horn_on();
        }
        else if (type == vboost_interconnect)
        {
            vboost_interconnect_on();
        }
        break;
    case msg_vboost_off:
        if (type == vboost_horn)
        {
            vboost_horn_off();
        }
        else if (type == vboost_interconnect)
        {
            vboost_interconnect_off();
        }
        break;
    default:
        break;
    }
}
