//******************************************************************
/*! \file cal_0_ppm.c
 *
 * \brief Kidde: Mongoose-fw - Impliment co cal 0 ppm functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "cal_0_ppm.h"
#include "app_database.h"
#include "app_serial.h"
#include "co_common.h"
#include "message.h"

//*********************************************************************
/**
 * \note Timing according to Ken Fasen
 *          -# 1. Wait 9 minutes for charge to dissipate
 *          -# 2. 50 samples over 30 seconds
 */
//*********************************************************************

//*********************************************************************
/**
 * \brief 30 second window to get sample data
 */
//*********************************************************************
#define SAMPLE_PERIOD     (30 * 1000) // 30 seconds

//*********************************************************************
/**
 * \brief THe number of samples collected during the sample period
 */
//*********************************************************************
#define SAMPLE_COUNT      50

//*********************************************************************
/**
 * \brief A fraction of the sample period / the sample count total
 */
//*********************************************************************
#define SAMPLE_INTERVAL   (SAMPLE_PERIOD / SAMPLE_COUNT)

//*********************************************************************
/**
 * \brief The average number of samples for a sample interval
 */
//*********************************************************************
#define AVERAGING_SAMPLES 4

//*********************************************************************
/**
 * \brief Lower the delta offset minimum value
 * \note //Ken says this should be -3 to +5
 */
//*********************************************************************
// Ken says this should be -3 to +5
#define OFFSET_DELTA_MIN  3

//*********************************************************************
/**
 * \brief Increase the delta offset maximum value
 * \note //Ken says this should be -3 to +5
 */
//*********************************************************************
#define OFFSET_DELTA_MAX  5

//*********************************************************************
/**
 * \brief delay before 0 ppm cal starts - 9 minutes
 */
//*********************************************************************
#define O_PPM_DELAY       (uint32_t)(ONE_MINUTE * 9)

//*********************************************************************
/**
 * \brief Average counts collected
 */
//*********************************************************************
static uint16_t average_counts;

void cal_0_ppm_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        break;
    case msg_cal_begin:
        schedule_message(O_PPM_DELAY, task_cal_0_ppm, msg_continue, 0xffff);
        break;
    case msg_continue:
        if (data == 0xffff)
        {
            // Take a CO measurement
            uint16_t co_counts = 0;
            if (!co_make_measurement(&co_counts))
            {
                ///\todo: Handle failure
            }
            uint16_t const co_circuit_offset = db_get_value(db_co_circuit_cal);
            char buffer[]                    = "CO Cal 0 PPM -     \r";
            uint16_to_hex(buffer + 15, co_counts);
            serial_send_string_debug(buffer, serial_enable_calibration);

            // Ken says this should be -3 to +5
            if (co_circuit_offset - OFFSET_DELTA_MIN <= co_counts && co_counts <= co_circuit_offset + OFFSET_DELTA_MAX)
            {
                average_counts = co_counts;
                schedule_message(SAMPLE_INTERVAL, task_cal_0_ppm, msg_continue, SAMPLE_COUNT);
            }
            else
            {
                // Measurement is outside of offset range
                schedule_message(0, task_state, msg_cal_fail, task_cal_0_ppm);
            }
        }
        else if (data)
        {
            // Take a CO measurement
            uint16_t co_counts = 0;
            if (!co_make_measurement(&co_counts))
            {
                ///\todo: Handle failure
            }
            average_counts = ((AVERAGING_SAMPLES - 1) * average_counts + co_counts) / AVERAGING_SAMPLES;
            char buffer[]  = "CO Cal 0 PPM -     \r";
            uint16_to_hex(buffer + 15, average_counts);
            serial_send_string_debug(buffer, serial_enable_calibration);

            schedule_message(SAMPLE_INTERVAL, task_cal_0_ppm, msg_continue, data - 1);
        }
        else if (CO_0PPM_MIN_OFFSET <= average_counts && average_counts <= CO_0PPM_MAX_OFFSET)
        {
            // Finished sampling and average is in range
            db_set_value16(db_co_offset_msb, db_co_offset_lsb, average_counts);
            db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_0_ppm);
            schedule_message(0, task_database, msg_update_primary_db, 0);
            schedule_message(0, task_database, msg_update_backup_db, 0);

            // send Cal. Pass message
            schedule_message(0, task_state, msg_cal_pass, task_cal_0_ppm);
        }
        else
        {
            // Offset is outside of range
            serial_send_string_debug("CO Cal 0 PPM - Calibration Failed\r", serial_enable_calibration);
            schedule_message(0, task_state, msg_cal_fail, task_cal_0_ppm);
        }
        break;
    default:
        break;
    }
}
