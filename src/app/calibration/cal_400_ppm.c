//******************************************************************
/*! \file cal_400_ppm.c
 *
 * \brief Kidde: Mongoose-fw - Impliment co cal 400 ppm functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "cal_400_ppm.h"
#include "app_database.h"
#include "app_serial.h"
#include "co_common.h"
#include "drv_co.h"
#include "drv_database.h"
#include "drv_timer.h"
#include "in_co.h"
#include "message.h"
#include "task_list.h"
#include <stdlib.h>

//*********************************************************************
/**
 * \brief This is the number of samples that will be averaged during CO measurement.
 */
//*********************************************************************
#define NUM_SAMPLES_CO_AVG      (uint16_t)4

//*********************************************************************
/**
 * \brief Measure circuit if it is stabalized
 * \note 400 counts (Output is Off the Rail)
 */
//*********************************************************************
#define VMEAS_CIRCUIT_STABILIZE (uint16_t)400

// Measurement task is called every 2 seconds.  All timer functions should
// be based on this interval in case it is changed in the future.

//*********************************************************************
/**
 * \brief 30 second window to get sample data
 * \note Measurement task is called every 2 seconds.
 *      -# All timer functions should be based on this interval in case it is changed in the future.
 */
//*********************************************************************
#define SAMPLE_PERIOD           (2 * 1000)

//*********************************************************************
/**
 * \brief Period of co value accumulation
 */
//*********************************************************************
#define ACCUMULATE_PERIOD       (30 * 1000)

//*********************************************************************
/**
 * \brief Accumulated period / sample period to get the accumulated sample values
 */
//*********************************************************************
#define ACCUMULATE_SAMPLES      (ACCUMULATE_PERIOD / SAMPLE_PERIOD)

//*********************************************************************
/**
 * \note UL Alarm Definitions
 */
//*********************************************************************

//*********************************************************************
/**
 * \brief Maximum allowed stabalize time
 */
//*********************************************************************
#define MAX_STABILIZE_TIME_SEC  (45u * 1000)

//*********************************************************************
/**
 * \brief Max stabalize count based on Max stabalize time / Sample period
 */
//*********************************************************************
#define MAX_STABILIZE_COUNT     (MAX_STABILIZE_TIME_SEC / SAMPLE_PERIOD)

//*********************************************************************
/**
 * \brief Minimum accumulated counts based om sample period
 */
//*********************************************************************
#define MIN_ACCUMULATE_COUNT    ((uint16_t)((5ul * 60 * 1000) / SAMPLE_PERIOD))

//*********************************************************************
/**
 * \brief Maximum accumulated counts based om sample period
 */
//*********************************************************************
#define MAX_ACCUMULATE_COUNT    ((uint16_t)((15ul * 60 * 1000) / SAMPLE_PERIOD))

//*********************************************************************
/**
 * \brief Variables used for CO measurement
 */
//*********************************************************************
static uint16_t co_raw_avg_counts;

// msg_continue:
// msb -> 15
// lsb -> 0
// bit 15 - in stabilize
// bit 0-4 - stabilize counter
// bit 0-3 - accumulate counter
// bit 4-12 - total counter

//*********************************************************************
/**
 * \brief Stabalize mask shifted to bit 15
 */
//*********************************************************************
#define STABILIZE_MASK                   (1u << 15) ///< bit 15

//*********************************************************************
/**
 * \brief Check to see if the bit is set for the stabalize mask
 */
//*********************************************************************
#define STABILIZE(x)                     ((bool)((x)&STABILIZE_MASK)) ///< is the bit set?

//*********************************************************************
/**
 * \brief Value for the stabalize counter mask to apply over the stabalize counter
 */
//*********************************************************************
#define STABILIZE_COUNTER_MASK           0x001f

//*********************************************************************
/**
 * \brief Masking the stabalize counter
 */
//*********************************************************************
#define STABILIZE_COUNTER(x)             ((x)&STABILIZE_COUNTER_MASK)

//*********************************************************************
/**
 * \brief Value for the accumulate counter mask to apply over the accumulate counter
 */
//*********************************************************************
#define ACCUMULATE_COUNTER_MASK          0x000f

//*********************************************************************
/**
 * \brief Masking the accumulate counter
 */
//*********************************************************************
#define ACCUMULATE_COUNTER(x)            ((x)&ACCUMULATE_COUNTER_MASK)

//*********************************************************************
/**
 * \brief Value for the total counter mask to apply over the total counter
 */
//*********************************************************************
#define TOTAL_COUNTER_MASK               0x1ff0

//*********************************************************************
/**
 * \brief Masking the total counter then bitshifting right 4 bits
 */
//*********************************************************************
#define TOTAL_COUNTER(x)                 (((x)&TOTAL_COUNTER_MASK) >> 4) ///< mask then shift right 4

//*********************************************************************
/**
 * \brief Stabalize over a series of data in a given sample window
 * \param counter The number of the counts needed to stabalize across
 */
//*********************************************************************
#define STABILIZE_CONTINUE_DATA(counter) (STABILIZE_MASK | (counter))

//*********************************************************************
/**
 * \brief Sample both accumulated counter and re-shifted left total counter data.
 * \param acc_cnt Accumulated counter compared to accumulated counter mask
 * \param ttl_cnt Total counter compared to total counter mask and left shifted 4
 */
//*********************************************************************
#define SAMPLE_CONTINUE_DATA(acc_cnt, ttl_cnt)                                                                         \
    ((((ttl_cnt) << 4) & TOTAL_COUNTER_MASK) | ((acc_cnt)&ACCUMULATE_COUNTER_MASK))

void cal_400_ppm_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        break;
    case msg_cal_begin:
        schedule_message(SAMPLE_PERIOD, task_cal_400_ppm, msg_continue, STABILIZE_CONTINUE_DATA(MAX_STABILIZE_COUNT));
        break;
    case msg_continue:
        if (STABILIZE(data))
        {
            // Take a CO measurement
            uint16_t measurement = 0;
            if (!co_make_measurement(&measurement))
            {
                ///\todo: Handle failure
            }
            uint8_t const counter = STABILIZE_COUNTER(data);
            if (VMEAS_CIRCUIT_STABILIZE > measurement || !counter)
            {
                // Either we have stabilized or
                // the device likely powered up in 400 PPM air
                schedule_message(SAMPLE_PERIOD, task_cal_400_ppm, msg_continue,
                                 SAMPLE_CONTINUE_DATA(ACCUMULATE_SAMPLES, 0));
            }
            else
            {
                schedule_message(SAMPLE_PERIOD, task_cal_400_ppm, msg_continue, STABILIZE_CONTINUE_DATA(counter - 1));
            }
        }
        else
        {
            uint16_t const total_count     = TOTAL_COUNTER(data) + 1;
            uint8_t const accumulate_count = ACCUMULATE_COUNTER(data) - 1;

            // Take a CO measurement
            uint16_t co_raw_counts         = 0;
            if (!co_make_measurement(&co_raw_counts))
            {
                ///\todo: Handle failure
            }

            // calculate average
            co_raw_avg_counts = (uint16_t)((((uint32_t)co_raw_avg_counts * (NUM_SAMPLES_CO_AVG - 1)) + co_raw_counts) /
                                           NUM_SAMPLES_CO_AVG);

            // calculate if short is detected
            co_short_detect(co_raw_counts);

            char buffer[] = "CO Cal 400 PPM -     \r";
            uint16_to_hex(buffer + 17, co_raw_avg_counts);
            serial_send_string_debug(buffer, serial_enable_calibration);

            // calculate PPM from raw counts
            co_calc_PPM(co_raw_avg_counts);

            if (!accumulate_count)
            {
                if (co_alarm())
                {
                    if (total_count > MIN_ACCUMULATE_COUNT)
                    {
                        // Cal passed
                        // Update system data to reflect calibration status.
                        db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_400_ppm);

                        schedule_message(0, task_database, msg_update_primary_db, 0);
                        schedule_message(0, task_database, msg_update_backup_db, 0);

                        // send cal pass and stop
                        schedule_message(0, task_state, msg_cal_pass, task_cal_400_ppm);
                    }
                    else
                    {
                        // alarm happened too soon
                        schedule_message(0, task_state, msg_cal_fail, task_cal_400_ppm);
                    }
                }
                else if (total_count < MAX_ACCUMULATE_COUNT)
                {
                    schedule_message(SAMPLE_PERIOD, task_cal_400_ppm, msg_continue,
                                     SAMPLE_CONTINUE_DATA(ACCUMULATE_SAMPLES, total_count));
                }
                else
                {
                    // alarm did not occur during the time allowed
                    serial_send_string_debug("CO Cal 400 PPM - Calibration Failed\r", serial_enable_calibration);
                    schedule_message(0, task_state, msg_cal_fail, task_cal_400_ppm);
                }
            }
            else
            {
                schedule_message(SAMPLE_PERIOD, task_cal_400_ppm, msg_continue,
                                 SAMPLE_CONTINUE_DATA(accumulate_count, total_count));
            }
        }
        break;
    default:
        break;
    }
}
