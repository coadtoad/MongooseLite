//******************************************************************
/*! \file no_cal_hd2.c
 *
 * \brief Kidde: Mongoose-fw - Implement hd2 calibration functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "cal_hd2.h"
#include "drv_events.h"

void cal_hd2_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        break;
    default:
    {
        serial_send_string_debug("ERROR - no_cal_hd2\r", serial_enable_debug);
        schedule_message(0, task_state, msg_fault_fatal, event_fault_fatal_no_cal);
        break;
    }
    }
}
