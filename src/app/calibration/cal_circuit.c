//******************************************************************
/*! \file cal_circuit.c
 *
 * \brief Kidde: Mongoose-fw - Implement battery calibration functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "cal_circuit.h"
#include "app_database.h"
#include "app_serial.h"
#include "cal_0_ppm.h"
#include "co_common.h"
#include "message.h"

//*********************************************************************
/**
 * \brief Delay value for sampling CO curcuit
 */
//*********************************************************************
#define SAMPLING_DELAY    1000

//*********************************************************************
/**
 * \brief Sampleing interval to calibrate the CO circuit
 */
//*********************************************************************
#define SAMPLING_INTERVAL 250

//*********************************************************************
/**
 * \brief The number of samples to get in the sampling interval
 */
//*********************************************************************
#define SAMPLE_COUNT      4

void cal_circuit_task(message_id const message, uint16_t const data)
{
    static uint16_t accum;
    switch (message)
    {
    case msg_init:
        break;
    case msg_cal_begin:
        accum = 0;
        // Ken Fasen recommended 1 second before measuring
        schedule_message(SAMPLING_DELAY, task_cal_circuit, msg_continue, SAMPLE_COUNT);
        break;
    case msg_continue:
        if (data)
        {
            // Take a CO measurement
            uint16_t measurement = 0;
            if (!co_make_measurement(&measurement))
            {
                ///\todo: Handle failure
            }
            accum += measurement;
            serial_send_int_ex_debug("CO Circuit measure: ", measurement, 10, "\r", serial_enable_calibration);
            schedule_message(SAMPLING_INTERVAL, task_cal_circuit, msg_continue, data - 1);
        }
        else
        {
            uint8_t const average = accum / SAMPLE_COUNT;
            serial_send_int_ex_debug("CO Circuit average: ", average, 10, "\r", serial_enable_calibration);
            serial_send_int_ex_debug("CO Circuit min: ", CO_CKT_MIN_OFFSET, 10, "\r", serial_enable_calibration);
            serial_send_int_ex_debug("CO Circuit max: ", CO_CKT_MAX_OFFSET, 10, "\r", serial_enable_calibration);
            if (CO_CKT_MIN_OFFSET <= average && average <= CO_CKT_MAX_OFFSET)
            {
                // Circuit offset is within range.  Save the value.
                // Save lowest 8 bits since we care about precision, not range.
                db_set_value(db_co_circuit_cal, average);
                db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_co_circuit);
                schedule_message(0, task_database, msg_update_primary_db, 0);
                schedule_message(0, task_database, msg_update_backup_db, 0);

                // send Cal. Pass message
                schedule_message(0, task_state, msg_cal_pass, task_cal_circuit);
            }
            else
            {
                // Circuit offset is out of range,
                // send cal fail message
                schedule_message(0, task_state, msg_cal_fail, task_cal_circuit);
            }
        }
        break;
    default:
        break;
    }
}
