//******************************************************************
/*! \file cal_inline_mwma.c
 *
 * \brief Kidde: Mongoose-fw - Implement inline calibration functions
 *
 * \author Stan Burnette
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "cal_2_point.h"
#include "database.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_mwma.h"
#include "state.h"
#include "stdlib.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief State list of devce in inline cal enum
 */
//*********************************************************************
typedef enum
{
    low_current,
    high_current,
    hd_clean_air,
    blue_supervision
} state;

//*********************************************************************
/**
 * \brief ILC state list enum
 */
//*********************************************************************
typedef enum
{
    ILC_WAIT_DELTA,
    ILC_WAIT_STABILIZE,
    ILC_RATIO_AVERAGE,
    ILC_RATIO_VERIFY,
    ILC_POST_DWELL,
} ilc_state;

//*********************************************************************
/**
 * \brief filter info for LED filters
 */
//*********************************************************************
typedef struct
{
    uint16_t pre_a;
    uint16_t pre_b;
    uint16_t x0;
} filter_info;

//*********************************************************************
/**
 * \brief High limit of ratio between IR F and IR B
 */
//*********************************************************************
#define IRF_IRB_RATIO_FACTOR_HI_LIMIT   0x00FA // 2.50

//*********************************************************************
/**
 * \brief Low limit of ratio between IR F and IR B
 */
//*********************************************************************
#define IRF_IRB_RATIO_FACTOR_LO_LIMIT   0x0028 // .50

//*********************************************************************
/**
 * \brief High limit of ratio between BL F and IR B
 */
//*********************************************************************
#define BLF_IRB_RATIO_FACTOR_HI_LIMIT   0x00FA // 2.50

//*********************************************************************
/**
 * \brief Low limit of ratio between BL F and IR B
 */
//*********************************************************************
#define BLF_IRB_RATIO_FACTOR_LO_LIMIT   0x0028 // .50

//*********************************************************************
/**
 * \brief Adjusted high limit of ratio between IR F and IR B
 */
//*********************************************************************
#define ADJUSTED_IRF_IRB_RATIO_HI_LIMIT 0x00FA // 2.50

//*********************************************************************
/**
 * \brief Adjusted low limit of ratio between IR F and IR B
 */
//*********************************************************************
#define ADJUSTED_IRF_IRB_RATIO_LO_LIMIT 0x0032 // .50

//*********************************************************************
/**
 * \brief Adjusted high limit of ratio between BL F and IR B
 */
//*********************************************************************
#define ADJUSTED_BLF_IRB_RATIO_HI_LIMIT 0x00FA // 2.50

//*********************************************************************
/**
 * \brief Adjusted low limit of ratio between BL F and IR B
 */
//*********************************************************************
#define ADJUSTED_BLF_IRB_RATIO_LO_LIMIT 0x0032 // .50

//*********************************************************************
/**
 * \brief Default ratio factor for photo chamber
 */
//*********************************************************************
#define PHOTO_RATIO_FACTOR_DEFAULT      (uint8_t)100

//*********************************************************************
/**
 * \brief Calculation and measurment related globals.
 */
//*********************************************************************
static filter_info ir_f_filters = {0, 0, 0};

//*********************************************************************
/**
 * \brief Calculation and measurment related globals.
 */
//*********************************************************************
static filter_info ir_b_filters = {0, 0, 0};

//*********************************************************************
/**
 * \brief Calculation and measurment related globals.
 */
//*********************************************************************
static filter_info bl_f_filters = {0, 0, 0};

//*********************************************************************
/**
 * \brief Photo cal sample that is taken as a DB value
 */
//*********************************************************************
static uint8_t photo_cal_sample;

//*********************************************************************
/**
 * \brief What state to avoid running
 */
//*********************************************************************
static bool avoid        = false;

//*********************************************************************
/**
 * \brief What state needs to be run
 */
//*********************************************************************
static bool run          = false;

//*********************************************************************
/**
 * \brief DO a soft reset if criteria is met
 */
//*********************************************************************
static bool do_reset     = false;

//*********************************************************************
/**
 * \brief Initialize filters if they are not currently set
 */
//*********************************************************************
static bool init_filters = true;

//*********************************************************************
/**
 * \brief Average of ratio between IR F and IR B
 */
//*********************************************************************
static uint16_t irf_irb_ratio_avg;

//*********************************************************************
/**
 * \brief Average of ratio between BL F and IR B
 */
//*********************************************************************
static uint16_t blf_irb_ratio_avg;

//*********************************************************************
/**
 * \brief Ratio between IR F and IR B
 */
//*********************************************************************
static uint16_t ir_f_ir_b_ratio;

//*********************************************************************
/**
 * \brief Ratio between BL F and IR B
 */
//*********************************************************************
static uint16_t bl_f_ir_b_ratio;

//*********************************************************************
/**
 * \brief Light value for IR F
 */
//*********************************************************************
static uint8_t light_ir_f;

//*********************************************************************
/**
 * \brief Light value for BL F
 */
//*********************************************************************
static int8_t light_bl_f;

//*********************************************************************
/**
 * \brief Light value for IR B
 */
//*********************************************************************
static uint8_t light_ir_b;

//*********************************************************************
/**
 * \brief IR F delta value
 */
//*********************************************************************
static uint8_t ir_f_delta;

//*********************************************************************
/**
 * \brief state of photo inline calibration
 */
//*********************************************************************
static uint8_t photo_inline_cal_state = ILC_WAIT_DELTA;

//*********************************************************************
/**
 * \brief The filters are updated
 *
 *\note This update is solely based on the filter update equations:
 *          -# pre_a : a[t] = (RawADC[t] + (3*A[t-1]))/4
 *          -# pre_b : b[t] = (RawADC[t] + B[t-1])/2
 *          -# Filter X0 : X0[t] = 2*a[t] - b[t]
 *
 * PRE-CONDITION: filter info is pointing at the filter to update  <br>
 *
 * POST-CONDITION:the filters are updated
 *
 * @see reset_program.asm
 */
//*********************************************************************
static void update_filters(uint8_t const value, filter_info *const filters)
{
    // Expand to 24.8 fixed point
    uint32_t const raw            = ((uint16_t)value) << 8;
    uint32_t const previous_pre_a = filters->pre_a;
    uint32_t const previous_pre_b = filters->pre_b;

    // Calculate prefilter A
    uint32_t const pre_a          = (raw + 3 * previous_pre_a) >> 2;
    filters->pre_a                = pre_a;

    // Calculate prefilter B
    uint32_t const pre_b          = (raw + previous_pre_b) >> 1;
    filters->pre_b                = pre_b;

    // Calculate filter X0
    if ((2 * pre_a) > pre_b)
    {
        filters->x0 = 2 * pre_a - pre_b;
    }
    else
    {
        filters->x0 = 0;
    }

    ///< \todo Store historical IR-F X0 max/min values
}

//*********************************************************************
/**
 * \brief Begins a photo read for inline cal
 */
//*********************************************************************
void cal_inline_read_photo(void)
{
    // Begin reading photo chamber
    if (!begin_photo_read())
    {
        return;
    }

    // wait microseconds for the circuit to stabilize
    timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

    // take a dark measurement
    uint8_t comp_f;
    uint8_t const dark_f = read_dark(IR_F, &comp_f);

    // take a light measurement
    light_ir_f           = read_light(IR_F, comp_f, db_get_value(db_current_ir_f));
    led_off();

    // take a blue led forward measurement
    light_bl_f = read_light(BL_F, comp_f, db_get_value(db_current_bl_f));
    led_off();

    uint8_t comp_b;
    // take dark and light ir back measurements
    uint8_t const dark_b = read_dark(IR_B, &comp_b);
    light_ir_b           = read_light(IR_B, comp_b, db_get_value(db_current_ir_b));
    led_off();

    // photo power, the IR DAC, and the CAV DAC are all disabled
    end_photo_read();

    // Update filters

    if (init_filters)
    {
        init_filters       = false;

        ir_f_filters.pre_a = (uint16_t)(light_ir_f << 8);
        ir_f_filters.pre_b = (uint16_t)(light_ir_f << 8);
        ir_f_filters.x0    = (uint16_t)(light_ir_f << 8);

        ir_b_filters.pre_a = (uint16_t)(light_ir_b << 8);
        ir_b_filters.pre_b = (uint16_t)(light_ir_b << 8);
        ir_b_filters.x0    = (uint16_t)(light_ir_b << 8);

        bl_f_filters.pre_a = (uint16_t)(light_bl_f << 8);
        bl_f_filters.pre_b = (uint16_t)(light_bl_f << 8);
        bl_f_filters.x0    = (uint16_t)(light_bl_f << 8);
    }

    update_filters(light_ir_f, &ir_f_filters);
    update_filters(light_ir_b, &ir_b_filters);
    update_filters(light_bl_f, &bl_f_filters);

    // if (do_filter_update)
    // { //every other pass through analysis mode, update the filter chain
    //     ++filter_index;
    // }

    // Calculate deltas for ir_f, ir_b, and bl_f
    uint8_t const ir_f_clean_air_count = db_get_value(db_ir_f_clean_air);
    ir_f_delta                         = 1;
    if ((uint8_t)(ir_f_filters.x0 >> 8) > ir_f_clean_air_count)
    {
        ir_f_delta = (uint8_t)(ir_f_filters.x0 >> 8) - ir_f_clean_air_count;
    }

    uint8_t const ir_b_clean_air_count = db_get_value(db_ir_b_clean_air);
    uint8_t ir_b_delta                 = 1;
    if ((uint8_t)(ir_b_filters.x0 >> 8) > ir_b_clean_air_count)
    {
        ir_b_delta = (uint8_t)(ir_b_filters.x0 >> 8) - ir_b_clean_air_count;
    }

    uint8_t const bl_f_clean_air_count = db_get_value(db_blue_clean_air);
    uint8_t bl_f_delta                 = 1;
    if ((uint8_t)(bl_f_filters.x0 >> 8) > bl_f_clean_air_count)
    {
        bl_f_delta = (uint8_t)(bl_f_filters.x0 >> 8) - bl_f_clean_air_count;
    }

    ir_f_ir_b_ratio       = ir_f_delta * 100 / ir_b_delta;
    bl_f_ir_b_ratio       = bl_f_delta * 100 / ir_b_delta;

    uint16_t serial_flags = db_get_value16(db_serial_enable_msb, db_serial_enable_lsb);
    if (serial_flags & serial_enable_edwards_mode)
    {
        char buffer[97] = {0};
        uint8_to_hex(buffer + 0, light_ir_f); ///< 1	Photo.RawADC_S2[PHOTO_IR_FW]
        buffer[2] = ' ';
        uint8_to_hex(buffer + 3, light_ir_b); ///< 2	Photo.RawADC_S2[PHOTO_IR_BW]
        buffer[5] = ' ';
        uint8_to_hex(buffer + 6, light_bl_f); ///< 3	Photo.RawADC_S2[PHOTO_BLUE_FW]
        buffer[8] = ' ';
        uint8_to_hex(buffer + 9, ir_f_filters.x0 >> 8); ///< 4	Photo.X_S2[PHOTO_IR_FW][0] >> 8
        buffer[11] = ' ';
        uint8_to_hex(buffer + 12, ir_b_filters.x0 >> 8); ///< 5	Photo.X_S2[PHOTO_IR_BW][0] >> 8
        buffer[14] = ' ';
        uint8_to_hex(buffer + 15, bl_f_filters.x0 >> 8); ///< 6	Photo.X_S2[PHOTO_BLUE_FW][0] >> 8
        buffer[17] = ' ';
        uint8_to_hex(buffer + 18, 0); ///< 7	Not needed
        buffer[20] = ' ';
        uint8_to_hex(buffer + 21, 0); ///< 8    Not needed
        buffer[23] = ' ';
        uint8_to_hex(buffer + 24, 0); ///< 9    Not needed
        buffer[26] = ' ';
        uint8_to_hex(buffer + 27, dark_f); ///< 10	Photo.AmbientLight[PHOTO_IR_FW]
        buffer[29] = ' ';
        uint8_to_hex(buffer + 30, dark_b); ///< 11	Photo.AmbientLight[PHOTO_IR_BW]
        buffer[32] = ' ';
        uint8_to_hex(buffer + 33, ir_f_ir_b_ratio >> 8); ///< 12	IrF_IrB_ratio >> 8
        buffer[35] = ' ';
        uint8_to_hex(buffer + 36, ir_f_ir_b_ratio); ///< 13	IrF_IrB_ratio & 0xFF
        buffer[38] = ' ';
        uint8_to_hex(buffer + 39, bl_f_ir_b_ratio >> 8); ///< 14	BlF_IrB_ratio >> 8
        buffer[41] = ' ';
        uint8_to_hex(buffer + 42, bl_f_ir_b_ratio); ///< 15	BlF_IrB_ratio & 0xFF
        buffer[44] = ' ';
        uint8_to_hex(buffer + 45, 0); ///< 16	Not needed
        buffer[47] = ' ';
        uint8_to_hex(buffer + 48, 0); ///< 17	BlF_IrF_ratio & 0xFF
        buffer[50] = ' ';
        uint8_to_hex(buffer + 51, 0); ///< 18	MW_Flage.Byte
        buffer[53] = ' ';
        uint8_to_hex(buffer + 54, 0); ///< 19	Photo_Status.Word >> 8
        buffer[56] = ' ';
        uint8_to_hex(buffer + 57, ir_f_clean_air_count); ///< 20
        buffer[59] = ' ';
        uint8_to_hex(buffer + 60, 0); ///< 21
        buffer[62] = ' ';
        uint8_to_hex(buffer + 63, ir_b_clean_air_count); ///< 22
        buffer[65] = ' ';
        uint8_to_hex(buffer + 66, bl_f_clean_air_count); ///< 23
        buffer[68] = ' ';
        uint8_to_hex(buffer + 69, comp_f); ///< 24	IR_Fwd_offset
        buffer[71] = ' ';
        uint8_to_hex(buffer + 72, comp_b); ///< 25	IR_Back_offset
        buffer[74] = ' ';
        uint8_to_hex(buffer + 75, 0); ///< 26
        buffer[77] = ' ';
        uint8_to_hex(buffer + 78, ir_f_delta); ///< 27	(uint8_t)IrF_change_S2
        buffer[80] = ' ';
        uint8_to_hex(buffer + 81, ir_b_delta); ///< 28	(uint8_t)IrB_change_S2
        buffer[83] = ' ';
        uint8_to_hex(buffer + 84, bl_f_delta); ///< 29	(uint8_t)BlF_change_S2
        buffer[86] = ' ';
        uint8_to_hex(buffer + 87, 0); ///< 30	Photo.FilterCounter >> 8
        buffer[89] = ' ';
        uint8_to_hex(buffer + 90, 0); ///< 31	Photo.FilterCounter & 0xFF
        buffer[92] = ' ';
        buffer[93] = ' ';
        buffer[94] = 'C';
        buffer[95] = '\r';
        serial_send_string(buffer);
    }
}

void cal_inline_smoke_task(message_id const message, uint16_t const data)
{
    // static state current_state;
    switch (message)
    {
    case msg_init:
        photo_cal_sample = 0;
        schedule_message(0, task_key_value_store, msg_add_callback,
                         ((uint16_t)task_cal_inline_smoke << 8) | key_avoidance_flags);
        break;
    case msg_cal_begin:
        // Immediately run msg_continue with the first state
        schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_inline_smoke);

        schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_wait);

        break;
    case msg_continue:
        if (avoid)
        {
            run = true;
        }
        else
        {
            schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_inline_smoke);
        }
        break;
    case msg_value:
        if ((key_name)data == key_avoidance_flags)
        {
            avoid = data >> 8;
            if (!avoid && run)
            {
                schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_inline_smoke);
            }
        }
        break;
    case msg_ready:
        if ((task_id)data == task_blue_charge_pump)
        {
            if (avoid)
            {
                run = true;
                break;
            }
            run = false;

            cal_inline_read_photo();

            switch (photo_inline_cal_state)
            {
            case ILC_WAIT_DELTA:
                // Wait in this state until the IRF delta goes above the threshold.
                if (ir_f_delta > db_get_value(inline_start_irf_delta_th))
                {
                    // Go to stabilization state.
                    photo_inline_cal_state = ILC_WAIT_STABILIZE;

                    // Initialize the sample counter.
                    photo_cal_sample       = 0;

                    // Make sure that the ratio factors are set to 1.00
                    db_set_value(irf_irb_ratio_factor, PHOTO_RATIO_FACTOR_DEFAULT);
                    db_set_value(blf_irb_ratio_factor, PHOTO_RATIO_FACTOR_DEFAULT);

                    serial_send_string("Starting Stabilization...\r");

                    schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_stabilize);
                }
                break;

            case ILC_WAIT_STABILIZE:
                // Wait for the stabilization counter to time out.
                if (++photo_cal_sample >= db_get_value(inline_ratio_stabilization_time))
                {
                    photo_cal_sample       = 0;
                    photo_inline_cal_state = ILC_RATIO_AVERAGE;

                    // Init the averages so we're not starting from zero.
                    irf_irb_ratio_avg      = ir_f_ir_b_ratio;
                    blf_irb_ratio_avg      = bl_f_ir_b_ratio;
                    serial_send_string("Starting Averaging...\r");

                    schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_average);
                }
                break;

            case ILC_RATIO_AVERAGE:
                // Note:    Ratio1 = IRF/IRB
                //          Ratio2 = BLF/IRB
                if (++photo_cal_sample < db_get_value(inline_ratio_sample_count))
                {
                    // Average the ratios calculated.
                    irf_irb_ratio_avg = ((uint32_t)irf_irb_ratio_avg * 3 + ir_f_ir_b_ratio) / 4;
                    blf_irb_ratio_avg = ((uint32_t)blf_irb_ratio_avg * 3 + bl_f_ir_b_ratio) / 4;
                }
                else
                {
                    // All samples have been averaged.  Process them.
                    // First calculate the ratio percentage
                    // ratio percentage = target/ratio
                    uint16_t ratio1_percentage =
                        ((uint32_t)db_get_value(irf_irb_ratio_target_lsb) * 100) / irf_irb_ratio_avg;
                    uint16_t ratio2_percentage =
                        ((uint32_t)db_get_value(blf_irb_ratio_target_lsb) * 100) / blf_irb_ratio_avg;

                    serial_send_int_ex("R1 Percent: ", ratio1_percentage, 10, "\r");
                    serial_send_int_ex("R2 Percent: ", ratio2_percentage, 10, "\r");

                    // Calculate ratio adjustment factor
                    // adjustment factor = K1 * percentage + K2
                    uint16_t ratio1_adj_factor =
                        ((uint32_t)db_get_value(irf_irb_factor_equation_k1) * ratio1_percentage) / 100 +
                        db_get_value(irf_irb_factor_equation_k2);
                    uint16_t ratio2_adj_factor =
                        ((uint32_t)db_get_value(blf_irb_factor_equation_k3) * ratio2_percentage) / 100 +
                        db_get_value(blf_irb_factor_equation_k4);

                    serial_send_int_ex("R1 Adj Fac: ", ratio1_adj_factor, 10, "\r");
                    serial_send_int_ex("R2 Adj Fac: ", ratio2_adj_factor, 10, "\r");

                    // Check against limits.
                    if (ratio1_adj_factor < IRF_IRB_RATIO_FACTOR_HI_LIMIT &&
                        ratio1_adj_factor > IRF_IRB_RATIO_FACTOR_LO_LIMIT)
                    {
                        // Ratio1 Adjustment Factor is within limits.  Check Ratio 2.
                        if (ratio2_adj_factor < BLF_IRB_RATIO_FACTOR_HI_LIMIT &&
                            ratio2_adj_factor > BLF_IRB_RATIO_FACTOR_LO_LIMIT)
                        {
                            // Both factors are within limits, save factors to db and
                            // go to verify state.
                            db_set_value(irf_irb_ratio_factor, ratio1_adj_factor);
                            db_set_value(blf_irb_ratio_factor, ratio2_adj_factor);
                            photo_inline_cal_state = ILC_RATIO_VERIFY;
                            photo_cal_sample       = 0;
                            schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_verify);

                            serial_send_string("ILC Averaging Pass, starting Verify\r");
                        }
                        else
                        {
                            // Ratio2 Factor out of limits.  Fail.
                            photo_inline_cal_state = ILC_POST_DWELL;
                            serial_send_string("R2 avg out of limits, Fail\r");
                            schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_error);
                        }
                    }
                    else
                    {
                        // Ratio1 factor out of limits
                        photo_inline_cal_state = ILC_POST_DWELL;
                        serial_send_string("R1 avg out of limits, Fail\r");
                        schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_error);
                    }
                }
                break;

            case ILC_RATIO_VERIFY:
                // If this is the first sample of the verify step, just init the
                // averages.
                if (0 == photo_cal_sample)
                {
                    ++photo_cal_sample;
                    irf_irb_ratio_avg = ir_f_ir_b_ratio;
                    blf_irb_ratio_avg = bl_f_ir_b_ratio;
                }
                else if (++photo_cal_sample > db_get_value(inline_ratio_sample_count))
                {
                    // Average the ratios now that the adjustment factor has been
                    // applied.
                    serial_send_int_ex("R1 Verify: ", irf_irb_ratio_avg, 10, "\r");
                    serial_send_int_ex("R2 Verify: ", blf_irb_ratio_avg, 10, "\r");

                    // Sampling has finished. Check against limits.
                    if (irf_irb_ratio_avg < ADJUSTED_IRF_IRB_RATIO_HI_LIMIT &&
                        irf_irb_ratio_avg > ADJUSTED_IRF_IRB_RATIO_LO_LIMIT)
                    {
                        // Ratio1 Adjustment Factor is within limits.  Check Ratio 2.
                        if (blf_irb_ratio_avg < ADJUSTED_BLF_IRB_RATIO_HI_LIMIT &&
                            blf_irb_ratio_avg > ADJUSTED_BLF_IRB_RATIO_LO_LIMIT)
                        {
                            // Both ratios are within limits, save ratios to db and
                            photo_inline_cal_state = ILC_POST_DWELL;

                            // Update Status Bit Position
                            db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_inline_smoke);

                            // Calibration is complete, update db.
                            schedule_message(0, task_database, msg_update_primary_db, 0);
                            schedule_message(0, task_database, msg_update_backup_db, 0);
                            serial_send_string("Verify Pass...Soft Reset\r");
                            do_reset = true;
                        }
                        else
                        {
                            // Ratio2 out of limits.  Fail.
                            photo_inline_cal_state = ILC_POST_DWELL;
                            serial_send_string("R2 verify out of limits, Fail\r");
                            schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_error);
                        }
                    }
                    else
                    {
                        // Ratio1 out of limits
                        photo_inline_cal_state = ILC_POST_DWELL;
                        serial_send_string("R1 verify out of limits, Fail\r");
                        schedule_message(0, task_state, msg_inline_smoke_cal, prior_cal_inline_error);
                    }
                }
                else
                {
                    // Average the ratios calculated.  Note:  The FW is starting the
                    // average from the existing averages calculated above without the
                    // adjustment factor.  This is better than starting from zero.
                    // We can have another stabilization time before the verify state
                    // to init the averages if needed.
                    irf_irb_ratio_avg = ((uint32_t)irf_irb_ratio_avg * 3 + ir_f_ir_b_ratio) / 4;
                    blf_irb_ratio_avg = ((uint32_t)blf_irb_ratio_avg * 3 + bl_f_ir_b_ratio) / 4;
                }

                break;

            case ILC_POST_DWELL:
                // If the reset bit is set, do a soft reset.
                if (do_reset)
                {
                    // Force a soft reset after calibration is complete.  We delayed a second above
                    // but this delay will ensure that db write is complete.
                    schedule_message(1000, task_state, msg_cal_pass, task_cal_inline_smoke);
                }
                break;
            }

            schedule_message(1000, task_cal_inline_smoke, msg_continue, 0);
        }
        break;
    default:
        break;
    }
}
