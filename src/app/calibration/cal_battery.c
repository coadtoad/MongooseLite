//******************************************************************
/*! \file cal_battery.c
 *
 * \brief Kidde: Mongoose-fw - Implement battery calibration functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "cal_battery.h"
#include "app_database.h"
#include "app_serial.h"
#include "drv_battery.h"
#include "drv_vboost.h"
#include "message.h"

//*********************************************************************
/**
 * \brief BATTERY_VOLT CIRCUIT
 * \note BATTERY_VOLT circuit
 *          -# R1 = 13.3 ohms
 *          -# R2 = 6.65 ohms
 *          -# LB volt = 2550mV
 *          -# I = (Vbatt_circuit)/(R1 + R2)
 *          -# I = 2550mV/(13.3 ohms + 6.65 ohms) = 127.8 mA
 *          -# VLB = I x R2
 *          -# VLB = 127.8 mA x 6.65 ohms = 849.9 mV
 *          -# LB A/D value = (VLB)/(A/D Vref) * (max ADC counts) = expected_battery_voltage
 *          -# LB A/D value = 850mV/2500mV * 4096 = 1392 counts = 0x570
 */
//*********************************************************************

//*********************************************************************
/**
 * \brief +/-10mV calibration tolerance variable for 12-bit resolution
 * \note TOLERANCE CALCULATION
 *          -# +10mV LB volt = 2560mV
 *          -# I = 2560mV/(13.3 ohms + 6.65 ohms) = 128.3 mA
 *          -# VLB = 6.65 ohms x 128.3 mA = 853.2 mV
 *          -# LB A/D value = 853mV/2500mV * 4096 = 1397.5 counts = 0x576
 *
 *          -# +0mV LB A/D value = 0x570
 *
 *          -# -10mV LB volt = 2540mV
 *          -# I = 2540mV/(13.3 ohms + 6.65 ohms) = 127.3 mA
 *          -# VLB = 6.65 ohms x 127.3 mA = 846.5 mV
 *          -# LB A/D value = 846mV/2500mV * 4096 = 1386.0 counts = 0x56A
 */
//*********************************************************************
#define CAL_TOLERANCE 6

void cal_battery_task(message_id const message, uint16_t const data)
{
    (void)data;
    switch (message)
    {
    case msg_init:
        break;
    case msg_cal_begin:
        battery_test_on();
        // Enable the VBoost for the battery
        vboost_battery_on();
        schedule_message(BATTERY_STABILIZATION_TIME, task_battery_cal, msg_continue, 0);
        break;
    case msg_continue:
    {
        uint16_t const batt_voltage = battery_sample();
        // Disable the VBoost for the battery
        vboost_battery_off();
        uint16_t const expected_battery_voltage =
            db_get_value16(db_battery_voltage_min_msb, db_battery_voltage_min_lsb);
        serial_send_int_ex_debug("batt_voltage: ", batt_voltage, 16, "\r", serial_enable_calibration);
        serial_send_int_ex_debug("expected_battery_voltage: ", expected_battery_voltage, 16, "\r",
                                 serial_enable_calibration);
        if (expected_battery_voltage - CAL_TOLERANCE <= batt_voltage &&
            batt_voltage <= expected_battery_voltage + CAL_TOLERANCE)
        {
            db_set_value16(db_battery_voltage_min_msb, db_battery_voltage_min_lsb, batt_voltage);
            db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_battery);

            schedule_message(0, task_database, msg_update_primary_db, 0);
            schedule_message(0, task_database, msg_update_backup_db, 0);
            schedule_message(0, task_state, msg_cal_pass, task_battery_cal);
        }
        else
        {
            // If failure, repeat until passing
            schedule_message(1000, task_battery_cal, msg_cal_begin, 0);
        }
    }
    break;
    default:
        break;
    }
}
