//******************************************************************
/*! \file no_cal_battery.c
 *
 * \brief Kidde: Mongoose-fw - Implement battery calibration functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_events.h"
#include "app_serial.h"
#include "cal_battery.h"

void cal_battery_task(message_id const message, uint16_t const data)
{
    (void)data;
    switch (message)
    {
    case msg_init:
        break;
    default:
    {
        serial_send_string_debug("ERROR - no_cal_battery\r", serial_enable_debug);
        //schedule_message(0, task_state, msg_fault_fatal, event_fault_fatal_no_cal);
        break;
    }
    }
}
