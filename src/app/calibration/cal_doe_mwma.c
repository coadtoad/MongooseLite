//******************************************************************
/*! \file cal_doe_mwma.c
 *
 * \brief Kidde: Mongoose-fw - Implement doe calibration functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "cal_hd2.h"
#include "drv_photo.h"
#include "drv_serial.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_mwma.h"
#include "state.h"
#include <stdbool.h>

void cal_doe_task(message_id const message, uint16_t const data)
{
    static bool is_high = false;

    switch (message)
    {
    case msg_init:
        break;
    case msg_cal_begin:
        if (get_flash_in_progress())
        {
            schedule_message(100, task_cal_doe, msg_cal_begin, 0);
            return;
        }
        else
        {
            obliterate();
        }
    case msg_continue:
        schedule_message(1000, task_cal_doe, msg_continue, 0);
        schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_doe);
        break;
    case msg_ready:
        if ((task_id)data == task_blue_charge_pump)
        {
            uint8_t ir_fw, ir_bw, bl_fw;

            if (is_high)
            {
                ir_fw = db_get_value(db_ptt_current_ir_f);
                ir_bw = db_get_value(db_ptt_current_ir_b);
                bl_fw = db_get_value(db_ptt_current_blue);
            }
            else
            {
                ir_fw = db_get_value(db_current_ir_f);
                ir_bw = db_get_value(db_current_ir_b);
                bl_fw = db_get_value(db_current_bl_f);
            }

            is_high = !is_high;

            if (!begin_photo_read())
            {
                return;
            }
            // wait microseconds for the circuit to stabilize
            timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

            uint8_t comp_f;
            uint8_t const dark_f     = read_dark(IR_F, &comp_f);

            // Read IR-F
            uint8_t const light_ir_f = read_light(IR_F, comp_f, ir_fw);
            led_off();

            // Read BL-F
            uint8_t const light_bl_f = read_light(BL_F, comp_f, bl_fw);
            led_off();

            // Read IR-B
            uint8_t comp_b;
            uint8_t const dark_b     = read_dark(IR_B, &comp_b); // Get rid of variable to fix warning
            uint8_t const light_ir_b = read_light(IR_B, comp_b, ir_bw);

            end_photo_read();

            // Use Normal mode if smoke_status bit is not set
            // Variables that are not calculated during Normal Sampling mode
            //  are sent as zero values
            char buffer[97] = {0};
            uint8_to_hex(buffer + 0, light_ir_f); ///< 1	Photo.RawADC_S2[PHOTO_IR_FW]
            buffer[2] = ' ';
            uint8_to_hex(buffer + 3, light_ir_b); ///< 2	0
            buffer[5] = ' ';
            uint8_to_hex(buffer + 6, light_bl_f); ///< 3	0
            buffer[8] = ' ';
            uint8_to_hex(buffer + 9, 0); ///< 4	Photo.X_S2[PHOTO_IR_FW][0] >> 8
            buffer[11] = ' ';
            uint8_to_hex(buffer + 12, 0); ///< 5	0
            buffer[14] = ' ';
            uint8_to_hex(buffer + 15, 0); ///< 6	0
            buffer[17] = ' ';
            uint8_to_hex(buffer + 18, 0); ///< 7	0
            buffer[20] = ' ';
            uint8_to_hex(buffer + 21, 0); ///< 8	0
            buffer[23] = ' ';
            uint8_to_hex(buffer + 24, 0); ///< 9	0
            buffer[26] = ' ';
            uint8_to_hex(buffer + 27, dark_f); ///< 10	Photo.AmbientLight[PHOTO_IR_FW]
            buffer[29] = ' ';
            uint8_to_hex(buffer + 30, dark_b); ///< 11	0
            buffer[32] = ' ';
            uint8_to_hex(buffer + 33, 0); ///< 12	0
            buffer[35] = ' ';
            uint8_to_hex(buffer + 36, 0); ///< 13	0
            buffer[38] = ' ';
            uint8_to_hex(buffer + 39, 0); ///< 14	0
            buffer[41] = ' ';
            uint8_to_hex(buffer + 42, 0); ///< 15	0
            buffer[44] = ' ';
            uint8_to_hex(buffer + 45, 0); ///< 16	0
            buffer[47] = ' ';
            uint8_to_hex(buffer + 48, 0); ///< 17	0
            buffer[50] = ' ';
            uint8_to_hex(buffer + 51, 0); ///< 18	0
            buffer[53] = ' ';
            uint8_to_hex(buffer + 54, 0); ///< 19	0
            buffer[56] = ' ';
            uint8_to_hex(buffer + 57, 0); ///< 20	0
            buffer[59] = ' ';
            uint8_to_hex(buffer + 60, 0); ///< 21	0
            buffer[62] = ' ';
            uint8_to_hex(buffer + 63, 0); ///< 22	0
            buffer[65] = ' ';
            uint8_to_hex(buffer + 66, 0); ///< 23	0
            buffer[68] = ' ';
            uint8_to_hex(buffer + 69, comp_f); ///< 24	IR_Fwd_offset
            buffer[71] = ' ';
            uint8_to_hex(buffer + 72, 0); ///< 25	0
            buffer[74] = ' ';
            uint8_to_hex(buffer + 75, 0); ///< 26	0
            buffer[77] = ' ';
            uint8_to_hex(buffer + 78, 0); ///< 27	0
            buffer[80] = ' ';
            uint8_to_hex(buffer + 81, 0); ///< 28	0
            buffer[83] = ' ';
            uint8_to_hex(buffer + 84, 0); ///< 29	0
            buffer[86] = ' ';
            uint8_to_hex(buffer + 87, is_high + 1); ///< 30	doe_hi_low
            buffer[89] = ' ';
            uint8_to_hex(buffer + 90, 0); ///< 31	0
            buffer[92] = ' ';
            buffer[93] = ' ';
            buffer[94] = 'N';
            buffer[95] = '\r';
            serial_send_string(buffer);
        }
        break;
    default:
        break;
    }
}
