//******************************************************************
/*! \file cal_hd2_mwma.c
 *
 * \brief Kidde: Mongoose-fw - Implement hd2 calibration functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "cal_hd2.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_mwma.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief What state to avoid running
 */
//*********************************************************************
static bool avoid = false;

//*********************************************************************
/**
 * \brief What state needs to be run
 */
//*********************************************************************
static bool run   = false;

void cal_hd2_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        schedule_message(0, task_key_value_store, msg_add_callback,
                         ((uint16_t)task_cal_hd2 << 8) | key_avoidance_flags);
        break;
    case msg_cal_begin:
    case msg_continue:
        schedule_message(1000, task_cal_hd2, msg_continue, 0);
        if (avoid)
        {
            run = true;
        }
        else
        {
            schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_hd2);
        }
        break;
    case msg_value:
        if ((key_name)data == key_avoidance_flags)
        {
            avoid = data >> 8;
            if (!avoid && run)
            {
                schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_hd2);
            }
        }
        break;
    case msg_ready:
        if ((task_id)data == task_blue_charge_pump)
        {
            if (avoid)
            {
                run = true;
                break;
            }
            run = false;
            if (!begin_photo_read())
            {
                return;
            }
            // wait microseconds for the circuit to stabilize
            timer_delay_us(PHOTO_OP_AMP_STABLE_TIME);

            uint8_t comp_f;
            read_dark(IR_F, &comp_f);

            // Read IR-F
            read_light(IR_F, comp_f, db_get_value(db_current_ir_f));
            led_off();

            // Read BL-F
            read_light(BL_F, comp_f, db_get_value(db_current_bl_f));
            led_off();
            // Read IR-B

            uint8_t comp_b;
            read_dark(IR_B, &comp_b);
            read_light(IR_B, comp_b, db_get_value(db_current_ir_b));

            end_photo_read();

            // HD2 cal turns off Edwards mode output, so no need to send to DB
            // Send to key-value store so that they get dumped to serial
            /*schedule_message(0, task_key_value_store, msg_set_value,
                    ((uint16_t)light_ir_f) << 8 | key_light_ir_f);
            schedule_message(0, task_key_value_store, msg_set_value,
                    ((uint16_t)light_ir_b) << 8 | key_light_ir_b);
            schedule_message(0, task_key_value_store, msg_set_value,
                    ((uint16_t)light_bl_f) << 8 | key_light_bl_f);
            schedule_message(0, task_key_value_store, msg_set_value,
                    ((uint16_t)dark_f) << 8 | key_dark_f);
            schedule_message(0, task_key_value_store, msg_set_value,
                    ((uint16_t)dark_b) << 8 | key_dark_b);*/
        }
        break;
    default:
        break;
    }
}
