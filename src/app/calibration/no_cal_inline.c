//******************************************************************
/*! \file no_cal_inline.c
 *
 * \brief Kidde: Mongoose-fw - Implement inline calibration functions
 *
 * \author Stan Burnette
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "cal_inline.h"

void cal_inline_smoke_task(message_id const message, uint16_t const data)
{
    // No inline calibration
    (void)message;
    (void)data;
}
