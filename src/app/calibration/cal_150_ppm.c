//******************************************************************
/*! \file cal_150_ppm.c
 *
 * \brief Kidde: Mongoose-fw - Impliment co cal 0 ppm functions
 *
 * \author Valeriy M
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "cal_150_ppm.h"
#include "app_database.h"
#include "app_serial.h"
#include "co_common.h"
#include "message.h"

//*********************************************************************
/**
 * \note Timing according to Ken Fasen
 *          -# 1. Wait 9 minutes for charge to dissipate
 *          -# 2. 50 samples over 30 seconds
 */
//*********************************************************************

//*********************************************************************
/**
 * \brief 1 minute window to get sample data
 */
//*********************************************************************
#define SAMPLE_PERIOD     (60u * 1000) // 1 minute

//*********************************************************************
/**
 * \brief THe number of samples collected during the sample period
 */
//*********************************************************************
#define SAMPLE_COUNT      120

//*********************************************************************
/**
 * \brief A fraction of the sample period / the sample count total
 */
//*********************************************************************
#define SAMPLE_INTERVAL   (SAMPLE_PERIOD / SAMPLE_COUNT)

//*********************************************************************
/**
 * \brief The average number of samples for a sample interval
 */
//*********************************************************************
#define AVERAGING_SAMPLES 4

//*********************************************************************
/**
 * \brief Maximum slope value
 */
//*********************************************************************
#define MAX_SLOPE         250

//*********************************************************************
/**
 * \brief Minimum slope value
 */
//*********************************************************************
#define MIN_SLOPE         50

//*********************************************************************
/**
 * \brief delay before 160 ppm calibration - 6.5 minutes
 */
//*********************************************************************
#define CAL_160_WAIT      (uint32_t)((6 * ONE_MINUTE) + (30 * ONE_SECOND))

//*********************************************************************
/**
 * \brief Average counts collected
 */
//*********************************************************************
static uint16_t average_counts;

void cal_150_ppm_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        break;
    case msg_cal_begin:
        // start the half minute timer to countdown 6.5 minutes before starting cal
        schedule_message(CAL_160_WAIT, task_cal_150_ppm, msg_continue, 0xffff);
        break;
    case msg_continue:
        if (data == 0xffff)
        {
            // Take a CO measurement
            if (!co_make_measurement(&average_counts))
            {
                ///\todo: Handle failure
            }

            char buffer[] = "CO Cal 150 PPM -     \r";
            uint16_to_hex(buffer + 17, average_counts);
            serial_send_string_debug(buffer, serial_enable_calibration);

            schedule_message(SAMPLE_INTERVAL, task_cal_150_ppm, msg_continue, SAMPLE_COUNT);
        }
        else if (data)
        {
            // Take a CO measurement
            uint16_t co_counts = 0;
            if (!co_make_measurement(&co_counts))
            {
                ///\todo: Handle failure
            }
            average_counts = ((AVERAGING_SAMPLES - 1) * average_counts + co_counts) / AVERAGING_SAMPLES;

            char buffer[]  = "CO Cal 150 PPM -     \r";
            uint16_to_hex(buffer + 17, average_counts);
            serial_send_string_debug(buffer, serial_enable_calibration);

            schedule_message(SAMPLE_INTERVAL, task_cal_150_ppm, msg_continue, data - 1);
        }
        else
        {
            uint16_t const delta = average_counts - db_get_value16(db_co_offset_msb, db_co_offset_lsb);
            if (MIN_SLOPE <= delta && delta <= MAX_SLOPE)
            {
                // Finished sampling and delta from 0PPM is in range
                uint32_t const scale = ((150ul << 16) / delta) >> 8;

                db_set_value16(db_co_scale_msb, db_co_scale_lsb, scale);
                db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_150_ppm);
                schedule_message(0, task_database, msg_update_primary_db, 0);
                schedule_message(0, task_database, msg_update_backup_db, 0);

                // send Cal. Pass message
                schedule_message(0, task_state, msg_cal_pass, task_cal_150_ppm);
            }
            else
            {
                // Offset is outside of range
                serial_send_string_debug("CO Cal 150 PPM - Calibration Failed\r", serial_enable_calibration);
                schedule_message(0, task_state, msg_cal_fail, task_cal_150_ppm);
            }
        }
        break;
    default:
        break;
    }
}
