//******************************************************************
/*! \file cal_lic_single.c
 *
 * \brief Kidde: Mongoose-fw - Implement lic calibration functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "cal_lic.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "drv_vboost.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_single.h"
#include <stdbool.h>

void cal_lic_task(message_id const message, uint16_t const data)
{
    switch (message)
    {
    case msg_init:
        break;

    case msg_cal_begin:
    case msg_continue:
        if (data)
        {
            // Vboost has been on long enough.  Turn it off and take the sample.
            vboost_photo_off();
            uint8_t photo_reading_arr[num_photo_data];
            read_photo(db_get_value(db_current_ir_f), db_get_value(db_current_ir_b), photo_reading_arr);

            // Schedule next sample for 100 ms out.
            //schedule_message(97, task_cal_lic, msg_continue, 0);
        }
        else
        {
            vboost_photo_on();
            // Minimum of 2ms of VBST on time is needed before disabling
            //schedule_message(3, task_cal_lic, msg_continue, 1);
        }

        break;
    case msg_value:
        break;
    case msg_ready:
        break;
    default:
        break;
    }
}
