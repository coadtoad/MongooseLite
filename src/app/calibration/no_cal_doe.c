//******************************************************************
/*! \file no_cal_doe.c
 *
 * \brief Kidde: Mongoose-fw - Implement doe calibration functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "cal_doe.h"
#include "drv_events.h"

void cal_doe_task(message_id const message, uint16_t const data)
{
    (void)data;
    switch (message)
    {
    case msg_init:
        break;
    default:
        break;
    }
}
