//******************************************************************
/*! \file cal_2_point_single.c
 *
 * \brief Kidde: Mongoose-fw - Impliment 2 point cal functions on single LED devices
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_database.h"
#include "app_serial.h"
#include "cal_2_point.h"
#include "database.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "key_value_store.h"
#include "message.h"
#include "photo_common_single.h"
#include <stdbool.h>

//*********************************************************************
/**
 * \brief Count of photo cal sample
 */
//*********************************************************************
#define PHOTO_CAL_SAMPLE_CNT 0x10

//*********************************************************************
/**
 * \brief Blue supervision count
 */
//*********************************************************************
#define BLUE_SUP_COUNT       4

//*********************************************************************
/**
 * \def PHOTO_CAL_I_DELTA
 * \brief Delta for photo cal from IR B high and IR F low
 * \note // Standard IR_B Cal Current values (calibrate with these currents)
 *          since HD Calibration will set variable currents for each unit
 */
//*********************************************************************
#define PHOTO_CAL_I_DELTA    (uint16_t)(PHOTO_CAL_IR_B_HI_I - PHOTO_CAL_IR_B_LO_I)

//*********************************************************************
/**
 * \brief Minimum blue supervision time
 */
//*********************************************************************
#define BLUE_SUP_MIN         9

//*********************************************************************
/**
 * \par // These Equation constants are for default equation and only used if HD Cal is not set in Cal Status
//          The HD Cal equation is written in the dBase at flash program time (Store in 8.8 Format)
//          -# Example: Equation - 57.6 - 0.453(CAC) + 44(SLOPE)
//          -# PHOTO_CAL_K1    0x3999
//          -# PHOTO_CAL_K2    0x0073
//          -# PHOTO_CAL_K3    0x2C00
//
//          Math Results (in hex)
//          -# Term1 - 3999
//          -# Term2 - 26A2
//          -# Term3 - 580
//
//          Alarm Th (in decimal)
//          -# AT = 24
 */
//*********************************************************************

//*********************************************************************
/**
 * \brief State of devce in 2 point cal enum
 */
//*********************************************************************
typedef enum
{
    low_current,
    high_current,
    hd_clean_air,
    blue_supervision
} state;

//*********************************************************************
/**
 * \brief Photo cal sample that is taken as a DB value
 */
//*********************************************************************
static uint8_t photo_cal_sample;

//*********************************************************************
/**
 * \brief Ambient average backward
 */
//*********************************************************************
static uint8_t b_ambient_average;

//*********************************************************************
/**
 * \brief IR back average
 */
//*********************************************************************
static uint8_t ir_b_average;

//*********************************************************************
/**
 * \brief What state to avoid running
 */
//*********************************************************************
static bool avoid = false;

//*********************************************************************
/**
 * \brief What state needs to be run
 */
//*********************************************************************
static bool run   = false;

//*********************************************************************
/**
 * \brief Evaluate blue supervision samples
 */
//*********************************************************************
void blue_supervision_samples(void)
{
    ++photo_cal_sample;

    if (photo_cal_sample >= BLUE_SUP_COUNT)
    {
        // calc and exit
        photo_cal_sample = 0;
    }
    else
    {
        // try agian in a second.
        //schedule_message(1000, task_cal_2_point, msg_continue, blue_supervision);
    }
}

//*********************************************************************
/**
 * \brief Full 2 point calibration
 * \par Algorithm
 *      IR_F Alm Threshold  = K1 + K2*Clean Air Value + K3*m where
 *          -# m = (Hi ADC � Lo ADC) / (Hi Current - Lo Current)
|           -# K1, K2, K3 are stored in the data base in 8.8 format (multiply 256)
|           -# K2,K3 signs ( 0 = +, 1 = -) are store in data base, K1 is always +
            -# Alarm threshold = (K1 + K2*Clean Air Value + K3*m)
| \returns If the calibration was successful
 */
//*********************************************************************
static _Bool two_point_calibration(uint8_t const photo_cal_ir_b_adc)
{
    uint32_t temp;
    if (db_get_value(db_calibration_lsb) & cal_lic)
    {
        // Use lic CAV if has been through lic cal
        temp = (uint32_t)PHOTO_CAL_K2 * db_get_value(db_ir_b_clean_air);
    }
    else
    {
        // Use low CAV if has not been through lic cal
        temp = (uint32_t)PHOTO_CAL_K2 * db_get_value(db_ir_b_factory_clean_air);
    }
    uint16_t const term2       = ((temp > 0xffff) ? 0xffff : temp); // Saturate

    //
    // find K3*(HigH current ADC - Clean Air ADC)/203
    // (should be K3 * slope calculation)
    //
    uint8_t const ir_b_factory = db_get_value(db_ir_b_factory_clean_air);
    //
    // Slope calc. should be delta volt output / delta current drive
    // Hi Current measurement - Normal CAV Current measurement
    //
    uint8_t const delta        = ((photo_cal_ir_b_adc > ir_b_factory) ? (photo_cal_ir_b_adc - ir_b_factory) : 0);
    //
    // slope = delta measured output A/D (high-low) / delta current ma (~200ma)
    //
    uint16_t const mathb       = PHOTO_CAL_I_DELTA << 8;
    uint32_t const matha_l     = ((uint32_t)delta) << 16;
    uint16_t const slope       = matha_l / mathb;

    uint16_t term3;
    if (slope)
    {
        uint32_t const temp = ((uint32_t)PHOTO_CAL_K3 * slope) >> 8;
        term3               = (temp > 0xffff) ? 0xffff : temp;
    }
    else
    {
        term3 = 0;
    }

    //
    // Add terms  K1 + K2*Clean air + K3*slope
    //
    uint8_t const k2_sign = PHOTO_CAL_K2_SIGN;
    uint8_t const k3_sign = PHOTO_CAL_K3_SIGN;
    uint16_t const k1     = PHOTO_CAL_K1;
    if (k2_sign == POSITIVE && k3_sign == POSITIVE)
    {
        temp = (uint32_t)k1 + term2 + term3;
    }
    else if (k2_sign == NEGATIVE && k3_sign == POSITIVE)
    {
        temp = (uint32_t)k1 + term3;
        temp = (temp > term2) ? (temp - term2) : 0;
    }
    else if (k2_sign == POSITIVE && k3_sign == NEGATIVE)
    {
        temp = (uint32_t)k1 + term2;
        temp = (temp > term3) ? (temp - term3) : 0;
    }
    else if (k2_sign == NEGATIVE && k3_sign == NEGATIVE)
    {
        temp = (uint32_t)k1;
        temp = (temp > term2 + term3) ? (temp - term2 - term3) : 0;
    }
    //
    // temp is 8.8 fromat, divide by 256 to keep integer part
    //
    if (temp & 0x0080)
    {
        temp >>= 8;
        temp++; // Round Up
    }
    else
    {
        temp >>= 8;
    }

    uint8_t threshold = temp;
    // Make sure there is enough room for failsafe and threshold
    uint8_t clean_air = db_get_value(db_ir_b_factory_clean_air);
    uint16_t hush     = (255u << 8) / db_get_value16(db_hush_multiplier_msb, db_hush_multiplier_lsb);
    uint16_t failsafe = (255u << 8) / db_get_value16(db_failsafe_multiplier_msb, db_failsafe_multiplier_lsb);
    serial_send_int_ex_debug("clean air = ", clean_air, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("hush = ", hush, 16, "\r", serial_enable_calibration);
    serial_send_int_ex_debug("failsafe = ", failsafe, 16, "\r", serial_enable_calibration);

    _Bool photo_cal_trouble = db_get_value(db_ir_b_factory_clean_air) >
                                  (255u << 8) / db_get_value16(db_hush_multiplier_msb, db_hush_multiplier_lsb) ||
                              db_get_value(db_ir_b_factory_clean_air) >
                                  (255u << 8) / db_get_value16(db_failsafe_multiplier_msb, db_failsafe_multiplier_lsb);
    // Validate threshold against some limits
    if (threshold > ALM_TH_MAX)
    {
        // above Upper Limit
        threshold = 0xFF;
        serial_send_string_debug("photo_cal_trouble high\r", serial_enable_calibration);
        photo_cal_trouble = 1;
    }
    else if (threshold < ALM_TH_MIN)
    {
        // below Lower Limit
        threshold = 0x00;
        serial_send_string_debug("photo_cal_trouble low\r", serial_enable_calibration);
        photo_cal_trouble = 1;
    }

    if (!photo_cal_trouble)
    {
        db_set_value(db_photo_alarm_eqa_threshold, threshold);
    }
    return photo_cal_trouble;
}

//*********************************************************************
/**
 * \brief Evaluate low current samples
 */
//*********************************************************************
static void low_current_samples(void)
{
    state next_state = low_current;

    uint8_t photo_reading_arr[num_photo_data];
    if (!read_photo(IR_F_LO_DAC, IR_B_LO_DAC, photo_reading_arr))
    {
        return;
    }

    uint8_t const dark_b     = photo_reading_arr[dark_reading];
    uint8_t const light_ir_b = photo_reading_arr[ir_b_reading];

    if (++photo_cal_sample == 1)
    {
        // Initialize rather than averaging the first value
        b_ambient_average = dark_b;
        ir_b_average      = light_ir_b;
    }
    else
    {
        // Average of the accumulated and the new value
        b_ambient_average = (b_ambient_average + dark_b) / 2;
        ir_b_average      = (ir_b_average + light_ir_b) / 2;
    }

    uint8_t const offset_dac_cnt = OFFSET_DAC_CNT;
    uint8_t const ir_b_min       = db_get_value(db_photo_cal_ir_b_min);
    uint8_t const ir_b_max       = db_get_value(db_photo_cal_ir_b_max);
    _Bool photo_cal_trouble      = 0;
    if (PHOTO_CAL_SAMPLE_CNT <= photo_cal_sample)
    {

        // If any one of these is true, then something is wrong

        photo_cal_trouble = b_ambient_average < offset_dac_cnt - 10 || b_ambient_average > offset_dac_cnt + 10 ||
                            ir_b_average < ir_b_min || ir_b_average > ir_b_max;

        if (!photo_cal_trouble)
        {
            // We only keep these as CAV values if HD Cal is not set.
            // If HD Cal is set these will be overwritten
            //  when we collect HD calibrated CAV data.
            //
            // For *now* Save Cal CAV values in Ram copy of DBase
            db_set_value(db_ir_b_factory_clean_air, ir_b_average);

            // Also Init the CAV Quiescent Values
            db_set_value(db_ir_b_clean_air, ir_b_average);

            if (db_get_value(db_calibration_lsb) & cal_lic)
            {
                next_state = hd_clean_air;
            }
            else
            {
                next_state = high_current;
            }
            photo_cal_sample = 0;
        }
    }

    if (db_get_value16(db_serial_enable_msb, db_serial_enable_lsb) & serial_enable_calibration)
    { //         0         1         2         3         4         5
        char buffer[] = "Cal2pt Lo Current -       -          -       -          \r";
        uint8_to_hex(buffer + 20, dark_b);
        uint8_to_hex(buffer + 28, light_ir_b);
        uint8_to_hex(buffer + 39, b_ambient_average);
        uint8_to_hex(buffer + 47, ir_b_average);
        serial_send_string(buffer);
    }

    if (photo_cal_trouble)
    {
        // Calibration failed
        //schedule_message(0, task_state, msg_cal_fail, task_cal_2_point);
        serial_send_string_debug("Fail 1\r", serial_enable_calibration);
        serial_send_int_ex_debug("offset_dac_cnt - ", offset_dac_cnt, 16, "\r", serial_enable_calibration);
        serial_send_int_ex_debug("ir_b_min - ", ir_b_min, 16, "\r", serial_enable_calibration);
        serial_send_int_ex_debug("ir_b_max - ", ir_b_max, 16, "\r", serial_enable_calibration);

        serial_send_string_debug("Cal2pt Lo Current - Cal Failed\r", serial_enable_calibration);
    }
    else
    {
        //schedule_message(1000, task_cal_2_point, msg_continue, next_state);
    }
}

//*********************************************************************
/**
 * \brief Evaluate high current samples
 */
//*********************************************************************
static void high_current_samples(void)
{
    uint8_t photo_reading_arr[num_photo_data];
    if (!read_photo(IR_F_HI_DAC, IR_B_HI_DAC, photo_reading_arr))
    {
        return;
    }

    uint8_t const dark_b     = photo_reading_arr[dark_reading];
    uint8_t const light_ir_b = photo_reading_arr[ir_b_reading];

    if (++photo_cal_sample == 1)
    {
        ir_b_average = light_ir_b;
        // Currently IR_F and Blue HI are not used
    }
    else
    {
        ir_b_average = (ir_b_average + light_ir_b) / 2;
        // Currently IR_F and Blue HI are not used
    }

    _Bool photo_cal_trouble = 0;
    if (PHOTO_CAL_SAMPLE_CNT <= photo_cal_sample)
    {
        //
        // Calculate alarm threshold
        //

        //
        // Calculate Alarm Threshold based upon data just collected
        // (Calibration ir_b LO/HI currents used)
        //
        photo_cal_trouble = two_point_calibration(ir_b_average);

        //
        // If HD Cal has been completed we still need to Set the
        // Cal Clean air dbase values using HD currents
        // that HD Cal has saved in the dBase
        //
        if (!photo_cal_trouble)
        {
            // Copy Qusi over Factory, since factory has low drive
            // values while Qusi has lic cal values
            // in cases where there is an lic cal done
            db_set_value(db_ir_b_factory_clean_air, db_get_value(db_ir_b_clean_air));

            uint8_t const hush_value = (db_get_value(db_photo_alarm_eqa_threshold) *
                                        db_get_value16(db_hush_multiplier_msb, db_hush_multiplier_lsb)) >>
                                       8;
            db_set_value(db_photo_hush_threshold, hush_value);
            uint8_t const failsafe_value = (db_get_value(db_photo_alarm_eqa_threshold) *
                                            db_get_value16(db_failsafe_multiplier_msb, db_failsafe_multiplier_lsb)) >>
                                           8;
            db_set_value(db_photo_fail_safe_threshold, failsafe_value);

            //schedule_message(0, task_database, msg_update_primary_db, 0);
            //schedule_message(0, task_database, msg_update_backup_db, 0);

            photo_cal_sample = 0;

            serial_send_string("Cal2pt complete");
            // 2 point complete
            db_set_value(db_calibration_lsb, db_get_value(db_calibration_lsb) | cal_2_point);
            //schedule_message(100, task_state, msg_cal_pass, task_cal_2_point);
            return;
        }
    }

    if (db_get_value16(db_serial_enable_msb, db_serial_enable_lsb) & serial_enable_calibration)
    {
        char buffer[] = "Cal2pt Hi Current -    NA -    NA NA -    NA NA \r";
        uint8_to_hex(buffer + 20, dark_b);
        uint8_to_hex(buffer + 28, light_ir_b);
        uint8_to_hex(buffer + 39, ir_b_average);
        serial_send_string(buffer);
    }

    if (photo_cal_trouble)
    {
        // Calibration failed
        //schedule_message(0, task_state, msg_cal_fail, task_cal_2_point);
        serial_send_string_debug("Cal2pt Hi Current - Cal Failed\r", serial_enable_calibration);
    }
    else
    {
        //schedule_message(1000, task_cal_2_point, msg_continue, high_current);
    }
}

//*********************************************************************
/**
 * \brief Evaluate HD cal air samples
 */
//*********************************************************************
static void hd_clean_air_samples(void)
{
    uint8_t photo_reading_arr[num_photo_data];
    if (!read_photo(db_get_value(db_current_ir_f), db_get_value(db_current_ir_b), photo_reading_arr))
    {
        return;
    }

    uint8_t const dark_b     = photo_reading_arr[dark_reading];
    uint8_t const light_ir_b = photo_reading_arr[ir_b_reading];

    if (++photo_cal_sample == 1)
    {
        // Initialize rather than averaging the first value
        b_ambient_average = dark_b;
        ir_b_average      = light_ir_b;
    }
    else
    {
        // Average of the accumulated and the new value
        b_ambient_average = (b_ambient_average + dark_b) / 2;
        ir_b_average      = (ir_b_average + light_ir_b) / 2;
    }

    _Bool photo_cal_trouble = 0;
    if (PHOTO_CAL_SAMPLE_CNT <= photo_cal_sample)
    {
        // If any one of these is true, then something is wrong
        photo_cal_trouble =
            ir_b_average < db_get_value(db_photo_cal_ir_b_min) || ir_b_average > db_get_value(db_photo_cal_ir_b_max);

        if (photo_cal_trouble)
        {
            // Calibration failed
            //schedule_message(0, task_state, msg_cal_fail, task_cal_2_point);
        }
        else
        {
            //
            // Save HD Cal CAV values
            //
            // Leave Factory values, since the low drive current
            // readings are here

            // Also Init the CAV Quiescent Values
            db_set_value(db_ir_b_clean_air, ir_b_average);

            photo_cal_sample = 0;
            //schedule_message(1000, task_cal_2_point, msg_continue, high_current);
        }
    }
    else
    {
        //schedule_message(1000, task_cal_2_point, msg_continue, hd_clean_air);
    }
}

void cal_2_point_task(message_id const message, uint16_t const data)
{
    static state current_state;
    switch (message)
    {
    case msg_init:
        photo_cal_sample = 0;
        //schedule_message(0, task_key_value_store, msg_add_callback,
                         ((uint16_t)task_cal_2_point << 8) | key_avoidance_flags);
        break;
    case msg_cal_begin:
        // Immediately run msg_continue with the first state
        current_state = low_current;
        //schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_2_point);
        break;
    case msg_continue:
        current_state = (state)data;
        if (avoid)
        {
            run = true;
        }
        else
        {
            //schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_2_point);
        }
        break;
    case msg_value:
        if ((key_name)data == key_avoidance_flags)
        {
            avoid = data >> 8;
            if (!avoid && run)
            {
                //schedule_message(0, task_blue_charge_pump, msg_charge_blue_led, task_cal_2_point);
            }
        }
        break;
    case msg_ready:
        if ((task_id)data == task_blue_charge_pump)
        {
            if (avoid)
            {
                run = true;
                break;
            }
            run = false;
            switch (current_state)
            {
            case low_current:
                low_current_samples();
                break;
            case high_current:
                high_current_samples();
                break;
            case hd_clean_air:
                hd_clean_air_samples();
                break;
            case blue_supervision:
                blue_supervision_samples();
                break;
            }
        }
        break;
    default:
        break;
    }
}
