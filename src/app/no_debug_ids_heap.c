//******************************************************************
/*! \file no_debug_ids_heap.c
 *
 * \brief Kidde: Mongoose-fw - Define heap functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_serial.h"
#include "debug_ids_heap.h"
#include "state.h"
#include <stdbool.h>
#include <stdlib.h>

//*********************************************************************
/**
 *	\brief Output debug message
 * \param task Task ID passed to debug output
 * \param message Message ID passed to debug output
 * \param data Any data sent with each debug output
 */
//*********************************************************************
void message_debug_out(task_id const task, message_id const message, uint16_t data)
{
    char buffer[] = "task - ,  , - message - ,  , - data - ,    \r";
    uint8_to_hex(buffer + 8, task);
    uint8_to_hex(buffer + 25, message);
    uint16_to_hex(buffer + 39, data);
    serial_send_string_debug(buffer, serial_enable_heap);
}

//*********************************************************************
/**
 *	\brief Send debug message over serial
 * \param message Message ID sent to serial output
 * \param output_flag Output flag type to serial
 */
//*********************************************************************
void serial_send_msg_debug(message_id const message, Serial_output_flags_type_e output_flag)
{
    serial_send_int_ex_debug(NULL, message, 16, NULL, output_flag);
}
