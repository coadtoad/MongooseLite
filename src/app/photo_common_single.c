//******************************************************************
/*! \file photo_common_single.c
 *
 * \brief Kidde: Mongoose-fw - Implement single LED photo chamber common functions
 *
 * \author Daniel
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "photo_common_single.h"
#include "app_database.h"
#include "app_events.h"
#include "app_serial.h"
#include "drv_afe.h"
#include "drv_photo.h"
#include "drv_timer.h"
#include "drv_vboost.h"
#include "key_value_store.h"
#include "message.h"
#include <limits.h>
#include <stdint.h>

#define PHOTO_TROUBLE_COUNT   0x03
#define AMB_LGT_TROUBLE_COUNT 0x02

#define LED_OFF               false
#define LED_ON                true

//*********************************************************************
/**
 * \brief Number of consecutive Vcc low faults before error.
 */
//*********************************************************************
#define VCC_LOW_ERROR_COUNT   3

//*********************************************************************
/**
 * \brief ambient LED fault low reading
 */
//*********************************************************************
static bool dark_fault_low                         = false;

//*********************************************************************
/**
 * \brief ambient LED fault high reading
 */
//*********************************************************************
static bool dark_fault_high                        = false;

//*********************************************************************
/**
 * \brief structure to hold photo faults
 */
//*********************************************************************
static bool current_fault[number_of_light_signals] = {false, false};

//*********************************************************************
/**
 * \brief Contains the override value for IR-Forward signals.
 * \note if value is 0, override is disabled. Positive values will override with value.
 */
//*********************************************************************
static uint8_t IR_F_Override                       = 0;
//*********************************************************************
/**
 * \brief Contains the override value for IR-Backward signals.
 * \note if value is 0, override is disabled. Positive values will override with value.
 */
//*********************************************************************
static uint8_t IR_B_Override                       = 0;

//*********************************************************************
/**
 * \brief This function set the IR Forward override.
 */
//*********************************************************************
void set_ir_f_override(uint8_t value)
{
    IR_F_Override = value;
}

//*********************************************************************
/**
 * \brief This function set the IR Backward override.
 */
//*********************************************************************
void set_ir_b_override(uint8_t value)
{
    IR_B_Override = value;
}

//*********************************************************************
/**
 * \brief This function set the Blue Forward override. not currently supported in Iris
 */
//*********************************************************************
void set_bl_f_override(uint8_t value)
{
    (void)value; // no blue
}

void clear_photo_override(void)
{
    IR_F_Override = 0;
    IR_B_Override = 0;
}

//*********************************************************************
/**
 * \brief This function returns true if the measurement is valid and false otherwise if the measurement is invalid then
 * the fault flag is set
 *
 * Function : validate_dark_f()
 *
 * PRE-CONDITION: measurement is a dark forward reading  <br>
 *
 * POST-CONDITION: a fault flag is set true if the measurement is invalid
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
_Bool validate_dark(uint8_t const dark_reading)
{
    static uint8_t ambient_debounce_counter_low  = 0;
    static uint8_t ambient_debounce_counter_high = 0;

    if (dark_reading < db_get_value(db_photo_low_dark_fault_threshold))
    {
        if (ambient_debounce_counter_low >= AMB_LGT_TROUBLE_COUNT)
        {
            if (!dark_fault_low)
            {
                // record event history only on fault entry
                //schedule_message(0, task_events, msg_record_history, event_fault_photo_amb_low);
            }
            dark_fault_low = true;
        }
        else
        {
            ++ambient_debounce_counter_low;
        }
    }
    else
    {
        if (ambient_debounce_counter_low)
        {
            // Debounce check
            --ambient_debounce_counter_low;
        }
        else
        {
            if (dark_fault_low)
            {
                // record event history only on fault exit
                //schedule_message(0, task_events, msg_record_history, event_fault_photo_amb_low_clr);
            }
            dark_fault_low = false;
        }
    }

    if (dark_reading > db_get_value(db_photo_hi_dark_fault_threshold))
    {
        if (ambient_debounce_counter_high >= AMB_LGT_TROUBLE_COUNT)
        {
            if (!dark_fault_high)
            {
                // record event history only on fault entry
                //schedule_message(0, task_events, msg_record_history, event_fault_photo_amb_high);
            }
            dark_fault_high = true;
        }
        else
        {
            ++ambient_debounce_counter_high;
        }
    }
    else
    {
        if (ambient_debounce_counter_high)
        {
            // Debounce check
            --ambient_debounce_counter_high;
        }
        else
        {
            if (dark_fault_high)
            {
                // record event history only on fault exit
                //schedule_message(0, task_events, msg_record_history, event_fault_photo_amb_high_clr);
            }
            dark_fault_high = false;
        }
    }

    return dark_fault_low || dark_fault_high;
}

bool dark_faulted(void)
{
    return dark_fault_low || dark_fault_high;
}

//*********************************************************************
/**
 * \brief This function returns true if the measurement is valid and false otherwise if the measurement is invalid then
 * the fault flag is set
 *
 * Function : validate_light()
 *
 * PRE-CONDITION: measurement is a light ir forward reading  <br>
 *
 * POST-CONDITION: a fault flag is set true if the measurement is invalid
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 06/28/2019 </td><td> 0.0.1            </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
_Bool validate_light(const light_signal light_signal_value, uint8_t const light_reading)
{
    static uint8_t fault_debounce_count[number_of_light_signals]     = {0, 0};

    static const event_id fault_photo_event[number_of_light_signals] = {event_fault_photo_fwd, event_fault_photo_bck};
    static const event_id fault_photo_event_clear[number_of_light_signals] = {event_fault_photo_fwd_clr,
                                                                              event_fault_photo_bck_clr};

    uint8_t const min_compare[number_of_light_signals] = {db_get_value(db_photo_low_fwd_light_threshold),
                                                          db_get_value(db_photo_low_bck_light_threshold)};

    if (light_reading < min_compare[light_signal_value]) // fault detect
    {
        if (fault_debounce_count[light_signal_value] >= PHOTO_TROUBLE_COUNT)
        {
            if (!current_fault[light_signal_value])
            {
                // record event history only on fault entry
                //schedule_message(0, task_events, msg_record_history, fault_photo_event[light_signal_value]);
            }
            current_fault[light_signal_value] = true;
        }
        else
        {
            ++fault_debounce_count[light_signal_value];
        }
    }
    else // fault clear
    {
        if (fault_debounce_count[light_signal_value])
        {
            // Debounce check
            --fault_debounce_count[light_signal_value];
        }
        else
        {
            if (current_fault[light_signal_value])
            {
                // record event history only on fault exit
                //schedule_message(0, task_events, msg_record_history, fault_photo_event_clear[light_signal_value]);
            }
            current_fault[light_signal_value] = false;
        }
    }

    return current_fault[light_signal_value];
}

//*********************************************************************
/**
 * \brief This function updates the 4 photo IR LED statuses in the Key Value Store
 *
 * Function : update_ir_photo_fault_kvs()
 *
 * PRE-CONDITION: validate_dark_f, validate_dark_b, validate_light <br>
 *
 * POST-CONDITION: key_ir_photo_faults is updated
 *
 * @see reset_program.asm
 *
 * <br><b> - HISTORY OF CHANGES - </b>
 *
 * <table align="left" style="width:800px">
 * <tr><td> Date       </td><td> Software Version </td><td> Initials </td><td> Description </td></tr>
 * <tr><td> 02/01/2021 </td><td> 21.0.3           </td><td> BW      </td><td> Created </td></tr>
 * </table><br><br>
 * <hr>
 */
//*********************************************************************
void update_ir_photo_fault_kvs(void)
{
    uint8_t ir_photo_faults = ((uint8_t)dark_faulted() << kvs_fwd_ambient_fault) |
                              ((uint8_t)current_fault[IR_F] << kvs_fwd_ir_led_fault) |
                              ((uint8_t)current_fault[IR_B] << kvs_bck_ir_led_fault);

    //schedule_message(0, task_key_value_store, msg_set_value, (uint16_t)ir_photo_faults << 8 | key_ir_photo_faults);
}

//*********************************************************************
/**
 * \brief read_dark() takes a dark reading for the single channel system
 */
//*********************************************************************
uint8_t read_dark(void)
{
    select_high_gain();

    uint8_t const photo_sample = sample(LED_OFF);
    validate_dark(photo_sample);
    return photo_sample;
}

//*********************************************************************
/**
 * \brief This function performs a photo read.  It does this by first preparing the photo read.
 * 			it then turns on the led, takes an ADC sample and then turns off the led.
 * \param photo_reading input light_signal is the sensor that is to be used in the reading
 * \param dac_value this is what the output current is set to.. some phot readings have specific dac values
 * \note function will return override values if photo overrides are enabled
 *
 * \returns returns ADC reading for photo reading from input source.
 */
//*********************************************************************
uint8_t read_light(const light_signal light_signal_value, const uint8_t dac_value)
{
    // return override value if ovverride enabled
    switch (light_signal_value)
    {
    case IR_F:
        if (IR_F_Override)
        {
            return IR_F_Override;
        }
        break;
    case IR_B:
        if (IR_B_Override)
        {
            return IR_B_Override;
        }
        break;
    default:
        break;
    }

    // prepare light reading
    /// @todo How do we handle a failure here?
    set_drive_current(light_signal_value, dac_value);
    select_led_direction(light_signal_value);
    select_high_gain();

    // take light reading
    /// @todo How do we handle a failure here?
    uint8_t const photo_sample = sample(LED_ON);
    validate_light(light_signal_value, photo_sample);
    return photo_sample;
}

bool read_photo(const uint8_t ir_f_dac, const uint8_t ir_b_dac, uint8_t return_array[])
{
    if (!begin_photo_read())
    {
        serial_send_string_debug("begin_photo_read() FAIL \r", serial_enable_debug);
        return false;
    }

    // take a dark measurement
    return_array[dark_reading] = read_dark();

    // take a light measurement
    return_array[ir_f_reading] = read_light(IR_F, ir_f_dac);

    return_array[ir_b_reading] = read_light(IR_B, ir_b_dac);

    // Enable the vboost if was it enabled before taking photo measurements
    if (get_vboost_state())
    {
        vboost_enable();
    }

    // Turn off IRed, disable ADC and make afe enter sleep - low power mode
    end_photo_read();
    return true;
}
