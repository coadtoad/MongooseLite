//******************************************************************
/*! \file no_debug_ids_priority.c
 *
 * \brief Kidde: Mongoose-fw - Define debug ids priority functions
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "app_serial.h"
#include "debug_ids_priority.h"
#include "state.h"
#include <stdbool.h>
#include <stdlib.h>

void serial_send_msg_priority_debug(priority_name const priority, Serial_output_flags_type_e output_flag)
{
    serial_send_int_ex_debug(NULL, priority, 16, NULL, output_flag);
}

void serial_send_msg_serial_flags_debug(uint8_t const new_flag, Serial_output_flags_type_e output_flag)
{
    serial_send_int_ex_debug(NULL, new_flag, 16, NULL, output_flag);
}
