//******************************************************************
/*! \file fade_common.c
 *
 * \brief Kidde: Mongoose-fw - Define the LED Fade functions
 *
 * \author Unknown
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup drivers
 *
 * \version 1.0
 *
 */
//*******************************************************************

#include "fade_common.h"
#include "app_serial.h"

struct fade_data_t
{
    size_t index;
    bool to_on;
    bool fast;
    void (*begin)(void);
    void (*end)(void);
    void (*pwm)(uint8_t const duty);
};

typedef struct
{
    uint8_t duty_cycle;
    uint8_t ms_interval;
} led_profile;

//*********************************************************************
/**
 * \brief Led profile, PWM structure
 */
//*********************************************************************
static led_profile const profile[] = {
    // Array of PWM structure
    {0, 0},   {2, 50},  {4, 50},  {6, 50},  {8, 50},  {10, 50}, {12, 50}, {14, 40}, {16, 40}, {18, 40}, {20, 40},
    {22, 40}, {24, 40}, {26, 40}, {28, 30}, {30, 30}, {32, 30}, {34, 30}, {36, 30}, {38, 30}, {40, 30}, {42, 30},
    {44, 20}, {46, 20}, {48, 20}, {50, 20}, {52, 20}, {54, 20}, {56, 20}, {58, 20}, {60, 20}, {62, 15}, {64, 15},
    {66, 15}, {68, 15}, {70, 15}, {72, 15}, {74, 10}, {76, 10}, {78, 10}, {80, 10}, {82, 10}, {84, 10}, {86, 10},
    {88, 10}, {90, 5},  {92, 5},  {94, 5},  {96, 5},  {98, 5},  {100, 5}};

static fade_data data;

fade_data *fade_begin(bool const fast, void (*begin)(void), void (*end)(void), void (*pwm)(uint8_t const duty))
{
    data.index = 0;
    data.to_on = true;
    data.fast  = fast;
    data.begin = begin;
    data.end   = end;
    data.pwm   = pwm;
    data.begin();
    return &data;
}

void fade_end(fade_data *fade)
{
    fade->end();
}

uint16_t fade_step(fade_data *fade)
{
    // fade off after fading on
    if (profile[fade->index].duty_cycle == 100)
    {
        fade->to_on = false;
    }

    fade->to_on ? fade->index++ : fade->index--;
    // update PWM duty cycle
    fade->pwm(profile[fade->index].duty_cycle);

    if (fade->index)
    {
        return profile[fade->index].ms_interval >> fade->fast;
    }
    else
    {
        return 0;
    }
}
