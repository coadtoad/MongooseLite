//******************************************************************
/*! \file main_loop.c
 *
 * \brief Kidde: Mongoose-fw - "Brief sentence about file"
 *
 * \author author name if any
 * \author (c) 2020 Kidde Safety
 *
 * \ingroup
 *
 * \version 1.0
 *
 */
//*******************************************************************
/******************************************************************************
 * main_loop.c
 *******************************************************************************/

/******************************************************************************
 * Includes
 *******************************************************************************/

#include "drv_led.h"
#include "drv_sleep.h"
#include "drv_timer.h"
#include "hal_gpio.h"
#include "hal_pwr.h"
#include "hal_rcc.h"
#include "hal_rtc.h"
#include "main.h"
#include "utility.h"
#include "hal_nvic.h"


unsigned LEDTask(void) ;
static unsigned bTicTimer ;

typedef enum
{
    led_task_id,
    num_task_ids
} task_id_t;


struct TaskInfo
{
  unsigned TaskTime ;
  unsigned TaskInterval;
  unsigned (*funcptr)(void) ;
} ;

struct TaskInfo LEDTaskInfo = {0, 0, &LEDTask} ;

struct TaskInfo  * const(TaskTable)[num_task_ids] = { &LEDTaskInfo};

void main_loop(void)
{
    static uint8_t task;
	static unsigned interval ;     
    
    rtc_enable_wakeup_timer(168);
    
    for (;;)
    {
		if ( (bTicTimer - TaskTable[task]->TaskTime) >=
		  	TaskTable[task]->TaskInterval)
		{      		
			// task timer has expired 
			#ifdef _CONFIGURE_DEBUG_TIMING_OUTPUT
				// This is for timing each task as it executes
				// put debug pulse here.
				interval = TaskTable[task]->funcptr() ;
				// put debug pulse here.
			#else
				interval = TaskTable[task]->funcptr() ;
			#endif
			
			if (interval != TaskTable[task]->TaskInterval)
			{
			  TaskTable[task]->TaskInterval = interval ;
			  TaskTable[task]->TaskTime = bTicTimer ;
			}
			else
			{
			  TaskTable[task]->TaskTime += interval ;
			}  
		}

        task++ ;
		if (task >= num_task_ids)
		{
            task = 0 ;
            // Put actions here that happen at the end of each task processing loop.
            rtc_set_alarma_subminute(10);
            nvic_enable_irq(irq_rtc_tamp);
            enter_sleep_mode();

        }			
    }
}

unsigned LEDTask(void)
{
    return 10;
}

//*************************************************************************
void	MAIN_Reset_Task(task_id_t taskID)
{
	// To reset the task, set the time to the current time and
	// set the interval to 0.  This will cause the task to execute
	// on the next tic.
	TaskTable[taskID]->TaskTime = bTicTimer ;
	TaskTable[taskID]->TaskInterval = 0 ;
}
