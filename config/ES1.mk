# {"mcuModel":"STM32_G31 ", "mcuAlarmPart":"STM32G031K8", "mcuModulePart":"STM32G031K8", "firmwareOs":"Mongoose"}
product_id := $(basename $(notdir $(lastword $(MAKEFILE_LIST))))

include config/common/feature/common.mk
include config/common/feature/no_interconnect.mk
include config/common/family/afe_TPS8802.mk
include config/common/family/STM32G031.mk
include config/common/feature/no_co.mk
include config/common/feature/battery_3v_discrete.mk

# Make sure that ./gitsha.c is always generated.
# This will cause it to compile and the linker to always run,
# but it guarantees that it will never be out of date.
.PHONY: ./gitsha.c
./gitsha.c :
	echo char const *const git_commit = "$(shell git rev-parse HEAD)$(if $(strip $(shell git status -s)),+changes)";> $@

common_source_files += ./gitsha.c \
                       ./bsp/STM32G031/src/driver/drv_led_4in.c \
                       ./src/app/no_app_blue_charge_pump.c \
                       ./src/app/app_supervisor.c \
                       ./src/app/clean_air_value_single.c \
                       ./src/app/debug_ids_heap.c \
                       ./src/app/debug_ids_priority.c \
                       ./src/app/fade_common.c \
                       ./src/app/photo_common_single.c \
                       ./src/app/vboost.c \
                       ./src/app/calibration/cal_2_point_single.c \
                       ./src/app/calibration/no_cal_doe.c \
                       ./src/app/calibration/cal_lic_single.c \
                       ./src/app/calibration/no_cal_inline.c \
                       ./src/app/input/no_in_ac.c \
                       ./src/app/input/in_battery.c \
                       ./src/app/input/in_button.c \
                       ./src/app/input/in_day_count.c \
                       ./src/app/input/in_photo_single_decision.c \
                       ./src/app/input/in_photo_single.c \
                       ./src/app/output/no_out_voice.c \
                       ./visual_state/coder/expression_task.c \
                       ./visual_state/coder/Project.c
release_source_files += 
debug_source_files += 
doe_source_files += 

cc += "C:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe"

common_defines += CONFIG_PHOTO_SINGLE \
                  BEEP_ON_RESET=0 \
                  PHOTO_CAL_K1=0 \
                  PHOTO_CAL_K2=0 \
                  PHOTO_CAL_K3=0 \
                  PHOTO_CAL_K2_SIGN=0 \
                  PHOTO_CAL_K3_SIGN=0 \
                  PHOTO_CAL_IR_B_HI_I=100 \
                  PHOTO_CAL_IR_B_LO_I=0 \
                  ALM_TH_MAX=0 \
                  ALM_TH_MIN=0 \
                  IR_B_LO_DAC=145 \
                  IR_F_LO_DAC=145 \
                  OFFSET_DAC_CNT=0 \
                  IR_B_HI_DAC=255 \
                  IR_F_HI_DAC=255 \
                  IR_F_PTT_TH=5 \
                  IR_B_PTT_TH=0 \
                  CONFIG_MARKET=MARKET_US \
                  CONFIG_CAL_HD2=CAL_HD2 \
                  CONFIG_CAL_2_POINT=NO_CAL_2_POINT \
                  CONFIG_CAL_INLINE_SMOKE=NO_CAL_INLINE_SMOKE \
                  CONFIG_CURRENT_IR_F=0x30 \
                  CONFIG_CURRENT_IR_B=0xA0 \
                  CONFIG_CURRENT_BL_F=0xFF \
                  CONFIG_PHOTO_SW_WAIT=0x32 \
                  CONFIG_PTT_CURRENT_IR_F=0xFF \
                  CONFIG_PTT_CURRENT_IR_B=0xFF \
                  CONFIG_PHOTO_ALARM_EQA_THRESHOLD=0x26 \
                  CONFIG_PHOTO_HUSH_THRESHOLD=0x2C \
                  CONFIG_PHOTO_FAIL_SAFE_THRESHOLD=0x28 \
                  CONFIG_IR_F_CLEAN_AIR=0xFF \
                  CONFIG_IR_B_CLEAN_AIR=0xFF \
                  CONFIG_BLUE_F_CLEAN_AIR=0xFF \
                  CONFIG_PHOTO_CAL_IR_F_MIN=0x14 \
                  CONFIG_PHOTO_CAL_IR_F_MAX=0x96 \
                  CONFIG_PHOTO_CAL_IR_B_MIN=0x14 \
                  CONFIG_PHOTO_CAL_IR_B_MAX=0xFF \
                  CONFIG_PHOTO_CAL_BLUE_MIN=0xFF \
                  CONFIG_PHOTO_CAL_BLUE_MAX=0xFF \
                  CONFIG_PHOTO_CAL_BLUE_BACK_SUPERVISION=0xFF \
                  CONFIG_CO_CIRCUIT_CAL=0xFF \
                  CONFIG_CO_OFFSET=0xFFFF \
                  CONFIG_CO_SCALE=0xFFFF \
                  CONFIG_BATTERY_VOLTAGE_MIN=0x0570 \
                  CONFIG_HUSH_MULTIPLIER=0xFFFF \
                  CONFIG_FAILSAFE_MULTIPLIER=0xFFFF \
                  AMBIENT_LIGHT_CAL_MULTIPLIER=0x0150 \
                  CONFIG_FAILSAFE_MIN=0x00 \
                  CONFIG_FAILSAFE_MAX=0xFF \
                  CONFIG_PHOTO_READ_SAMPLE_DELAY=5 \
                  CONFIG_PHOTO_READ_SAMPLE_COUNT=6 \
                  CONFIG_PHOTO_READ_SAMPLE_DISCARD=1 \
                  CONFIG_NORMAL_MODE_PERIOD=10 \
                  CONFIG_PHOTO_IR_F_DELTA=0x06 \
                  CONFIG_PHOTO_IR_B_DELTA=0x06 \
                  CONFIG_PHOTO_LOW_DARK_FAULT_TH=0x10 \
                  CONFIG_PHOTO_HI_DARK_FAULT_TH=0x35 \
                  CONFIG_PHOTO_LOW_FWD_LIGHT_TH=0x35 \
                  CONFIG_PHOTO_LOW_BCK_LIGHT_TH=0x00 \
                  CONFIG_SMOLD_COUNT_THRESHOLD=32 \
                  CONFIG_SMOLD_TIME_THRESHOLD=0x01F4 \
                  CONFIG_ROR_THRESHOLD=200 \
                  CONFIG_ROR_CONFIDENCE_CHECK=5 \
                  CONFIG_ROR_MIN_THRESHOLD=10 \
                  CONFIG_ACCEL_THRESHOLD=2400 \
                  CONFIG_ACCEL_ROR_THRESHOLD=10 \
                  CONFIG_ACCEL_CONFIDENCE_CHECK=5 \
                  CONFIG_ACCEL_MIN_THRESHOLD=10 \
                  CONFIG_FAIL_SAFE_HIGH_THRESHOLD=60 \
                  CONFIG_FAIL_SAFE_CONFIDENCE_CHECK=5 \
                  CONFIG_PHOTO_NORMAL_SENSORS=0x01 \
                  CONFIG_INLINE_START_IRF_DELTA_TH=0xFF \
                  CONFIG_INLINE_RATIO_STABILIZATION_TIME=0xFF \
                  CONFIG_INLINE_RATIO_SAMPLE_COUNT=0xFF \
                  CONFIG_IRF_IRB_RATIO_TARGET=0xFFFF \
                  CONFIG_BLF_IRB_RATIO_TARGET=0xFFFF \
                  CONFIG_IRF_IRB_FACTOR_EQUATION_K1=0xFF \
                  CONFIG_IRF_IRB_FACTOR_EQUATION_K2=0xFF \
                  CONFIG_BLF_IRB_FACTOR_EQUATION_K3=0xFF \
                  CONFIG_BLF_IRB_FACTOR_EQUATION_K4=0xFF \
                  CONFIG_IRF_IRB_RATIO_FACTOR=0xFF \
                  CONFIG_BLF_IRB_RATIO_FACTOR=0xFF \
                  CONFIG_HI_AMBIENT_THRESHOLD=0xa0

release_defines += 
debug_defines += 
doe_defines += 

common_includes += ./bsp/STM32G031/inc/hal \
                   ./inc/driver \
                   ./inc/app/output \
                   ./inc/app \
                   ./bsp/AFE_TPS8802 \
                   ./inc \
                   ./inc/app/input \
                   ./inc/app/calibration \
                   ./visual_state/coder \
                   $$($$env:LOCALAPPDATA)/Arm/Packs/Keil/STM32G0xx_DFP/1.3.0/Drivers/CMSIS/Device/ST/STM32G0xx/Include
release_includes += 
debug_includes += 
doe_includes += 

common_build_flags += -xc \
                      --target=arm-arm-none-eabi \
                      -mcpu=cortex-m0plus \
                      -c \
                      -fno-rtti \
                      -Werror \
                      -funsigned-char \
                      -fshort-enums \
                      -fshort-wchar \
                      -ffunction-sections \
                      -Wall \
                      -Wextra \
                      -Wpedantic \
                      -fno-aapcs-bitfield-load \
                      -Wno-varargs
release_build_flags += -flto \
                       -Oz
debug_build_flags += -gdwarf-3 \
                     -O1
doe_build_flags += -O1

common_cpp_flags += -std=c++14
release_cpp_flags += 
debug_cpp_flags += 
doe_cpp_flags += 

common_c_flags += -std=c11
release_c_flags += 
debug_c_flags += 
doe_c_flags += 

dependency_flags += -MD

linker += C:\Keil_v5\ARM\ARMCLANG\bin\armlink.exe
linker_output_flag += -o

common_linker_flags += --cpu Cortex-M0+ \
                       --strict \
                       --scatter ".\bsp\STM32G031\iris.sct" \
                       --entry reset_handler \
                       --keep=vector \
                       --keep=database_struct \
                       --keep=backup_database_struct \
                       --keep=code_crc \
                       --keep=option_bytes \
                       --keep=security_bytes \
                       --summary_stderr \
                       --info summarysizes \
                       --map \
                       --load_addr_map_info \
                       --xref \
                       --callgraph \
                       --symbols \
                       --info sizes \
                       --info totals \
                       --info unused \
                       --info veneers
release_linker_flags += --list ./build/$(product_id)/release/$(product_id).map \
                        --lto
debug_linker_flags += --list ./build/$(product_id)/debug/$(product_id).map
doe_linker_flags += --list ./build/$(product_id)/doe/$(product_id).map

post_build += powershell -Command Copy-Item

common_post_build_flags += 
release_post_build_flags += 
debug_post_build_flags += 
doe_post_build_flags += 

hex_build += C:\Keil_v5\ARM\ARMCLANG\bin\fromelf.exe

hex_build_flags += --i32combined
hex_build_output_flag += --output