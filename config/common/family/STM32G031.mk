include config/common/feature/static_assert.mk

common_source_files += ./bsp/STM32G031/src/hal/hal_adc.c \
                       ./bsp/STM32G031/src/hal/hal_crc.c \
                       ./bsp/STM32G031/src/hal/hal_electronic_signature.c \
                       ./bsp/STM32G031/src/hal/hal_exti.c \
                       ./bsp/STM32G031/src/hal/hal_flash.c \
                       ./bsp/STM32G031/src/hal/hal_gpio.c \
                       ./bsp/STM32G031/src/hal/hal_i2c.c \
                       ./bsp/STM32G031/src/hal/hal_interrupt.c \
                       ./bsp/STM32G031/src/hal/hal_iwdg.c \
                       ./bsp/STM32G031/src/hal/hal_nvic_scb.c \
                       ./bsp/STM32G031/src/hal/hal_pwr.c \
                       ./bsp/STM32G031/src/hal/hal_rcc.c \
                       ./bsp/STM32G031/src/hal/hal_rtc.c \
                       ./bsp/STM32G031/src/hal/hal_tim3.c \
                       ./bsp/STM32G031/src/hal/hal_tim14.c \
                       ./bsp/STM32G031/src/hal/hal_usart.c \
                       ./bsp/STM32G031/src/hal/hal_options.c \
                       ./bsp/STM32G031/src/driver/drv_adc.c \
                       ./bsp/STM32G031/src/driver/drv_battery.c \
                       ./bsp/STM32G031/src/driver/drv_button.c \
                       ./bsp/STM32G031/src/driver/drv_co.c \
                       ./bsp/STM32G031/src/driver/drv_critical_section.c \
                       ./bsp/STM32G031/src/driver/drv_database.c \
                       ./bsp/STM32G031/src/driver/drv_events.c \
                       ./bsp/STM32G031/src/driver/drv_i2c.c \
                       ./bsp/STM32G031/src/driver/drv_intercon.c \
                       ./bsp/STM32G031/src/driver/drv_main.c \
                       ./bsp/STM32G031/src/driver/drv_memory.c \
                       ./bsp/STM32G031/src/driver/drv_photo.c \
                       ./bsp/STM32G031/src/driver/drv_serial.c \
                       ./bsp/STM32G031/src/driver/drv_shared_interrupt_handlers.c \
                       ./bsp/STM32G031/src/driver/drv_sleep.c \
                       ./bsp/STM32G031/src/driver/drv_sounder.c \
                       ./bsp/STM32G031/src/driver/drv_timer.c \
                       ./bsp/STM32G031/src/driver/drv_vboost.c \
                       ./bsp/STM32G031/src/driver/drv_voice.c \
                       ./bsp/STM32G031/src/driver/drv_watchdog.c

common_defines += STM32G031xx

flash_base += 0x08000000
# 4 bytes before the 3 reserved flash pages (0x0800E800, 0x0800F000, 0x0800F800)
crc_address += 0x0800E7FC