common_source_files += ./src/app/calibration/cal_0_ppm.c \
                       ./src/app/calibration/cal_150_ppm.c \
                       ./src/app/calibration/cal_400_ppm.c \
                       ./src/app/calibration/cal_circuit.c \
                       ./src/app/co_common.c \
                       ./src/app/input/in_co.c \

#TODO: Get with EE team to determine CO circuit and 0ppm min/max offsets
common_defines += CONFIG_CAL_CO_CIRCUIT=CAL_CO_CIRCUIT \
                  CONFIG_CAL_0_PPM=CAL_0_PPM \
                  CONFIG_CAL_150_PPM=CAL_150_PPM \
                  CONFIG_CAL_400_PPM=CAL_400_PPM \
                  CO_CKT_MIN_OFFSET=0 \
                  CO_CKT_MAX_OFFSET=255 \
                  CO_0PPM_MIN_OFFSET=0 \
                  CO_0PPM_MAX_OFFSET=255