
common_source_files += ./src/main_loop.c \
                       ./src/app/app_database.c \
                       ./src/app/app_events.c \
                       ./src/app/app_expression.c \
                       ./src/app/app_serial.c \
                       ./src/app/app_supervisor.c  \
                       ./src/app/database.cpp \
                       ./src/app/input/in_battery.c \
                       ./src/app/input/in_button.c \
                       ./src/app/input/in_day_count.c \
                       ./src/app/key_value_store.c \
                       ./src/app/message.c \
                       ./src/app/message_heap.c \
                       ./src/app/state.c \
                       ./src/app/task_list.c \
                       ./src/app/app_supervisor.c \
                       ./src/app/ptt.c
release_source_files +=
debug_source_files +=
doe_source_files +=

common_defines += DEVICE_MODEL=\"$(product_id)\" \
                  __UVISION_VERSION="534" \
                  UNUSED_VALUE=0xFF \
                  CONFIG_SERIAL_ENABLE=0x0080 \
                  CONFIG_SERIAL_FLAGS=0x00 \
                  CONFIG_MFG_DATA_RESERVED=0xFF \
                  CONFIG_PTT_FAULT_HISTORY=0x00 \
                  CONFIG_COUNTER_NO_OF_DAYS=0x0000 \
                  CONFIG_COUNTER_SENSOR_HEALTH=0x0000 \
                  CONFIG_COUNTER_SENSOR_SHORT=0x0000 \
                  CONFIG_COUNTER_SMOKE_CHAMBER=0x0000 \
                  CONFIG_COUNTER_PTT=0x0000 \
                  CONFIG_ERASE_COUNT=0x0000 \
                  CONFIG_PAGE_NUMBER=0x00