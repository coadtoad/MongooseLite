common_source_files += ./src/app/photo_common_single.c \
                       ./src/app/calibration/cal_2_point_single.c \
                       ./src/app/calibration/cal_lic_single.c \
                       ./src/app/clean_air_value_single.c \
                       ./src/app/input/in_photo_single.c \
                       ./src/app/input/in_photo_single_decision.c \
                       ./src/app/calibration/no_cal_inline.c

common_defines += CONFIG_CAL_HD2=CAL_HD2 \
                  CONFIG_CAL_2_POINT=CAL_2_POINT \
                  CONFIG_PHOTO_SINGLE