common_source_files += ./src/app/calibration/cal_2_point_mwma.c \
                       ./src/app/calibration/cal_hd2_mwma.c \
                       ./src/app/clean_air_value_mwma.c \
                       ./src/app/input/in_photo_mwma.c \
                       ./src/app/input/in_photo_mwma_decision.c \
                       ./src/app/photo_common_mwma.c \
                       ./src/app/ptt_photo_mwma.c \
                       ./src/app/calibration/cal_inline_mwma.c \
                       ./src/app/expressions/exp_alarm_memory_smoke.c \
                       ./src/app/expressions/exp_cal_photo.c \
                       ./src/app/expressions/exp_fault_photo.c \
                       ./src/app/expressions/exp_smoke.c \
                       ./src/app/expressions/exp_smoke_hush.c \
                       ./src/app/expressions/exp_cal_inline_smoke.c

common_defines += CONFIG_CAL_HD2=CAL_HD2 \
                  CONFIG_CAL_2_POINT=CAL_2_POINT \
                  CONFIG_FLM_IR_B_THRESHOLD=0x0A \
                  CONFIG_PHOTO_MWMA