common_source_files += ./src/app/calibration/no_cal_0_ppm.c \
                       ./src/app/calibration/no_cal_150_ppm.c \
                       ./src/app/calibration/no_cal_400_ppm.c \
                       ./src/app/calibration/no_cal_circuit.c \
                       ./src/app/input/no_in_co.c \

common_defines += CONFIG_CAL_CO_CIRCUIT=NO_CAL_CO_CIRCUIT \
                  CONFIG_CAL_0_PPM=NO_CAL_0_PPM \
                  CONFIG_CAL_150_PPM=NO_CAL_150_PPM \
                  CONFIG_CAL_400_PPM=NO_CAL_400_PPM