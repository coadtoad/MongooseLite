# This makefile uses all *.mk files in ./config/ to generate build targets.
# The file name of the *.mk (eg. '2040-PG01' for ./config/2040-PG01.mk) is <PDCT>
# in the Provided Targets listed below.
#
# All build artifacts are in the directory ./build/.  Product specific artifacts
# will be in ./build/<PDCT>/.
#
# "make clean" deletes ./build/.  There is no way to clean a single target.
#
# Provided Targets:
#   all - build all variants (release, debug, and doe) for all <PDCT>s
#   release - build release variant for all <PDCT>s
#   debug - build debug variant for all <PDCT>s
#   doe - build doe variant for all <PDCT>s
#   cstat - build release variant and run C-Stat on all <PDCT>s
#   <PDCT> - builds <PDCT>_release, <PDCT>_debug, <PDCT>_doe, and <PDCT>_cstat
#   <PDCT>_release - release build of <PDCT>
#   <PDCT>_debug - debug build of <PDCT>
#   <PDCT>_doe - DOE build of <PDCT>
#   <PDCT>_cstat - release build and C-Stat of <PDCT>
#
# All variables in ./config/<PDCT>.mk should be specified like this:
#    <VAR_NAME> += <VALUE_1> \
#                  <VALUE_2> \
#                  <...> \
#                  <VALUE_N>
# ./config/<PDCT>.mk should provide the following variables:
#   cc - The compiler used to generate object (.o) files
#   ichecks - The executable used to generate the list of C-Stat checks
#   icstat - The C-Stat executable used to analyze a product
#   common_source_files - A list of source files used by all variants
#   release_source_files - A list of source files specific to the release variant
#   debug_source_files - A list of source files specific to the debug variant
#   doe_source_files - A list of source files specific to the doe variant
#   common_defines - A list of #defines to specify on the command line for all variants
#                    Do not include the -D compiler flag
#   release_defines - A list of #defines to specify on the command line for the release variant
#                     Do not include the -D compiler flag
#   debug_defines - A list of #defines to specify on the command line for the debug variant
#                   Do not include the -D compiler flag
#   doe_defines - A list of #defines to specify on the command line for the doe variant
#                 Do not include the -D compiler flag
#   common_includes - A list of directories to search for #include files for all variants
#                     Do not include the -I compiler flag
#   release_includes - A list of directories to search for #include files for the release variant
#                     Do not include the -I compiler flag
#   debug_includes - A list of directories to search for #include files for the debug variant
#                     Do not include the -I compiler flag
#   doe_includes - A list of directories to search for #include files for the doe variant
#                     Do not include the -I compiler flag
#   common_build_flags - A list of additional command line parameters for all variants
#   release_build_flags - A list of additional command line parameters for the release variant
#   debug_build_flags - A list of additional command line parameters for the debug variant
#   doe_build_flags - A list of additional command line parameters for the doe variant
#   common_cpp_flags - A list of additional command line parameters used to build .cpp files for all variants
#                      Note that associated *_build_flags are also used
#   release_cpp_flags - A list of additional command line parameters used to build .cpp files for the release variant
#                       Note that associated *_build_flags are also used
#   debug_cpp_flags - A list of additional command line parameters used to build .cpp files for the debug variant
#                     Note that associated *_build_flags are also used
#   doe_cpp_flags - A list of additional command line parameters used to build .cpp files for the doe variant
#                   Note that associated *_build_flags are also used
#   release_c_flags - A list of additional command line parameters used to build .c files for the release variant
#                     Note that associated *_build_flags are also used
#   debug_c_flags - A list of additional command line parameters used to build .c files for the debug variant
#                   Note that associated *_build_flags are also used
#   doe_c_flags - A list of additional command line parameters used to build .c files for the doe variant
#                 Note that associated *_build_flags are also used
#   dependency_flags - A list of additional command line parameters to generate .d files for header dependencies
#                      Not that associated *_build_flags and *_cpp_flags are also used
#   linker - The linker used to generate an ELF (.out) file from a list of object (.o) files
#   linker_output_flag - The flag (often -o) used to specify the linker output
#   common_linker_flags - A list of linker command line parameters used to link object files for all variants
#   release_linker_flags - A list of linker command line parameters used to link object files for the release variant
#   debug_linker_flags - A list of linker command line parameters used to link object files for the debug variant
#   doe_linker_flags - A list of linker command line parameters used to link object files for the doe variant
#   post_build - An executable used to massage the linker output file
#   common_post_build_flags - A list of command line parameters used to massage the output file for all variants
#   release_post_build_flags - A list of command line parameters used to massage the output file for the release variant
#   debug_post_build_flags - A list of command line parameters used to massage the output file for the debug variant
#   doe_post_build_flags - A list of command line parameters used to massage the output file for the doe variant
#   hex_build - An executable used to generate a .hex from from the output file
#   hex_build_flags - A list of command line parameters used to generate a .hex from the output file
#   hex_build_output_flag - An optional flag that it output immediately before the .hex output file name
#   flash_base - The base address, in hex, for flash
#   crc_address - The address, in hex, of the code-space CRC
#
#          | Normal         | DOE
# Release  | <PDCT>_release | <PDCT>_doe
# Debug    | <PDCT>_debug   |
#
# <PDCT>_release -> <BASE>/build/<PDCT>/release
# <PDCT>_debug -> <BASE>/build/<PDCT>/debug
# <PDCT>_doe -> <BASE>/build/<PDCT>/doe
#
# URLs to documentation used for development
# http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
# http://ismail.badawi.io/blog/2017/03/28/automatic-directory-creation-in-make/
# https://www.gnu.org/software/make/manual/html_node/Functions.html#Functions
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

.SECONDEXPANSION:

config_dir = ./config
build_dir = ./build
release_dir = release
debug_dir = debug
doe_dir = doe

files_list = $(sort $(basename $(wildcard $(config_dir)/*.mk)))

.PHONY: all release debug doe clean cstat format

all: release debug doe cstat

format:
	bin/clang-format.exe -i bsp/AFE_TPS8802/*.c
	bin/clang-format.exe -i bsp/STM32G031/inc/hal/*.h
	bin/clang-format.exe -i bsp/STM32G031/src/driver/*.c
	bin/clang-format.exe -i bsp/STM32G031/src/hal/*.c
	bin/clang-format.exe -i inc/*.h
	bin/clang-format.exe -i inc/app/*.h
	bin/clang-format.exe -i inc/app/calibration/*.h
	bin/clang-format.exe -i inc/app/input/*.h
	bin/clang-format.exe -i inc/app/output/*.h
	bin/clang-format.exe -i inc/driver/*.h
	bin/clang-format.exe -i src/*.c
	bin/clang-format.exe -i src/app/*.c
	bin/clang-format.exe -i src/app/*.cpp
	bin/clang-format.exe -i src/app/calibration/*.c
	bin/clang-format.exe -i src/app/input/*.c
	bin/clang-format.exe -i src/app/output/*.c

clean:
	if exist "$(build_dir)" rmdir /S /Q "$(build_dir)"

# Make sure output directories aren't deleted by make thinking they are intermediate files
.PRECIOUS: $(build_dir)/. $(build_dir)%/.
$(build_dir)/.:
	if not exist "$@" mkdir "$@"
$(build_dir)%/.:
	if not exist "$@" mkdir "$@"

# $(1) - source file
# $(2) - build directory, not including app, hal, or driver
object_file = $(2)/$(lastword $(subst /, ,$(dir $(1))))/$(basename $(notdir $(1))).o

# $(1) - source file list
# $(2) - build directory, not including app, hal, or driver
object_file_list = $(foreach v,$(1),$(call object_file,$(v),$(2)))

# $(1) - source file
# $(2) - output directory
# $(3) - cc - compiler executable
# $(4) - defines
# $(5) - includes
# $(6) - other commandline arguments
# $(7) - dependency commandline arguments
define object_template =
 # Empty rule for header dependency file
 $(basename $(call object_file,$(1),$(2))).d:

 # Generate .o and .d files
 $(call object_file,$(1),$(2)) : $(1) $(basename $(call object_file,$(1),$(2))).d | $$$$(@D)/.
	$(3) $$< -o $$@ $(4) $(5) $(6) $(7)

 # Include header dependencies if the file exists
 include $(wildcard $(basename $(call object_file,$(1),$(2))).d)
endef

define target_template =
 release: $(1)_release
 debug:   $(1)_debug
 doe:     $(1)_doe
 cstat:   $(1)_cstat

 .PHONY: $(1) $(1)_release $(1)_debug $(1)_doe $(1)_cstat

 $(1):         $(1)_release $(1)_debug $(1)_doe $(1)_cstat
 $(1)_release: $(build_dir)/$(1)/$(release_dir)/$(1).hex
 $(1)_debug:   $(build_dir)/$(1)/$(debug_dir)/$(1).hex
 $(1)_doe:     $(build_dir)/$(1)/$(doe_dir)/$(1).hex
 $(1)_cstat:   $(build_dir)/$(1)/$(1)_results.txt

 cc := 
 icstat := 
 ichecks := 
 common_source_files := 
 release_source_files := 
 debug_source_files := 
 doe_source_files := 
 common_defines := 
 release_defines := 
 debug_defines := 
 doe_defines := 
 common_includes := 
 release_includes := 
 debug_includes := 
 doe_includes := 
 common_build_flags := 
 release_build_flags := 
 debug_build_flags := 
 doe_build_flags := 
 common_cpp_flags := 
 release_cpp_flags := 
 debug_cpp_flags := 
 doe_cpp_flags := 
 release_c_flags := 
 debug_c_flags := 
 doe_c_flags := 
 dependency_flags := 
 linker := 
 linker_output_flag := 
 common_linker_flags := 
 release_linker_flags := 
 debug_linker_flags := 
 doe_linker_flags := 
 post_build := 
 common_post_build_flags := 
 release_post_build_flags :=
 debug_post_build_flags := 
 doe_post_build_flags := 
 hex_build := 
 hex_build_flags := 
 hex_build_output_flag := 
 flash_base := 
 crc_address := 

 include $(config_dir)/$(1).mk

 cc_$(1) := $$(cc)
 icstat_$(1) := $$(icstat)
 ichecks_$(1) := $$(ichecks)

 release_source_files_$(1) := $$(sort $$(common_source_files) $$(release_source_files))
 debug_source_files_$(1) := $$(sort $$(common_source_files) $$(debug_source_files))
 doe_source_files_$(1) := $$(sort $$(common_source_files) $$(doe_source_files))

 release_defines_$(1) := $$(foreach v,$$(common_defines) $$(release_defines),-D$$(v))
 debug_defines_$(1) := $$(foreach v,$$(common_defines) $$(debug_defines),-D$$(v))
 doe_defines_$(1) := $$(foreach v,$$(common_defines) $$(doe_defines),-D$$(v))

 release_includes_$(1) := $$(foreach v,$$(common_includes) $$(release_includes),-I$$(v))
 debug_includes_$(1) := $$(foreach v,$$(common_includes) $$(debug_includes),-I$$(v))
 doe_includes_$(1) := $$(foreach v,$$(common_includes) $$(doe_includes),-I$$(v))

 release_build_flags_$(1) := $$(common_build_flags) $$(release_build_flags)
 debug_build_flags_$(1) := $$(common_build_flags) $$(debug_build_flags)
 doe_build_flags_$(1) := $$(common_build_flags) $$(doe_build_flags)

 release_cpp_flags_$(1) := $$(common_cpp_flags) $$(release_cpp_flags)
 debug_cpp_flags_$(1) := $$(common_cpp_flags) $$(debug_cpp_flags)
 doe_cpp_flags_$(1) := $$(common_cpp_flags) $$(doe_cpp_flags)

 release_c_flags_$(1) := $$(common_c_flags) $$(release_c_flags)
 debug_c_flags_$(1) := $$(common_c_flags) $$(debug_c_flags)
 doe_c_flags_$(1) := $$(common_c_flags) $$(doe_c_flags)

 dependency_flags_$(1) := $$(dependency_flags)
 linker_$(1) := $$(linker)
 linker_output_flag_$(1) := $$(linker_output_flag)

 release_linker_flags_$(1) := $$(common_linker_flags) $$(release_linker_flags)
 debug_linker_flags_$(1) := $$(common_linker_flags) $$(debug_linker_flags)
 doe_linker_flags_$(1) := $$(common_linker_flags) $$(doe_linker_flags)
 
 post_build_$(1) := $$(post_build)

 release_post_build_flags_$(1) := $$(common_post_build_flags) $$(release_post_build_flags)
 debug_post_build_flags_$(1) := $$(common_post_build_flags) $$(debug_post_build_flags)
 doe_post_build_flags_$(1) := $$(common_post_build_flags) $$(doe_post_build_flags)
 
 hex_build_$(1) := $$(hex_build)
 hex_build_flags_$(1) := $$(hex_build_flags)
 hex_build_output_flag_$(1) := $$(hex_build_output_flag)
 flash_base_$(1) := $$(flash_base)
 crc_address_$(1) := $$(crc_address)

 # Variables for output directories for this product
 release_dir_$(1) := $(build_dir)/$(1)/$(release_dir)
 debug_dir_$(1) := $(build_dir)/$(1)/$(debug_dir)
 doe_dir_$(1) := $(build_dir)/$(1)/$(doe_dir)

 ## ***C-STAT TARGETS***
 $(build_dir)/$(1)/checks.ch: | $$$$(@D)/.
	$$(ichecks_$(1)) --default stdchecks --output $$@
 $(build_dir)/$(1)/commands.txt: | $$$$(@D)/.
	$$(file > $$@,)
	$$(foreach v,$$(release_source_files_$(1)),$$(shell echo analyze -- $$(cc_$(1)) $$(v) -o $$(call object_file,$$(v),$$(release_dir_$(1))) $$(release_defines_$(1)) $$(release_includes_$(1)) $$(if $$(filter .cpp,$$(suffix $$(v))),$$(release_cpp_flags_$(1)),$$(release_c_flags_$(1))) $$(release_build_flags_$(1)) $$(dependency_flags_$(1)) >> $$@))
	$$(shell echo link_analyze -- $$(linker_$(1)) $$(call object_file_list,$$(release_source_files_$(1)),$$(release_dir_$(1))) $$(linker_output_flag_$(1))$$@ $$(release_linker_flags_$(1)) >> $$@)
 $(build_dir)/$(1)/$(1)_results.txt: $(build_dir)/$(1)/checks.ch $(build_dir)/$(1)/commands.txt $(1)_release | $$$$(@D)/.
	$$(icstat_$(1)) --db $(build_dir)/$(1)/$(1).db clear
	$$(icstat_$(1)) --db $(build_dir)/$(1)/$(1).db --checks $(build_dir)/$(1)/checks.ch commands $(build_dir)/$(1)/commands.txt --parallel 12 > $$@

 ## ***RELEASE TARGETS***
 # Generate targets for all .o files
 $$(foreach v,$$(release_source_files_$(1)),$$(eval $$(call object_template,$$(v),$$(release_dir_$(1)),$$(cc_$(1)),$$(release_defines_$(1)),$$(release_includes_$(1)),$$(if $$(filter .cpp,$$(suffix $$(v))),$$(release_cpp_flags_$(1)),$$(release_c_flags_$(1))) $$(release_build_flags_$(1)),$$(dependency_flags_$(1)))))
 # Link build
 $$(release_dir_$(1))/$(1).out.tmp: $$(call object_file_list,$$(release_source_files_$(1)),$$(release_dir_$(1))) | $$$$(@D)/.
	$$(linker_$(1)) $$^ $$(linker_output_flag_$(1))$$@ $$(release_linker_flags_$(1))
 $$(release_dir_$(1))/$(1).out: $$(release_dir_$(1))/$(1).out.tmp | $$$$(@D)/.
	$$(post_build_$(1)) $$(release_post_build_flags_$(1)) $$^ $$@
 # Generate hex
 $$(release_dir_$(1))/$(1).hex: $$(release_dir_$(1))/$(1).out | $$$$(@D)/.
	$$(hex_build_$(1)) $$(hex_build_flags_$(1)) $$^ $$(hex_build_output_flag_$(1)) $$@
	./bin/intel_hex_crc.exe "$$@" $$(flash_base_$(1)) $$(crc_address_$(1))

 ## ***DEBUG TARGETS***
 # Generate targets for all .o files
 $$(foreach v,$$(debug_source_files_$(1)),$$(eval $$(call object_template,$$(v),$$(debug_dir_$(1)),$$(cc_$(1)),$$(debug_defines_$(1)),$$(debug_includes_$(1)),$$(if $$(filter .cpp,$$(suffix $$(v))),$$(release_cpp_flags_$(1)),$$(release_c_flags_$(1))) $$(debug_build_flags_$(1)),$$(dependency_flags_$(1)))))
 # Link build
 $$(debug_dir_$(1))/$(1).out.tmp: $$(call object_file_list,$$(debug_source_files_$(1)),$$(debug_dir_$(1))) | $$$$(@D)/.
	$$(linker_$(1)) $$^ $$(linker_output_flag_$(1))$$@ $$(debug_linker_flags_$(1))
 $$(debug_dir_$(1))/$(1).out: $$(debug_dir_$(1))/$(1).out.tmp | $$$$(@D)/.
	$$(post_build_$(1)) $$(debug_post_build_flags_$(1)) $$^ $$@
 # Generate hex
 $$(debug_dir_$(1))/$(1).hex: $$(debug_dir_$(1))/$(1).out | $$$$(@D)/.
	$$(hex_build_$(1)) $$(hex_build_flags_$(1)) $$^ $$(hex_build_output_flag_$(1)) $$@
	./bin/intel_hex_crc.exe "$$@" $$(flash_base_$(1)) $$(crc_address_$(1))

 ## ***DOE TARGETS***
 # Generate targets for all .o files
 $$(foreach v,$$(doe_source_files_$(1)),$$(eval $$(call object_template,$$(v),$$(doe_dir_$(1)),$$(cc_$(1)),$$(doe_defines_$(1)),$$(doe_includes_$(1)),$$(if $$(filter .cpp,$$(suffix $$(v))),$$(doe_cpp_flags_$(1)),$$(doe_c_flags_$(1))) $$(doe_build_flags_$(1)),$$(dependency_flags_$(1)))))
 # Link build
 $$(doe_dir_$(1))/$(1).out.tmp: $$(call object_file_list,$$(doe_source_files_$(1)),$$(doe_dir_$(1))) | $$$$(@D)/.
	$$(linker_$(1)) $$^ $$(linker_output_flag_$(1))$$@ $$(doe_linker_flags_$(1))
 $$(doe_dir_$(1))/$(1).out: $$(doe_dir_$(1))/$(1).out.tmp | $$$$(@D)/.
	$$(post_build_$(1)) $$(doe_post_build_flags_$(1)) $$^ $$@
 # Generate hex
 $$(doe_dir_$(1))/$(1).hex: $$(doe_dir_$(1))/$(1).out | $$$$(@D)/.
	$$(hex_build_$(1)) $$(hex_build_flags_$(1)) $$^ $$(hex_build_output_flag_$(1)) $$@
	./bin/intel_hex_crc.exe "$$@" $$(flash_base_$(1)) $$(crc_address_$(1))
endef

# Generate targets for each .mk file in $(config_dir)
$(foreach v,$(notdir $(files_list)),$(eval $(call target_template,$(v))))

print-% : ; @echo $* = $($*)